/****************************************************************************************************************************************************************************************
 PROGRAMA: GenerarPDF.cs
 VERSION : 1.0
 OBJETIVO: Clase para generar PDF
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using iTextSharp.text;
using iTextSharp.text.pdf;
using ThoughtWorks.QRCode.Codec;
using CEN;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Newtonsoft.Json;


namespace AppConfiguracion
{


public class GenerarPDF
{
    
    public void GenerarPDFBoletaFactura(EN_Empresa oEmpresa, EN_Documento oDocumento, string montoenletras, string monedaenletras, List<EN_DetalleDocumento> oDetalle, EN_ComprobanteSunat oCompSunat, string valorResumen, string firma, string rutabase, string ruta, string symbmoneda, string strGuias, string strAnticipos,  string Tipodoccliente, string situacion = EN_Constante.g_const_vacio, string mensaje = EN_Constante.g_const_vacio)
    {
        //DESCRIPCION: FUNCION PARA GENERAR PDF DE BOLETA O FACTURA 

        string rutalogo = string.Format("{0}/{1}/", rutabase, oDocumento.NroDocEmpresa);
        Document doc = new Document(PageSize.A4, 20.0F, 20.0F, 20.0F, 20.0F);
        string filename = ruta + oEmpresa.Nrodocumento.Trim() + "-" + oDocumento.Idtipodocumento.Trim() + "-" + oDocumento.Serie.Trim() + "-" + oDocumento.Numero.Trim() + EN_Constante.g_const_extension_pdf;

        string logo = rutalogo + oEmpresa.Imagen;

        if (filename.Trim() != EN_Constante.g_const_vacio)
        {
            FileStream file = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            PdfWriter.GetInstance(doc, file);
            doc.Open();

            // Para el logo
           dynamic imagen = null;

            if (File.Exists(logo))
            {

                imagen = iTextSharp.text.Image.GetInstance(logo);
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_LEFT;
                imagen.ScaleAbsolute(100, 100);
            }
            else
            {
                imagen=new Phrase(EN_Constante.g_const_vacio);
            }


            // Para el codigo QR
            string valorCodigoBarras;
            valorCodigoBarras = oEmpresa.Nrodocumento.Trim() + "|" + 
                            oDocumento.Idtipodocumento.Trim() + "|" + 
                            oDocumento.Serie.Trim() + "|" + 
                            oDocumento.Numero.Trim() + "|" + 
                            oDocumento.Igvventa + "|" + 
                            oDocumento.Importefinal + "|" + 
                            Strings.Format(Conversions.ToDate(oDocumento.Fecha), EN_Constante.g_const_formfecha_yyyyMM)+ "|" + 
                            ( (oDocumento.Tipodoccliente.Trim() == EN_Constante.g_const_vacio)? EN_Constante.g_const_0.ToString(): oDocumento.Tipodoccliente) + "|" + 
                            ((oDocumento.Nrodoccliente.Trim() == EN_Constante.g_const_vacio)? "-": oDocumento.Nrodoccliente);
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode(valorCodigoBarras, 1000,1000, null);
            iTextSharp.text.Image pdfImage= barcodeQRCode.GetImage();
            pdfImage.ScaleAbsolute(70, 70);
            pdfImage.Alignment = EN_Constante.g_const_2;
            // Fin Codigo QR


            // Tabla para Tipo de documento - RUC - Serie y Numero de documento
            PdfPTable table1 = new PdfPTable(1);
            string textoTipoDoc = EN_Constante.g_const_vacio;
            string textoTipoDoc2 = EN_Constante.g_const_vacio;
            if (oDocumento.Idtipodocumento.Trim() == oCompSunat.IdFC)
            {
                textoTipoDoc = EN_Constante.g_const_factura;
                textoTipoDoc2 = EN_Constante.g_const_electronica;
            }
            else if (oDocumento.Idtipodocumento.Trim() == oCompSunat.IdBV)
            {
                textoTipoDoc = EN_Constante.g_const_boletaVenta;
                textoTipoDoc2 = EN_Constante.g_const_electronica;
            }
            Chunk ruc = new Chunk("RUC N° " + oEmpresa.Nrodocumento, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            Chunk tipodocumento = new Chunk(textoTipoDoc, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            Chunk tipodocumento2 = new Chunk(textoTipoDoc2, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            Chunk nrodocumento = new Chunk(oDocumento.Serie + "-" + oDocumento.Numero, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            // Alineacion 0=Left, 1=Centre, 2=Right
            PdfPCell cell12 = new PdfPCell(new Phrase(ruc));
            cell12.HorizontalAlignment = 1;
            cell12.BorderWidthBottom = 0.0F;
            cell12.FixedHeight = 18.0F;
            PdfPCell cell11 = new PdfPCell(new Phrase(tipodocumento));
            cell11.HorizontalAlignment = 1;
            cell11.BorderWidthBottom = 0.0F;
            cell11.BorderWidthTop = 0.0F;
            cell11.FixedHeight = 18.0F;
            PdfPCell cell14 = new PdfPCell(new Phrase(tipodocumento2));
            cell14.HorizontalAlignment = 1;
            cell14.BorderWidthBottom = 0.0F;
            cell14.BorderWidthTop = 0.0F;
            cell14.FixedHeight = 18.0F;
            PdfPCell cell13 = new PdfPCell(new Phrase(nrodocumento));
            cell13.HorizontalAlignment = 1;
            cell13.BorderWidthTop = 0.0F;
            cell13.FixedHeight = 18.0F;

            table1.AddCell(cell12);
            table1.AddCell(cell11);
            table1.AddCell(cell14);
            table1.AddCell(cell13);
            // Tamaño de tabla
            table1.WidthPercentage = 15.0F;
            table1.HorizontalAlignment = 2;

            var outerTable = new PdfPTable(2);
            var outTblWidth2 = new int[] { (int)Math.Round(65.0), (int)Math.Round(35.0) };
                outerTable.SetWidths(outTblWidth2);
            PdfPCell c1 = new PdfPCell(imagen);
            PdfPCell c2 = new PdfPCell(table1);
            c1.BorderWidthBottom = 0.0F;
            c1.BorderWidthTop = 0.0F;
            c1.BorderWidthLeft = 0.0F;
            c1.BorderWidthRight = 0.0F;
            c2.BorderWidthBottom = 0.0F;
            c2.BorderWidthTop = 0.0F;
            c2.BorderWidthLeft = 0.0F;
            c2.BorderWidthRight = 0.0F;
            c1.HorizontalAlignment = 0;
            c2.HorizontalAlignment = 2;
            outerTable.AddCell(c1);
            outerTable.AddCell(c2);
            outerTable.WidthPercentage = 100.0F;
            outerTable.HorizontalAlignment = 0;
            doc.Add(outerTable);

            // Mostramos los datos de la empresa
            Chunk razonsocial = new Chunk(oEmpresa.RazonSocial, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
            Chunk nombrecomercial = new Chunk(oEmpresa.Nomcomercial, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));
            Chunk direccion = new Chunk(oEmpresa.Direccion + " " + oEmpresa.Sector, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            Chunk ubicacion = new Chunk(oEmpresa.Distrito + " - " + oEmpresa.Provincia + " - " + oEmpresa.Departamento + " - PERÚ", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            doc.Add(new Paragraph(razonsocial));
            doc.Add(new Paragraph(nombrecomercial));
            doc.Add(new Paragraph(direccion));
            doc.Add(new Paragraph(ubicacion));
            // doc.Add(New Paragraph(" "))

            // Tabla para datos del cabecera
            string tipdoccli = Tipodoccliente;
            


            PdfPTable table2A = new PdfPTable(4);
            var intTblWidth2A = new int[] { (int)Math.Round(15.0), (int)Math.Round(50.0), (int)Math.Round(15.0), (int)Math.Round(20.0) };
                table2A.SetWidths(intTblWidth2A);

            Chunk tnombreclienteB = new Chunk("Nombre:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tnombreclienteF = new Chunk("Razón Social:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            Chunk tnrodocclienteB = new Chunk("Doc.Identidad:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tnrodocclienteF = new Chunk("RUC:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            Chunk tdireccioncliente = new Chunk("Direccion:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tfechaemision = new Chunk("Fecha Emisión:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            Chunk tmoneda = new Chunk("Moneda:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tordencompra = new Chunk("Orden de Compra:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            Chunk nombrecliente = new Chunk(oDocumento.Nombrecliente, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            Chunk nrodocclienteB = new Chunk(tipdoccli + " " + oDocumento.Nrodoccliente, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            Chunk nrodocclienteF = new Chunk(oDocumento.Nrodoccliente, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            Chunk direccioncliente = new Chunk(oDocumento.Direccioncliente, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
   
            Chunk fechaemision = new Chunk( Convert.ToDateTime(oDocumento.Fecha).ToString("dd/MM/yyyy"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            Chunk moneda = new Chunk(monedaenletras, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            Chunk ordencompra = new Chunk(oDocumento.OrdenCompra, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // Espacio de Tabla
            PdfPCell cell20A = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell20A.HorizontalAlignment = 0;
            cell20A.BorderWidthTop = 0.0F;
            cell20A.BorderWidthBottom = 0.0F;
            cell20A.BorderWidthLeft = 0.0F;
            cell20A.BorderWidthRight = 0.0F;
            cell20A.FixedHeight = 8.0F;

            PdfPCell cell20B = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell20B.HorizontalAlignment = 0;
            cell20B.BorderWidthTop = 0.0F;
            cell20B.BorderWidthBottom = 0.0F;
            cell20B.BorderWidthLeft = 0.0F;
            cell20B.BorderWidthRight = 0.0F;
            cell20B.FixedHeight = 8.0F;

            PdfPCell cell20C = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell20C.HorizontalAlignment = 0;
            cell20C.BorderWidthTop = 0.0F;
            cell20C.BorderWidthBottom = 0.0F;
            cell20C.BorderWidthLeft = 0.0F;
            cell20C.BorderWidthRight = 0.0F;
            cell20C.FixedHeight = 8.0F;

            PdfPCell cell20D = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell20D.HorizontalAlignment = 0;
            cell20D.BorderWidthTop = 0.0F;
            cell20D.BorderWidthBottom = 0.0F;
            cell20D.BorderWidthLeft = 0.0F;
            cell20D.BorderWidthRight = 0.0F;
            cell20D.FixedHeight = 8.0F;

            // Nombre del cliente
            PdfPCell cell22AB = new PdfPCell(new Phrase(tnombreclienteB));
            cell22AB.HorizontalAlignment = 0;
            cell22AB.BorderWidthTop = 0.0F;
            cell22AB.BorderWidthBottom = 0.0F;
            cell22AB.BorderWidthLeft = 0.0F;
            cell22AB.BorderWidthRight = 0.0F;
            // cell22AB.FixedHeight = 14.0F

            PdfPCell cell22AF = new PdfPCell(new Phrase(tnombreclienteF));
            cell22AF.HorizontalAlignment = 0;
            cell22AF.BorderWidthTop = 0.0F;
            cell22AF.BorderWidthBottom = 0.0F;
            cell22AF.BorderWidthLeft = 0.0F;
            cell22AF.BorderWidthRight = 0.0F;
            // cell22AF.FixedHeight = 14.0F

            PdfPCell cell22B = new PdfPCell(new Phrase(nombrecliente));
            cell22B.HorizontalAlignment = 0;
            cell22B.BorderWidthTop = 0.0F;
            cell22B.BorderWidthBottom = 0.0F;
            cell22B.BorderWidthLeft = 0.0F;
            cell22B.BorderWidthRight = 0.0F;
            // cell22B.FixedHeight = 14.0F

            // Nro de Documento del cliente
            PdfPCell cell23AB = new PdfPCell(new Phrase(tnrodocclienteB));
            cell23AB.HorizontalAlignment = 0;
            cell23AB.BorderWidthTop = 0.0F;
            cell23AB.BorderWidthBottom = 0.0F;
            cell23AB.BorderWidthLeft = 0.0F;
            cell23AB.BorderWidthRight = 0.0F;
            // cell23AB.FixedHeight = 14.0F

            PdfPCell cell23AF = new PdfPCell(new Phrase(tnrodocclienteF));
            cell23AF.HorizontalAlignment = 0;
            cell23AF.BorderWidthTop = 0.0F;
            cell23AF.BorderWidthBottom = 0.0F;
            cell23AF.BorderWidthLeft = 0.0F;
            cell23AF.BorderWidthRight = 0.0F;
            // cell23AF.FixedHeight = 14.0F

            PdfPCell cell23BB = new PdfPCell(new Phrase(nrodocclienteB));
            cell23BB.HorizontalAlignment = 0;
            cell23BB.BorderWidthTop = 0.0F;
            cell23BB.BorderWidthBottom = 0.0F;
            cell23BB.BorderWidthLeft = 0.0F;
            cell23BB.BorderWidthRight = 0.0F;
            // cell23BB.FixedHeight = 14.0F

            PdfPCell cell23BF = new PdfPCell(new Phrase(nrodocclienteF));
            cell23BF.HorizontalAlignment = 0;
            cell23BF.BorderWidthTop = 0.0F;
            cell23BF.BorderWidthBottom = 0.0F;
            cell23BF.BorderWidthLeft = 0.0F;
            cell23BF.BorderWidthRight = 0.0F;
            // cell23BF.FixedHeight = 14.0F

            // Direccion del cliente
            PdfPCell cell24A = new PdfPCell(new Phrase(tdireccioncliente));
            cell24A.HorizontalAlignment = 0;
            cell24A.BorderWidthTop = 0.0F;
            cell24A.BorderWidthBottom = 0.0F;
            cell24A.BorderWidthLeft = 0.0F;
            cell24A.BorderWidthRight = 0.0F;
            // cell24A.FixedHeight = 14.0F

            PdfPCell cell24B = new PdfPCell(new Phrase(direccioncliente));
            cell24B.HorizontalAlignment = 0;
            cell24B.BorderWidthTop = 0.0F;
            cell24B.BorderWidthBottom = 0.0F;
            cell24B.BorderWidthLeft = 0.0F;
            cell24B.BorderWidthRight = 0.0F;
            // cell24B.FixedHeight = 14.0F

            // Fecha de emision
            PdfPCell cell21A = new PdfPCell(new Phrase(tfechaemision));
            cell21A.HorizontalAlignment = 0;
            cell21A.BorderWidthTop = 0.0F;
            cell21A.BorderWidthBottom = 0.0F;
            cell21A.BorderWidthLeft = 0.0F;
            cell21A.BorderWidthRight = 0.0F;
            // cell21A.FixedHeight = 14.0F

            PdfPCell cell21B = new PdfPCell(new Phrase(fechaemision));
            cell21B.HorizontalAlignment = 0;
            cell21B.BorderWidthTop = 0.0F;
            cell21B.BorderWidthBottom = 0.0F;
            cell21B.BorderWidthLeft = 0.0F;
            cell21B.BorderWidthRight = 0.0F;
            // cell21B.FixedHeight = 14.0F

            // Moneda
            PdfPCell cell25A = new PdfPCell(new Phrase(tmoneda));
            cell25A.HorizontalAlignment = 0;
            cell25A.BorderWidthTop = 0.0F;
            cell25A.BorderWidthBottom = 0.0F;
            cell25A.BorderWidthLeft = 0.0F;
            cell25A.BorderWidthRight = 0.0F;
            cell25A.FixedHeight = 14.0F;

            PdfPCell cell25B = new PdfPCell(new Phrase(moneda));
            cell25B.HorizontalAlignment = 0;
            cell25B.BorderWidthTop = 0.0F;
            cell25B.BorderWidthBottom = 0.0F;
            cell25B.BorderWidthLeft = 0.0F;
            cell25B.BorderWidthRight = 0.0F;
            cell25B.FixedHeight = 14.0F;

            // Orden de compra
            PdfPCell cell26A = new PdfPCell(new Phrase(tordencompra));
            cell26A.HorizontalAlignment = 0;
            cell26A.BorderWidthTop = 0.0F;
            cell26A.BorderWidthBottom = 0.0F;
            cell26A.BorderWidthLeft = 0.0F;
            cell26A.BorderWidthRight = 0.0F;
            cell26A.FixedHeight = 14.0F;

            PdfPCell cell26B = new PdfPCell(new Phrase(ordencompra));
            cell26B.HorizontalAlignment = 0;
            cell26B.BorderWidthTop = 0.0F;
            cell26B.BorderWidthBottom = 0.0F;
            cell26B.BorderWidthLeft = 0.0F;
            cell26B.BorderWidthRight = 0.0F;
            cell26B.FixedHeight = 14.0F;

            // Espacio de Tabla
            PdfPCell cell27A = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell27A.HorizontalAlignment = 0;
            cell27A.BorderWidthTop = 0.0F;
            cell27A.BorderWidthBottom = 0.0F;
            cell27A.BorderWidthLeft = 0.0F;
            cell27A.BorderWidthRight = 0.0F;
            cell27A.FixedHeight = 8.0F;

            PdfPCell cell27B = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell27B.HorizontalAlignment = 0;
            cell27B.BorderWidthTop = 0.0F;
            cell27B.BorderWidthBottom = 0.0F;
            cell27B.BorderWidthLeft = 0.0F;
            cell27B.BorderWidthRight = 0.0F;
            cell27B.FixedHeight = 8.0F;

            PdfPCell cell28A = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell28A.HorizontalAlignment = 0;
            cell28A.BorderWidthTop = 0.0F;
            cell28A.BorderWidthBottom = 0.0F;
            cell28A.BorderWidthLeft = 0.0F;
            cell28A.BorderWidthRight = 0.0F;
            cell28A.FixedHeight = 8.0F;

            PdfPCell cell28B = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell28B.HorizontalAlignment = 0;
            cell28B.BorderWidthTop = 0.0F;
            cell28B.BorderWidthBottom = 0.0F;
            cell28B.BorderWidthLeft = 0.0F;
            cell28B.BorderWidthRight = 0.0F;
            cell28B.FixedHeight = 8.0F;


            // -----Separador
            table2A.AddCell(cell20A);
            table2A.AddCell(cell20B);
            table2A.AddCell(cell20C);
            table2A.AddCell(cell20D);

            // ---Nombre o Razon Social
            if (tipdoccli == "RUC")
                table2A.AddCell(cell22AF);
            else
                table2A.AddCell(cell22AB);
            table2A.AddCell(cell22B);

            // ---Documento del Cliente
            if (tipdoccli == "RUC")
                table2A.AddCell(cell23AF);
            else
                table2A.AddCell(cell23AB);
            if (tipdoccli == "RUC")
                table2A.AddCell(cell23BF);
            else
                table2A.AddCell(cell23BB);

            // ---Direccion
            table2A.AddCell(cell24A);
            table2A.AddCell(cell24B);

            // ---Fecha Emision
            table2A.AddCell(cell21A);
            table2A.AddCell(cell21B);

            // ---Moneda
            table2A.AddCell(cell25A);
            table2A.AddCell(cell25B);

            // ---Orden de Compra
            table2A.AddCell(cell26A);
            table2A.AddCell(cell26B);

            // -----Separador
            table2A.AddCell(cell27A);
            table2A.AddCell(cell27B);
            table2A.AddCell(cell28A);
            table2A.AddCell(cell28B);

            // Tamaño de tabla
            table2A.WidthPercentage = 100.0F;
            table2A.HorizontalAlignment = 0;
            doc.Add(table2A);

            int n1, n2, n3, ntot;
            decimal tdci, tcgi, tiscv;
            tdci = (decimal) oDocumento.TotalDsctoItem;
            tcgi = (decimal)oDocumento.TotalCargoItem;
            tiscv = (decimal) oDocumento.Iscventa;

            if (tdci > 0)
                n1 = 1;
            else
                n1 = 0;
            if (tcgi > 0)
                n2 = 1;
            else
                n2 = 0;
            if (tiscv > 0)
                n3 = 1;
            else
                n3 = 0;
            ntot = n1 + n2 + n3 + 8;

            // Tabla para el detalle del documento de venta
            PdfPTable table3 = new PdfPTable(ntot);
            int[] intTblWidth3;
            if (ntot == 11)
                intTblWidth3 = new int[] { (int)Math.Round(4.0), (int)Math.Round(9.0), (int)Math.Round(35.0), (int)Math.Round(4.0), (int)Math.Round(7.0), (int)Math.Round(7.0), (int)Math.Round(6.0), (int)Math.Round(6.0), (int)Math.Round(10.0), (int)Math.Round(6.0), (int)Math.Round(10.0) }; 

            else if (ntot == 10)
            {
                    if (n1 == 1 && n2 == 1)
                        intTblWidth3 = new int[] { (int)Math.Round(4.0), (int)Math.Round(9.0), (int)Math.Round(35.0), (int)Math.Round(4.0), (int)Math.Round(7.0), (int)Math.Round(7.0), (int)Math.Round(6.0), (int)Math.Round(6.0), (int)Math.Round(10.0), (int)Math.Round(10.0) };
                    else
                        intTblWidth3 = new int[] { (int)Math.Round(4.0), (int)Math.Round(9.0), (int)Math.Round(35.0), (int)Math.Round(4.0), (int)Math.Round(7.0), (int)Math.Round(7.0), (int)Math.Round(6.0), (int)Math.Round(10.0), (int)Math.Round(6.0), (int)Math.Round(10.0) };
            }
            else if (ntot == 9)
            {
                    if (n3 == 1)
                    {
                        intTblWidth3 = new int[] { (int)Math.Round(4.0), (int)Math.Round(9.0), (int)Math.Round(35.0), (int)Math.Round(4.0), (int)Math.Round(7.0), (int)Math.Round(7.0), (int)Math.Round(10.0), (int)Math.Round(6.0), (int)Math.Round(10.0) };
                    }                 
                    else
                    {
                        intTblWidth3 = new int[] { (int)Math.Round(4.0), (int)Math.Round(9.0), (int)Math.Round(35.0), (int)Math.Round(4.0), (int)Math.Round(7.0), (int)Math.Round(7.0), (int)Math.Round(6.0), (int)Math.Round(10.0), (int)Math.Round(10.0) }; 
                    }
                    
            }
            else
                     intTblWidth3 = new int[] { (int)Math.Round(4.0), (int)Math.Round(9.0), (int)Math.Round(35.0), (int)Math.Round(4.0), (int)Math.Round(7.0), (int)Math.Round(7.0), (int)Math.Round(10.0), (int)Math.Round(10.0) };
                table3.SetWidths(intTblWidth3);

            Chunk tItem = new Chunk("Item", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tCodigo = new Chunk("Código", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tDescripcion = new Chunk("Descripción", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tUM = new Chunk("Und", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tCantidad = new Chunk("Cantid.", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tVU = new Chunk("V.Unit.", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tDscto = new Chunk("Dscto.", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tCargo = new Chunk("Cargo.", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tValorVenta = new Chunk("Valor.Vta", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tIscItem = new Chunk("ISC", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tTotalItem = new Chunk("Total", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            PdfPCell cell30A = new PdfPCell(new Phrase(tItem));
            cell30A.HorizontalAlignment = 1;
            cell30A.FixedHeight = 18.0F;

            PdfPCell cell31A = new PdfPCell(new Phrase(tCodigo));
            cell31A.HorizontalAlignment = 1;
            cell31A.FixedHeight = 18.0F;

            PdfPCell cell32A = new PdfPCell(new Phrase(tDescripcion));
            cell32A.HorizontalAlignment = 1;
            cell32A.FixedHeight = 18.0F;

            PdfPCell cell33A = new PdfPCell(new Phrase(tUM));
            cell33A.HorizontalAlignment = 1;
            cell33A.FixedHeight = 18.0F;

            PdfPCell cell34A = new PdfPCell(new Phrase(tCantidad));
            cell34A.HorizontalAlignment = 1;
            cell34A.FixedHeight = 18.0F;

            PdfPCell cell35A = new PdfPCell(new Phrase(tVU));
            cell35A.HorizontalAlignment = 1;
            cell35A.FixedHeight = 18.0F;

            PdfPCell cell36A = new PdfPCell(new Phrase(tDscto));
            cell36A.HorizontalAlignment = 1;
            cell36A.FixedHeight = 18.0F;

            PdfPCell cell37A = new PdfPCell(new Phrase(tCargo));
            cell37A.HorizontalAlignment = 1;
            cell37A.FixedHeight = 18.0F;

            PdfPCell cell38A = new PdfPCell(new Phrase(tValorVenta));
            cell38A.HorizontalAlignment = 1;
            cell38A.FixedHeight = 18.0F;

            PdfPCell cell39A = new PdfPCell(new Phrase(tIscItem));
            cell39A.HorizontalAlignment = 1;
            cell39A.FixedHeight = 18.0F;

            PdfPCell cell40A = new PdfPCell(new Phrase(tTotalItem));
            cell40A.HorizontalAlignment = 1;
            cell40A.FixedHeight = 18.0F;

            table3.AddCell(cell30A);
            table3.AddCell(cell31A);
            table3.AddCell(cell32A);
            table3.AddCell(cell33A);
            table3.AddCell(cell34A);
            table3.AddCell(cell35A);
            if (n1 == 1)
                table3.AddCell(cell36A);
            if (n2 == 1)
                table3.AddCell(cell37A);
            table3.AddCell(cell38A);
            if (n3 == 1)
                table3.AddCell(cell39A);
            table3.AddCell(cell40A);

            if (oDetalle.Count > 0)
            {
                foreach (var item in oDetalle.ToList())
                {
                    Chunk itemx = new Chunk(item.Lineid.ToString(), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk Codigo = new Chunk(item.Codigo, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk Descripcion = new Chunk(item.Producto, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk UM = new Chunk(item.Unidad, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                    string[] cant;
                    string cantmostrar;
                    // if (System.Convert.ToString(item.Cantidad).ToString().Replace(",", ".").Contains("."))
                    // {
                    //     cant = Strings.Split(System.Convert.ToString(item.Cantidad), ".");
                    //     if (Strings.Len(cant[1]) > 2)
                    //     {
                    //         // Imprimo mas de dos decimales
                    //         if (Strings.Len(cant[1]) > 3)
                    //             cantmostrar = Strings.Format(item.Cantidad, "##,##0.0000");
                    //         else
                    //             cantmostrar = Strings.Format(item.Cantidad, "##,##0.000");
                    //     }
                    //     else
                    //         // Imprimo dos decimales
                    //         cantmostrar = Strings.Format(item.Cantidad, "##,##0.00");
                    // }
                    // else
                        // Imprimo dos decimales
                        cantmostrar = Strings.Format(item.Cantidad, "##,##0.00");
                    Chunk Cantidad = new Chunk(cantmostrar, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                    string[] valor;
                    string valormostrar;
                    if (item.Valorventa > 0)
                    {
                        // if (System.Convert.ToString(item.Valorunitario).ToString().Replace(",", ".").Contains(".") && item.Valorunitario > 0)
                        // {
                        //     valor = Strings.Split(System.Convert.ToString(item.Valorunitario.ToString().Replace(",", ".")), ".");
                        //     if (Strings.Len(valor[1]) > 2)
                        //     {
                        //         // Imprimo mas de dos decimales
                        //         if (Strings.Len(valor[1]) > 3)
                        //             valormostrar = Strings.Format(item.Valorunitario, "##,##0.0000");
                        //         else
                        //             valormostrar = Strings.Format(item.Valorunitario, "##,##0.000");
                        //     }
                        //     else
                        //         // Imprimo dos decimales
                        //         valormostrar = Strings.Format(item.Valorunitario, "##,##0.00");
                        // }
                        // else
                            // Imprimo dos decimales
                            valormostrar = Strings.Format(item.Valorunitario, "##,##0.00");
                    }
                    else
                        // Gratuito
                        valormostrar = Strings.Format(item.Precioreferencial, "##,##0.00");
                    Chunk VU = new Chunk(valormostrar, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                    Chunk Dscto = new Chunk(Strings.Format(item.Valordscto, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk Cargo = new Chunk(Strings.Format(item.Valorcargo, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk ValorVenta = new Chunk(Strings.Format(item.Valorventa, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk IscItem = new Chunk(Strings.Format(item.Isc, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk TotalItem = new Chunk(Strings.Format(item.Total, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                    PdfPCell cell30B = new PdfPCell(new Phrase(itemx));
                    cell30B.HorizontalAlignment = 1;

                    PdfPCell cell31B = new PdfPCell(new Phrase(Codigo));
                    cell31B.HorizontalAlignment = 1;

                    PdfPCell cell32B = new PdfPCell(new Phrase(Descripcion));
                    cell32B.HorizontalAlignment = 0;

                    PdfPCell cell33B = new PdfPCell(new Phrase(UM));
                    cell33B.HorizontalAlignment = 1;

                    PdfPCell cell34B = new PdfPCell(new Phrase(Cantidad));
                    cell34B.HorizontalAlignment = 2;

                    PdfPCell cell35B = new PdfPCell(new Phrase(VU));
                    cell35B.HorizontalAlignment = 2;

                    PdfPCell cell36B = new PdfPCell(new Phrase(Dscto));
                    cell36B.HorizontalAlignment = 2;

                    PdfPCell cell37B = new PdfPCell(new Phrase(Cargo));
                    cell37B.HorizontalAlignment = 2;

                    PdfPCell cell38B = new PdfPCell(new Phrase(ValorVenta));
                    cell38B.HorizontalAlignment = 2;

                    PdfPCell cell39B = new PdfPCell(new Phrase(IscItem));
                    cell39B.HorizontalAlignment = 2;

                    PdfPCell cell40B = new PdfPCell(new Phrase(TotalItem));
                    cell40B.HorizontalAlignment = 2;

                    table3.AddCell(cell30B);
                    table3.AddCell(cell31B);
                    table3.AddCell(cell32B);
                    table3.AddCell(cell33B);
                    table3.AddCell(cell34B);
                    table3.AddCell(cell35B);
                    if (n1 == 1)
                        table3.AddCell(cell36B);
                    if (n2 == 1)
                        table3.AddCell(cell37B);
                    table3.AddCell(cell38B);
                    if (n3 == 1)
                        table3.AddCell(cell39B);
                    table3.AddCell(cell40B);
                }
            }
            // Tamaño de tabla
            table3.WidthPercentage = 100.0F;
            table3.HorizontalAlignment = 0;
            doc.Add(table3);
            doc.Add(new Paragraph(" "));

            // Tabla para los totales y monto en letras
            PdfPTable table4 = new PdfPTable(5);
                var intTblWidth4 = new int[] { (int)Math.Round(50.0), (int)Math.Round(5.0), (int)Math.Round(20.0), (int)Math.Round(10.0), (int)Math.Round(15.0) };
                table4.SetWidths(intTblWidth4);

            // Monto en letras
            Chunk montoventaletras = new Chunk("SON: " + montoenletras + " " + monedaenletras, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // Para los totales
            // Chunk tDescuento = new Chunk("Descuento(%):", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tDescuento = new Chunk("Descuento:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            // Chunk tCargoGb = new Chunk("Cargo(%):", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tCargoGb = new Chunk("Cargo:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tAfectas = new Chunk("Op. Gravada:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tInafectas = new Chunk("Op. Inafecta:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tExoneradas = new Chunk("Op. Exonerada:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tExportadas = new Chunk("Op. Exportación:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tGratuitas = new Chunk("Op. Gratuita:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tIGV = new Chunk("IGV:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tISC = new Chunk("ISC:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tImporteVenta = new Chunk("Importe Venta:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tImporteAnticipo = new Chunk("Importe Anticipos:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tPercepcion = new Chunk("Monto Percepción:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tImporteFinal = new Chunk("Importe Total:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));

            Chunk simboloMoneda = new Chunk(symbmoneda, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));

            decimal valoropexport;
            if (oDocumento.Estransgratuita == "SI")
                valoropexport = 0;
            else
                valoropexport = (decimal)oDocumento.Valorperexportacion;

            // para calcular descuentos en monto

            decimal totalOperaciones;
            decimal totalImpuestos;
            decimal totalImpGratuitos;
            decimal factorDscto;
            decimal factorCargo;
            decimal totalDescuentos;
            decimal totalCargos;

            totalOperaciones =(decimal) (oDocumento.Valorpergravadas + oDocumento.Valorperinafectas + oDocumento.Valorperexoneradas + oDocumento.Valorperexportacion);
            totalImpuestos = (decimal)(oDocumento.Igvventa + oDocumento.Iscventa + oDocumento.OtrosTributos);
            totalImpGratuitos = (decimal)(oDocumento.SumaIgvGratuito + oDocumento.SumaIscGratuito);

            factorCargo = (decimal)(Math.Round(oDocumento.OtrosCargos / (double)100, 5));
            factorDscto = (decimal)(Math.Round(oDocumento.Descuento / (double)100, 5));
            // factorDscto = (decimal)(Math.Round((decimal)oDocumento.Descuento / (decimal)(totalOperaciones + totalImpuestos), 5));  //add

            totalDescuentos = (totalOperaciones + totalImpuestos) * factorDscto;
            totalCargos = (totalOperaciones + totalImpuestos)* factorCargo;

            // Chunk Descuento = new Chunk(oDocumento.Descuento.ToString() + "%", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Descuento = new Chunk(Strings.Format(totalDescuentos, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));

            // Chunk CargoGb = new Chunk(oDocumento.OtrosCargos.ToString() + "%", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk CargoGb = new Chunk(Strings.Format(totalCargos, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));

            Chunk Afectas = new Chunk(Strings.Format(oDocumento.Valorpergravadas, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Inafectas = new Chunk(Strings.Format(oDocumento.Valorperinafectas, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Exoneradas = new Chunk(Strings.Format(oDocumento.Valorperexoneradas, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Exportadas = new Chunk(Strings.Format(valoropexport, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Gratuitas = new Chunk(Strings.Format(oDocumento.Valorpergratuitas + oDocumento.SumaIgvGratuito + oDocumento.SumaIscGratuito, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk IGV = new Chunk(Strings.Format(oDocumento.Igvventa, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk ISC = new Chunk(Strings.Format(oDocumento.Iscventa, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk ImporteVenta = new Chunk(Strings.Format(oDocumento.Importeventa, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk ImporteAnticipo = new Chunk(Strings.Format(oDocumento.Importeanticipo, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Percepcion = new Chunk(Strings.Format(oDocumento.Importepercep, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk ImporteFinal = new Chunk(Strings.Format(oDocumento.Importefinal, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));

            // Para la leyenda de monto en letras
            PdfPCell cell4ML = new PdfPCell(new Phrase(montoventaletras));
            cell4ML.HorizontalAlignment = 0;
            cell4ML.BorderWidthTop = 0.0F;
            cell4ML.BorderWidthBottom = 0.0F;
            cell4ML.BorderWidthLeft = 0.0F;
            cell4ML.BorderWidthRight = 0.0F;
            cell4ML.FixedHeight = 14.0F;
            cell4ML.Rowspan = 2;

            // Celda Vacia
            PdfPCell cell4MV = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell4MV.HorizontalAlignment = 0;
            cell4MV.BorderWidthTop = 0.0F;
            cell4MV.BorderWidthBottom = 0.0F;
            cell4MV.BorderWidthLeft = 0.0F;
            cell4MV.BorderWidthRight = 0.0F;
            cell4MV.FixedHeight = 14.0F;

            // Para la moneda de los montos
            PdfPCell cell4MM = new PdfPCell(new Phrase(simboloMoneda));
            cell4MM.HorizontalAlignment = 0;
            cell4MM.BorderWidthTop = 0.0F;
            cell4MM.BorderWidthBottom = 0.0F;
            cell4MM.BorderWidthLeft = 0.0F;
            cell4MM.BorderWidthRight = 0.0F;
            cell4MM.FixedHeight = 14.0F;

            // Para Op Gravadas
            PdfPCell cell41A = new PdfPCell(new Phrase(tAfectas));
            cell41A.HorizontalAlignment = 0;
            cell41A.BorderWidthTop = 0.0F;
            cell41A.BorderWidthBottom = 0.0F;
            cell41A.BorderWidthLeft = 0.0F;
            cell41A.BorderWidthRight = 0.0F;
            cell41A.FixedHeight = 14.0F;

            PdfPCell cell41B = new PdfPCell(new Phrase(Afectas));
            cell41B.HorizontalAlignment = 2;
            cell41B.BorderWidthTop = 0.0F;
            cell41B.BorderWidthBottom = 0.0F;
            cell41B.BorderWidthLeft = 0.0F;
            cell41B.BorderWidthRight = 0.0F;
            cell41B.FixedHeight = 14.0F;

            // Para Op Inafectas
            PdfPCell cell42A = new PdfPCell(new Phrase(tInafectas));
            cell42A.HorizontalAlignment = 0;
            cell42A.BorderWidthTop = 0.0F;
            cell42A.BorderWidthBottom = 0.0F;
            cell42A.BorderWidthLeft = 0.0F;
            cell42A.BorderWidthRight = 0.0F;
            cell42A.FixedHeight = 14.0F;

            PdfPCell cell42B = new PdfPCell(new Phrase(Inafectas));
            cell42B.HorizontalAlignment = 2;
            cell42B.BorderWidthTop = 0.0F;
            cell42B.BorderWidthBottom = 0.0F;
            cell42B.BorderWidthLeft = 0.0F;
            cell42B.BorderWidthRight = 0.0F;
            cell42B.FixedHeight = 14.0F;

            // Op Exoneradas
            PdfPCell cell43A = new PdfPCell(new Phrase(tExoneradas));
            cell43A.HorizontalAlignment = 0;
            cell43A.BorderWidthTop = 0.0F;
            cell43A.BorderWidthBottom = 0.0F;
            cell43A.BorderWidthLeft = 0.0F;
            cell43A.BorderWidthRight = 0.0F;
            cell43A.FixedHeight = 14.0F;

            PdfPCell cell43B = new PdfPCell(new Phrase(Exoneradas));
            cell43B.HorizontalAlignment = 2;
            cell43B.BorderWidthTop = 0.0F;
            cell43B.BorderWidthBottom = 0.0F;
            cell43B.BorderWidthLeft = 0.0F;
            cell43B.BorderWidthRight = 0.0F;
            cell43B.FixedHeight = 14.0F;

            // Op Exportacion
            PdfPCell cell44A = new PdfPCell(new Phrase(tExportadas));
            cell44A.HorizontalAlignment = 0;
            cell44A.BorderWidthTop = 0.0F;
            cell44A.BorderWidthBottom = 0.0F;
            cell44A.BorderWidthLeft = 0.0F;
            cell44A.BorderWidthRight = 0.0F;
            cell44A.FixedHeight = 14.0F;

            PdfPCell cell44B = new PdfPCell(new Phrase(Exportadas));
            cell44B.HorizontalAlignment = 2;
            cell44B.BorderWidthTop = 0.0F;
            cell44B.BorderWidthBottom = 0.0F;
            cell44B.BorderWidthLeft = 0.0F;
            cell44B.BorderWidthRight = 0.0F;
            cell44B.FixedHeight = 14.0F;

            // Op Gratuitas
            PdfPCell cell45A = new PdfPCell(new Phrase(tGratuitas));
            cell45A.HorizontalAlignment = 0;
            cell45A.BorderWidthTop = 0.0F;
            cell45A.BorderWidthBottom = 0.0F;
            cell45A.BorderWidthLeft = 0.0F;
            cell45A.BorderWidthRight = 0.0F;
            cell45A.FixedHeight = 14.0F;

            PdfPCell cell45B = new PdfPCell(new Phrase(Gratuitas));
            cell45B.HorizontalAlignment = 2;
            cell45B.BorderWidthTop = 0.0F;
            cell45B.BorderWidthBottom = 0.0F;
            cell45B.BorderWidthLeft = 0.0F;
            cell45B.BorderWidthRight = 0.0F;
            cell45B.FixedHeight = 14.0F;

            // IGV
            PdfPCell cell46A = new PdfPCell(new Phrase(tIGV));
            cell46A.HorizontalAlignment = 0;
            cell46A.BorderWidthTop = 0.0F;
            cell46A.BorderWidthBottom = 0.0F;
            cell46A.BorderWidthLeft = 0.0F;
            cell46A.BorderWidthRight = 0.0F;
            cell46A.FixedHeight = 14.0F;

            PdfPCell cell46B = new PdfPCell(new Phrase(IGV));
            cell46B.HorizontalAlignment = 2;
            cell46B.BorderWidthTop = 0.0F;
            cell46B.BorderWidthBottom = 0.0F;
            cell46B.BorderWidthLeft = 0.0F;
            cell46B.BorderWidthRight = 0.0F;
            cell46B.FixedHeight = 14.0F;

            // ISC
            PdfPCell cell47A = new PdfPCell(new Phrase(tISC));
            cell47A.HorizontalAlignment = 0;
            cell47A.BorderWidthTop = 0.0F;
            cell47A.BorderWidthBottom = 0.0F;
            cell47A.BorderWidthLeft = 0.0F;
            cell47A.BorderWidthRight = 0.0F;
            cell47A.FixedHeight = 14.0F;

            PdfPCell cell47B = new PdfPCell(new Phrase(ISC));
            cell47B.HorizontalAlignment = 2;
            cell47B.BorderWidthTop = 0.0F;
            cell47B.BorderWidthBottom = 0.0F;
            cell47B.BorderWidthLeft = 0.0F;
            cell47B.BorderWidthRight = 0.0F;
            cell47B.FixedHeight = 14.0F;

            // Importe de Venta
            PdfPCell cell48A = new PdfPCell(new Phrase(tImporteVenta));
            cell48A.HorizontalAlignment = 0;
            cell48A.BorderWidthTop = 0.0F;
            cell48A.BorderWidthBottom = 0.0F;
            cell48A.BorderWidthLeft = 0.0F;
            cell48A.BorderWidthRight = 0.0F;
            cell48A.FixedHeight = 14.0F;

            PdfPCell cell48B = new PdfPCell(new Phrase(ImporteVenta));
            cell48B.HorizontalAlignment = 2;
            cell48B.BorderWidthTop = 0.0F;
            cell48B.BorderWidthBottom = 0.0F;
            cell48B.BorderWidthLeft = 0.0F;
            cell48B.BorderWidthRight = 0.0F;
            cell48B.FixedHeight = 14.0F;

            // Descuento
            PdfPCell cell49A = new PdfPCell(new Phrase(tDescuento));
            cell49A.HorizontalAlignment = 0;
            cell49A.BorderWidthTop = 0.0F;
            cell49A.BorderWidthBottom = 0.0F;
            cell49A.BorderWidthLeft = 0.0F;
            cell49A.BorderWidthRight = 0.0F;
            cell49A.FixedHeight = 14.0F;

            PdfPCell cell49B = new PdfPCell(new Phrase(Descuento));
            cell49B.HorizontalAlignment = 2;
            cell49B.BorderWidthTop = 0.0F;
            cell49B.BorderWidthBottom = 0.0F;
            cell49B.BorderWidthLeft = 0.0F;
            cell49B.BorderWidthRight = 0.0F;
            cell49B.FixedHeight = 14.0F;

            // Cargo
            PdfPCell cell50A = new PdfPCell(new Phrase(tCargoGb));
            cell50A.HorizontalAlignment = 0;
            cell50A.BorderWidthTop = 0.0F;
            cell50A.BorderWidthBottom = 0.0F;
            cell50A.BorderWidthLeft = 0.0F;
            cell50A.BorderWidthRight = 0.0F;
            cell50A.FixedHeight = 14.0F;

            PdfPCell cell50B = new PdfPCell(new Phrase(CargoGb));
            cell50B.HorizontalAlignment = 2;
            cell50B.BorderWidthTop = 0.0F;
            cell50B.BorderWidthBottom = 0.0F;
            cell50B.BorderWidthLeft = 0.0F;
            cell50B.BorderWidthRight = 0.0F;
            cell50B.FixedHeight = 14.0F;

            // Anticipos
            PdfPCell cell51A = new PdfPCell(new Phrase(tImporteAnticipo));
            cell51A.HorizontalAlignment = 0;
            cell51A.BorderWidthTop = 0.0F;
            cell51A.BorderWidthBottom = 0.0F;
            cell51A.BorderWidthLeft = 0.0F;
            cell51A.BorderWidthRight = 0.0F;
            cell51A.FixedHeight = 14.0F;

            PdfPCell cell51B = new PdfPCell(new Phrase(ImporteAnticipo));
            cell51B.HorizontalAlignment = 2;
            cell51B.BorderWidthTop = 0.0F;
            cell51B.BorderWidthBottom = 0.0F;
            cell51B.BorderWidthLeft = 0.0F;
            cell51B.BorderWidthRight = 0.0F;
            cell51B.FixedHeight = 14.0F;

            // Importe Final
            PdfPCell cell52A = new PdfPCell(new Phrase(tImporteFinal));
            cell52A.HorizontalAlignment = 0;
            cell52A.BorderWidthTop = 0.0F;
            cell52A.BorderWidthBottom = 0.0F;
            cell52A.BorderWidthLeft = 0.0F;
            cell52A.BorderWidthRight = 0.0F;
            cell52A.FixedHeight = 14.0F;

            PdfPCell cell52B = new PdfPCell(new Phrase(ImporteFinal));
            cell52B.HorizontalAlignment = 2;
            cell52B.BorderWidthTop = 0.0F;
            cell52B.BorderWidthBottom = 0.0F;
            cell52B.BorderWidthLeft = 0.0F;
            cell52B.BorderWidthRight = 0.0F;
            cell52B.FixedHeight = 14.0F;

            // Percepcion
            PdfPCell cell53A = new PdfPCell(new Phrase(tPercepcion));
            cell53A.HorizontalAlignment = 0;
            cell53A.BorderWidthTop = 0.0F;
            cell53A.BorderWidthBottom = 0.0F;
            cell53A.BorderWidthLeft = 0.0F;
            cell53A.BorderWidthRight = 0.0F;
            cell53A.FixedHeight = 14.0F;

            PdfPCell cell53B = new PdfPCell(new Phrase(Percepcion));
            cell53B.HorizontalAlignment = 2;
            cell53B.BorderWidthTop = 0.0F;
            cell53B.BorderWidthBottom = 0.0F;
            cell53B.BorderWidthLeft = 0.0F;
            cell53B.BorderWidthRight = 0.0F;
            cell53B.FixedHeight = 14.0F;

            // Agregamos los datos en la tabla

            // Op Gravadas
            table4.AddCell(cell4ML);
            table4.AddCell(cell4MV);
            table4.AddCell(cell41A);
            table4.AddCell(cell4MM);
            table4.AddCell(cell41B);

            // Op Inafectas
            // table4.AddCell(cell4MV)
            table4.AddCell(cell4MV);
            table4.AddCell(cell42A);
            table4.AddCell(cell4MM);
            table4.AddCell(cell42B);

            // Op Exoneradas
            if (oDocumento.Valorperexoneradas > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell43A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell43B);
            }

            // Op Exportacion 
            if (oDocumento.Valorperexportacion > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell44A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell44B);
            }

            // Op Gratuitas 
            if (oDocumento.Valorpergratuitas > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell45A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell45B);
            }

            // IGV
            table4.AddCell(cell4MV);
            table4.AddCell(cell4MV);
            table4.AddCell(cell46A);
            table4.AddCell(cell4MM);
            table4.AddCell(cell46B);

            // ISC
            if (oDocumento.Iscventa > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell47A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell47B);
            }

            // Importe Venta
            table4.AddCell(cell4MV);
            table4.AddCell(cell4MV);
            table4.AddCell(cell48A);
            table4.AddCell(cell4MM);
            table4.AddCell(cell48B);

            // Descuento
            if (oDocumento.Descuento > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell49A);
                // table4.AddCell(cell4MV); // celda Vacia
                table4.AddCell(cell4MM);
                table4.AddCell(cell49B);
            }

            // Cargo
            if (oDocumento.OtrosCargos > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell50A);
                // table4.AddCell(cell4MV); //celda vacia
                table4.AddCell(cell4MM);
                table4.AddCell(cell50B);
            }

            // Anticipos
            if (oDocumento.Importeanticipo > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell51A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell51B);
            }

            // Importe Final
            table4.AddCell(cell4MV);
            table4.AddCell(cell4MV);
            table4.AddCell(cell52A);
            table4.AddCell(cell4MM);
            table4.AddCell(cell52B);

            // Percepciones
            if (oDocumento.Importepercep > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell53A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell53B);
            }

            // Tamaño de tabla
            table4.WidthPercentage = 100.0F;
            table4.HorizontalAlignment = 1;
            doc.Add(table4);

            // Texto Observacion SUNAT - Solo Facturas
            // if (oDocumento.Serie.Substring(0, 1) == "F")
            // {
            //     string textoSunat = "Observaciones de SUNAT";
            //     Chunk tTextoSunat = new Chunk(textoSunat, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            //     // Texto Respuesta SUNAT
            //     string textoCDR = EN_Constante.g_const_vacio;
            //     if (situacion == "P" || situacion == EN_Constante.g_const_vacio)
            //         textoCDR = "El comprobante esta pendiente de envío a SUNAT.";
            //     else if (situacion == "A")
            //         textoCDR = mensaje;
            //     else if (situacion == "R" || situacion == "E")
            //         textoCDR = mensaje;
            //     else if (oDocumento.Situacion.Trim() == "B")
            //         textoCDR = "El comprobante ha sido de baja.";
            //     else
            //         textoCDR = "-";
            //     Chunk tTextoCDR = new Chunk(textoCDR, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            //     PdfPTable table4A = new PdfPTable(1);
            //         var intTblWidth4A = new int[] { (int)Math.Round(90.0) };
            //         table4A.SetWidths(intTblWidth4A);

            //     PdfPCell cell4A1 = new PdfPCell(new Phrase(tTextoSunat));
            //     cell4A1.HorizontalAlignment = 0;
            //     cell4A1.VerticalAlignment = 1;
            //     cell4A1.BorderWidthTop = 0.0F;
            //     cell4A1.BorderWidthBottom = 0.0F;
            //     cell4A1.BorderWidthLeft = 0.0F;
            //     cell4A1.BorderWidthRight = 0.0F;
            //     cell4A1.FixedHeight = 14.0F;

            //     PdfPCell cell4A2 = new PdfPCell(new Phrase(tTextoCDR));
            //     cell4A2.HorizontalAlignment = 0;
            //     cell4A2.VerticalAlignment = 1;
            //     cell4A2.BorderWidthTop = 0.0F;
            //     cell4A2.BorderWidthBottom = 0.0F;
            //     cell4A2.BorderWidthLeft = 0.0F;
            //     cell4A2.BorderWidthRight = 0.0F;
            //     cell4A2.FixedHeight = 14.0F;

            //     table4A.AddCell(cell4A1);
            //     table4A.AddCell(cell4A2);

            //     table4A.WidthPercentage = 100.0F;
            //     table4A.HorizontalAlignment = 0;

            //     doc.Add(table4A);
            // }

            // Para los montos con detraccion
            if (oDocumento.Porcentdetrac > 0 && oDocumento.Importedetrac > 0)
            {
                PdfPTable table5 = new PdfPTable(6);
                    var intTblWidth5 = new int[] { (int)Math.Round(15.0), (int)Math.Round(5.0), (int)Math.Round(20.0), (int)Math.Round(10.0), (int)Math.Round(40.0), (int)Math.Round(10.0) };
                    table5.SetWidths(intTblWidth5);

                string valortitulodetrac;
                valortitulodetrac = "Detracción en Moneda Soles:";

                Chunk tTituloDetr = new Chunk(valortitulodetrac, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
                Chunk tPorcDetr = new Chunk("% Detracción:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
                Chunk tMontoDetr = new Chunk("Monto Detracción:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
                Chunk tMontoDetrRef = new Chunk("Monto Detracción Referencial:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
                Chunk tDescripDetr = new Chunk("Descripción:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));

                Chunk PorcDetr = new Chunk(Strings.Format(oDocumento.Porcentdetrac, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));
                Chunk MontoDetr = new Chunk(Strings.Format(oDocumento.Importedetrac, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));
                Chunk MontoDetrRef = new Chunk(Strings.Format(oDocumento.Importerefdetrac, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));
                Chunk DescripDetr = new Chunk(oDocumento.CodigoBBSSSujetoDetrac.Trim() + " - " + oDocumento.descripciondetrac, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));

                // Titulo
                PdfPCell cell60 = new PdfPCell(new Phrase(tTituloDetr));
                cell60.HorizontalAlignment = 0;
                cell60.BorderWidthTop = 0.0F;
                cell60.BorderWidthBottom = 0.0F;
                cell60.BorderWidthLeft = 0.0F;
                cell60.BorderWidthRight = 0.0F;
                cell60.Colspan = 6;
                cell60.FixedHeight = 14.0F;

                PdfPCell cell61 = new PdfPCell(new Phrase(tPorcDetr));
                cell61.HorizontalAlignment = 0;
                cell61.BorderWidthBottom = 0.0F;
                cell61.BorderWidthRight = 0.0F;
                cell61.FixedHeight = 14.0F;

                PdfPCell cell62 = new PdfPCell(new Phrase(PorcDetr));
                cell62.HorizontalAlignment = 1;
                cell62.BorderWidthBottom = 0.0F;
                cell62.BorderWidthLeft = 0.0F;
                cell62.BorderWidthRight = 0.0F;
                cell62.FixedHeight = 14.0F;

                PdfPCell cell63 = new PdfPCell(new Phrase(tMontoDetr));
                cell63.HorizontalAlignment = 0;
                cell63.BorderWidthBottom = 0.0F;
                cell63.BorderWidthLeft = 0.0F;
                cell63.BorderWidthRight = 0.0F;
                cell63.FixedHeight = 14.0F;

                PdfPCell cell64 = new PdfPCell(new Phrase(MontoDetr));
                cell64.HorizontalAlignment = 1;
                cell64.BorderWidthBottom = 0.0F;
                cell64.BorderWidthLeft = 0.0F;
                cell64.BorderWidthRight = 0.0F;
                cell64.FixedHeight = 14.0F;

                PdfPCell cell65 = new PdfPCell(new Phrase(tMontoDetrRef));
                cell65.HorizontalAlignment = 0;
                cell65.BorderWidthBottom = 0.0F;
                cell65.BorderWidthLeft = 0.0F;
                cell65.BorderWidthRight = 0.0F;
                cell65.FixedHeight = 14.0F;

                PdfPCell cell66 = new PdfPCell(new Phrase(MontoDetrRef));
                cell66.HorizontalAlignment = 1;
                cell66.BorderWidthBottom = 0.0F;
                cell66.BorderWidthLeft = 0.0F;
                cell66.FixedHeight = 14.0F;

                PdfPCell cell67 = new PdfPCell(new Phrase(tDescripDetr));
                cell67.HorizontalAlignment = 0;
                cell67.BorderWidthTop = 0.0F;
                cell67.BorderWidthRight = 0.0F;
                cell67.FixedHeight = 14.0F;

                PdfPCell cell68 = new PdfPCell(new Phrase(DescripDetr));
                cell68.HorizontalAlignment = 0;
                cell68.BorderWidthTop = 0.0F;
                cell68.BorderWidthLeft = 0.0F;
                cell68.FixedHeight = 14.0F;
                cell68.Colspan = 5;

                table5.AddCell(cell60);
                table5.AddCell(cell61);
                table5.AddCell(cell62);
                table5.AddCell(cell63);
                table5.AddCell(cell64);
                table5.AddCell(cell65);
                table5.AddCell(cell66);
                table5.AddCell(cell67);
                table5.AddCell(cell68);

                // Tamaño de tabla
                table5.WidthPercentage = 100.0F;
                table5.HorizontalAlignment = 0;
                doc.Add(table5);
            }

            // Para la informaciòn adicional
            PdfPTable table6 = new PdfPTable(2);
                var intTblWidth6 = new int[] { (int)Math.Round(20.0), (int)Math.Round(60.0) };
                table6.SetWidths(intTblWidth6);

            Chunk tTituloAdic = new Chunk("Información Adicional:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            // strGuias
            // Guias Referencias
            Chunk tReferencia = new Chunk("Documento Referencia:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk Referencia = new Chunk("Guía(s): " + strGuias, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // oDocumento.text_aux_01
            // 9024 - Codigo Cliente y Nro Pedido
            Chunk tCodCliente = new Chunk("Código Cliente:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk CodCliente = new Chunk(oDocumento.text_aux_01, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // oDocumento.text_aux_02
            // 9020 - Vencimiento y Forma de Pago
            Chunk tVencimiento = new Chunk("Vencimiento:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk Vencimiento = new Chunk(oDocumento.text_aux_02, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // oDocumento.text_aux_03
            // 9096 - Tipo de Cambio
            Chunk tMsjTC = new Chunk("Tipo Cambio:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk MsjTC = new Chunk(oDocumento.text_aux_03, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // oDocumento.text_aux_04
            // 9125 - Msg Agente retencion IGV
            Chunk tMsjRet = new Chunk("Mensaje:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk MsjRet = new Chunk(oDocumento.text_aux_04, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // 1002 - Mensaje Donacion o MSVC
            Chunk tMsjDon = new Chunk("Mensaje:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk MsjDon = new Chunk(oDocumento.text_aux_06, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // 9138 - Mensaje Sin Valor Comercial - Valor Referencial
            Chunk tMsjSinVC = new Chunk("Mensaje:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk MsjSinVC = new Chunk(oDocumento.text_aux_07, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // 9404 - Mensaje Flete 
            Chunk tMsjFlet = new Chunk("Mensaje:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk MsjFlet = new Chunk(oDocumento.text_aux_08, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            PdfPCell cell70 = new PdfPCell(new Phrase(tTituloAdic));
            cell70.HorizontalAlignment = 0;
            cell70.BorderWidthTop = 0.0F;
            cell70.BorderWidthBottom = 0.0F;
            cell70.BorderWidthLeft = 0.0F;
            cell70.BorderWidthRight = 0.0F;
            cell70.Colspan = 2;
            cell70.FixedHeight = 14.0F;

            PdfPCell cell71 = new PdfPCell(new Phrase(tReferencia));
            cell71.HorizontalAlignment = 0;
            PdfPCell cell72 = new PdfPCell(new Phrase(Referencia));
            cell72.HorizontalAlignment = 0;

            PdfPCell cell73 = new PdfPCell(new Phrase(tCodCliente));
            cell73.HorizontalAlignment = 0;
            cell73.FixedHeight = 14.0F;
            PdfPCell cell74 = new PdfPCell(new Phrase(CodCliente));
            cell74.HorizontalAlignment = 0;
            cell74.FixedHeight = 14.0F;

            PdfPCell cell75 = new PdfPCell(new Phrase(tVencimiento));
            cell75.HorizontalAlignment = 0;
            cell75.FixedHeight = 14.0F;
            PdfPCell cell76 = new PdfPCell(new Phrase(Vencimiento));
            cell76.HorizontalAlignment = 0;
            cell76.FixedHeight = 14.0F;

            PdfPCell cell77 = new PdfPCell(new Phrase(tMsjTC));
            cell77.HorizontalAlignment = 0;
            PdfPCell cell78 = new PdfPCell(new Phrase(MsjTC));
            cell78.HorizontalAlignment = 0;

            PdfPCell cell79 = new PdfPCell(new Phrase(tMsjRet));
            cell79.HorizontalAlignment = 0;
            PdfPCell cell7A = new PdfPCell(new Phrase(MsjRet));
            cell7A.HorizontalAlignment = 0;

            PdfPCell cell7B = new PdfPCell(new Phrase(tMsjDon));
            cell7B.HorizontalAlignment = 0;
            PdfPCell cell7C = new PdfPCell(new Phrase(MsjDon));
            cell7C.HorizontalAlignment = 0;

            PdfPCell cell7D = new PdfPCell(new Phrase(tMsjSinVC));
            cell7D.HorizontalAlignment = 0;
            PdfPCell cell7E = new PdfPCell(new Phrase(MsjSinVC));
            cell7E.HorizontalAlignment = 0;

            PdfPCell cell7F = new PdfPCell(new Phrase(tMsjFlet));
            cell7F.HorizontalAlignment = 0;
            PdfPCell cell7G = new PdfPCell(new Phrase(MsjFlet));
            cell7G.HorizontalAlignment = 0;

            table6.AddCell(cell70);

            if (strGuias.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell71);
                table6.AddCell(cell72);
            }

            if (oDocumento.text_aux_01.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell73);
                table6.AddCell(cell74);
            }

            if (oDocumento.text_aux_02.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell75);
                table6.AddCell(cell76);
            }

            if (oDocumento.text_aux_03.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell77);
                table6.AddCell(cell78);
            }

            if (oDocumento.text_aux_04.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell79);
                table6.AddCell(cell7A);
            }

            if (oDocumento.text_aux_06.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell7B);
                table6.AddCell(cell7C);
            }

            if (oDocumento.text_aux_07.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell7D);
                table6.AddCell(cell7E);
            }

            if (oDocumento.text_aux_08.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell7F);
                table6.AddCell(cell7G);
            }


            // forma de pago

       
            Chunk formaPago = new Chunk("Forma de Pago:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk RefFormaPAgo = new Chunk(oDocumento.Formapago, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            PdfPCell cell7FP = new PdfPCell(new Phrase(formaPago));
            cell7F.HorizontalAlignment = 0;
            PdfPCell cell7GFP = new PdfPCell(new Phrase(RefFormaPAgo));
            cell7G.HorizontalAlignment = 0;

            if(oDocumento.Idtipodocumento== EN_ConfigConstantes.Instance.const_IdFC ){

                if (oDocumento.Formapago.Trim() != EN_Constante.g_const_vacio)
                {
                    table6.AddCell(cell7FP);
                    table6.AddCell(cell7GFP);

                    if(oDocumento.Formapago != EN_Constante.g_const_formaPago_contado)
                    {
                        List<EN_FormaPagoCredito> jsonCredigo=  (List<EN_FormaPagoCredito>)JsonConvert.DeserializeObject(oDocumento.jsonpagocredito,typeof( List<EN_FormaPagoCredito>));
                        foreach (var item in jsonCredigo)
                        {
                            Chunk desFPcre = new Chunk(item.NumeroCuota +" - "+ item.FechaCuota, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
                            Chunk RefdesFPcre = new Chunk("Monto: "+item.MontoCuota, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                            PdfPCell cell7desFPcre = new PdfPCell(new Phrase(desFPcre));
                            cell7F.HorizontalAlignment = 0;
                            PdfPCell cell7RefdesFPcre = new PdfPCell(new Phrase(RefdesFPcre));
                            cell7G.HorizontalAlignment = 0;

                            table6.AddCell(cell7desFPcre);
                            table6.AddCell(cell7RefdesFPcre);


                        }
                    }
                }   
            }

            
           


            
            //end formad e apgo

            // Tamaño de tabla
            table6.WidthPercentage = 80.0F;
            table6.HorizontalAlignment = 0;

            // Para las Leyendas
            PdfPTable table7 = new PdfPTable(2);
            var intTblWidth7 = new int[] { (int)Math.Round(20.0), (int)Math.Round(60.0) };
             table7.SetWidths(intTblWidth7);

            Chunk tTituloLeyendas = new Chunk("Leyendas:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));


            //Leyenda para IVAP oDocumento.text_aux_05



            // oDocumento.text_aux_05
            // Leyenda
            Chunk tLeyenda = new Chunk("Leyenda:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk Leyenda = new Chunk(oDocumento.text_aux_05.Trim(), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // oDocumento.CodigoBBSSSujetoDetrac
            // Codigo Sujeto a Detraccion
            Chunk tLeyDetrac1 = new Chunk("Detracciones: Código BB y SS sujeto a detracción:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk LeyDetrac1 = new Chunk(oDocumento.CodigoBBSSSujetoDetrac.Trim(), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // oDocumento.NumCtaBcoNacion
            // Nro Cuenta en el BN
            Chunk tLeyDetrac2 = new Chunk("Detracciones: Número de cta en el BN:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk LeyDetrac2 = new Chunk(oDocumento.NumCtaBcoNacion.Trim(), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // oDocumento.DescRegimenpercep
            // 'Nro Registro en el MTC
            Chunk tLeyPercep = new Chunk("Operación con Percepción:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk LeyPercep = new Chunk(oDocumento.Regimenpercep.ToString() + "-" + oDocumento.DescRegimenpercep.Trim() + "(" + oDocumento.Porcentpercep.ToString() + "%)", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // oDocumento.Estransgratuita
            // Transferencia Gratuita
            Chunk tLGratuito = new Chunk(EN_Constante.g_const_vacio, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk LGratuito = new Chunk("TRANSFERENCIA GRATUITA DE UN BIEN Y/O SERVICIO PRESTADO GRATUITAMENTE", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // oDocumento.IndicaAnticipo
            Chunk tAnticipos = new Chunk("Documentos Anticipados:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk LAnticipos = new Chunk(strAnticipos, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            PdfPCell cell80 = new PdfPCell(new Phrase(tTituloLeyendas));
            cell80.HorizontalAlignment = 0;
            cell80.BorderWidthTop = 0.0F;
            cell80.BorderWidthBottom = 0.0F;
            cell80.BorderWidthLeft = 0.0F;
            cell80.BorderWidthRight = 0.0F;
            cell80.Colspan = 2;
            cell80.FixedHeight = 14.0F;

            PdfPCell cell81 = new PdfPCell(new Phrase(tLeyenda));
            cell81.HorizontalAlignment = 0;
            // cell81.FixedHeight = 14.0F

            PdfPCell cell82 = new PdfPCell(new Phrase(Leyenda));
            cell82.HorizontalAlignment = 0;
            // cell82.FixedHeight = 14.0F

            PdfPCell cell83 = new PdfPCell(new Phrase(tLeyDetrac1));
            cell83.HorizontalAlignment = 0;
            // cell83.FixedHeight = 14.0F

            PdfPCell cell84 = new PdfPCell(new Phrase(LeyDetrac1));
            cell84.HorizontalAlignment = 0;
            // cell84.FixedHeight = 14.0F

            PdfPCell cell85 = new PdfPCell(new Phrase(tLeyDetrac2));
            cell85.HorizontalAlignment = 0;
            // cell85.FixedHeight = 14.0F

            PdfPCell cell86 = new PdfPCell(new Phrase(LeyDetrac2));
            cell86.HorizontalAlignment = 0;
            // cell86.FixedHeight = 14.0F

            PdfPCell cell87 = new PdfPCell(new Phrase(tLeyPercep));
            cell87.HorizontalAlignment = 0;
            // cell87.FixedHeight = 14.0F

            PdfPCell cell88 = new PdfPCell(new Phrase(LeyPercep));
            cell88.HorizontalAlignment = 0;
            // cell88.FixedHeight = 14.0F

            PdfPCell cell89 = new PdfPCell(new Phrase(tLGratuito));
            cell89.HorizontalAlignment = 0;
            // cell89.FixedHeight = 14.0F

            PdfPCell cell8A = new PdfPCell(new Phrase(LGratuito));
            cell8A.HorizontalAlignment = 0;
            // cell8A.FixedHeight = 14.0F

            PdfPCell cell8B = new PdfPCell(new Phrase(tAnticipos));
            cell8B.HorizontalAlignment = 0;
            // cell8B.FixedHeight = 14.0F

            PdfPCell cell8C = new PdfPCell(new Phrase(LAnticipos));
            cell8C.HorizontalAlignment = 0;
            // cell8C.FixedHeight = 14.0F

            table7.AddCell(cell80);

            if (oDocumento.text_aux_05.Trim() != EN_Constante.g_const_vacio)
            {
                table7.AddCell(cell81);
                table7.AddCell(cell82);
            }
            if (oDocumento.CodigoBBSSSujetoDetrac.Trim() != EN_Constante.g_const_vacio)
            {
                table7.AddCell(cell83);
                table7.AddCell(cell84);
            }

            if (oDocumento.NumCtaBcoNacion.Trim() != EN_Constante.g_const_vacio)
            {
                table7.AddCell(cell85);
                table7.AddCell(cell86);
            }

            if (oDocumento.Regimenpercep.Trim() != EN_Constante.g_const_vacio)
            {
                table7.AddCell(cell87);
                table7.AddCell(cell88);
            }

            if (oDocumento.Estransgratuita.Trim() == "SI")
            {
                table7.AddCell(cell89);
                table7.AddCell(cell8A);
            }

            if (oDocumento.IndicaAnticipo.Trim() == "1")
            {
                table7.AddCell(cell8B);
                table7.AddCell(cell8C);
            }

            // Tamaño de tabla
            table7.WidthPercentage = 80.0F;
            table7.HorizontalAlignment = 0;

            // Para codigo QR
            PdfPTable table8 = new PdfPTable(1);
                var intTblWidth8 = new int[] { (int)Math.Round(20.0) };
                table8.SetWidths(intTblWidth8);

            PdfPCell cell90 = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell90.HorizontalAlignment = 2;
            cell90.VerticalAlignment = 1;
            cell90.BorderWidthTop = 0.0F;
            cell90.BorderWidthBottom = 0.0F;
            cell90.BorderWidthLeft = 0.0F;
            cell90.BorderWidthRight = 0.0F;
            cell90.FixedHeight = 14.0F;

            // generarCodigoQR de codigo qr

            PdfPCell cell91 = new PdfPCell(pdfImage);
            cell91.HorizontalAlignment = 2;
            cell91.VerticalAlignment = 1;
            cell91.BorderWidthTop = 0.0F;
            cell91.BorderWidthBottom = 0.0F;
            cell91.BorderWidthLeft = 0.0F;
            cell91.BorderWidthRight = 0.0F;

            table8.AddCell(cell90);
            table8.AddCell(cell91);
            table8.WidthPercentage = 20.0F;
            table8.HorizontalAlignment = 2;

            var outerTablex = new PdfPTable(2);
                var outTblWidthx = new int[] { (int)Math.Round(80.0), (int)Math.Round(20.0) };
                outerTablex.SetWidths(outTblWidthx);

            PdfPCell cx1 = new PdfPCell(table6);
            PdfPCell cx2 = new PdfPCell(table8);
            PdfPCell cx3 = new PdfPCell(table7);

            cx1.BorderWidthBottom = 0.0F;
            cx1.BorderWidthTop = 0.0F;
            cx1.BorderWidthLeft = 0.0F;
            cx1.BorderWidthRight = 0.0F;

            cx2.BorderWidthBottom = 0.0F;
            cx2.BorderWidthTop = 0.0F;
            cx2.BorderWidthLeft = 0.0F;
            cx2.BorderWidthRight = 0.0F;
            cx2.Rowspan = 2;

            cx3.BorderWidthBottom = 0.0F;
            cx3.BorderWidthTop = 0.0F;
            cx3.BorderWidthLeft = 0.0F;
            cx3.BorderWidthRight = 0.0F;

            cx1.HorizontalAlignment = 0;
            cx2.HorizontalAlignment = 2;
            cx3.HorizontalAlignment = 0;

            outerTablex.AddCell(cx1);
            outerTablex.AddCell(cx2);
            outerTablex.AddCell(cx3);

            outerTablex.WidthPercentage = 100.0F;
            outerTablex.HorizontalAlignment = 0;

            doc.Add(outerTablex);

            // Texto de Autorización de SUNAT
            string textoautorizac = EN_Constante.g_const_vacio;
            if (oEmpresa.NroResolucion.Trim() != EN_Constante.g_const_vacio)
                textoautorizac = "Autorizado a ser emisor electrónico mediante R.I. SUNAT N° " + oEmpresa.NroResolucion;

            Chunk tTextoautorizac = new Chunk(textoautorizac, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // Texto de Leyenda de Representación impresa
            string textoleyenda = EN_Constante.g_const_vacio;
            string textolink = ", consulte en " + oEmpresa.Web + ".";
            if (oDocumento.Idtipodocumento.Trim() == oCompSunat.IdFC)
                textoleyenda = "Representación Impresa de la Factura Electrónica.";
            else if (oDocumento.Idtipodocumento.Trim() == oCompSunat.IdBV)
                textoleyenda = "Representación Impresa de la Boleta de Venta Electrónica.";
            if (oEmpresa.Web.Trim() == EN_Constante.g_const_vacio)
                textoleyenda = textoleyenda + " Código Hash: " + valorResumen;
            else
                textoleyenda = textoleyenda + textolink + " Código Hash: " + valorResumen;

            Chunk tTextoleyenda = new Chunk(textoleyenda, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.ITALIC));

            PdfPTable table9 = new PdfPTable(1);
                var intTblWidth9 = new int[] { (int)Math.Round(90.0) };
                table9.SetWidths(intTblWidth9);

            PdfPCell cell98 = new PdfPCell(new Phrase(tTextoautorizac));
            cell98.HorizontalAlignment = 0;
            cell98.VerticalAlignment = 1;
            cell98.BorderWidthTop = 0.0F;
            cell98.BorderWidthBottom = 0.0F;
            cell98.BorderWidthLeft = 0.0F;
            cell98.BorderWidthRight = 0.0F;
            // cell98.FixedHeight = 14.0F

            PdfPCell cell99 = new PdfPCell(new Phrase(tTextoleyenda));
            cell99.HorizontalAlignment = 0;
            cell99.VerticalAlignment = 1;
            cell99.BorderWidthTop = 0.0F;
            cell99.BorderWidthBottom = 0.0F;
            cell99.BorderWidthLeft = 0.0F;
            cell99.BorderWidthRight = 0.0F;
            // cell99.FixedHeight = 14.0F
            if (textoautorizac.Trim() != EN_Constante.g_const_vacio)
                table9.AddCell(cell98);
            table9.AddCell(cell99);

            table9.WidthPercentage = 100.0F;
            table9.HorizontalAlignment = 0;

            doc.Add(table9);

            doc.Dispose();
            doc.Close();
            file.Dispose();
            file.Close();


            //COPIAMOS ARCHIVO A S3 Y ELIMINAMOS DE TEMPORAL
            string nombrePDF= oEmpresa.Nrodocumento.Trim() + "-" + oDocumento.Idtipodocumento.Trim() + "-" + oDocumento.Serie.Trim() + "-" + oDocumento.Numero.Trim();
            AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oDocumento.NroDocEmpresa,rutabase,EN_Constante.g_const_rutaSufijo_pdf,nombrePDF,EN_Constante.g_const_extension_pdf);




        }
    }
    public void GenerarPDFNotaCreditoDebito(EN_Empresa oEmpresa, EN_Documento oDocumento, string montoenletras, string monedaenletras, List<EN_DetalleDocumento> oDetalle, EN_ComprobanteSunat oCompSunat, string valorResumen, string firma, string rutabase, string ruta, string symbmoneda, List<EN_DocReferencia> oDetalleNCNDDocRefencia, string situacion = EN_Constante.g_const_vacio, string mensaje = EN_Constante.g_const_vacio)
    {
        //DESCRIPCION: FUNCION PARA GENERAR PDF DE NOTAS

        string rutalogo = string.Format("{0}/{1}/", rutabase, oDocumento.NroDocEmpresa);
        Document doc = new Document(PageSize.A4, 20.0F, 20.0F, 20.0F, 20.0F);
        string filename = ruta + oEmpresa.Nrodocumento.Trim() + "-" + oDocumento.Idtipodocumento.Trim() + "-" + oDocumento.Serie.Trim() + "-" + oDocumento.Numero.Trim() +  EN_Constante.g_const_extension_pdf;

        string logo = rutalogo + oEmpresa.Imagen;

        if (filename.Trim() != EN_Constante.g_const_vacio)
        {
            FileStream file = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            PdfWriter.GetInstance(doc, file);
            doc.Open();
            
            // Para el logo
            dynamic imagen =null;
            if (File.Exists(logo))
            {

                imagen = iTextSharp.text.Image.GetInstance(logo);
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_LEFT;
                imagen.ScaleAbsolute(100, 100);

            }
            else
            {
                imagen=new Phrase(EN_Constante.g_const_vacio);
            }
            

            // Para el codigo QR
          

            string valorCodigoBarras;
            valorCodigoBarras = oEmpresa.Nrodocumento.Trim() + "|" + oDocumento.Idtipodocumento.Trim() + "|" + oDocumento.Serie.Trim() + "|" + oDocumento.Numero.Trim() + "|" + oDocumento.Igvventa + "|" + oDocumento.Importeventa + "|" + String.Format(oDocumento.Fecha, "dd/MM/yyyy") + "|" + ((oDocumento.Tipodoccliente.Trim() == EN_Constante.g_const_vacio)? "0": oDocumento.Tipodoccliente) + "|" + ((oDocumento.Nrodoccliente.Trim() == EN_Constante.g_const_vacio)? "-": oDocumento.Nrodoccliente);
            BarcodeQRCode barcodeQRCode = new BarcodeQRCode(valorCodigoBarras, 1000,1000, null);
            iTextSharp.text.Image pdfImage= barcodeQRCode.GetImage();
            pdfImage.ScaleAbsolute(70, 70);
            
            pdfImage.Alignment = 2;
            // Fin Codigo QR

            // Tabla para Tipo de documento - RUC - Serie y Numero de documento
            PdfPTable table1 = new PdfPTable(1);
            string textoTipoDoc = EN_Constante.g_const_vacio;
            string textoTipoDoc2 = EN_Constante.g_const_vacio;
            if (oDocumento.Idtipodocumento.Trim() == oCompSunat.IdNC)
            {
                textoTipoDoc = "NOTA DE CREDITO";
                textoTipoDoc2 = "ELECTRÓNICA";
            }
            else if (oDocumento.Idtipodocumento.Trim() == oCompSunat.IdND)
            {
                textoTipoDoc = "NOTA DE DÉBITO";
                textoTipoDoc2 = "ELECTRÓNICA";
            }


            Chunk ruc = new Chunk("RUC N° " + oEmpresa.Nrodocumento, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            Chunk tipodocumento = new Chunk(textoTipoDoc, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            Chunk tipodocumento2 = new Chunk(textoTipoDoc2, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            Chunk nrodocumento = new Chunk(oDocumento.Serie + "-" + oDocumento.Numero, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            // Alineacion 0=Left, 1=Centre, 2=Right
            PdfPCell cell12 = new PdfPCell(new Phrase(ruc));
            cell12.HorizontalAlignment = 1;
            cell12.BorderWidthBottom = 0.0F;
            cell12.FixedHeight = 18.0F;
            PdfPCell cell11 = new PdfPCell(new Phrase(tipodocumento));
            cell11.HorizontalAlignment = 1;
            cell11.BorderWidthBottom = 0.0F;
            cell11.BorderWidthTop = 0.0F;
            cell11.FixedHeight = 18.0F;
            PdfPCell cell14 = new PdfPCell(new Phrase(tipodocumento2));
            cell14.HorizontalAlignment = 1;
            cell14.BorderWidthBottom = 0.0F;
            cell14.BorderWidthTop = 0.0F;
            cell14.FixedHeight = 18.0F;
            PdfPCell cell13 = new PdfPCell(new Phrase(nrodocumento));
            cell13.HorizontalAlignment = 1;
            cell13.BorderWidthTop = 0.0F;
            cell13.FixedHeight = 18.0F;

            table1.AddCell(cell12);
            table1.AddCell(cell11);
            table1.AddCell(cell14);
            table1.AddCell(cell13);
            // Tamaño de tabla
            table1.WidthPercentage = 35.0F;
            table1.HorizontalAlignment = 2;

            var outerTable = new PdfPTable(2);
                var outTblWidth2 = new int[] { (int)Math.Round(65.0), (int)Math.Round(35.0) };
                outerTable.SetWidths(outTblWidth2);
            PdfPCell c1 = new PdfPCell(imagen);
            PdfPCell c2 = new PdfPCell(table1);
            c1.BorderWidthBottom = 0.0F;
            c1.BorderWidthTop = 0.0F;
            c1.BorderWidthLeft = 0.0F;
            c1.BorderWidthRight = 0.0F;
            c2.BorderWidthBottom = 0.0F;
            c2.BorderWidthTop = 0.0F;
            c2.BorderWidthLeft = 0.0F;
            c2.BorderWidthRight = 0.0F;
            c1.HorizontalAlignment = 0;
            c2.HorizontalAlignment = 2;
            outerTable.AddCell(c1);
            outerTable.AddCell(c2);
            outerTable.WidthPercentage = 100.0F;
            outerTable.HorizontalAlignment = 0;
            doc.Add(outerTable);

            // Mostramos los datos de la empresa
            Chunk razonsocial = new Chunk(oEmpresa.RazonSocial, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
            Chunk nombrecomercial = new Chunk(oEmpresa.Nomcomercial, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));
            Chunk direccion = new Chunk(oEmpresa.Direccion + " " + oEmpresa.Sector, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            Chunk ubicacion = new Chunk(oEmpresa.Distrito + " " + oEmpresa.Provincia + " " + oEmpresa.Departamento + " - PERÚ", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            doc.Add(new Paragraph(razonsocial));
            doc.Add(new Paragraph(nombrecomercial));
            doc.Add(new Paragraph(direccion));
            doc.Add(new Paragraph(ubicacion));
            // doc.Add(New Paragraph(" "))

            // Tabla para datos del cabecera
            // Tabla para datos del cabecera
            string tipdoccli = EN_Constante.g_const_vacio;
            if (oDocumento.Tipodoccliente.Length == 1)
            {
                if (oDocumento.Tipodoccliente == "6")
                    tipdoccli = "RUC";
                else if (oDocumento.Tipodoccliente == "1")
                    tipdoccli = "DNI";
                else if (oDocumento.Tipodoccliente == "4")
                    tipdoccli = "CAE";
                else if (oDocumento.Tipodoccliente == "7")
                    tipdoccli = "PAS";
                else if (oDocumento.Tipodoccliente == "A")
                    tipdoccli = "CDI";
                else if (oDocumento.Tipodoccliente == "B")
                    tipdoccli = "DPR";
                else if (oDocumento.Tipodoccliente == "C")
                    tipdoccli = "TAX";
                else if (oDocumento.Tipodoccliente == "D")
                    tipdoccli = "IND";
                else
                    tipdoccli = "OTR";
            }
            else
                tipdoccli = oDocumento.Tipodoccliente;

            PdfPTable table2A = new PdfPTable(4);
                var intTblWidth2A = new int[] { (int)Math.Round(15.0), (int)Math.Round(50.0), (int)Math.Round(15.0), (int)Math.Round(20.0) };
                table2A.SetWidths(intTblWidth2A);

            Chunk tnombreclienteB = new Chunk("Nombre:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tnombreclienteF = new Chunk("Razón Social:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            Chunk tnrodocclienteB = new Chunk("Doc.Identidad:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tnrodocclienteF = new Chunk("RUC:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            Chunk tdireccioncliente = new Chunk("Direccion:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tfechaemision = new Chunk("Fecha Emisión:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            Chunk tmoneda = new Chunk("Moneda:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tordencompra = new Chunk("Orden de Compra:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            Chunk nombrecliente = new Chunk(oDocumento.Nombrecliente, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            Chunk nrodocclienteB = new Chunk(tipdoccli + " " + oDocumento.Nrodoccliente, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            Chunk nrodocclienteF = new Chunk(oDocumento.Nrodoccliente, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            Chunk direccioncliente = new Chunk(oDocumento.Direccioncliente, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            Chunk fechaemision = new Chunk( Convert.ToDateTime(oDocumento.Fecha).ToString("dd/MM/yyyy"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            Chunk moneda = new Chunk(monedaenletras, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            Chunk ordencompra = new Chunk(oDocumento.OrdenCompra, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // Espacio de Tabla
            PdfPCell cell20A = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell20A.HorizontalAlignment = 0;
            cell20A.BorderWidthTop = 0.0F;
            cell20A.BorderWidthBottom = 0.0F;
            cell20A.BorderWidthLeft = 0.0F;
            cell20A.BorderWidthRight = 0.0F;
            cell20A.FixedHeight = 8.0F;

            PdfPCell cell20B = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell20B.HorizontalAlignment = 0;
            cell20B.BorderWidthTop = 0.0F;
            cell20B.BorderWidthBottom = 0.0F;
            cell20B.BorderWidthLeft = 0.0F;
            cell20B.BorderWidthRight = 0.0F;
            cell20B.FixedHeight = 8.0F;

            PdfPCell cell20C = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell20C.HorizontalAlignment = 0;
            cell20C.BorderWidthTop = 0.0F;
            cell20C.BorderWidthBottom = 0.0F;
            cell20C.BorderWidthLeft = 0.0F;
            cell20C.BorderWidthRight = 0.0F;
            cell20C.FixedHeight = 8.0F;

            PdfPCell cell20D = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell20D.HorizontalAlignment = 0;
            cell20D.BorderWidthTop = 0.0F;
            cell20D.BorderWidthBottom = 0.0F;
            cell20D.BorderWidthLeft = 0.0F;
            cell20D.BorderWidthRight = 0.0F;
            cell20D.FixedHeight = 8.0F;

            // Nombre del cliente
            PdfPCell cell22AB = new PdfPCell(new Phrase(tnombreclienteB));
            cell22AB.HorizontalAlignment = 0;
            cell22AB.BorderWidthTop = 0.0F;
            cell22AB.BorderWidthBottom = 0.0F;
            cell22AB.BorderWidthLeft = 0.0F;
            cell22AB.BorderWidthRight = 0.0F;
            // cell22AB.FixedHeight = 14.0F

            PdfPCell cell22AF = new PdfPCell(new Phrase(tnombreclienteF));
            cell22AF.HorizontalAlignment = 0;
            cell22AF.BorderWidthTop = 0.0F;
            cell22AF.BorderWidthBottom = 0.0F;
            cell22AF.BorderWidthLeft = 0.0F;
            cell22AF.BorderWidthRight = 0.0F;
            // cell22AF.FixedHeight = 14.0F

            PdfPCell cell22B = new PdfPCell(new Phrase(nombrecliente));
            cell22B.HorizontalAlignment = 0;
            cell22B.BorderWidthTop = 0.0F;
            cell22B.BorderWidthBottom = 0.0F;
            cell22B.BorderWidthLeft = 0.0F;
            cell22B.BorderWidthRight = 0.0F;
            // cell22B.FixedHeight = 14.0F

            // Nro de Documento del cliente
            PdfPCell cell23AB = new PdfPCell(new Phrase(tnrodocclienteB));
            cell23AB.HorizontalAlignment = 0;
            cell23AB.BorderWidthTop = 0.0F;
            cell23AB.BorderWidthBottom = 0.0F;
            cell23AB.BorderWidthLeft = 0.0F;
            cell23AB.BorderWidthRight = 0.0F;
            // cell23AB.FixedHeight = 14.0F

            PdfPCell cell23AF = new PdfPCell(new Phrase(tnrodocclienteF));
            cell23AF.HorizontalAlignment = 0;
            cell23AF.BorderWidthTop = 0.0F;
            cell23AF.BorderWidthBottom = 0.0F;
            cell23AF.BorderWidthLeft = 0.0F;
            cell23AF.BorderWidthRight = 0.0F;
            // cell23AF.FixedHeight = 14.0F

            PdfPCell cell23BB = new PdfPCell(new Phrase(nrodocclienteB));
            cell23BB.HorizontalAlignment = 0;
            cell23BB.BorderWidthTop = 0.0F;
            cell23BB.BorderWidthBottom = 0.0F;
            cell23BB.BorderWidthLeft = 0.0F;
            cell23BB.BorderWidthRight = 0.0F;
            // cell23BB.FixedHeight = 14.0F

            PdfPCell cell23BF = new PdfPCell(new Phrase(nrodocclienteF));
            cell23BF.HorizontalAlignment = 0;
            cell23BF.BorderWidthTop = 0.0F;
            cell23BF.BorderWidthBottom = 0.0F;
            cell23BF.BorderWidthLeft = 0.0F;
            cell23BF.BorderWidthRight = 0.0F;
            // cell23BF.FixedHeight = 14.0F

            // Direccion del cliente
            PdfPCell cell24A = new PdfPCell(new Phrase(tdireccioncliente));
            cell24A.HorizontalAlignment = 0;
            cell24A.BorderWidthTop = 0.0F;
            cell24A.BorderWidthBottom = 0.0F;
            cell24A.BorderWidthLeft = 0.0F;
            cell24A.BorderWidthRight = 0.0F;
            // cell24A.FixedHeight = 14.0F

            PdfPCell cell24B = new PdfPCell(new Phrase(direccioncliente));
            cell24B.HorizontalAlignment = 0;
            cell24B.BorderWidthTop = 0.0F;
            cell24B.BorderWidthBottom = 0.0F;
            cell24B.BorderWidthLeft = 0.0F;
            cell24B.BorderWidthRight = 0.0F;
            // cell24B.FixedHeight = 14.0F

            // Fecha de emision
            PdfPCell cell21A = new PdfPCell(new Phrase(tfechaemision));
            cell21A.HorizontalAlignment = 0;
            cell21A.BorderWidthTop = 0.0F;
            cell21A.BorderWidthBottom = 0.0F;
            cell21A.BorderWidthLeft = 0.0F;
            cell21A.BorderWidthRight = 0.0F;
            // cell21A.FixedHeight = 14.0F

            PdfPCell cell21B = new PdfPCell(new Phrase(fechaemision));
            cell21B.HorizontalAlignment = 0;
            cell21B.BorderWidthTop = 0.0F;
            cell21B.BorderWidthBottom = 0.0F;
            cell21B.BorderWidthLeft = 0.0F;
            cell21B.BorderWidthRight = 0.0F;
            // cell21B.FixedHeight = 14.0F

            // Moneda
            PdfPCell cell25A = new PdfPCell(new Phrase(tmoneda));
            cell25A.HorizontalAlignment = 0;
            cell25A.BorderWidthTop = 0.0F;
            cell25A.BorderWidthBottom = 0.0F;
            cell25A.BorderWidthLeft = 0.0F;
            cell25A.BorderWidthRight = 0.0F;
            cell25A.FixedHeight = 14.0F;

            PdfPCell cell25B = new PdfPCell(new Phrase(moneda));
            cell25B.HorizontalAlignment = 0;
            cell25B.BorderWidthTop = 0.0F;
            cell25B.BorderWidthBottom = 0.0F;
            cell25B.BorderWidthLeft = 0.0F;
            cell25B.BorderWidthRight = 0.0F;
            cell25B.FixedHeight = 14.0F;

            // Orden de compra
            PdfPCell cell26A = new PdfPCell(new Phrase(tordencompra));
            cell26A.HorizontalAlignment = 0;
            cell26A.BorderWidthTop = 0.0F;
            cell26A.BorderWidthBottom = 0.0F;
            cell26A.BorderWidthLeft = 0.0F;
            cell26A.BorderWidthRight = 0.0F;
            cell26A.FixedHeight = 14.0F;

            PdfPCell cell26B = new PdfPCell(new Phrase(ordencompra));
            cell26B.HorizontalAlignment = 0;
            cell26B.BorderWidthTop = 0.0F;
            cell26B.BorderWidthBottom = 0.0F;
            cell26B.BorderWidthLeft = 0.0F;
            cell26B.BorderWidthRight = 0.0F;
            cell26B.FixedHeight = 14.0F;

            // Espacio de Tabla
            PdfPCell cell27A = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell27A.HorizontalAlignment = 0;
            cell27A.BorderWidthTop = 0.0F;
            cell27A.BorderWidthBottom = 0.0F;
            cell27A.BorderWidthLeft = 0.0F;
            cell27A.BorderWidthRight = 0.0F;
            cell27A.FixedHeight = 8.0F;

            PdfPCell cell27B = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell27B.HorizontalAlignment = 0;
            cell27B.BorderWidthTop = 0.0F;
            cell27B.BorderWidthBottom = 0.0F;
            cell27B.BorderWidthLeft = 0.0F;
            cell27B.BorderWidthRight = 0.0F;
            cell27B.FixedHeight = 8.0F;

            PdfPCell cell28A = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell28A.HorizontalAlignment = 0;
            cell28A.BorderWidthTop = 0.0F;
            cell28A.BorderWidthBottom = 0.0F;
            cell28A.BorderWidthLeft = 0.0F;
            cell28A.BorderWidthRight = 0.0F;
            cell28A.FixedHeight = 8.0F;

            PdfPCell cell28B = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell28B.HorizontalAlignment = 0;
            cell28B.BorderWidthTop = 0.0F;
            cell28B.BorderWidthBottom = 0.0F;
            cell28B.BorderWidthLeft = 0.0F;
            cell28B.BorderWidthRight = 0.0F;
            cell28B.FixedHeight = 8.0F;


            // -----Separador
            table2A.AddCell(cell20A);
            table2A.AddCell(cell20B);
            table2A.AddCell(cell20C);
            table2A.AddCell(cell20D);

            // ---Nombre o Razon Social
            if (tipdoccli == "RUC")
                table2A.AddCell(cell22AF);
            else
                table2A.AddCell(cell22AB);
            table2A.AddCell(cell22B);

            // ---Documento del Cliente
            if (tipdoccli == "RUC")
                table2A.AddCell(cell23AF);
            else
                table2A.AddCell(cell23AB);
            if (tipdoccli == "RUC")
                table2A.AddCell(cell23BF);
            else
                table2A.AddCell(cell23BB);

            // ---Direccion
            table2A.AddCell(cell24A);
            table2A.AddCell(cell24B);

            // ---Fecha Emision
            table2A.AddCell(cell21A);
            table2A.AddCell(cell21B);

            // ---Moneda
            table2A.AddCell(cell25A);
            table2A.AddCell(cell25B);

            // ---Orden de Compra
            table2A.AddCell(cell26A);
            table2A.AddCell(cell26B);

            // -----Separador
            table2A.AddCell(cell27A);
            table2A.AddCell(cell27B);
            table2A.AddCell(cell28A);
            table2A.AddCell(cell28B);

            // Tamaño de tabla
            table2A.WidthPercentage = 100.0F;
            table2A.HorizontalAlignment = 0;
            doc.Add(table2A);


             int n1, n2, n3, ntot;
            decimal tdci, tcgi, tiscv;
            tdci = (decimal) oDocumento.TotalDsctoItem;
            tcgi = (decimal)oDocumento.TotalCargoItem;
            tiscv = (decimal) oDocumento.Iscventa;

            if (tdci > 0)
                n1 = 1;
            else
                n1 = 0;
            if (tcgi > 0)
                n2 = 1;
            else
                n2 = 0;
            if (tiscv > 0)
                n3 = 1;
            else
                n3 = 0;
            ntot = n1 + n2 + n3 + 8;

            // Tabla para el detalle del documento de venta
            int[] intTblWidth3;

            PdfPTable table3;
                if( n1 == 1)
                {
                    table3 = new PdfPTable(9);
                    intTblWidth3 = new int[] { (int)Math.Round(5.0), (int)Math.Round(9.0), (int)Math.Round(38.0), (int)Math.Round(4.0), (int)Math.Round(8.0), (int)Math.Round(9.0), (int)Math.Round(9.0), (int)Math.Round(7.0), (int)Math.Round(11.0) };
                }
                else{
                      table3 = new PdfPTable(8);
                    intTblWidth3 = new int[] { (int)Math.Round(5.0), (int)Math.Round(9.0), (int)Math.Round(38.0), (int)Math.Round(5.0), (int)Math.Round(10.0), (int)Math.Round(10.0), (int)Math.Round(10.0), (int)Math.Round(13.0) };
                }

                 
                table3.SetWidths(intTblWidth3);

            Chunk tItem = new Chunk("Item", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tCodigo = new Chunk("Código", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tDescripcion = new Chunk("Descripción", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tUM = new Chunk("Und", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tCantidad = new Chunk("Cantidad", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tVU = new Chunk("V.Unit.", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tPU = new Chunk("P.Unit.", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tDscto = new Chunk("Dscto.", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tImporteSinIgv = new Chunk("Valor Total", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            PdfPCell cell31A = new PdfPCell(new Phrase(tItem));
            cell31A.HorizontalAlignment = 1;
            cell31A.FixedHeight = 18.0F;

            PdfPCell cell39A = new PdfPCell(new Phrase(tCodigo));
            cell39A.HorizontalAlignment = 1;
            cell39A.FixedHeight = 18.0F;

            PdfPCell cell32A = new PdfPCell(new Phrase(tDescripcion));
            cell32A.HorizontalAlignment = 1;
            cell32A.FixedHeight = 18.0F;

            PdfPCell cell33A = new PdfPCell(new Phrase(tUM));
            cell33A.HorizontalAlignment = 1;
            cell33A.FixedHeight = 18.0F;

            PdfPCell cell36A = new PdfPCell(new Phrase(tCantidad));
            cell36A.HorizontalAlignment = 1;
            cell36A.FixedHeight = 18.0F;

            PdfPCell cell35A = new PdfPCell(new Phrase(tVU));
            cell35A.HorizontalAlignment = 1;
            cell35A.FixedHeight = 18.0F;

            PdfPCell cell34A = new PdfPCell(new Phrase(tPU));
            cell34A.HorizontalAlignment = 1;
            cell34A.FixedHeight = 18.0F;

            PdfPCell cell37A = new PdfPCell(new Phrase(tDscto));
            cell37A.HorizontalAlignment = 1;
            cell37A.FixedHeight = 18.0F;

            PdfPCell cell38A = new PdfPCell(new Phrase(tImporteSinIgv));
            cell38A.HorizontalAlignment = 1;
            cell38A.FixedHeight = 18.0F;

            table3.AddCell(cell31A);
            table3.AddCell(cell39A);
            table3.AddCell(cell32A);
            table3.AddCell(cell33A);
            table3.AddCell(cell36A);
            table3.AddCell(cell35A);
            table3.AddCell(cell34A);
              //dscto
            if (n1 == 1)   table3.AddCell(cell37A);
            

            table3.AddCell(cell38A);

            if (oDetalle.Count > 0)
            {
                foreach (var item in oDetalle.ToList())
                {
                    Chunk itemx = new Chunk(item.Lineid.ToString(), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk Codigo = new Chunk(item.Codigo, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk Descripcion = new Chunk(item.Producto, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk UM = new Chunk(item.Unidad, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                    string[] cant;
                    string cantmostrar;
                    // if (System.Convert.ToString(item.Cantidad).ToString().Replace(",", ".").Contains("."))
                    // {
                    //     cant = Strings.Split(System.Convert.ToString(item.Cantidad).ToString().Replace(",", "."), ".");
                    //     if (Strings.Len(cant[1]) > 2)
                    //     {
                    //         // Imprimo mas de dos decimales
                    //         if (Strings.Len(cant[1]) > 3)
                    //             cantmostrar = Strings.Format(item.Cantidad, "##,##0.0000");
                    //         else
                    //             cantmostrar = Strings.Format(item.Cantidad, "##,##0.000");
                    //     }
                    //     else
                    //         // Imprimo dos decimales
                    //         cantmostrar = Strings.Format(item.Cantidad, "##,##0.00");
                    // }
                    // else
                        // Imprimo dos decimales
                        cantmostrar = Strings.Format(item.Cantidad, "##,##0.00");
                    Chunk Cantidad = new Chunk(cantmostrar, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                    string[] valor2;
                    string valormostrar;
                    // if (System.Convert.ToString(item.Valorunitario).ToString().Replace(",", ".").Contains("."))
                    // {
                    //     valor2 = Strings.Split(System.Convert.ToString(item.Valorunitario.ToString().Replace(",", ".")), ".");
                    //     if (Strings.Len(valor2[1]) > 2)
                    //     {
                    //         // Imprimo mas de dos decimales
                    //         if (Strings.Len(valor2[1]) > 3)
                    //             valormostrar = Strings.Format(item.Valorunitario, "##,##0.0000");
                    //         else
                    //             valormostrar = Strings.Format(item.Valorunitario, "##,##0.000");
                    //     }
                    //     else
                    //         // Imprimo dos decimales
                    //         valormostrar = Strings.Format(item.Valorunitario, "##,##0.00");
                    // }
                    // else
                        // Imprimo dos decimales
                        valormostrar = Strings.Format(item.Valorunitario, "##,##0.00");
                    Chunk VU = new Chunk(valormostrar, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                    string[] valor;
                    string preciomostrar;
                    // if (System.Convert.ToString(item.Preciounitario).ToString().Replace(",", ".").Contains(".") && item.Preciounitario > 0)
                    // {
                    //     valor = Strings.Split(System.Convert.ToString(item.Preciounitario.ToString().Replace(",", ".")), ".");
                    //     if (Strings.Len(valor[1]) > 2)
                    //     {
                    //         // Imprimo mas de dos decimales
                    //         if (Strings.Len(valor[1]) > 3)
                    //             preciomostrar = Strings.Format(item.Preciounitario, "##,##0.0000");
                    //         else
                    //             preciomostrar = Strings.Format(item.Preciounitario, "##,##0.000");
                    //     }
                    //     else
                    //         // Imprimo dos decimales
                    //         preciomostrar = Strings.Format(item.Preciounitario, "##,##0.00");
                    // }
                    // else
                        // Imprimo dos decimales
                        preciomostrar = Strings.Format(item.Preciounitario, "##,##0.00");
                    Chunk PU = new Chunk(preciomostrar, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                        //jc
                    // Chunk Dscto = new Chunk((char)item.Valordscto, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk Dscto = new Chunk(Strings.Format(item.Valordscto, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk ImporteSinIgv = new Chunk(Strings.Format(item.Valorventa, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                    PdfPCell cell31B = new PdfPCell(new Phrase(itemx));
                    cell31B.HorizontalAlignment = 1;

                    PdfPCell cell39B = new PdfPCell(new Phrase(Codigo));
                    cell39B.HorizontalAlignment = 1;

                    PdfPCell cell32B = new PdfPCell(new Phrase(Descripcion));
                    cell32B.HorizontalAlignment = 0;

                    PdfPCell cell33B = new PdfPCell(new Phrase(UM));
                    cell33B.HorizontalAlignment = 1;

                    PdfPCell cell36B = new PdfPCell(new Phrase(Cantidad));
                    cell36B.HorizontalAlignment = 2;

                    PdfPCell cell35B = new PdfPCell(new Phrase(VU));
                    cell35B.HorizontalAlignment = 2;

                    PdfPCell cell34B = new PdfPCell(new Phrase(PU));
                    cell34B.HorizontalAlignment = 2;

                    PdfPCell cell37B = new PdfPCell(new Phrase(Dscto));
                    cell37B.HorizontalAlignment = 2;

                    PdfPCell cell38B = new PdfPCell(new Phrase(ImporteSinIgv));
                    cell38B.HorizontalAlignment = 2;

                    table3.AddCell(cell31B);
                    table3.AddCell(cell39B);
                    table3.AddCell(cell32B);
                    table3.AddCell(cell33B);
                    table3.AddCell(cell36B);
                    table3.AddCell(cell35B);
                    table3.AddCell(cell34B);

                    //dscto
                     if (n1 == 1)   table3.AddCell(cell37B);


                    table3.AddCell(cell38B);
                }
            }
            // Tamaño de tabla
            table3.WidthPercentage = 100.0F;
            table3.HorizontalAlignment = 0;
            doc.Add(table3);
            doc.Add(new Paragraph(" "));



            // Tabla para los totales y monto en letras
            double porcentajedescuento = oDocumento.Descuento;
            PdfPTable table4 = new PdfPTable(5);
                var intTblWidth4 = new int[] { (int)Math.Round(50.0), (int)Math.Round(5.0), (int)Math.Round(20.0), (int)Math.Round(10.0), (int)Math.Round(15.0) };
                table4.SetWidths(intTblWidth4);

            // Monto en letras
            Chunk montoventaletras = new Chunk("SON: " + montoenletras + " " + monedaenletras, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // Para los totales
            Chunk tDescuento = new Chunk("Descuento(%):", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tAfectas = new Chunk("Op. Gravada:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tInafectas = new Chunk("Op. Inafecta:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tExoneradas = new Chunk("Op. Exonerada:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tGratuitas = new Chunk("Op. Gratuita:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tExportadas = new Chunk("Op. Exportación:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tIGV = new Chunk("IGV:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tISC = new Chunk("ISC:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tImporteVenta = new Chunk("Importe Venta:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tImporteAnticipo = new Chunk("Importe Anticipos:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tPercepcion = new Chunk("Monto Percepción:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tImporteFinal = new Chunk("Importe Total:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk tTotalDescuento = new Chunk("Total Descuentos:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));

            Chunk simboloMoneda = new Chunk(symbmoneda, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));

            decimal valoropexport;
            if (oDocumento.Estransgratuita == "SI")
                valoropexport = 0;
            else
                valoropexport = (decimal)oDocumento.Valorperexportacion;

            Chunk Descuento = new Chunk((char)oDocumento.Descuento, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Afectas = new Chunk(Strings.Format(oDocumento.Valorpergravadas, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Inafectas = new Chunk(Strings.Format(oDocumento.Valorperinafectas, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Exoneradas = new Chunk(Strings.Format(oDocumento.Valorperexoneradas, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Exportadas = new Chunk(Strings.Format(valoropexport, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Gratuitas = new Chunk(Strings.Format(oDocumento.Valorpergratuitas, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk IGV = new Chunk(Strings.Format(oDocumento.Igvventa, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk ISC = new Chunk(Strings.Format(oDocumento.Iscventa, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk ImporteVenta = new Chunk(Strings.Format(oDocumento.Importeventa, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk ImporteAnticipo = new Chunk(Strings.Format(oDocumento.Importeanticipo, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Percepcion = new Chunk(Strings.Format(oDocumento.Importepercep, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk ImporteFinal = new Chunk(Strings.Format(oDocumento.Importefinal, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk TotalDescuento = new Chunk(Strings.Format(porcentajedescuento, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));

            // Para la leyenda de monto en letras
            PdfPCell cell4ML = new PdfPCell(new Phrase(montoventaletras));
            cell4ML.HorizontalAlignment = 0;
            cell4ML.BorderWidthTop = 0.0F;
            cell4ML.BorderWidthBottom = 0.0F;
            cell4ML.BorderWidthLeft = 0.0F;
            cell4ML.BorderWidthRight = 0.0F;
            cell4ML.FixedHeight = 14.0F;
            cell4ML.Rowspan = 3;

            // Celda Vacia
            PdfPCell cell4MV = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell4MV.HorizontalAlignment = 0;
            cell4MV.BorderWidthTop = 0.0F;
            cell4MV.BorderWidthBottom = 0.0F;
            cell4MV.BorderWidthLeft = 0.0F;
            cell4MV.BorderWidthRight = 0.0F;
            cell4MV.FixedHeight = 14.0F;

            // Para la moneda de los montos
            PdfPCell cell4MM = new PdfPCell(new Phrase(simboloMoneda));
            cell4MM.HorizontalAlignment = 0;
            cell4MM.BorderWidthTop = 0.0F;
            cell4MM.BorderWidthBottom = 0.0F;
            cell4MM.BorderWidthLeft = 0.0F;
            cell4MM.BorderWidthRight = 0.0F;
            cell4MM.FixedHeight = 14.0F;

            // Para descuentos
            PdfPCell cell41A = new PdfPCell(new Phrase(tDescuento));
            cell41A.HorizontalAlignment = 0;
            cell41A.BorderWidthTop = 0.0F;
            cell41A.BorderWidthBottom = 0.0F;
            cell41A.BorderWidthLeft = 0.0F;
            cell41A.BorderWidthRight = 0.0F;
            cell41A.FixedHeight = 14.0F;

            PdfPCell cell41B = new PdfPCell(new Phrase(Descuento));
            cell41B.HorizontalAlignment = 2;
            cell41B.BorderWidthTop = 0.0F;
            cell41B.BorderWidthBottom = 0.0F;
            cell41B.BorderWidthLeft = 0.0F;
            cell41B.BorderWidthRight = 0.0F;
            cell41B.FixedHeight = 14.0F;

            // Para Op Afectas
            PdfPCell cell42A = new PdfPCell(new Phrase(tAfectas));
            cell42A.HorizontalAlignment = 0;
            cell42A.BorderWidthTop = 0.0F;
            cell42A.BorderWidthBottom = 0.0F;
            cell42A.BorderWidthLeft = 0.0F;
            cell42A.BorderWidthRight = 0.0F;
            cell42A.FixedHeight = 14.0F;

            PdfPCell cell42B = new PdfPCell(new Phrase(Afectas));
            cell42B.HorizontalAlignment = 2;
            cell42B.BorderWidthTop = 0.0F;
            cell42B.BorderWidthBottom = 0.0F;
            cell42B.BorderWidthLeft = 0.0F;
            cell42B.BorderWidthRight = 0.0F;
            cell42B.FixedHeight = 14.0F;

            // Op Inafectas
            PdfPCell cell43A = new PdfPCell(new Phrase(tInafectas));
            cell43A.HorizontalAlignment = 0;
            cell43A.BorderWidthTop = 0.0F;
            cell43A.BorderWidthBottom = 0.0F;
            cell43A.BorderWidthLeft = 0.0F;
            cell43A.BorderWidthRight = 0.0F;
            cell43A.FixedHeight = 14.0F;

            PdfPCell cell43B = new PdfPCell(new Phrase(Inafectas));
            cell43B.HorizontalAlignment = 2;
            cell43B.BorderWidthTop = 0.0F;
            cell43B.BorderWidthBottom = 0.0F;
            cell43B.BorderWidthLeft = 0.0F;
            cell43B.BorderWidthRight = 0.0F;
            cell43B.FixedHeight = 14.0F;

            // Op Exoneradas
            PdfPCell cell44A = new PdfPCell(new Phrase(tExoneradas));
            cell44A.HorizontalAlignment = 0;
            cell44A.BorderWidthTop = 0.0F;
            cell44A.BorderWidthBottom = 0.0F;
            cell44A.BorderWidthLeft = 0.0F;
            cell44A.BorderWidthRight = 0.0F;
            cell44A.FixedHeight = 14.0F;

            PdfPCell cell44B = new PdfPCell(new Phrase(Exoneradas));
            cell44B.HorizontalAlignment = 2;
            cell44B.BorderWidthTop = 0.0F;
            cell44B.BorderWidthBottom = 0.0F;
            cell44B.BorderWidthLeft = 0.0F;
            cell44B.BorderWidthRight = 0.0F;
            cell44B.FixedHeight = 14.0F;

            // Op Exportacion
            PdfPCell cell44AX = new PdfPCell(new Phrase(tExportadas));
            cell44AX.HorizontalAlignment = 0;
            cell44AX.BorderWidthTop = 0.0F;
            cell44AX.BorderWidthBottom = 0.0F;
            cell44AX.BorderWidthLeft = 0.0F;
            cell44AX.BorderWidthRight = 0.0F;
            cell44AX.FixedHeight = 14.0F;

            PdfPCell cell44BX = new PdfPCell(new Phrase(Exportadas));
            cell44BX.HorizontalAlignment = 2;
            cell44BX.BorderWidthTop = 0.0F;
            cell44BX.BorderWidthBottom = 0.0F;
            cell44BX.BorderWidthLeft = 0.0F;
            cell44BX.BorderWidthRight = 0.0F;
            cell44BX.FixedHeight = 14.0F;

            // Op Gratuitas
            PdfPCell cell45A = new PdfPCell(new Phrase(tGratuitas));
            cell45A.HorizontalAlignment = 0;
            cell45A.BorderWidthTop = 0.0F;
            cell45A.BorderWidthBottom = 0.0F;
            cell45A.BorderWidthLeft = 0.0F;
            cell45A.BorderWidthRight = 0.0F;
            cell45A.FixedHeight = 14.0F;

            PdfPCell cell45B = new PdfPCell(new Phrase(Gratuitas));
            cell45B.HorizontalAlignment = 2;
            cell45B.BorderWidthTop = 0.0F;
            cell45B.BorderWidthBottom = 0.0F;
            cell45B.BorderWidthLeft = 0.0F;
            cell45B.BorderWidthRight = 0.0F;
            cell45B.FixedHeight = 14.0F;

            // IGV
            PdfPCell cell46A = new PdfPCell(new Phrase(tIGV));
            cell46A.HorizontalAlignment = 0;
            cell46A.BorderWidthTop = 0.0F;
            cell46A.BorderWidthBottom = 0.0F;
            cell46A.BorderWidthLeft = 0.0F;
            cell46A.BorderWidthRight = 0.0F;
            cell46A.FixedHeight = 14.0F;

            PdfPCell cell46B = new PdfPCell(new Phrase(IGV));
            cell46B.HorizontalAlignment = 2;
            cell46B.BorderWidthTop = 0.0F;
            cell46B.BorderWidthBottom = 0.0F;
            cell46B.BorderWidthLeft = 0.0F;
            cell46B.BorderWidthRight = 0.0F;
            cell46B.FixedHeight = 14.0F;

            // ISC
            PdfPCell cell47A = new PdfPCell(new Phrase(tISC));
            cell47A.HorizontalAlignment = 0;
            cell47A.BorderWidthTop = 0.0F;
            cell47A.BorderWidthBottom = 0.0F;
            cell47A.BorderWidthLeft = 0.0F;
            cell47A.BorderWidthRight = 0.0F;
            cell47A.FixedHeight = 14.0F;

            PdfPCell cell47B = new PdfPCell(new Phrase(ISC));
            cell47B.HorizontalAlignment = 2;
            cell47B.BorderWidthTop = 0.0F;
            cell47B.BorderWidthBottom = 0.0F;
            cell47B.BorderWidthLeft = 0.0F;
            cell47B.BorderWidthRight = 0.0F;
            cell47B.FixedHeight = 14.0F;

            // Importe de Venta
            PdfPCell cell48A = new PdfPCell(new Phrase(tImporteVenta));
            cell48A.HorizontalAlignment = 0;
            cell48A.BorderWidthTop = 0.0F;
            cell48A.BorderWidthBottom = 0.0F;
            cell48A.BorderWidthLeft = 0.0F;
            cell48A.BorderWidthRight = 0.0F;
            cell48A.FixedHeight = 14.0F;

            PdfPCell cell48B = new PdfPCell(new Phrase(ImporteVenta));
            cell48B.HorizontalAlignment = 2;
            cell48B.BorderWidthTop = 0.0F;
            cell48B.BorderWidthBottom = 0.0F;
            cell48B.BorderWidthLeft = 0.0F;
            cell48B.BorderWidthRight = 0.0F;
            cell48B.FixedHeight = 14.0F;

            // Percepcion
            PdfPCell cell49A = new PdfPCell(new Phrase(tPercepcion));
            cell49A.HorizontalAlignment = 0;
            cell49A.BorderWidthTop = 0.0F;
            cell49A.BorderWidthBottom = 0.0F;
            cell49A.BorderWidthLeft = 0.0F;
            cell49A.BorderWidthRight = 0.0F;
            cell49A.FixedHeight = 14.0F;

            PdfPCell cell49B = new PdfPCell(new Phrase(Percepcion));
            cell49B.HorizontalAlignment = 2;
            cell49B.BorderWidthTop = 0.0F;
            cell49B.BorderWidthBottom = 0.0F;
            cell49B.BorderWidthLeft = 0.0F;
            cell49B.BorderWidthRight = 0.0F;
            cell49B.FixedHeight = 14.0F;

            // Total de descuentos
            PdfPCell cell51A = new PdfPCell(new Phrase(tTotalDescuento));
            cell51A.HorizontalAlignment = 0;
            cell51A.BorderWidthTop = 0.0F;
            cell51A.BorderWidthBottom = 0.0F;
            cell51A.BorderWidthLeft = 0.0F;
            cell51A.BorderWidthRight = 0.0F;
            cell51A.FixedHeight = 14.0F;

            PdfPCell cell51B = new PdfPCell(new Phrase(TotalDescuento));
            cell51B.HorizontalAlignment = 2;
            cell51B.BorderWidthTop = 0.0F;
            cell51B.BorderWidthBottom = 0.0F;
            cell51B.BorderWidthLeft = 0.0F;
            cell51B.BorderWidthRight = 0.0F;
            cell51B.FixedHeight = 14.0F;

            // Anticipos
            PdfPCell cell52A = new PdfPCell(new Phrase(tImporteAnticipo));
            cell52A.HorizontalAlignment = 0;
            cell52A.BorderWidthTop = 0.0F;
            cell52A.BorderWidthBottom = 0.0F;
            cell52A.BorderWidthLeft = 0.0F;
            cell52A.BorderWidthRight = 0.0F;
            cell52A.FixedHeight = 14.0F;

            PdfPCell cell52B = new PdfPCell(new Phrase(ImporteAnticipo));
            cell52B.HorizontalAlignment = 2;
            cell52B.BorderWidthTop = 0.0F;
            cell52B.BorderWidthBottom = 0.0F;
            cell52B.BorderWidthLeft = 0.0F;
            cell52B.BorderWidthRight = 0.0F;
            cell52B.FixedHeight = 14.0F;

            // Importe Final
            PdfPCell cell50A = new PdfPCell(new Phrase(tImporteFinal));
            cell50A.HorizontalAlignment = 0;
            cell50A.BorderWidthTop = 0.0F;
            cell50A.BorderWidthBottom = 0.0F;
            cell50A.BorderWidthLeft = 0.0F;
            cell50A.BorderWidthRight = 0.0F;
            cell50A.FixedHeight = 14.0F;

            PdfPCell cell50B = new PdfPCell(new Phrase(ImporteFinal));
            cell50B.HorizontalAlignment = 2;
            cell50B.BorderWidthTop = 0.0F;
            cell50B.BorderWidthBottom = 0.0F;
            cell50B.BorderWidthLeft = 0.0F;
            cell50B.BorderWidthRight = 0.0F;
            cell50B.FixedHeight = 14.0F;


            // Agregamos los datos en la tabla

            // Descuento
            table4.AddCell(cell4ML);
            table4.AddCell(cell4MV);
            if (n1 == 1)
            {
                table4.AddCell(cell41A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell41B);
            }
            else 
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
            }



            // Op Gravadas
            // table4.AddCell(cell4MV)
            table4.AddCell(cell4MV);
            table4.AddCell(cell42A);
            table4.AddCell(cell4MM);
            table4.AddCell(cell42B);

            // IGV
            // table4.AddCell(cell4MV)
            table4.AddCell(cell4MV);
            table4.AddCell(cell46A);
            table4.AddCell(cell4MM);
            table4.AddCell(cell46B);

            // ISC
            if (oDocumento.Iscventa > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell47A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell47B);
            }

            // Op Inafectas
            table4.AddCell(cell4MV);
            table4.AddCell(cell4MV);
            table4.AddCell(cell43A);
            table4.AddCell(cell4MM);
            table4.AddCell(cell43B);

            // Op Exoneradas 
            table4.AddCell(cell4MV);
            table4.AddCell(cell4MV);
            table4.AddCell(cell44A);
            table4.AddCell(cell4MM);
            table4.AddCell(cell44B);

            // Op Exportadas 
            if (oDocumento.Valorperexportacion > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell44AX);
                table4.AddCell(cell4MM);
                table4.AddCell(cell44BX);
            }

            // Op Gratuitas
            if (oDocumento.Valorpergratuitas > 0 && oDocumento.Estransgratuita == "SI")
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell45A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell45B);
            }

            // Percepciones
            if (oDocumento.Importepercep > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell48A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell48B);
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell49A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell49B);
            }

            // Anticipos
            if (oDocumento.Importeanticipo > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell52A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell52B);
            }

            // Total de Descuentos
            if (porcentajedescuento > 0)
            {
                table4.AddCell(cell4MV);
                table4.AddCell(cell4MV);
                table4.AddCell(cell51A);
                table4.AddCell(cell4MM);
                table4.AddCell(cell51B);
            }

            // Importe Final
            table4.AddCell(cell4MV);
            table4.AddCell(cell4MV);
            table4.AddCell(cell50A);
            table4.AddCell(cell4MM);
            table4.AddCell(cell50B);

            // Tamaño de tabla
            table4.WidthPercentage = 100.0F;
            table4.HorizontalAlignment = 1;
            doc.Add(table4);

           

            // Motivo Sustento
            PdfPTable table5 = new PdfPTable(1);
                var intTblWidth5 = new int[] { (int)Math.Round(100.0) };
                
            table5.SetWidths(intTblWidth5);

            Chunk TituloMotivoNota = new Chunk("Motivo o Sustento:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk TipoNota = new Chunk("Tipo de Nota: " + oDocumento.desctiponotcreddeb, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));
            Chunk GlosaNota = new Chunk(oDocumento.Glosa.Trim(), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));

            PdfPCell cell60 = new PdfPCell(new Phrase(TituloMotivoNota));
            cell60.HorizontalAlignment = 0;
            cell60.BorderWidthBottom = 0.0F;
            cell60.FixedHeight = 14.0F;

            PdfPCell cell61 = new PdfPCell(new Phrase(TipoNota));
            cell61.HorizontalAlignment = 0;
            cell61.BorderWidthBottom = 0.0F;
            cell61.FixedHeight = 14.0F;

            PdfPCell cell62 = new PdfPCell(new Phrase(GlosaNota));
            cell62.HorizontalAlignment = 0;
            cell62.BorderWidthTop = 0.0F;
            cell62.FixedHeight = 14.0F;

            table5.AddCell(cell60);
            table5.AddCell(cell61);
            table5.AddCell(cell62);

            table5.WidthPercentage = 100.0F;
            table5.HorizontalAlignment = 0;
            doc.Add(table5);


            // Para la informaciòn adicional
            PdfPTable table6 = new PdfPTable(2);
                var intTblWidth6 = new int[] { (int)Math.Round(20.0), (int)Math.Round(60.0) };
                table6.SetWidths(intTblWidth6);

            Chunk tTituloAdic = new Chunk("Información Adicional:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));


            string documentoreferenciancnd = EN_Constante.g_const_vacio;
            if (oDetalleNCNDDocRefencia.Count > 0)
                documentoreferenciancnd = oDetalleNCNDDocRefencia[0].Numerodocref;

            // documentoreferenciancnd
            // Guias Referencias
            Chunk tReferencia = new Chunk("Documento Referencia:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Referencia = new Chunk(documentoreferenciancnd, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));

            // oDocumento.text_aux_04
            // Mensaje
            Chunk tMsj = new Chunk("Mensaje:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Msj = new Chunk(oDocumento.text_aux_04, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));

            // oDocumento.text_aux_01
            // Código Cliente
            Chunk tCodCliente = new Chunk("Código Cliente:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk CodCliente = new Chunk(oDocumento.text_aux_01, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));

            // oDocumento.text_aux_02
            // Vencimiento
            Chunk tVencimiento = new Chunk("Vencimiento:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Vencimiento = new Chunk(oDocumento.text_aux_02, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));

            // oDocumento.text_aux_05
            // Leyenda
            Chunk tLeyenda = new Chunk("Leyenda:", FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.BOLD));
            Chunk Leyenda = new Chunk(oDocumento.text_aux_05.Trim(), FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));

            PdfPCell cell70 = new PdfPCell(new Phrase(tTituloAdic));
            cell70.HorizontalAlignment = 0;
            cell70.BorderWidthTop = 0.0F;
            cell70.BorderWidthBottom = 0.0F;
            cell70.BorderWidthLeft = 0.0F;
            cell70.BorderWidthRight = 0.0F;
            cell70.Colspan = 2;
            cell70.FixedHeight = 14.0F;

            PdfPCell cell71 = new PdfPCell(new Phrase(tReferencia));
            cell71.HorizontalAlignment = 0;
            PdfPCell cell72 = new PdfPCell(new Phrase(Referencia));
            cell72.HorizontalAlignment = 0;
            PdfPCell cell73 = new PdfPCell(new Phrase(tMsj));
            cell73.HorizontalAlignment = 0;
            PdfPCell cell74 = new PdfPCell(new Phrase(Msj));
            cell74.HorizontalAlignment = 0;
            PdfPCell cell75 = new PdfPCell(new Phrase(tCodCliente));
            cell75.HorizontalAlignment = 0;
            PdfPCell cell76 = new PdfPCell(new Phrase(CodCliente));
            cell76.HorizontalAlignment = 0;
            PdfPCell cell77 = new PdfPCell(new Phrase(tVencimiento));
            cell77.HorizontalAlignment = 0;
            PdfPCell cell78 = new PdfPCell(new Phrase(Vencimiento));
            cell78.HorizontalAlignment = 0;
            PdfPCell cell79 = new PdfPCell(new Phrase(tLeyenda));
            cell79.HorizontalAlignment = 0;
            PdfPCell cell7A = new PdfPCell(new Phrase(Leyenda));
            cell7A.HorizontalAlignment = 0;

            table6.AddCell(cell70);

            table6.AddCell(cell71);
            table6.AddCell(cell72);

            if (oDocumento.text_aux_04.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell73);
                table6.AddCell(cell74);
            }

            if (oDocumento.text_aux_01.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell75);
                table6.AddCell(cell76);
            }

            if (oDocumento.text_aux_02.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell77);
                table6.AddCell(cell78);
            }

            if (oDocumento.text_aux_05.Trim() != EN_Constante.g_const_vacio)
            {
                table6.AddCell(cell79);
                table6.AddCell(cell7A);
            }

            // Tamaño de tabla
            table6.WidthPercentage = 80.0F;
            table6.HorizontalAlignment = 0;


            // Para codigo QR
            PdfPTable table8 = new PdfPTable(1);
                var intTblWidth8 = new int[] { (int)Math.Round(20.0) };
                table8.SetWidths(intTblWidth8);

            PdfPCell cell90 = new PdfPCell(new Phrase(EN_Constante.g_const_vacio));
            cell90.HorizontalAlignment = 2;
            cell90.VerticalAlignment = 1;
            cell90.BorderWidthTop = 0.0F;
            cell90.BorderWidthBottom = 0.0F;
            cell90.BorderWidthLeft = 0.0F;
            cell90.BorderWidthRight = 0.0F;
            cell90.FixedHeight = 14.0F;

            PdfPCell cell91 = new PdfPCell(pdfImage);
            cell91.HorizontalAlignment = 2;
            cell91.VerticalAlignment = 1;
            cell91.BorderWidthTop = 0.0F;
            cell91.BorderWidthBottom = 0.0F;
            cell91.BorderWidthLeft = 0.0F;
            cell91.BorderWidthRight = 0.0F;


            table8.AddCell(cell90);
            table8.AddCell(cell91);
            table8.WidthPercentage = 20.0F;
            table8.HorizontalAlignment = 2;

            var outerTablex = new PdfPTable(2);
                var outTblWidthx = new int[] { (int)Math.Round(80.0), (int)Math.Round(20.0) };
                outerTablex.SetWidths(outTblWidthx);

            PdfPCell cx1 = new PdfPCell(table6);
            PdfPCell cx2 = new PdfPCell(table8);

            cx1.BorderWidthBottom = 0.0F;
            cx1.BorderWidthTop = 0.0F;
            cx1.BorderWidthLeft = 0.0F;
            cx1.BorderWidthRight = 0.0F;

            cx2.BorderWidthBottom = 0.0F;
            cx2.BorderWidthTop = 0.0F;
            cx2.BorderWidthLeft = 0.0F;
            cx2.BorderWidthRight = 0.0F;

            cx1.HorizontalAlignment = 0;
            cx2.HorizontalAlignment = 2;

            outerTablex.AddCell(cx1);
            outerTablex.AddCell(cx2);

            outerTablex.WidthPercentage = 100.0F;
            outerTablex.HorizontalAlignment = 0;

            doc.Add(outerTablex);

            // Texto de Autorización de SUNAT
            string textoautorizac = EN_Constante.g_const_vacio;
            if (oEmpresa.NroResolucion.Trim() != EN_Constante.g_const_vacio)
                textoautorizac = "Autorizado a ser emisor electrónico mediante R.I. SUNAT N° " + oEmpresa.NroResolucion;

            Chunk tTextoautorizac = new Chunk(textoautorizac, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // Texto de Leyenda de Representación impresa
            string textoleyenda = EN_Constante.g_const_vacio;
            string textolink = ", consulte en " + oEmpresa.Web + ".";
            if (oDocumento.Idtipodocumento.Trim() == oCompSunat.IdNC)
                textoleyenda = "Representación Impresa de la Nota de Crédito Electrónica.";
            else if (oDocumento.Idtipodocumento.Trim() == oCompSunat.IdND)
                textoleyenda = "Representación Impresa de la Nota de Débito Electrónica.";
            if (oEmpresa.Web.Trim() == EN_Constante.g_const_vacio)
                textoleyenda = textoleyenda + " Código Hash: " + valorResumen;
            else
                textoleyenda = textoleyenda + textolink + " Código Hash: " + valorResumen;

            Chunk tTextoleyenda = new Chunk(textoleyenda, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.ITALIC));

            PdfPTable table9 = new PdfPTable(1);
                var intTblWidth9 = new int[] { (int)Math.Round(90.0) };
                table9.SetWidths(intTblWidth9);

            PdfPCell cell98 = new PdfPCell(new Phrase(tTextoautorizac));
            cell98.HorizontalAlignment = 0;
            cell98.VerticalAlignment = 1;
            cell98.BorderWidthTop = 0.0F;
            cell98.BorderWidthBottom = 0.0F;
            cell98.BorderWidthLeft = 0.0F;
            cell98.BorderWidthRight = 0.0F;
            // cell98.FixedHeight = 14.0F

            PdfPCell cell99 = new PdfPCell(new Phrase(tTextoleyenda));
            cell99.HorizontalAlignment = 0;
            cell99.VerticalAlignment = 1;
            cell99.BorderWidthTop = 0.0F;
            cell99.BorderWidthBottom = 0.0F;
            cell99.BorderWidthLeft = 0.0F;
            cell99.BorderWidthRight = 0.0F;
            // cell99.FixedHeight = 14.0F
            if (textoautorizac.Trim() != EN_Constante.g_const_vacio)
                table9.AddCell(cell98);
            table9.AddCell(cell99);

            table9.WidthPercentage = 100.0F;
            table9.HorizontalAlignment = 0;

            doc.Add(table9);

            doc.Dispose();
            doc.Close();
            file.Dispose();
            file.Close();

             //COPIAMOS ARCHIVO A S3 Y ELIMINAMOS DE TEMPORAL
            string nombrePDF= oEmpresa.Nrodocumento.Trim() + "-" + oDocumento.Idtipodocumento.Trim() + "-" + oDocumento.Serie.Trim() + "-" + oDocumento.Numero.Trim();
            AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oDocumento.NroDocEmpresa,rutabase,EN_Constante.g_const_rutaSufijo_pdf,nombrePDF,EN_Constante.g_const_extension_pdf);


        }
    }


    public void GenerarPDFComunicadoBaja(EN_Empresa oEmpresa, EN_Baja oBaja, List<EN_DetalleBaja> oDetalle, string ruta, EN_ComprobanteSunat oCompSunat, string valorResumen, string firma, string rutabase, string situacion = "", string mensaje = "")
    {
        //DESCRIPCION: FUNCION PARA GENERAR PDF DE COMUNICADO DE BAJA
        ruta= ruta + EN_Constante.g_const_rutaSufijo_pdf;
        string rutalogo = string.Format("{0}/{1}/", rutabase, oBaja.Nrodocumento);
        var doc = new Document(PageSize.A4.Rotate(), 35.0f, 35.0f, 35.0f, 35.0f);
        string filename = ruta + oBaja.Nrodocumento.Trim() + "-" + oBaja.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oBaja.FechaComunicacion), EN_Constante.g_const_formfecha_yyyyMMdd) + "-" + oBaja.Correlativo + EN_Constante.g_const_extension_pdf;

        string logo = rutalogo + oEmpresa.Imagen;

        if (!string.IsNullOrEmpty(filename.Trim()))
        {
            var file = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            PdfWriter.GetInstance(doc, file);
            doc.Open();


            // Para el logo
            dynamic imagen =null;

            if (File.Exists(logo))
            {
                imagen = iTextSharp.text.Image.GetInstance(logo);
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_LEFT;
                imagen.ScaleAbsolute(100, 100);
            }
            else
            {
                imagen=new Phrase(EN_Constante.g_const_vacio);
            }

            // Tabla para Tipo de documento - RUC - Serie y Numero de documento
            var table1 = new PdfPTable(1);
            string textoTipoDoc = "";
            if (oBaja.Tiporesumen.Trim() == oCompSunat.IdCB)
            {
                textoTipoDoc = "COMUNICADO DE BAJAS";
            }

            var tipodocumento = new Chunk(textoTipoDoc, FontFactory.GetFont(BaseFont.COURIER, 14, iTextSharp.text.Font.BOLD));
            var ruc = new Chunk("RUC: " + oEmpresa.Nrodocumento, FontFactory.GetFont(BaseFont.COURIER, 11, iTextSharp.text.Font.NORMAL));
            var nrodocumento = new Chunk(oBaja.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oBaja.FechaComunicacion), "yyyyMMdd") + "-" + oBaja.Correlativo, FontFactory.GetFont(BaseFont.COURIER, 11, iTextSharp.text.Font.NORMAL));
            // Alineacion 0=Left, 1=Centre, 2=Right
            var cell11 = new PdfPCell(new Phrase(tipodocumento));
            cell11.HorizontalAlignment = 1;
            cell11.BorderWidthBottom = 0.0f;
            cell11.FixedHeight = 18.0f;
            var cell12 = new PdfPCell(new Phrase(ruc));
            cell12.HorizontalAlignment = 1;
            cell12.BorderWidthBottom = 0.0f;
            cell12.BorderWidthTop = 0.0f;
            cell12.FixedHeight = 18.0f;
            var cell13 = new PdfPCell(new Phrase(nrodocumento));
            cell13.HorizontalAlignment = 1;
            cell13.BorderWidthTop = 0.0f;
            cell13.FixedHeight = 18.0f;
            table1.AddCell(cell11);
            table1.AddCell(cell12);
            table1.AddCell(cell13);
            // Tamaño de tabla
            table1.WidthPercentage = 35.0f;
            table1.HorizontalAlignment = 2;
            var outerTable = new PdfPTable(2);
            var outTblWidth2 = new int[] { (int)Math.Round(65.0), (int)Math.Round(35.0) };
            outerTable.SetWidths(outTblWidth2);
            var c1 = new PdfPCell(imagen);
            var c2 = new PdfPCell(table1);
            c1.BorderWidthBottom = 0.0f;
            c1.BorderWidthTop = 0.0f;
            c1.BorderWidthLeft = 0.0f;
            c1.BorderWidthRight = 0.0f;
            c2.BorderWidthBottom = 0.0f;
            c2.BorderWidthTop = 0.0f;
            c2.BorderWidthLeft = 0.0f;
            c2.BorderWidthRight = 0.0f;
            c1.HorizontalAlignment = 0;
            c2.HorizontalAlignment = 2;
            outerTable.AddCell(c1);
            outerTable.AddCell(c2);
            outerTable.WidthPercentage = 100.0f;
            outerTable.HorizontalAlignment = 0;
            doc.Add(outerTable);

            // Mostramos los datos de la empresa
            var razonsocial = new Chunk(oEmpresa.RazonSocial, FontFactory.GetFont(BaseFont.COURIER, 14, iTextSharp.text.Font.BOLD));
            var direccion = new Chunk(oEmpresa.Direccion + " " + oEmpresa.Sector, FontFactory.GetFont(BaseFont.COURIER, 10, iTextSharp.text.Font.NORMAL));
            var ubicacion = new Chunk(oEmpresa.Departamento + " " + oEmpresa.Provincia + " " + oEmpresa.Distrito, FontFactory.GetFont(BaseFont.COURIER, 10, iTextSharp.text.Font.NORMAL));
            var fechacomunicacion = new Chunk("Fecha Comunicación Baja: " + Strings.Format(Conversions.ToDate(oBaja.FechaComunicacion), "dd/MM/yyyy"), FontFactory.GetFont(BaseFont.COURIER, 10, iTextSharp.text.Font.NORMAL));
            var fechaemision = new Chunk("Fecha Emisión Documentos: " + Strings.Format(Conversions.ToDate(oBaja.FechaDocumento), "dd/MM/yyyy"), FontFactory.GetFont(BaseFont.COURIER, 10, iTextSharp.text.Font.NORMAL));
            doc.Add(new Paragraph(razonsocial));
            doc.Add(new Paragraph(direccion));
            doc.Add(new Paragraph(ubicacion));
            doc.Add(new Paragraph(fechacomunicacion));
            doc.Add(new Paragraph(fechaemision));
            doc.Add(new Paragraph(" "));

            // Tabla para el detalle de la baja
            var table3 = new PdfPTable(5);
            var intTblWidth3 = new int[] { (int)Math.Round(10.0), (int)Math.Round(30.0), (int)Math.Round(10.0), (int)Math.Round(15.0), (int)Math.Round(35.0) };
            table3.SetWidths(intTblWidth3);
            var tItem = new Chunk("Item", FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
            var tComprobante = new Chunk("Comprobante", FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
            var tSerie = new Chunk("Serie", FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
            var tNumero = new Chunk("Numero", FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
            var tMotivo = new Chunk("Motivo", FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
            var cell31A = new PdfPCell(new Phrase(tItem));
            cell31A.HorizontalAlignment = 1;
            cell31A.FixedHeight = 18.0f;
            var cell32A = new PdfPCell(new Phrase(tComprobante));
            cell32A.HorizontalAlignment = 1;
            cell32A.FixedHeight = 18.0f;
            var cell33A = new PdfPCell(new Phrase(tSerie));
            cell33A.HorizontalAlignment = 1;
            cell33A.FixedHeight = 18.0f;
            var cell34A = new PdfPCell(new Phrase(tNumero));
            cell34A.HorizontalAlignment = 1;
            cell34A.FixedHeight = 18.0f;
            var cell35A = new PdfPCell(new Phrase(tMotivo));
            cell35A.HorizontalAlignment = 1;
            cell35A.FixedHeight = 18.0f;
            table3.AddCell(cell31A);
            table3.AddCell(cell32A);
            table3.AddCell(cell33A);
            table3.AddCell(cell34A);
            table3.AddCell(cell35A);
            if (oDetalle.Count > EN_Constante.g_const_0)
            {
                foreach (var item in oDetalle.ToList())
                {
                    var itemx = new Chunk(item.Item.ToString(), FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.NORMAL));
                    var Comprobante = new Chunk((string)Operators.ConcatenateObject(Operators.ConcatenateObject(item.Tipodocumentoid, " - "), item.DescTipodocumento), FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.NORMAL));
                    var Serie = new Chunk(item.Serie, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.NORMAL));
                    var Numero = new Chunk(item.Numero, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.NORMAL));
                    var Motivo = new Chunk(item.MotivoBaja, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.NORMAL));
                    var cell31B = new PdfPCell(new Phrase(itemx));
                    cell31B.HorizontalAlignment = 1;
                    // cell31B.FixedHeight = 18.0F
                    var cell32B = new PdfPCell(new Phrase(Comprobante));
                    cell32B.HorizontalAlignment = 0;
                    // cell32B.FixedHeight = 18.0F
                    var cell33B = new PdfPCell(new Phrase(Serie));
                    cell33B.HorizontalAlignment = 1;
                    // cell33B.FixedHeight = 18.0F
                    var cell34B = new PdfPCell(new Phrase(Numero));
                    cell34B.HorizontalAlignment = 1;
                    // cell34B.FixedHeight = 18.0F
                    var cell35B = new PdfPCell(new Phrase(Motivo));
                    cell35B.HorizontalAlignment = 0;
                    // cell35B.FixedHeight = 18.0F

                    table3.AddCell(cell31B);
                    table3.AddCell(cell32B);
                    table3.AddCell(cell33B);
                    table3.AddCell(cell34B);
                    table3.AddCell(cell35B);
                }
            }
            // Tamaño de tabla
            table3.WidthPercentage = 100.0f;
            table3.HorizontalAlignment = 0;
            doc.Add(table3);

            // Texto Observacion SUNAT
            string textoSunat="";
            // string textoSunat = "Observaciones de SUNAT";
            var tTextoSunat = new Chunk(textoSunat, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            // // Texto Respuesta SUNAT
            string textoCDR = EN_Constante.g_const_vacio;
            // if (situacion == EN_Constante.g_const_situacion_pendiente || string.IsNullOrEmpty(situacion))
            // {
            //     textoCDR = "El comunicado de bajas esta pendiente de envío a SUNAT.";
            // }
            // else if (situacion == EN_Constante.g_const_situacion_aceptado)
            // {
            //     textoCDR = mensaje;
            // }
            // else if (situacion == EN_Constante.g_const_situacion_rechazado || situacion == EN_Constante.g_const_situacion_error)
            // {
            //     textoCDR = mensaje;
            // }
            // else
            // {
            //     textoCDR = "-";
            // }

            var tTextoCDR = new Chunk(textoCDR, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            var table4A = new PdfPTable(1);
            var intTblWidth4A = new int[] { (int)Math.Round(90.0) };
            table4A.SetWidths(intTblWidth4A);
            var cell4A1 = new PdfPCell(new Phrase(tTextoSunat));
            cell4A1.HorizontalAlignment = 0;
            cell4A1.VerticalAlignment = 1;
            cell4A1.BorderWidthTop = 0.0f;
            cell4A1.BorderWidthBottom = 0.0f;
            cell4A1.BorderWidthLeft = 0.0f;
            cell4A1.BorderWidthRight = 0.0f;
            cell4A1.FixedHeight = 14.0f;
            var cell4A2 = new PdfPCell(new Phrase(tTextoCDR));
            cell4A2.HorizontalAlignment = 0;
            cell4A2.VerticalAlignment = 1;
            cell4A2.BorderWidthTop = 0.0f;
            cell4A2.BorderWidthBottom = 0.0f;
            cell4A2.BorderWidthLeft = 0.0f;
            cell4A2.BorderWidthRight = 0.0f;
            cell4A2.FixedHeight = 14.0f;
            table4A.AddCell(cell4A1);
            table4A.AddCell(cell4A2);
            table4A.WidthPercentage = 100.0f;
            table4A.HorizontalAlignment = 0;
            doc.Add(table4A);

            // Código Hash
            string textoHash = "Código Hash: " + valorResumen;
            var textValorResumen = new Chunk(textoHash, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            doc.Add(new Paragraph(textValorResumen));

            // Leyenda de Representacion Impresa
            string textoleyenda = "Representación Impresa de Comunicado de Baja.";
            if (oEmpresa.NroResolucion.Trim() != "")
            {
                textoleyenda = textoleyenda + " Autorizado mediante Resolución N° " + oEmpresa.NroResolucion;
            }

            var leyenda = new Chunk(textoleyenda, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            doc.Add(new Paragraph(leyenda));
            doc.Dispose();
            doc.Close();
            file.Dispose();
            file.Close();



             //COPIAMOS ARCHIVO A S3 Y ELIMINAMOS DE TEMPORAL
            string nombrePDF= oBaja.Nrodocumento.Trim() + "-" + oBaja.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oBaja.FechaComunicacion), EN_Constante.g_const_formfecha_yyyyMMdd) + "-" + oBaja.Correlativo;
            AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oBaja.Nrodocumento,rutabase,EN_Constante.g_const_rutaSufijo_pdf,nombrePDF,EN_Constante.g_const_extension_pdf);


        }
    }


    // OTROS CPE



    public void GenerarPDFRetencionPercepcion(EN_Empresa oEmpresa, EN_OtrosCpe oDocumento, string montoenletras, string monedaenletras, List<EN_DetalleOtrosCpe> oDetalle, EN_ComprobanteSunat oCompSunat, string valorResumen, string firma, string rutabase, string ruta, string situacion = "", string mensaje = "")
    {
    string rutalogo = string.Format("{0}/{1}/", rutabase, oDocumento.nrodocempresa);
    Document doc = new Document(PageSize.A4, 20.0F, 20.0F, 20.0F, 20.0F);
    string filename = ruta + EN_Constante.g_const_rutaSufijo_pdf+ oEmpresa.Nrodocumento.Trim() + "-" + oDocumento.tipodocumento_id.Trim() + "-" + oDocumento.serie.Trim() + "-" + oDocumento.numero.Trim() + EN_Constante.g_const_extension_pdf;

    string logo = rutalogo + oEmpresa.Imagen;

    if (filename.Trim() != "")
    {
        FileStream file = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
        PdfWriter.GetInstance(doc, file);
        doc.Open();

        // Para el logo
        dynamic imagen =null;

        if (File.Exists(logo))
        {
            imagen = iTextSharp.text.Image.GetInstance(logo);
            imagen.BorderWidth = 0;
            imagen.Alignment = Element.ALIGN_LEFT;
            imagen.ScaleAbsolute(100, 100);
        }
        else
        {
            imagen=new Phrase(EN_Constante.g_const_vacio);
        }

        // Tabla para Tipo de documento - RUC - Serie y Numero de documento
        PdfPTable table1 = new PdfPTable(1);
        string textoTipoDoc = "";
        string textoTipoDoc2 = "";
        if (oDocumento.tipodocumento_id.Trim() == oCompSunat.IdCR)
        {
            textoTipoDoc = "COMPROBANTE RETENCIÓN";
            textoTipoDoc2 =  EN_Constante.g_const_electronica;
        }
        else if (oDocumento.tipodocumento_id.Trim() == oCompSunat.IdCP)
        {
            textoTipoDoc = "COMPROBANTE PERCEPCIÓN";
            textoTipoDoc2 =  EN_Constante.g_const_electronica;
        }
        Chunk tipodocumento = new Chunk(textoTipoDoc, FontFactory.GetFont(BaseFont.COURIER, 12, iTextSharp.text.Font.BOLD));
        Chunk tipodocumento2 = new Chunk(textoTipoDoc2, FontFactory.GetFont(BaseFont.COURIER, 12, iTextSharp.text.Font.BOLD));
        Chunk ruc = new Chunk("RUC: " + oEmpresa.Nrodocumento, FontFactory.GetFont(BaseFont.COURIER, 12, iTextSharp.text.Font.NORMAL));
        Chunk nrodocumento = new Chunk(oDocumento.serie + "-" + oDocumento.numero, FontFactory.GetFont(BaseFont.COURIER, 12, iTextSharp.text.Font.NORMAL));
        // Alineacion 0=Left, 1=Centre, 2=Right
        PdfPCell cell11 = new PdfPCell(new Phrase(tipodocumento));
        cell11.HorizontalAlignment = 1;
        cell11.BorderWidthBottom = 0.0F;
        cell11.FixedHeight = 18.0F;
        PdfPCell cell14 = new PdfPCell(new Phrase(tipodocumento2));
        cell14.HorizontalAlignment = 1;
        cell14.BorderWidthBottom = 0.0F;
        cell14.BorderWidthTop = 0.0F;
        cell14.FixedHeight = 18.0F;
        PdfPCell cell12 = new PdfPCell(new Phrase(ruc));
        cell12.HorizontalAlignment = 1;
        cell12.BorderWidthBottom = 0.0F;
        cell12.BorderWidthTop = 0.0F;
        cell12.FixedHeight = 18.0F;
        PdfPCell cell13 = new PdfPCell(new Phrase(nrodocumento));
        cell13.HorizontalAlignment = 1;
        cell13.BorderWidthTop = 0.0F;
        cell13.FixedHeight = 18.0F;

        table1.AddCell(cell11);
        table1.AddCell(cell14);
        table1.AddCell(cell12);
        table1.AddCell(cell13);
        // Tamaño de tabla
        table1.WidthPercentage = 55.0F;
        table1.HorizontalAlignment = 2;

        var outerTable = new PdfPTable(2);
        var outTblWidth2 = new int[] { (int)Math.Round(65.0), (int)Math.Round(35.0) };
        outerTable.SetWidths(outTblWidth2);
        PdfPCell c1 = new PdfPCell(imagen);
        PdfPCell c2 = new PdfPCell(table1);
        c1.BorderWidthBottom = 0.0F;
        c1.BorderWidthTop = 0.0F;
        c1.BorderWidthLeft = 0.0F;
        c1.BorderWidthRight = 0.0F;
        c2.BorderWidthBottom = 0.0F;
        c2.BorderWidthTop = 0.0F;
        c2.BorderWidthLeft = 0.0F;
        c2.BorderWidthRight = 0.0F;
        c1.HorizontalAlignment = 0;
        c2.HorizontalAlignment = 2;
        outerTable.AddCell(c1);
        outerTable.AddCell(c2);
        outerTable.WidthPercentage = 100.0F;
        outerTable.HorizontalAlignment = 0;
        doc.Add(outerTable);

        // Mostramos los datos de la empresa
        Chunk razonsocial = new Chunk(oEmpresa.RazonSocial, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
        Chunk nombrecomercial = new Chunk(oEmpresa.Nomcomercial, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));
        Chunk direccion = new Chunk(oEmpresa.Direccion + " " + oEmpresa.Sector, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
        Chunk ubicacion = new Chunk(oEmpresa.Distrito + " - " + oEmpresa.Provincia + " - " + oEmpresa.Departamento + " - PERÚ", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
        doc.Add(new Paragraph(razonsocial));
        doc.Add(new Paragraph(nombrecomercial));
        doc.Add(new Paragraph(direccion));
        doc.Add(new Paragraph(ubicacion));
        // doc.Add(New Paragraph(" "))

        // Tabla para datos del cabecera

        PdfPTable table2A = new PdfPTable(4);
        var intTblWidth2A = new int[] { (int)Math.Round(15.0), (int)Math.Round(50.0), (int)Math.Round(15.0), (int)Math.Round(20.0) };
        table2A.SetWidths(intTblWidth2A);

        Chunk tnombreclienteB = new Chunk("Nombre:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tnombreclienteF = new Chunk("Razón Social:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

        Chunk tnrodocclienteB = new Chunk("Doc.Identidad:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tnrodocclienteF = new Chunk("RUC:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

        Chunk tdireccioncliente = new Chunk("Direccion:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tfechaemision = new Chunk("Fecha Emisión:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

        Chunk tTasa = new Chunk("Tasa:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

        Chunk nombrecliente = new Chunk(oDocumento.persona_descripcion, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

        Chunk nrodocclienteB = new Chunk(oDocumento.persona_tipodoc + " " + oDocumento.persona_nrodoc, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
        Chunk nrodocclienteF = new Chunk(oDocumento.persona_nrodoc, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

        Chunk direccioncliente = new Chunk(oDocumento.persona_direccion + " " + oDocumento.persona_urbanizacion + "-" + oDocumento.persona_distrito + "-" + oDocumento.persona_provincia + "-" + oDocumento.persona_departamento, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
        Chunk fechaemision = new Chunk(Convert.ToDateTime(oDocumento.fechaemision).ToString("dd/MM/yyyy"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
        Chunk Tasa = new Chunk(Strings.Format(oDocumento.tasa_retper, "##,##0.00") + " %", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

        // Espacio de Tabla
        PdfPCell cell20A = new PdfPCell(new Phrase(""));
        cell20A.HorizontalAlignment = 0;
        cell20A.BorderWidthTop = 0.0F;
        cell20A.BorderWidthBottom = 0.0F;
        cell20A.BorderWidthLeft = 0.0F;
        cell20A.BorderWidthRight = 0.0F;
        cell20A.FixedHeight = 8.0F;

        PdfPCell cell20B = new PdfPCell(new Phrase(""));
        cell20B.HorizontalAlignment = 0;
        cell20B.BorderWidthTop = 0.0F;
        cell20B.BorderWidthBottom = 0.0F;
        cell20B.BorderWidthLeft = 0.0F;
        cell20B.BorderWidthRight = 0.0F;
        cell20B.FixedHeight = 8.0F;

        PdfPCell cell20C = new PdfPCell(new Phrase(""));
        cell20C.HorizontalAlignment = 0;
        cell20C.BorderWidthTop = 0.0F;
        cell20C.BorderWidthBottom = 0.0F;
        cell20C.BorderWidthLeft = 0.0F;
        cell20C.BorderWidthRight = 0.0F;
        cell20C.FixedHeight = 8.0F;

        PdfPCell cell20D = new PdfPCell(new Phrase(""));
        cell20D.HorizontalAlignment = 0;
        cell20D.BorderWidthTop = 0.0F;
        cell20D.BorderWidthBottom = 0.0F;
        cell20D.BorderWidthLeft = 0.0F;
        cell20D.BorderWidthRight = 0.0F;
        cell20D.FixedHeight = 8.0F;

        // Nombre del cliente
        PdfPCell cell22AB = new PdfPCell(new Phrase(tnombreclienteB));
        cell22AB.HorizontalAlignment = 0;
        cell22AB.BorderWidthTop = 0.0F;
        cell22AB.BorderWidthBottom = 0.0F;
        cell22AB.BorderWidthLeft = 0.0F;
        cell22AB.BorderWidthRight = 0.0F;
        // cell22AB.FixedHeight = 14.0F

        PdfPCell cell22AF = new PdfPCell(new Phrase(tnombreclienteF));
        cell22AF.HorizontalAlignment = 0;
        cell22AF.BorderWidthTop = 0.0F;
        cell22AF.BorderWidthBottom = 0.0F;
        cell22AF.BorderWidthLeft = 0.0F;
        cell22AF.BorderWidthRight = 0.0F;
        // cell22AF.FixedHeight = 14.0F

        PdfPCell cell22B = new PdfPCell(new Phrase(nombrecliente));
        cell22B.HorizontalAlignment = 0;
        cell22B.BorderWidthTop = 0.0F;
        cell22B.BorderWidthBottom = 0.0F;
        cell22B.BorderWidthLeft = 0.0F;
        cell22B.BorderWidthRight = 0.0F;
        // cell22B.FixedHeight = 14.0F

        // Nro de Documento del cliente
        PdfPCell cell23AB = new PdfPCell(new Phrase(tnrodocclienteB));
        cell23AB.HorizontalAlignment = 0;
        cell23AB.BorderWidthTop = 0.0F;
        cell23AB.BorderWidthBottom = 0.0F;
        cell23AB.BorderWidthLeft = 0.0F;
        cell23AB.BorderWidthRight = 0.0F;
        // cell23AB.FixedHeight = 14.0F

        PdfPCell cell23AF = new PdfPCell(new Phrase(tnrodocclienteF));
        cell23AF.HorizontalAlignment = 0;
        cell23AF.BorderWidthTop = 0.0F;
        cell23AF.BorderWidthBottom = 0.0F;
        cell23AF.BorderWidthLeft = 0.0F;
        cell23AF.BorderWidthRight = 0.0F;
        // cell23AF.FixedHeight = 14.0F

        PdfPCell cell23BB = new PdfPCell(new Phrase(nrodocclienteB));
        cell23BB.HorizontalAlignment = 0;
        cell23BB.BorderWidthTop = 0.0F;
        cell23BB.BorderWidthBottom = 0.0F;
        cell23BB.BorderWidthLeft = 0.0F;
        cell23BB.BorderWidthRight = 0.0F;
        // cell23BB.FixedHeight = 14.0F

        PdfPCell cell23BF = new PdfPCell(new Phrase(nrodocclienteF));
        cell23BF.HorizontalAlignment = 0;
        cell23BF.BorderWidthTop = 0.0F;
        cell23BF.BorderWidthBottom = 0.0F;
        cell23BF.BorderWidthLeft = 0.0F;
        cell23BF.BorderWidthRight = 0.0F;
        // cell23BF.FixedHeight = 14.0F

        // Direccion del cliente
        PdfPCell cell24A = new PdfPCell(new Phrase(tdireccioncliente));
        cell24A.HorizontalAlignment = 0;
        cell24A.BorderWidthTop = 0.0F;
        cell24A.BorderWidthBottom = 0.0F;
        cell24A.BorderWidthLeft = 0.0F;
        cell24A.BorderWidthRight = 0.0F;
        // cell24A.FixedHeight = 14.0F

        PdfPCell cell24B = new PdfPCell(new Phrase(direccioncliente));
        cell24B.HorizontalAlignment = 0;
        cell24B.BorderWidthTop = 0.0F;
        cell24B.BorderWidthBottom = 0.0F;
        cell24B.BorderWidthLeft = 0.0F;
        cell24B.BorderWidthRight = 0.0F;
        // cell24B.FixedHeight = 14.0F

        // Fecha de emision
        PdfPCell cell21A = new PdfPCell(new Phrase(tfechaemision));
        cell21A.HorizontalAlignment = 0;
        cell21A.BorderWidthTop = 0.0F;
        cell21A.BorderWidthBottom = 0.0F;
        cell21A.BorderWidthLeft = 0.0F;
        cell21A.BorderWidthRight = 0.0F;
        // cell21A.FixedHeight = 14.0F

        PdfPCell cell21B = new PdfPCell(new Phrase(fechaemision));
        cell21B.HorizontalAlignment = 0;
        cell21B.BorderWidthTop = 0.0F;
        cell21B.BorderWidthBottom = 0.0F;
        cell21B.BorderWidthLeft = 0.0F;
        cell21B.BorderWidthRight = 0.0F;
        // cell21B.FixedHeight = 14.0F

        // Tasa
        PdfPCell cell25A = new PdfPCell(new Phrase(tTasa));
        cell25A.HorizontalAlignment = 0;
        cell25A.BorderWidthTop = 0.0F;
        cell25A.BorderWidthBottom = 0.0F;
        cell25A.BorderWidthLeft = 0.0F;
        cell25A.BorderWidthRight = 0.0F;
        cell25A.FixedHeight = 14.0F;

        PdfPCell cell25B = new PdfPCell(new Phrase(Tasa));
        cell25B.HorizontalAlignment = 0;
        cell25B.BorderWidthTop = 0.0F;
        cell25B.BorderWidthBottom = 0.0F;
        cell25B.BorderWidthLeft = 0.0F;
        cell25B.BorderWidthRight = 0.0F;
        cell25B.FixedHeight = 14.0F;

        // Vacio
        PdfPCell cell26A = new PdfPCell(new Phrase(""));
        cell26A.HorizontalAlignment = 0;
        cell26A.BorderWidthTop = 0.0F;
        cell26A.BorderWidthBottom = 0.0F;
        cell26A.BorderWidthLeft = 0.0F;
        cell26A.BorderWidthRight = 0.0F;
        cell26A.FixedHeight = 14.0F;

        PdfPCell cell26B = new PdfPCell(new Phrase(""));
        cell26B.HorizontalAlignment = 0;
        cell26B.BorderWidthTop = 0.0F;
        cell26B.BorderWidthBottom = 0.0F;
        cell26B.BorderWidthLeft = 0.0F;
        cell26B.BorderWidthRight = 0.0F;
        cell26B.FixedHeight = 14.0F;

        // Espacio de Tabla
        PdfPCell cell27A = new PdfPCell(new Phrase(""));
        cell27A.HorizontalAlignment = 0;
        cell27A.BorderWidthTop = 0.0F;
        cell27A.BorderWidthBottom = 0.0F;
        cell27A.BorderWidthLeft = 0.0F;
        cell27A.BorderWidthRight = 0.0F;
        cell27A.FixedHeight = 8.0F;

        PdfPCell cell27B = new PdfPCell(new Phrase(""));
        cell27B.HorizontalAlignment = 0;
        cell27B.BorderWidthTop = 0.0F;
        cell27B.BorderWidthBottom = 0.0F;
        cell27B.BorderWidthLeft = 0.0F;
        cell27B.BorderWidthRight = 0.0F;
        cell27B.FixedHeight = 8.0F;

        PdfPCell cell28A = new PdfPCell(new Phrase(""));
        cell28A.HorizontalAlignment = 0;
        cell28A.BorderWidthTop = 0.0F;
        cell28A.BorderWidthBottom = 0.0F;
        cell28A.BorderWidthLeft = 0.0F;
        cell28A.BorderWidthRight = 0.0F;
        cell28A.FixedHeight = 8.0F;

        PdfPCell cell28B = new PdfPCell(new Phrase(""));
        cell28B.HorizontalAlignment = 0;
        cell28B.BorderWidthTop = 0.0F;
        cell28B.BorderWidthBottom = 0.0F;
        cell28B.BorderWidthLeft = 0.0F;
        cell28B.BorderWidthRight = 0.0F;
        cell28B.FixedHeight = 8.0F;


        // -----Separador
        table2A.AddCell(cell20A);
        table2A.AddCell(cell20B);
        table2A.AddCell(cell20C);
        table2A.AddCell(cell20D);

        // ---Nombre o Razon Social
        if (oDocumento.persona_tipodoc == "RUC")
            table2A.AddCell(cell22AF);
        else
            table2A.AddCell(cell22AB);
        table2A.AddCell(cell22B);

        // ---Documento del Cliente
        if (oDocumento.persona_tipodoc == "RUC")
            table2A.AddCell(cell23AF);
        else
            table2A.AddCell(cell23AB);
        if (oDocumento.persona_tipodoc == "RUC")
            table2A.AddCell(cell23BF);
        else
            table2A.AddCell(cell23BB);

        // ---Direccion
        table2A.AddCell(cell24A);
        table2A.AddCell(cell24B);

        // ---Fecha Emision
        table2A.AddCell(cell21A);
        table2A.AddCell(cell21B);

        // ---Tasa
        table2A.AddCell(cell25A);
        table2A.AddCell(cell25B);

        // ---Vacío
        table2A.AddCell(cell26A);
        table2A.AddCell(cell26B);

        // -----Separador
        table2A.AddCell(cell27A);
        table2A.AddCell(cell27B);
        table2A.AddCell(cell28A);
        table2A.AddCell(cell28B);

        // Tamaño de tabla
        table2A.WidthPercentage = 100.0F;
        table2A.HorizontalAlignment = 0;
        doc.Add(table2A);


        // Tabla para el detalle del comprobante
        PdfPTable table3 = new PdfPTable(10);
        int[] intTblWidth3 = new int[] { (int)Math.Round(11.0), (int)Math.Round(14.0), 
        (int)Math.Round(9.0), (int)Math.Round(9.0), (int)Math.Round(8.0), (int)Math.Round(8.0), 
        (int)Math.Round(10.0), (int)Math.Round(10.0), (int)Math.Round(10.0), (int)Math.Round(10.0) }; 
        
        table3.SetWidths(intTblWidth3);

        string textoImporteSinRet;
        string textoMontoPago;
        string textoImporRet;
        if (oDocumento.tipodocumento_id.Trim() == oCompSunat.IdCR)
        {
            textoImporteSinRet = "Importe de Pago sin Retención";
            textoImporRet = "Importe Retenido S/";
            textoMontoPago = "Importe Total a Pagar S/";
        }
        else if (oDocumento.tipodocumento_id.Trim() == oCompSunat.IdCP)
        {
            textoImporteSinRet = "Importe de Pago sin Percepción";
            textoImporRet = "Importe Percibido S/";
            textoMontoPago = "Importe Total a Cobrar S/";
        }
        else
        {
            textoImporteSinRet = "";
            textoMontoPago = "";
            textoImporRet = "";
        }

        Chunk tTipo = new Chunk("Tipo", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tSerie = new Chunk("Documento", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tFechaEmi = new Chunk("Fecha Emisión", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tFechaPago = new Chunk("Fecha Pago", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tNroPago = new Chunk("Nro Pago", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tMonOrig = new Chunk("Moneda Origen", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tImporOrig = new Chunk("Importe Operación Origen", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tImporSin = new Chunk(textoImporteSinRet, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        // Dim tTipoCambio As New Chunk("Tipo de Cambio", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD))
        Chunk tImporRetPer = new Chunk(textoImporRet, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tImporPagCob = new Chunk(textoMontoPago, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

        PdfPCell cell31A = new PdfPCell(new Phrase(tTipo));
        cell31A.HorizontalAlignment = 0;
        cell31A.FixedHeight = 25.0F;
        PdfPCell cell32A = new PdfPCell(new Phrase(tSerie));
        cell32A.HorizontalAlignment = 1;
        cell32A.FixedHeight = 25.0F;
        PdfPCell cell33A = new PdfPCell(new Phrase(tFechaEmi));
        cell33A.HorizontalAlignment = 1;
        cell33A.FixedHeight = 25.0F;
        PdfPCell cell34A = new PdfPCell(new Phrase(tFechaPago));
        cell34A.HorizontalAlignment = 1;
        cell34A.FixedHeight = 25.0F;
        PdfPCell cell35A = new PdfPCell(new Phrase(tNroPago));
        cell35A.HorizontalAlignment = 1;
        cell35A.FixedHeight = 25.0F;
        PdfPCell cell36A = new PdfPCell(new Phrase(tMonOrig));
        cell36A.HorizontalAlignment = 1;
        cell36A.FixedHeight = 25.0F;
        PdfPCell cell37A = new PdfPCell(new Phrase(tImporOrig));
        cell37A.HorizontalAlignment = 1;
        cell37A.FixedHeight = 25.0F;
        PdfPCell cell38A = new PdfPCell(new Phrase(tImporSin));
        cell38A.HorizontalAlignment = 1;
        cell38A.FixedHeight = 25.0F;
        // Dim cell39A As New PdfPCell(New Phrase(tTipoCambio))
        // cell39A.HorizontalAlignment = 1
        // cell39A.FixedHeight = 25.0F
        PdfPCell cell3AA = new PdfPCell(new Phrase(tImporRetPer));
        cell3AA.HorizontalAlignment = 1;
        cell3AA.FixedHeight = 25.0F;
        PdfPCell cell3BA = new PdfPCell(new Phrase(tImporPagCob));
        cell3BA.HorizontalAlignment = 1;
        cell3BA.FixedHeight = 25.0F;

        table3.AddCell(cell31A);
        table3.AddCell(cell32A);
        table3.AddCell(cell33A);
        table3.AddCell(cell34A);
        table3.AddCell(cell35A);
        table3.AddCell(cell36A);
        table3.AddCell(cell37A);
        table3.AddCell(cell38A);
        // table3.AddCell(cell39A)
        table3.AddCell(cell3AA);
        table3.AddCell(cell3BA);

        if (oDetalle.Count > 0)
        {
            foreach (var item in oDetalle.ToList())
            {
                string tipodocdesc;
                if (item.docrelac_tipodoc_id == oCompSunat.IdFC)
                    tipodocdesc = "Factura";
                else if (item.docrelac_tipodoc_id == oCompSunat.IdBV)
                    tipodocdesc = "Boleta de Venta";
                else if (item.docrelac_tipodoc_id == oCompSunat.IdNC)
                    tipodocdesc = "Nota de Crédito";
                else if (item.docrelac_tipodoc_id == oCompSunat.IdND)
                    tipodocdesc = "Nota de Débito";
                else
                    tipodocdesc = "Otro Comprobante";

                string monedalet;
                if (item.docrelac_moneda == "PEN")
                    monedalet = "Soles";
                else if (item.docrelac_moneda == "USD")
                    monedalet = "Dólares Americanos";
                else
                    monedalet = item.docrelac_moneda;

                Chunk Tipo = new Chunk(tipodocdesc, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                Chunk Serie = new Chunk(item.docrelac_numerodoc, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                Chunk FecEmi = new Chunk(Convert.ToDateTime(item.docrelac_fechaemision).ToString("dd/MM/yyyy"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                Chunk FecPago = new Chunk(Convert.ToDateTime(item.pagcob_fecha).ToString("dd/MM/yyyy"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                Chunk NroPago = new Chunk(item.pagcob_numero, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                Chunk MonOrigen = new Chunk(monedalet, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                Chunk ImporOrig = new Chunk(Strings.Format(item.docrelac_importetotal, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                Chunk ImporSin = new Chunk(Strings.Format(item.pagcob_importe, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                // Dim TipoCambio As New Chunk(Format(item.tipocambio_factor, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL))
                Chunk ImporteRetPer = new Chunk(Strings.Format(item.retper_importe, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                Chunk ImportePagCob = new Chunk(Strings.Format(item.retper_importe_pagcob, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                PdfPCell cell31B = new PdfPCell(new Phrase(Tipo));
                cell31B.HorizontalAlignment = 0;
                cell31B.FixedHeight = 14.0F;
                PdfPCell cell32B = new PdfPCell(new Phrase(Serie));
                cell32B.HorizontalAlignment = 1;
                cell32B.FixedHeight = 14.0F;
                PdfPCell cell33B = new PdfPCell(new Phrase(FecEmi));
                cell33B.HorizontalAlignment = 1;
                cell33B.FixedHeight = 14.0F;
                PdfPCell cell34B = new PdfPCell(new Phrase(FecPago));
                cell34B.HorizontalAlignment = 1;
                cell34B.FixedHeight = 14.0F;
                PdfPCell cell35B = new PdfPCell(new Phrase(NroPago));
                cell35B.HorizontalAlignment = 1;
                cell35B.FixedHeight = 14.0F;
                PdfPCell cell36B = new PdfPCell(new Phrase(MonOrigen));
                cell36B.HorizontalAlignment = 1;
                cell36B.FixedHeight = 14.0F;
                PdfPCell cell37B = new PdfPCell(new Phrase(ImporOrig));
                cell37B.HorizontalAlignment = 2;
                cell37B.FixedHeight = 14.0F;
                PdfPCell cell38B = new PdfPCell(new Phrase(ImporSin));
                cell38B.HorizontalAlignment = 2;
                cell38B.FixedHeight = 14.0F;
                // Dim cell39B As New PdfPCell(New Phrase(TipoCambio))
                // cell39B.HorizontalAlignment = 2
                // cell39B.FixedHeight = 14.0F
                PdfPCell cell3AB = new PdfPCell(new Phrase(ImporteRetPer));
                cell3AB.HorizontalAlignment = 2;
                cell3AB.FixedHeight = 14.0F;
                PdfPCell cell3BB = new PdfPCell(new Phrase(ImportePagCob));
                cell3BB.HorizontalAlignment = 2;
                cell3BB.FixedHeight = 14.0F;

                table3.AddCell(cell31B);
                table3.AddCell(cell32B);
                table3.AddCell(cell33B);
                table3.AddCell(cell34B);
                table3.AddCell(cell35B);
                table3.AddCell(cell36B);
                table3.AddCell(cell37B);
                table3.AddCell(cell38B);
                // table3.AddCell(cell39B)
                table3.AddCell(cell3AB);
                table3.AddCell(cell3BB);
            }
        }
        // Tamaño de tabla
        table3.WidthPercentage = 100.0F;
        table3.HorizontalAlignment = 0;
        doc.Add(table3);

        // Tabla Totales
        PdfPTable table4 = new PdfPTable(10);
        int[] intTblWidth4 = 
         new int[] { (int)Math.Round(11.0), (int)Math.Round(14.0), 
        (int)Math.Round(9.0), (int)Math.Round(9.0), (int)Math.Round(8.0), (int)Math.Round(8.0), 
        (int)Math.Round(10.0), (int)Math.Round(10.0), (int)Math.Round(10.0), (int)Math.Round(10.0) }; 
         
        table4.SetWidths(intTblWidth4);

        Chunk tT1 = new Chunk("", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tT2 = new Chunk("", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tT3 = new Chunk("", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tT4 = new Chunk("", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tT5 = new Chunk("", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        // Dim tT6 As New Chunk("", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD))
        Chunk tT7 = new Chunk("", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tT9 = new Chunk("Importe Total S/ ", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tTA = new Chunk(Strings.Format(oDocumento.importe_retper, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
        Chunk tTB = new Chunk(Strings.Format(oDocumento.importe_pagcob, "##,##0.00"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

        PdfPCell cell41A = new PdfPCell(new Phrase(tT1));
        cell41A.HorizontalAlignment = 1;
        cell41A.BorderWidthTop = 0.0F;
        cell41A.BorderWidthBottom = 0.0F;
        cell41A.BorderWidthLeft = 0.0F;
        cell41A.BorderWidthRight = 0.0F;
        cell41A.FixedHeight = 16.0F;
        PdfPCell cell42A = new PdfPCell(new Phrase(tT2));
        cell42A.HorizontalAlignment = 1;
        cell42A.BorderWidthTop = 0.0F;
        cell42A.BorderWidthBottom = 0.0F;
        cell42A.BorderWidthLeft = 0.0F;
        cell42A.BorderWidthRight = 0.0F;
        cell42A.FixedHeight = 16.0F;
        PdfPCell cell43A = new PdfPCell(new Phrase(tT3));
        cell43A.HorizontalAlignment = 1;
        cell43A.BorderWidthTop = 0.0F;
        cell43A.BorderWidthBottom = 0.0F;
        cell43A.BorderWidthLeft = 0.0F;
        cell43A.BorderWidthRight = 0.0F;
        cell43A.FixedHeight = 16.0F;
        PdfPCell cell44A = new PdfPCell(new Phrase(tT4));
        cell44A.HorizontalAlignment = 1;
        cell44A.BorderWidthTop = 0.0F;
        cell44A.BorderWidthBottom = 0.0F;
        cell44A.BorderWidthLeft = 0.0F;
        cell44A.BorderWidthRight = 0.0F;
        cell44A.FixedHeight = 16.0F;
        PdfPCell cell45A = new PdfPCell(new Phrase(tT5));
        cell45A.HorizontalAlignment = 1;
        cell45A.BorderWidthTop = 0.0F;
        cell45A.BorderWidthBottom = 0.0F;
        cell45A.BorderWidthLeft = 0.0F;
        cell45A.BorderWidthRight = 0.0F;
        cell45A.FixedHeight = 16.0F;
        // Dim cell46A As New PdfPCell(New Phrase(tT6))
        // cell46A.HorizontalAlignment = 1
        // cell46A.BorderWidthTop = 0.0F
        // cell46A.BorderWidthBottom = 0.0F
        // cell46A.BorderWidthLeft = 0.0F
        // cell46A.BorderWidthRight = 0.0F
        // cell46A.FixedHeight = 16.0F
        PdfPCell cell47A = new PdfPCell(new Phrase(tT7));
        cell47A.HorizontalAlignment = 1;
        cell47A.BorderWidthTop = 0.0F;
        cell47A.BorderWidthBottom = 0.0F;
        cell47A.BorderWidthLeft = 0.0F;
        cell47A.BorderWidthRight = 0.0F;
        cell47A.FixedHeight = 16.0F;
        PdfPCell cell49A = new PdfPCell(new Phrase(tT9));
        cell49A.HorizontalAlignment = 1;
        cell49A.BorderWidthTop = 0.0F;
        cell49A.BorderWidthBottom = 0.0F;
        cell49A.BorderWidthLeft = 0.0F;
        cell49A.BorderWidthRight = 0.0F;
        cell49A.FixedHeight = 16.0F;
        cell49A.Colspan = 2;

        PdfPCell cell4AA = new PdfPCell(new Phrase(tTA));
        cell4AA.HorizontalAlignment = 2;
        cell4AA.FixedHeight = 16.0F;
        PdfPCell cell4BA = new PdfPCell(new Phrase(tTB));
        cell4BA.HorizontalAlignment = 2;
        cell4BA.FixedHeight = 16.0F;

        table4.AddCell(cell41A);
        table4.AddCell(cell42A);
        table4.AddCell(cell43A);
        table4.AddCell(cell44A);
        table4.AddCell(cell45A);
        // table4.AddCell(cell46A)
        table4.AddCell(cell47A);
        table4.AddCell(cell49A);
        table4.AddCell(cell4AA);
        table4.AddCell(cell4BA);

        // Tamaño de tabla
        table4.WidthPercentage = 100.0F;
        table4.HorizontalAlignment = 0;
        doc.Add(table4);

        // Texto Observacion SUNAT - Solo Facturas
        string textoSunat = "Observaciones de SUNAT";
        Chunk tTextoSunat = new Chunk(textoSunat, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

        // Texto Respuesta SUNAT
        string textoCDR = "";
        // if (situacion == "P" | situacion == "")
        //     textoCDR = "El comprobante esta pendiente de envío a SUNAT.";
        // else if (situacion == "A")
        //     textoCDR = mensaje;
        // else if (situacion == "R" | situacion == "E")
        //     textoCDR = mensaje;
        // else if (oDocumento.situacion.Trim() == "B")
        //     textoCDR = "El comprobante ha sido revertido.";
        // else
        //     textoCDR = "-";
        Chunk tTextoCDR = new Chunk(textoCDR, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

        PdfPTable table4A = new PdfPTable(1);
        var intTblWidth4A = new int[] { (int)Math.Round(90.0) };
        table4A.SetWidths(intTblWidth4A);

        PdfPCell cell4A1 = new PdfPCell(new Phrase(tTextoSunat));
        cell4A1.HorizontalAlignment = 0;
        cell4A1.VerticalAlignment = 1;
        cell4A1.BorderWidthTop = 0.0F;
        cell4A1.BorderWidthBottom = 0.0F;
        cell4A1.BorderWidthLeft = 0.0F;
        cell4A1.BorderWidthRight = 0.0F;
        cell4A1.FixedHeight = 14.0F;

        PdfPCell cell4A2 = new PdfPCell(new Phrase(tTextoCDR));
        cell4A2.HorizontalAlignment = 0;
        cell4A2.VerticalAlignment = 1;
        cell4A2.BorderWidthTop = 0.0F;
        cell4A2.BorderWidthBottom = 0.0F;
        cell4A2.BorderWidthLeft = 0.0F;
        cell4A2.BorderWidthRight = 0.0F;
        cell4A2.FixedHeight = 14.0F;

        table4A.AddCell(cell4A1);
        table4A.AddCell(cell4A2);

        table4A.WidthPercentage = 100.0F;
        table4A.HorizontalAlignment = 0;

        doc.Add(table4A);

        doc.Add(new Paragraph(" "));

        Chunk montocomprobletras = new Chunk("SON: " + montoenletras + " " + monedaenletras, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
        doc.Add(new Paragraph(montocomprobletras));

        // Código Hash
        string textoHash = "Código Hash: " + valorResumen;
        Chunk textValorResumen = new Chunk(textoHash, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
        doc.Add(new Paragraph(textValorResumen));

        string textoleyenda = "";
        if (oDocumento.tipodocumento_id.Trim() == oCompSunat.IdCR)
            textoleyenda = "Representación Impresa del Comprobante de Retención Electrónico.";
        else if (oDocumento.tipodocumento_id.Trim() == oCompSunat.IdCP)
            textoleyenda = "Representación Impresa del Comprobante de Percepción Electrónico.";
        if (oEmpresa.NroResolucion.Trim() != "")
            textoleyenda = textoleyenda + " Autorizado mediante Resolución N° " + oEmpresa.NroResolucion;
        Chunk leyenda = new Chunk(textoleyenda, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
        doc.Add(new Paragraph(leyenda));

        doc.Dispose();
        doc.Close();
        file.Dispose();
        file.Close();


        //COPIAMOS ARCHIVO A S3 Y ELIMINAMOS DE TEMPORAL
        string nombrePDF= oEmpresa.Nrodocumento.Trim() + "-" + oDocumento.tipodocumento_id.Trim() + "-" + oDocumento.serie.Trim() + "-" + oDocumento.numero.Trim();
        AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oEmpresa.Nrodocumento.Trim(),rutabase,EN_Constante.g_const_rutaSufijo_pdf,nombrePDF,EN_Constante.g_const_extension_pdf);



    }
}



public void GenerarPDFReversion(EN_Empresa oEmpresa, EN_Reversion oReversion, List<EN_DetalleReversion> oDetalle, string ruta, EN_ComprobanteSunat oCompSunat, string valorResumen, string firma, string rutabase, string situacion = "", string mensaje = "")
{
    string rutalogo = string.Format("{0}/{1}/", rutabase, oReversion.Nrodocumento);
    Document doc = new Document(PageSize.A4.Rotate(), 35.0F, 35.0F, 35.0F, 35.0F);
    string filename = ruta + EN_Constante.g_const_rutaSufijo_pdf + oReversion.Nrodocumento.Trim() + "-" + oReversion.Tiporesumen.Trim() + "-" +  Strings.Format(Conversions.ToDate(oReversion.FechaComunicacion), "yyyyMMdd") + "-" + oReversion.Correlativo +  EN_Constante.g_const_extension_pdf;
;
    
    string logo = rutalogo + oEmpresa.Imagen;

    if (filename.Trim() != "")
    {
        FileStream file = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
        PdfWriter.GetInstance(doc, file);
        doc.Open();

        // Para el logo
        dynamic imagen =null;

        if (File.Exists(logo))
        {
            imagen = iTextSharp.text.Image.GetInstance(logo);
            imagen.BorderWidth = 0;
            imagen.Alignment = Element.ALIGN_LEFT;
            imagen.ScaleAbsolute(100, 100);
        }
        else
        {
            imagen=new Phrase(EN_Constante.g_const_vacio);
        }

        // Tabla para Tipo de documento - RUC - Serie y Numero de documento
        PdfPTable table1 = new PdfPTable(1);
        string textoTipoDoc = "";
        if (oReversion.Tiporesumen.Trim() == oCompSunat.IdCB)
            textoTipoDoc = "RESUMEN DE REVERSIONES";
        Chunk tipodocumento = new Chunk(textoTipoDoc, FontFactory.GetFont(BaseFont.COURIER, 14, iTextSharp.text.Font.BOLD));
        Chunk ruc = new Chunk("RUC: " + oEmpresa.Nrodocumento, FontFactory.GetFont(BaseFont.COURIER, 11, iTextSharp.text.Font.NORMAL));
        Chunk nrodocumento = new Chunk(oReversion.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oReversion.FechaComunicacion), "yyyyMMdd")
                                + "-" + oReversion.Correlativo, FontFactory.GetFont(BaseFont.COURIER, 11, iTextSharp.text.Font.NORMAL));
        // Alineacion 0=Left, 1=Centre, 2=Right
        PdfPCell cell11 = new PdfPCell(new Phrase(tipodocumento));
        cell11.HorizontalAlignment = 1;
        cell11.BorderWidthBottom = 0.0F;
        cell11.FixedHeight = 18.0F;
        PdfPCell cell12 = new PdfPCell(new Phrase(ruc));
        cell12.HorizontalAlignment = 1;
        cell12.BorderWidthBottom = 0.0F;
        cell12.BorderWidthTop = 0.0F;
        cell12.FixedHeight = 18.0F;
        PdfPCell cell13 = new PdfPCell(new Phrase(nrodocumento));
        cell13.HorizontalAlignment = 1;
        cell13.BorderWidthTop = 0.0F;
        cell13.FixedHeight = 18.0F;
        table1.AddCell(cell11);
        table1.AddCell(cell12);
        table1.AddCell(cell13);
        // Tamaño de tabla
        table1.WidthPercentage = 35.0F;
        table1.HorizontalAlignment = 2;

        var outerTable = new PdfPTable(2);
        int[] outTblWidth2 = new int[] { (int)Math.Round(65.0), (int)Math.Round(35.0) };
        outerTable.SetWidths(outTblWidth2);
        PdfPCell c1 = new PdfPCell(imagen);
        PdfPCell c2 = new PdfPCell(table1);
        c1.BorderWidthBottom = 0.0F;
        c1.BorderWidthTop = 0.0F;
        c1.BorderWidthLeft = 0.0F;
        c1.BorderWidthRight = 0.0F;
        c2.BorderWidthBottom = 0.0F;
        c2.BorderWidthTop = 0.0F;
        c2.BorderWidthLeft = 0.0F;
        c2.BorderWidthRight = 0.0F;
        c1.HorizontalAlignment = 0;
        c2.HorizontalAlignment = 2;
        outerTable.AddCell(c1);
        outerTable.AddCell(c2);
        outerTable.WidthPercentage = 100.0F;
        outerTable.HorizontalAlignment = 0;
        doc.Add(outerTable);

        // Mostramos los datos de la empresa
        Chunk razonsocial = new Chunk(oEmpresa.RazonSocial, FontFactory.GetFont(BaseFont.COURIER, 14, iTextSharp.text.Font.BOLD));
        Chunk direccion = new Chunk(oEmpresa.Direccion + " " + oEmpresa.Sector, FontFactory.GetFont(BaseFont.COURIER, 10, iTextSharp.text.Font.NORMAL));
        Chunk ubicacion = new Chunk(oEmpresa.Departamento + " " + oEmpresa.Provincia + " " + oEmpresa.Distrito, FontFactory.GetFont(BaseFont.COURIER, 10, iTextSharp.text.Font.NORMAL));
        Chunk fechacomunicacion = new Chunk("Fecha Reversion: " +
         Strings.Format(Conversions.ToDate(oReversion.FechaComunicacion), "dd/MM/yyyy") 
        , 
        FontFactory.GetFont(BaseFont.COURIER, 10, iTextSharp.text.Font.NORMAL));
        Chunk fechaemision = new Chunk("Fecha Emisión Documentos: " + Strings.Format(Conversions.ToDate(oReversion.FechaDocumento), "dd/MM/yyyy") , FontFactory.GetFont(BaseFont.COURIER, 10, iTextSharp.text.Font.NORMAL));
        doc.Add(new Paragraph(razonsocial));
        doc.Add(new Paragraph(direccion));
        doc.Add(new Paragraph(ubicacion));
        doc.Add(new Paragraph(fechacomunicacion));
        doc.Add(new Paragraph(fechaemision));
        doc.Add(new Paragraph(" "));

        // Tabla para el detalle de la baja
        PdfPTable table3 = new PdfPTable(5);
        int[] intTblWidth3 = new int[] { (int)Math.Round(10.0), (int)Math.Round(30.0), (int)Math.Round(10.0), (int)Math.Round(15.0), (int)Math.Round(35.0) }; 
        
        table3.SetWidths(intTblWidth3);

        Chunk tItem = new Chunk("Item", FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
        Chunk tComprobante = new Chunk("Comprobante", FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
        Chunk tSerie = new Chunk("Serie", FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
        Chunk tNumero = new Chunk("Numero", FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
        Chunk tMotivo = new Chunk("Motivo", FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));

        PdfPCell cell31A = new PdfPCell(new Phrase(tItem));
        cell31A.HorizontalAlignment = 1;
        cell31A.FixedHeight = 18.0F;
        PdfPCell cell32A = new PdfPCell(new Phrase(tComprobante));
        cell32A.HorizontalAlignment = 1;
        cell32A.FixedHeight = 18.0F;
        PdfPCell cell33A = new PdfPCell(new Phrase(tSerie));
        cell33A.HorizontalAlignment = 1;
        cell33A.FixedHeight = 18.0F;
        PdfPCell cell34A = new PdfPCell(new Phrase(tNumero));
        cell34A.HorizontalAlignment = 1;
        cell34A.FixedHeight = 18.0F;
        PdfPCell cell35A = new PdfPCell(new Phrase(tMotivo));
        cell35A.HorizontalAlignment = 1;
        cell35A.FixedHeight = 18.0F;

        table3.AddCell(cell31A);
        table3.AddCell(cell32A);
        table3.AddCell(cell33A);
        table3.AddCell(cell34A);
        table3.AddCell(cell35A);

        if (oDetalle.Count > 0)
        {
            int itemReversion=0;
            foreach (var item in oDetalle.ToList())
            {
                itemReversion++;
                
                Chunk itemx = new Chunk(itemReversion.ToString(), FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.NORMAL));
                Chunk Comprobante = new Chunk(item.Tipodocumentoid + " - " + item.DescTipodocumento, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.NORMAL));
                Chunk Serie = new Chunk(item.Serie, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.NORMAL));
                Chunk Numero = new Chunk(item.Numero, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.NORMAL));
                Chunk Motivo = new Chunk(item.MotivoReversion, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.NORMAL));

                PdfPCell cell31B = new PdfPCell(new Phrase(itemx));
                cell31B.HorizontalAlignment = 1;
                // cell31B.FixedHeight = 18.0F
                PdfPCell cell32B = new PdfPCell(new Phrase(Comprobante));
                cell32B.HorizontalAlignment = 0;
                // cell32B.FixedHeight = 18.0F
                PdfPCell cell33B = new PdfPCell(new Phrase(Serie));
                cell33B.HorizontalAlignment = 1;
                // cell33B.FixedHeight = 18.0F
                PdfPCell cell34B = new PdfPCell(new Phrase(Numero));
                cell34B.HorizontalAlignment = 1;
                // cell34B.FixedHeight = 18.0F
                PdfPCell cell35B = new PdfPCell(new Phrase(Motivo));
                cell35B.HorizontalAlignment = 0;
                // cell35B.FixedHeight = 18.0F

                table3.AddCell(cell31B);
                table3.AddCell(cell32B);
                table3.AddCell(cell33B);
                table3.AddCell(cell34B);
                table3.AddCell(cell35B);
            }
        }
        // Tamaño de tabla
        table3.WidthPercentage = 100.0F;
        table3.HorizontalAlignment = 0;
        doc.Add(table3);

        // Texto Observacion SUNAT
        string textoSunat = "Observaciones de SUNAT";
        Chunk tTextoSunat = new Chunk(textoSunat, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

        // Texto Respuesta SUNAT
        string textoCDR = "";
        // if (situacion == "P" | situacion == "")
        //     textoCDR = "El resumen de reversiones esta pendiente de envío a SUNAT.";
        // else if (situacion == "A")
        //     textoCDR = mensaje;
        // else if (situacion == "R" | situacion == "E")
        //     textoCDR = mensaje;
        // else
        //     textoCDR = "-";
        Chunk tTextoCDR = new Chunk(textoCDR, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

        PdfPTable table4A = new PdfPTable(1);
        int[] intTblWidth4A =new int[] { (int)Math.Round(90.0) };
        table4A.SetWidths(intTblWidth4A);

        PdfPCell cell4A1 = new PdfPCell(new Phrase(tTextoSunat));
        cell4A1.HorizontalAlignment = 0;
        cell4A1.VerticalAlignment = 1;
        cell4A1.BorderWidthTop = 0.0F;
        cell4A1.BorderWidthBottom = 0.0F;
        cell4A1.BorderWidthLeft = 0.0F;
        cell4A1.BorderWidthRight = 0.0F;
        cell4A1.FixedHeight = 14.0F;

        PdfPCell cell4A2 = new PdfPCell(new Phrase(tTextoCDR));
        cell4A2.HorizontalAlignment = 0;
        cell4A2.VerticalAlignment = 1;
        cell4A2.BorderWidthTop = 0.0F;
        cell4A2.BorderWidthBottom = 0.0F;
        cell4A2.BorderWidthLeft = 0.0F;
        cell4A2.BorderWidthRight = 0.0F;
        cell4A2.FixedHeight = 14.0F;

        table4A.AddCell(cell4A1);
        table4A.AddCell(cell4A2);

        table4A.WidthPercentage = 100.0F;
        table4A.HorizontalAlignment = 0;

        doc.Add(table4A);

        // Código Hash
        string textoHash = "Código Hash: " + valorResumen;
        Chunk textValorResumen = new Chunk(textoHash, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
        doc.Add(new Paragraph(textValorResumen));

        // Leyenda de Representacion Impresa
        string textoleyenda = "Representación Impresa de Reversiones.";
        if (oEmpresa.NroResolucion.Trim() != "")
            textoleyenda = textoleyenda + " Autorizado mediante Resolución N° " + oEmpresa.NroResolucion;
        Chunk leyenda = new Chunk(textoleyenda, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
        doc.Add(new Paragraph(leyenda));

        doc.Dispose();
        doc.Close();
        file.Dispose();
        file.Close();



        //COPIAMOS ARCHIVO A S3 Y ELIMINAMOS DE TEMPORAL
        string nombrePDF= oReversion.Nrodocumento.Trim() + "-" + oReversion.Tiporesumen.Trim() + "-" +  Strings.Format(Conversions.ToDate(oReversion.FechaComunicacion), "yyyyMMdd") + "-" + oReversion.Correlativo;
        AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oReversion.Nrodocumento,rutabase,EN_Constante.g_const_rutaSufijo_pdf,nombrePDF,EN_Constante.g_const_extension_pdf);

    }
}
    public EN_RespuestaRegistro GenerarPDFGuiaRemision( EN_Empresa oEmpresa, EN_Guia oGuia, List<EN_DetalleGuia> oDetalle, EN_ComprobanteSunat oCompSunat, string motivotraslado, string modalidadtraslado, string origen, string destino, string valorResumen, string firma, string rutabase, string ruta, string situacion = "", string mensaje = "") 
    {
    //DESCRIPCION: Generar PDF para Guia de Remision
    string rutalogo = string.Empty; //ruta logo  
    Document doc = new Document(PageSize.A4, 20.0F, 20.0F, 20.0F, 20.0F);
    EN_RespuestaRegistro respuestaPDF = new EN_RespuestaRegistro();
    EN_Concepto concepto = new EN_Concepto();
    string filename = string.Empty; //File archivo
    string logo = string.Empty; // logo
    string valorCodigoBarras = string.Empty; //valor para codigo de barras
    string textoTipoDoc = string.Empty; //tipo documento
    string textoTipoDoc2 = string.Empty; //tipo documento
    
    
    try
    {     
        ruta = ruta + EN_Constante.g_const_rutaSufijo_pdf;
        rutalogo = string.Format("{0}/{1}/", rutabase, oGuia.numerodocempresa);
        filename = ruta + oEmpresa.Nrodocumento.Trim() + "-" + oGuia.tipodocumento_id.Trim() + "-" + oGuia.serie.Trim() + "-" + oGuia.numero.Trim() + EN_Constante.g_const_extension_pdf;
        logo = rutalogo + oEmpresa.Imagen;

        if (filename.Trim() != "")
        {
            FileStream file = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            PdfWriter.GetInstance(doc, file);
            doc.Open();

            // Para el logo
            dynamic imagen =null;

            if (File.Exists(logo))
            {
                imagen = iTextSharp.text.Image.GetInstance(logo);
                imagen.BorderWidth = 0;
                imagen.Alignment = Element.ALIGN_LEFT;
                imagen.ScaleAbsolute(100, 100);
            }
            else
            {
                imagen=new Phrase(EN_Constante.g_const_vacio);
            }

            QRCodeEncoder generarCodigoQR = new QRCodeEncoder();
            generarCodigoQR.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            generarCodigoQR.QRCodeScale = Convert.ToInt32(2);
            generarCodigoQR.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;
            
            generarCodigoQR.QRCodeVersion = 0;
            generarCodigoQR.QRCodeBackgroundColor = System.Drawing.Color.FromArgb(-1);
            generarCodigoQR.QRCodeForegroundColor = System.Drawing.Color.FromArgb(-16777216);
           
            // Para el codigo QR           
            valorCodigoBarras = oEmpresa.Nrodocumento.Trim() + "|" + oGuia.tipodocumento_id.Trim() + "|" + oGuia.serie.Trim() + "|" + oGuia.numero.Trim() + "|" + oGuia.pesobrutototal + "|" + oGuia.unidadpeso + "|" + Strings.Format(Conversions.ToDate(oGuia.fechaemision), "dd/MM/yyyy")+ "|" + ( (oGuia.destinatario_tipodoc.Trim() == "")? "0": oGuia.destinatario_tipodoc) + "|" + ((oGuia.destinatario_nrodoc.Trim() == "")? "-": oGuia.destinatario_nrodoc);
          //  iTextSharp.text.Image pdfImage = iTextSharp.text.Image.GetInstance(generarCodigoQR.Encode(valorCodigoBarras, System.Text.Encoding.UTF8), System.Drawing.Imaging.ImageFormat.Jpeg);       
           BarcodeQRCode barcodeQRCode = new BarcodeQRCode(valorCodigoBarras, 1000,1000, null);
            iTextSharp.text.Image pdfImage= barcodeQRCode.GetImage();
            pdfImage.ScaleAbsolute(70, 70);
            // Fin Codigo QR

            // Tabla para Tipo de documento - RUC - Serie y Numero de documento
            PdfPTable table1 = new PdfPTable(1);            
            if (oGuia.tipodocumento_id.Trim() == oCompSunat.IdGR)
            {
                textoTipoDoc = EN_Constante.g_const_textoTipoDoc;
                textoTipoDoc2 = EN_Constante.g_const_textoTipoDoc2;
            }
            
            Chunk ruc = new Chunk("RUC N° " + oEmpresa.Nrodocumento, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            Chunk tipodocumento = new Chunk(textoTipoDoc, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            Chunk tipodocumento2 = new Chunk(textoTipoDoc2, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            Chunk nrodocumento = new Chunk(oGuia.serie + "-" + oGuia.numero, FontFactory.GetFont(BaseFont.HELVETICA, 12, iTextSharp.text.Font.BOLD));
 
            PdfPCell cell12 = new PdfPCell(new Phrase(ruc));
            cell12.HorizontalAlignment = 1;
            cell12.BorderWidthBottom = 0.0F;
            cell12.FixedHeight = 18.0F;
            PdfPCell cell11 = new PdfPCell(new Phrase(tipodocumento));
            cell11.HorizontalAlignment = 1;
            cell11.BorderWidthBottom = 0.0F;
            cell11.BorderWidthTop = 0.0F;
            cell11.FixedHeight = 18.0F;
            PdfPCell cell14 = new PdfPCell(new Phrase(tipodocumento2));
            cell14.HorizontalAlignment = 1;
            cell14.BorderWidthBottom = 0.0F;
            cell14.BorderWidthTop = 0.0F;
            cell14.FixedHeight = 18.0F;
            PdfPCell cell13 = new PdfPCell(new Phrase(nrodocumento));
            cell13.HorizontalAlignment = 1;
            cell13.BorderWidthTop = 0.0F;
            cell13.FixedHeight = 18.0F;

            table1.AddCell(cell12);
            table1.AddCell(cell11);
            table1.AddCell(cell14);
            table1.AddCell(cell13);
            // Tamaño de tabla
            table1.WidthPercentage = 35.0F;
            table1.HorizontalAlignment = 2;

            var outerTable = new PdfPTable(2);
            var outTblWidth2 = new int[] { (int)Math.Round(65.0), (int)Math.Round(35.0) };
                outerTable.SetWidths(outTblWidth2);
            PdfPCell c1 = new PdfPCell(imagen);
            PdfPCell c2 = new PdfPCell(table1);
            c1.BorderWidthBottom = 0.0F;
            c1.BorderWidthTop = 0.0F;
            c1.BorderWidthLeft = 0.0F;
            c1.BorderWidthRight = 0.0F;
            c2.BorderWidthBottom = 0.0F;
            c2.BorderWidthTop = 0.0F;
            c2.BorderWidthLeft = 0.0F;
            c2.BorderWidthRight = 0.0F;
            c1.HorizontalAlignment = 0;
            c2.HorizontalAlignment = 2;
            outerTable.AddCell(c1);
            outerTable.AddCell(c2);
            outerTable.WidthPercentage = 100.0F;
            outerTable.HorizontalAlignment = 0;
            doc.Add(outerTable);

            // Mostramos los datos de la empresa
            Chunk razonsocial = new Chunk(oEmpresa.RazonSocial, FontFactory.GetFont(BaseFont.COURIER, 9, iTextSharp.text.Font.BOLD));
            Chunk nombrecomercial = new Chunk(oEmpresa.Nomcomercial, FontFactory.GetFont(BaseFont.COURIER, 8, iTextSharp.text.Font.NORMAL));
            Chunk direccion = new Chunk(oEmpresa.Direccion + " " + oEmpresa.Sector, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            Chunk ubicacion = new Chunk(oEmpresa.Distrito + " - " + oEmpresa.Provincia + " - " + oEmpresa.Departamento + " - PERÚ", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            doc.Add(new Paragraph(razonsocial));
            doc.Add(new Paragraph(nombrecomercial));
            doc.Add(new Paragraph(direccion));
            doc.Add(new Paragraph(ubicacion));

            // Tabla para datos del cabecera
            string tipdocdest = "";
            if (oGuia.destinatario_tipodoc.Length == 1)
            {
                if (oGuia.destinatario_tipodoc == "6")
                    tipdocdest = "RUC";
                else if (oGuia.destinatario_tipodoc == "1")
                    tipdocdest = "DNI";
                else if (oGuia.destinatario_tipodoc == "4")
                    tipdocdest = "CAE";
                else if (oGuia.destinatario_tipodoc == "7")
                    tipdocdest = "PAS";
                else if (oGuia.destinatario_tipodoc == "A")
                    tipdocdest = "CDI";
                else if (oGuia.destinatario_tipodoc == "B")
                    tipdocdest = "DPR";
                else if (oGuia.destinatario_tipodoc == "C")
                    tipdocdest = "TAX";
                else if (oGuia.destinatario_tipodoc == "D")
                    tipdocdest = "IND";
                else
                    tipdocdest = "OTR";
            }
            else
                tipdocdest = oGuia.destinatario_tipodoc;


            PdfPTable table2A = new PdfPTable(4);
            var intTblWidth2A = new int[] { (int)Math.Round(15.0), (int)Math.Round(50.0), (int)Math.Round(15.0), (int)Math.Round(20.0) };
                table2A.SetWidths(intTblWidth2A);

            Chunk tnombreclienteB = new Chunk("Nombre:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tnombreclienteF = new Chunk("Razón Social:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            Chunk tnrodocclienteB = new Chunk("Doc.Identidad:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tnrodocclienteF = new Chunk("RUC:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            Chunk tmotivo = new Chunk("Motivo traslado:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tfechatraslado = new Chunk("Fecha traslado:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            
            Chunk tpesobruto = new Chunk("Peso Bruto:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
             Chunk tnrobultos = null;
            if (oGuia.nrobultos > 0) 
            {
                 tnrobultos = new Chunk("Nro. Bultos:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            }
           
          
            Chunk nombrecliente = new Chunk(oGuia.destinatario_nombre, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            Chunk nrodocclienteB = new Chunk(tipdocdest + " " + oGuia.destinatario_nrodoc, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            Chunk nrodocclienteF = new Chunk(oGuia.destinatario_nrodoc, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            Chunk motivo = new Chunk(motivotraslado, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            Chunk fechatraslado = new Chunk(Convert.ToDateTime(oGuia.fechatraslado).ToString("dd/MM/yyyy"), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));


            Chunk pesobruto = new Chunk(oGuia.pesobrutototal.ToString(), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk nrobultos = null;
            if (oGuia.nrobultos > 0) {
                 nrobultos = new Chunk(oGuia.nrobultos.ToString(), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            }
            

            // Espacio de Tabla
            PdfPCell cell20A = new PdfPCell(new Phrase(""));
            cell20A.HorizontalAlignment = 0;
            cell20A.BorderWidthTop = 0.0F;
            cell20A.BorderWidthBottom = 0.0F;
            cell20A.BorderWidthLeft = 0.0F;
            cell20A.BorderWidthRight = 0.0F;
            cell20A.FixedHeight = 8.0F;

            PdfPCell cell20B = new PdfPCell(new Phrase(""));
            cell20B.HorizontalAlignment = 0;
            cell20B.BorderWidthTop = 0.0F;
            cell20B.BorderWidthBottom = 0.0F;
            cell20B.BorderWidthLeft = 0.0F;
            cell20B.BorderWidthRight = 0.0F;
            cell20B.FixedHeight = 8.0F;

            PdfPCell cell20C = new PdfPCell(new Phrase(""));
            cell20C.HorizontalAlignment = 0;
            cell20C.BorderWidthTop = 0.0F;
            cell20C.BorderWidthBottom = 0.0F;
            cell20C.BorderWidthLeft = 0.0F;
            cell20C.BorderWidthRight = 0.0F;
            cell20C.FixedHeight = 8.0F;

            PdfPCell cell20D = new PdfPCell(new Phrase(""));
            cell20D.HorizontalAlignment = 0;
            cell20D.BorderWidthTop = 0.0F;
            cell20D.BorderWidthBottom = 0.0F;
            cell20D.BorderWidthLeft = 0.0F;
            cell20D.BorderWidthRight = 0.0F;
            cell20D.FixedHeight = 8.0F;

            // Nombre del cliente
            PdfPCell cell22AB = new PdfPCell(new Phrase(tnombreclienteB));
            cell22AB.HorizontalAlignment = 0;
            cell22AB.BorderWidthTop = 0.0F;
            cell22AB.BorderWidthBottom = 0.0F;
            cell22AB.BorderWidthLeft = 0.0F;
            cell22AB.BorderWidthRight = 0.0F;

            PdfPCell cell22AF = new PdfPCell(new Phrase(tnombreclienteF));
            cell22AF.HorizontalAlignment = 0;
            cell22AF.BorderWidthTop = 0.0F;
            cell22AF.BorderWidthBottom = 0.0F;
            cell22AF.BorderWidthLeft = 0.0F;
            cell22AF.BorderWidthRight = 0.0F;
            // cell22AF.FixedHeight = 14.0F

            PdfPCell cell22B = new PdfPCell(new Phrase(nombrecliente));
            cell22B.HorizontalAlignment = 0;
            cell22B.BorderWidthTop = 0.0F;
            cell22B.BorderWidthBottom = 0.0F;
            cell22B.BorderWidthLeft = 0.0F;
            cell22B.BorderWidthRight = 0.0F;


            // Nro de Documento del cliente
            PdfPCell cell23AB = new PdfPCell(new Phrase(tnrodocclienteB));
            cell23AB.HorizontalAlignment = 0;
            cell23AB.BorderWidthTop = 0.0F;
            cell23AB.BorderWidthBottom = 0.0F;
            cell23AB.BorderWidthLeft = 0.0F;
            cell23AB.BorderWidthRight = 0.0F;


            PdfPCell cell23AF = new PdfPCell(new Phrase(tnrodocclienteF));
            cell23AF.HorizontalAlignment = 0;
            cell23AF.BorderWidthTop = 0.0F;
            cell23AF.BorderWidthBottom = 0.0F;
            cell23AF.BorderWidthLeft = 0.0F;
            cell23AF.BorderWidthRight = 0.0F;

            PdfPCell cell23BB = new PdfPCell(new Phrase(nrodocclienteB));
            cell23BB.HorizontalAlignment = 0;
            cell23BB.BorderWidthTop = 0.0F;
            cell23BB.BorderWidthBottom = 0.0F;
            cell23BB.BorderWidthLeft = 0.0F;
            cell23BB.BorderWidthRight = 0.0F;
           

            PdfPCell cell23BF = new PdfPCell(new Phrase(nrodocclienteF));
            cell23BF.HorizontalAlignment = 0;
            cell23BF.BorderWidthTop = 0.0F;
            cell23BF.BorderWidthBottom = 0.0F;
            cell23BF.BorderWidthLeft = 0.0F;
            cell23BF.BorderWidthRight = 0.0F;
         

            // motivo
            PdfPCell cell24A = new PdfPCell(new Phrase(tmotivo));
            cell24A.HorizontalAlignment = 0;
            cell24A.BorderWidthTop = 0.0F;
            cell24A.BorderWidthBottom = 0.0F;
            cell24A.BorderWidthLeft = 0.0F;
            cell24A.BorderWidthRight = 0.0F;
         

            PdfPCell cell24B = new PdfPCell(new Phrase(motivo));
            cell24B.HorizontalAlignment = 0;
            cell24B.BorderWidthTop = 0.0F;
            cell24B.BorderWidthBottom = 0.0F;
            cell24B.BorderWidthLeft = 0.0F;
            cell24B.BorderWidthRight = 0.0F;


            // Fecha de traslado
            PdfPCell cell21A = new PdfPCell(new Phrase(tfechatraslado));
            cell21A.HorizontalAlignment = 0;
            cell21A.BorderWidthTop = 0.0F;
            cell21A.BorderWidthBottom = 0.0F;
            cell21A.BorderWidthLeft = 0.0F;
            cell21A.BorderWidthRight = 0.0F;
      

            PdfPCell cell21B = new PdfPCell(new Phrase(fechatraslado));
            cell21B.HorizontalAlignment = 0;
            cell21B.BorderWidthTop = 0.0F;
            cell21B.BorderWidthBottom = 0.0F;
            cell21B.BorderWidthLeft = 0.0F;
            cell21B.BorderWidthRight = 0.0F;
         

            // peso
            PdfPCell cell25A = new PdfPCell(new Phrase(tpesobruto));
            cell25A.HorizontalAlignment = 0;
            cell25A.BorderWidthTop = 0.0F;
            cell25A.BorderWidthBottom = 0.0F;
            cell25A.BorderWidthLeft = 0.0F;
            cell25A.BorderWidthRight = 0.0F;
            

            PdfPCell cell25B = new PdfPCell(new Phrase(pesobruto));
            cell25B.HorizontalAlignment = 0;
            cell25B.BorderWidthTop = 0.0F;
            cell25B.BorderWidthBottom = 0.0F;
            cell25B.BorderWidthLeft = 0.0F;
            cell25B.BorderWidthRight = 0.0F;
            cell25B.FixedHeight = 14.0F;

            // bultos
            PdfPCell cell26A = null;
             PdfPCell cell26B = null;
            if (oGuia.nrobultos > 0) 
            {
               cell26A = new PdfPCell(new Phrase(tnrobultos));
                cell26A.HorizontalAlignment = 0;
                cell26A.BorderWidthTop = 0.0F;
                cell26A.BorderWidthBottom = 0.0F;
                cell26A.BorderWidthLeft = 0.0F;
                cell26A.BorderWidthRight = 0.0F;
                cell26A.FixedHeight = 14.0F;

                cell26B = new PdfPCell(new Phrase(nrobultos));
                cell26B.HorizontalAlignment = 0;
                cell26B.BorderWidthTop = 0.0F;
                cell26B.BorderWidthBottom = 0.0F;
                cell26B.BorderWidthLeft = 0.0F;
                cell26B.BorderWidthRight = 0.0F;
                cell26B.FixedHeight = 14.0F;
            }
        

            // Espacio de Tabla
            PdfPCell cell27A = new PdfPCell(new Phrase(""));
            cell27A.HorizontalAlignment = 0;
            cell27A.BorderWidthTop = 0.0F;
            cell27A.BorderWidthBottom = 0.0F;
            cell27A.BorderWidthLeft = 0.0F;
            cell27A.BorderWidthRight = 0.0F;
            cell27A.FixedHeight = 8.0F;

            PdfPCell cell27B = new PdfPCell(new Phrase(""));
            cell27B.HorizontalAlignment = 0;
            cell27B.BorderWidthTop = 0.0F;
            cell27B.BorderWidthBottom = 0.0F;
            cell27B.BorderWidthLeft = 0.0F;
            cell27B.BorderWidthRight = 0.0F;
            cell27B.FixedHeight = 8.0F;

            PdfPCell cell28A = new PdfPCell(new Phrase(""));
            cell28A.HorizontalAlignment = 0;
            cell28A.BorderWidthTop = 0.0F;
            cell28A.BorderWidthBottom = 0.0F;
            cell28A.BorderWidthLeft = 0.0F;
            cell28A.BorderWidthRight = 0.0F;
            cell28A.FixedHeight = 8.0F;

            PdfPCell cell28B = new PdfPCell(new Phrase(""));
            cell28B.HorizontalAlignment = 0;
            cell28B.BorderWidthTop = 0.0F;
            cell28B.BorderWidthBottom = 0.0F;
            cell28B.BorderWidthLeft = 0.0F;
            cell28B.BorderWidthRight = 0.0F;
            cell28B.FixedHeight = 8.0F;


            // -----Separador
            table2A.AddCell(cell20A);
            table2A.AddCell(cell20B);
            table2A.AddCell(cell20C);
            table2A.AddCell(cell20D);

            // ---Nombre o Razon Social
            if (tipdocdest == EN_Constante.g_const_ruc)
                table2A.AddCell(cell22AF);
            else
                table2A.AddCell(cell22AB);
            table2A.AddCell(cell22B);

            // ---Documento del Cliente
            if (tipdocdest == EN_Constante.g_const_ruc)
                table2A.AddCell(cell23AF);
            else
                table2A.AddCell(cell23AB);
            if (tipdocdest == EN_Constante.g_const_ruc)
                table2A.AddCell(cell23BF);
            else
                table2A.AddCell(cell23BB);

            // ---Direccion
            table2A.AddCell(cell24A);
            table2A.AddCell(cell24B);

            // ---Fecha Emision
            table2A.AddCell(cell21A);
            table2A.AddCell(cell21B);

            // ---peso
            table2A.AddCell(cell25A);
            table2A.AddCell(cell25B);

            // ---bultos
            if (oGuia.nrobultos > 0 ) {
                table2A.AddCell(cell26A);
                table2A.AddCell(cell26B);

            }
            
            // -----Separador
            table2A.AddCell(cell27A);
            table2A.AddCell(cell27B);
            table2A.AddCell(cell28A);
            table2A.AddCell(cell28B);

            // Tamaño de tabla
            table2A.WidthPercentage = 100.0F;
            table2A.HorizontalAlignment = 0;
            doc.Add(table2A);

            

            // Tabla para el detalle del documento de venta
         
            PdfPTable table3 = new PdfPTable(5);
            int[] intTblWidth3;
            
            intTblWidth3 = new int[] { (int)Math.Round(5.0), (int)Math.Round(10.0), (int)Math.Round(50.0), (int)Math.Round(10.0), (int)Math.Round(15.0) }; 

            table3.SetWidths(intTblWidth3);

            Chunk tItem = new Chunk("Item", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tCodigo = new Chunk("Código", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tDescripcion = new Chunk("Descripción", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tUM = new Chunk("Und.", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk tCantidad = new Chunk("Cantidad", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            
            PdfPCell cell30A = new PdfPCell(new Phrase(tItem));
            cell30A.HorizontalAlignment = 1;
            cell30A.FixedHeight = 18.0F;

            PdfPCell cell31A = new PdfPCell(new Phrase(tCodigo));
            cell31A.HorizontalAlignment = 1;
            cell31A.FixedHeight = 18.0F;

            PdfPCell cell32A = new PdfPCell(new Phrase(tDescripcion));
            cell32A.HorizontalAlignment = 1;
            cell32A.FixedHeight = 18.0F; 

            PdfPCell cell33A = new PdfPCell(new Phrase(tUM));
            cell33A.HorizontalAlignment = 1;
            cell33A.FixedHeight = 18.0F; 

            PdfPCell cell34A = new PdfPCell(new Phrase(tCantidad));
            cell34A.HorizontalAlignment = 1;
            cell34A.FixedHeight = 18.0F;     

            table3.AddCell(cell30A);
            table3.AddCell(cell31A);
            table3.AddCell(cell32A);
            table3.AddCell(cell33A);
            table3.AddCell(cell34A);
           
                        
            if (oDetalle.Count > 0)
            {
                foreach (var item in oDetalle)
                {
                    Chunk itemx = new Chunk(item.line_id.ToString(), FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk Codigo = new Chunk(item.codigo, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk Descripcion = new Chunk(item.descripcion, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
                    Chunk UM = new Chunk(item.unidad, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

                    string[] cant;
                    string cantmostrar;
                    if (System.Convert.ToString(item.cantidad).ToString().Replace(",", ".").Contains("."))
                    {
                        cant = Strings.Split(System.Convert.ToString(item.cantidad), ".");
                        if (Strings.Len(cant[1]) > 2)
                        {
                            // Imprimo mas de dos decimales
                            if (Strings.Len(cant[1]) > 3)
                                cantmostrar = Strings.Format(item.cantidad, "##,##0.0000");
                            else
                                cantmostrar = Strings.Format(item.cantidad, "##,##0.000");
                        }
                        else
                            // Imprimo dos decimales
                            cantmostrar = Strings.Format(item.cantidad, "##,##0.00");
                    }
                    else
                    {
                          // Imprimo dos decimales
                        cantmostrar = Strings.Format(item.cantidad, "##,##0.00");
                    }
                      
                        
                    Chunk Cantidad = new Chunk(cantmostrar, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));                 
                  
                    PdfPCell cell30B = new PdfPCell(new Phrase(itemx));
                    cell30B.HorizontalAlignment = 1;

                    PdfPCell cell31B = new PdfPCell(new Phrase(Codigo));
                    cell31B.HorizontalAlignment = 1;

                    PdfPCell cell32B = new PdfPCell(new Phrase(Descripcion));
                    cell32B.HorizontalAlignment = 0;

                    PdfPCell cell33B = new PdfPCell(new Phrase(UM));
                    cell33B.HorizontalAlignment = 1;

                    PdfPCell cell34B = new PdfPCell(new Phrase(Cantidad));
                    cell34B.HorizontalAlignment = 2;
                   

                    table3.AddCell(cell30B);
                    table3.AddCell(cell31B);
                    table3.AddCell(cell32B);
                    table3.AddCell(cell33B);
                    table3.AddCell(cell34B);
                                     
                }
            }
            // Tamaño de tabla
            table3.WidthPercentage = 100.0F;
            table3.HorizontalAlignment = 0;
            doc.Add(table3);
            doc.Add(new Paragraph(" "));

            
            string textosunat = ""; // OBSERVACIONES DE SUNAT
            Chunk tTextosunat = new Chunk(textosunat, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            
            string textoCDR = "";
        

            Chunk tTextoCDR = new Chunk(textoCDR, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

             // Tabla para los totales y monto en letras
            PdfPTable table4 = new PdfPTable(1);
                var intTblWidth4 = new int[] { (int)Math.Round(90.0) };
                table4.SetWidths(intTblWidth4);
           
            // Para la leyenda de monto en letras
            PdfPCell cell4A1 = new PdfPCell(new Phrase(tTextosunat));
            cell4A1.HorizontalAlignment = 0;
            cell4A1.VerticalAlignment = 1;
            cell4A1.BorderWidthTop = 0.0F;
            cell4A1.BorderWidthBottom = 0.0F;
            cell4A1.BorderWidthLeft = 0.0F;
            cell4A1.BorderWidthRight = 0.0F;
            cell4A1.FixedHeight = 14.0F;

            // Celda Vtexto CDR
            PdfPCell cell4A2 = new PdfPCell(new Phrase(tTextoCDR));
            cell4A2.HorizontalAlignment = 0;
            cell4A2.VerticalAlignment = 1;
            cell4A2.BorderWidthTop = 0.0F;
            cell4A2.BorderWidthBottom = 0.0F;
            cell4A2.BorderWidthLeft = 0.0F;
            cell4A2.BorderWidthRight = 0.0F;
            cell4A2.FixedHeight = 14.0F;    
    
            // Agregamos los datos en la tabla

   
            table4.AddCell(cell4A1);
            table4.AddCell(cell4A2);                         

            table4.WidthPercentage = 100.0F;
            table4.HorizontalAlignment = 0;

            doc.Add(table4);
            
        
            // Para la informaciòn adicional
            PdfPTable table6 = new PdfPTable(2);
                var intTblWidth6 = new int[] { (int)Math.Round(20.0), (int)Math.Round(60.0) };
                table6.SetWidths(intTblWidth6);

            Chunk tTituloAdic = new Chunk("Datos de traslado:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));

            // punto de partida

            Chunk tdireccionpartida = new Chunk("Dirección Partida:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk direccionpartida = new Chunk(oGuia.direccionptopartida + " "+ origen, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

             // punto de llegada

            Chunk tdireccionllegada = new Chunk("Dirección Llegada:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk direccionllegada = new Chunk(oGuia.direccionptollegada + " "+ destino, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            // modalidad

            Chunk tmodalidad = new Chunk("Modalidad de Traslado:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            
            string varmodalidad = string.Empty;
            if (oGuia.modalidadtraslado.Trim() == "01") {
                varmodalidad = "Transporte público";
            } else if (oGuia.modalidadtraslado.Trim() == "02") {
                varmodalidad = "Transporte privado";
            } else {
                varmodalidad = "Modalidad de traslado desconocido";
            }
            
            Chunk modalidad = new Chunk(varmodalidad, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
            
            


            //transporte publico
            string trtipodoc  = DevuelveTipoDocumento(oGuia.trpublic_tipodoc.Trim());
            Chunk ttransportista = new Chunk("Transportista:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk trasnportista = new Chunk(oGuia.trpublic_nombre, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            Chunk ttrnrodocumento = new Chunk("Nro. Documento:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk trnrodocumento = new Chunk(trtipodoc+" "+oGuia.trpublic_nrodoc, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            //transporte privado
             string trtipodoc1 = DevuelveTipoDocumento(oGuia.trprivad_tipodocconductor.Trim());
            Chunk ttransportista1 = new Chunk("Placa del Vehículo:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk trasnportista1 = new Chunk(oGuia.trprivad_placa, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            Chunk ttrnrodocumento1 = new Chunk("Id. Conductor:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk trnrodocumento1 = new Chunk(trtipodoc1 +" "+oGuia.trprivad_nrodocconductor, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            //contenedor

            Chunk tcontenedor = new Chunk("Nro. Contenedor:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk contenedor = new Chunk(oGuia.nrocontenedorimport, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

             //Guia de baja

            Chunk tguiabaja = new Chunk("Guia de Baja:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk guiabaja = new Chunk(oGuia.numerodocumentogrbaja, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));

            //Documento relacionado

            Chunk tdocrelacionado = new Chunk("Documento relacionado:", FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.BOLD));
            Chunk docrelacionado = new Chunk(oGuia.numerodocumentorelc, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
          
            PdfPCell cell70 = new PdfPCell(new Phrase(tTituloAdic));
            cell70.HorizontalAlignment = 0;
            cell70.BorderWidthTop = 0.0F;
            cell70.BorderWidthBottom = 0.0F;
            cell70.BorderWidthLeft = 0.0F;
            cell70.BorderWidthRight = 0.0F;
            cell70.Colspan = 2;
            cell70.FixedHeight = 14.0F;

            PdfPCell cell71 = new PdfPCell(new Phrase(tdireccionpartida));
            cell71.HorizontalAlignment = 0;
            PdfPCell cell72 = new PdfPCell(new Phrase(direccionpartida));
            cell72.HorizontalAlignment = 0;

            PdfPCell cell73 = new PdfPCell(new Phrase(tdireccionllegada));
            cell73.HorizontalAlignment = 0;

            PdfPCell cell74 = new PdfPCell(new Phrase(direccionllegada));
            cell74.HorizontalAlignment = 0;
       

            PdfPCell cell75 = new PdfPCell(new Phrase(tmodalidad));
            cell75.HorizontalAlignment = 0;
            cell75.FixedHeight = 14.0F;
            PdfPCell cell76 = new PdfPCell(new Phrase(modalidad));
            cell76.HorizontalAlignment = 0;
            cell76.FixedHeight = 14.0F;

            PdfPCell cell77 = new PdfPCell(new Phrase(ttransportista));
            cell77.HorizontalAlignment = 0;
            PdfPCell cell78 = new PdfPCell(new Phrase(trasnportista));
            cell78.HorizontalAlignment = 0;

            PdfPCell cell79 = new PdfPCell(new Phrase(ttrnrodocumento));
            cell79.HorizontalAlignment = 0;
            PdfPCell cell7A = new PdfPCell(new Phrase(trnrodocumento));
            cell7A.HorizontalAlignment = 0;

            PdfPCell cell7B = new PdfPCell(new Phrase(ttransportista1));
            cell7B.HorizontalAlignment = 0;
            PdfPCell cell7C = new PdfPCell(new Phrase(trasnportista1));
            cell7C.HorizontalAlignment = 0;

            PdfPCell cell7D = new PdfPCell(new Phrase(ttrnrodocumento1));
            cell7D.HorizontalAlignment = 0;
            PdfPCell cell7E = new PdfPCell(new Phrase(trnrodocumento1));
            cell7E.HorizontalAlignment = 0;

            PdfPCell cell7F = new PdfPCell(new Phrase(tcontenedor));
            cell7F.HorizontalAlignment = 0;
            PdfPCell cell7G = new PdfPCell(new Phrase(contenedor));
            cell7G.HorizontalAlignment = 0;

            PdfPCell cell7H = new PdfPCell(new Phrase(tguiabaja));
            cell7F.HorizontalAlignment = 0;
            PdfPCell cell7I = new PdfPCell(new Phrase(guiabaja));
            cell7G.HorizontalAlignment = 0;

            PdfPCell cell7J = new PdfPCell(new Phrase(tdocrelacionado));
            cell7F.HorizontalAlignment = 0;
            PdfPCell cell7K = new PdfPCell(new Phrase(docrelacionado));
            cell7G.HorizontalAlignment = 0;

            table6.AddCell(cell70);
            table6.AddCell(cell71);
            table6.AddCell(cell72);
            table6.AddCell(cell73);
            table6.AddCell(cell74);
            table6.AddCell(cell75);
            table6.AddCell(cell76);

            if(oGuia.modalidadtraslado == "01")
            {
                table6.AddCell(cell77);
                table6.AddCell(cell78);
                table6.AddCell(cell79);
                table6.AddCell(cell7A);
            }else {
                table6.AddCell(cell7B);
                table6.AddCell(cell7C);
                table6.AddCell(cell7D);
                table6.AddCell(cell7E);
            }

            if(oGuia.nrocontenedorimport.Trim()!= "" )
            {
                table6.AddCell(cell7F);
                table6.AddCell(cell7G);
            }

            table6.AddCell(cell7H);
            table6.AddCell(cell7I);
            table6.AddCell(cell7J);
            table6.AddCell(cell7K);

            //Tamaño de tabla
            table6.WidthPercentage = 80.0F;
            table6.HorizontalAlignment = 0;

 
            // Para codigo QR
            PdfPTable table8 = new PdfPTable(1);
                var intTblWidth8 = new int[] { (int)Math.Round(20.0) };
                table8.SetWidths(intTblWidth8);

            PdfPCell cell90 = new PdfPCell(new Phrase(""));
            cell90.HorizontalAlignment = 2;
            cell90.VerticalAlignment = 1;
            cell90.BorderWidthTop = 0.0F;
            cell90.BorderWidthBottom = 0.0F;
            cell90.BorderWidthLeft = 0.0F;
            cell90.BorderWidthRight = 0.0F;
            cell90.FixedHeight = 14.0F;

            // generarCodigoQR 

            PdfPCell cell91 = new PdfPCell(pdfImage);
            cell91.HorizontalAlignment = 2;
            cell91.VerticalAlignment = 1;
            cell91.BorderWidthTop = 0.0F;
            cell91.BorderWidthBottom = 0.0F;
            cell91.BorderWidthLeft = 0.0F;
            cell91.BorderWidthRight = 0.0F;

            table8.AddCell(cell90);
            table8.AddCell(cell91);
            table8.WidthPercentage = 20.0F;
            table8.HorizontalAlignment = 2;

            var outerTablex = new PdfPTable(2);
                var outTblWidthx = new int[] { (int)Math.Round(80.0), (int)Math.Round(20.0) };
                outerTablex.SetWidths(outTblWidthx);

            PdfPCell cx1 = new PdfPCell(table6);
            PdfPCell cx2 = new PdfPCell(table8);


            cx1.BorderWidthBottom = 0.0F;
            cx1.BorderWidthTop = 0.0F;
            cx1.BorderWidthLeft = 0.0F;
            cx1.BorderWidthRight = 0.0F;

            cx2.BorderWidthBottom = 0.0F;
            cx2.BorderWidthTop = 0.0F;
            cx2.BorderWidthLeft = 0.0F;
            cx2.BorderWidthRight = 0.0F;
            cx2.Rowspan = 2;

            

            cx1.HorizontalAlignment = 0;
            cx2.HorizontalAlignment = 2;
           

            outerTablex.AddCell(cx1);
            outerTablex.AddCell(cx2);
            

            outerTablex.WidthPercentage = 100.0F;
            outerTablex.HorizontalAlignment = 0;

            doc.Add(outerTablex);
          
            string textoautorizac = "";  // Texto de Autorización de SUNAT
            if (oEmpresa.NroResolucion.Trim() != "")
                textoautorizac = "Autorizado a ser emisor electrónico mediante R.I. SUNAT N° " + oEmpresa.NroResolucion;

            Chunk tTextoautorizac = new Chunk(textoautorizac, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.NORMAL));
           
            string textoleyenda = ""; // Texto de Leyenda de Representación impresa
            string textolink = ", consulte en " + oEmpresa.Web + ".";            
                textoleyenda = "Representación Impresa de la Guía de Remisión Electrónica.";
            
            if(oEmpresa.Web.Trim() == "") {
                textoleyenda = textoleyenda + " Código Hash: " + valorResumen;
            }else {
                textoleyenda = textoleyenda + textolink + " Código Hash: " + valorResumen;
            }

            Chunk tTextoleyenda = new Chunk(textoleyenda, FontFactory.GetFont(BaseFont.COURIER, 7, iTextSharp.text.Font.ITALIC));

            PdfPTable table9 = new PdfPTable(1);
                var intTblWidth9 = new int[] { (int)Math.Round(90.0) };
                table9.SetWidths(intTblWidth9);

            PdfPCell cell98 = new PdfPCell(new Phrase(tTextoautorizac));
            cell98.HorizontalAlignment = 0;
            cell98.VerticalAlignment = 1;
            cell98.BorderWidthTop = 0.0F;
            cell98.BorderWidthBottom = 0.0F;
            cell98.BorderWidthLeft = 0.0F;
            cell98.BorderWidthRight = 0.0F;
           

            PdfPCell cell99 = new PdfPCell(new Phrase(tTextoleyenda));
            cell99.HorizontalAlignment = 0;
            cell99.VerticalAlignment = 1;
            cell99.BorderWidthTop = 0.0F;
            cell99.BorderWidthBottom = 0.0F;
            cell99.BorderWidthLeft = 0.0F;
            cell99.BorderWidthRight = 0.0F;
            
            if (textoautorizac.Trim() != "")
                table9.AddCell(cell98);
            table9.AddCell(cell99);

            table9.WidthPercentage = 100.0F;
            table9.HorizontalAlignment = 0;

            doc.Add(table9);

            doc.Dispose();
            doc.Close();
            file.Dispose();
            file.Close();

       
            
        }
        else 
        {
            respuestaPDF.FlagVerificacion = false;
            respuestaPDF.DescRespuesta = EN_Constante.g_const_genPDFerr;

        }
        respuestaPDF.FlagVerificacion = true;
        respuestaPDF.DescRespuesta = EN_Constante.g_const_genPDF;
        return respuestaPDF;
        
    }
    catch (Exception ex)
    {
        
        respuestaPDF.FlagVerificacion = false;
        respuestaPDF.DescRespuesta = ex.Message;
        return respuestaPDF;
    }
    finally
    {
        //COPIAMOS ARCHIVO A S3 Y ELIMINAMOS DE TEMPORAL
        string nombrePDF= oEmpresa.Nrodocumento.Trim() + "-" + oGuia.tipodocumento_id.Trim() + "-" + oGuia.serie.Trim() + "-" + oGuia.numero.Trim();
        AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oEmpresa.Nrodocumento.Trim(),rutabase,EN_Constante.g_const_rutaSufijo_pdf,nombrePDF,EN_Constante.g_const_extension_pdf);

    }
    
    }
    public string DevuelveTipoDocumento(string tipodocumento)
    {
        //DESCRIPCION: Devuelve tipo de documento
        string nombredocumento = "";  //Nombre de tipo de documento
        try
        {
          if (tipodocumento == "6" || tipodocumento == "06" )
            nombredocumento = "RUC";
        else if (tipodocumento == "1" || tipodocumento == "01")
            nombredocumento = "DNI";
        else if (tipodocumento == "4" || tipodocumento == "04")
            nombredocumento = "CAE";
        else if (tipodocumento == "7" || tipodocumento == "07")
            nombredocumento = "PAS";
        else if (tipodocumento == "A")
            nombredocumento = "CDI";
        else if (tipodocumento == "B")
            nombredocumento = "DPR";
        else if (tipodocumento == "C")
            nombredocumento = "TAX";
        else if (tipodocumento == "D")
            nombredocumento = "IND";
        else
            nombredocumento = "OTR";
        return nombredocumento;  
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        
        
    }
    
  
   
 
}
}