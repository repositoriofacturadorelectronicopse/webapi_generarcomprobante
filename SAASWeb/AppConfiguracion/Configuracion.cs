﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: Configuracion.cs
 VERSION : 1.0
 OBJETIVO: Clase de funciones comunes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System.IO;
using System.Net;
using CEN;
using Microsoft.VisualBasic;
using Newtonsoft.Json;
using System;
using System.Globalization;


namespace AppConfiguracion
{
    public class Configuracion
    {
   


    public string getDateNowLima()
    {

        TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);

        DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
        return cstTime.ToString(EN_Constante.g_const_formfech,  new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru));

    }
    public string getDateHourNowLima()
    {
        TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);
        DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
        return cstTime.ToString(EN_Constante.g_const_formhora , new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru));
    }

     public DateTime getDateTimeNowLima()
        {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime;
        }


    
    public static ClassRptaGenerarCpeDoc sendWebApi(string urlWA, string dataSerializado)
    {
        // DESCRIPCION: FUNCION PARA COMONICARSE CON OTRA WEB API

            ServicePointManager.ServerCertificateValidationCallback= delegate{return true;};
            string responseBody;                                    //respuesta
            var url = $"{urlWA}";                                   //url
            var request = (HttpWebRequest)WebRequest.Create(url);   // request
            string json = dataSerializado;                          // json
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";

            try
            {
                 using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }
              
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            responseBody = objReader.ReadToEnd();
                        
                            ClassRptaGenerarCpeDoc rpt=  (ClassRptaGenerarCpeDoc)JsonConvert.DeserializeObject(responseBody,typeof(ClassRptaGenerarCpeDoc));
                            objReader.Dispose();
                            strReader.Dispose();
                            response.Dispose();
                            return rpt;
                        }
                    }
                
                  }
                
            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
           

    }



     public bool ValidationRUC(string ruc)
    {
        if (!Information.IsNumeric(ruc))
            return false;
        else if (ruc.Length != 11)
            return false;
        else
        {
            int dig01 = System.Convert.ToInt32(ruc.Substring(0, 1)) * 5;
            int dig02 = System.Convert.ToInt32(ruc.Substring(1, 1)) * 4;
            int dig03 = System.Convert.ToInt32(ruc.Substring(2, 1)) * 3;
            int dig04 = System.Convert.ToInt32(ruc.Substring(3, 1)) * 2;
            int dig05 = System.Convert.ToInt32(ruc.Substring(4, 1)) * 7;
            int dig06 = System.Convert.ToInt32(ruc.Substring(5, 1)) * 6;
            int dig07 = System.Convert.ToInt32(ruc.Substring(6, 1)) * 5;
            int dig08 = System.Convert.ToInt32(ruc.Substring(7, 1)) * 4;
            int dig09 = System.Convert.ToInt32(ruc.Substring(8, 1)) * 3;
            int dig10 = System.Convert.ToInt32(ruc.Substring(9, 1)) * 2;
            int dig11 = System.Convert.ToInt32(ruc.Substring(10, 1));

            int suma = dig01 + dig02 + dig03 + dig04 + dig05 + dig06 + dig07 + dig08 + dig09 + dig10;
            int residuo = suma % 11;
            int resta = 11 - residuo;

            int digChk;
            if (resta == 10)
                digChk = 0;
            else if (resta == 11)
                digChk = 1;
            else
                digChk = resta;

            if (dig11 == digChk)
                return true;
            else
                return false;
        }
    }
    public EN_RegistrarGuiaElectronica sendWebApiGuia(string urlWA, string dataSerializado)
    {
        //DESCRIPCION: Envio a la SUNAT
        try
        {
            ServicePointManager.ServerCertificateValidationCallback= delegate{return true;};

            var url = $"{urlWA}";
            var request = (HttpWebRequest)WebRequest.Create(url);
            string json = dataSerializado;
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Accept = "application/json";
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(json);
            }
              
                using (WebResponse response = request.GetResponse())
                {
                    using (Stream strReader = response.GetResponseStream())
                    {
                        using (StreamReader objReader = new StreamReader(strReader))
                        {
                            string responseBody = objReader.ReadToEnd();
                        
                            EN_RegistrarGuiaElectronica rpt=  (EN_RegistrarGuiaElectronica)JsonConvert.DeserializeObject(responseBody,typeof(EN_RegistrarGuiaElectronica));
                            objReader.Dispose();
                            strReader.Dispose();
                            response.Dispose();
                            return rpt;
                        }
                    }
                
                  } 
        }
        catch (Exception ex)
        {
            
            EN_RegistrarGuiaElectronica respuesta= new  EN_RegistrarGuiaElectronica();
            respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
            respuesta.RespRegistrarGuiaElectronica.DescRespuesta = EN_Constante.g_const_error_interno;
            respuesta.RespRegistrarGuiaElectronica.mensajeRespuesta = "Error al conectarse con el servicio de Envio de Guía";
            respuesta.RespRegistrarGuiaElectronica.comprobante = "";
            respuesta.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
            respuesta.ErrorWebServ.TipoError = EN_Constante.g_const_1;
            respuesta.ErrorWebServ.DescripcionErr = ex.Message;
            return respuesta;
        }
        

    }
        public void verificarExisteTmpPse()
        {
            
            try
            {

                Console.WriteLine(Directory.GetCurrentDirectory());
                if ((!System.IO.Directory.Exists(Directory.GetCurrentDirectory()+EN_Constante.g_const_sufijoRutaTemporalPSE)))
                {

                    System.IO.Directory.CreateDirectory(Directory.GetCurrentDirectory()+EN_Constante.g_const_sufijoRutaTemporalPSE);
                    Console.WriteLine(Directory.GetCurrentDirectory()+EN_Constante.g_const_sufijoRutaTemporalPSE);
                }
            }
            catch(Exception ex)
            {
                throw ex;

            }

            
        }

       public ClassRptaGenerarCpeDoc GetRutasDocsS3(string ruc_emisor, string nombreArchivo)
       {
           //DESCRIPCION: FUNCION PARA OBTENER RUTAS TEMPORALES DE ARCHIVOS EN S3Objects

            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc();
           try
           {
                  String ruta_pdf= "/"+ruc_emisor+"/PDF/"+ nombreArchivo+ EN_Constante.g_const_extension_pdf;
            String ruta_xml= "/"+ruc_emisor+"/XML/"+ nombreArchivo+ EN_Constante.g_const_extension_xml;
            String ruta_cdr= "/"+ruc_emisor+"/CDR/R-"+ nombreArchivo+  EN_Constante.g_const_extension_xml;
            bool has_s3_pdf=AppConfiguracion.FuncionesS3.verificaExistS3Async(ruta_pdf).Result;
            bool has_s3_xml=AppConfiguracion.FuncionesS3.verificaExistS3Async(ruta_xml).Result;
            bool has_s3_cdr=AppConfiguracion.FuncionesS3.verificaExistS3Async(ruta_cdr).Result;


            respuesta.RptaRegistroCpeDoc.ruta_pdf = (has_s3_pdf)?AppConfiguracion.FuncionesS3.getUrlObjeto(ruta_pdf):"";
            respuesta.RptaRegistroCpeDoc.ruta_xml = (has_s3_xml)?AppConfiguracion.FuncionesS3.getUrlObjeto(ruta_xml):"";
            respuesta.RptaRegistroCpeDoc.ruta_cdr = (has_s3_cdr)?AppConfiguracion.FuncionesS3.getUrlObjeto(ruta_cdr):"";

            return respuesta;
               
           }
           catch (System.Exception ex)
           {
               
               throw ex;
           }

         


       }


       public string GetCadenaConexionFromWA(string p_url,string p_puerto,string p_api, string p_region,  string p_version, string p_secrectname)
       {
        // DESCRIPCION: FUNCION PARA OBTENER CADENA DE CONEXION DESDE API SERVICE MANAGER


           string cadenaConexion= EN_Constante.g_const_vacio;
           string urlWA     = EN_Constante.g_const_vacio;
       

           try
           {
            
            urlWA= p_url+":"+p_puerto+"/"+p_api;

                using(WebClient webClient = new WebClient())
                {
                    webClient.QueryString.Add("secretName", p_secrectname);
                    webClient.QueryString.Add("region", p_region);
                    webClient.QueryString.Add("versionStage", p_version);
                    cadenaConexion = webClient.DownloadString(urlWA);
                    return cadenaConexion;

                }
                
            
           }
           catch (System.Exception ex)
           {
               cadenaConexion = EN_Constante.g_const_vacio;
               return cadenaConexion;
               
           }
       }
    
    
    
    
    }
}