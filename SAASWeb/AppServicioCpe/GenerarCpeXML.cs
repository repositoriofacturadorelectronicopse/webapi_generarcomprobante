/****************************************************************************************************************************************************************************************
 PROGRAMA: GenerarCpeXML.cs
 VERSION : 1.0
 OBJETIVO: Clase generar xml
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using CEN;
using CLN;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic;
using AppConfiguracion;
using System.Xml;
using Newtonsoft.Json;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;
using Microsoft.VisualBasic.CompilerServices;
using System.Text;

namespace AppServicioCpe
{
     public static class GenerarCpeXML
    {
                
      


        // Inicio Generacion XML Para Comp. Retencion y Percepcion
        public static bool GenerarXmlOtrosCpe(EN_OtrosCpe oOtrosCpe, EN_Empresa oEmpresa, EN_Certificado oCertificado, string rutadoc, ref string nombre, ref string vResumen, ref string vFirma, EN_ComprobanteSunat oCompSunat, string rutabase, ref string msjRspta, ref string cadenaXML)
        {
            // Create XmlWriterSettings.
            XmlWriterSettings settings = new XmlWriterSettings();
            string prefixCac;
            string prefixCbc;
            string prefixExt;
            string prefixSac;
            string prefixDs;
            string cadCac = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2";
            string cadCbc = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2";
            string cadCcts = "urn:un:unece:uncefact:documentation:2";
            string cadDs = "http://www.w3.org/2000/09/xmldsig#";
            string cadExt = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2";
            string cadQdt = "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2";
            string cadSac = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1";
            string cadUdt = "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2";
            string cadXsi = "http://www.w3.org/2001/XMLSchema-instance";
      
            NE_OtrosCpe objProceso = new NE_OtrosCpe();
            List<EN_DetalleOtrosCpe> listaDetalle;
            string rutaemp;
            try
            {
                rutaemp = string.Format("{0}/{1}/", rutabase, oOtrosCpe.nrodocempresa);

                // Validamos si existe directorio del documento, de lo contrario lo creamos
                if ((!System.IO.Directory.Exists(rutadoc)))
                    System.IO.Directory.CreateDirectory(rutadoc);
                
                     // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
                if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml)))
                    System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml);

                if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf)))
                    System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf);

                if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr)))
                    System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr);

                listaDetalle = (List<EN_DetalleOtrosCpe>)objProceso.listaDetalleOtrosCpe(oOtrosCpe);
                nombre = oEmpresa.Nrodocumento.Trim() + "-" + oOtrosCpe.tipodocumento_id.Trim() + "-" + oOtrosCpe.serie.Trim() + "-" + oOtrosCpe.numero.Trim();

                // Validamos si existe comprobante en XML, ZIP y PDF generado y eliminamos
                if (File.Exists(rutadoc.Trim()  + EN_Constante.g_const_rutaSufijo_xml + nombre.Trim() +  EN_Constante.g_const_extension_xml))
                    File.Delete(rutadoc.Trim()  + EN_Constante.g_const_rutaSufijo_xml + nombre.Trim() + EN_Constante.g_const_extension_xml);
                if (File.Exists(rutadoc.Trim()  + EN_Constante.g_const_rutaSufijo_xml + nombre.Trim() + EN_Constante.g_const_extension_zip))
                    File.Delete(rutadoc.Trim()  + EN_Constante.g_const_rutaSufijo_xml + nombre.Trim() + EN_Constante.g_const_extension_zip);
                if (File.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf  + nombre.Trim() + EN_Constante.g_const_extension_pdf))
                    File.Delete(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf  + nombre.Trim() + EN_Constante.g_const_extension_pdf);
                // Validamos si existe CDR en XML y ZIP y eliminamos
                if (File.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_xml))
                    File.Delete(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_xml);
                if (File.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip))
                    File.Delete(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);

                settings.Indent = true;
                // Create XmlWriter.
                settings.Encoding = System.Text.UTF8Encoding.UTF8;

                rutadoc=rutadoc + EN_Constante.g_const_rutaSufijo_xml;
                using (XmlWriter writer = XmlWriter.Create(rutadoc.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml, settings))
                {
                    prefixCac = writer.LookupPrefix(cadCac);
                    prefixCbc = writer.LookupPrefix(cadCbc);
                    prefixExt = writer.LookupPrefix(cadExt);
                    prefixSac = writer.LookupPrefix(cadSac);
                    prefixDs = writer.LookupPrefix(cadDs);
                    // Begin writing.
                    writer.WriteStartDocument();
                    if (oOtrosCpe.tipodocumento_id.Trim() == oCompSunat.IdCR)
                        // Comprobantes de Retencion
                        writer.WriteStartElement("Retention", "urn:sunat:names:specification:ubl:peru:schema:xsd:Retention-1");
                    else if (oOtrosCpe.tipodocumento_id.Trim() == oCompSunat.IdCP)
                        // Comprobante de Percepcion
                        writer.WriteStartElement("Perception", "urn:sunat:names:specification:ubl:peru:schema:xsd:Perception-1");
                    writer.WriteAttributeString("xmlns", "cac", null/* TODO Change to default(_) if this is not a reference type */, cadCac);
                    writer.WriteAttributeString("xmlns", "cbc", null/* TODO Change to default(_) if this is not a reference type */, cadCbc);
                    writer.WriteAttributeString("xmlns", "ccts", null/* TODO Change to default(_) if this is not a reference type */, cadCcts);
                    writer.WriteAttributeString("xmlns", "ds", null/* TODO Change to default(_) if this is not a reference type */, cadDs);
                    writer.WriteAttributeString("xmlns", "ext", null/* TODO Change to default(_) if this is not a reference type */, cadExt);
                    writer.WriteAttributeString("xmlns", "qdt", null/* TODO Change to default(_) if this is not a reference type */, cadQdt);
                    writer.WriteAttributeString("xmlns", "sac", null/* TODO Change to default(_) if this is not a reference type */, cadSac);
                    writer.WriteAttributeString("xmlns", "udt", null/* TODO Change to default(_) if this is not a reference type */, cadUdt);
                    writer.WriteAttributeString("xmlns", "xsi", null/* TODO Change to default(_) if this is not a reference type */, cadXsi);
                    ObtenerExtensionesOtrosCpe(writer, prefixExt, prefixSac, prefixCbc, prefixDs, cadExt, cadSac, cadCbc, cadDs, oOtrosCpe, oCompSunat);
                    ObtenerDatosCabeceraOtrosCpe(writer, prefixCbc, prefixCac, prefixSac, cadCbc, cadCac, cadSac, oOtrosCpe, oEmpresa, oCompSunat);
                    if (listaDetalle.Count > 0)
                    {
                        foreach (var item in listaDetalle.ToList())
                            ObtenerDatosDetalleOtrosCpe(writer, prefixCbc, prefixCac, prefixSac, cadCbc, cadCac, cadSac, item, oCompSunat);
                    }
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
                AgregarFirma(oCertificado, rutadoc, rutaemp, nombre, ref vResumen, ref vFirma, oOtrosCpe.tipodocumento_id, oCompSunat, ref cadenaXML);
                // Me.ValidarXML(ruta, nombre, rutabase)

                 //COPIAMOS XML A S3 Y ELIMINAMOS DE TEMPORAL
                AppConfiguracion.FuncionesS3.CopyS3DeleteLocal( oOtrosCpe.nrodocempresa,rutabase,EN_Constante.g_const_rutaSufijo_xml,nombre.Trim(),EN_Constante.g_const_extension_xml);


                msjRspta = "Comprobante Generado Correctamente";
                return true;
            }
            catch (Exception ex)
            {
                if (File.Exists(rutadoc.Trim() + nombre.Trim() +  EN_Constante.g_const_extension_xml))
                    File.Delete(rutadoc.Trim() + nombre.Trim() +  EN_Constante.g_const_extension_xml);
                msjRspta = "Error al generar Comprobante: " + ex.Message.ToString();
                return false;
            }
        }



        private static object ObtenerExtensionesOtrosCpe(XmlWriter writer, string prefixExt, string prefixSac, string prefixCbc, string prefixDs, string cadExt, string cadSac, string cadCbc, string cadDs, EN_OtrosCpe oOtrosCpe, EN_ComprobanteSunat oCompSunat)
        {
            writer.WriteStartElement(prefixExt, "UBLExtensions", cadExt);
            writer.WriteStartElement(prefixExt, "UBLExtension", cadExt);
            writer.WriteStartElement(prefixExt, "ExtensionContent", cadExt);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            return writer;
        }



        private static XmlWriter ObtenerDatosCabeceraOtrosCpe(XmlWriter writer, string prefixCbc, string prefixCac, string prefixSac, string cadCbc, string cadCac, string cadSac, EN_OtrosCpe oOtrosCpe, EN_Empresa oEmpresa, EN_ComprobanteSunat oCompSunat)
        {
            List<EN_DocPersona> listaDocEmpresa = new List<EN_DocPersona>();
            NE_DocPersona objDocEmpresa = new NE_DocPersona();
            EN_DocPersona oDocEmpresa = new EN_DocPersona();
            // Listamos el codigo del tipo de documento de la Empresa
            oDocEmpresa.Codigo = oEmpresa.Tipodocumento.Trim();
            oDocEmpresa.Estado = "";
            oDocEmpresa.Indpersona = "";

            listaDocEmpresa = (List<EN_DocPersona>)objDocEmpresa.listarDocPersona(oDocEmpresa, EN_Constante.g_const_vacio, EN_Constante.g_const_0);

            List<EN_DocPersona> listaDocPersona = new List<EN_DocPersona>();
            NE_DocPersona objDocPersona = new NE_DocPersona();
            EN_DocPersona oDocPersona = new EN_DocPersona();
            // Listamos el codigo del tipo de documento del Proveedor o Cliente
            oDocPersona.Codigo = oOtrosCpe.persona_tipodoc.Trim();
            oDocPersona.Estado = "";
            oDocPersona.Indpersona = "";
            listaDocPersona = (List<EN_DocPersona>)objDocPersona.listarDocPersona(oDocPersona , EN_Constante.g_const_vacio, EN_Constante.g_const_0);

            // Version UBL - Version Estructura Documento
            writer.WriteElementString("UBLVersionID", cadCbc, "2.0");
            writer.WriteElementString("CustomizationID", cadCbc, "1.0");

            // Datos de la Firma Digital
            writer.WriteStartElement(prefixCac, "Signature", cadCac);
            writer.WriteElementString("ID", cadCbc, oEmpresa.SignatureID.Trim());
            writer.WriteStartElement(prefixCac, "SignatoryParty", cadCac);
            writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
            writer.WriteElementString("ID", cadCbc, oEmpresa.Nrodocumento.Trim());
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "PartyName", cadCac);
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(oEmpresa.RazonSocial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "DigitalSignatureAttachment", cadCac);
            writer.WriteStartElement(prefixCac, "ExternalReference", cadCac);
            writer.WriteElementString("URI", cadCbc, oEmpresa.SignatureURI.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();

            // Datos del comprobante
            writer.WriteElementString("ID", cadCbc, oOtrosCpe.serie.Trim() + "-" + oOtrosCpe.numero.Trim());
            writer.WriteElementString("IssueDate", cadCbc, System.Convert.ToDateTime(oOtrosCpe.fechaemision).Year.ToString().PadLeft(2,'0').Trim() + "-" + System.Convert.ToDateTime(oOtrosCpe.fechaemision).Month.ToString().ToString().PadLeft(2, '0').Trim() + "-" + System.Convert.ToDateTime(oOtrosCpe.fechaemision).Day.ToString().PadLeft(2, '0').Trim());

            // Datos de la empresa
            writer.WriteStartElement(prefixCac, "AgentParty", cadCac);
            // PartyIdentification
            writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteAttributeString("schemeID", listaDocEmpresa[0].CodigoSunat.Trim());
            writer.WriteValue(oEmpresa.Nrodocumento.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            // PartyName
            writer.WriteStartElement(prefixCac, "PartyName", cadCac);
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(oEmpresa.Nomcomercial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            // PostalAddress
            writer.WriteStartElement(prefixCac, "PostalAddress", cadCac);
            writer.WriteElementString("ID", cadCbc, oEmpresa.Coddis.Trim());
            writer.WriteStartElement(prefixCbc, "StreetName", cadCbc);
            writer.WriteCData(oEmpresa.Direccion.Trim());
            writer.WriteEndElement();

            if (oEmpresa.Sector.Trim() != "")
            {
                writer.WriteStartElement(prefixCbc, "CitySubdivisionName", cadCbc);
                writer.WriteCData(oEmpresa.Sector.Trim());
                writer.WriteEndElement();
            }

            writer.WriteStartElement(prefixCbc, "CityName", cadCbc);
            writer.WriteCData(oEmpresa.Departamento.Trim());
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "CountrySubentity", cadCbc);
            writer.WriteCData(oEmpresa.Provincia.Trim());
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "District", cadCbc);
            writer.WriteCData(oEmpresa.Distrito.Trim());
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "Country", cadCac);
            writer.WriteElementString("IdentificationCode", cadCbc, oEmpresa.Codpais.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            // PartyLegalEntity
            writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
            writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
            writer.WriteCData(oEmpresa.RazonSocial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();

            // Datos del Proveedor (Retenciones) - Cliente (Percepciones)
            writer.WriteStartElement(prefixCac, "ReceiverParty", cadCac);
            // PartyIdentification
            writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteAttributeString("schemeID", listaDocPersona[0].CodigoSunat.Trim());
            writer.WriteValue(oOtrosCpe.persona_nrodoc.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            // PartyName
            if (oOtrosCpe.persona_nombrecomercial.Trim() != "")
            {
                writer.WriteStartElement(prefixCac, "PartyName", cadCac);
                writer.WriteStartElement(prefixCbc, "Name", cadCbc);
                writer.WriteCData(oOtrosCpe.persona_nombrecomercial.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // PostalAddress
            if (oOtrosCpe.persona_direccion.Trim() != "")
            {
                writer.WriteStartElement(prefixCac, "PostalAddress", cadCac);
                writer.WriteElementString("ID", cadCbc, oOtrosCpe.persona_ubigeo.Trim());

                writer.WriteStartElement(prefixCbc, "StreetName", cadCbc);
                writer.WriteCData(oOtrosCpe.persona_direccion.Trim());
                writer.WriteEndElement();

                if (oOtrosCpe.persona_urbanizacion.Trim() != "")
                {
                    writer.WriteStartElement(prefixCbc, "CitySubdivisionName", cadCbc);
                    writer.WriteCData(oOtrosCpe.persona_urbanizacion.Trim());
                    writer.WriteEndElement();
                }

                writer.WriteStartElement(prefixCbc, "CityName", cadCbc);
                writer.WriteCData(oOtrosCpe.persona_departamento.Trim());
                writer.WriteEndElement();

                writer.WriteStartElement(prefixCbc, "CountrySubentity", cadCbc);
                writer.WriteCData(oOtrosCpe.persona_provincia.Trim());
                writer.WriteEndElement();

                writer.WriteStartElement(prefixCbc, "District", cadCbc);
                writer.WriteCData(oOtrosCpe.persona_distrito.Trim());
                writer.WriteEndElement();

                writer.WriteStartElement(prefixCac, "Country", cadCac);
                writer.WriteElementString("IdentificationCode", cadCbc, oOtrosCpe.persona_codpais.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // PartyLegalEntity
            writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
            writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
            writer.WriteCData(oOtrosCpe.persona_descripcion.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();

            // Regimen y Tasa 
            if (oOtrosCpe.tipodocumento_id.Trim() == oCompSunat.IdCR)
            {
                // Retencion
                writer.WriteElementString("SUNATRetentionSystemCode", cadSac, oOtrosCpe.regimen_retper.Trim());
                writer.WriteElementString("SUNATRetentionPercent", cadSac, oOtrosCpe.tasa_retper.ToString());
            }
            else if (oOtrosCpe.tipodocumento_id.Trim() == oCompSunat.IdCP)
            {
                // Percepcion
                writer.WriteElementString("SUNATPerceptionSystemCode", cadSac, oOtrosCpe.regimen_retper.Trim());
                writer.WriteElementString("SUNATPerceptionPercent", cadSac, oOtrosCpe.tasa_retper.ToString());
            }

            if (oOtrosCpe.observaciones.Trim() != "")
                writer.WriteElementString("Note", cadCbc, oOtrosCpe.observaciones.Trim());

            // Importe Total Retencion - Percepcion
            writer.WriteStartElement(prefixCbc, "TotalInvoiceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oOtrosCpe.moneda_impretper.Trim());
            writer.WriteValue(oOtrosCpe.importe_retper.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();

            // Importe Total Pagado - Cobrado
            if (oOtrosCpe.tipodocumento_id.Trim() == oCompSunat.IdCR)
                // writer.WriteStartElement(prefixCbc, "TotalPaid", cadCbc)
                writer.WriteStartElement(prefixSac, "SUNATTotalPaid", cadSac);
            else if (oOtrosCpe.tipodocumento_id.Trim() == oCompSunat.IdCP)
                // Importe Total Pagado
                writer.WriteStartElement(prefixSac, "SUNATTotalCashed", cadSac);
            writer.WriteAttributeString("currencyID", oOtrosCpe.moneda_imppagcob.Trim());
            writer.WriteValue(oOtrosCpe.importe_pagcob.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            return writer;
        }



        private static XmlWriter ObtenerDatosDetalleOtrosCpe(XmlWriter writer, string prefixCbc, string prefixCac, string prefixSac, string cadCbc, string cadCac, string cadSac, EN_DetalleOtrosCpe listaDetalle, EN_ComprobanteSunat oCompSunat)
        {
            NE_Facturacion objFacturacion = new NE_Facturacion();

            if (listaDetalle.tipodocumento_id.Trim() == oCompSunat.IdCR)
                writer.WriteStartElement(prefixSac, "SUNATRetentionDocumentReference", cadSac);
            else if (listaDetalle.tipodocumento_id.Trim() == oCompSunat.IdCP)
                writer.WriteStartElement(prefixSac, "SUNATPerceptionDocumentReference", cadSac);

            // Datos del Comprobante Relacionado
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteAttributeString("schemeID", listaDetalle.docrelac_tipodoc_id.ToString());
            writer.WriteValue(listaDetalle.docrelac_numerodoc.ToString());
            writer.WriteEndElement();
            // Fecha emisión documento relacionado
            writer.WriteElementString("IssueDate", cadCbc,Convert.ToDateTime(listaDetalle.docrelac_fechaemision).ToString("yyyy-MM-dd"));
            // Importe total documento relacionado
            writer.WriteStartElement(prefixCbc, "TotalInvoiceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", listaDetalle.docrelac_moneda.Trim());
            writer.WriteValue(listaDetalle.docrelac_importetotal.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();

            // Datos del Pago - Cobro
            writer.WriteStartElement(prefixCac, "Payment", cadCac);
            writer.WriteElementString("ID", cadCbc, listaDetalle.pagcob_numero.Trim());
            writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
            writer.WriteAttributeString("currencyID", listaDetalle.pagcob_moneda.Trim());
            writer.WriteValue(listaDetalle.pagcob_importe.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteElementString("PaidDate", cadCbc,Convert.ToDateTime(listaDetalle.pagcob_fecha).ToString("yyyy-MM-dd"));
            writer.WriteEndElement();

            // Datos de la Retencion - Percepcion - Importe Retenido
            if (listaDetalle.tipodocumento_id.Trim() == oCompSunat.IdCR)
            {
                writer.WriteStartElement(prefixSac, "SUNATRetentionInformation", cadSac);
                writer.WriteStartElement(prefixSac, "SUNATRetentionAmount", cadSac);
            }
            else if (listaDetalle.tipodocumento_id.Trim() == oCompSunat.IdCP)
            {
                writer.WriteStartElement(prefixSac, "SUNATPerceptionInformation", cadSac);
                writer.WriteStartElement(prefixSac, "SUNATPerceptionAmount", cadSac);
            }
            writer.WriteAttributeString("currencyID", listaDetalle.retper_moneda.ToString());
            writer.WriteValue(listaDetalle.retper_importe);
            writer.WriteEndElement();

            // Fecha Retencion - Percepcion - Monto total a pagar 
            if (listaDetalle.tipodocumento_id.Trim() == oCompSunat.IdCR)
            {
                writer.WriteElementString("SUNATRetentionDate", cadSac,Convert.ToDateTime(listaDetalle.retper_fecha).ToString("yyyy-MM-dd"));
                writer.WriteStartElement(prefixSac, "SUNATNetTotalPaid", cadSac);
            }
            else if (listaDetalle.tipodocumento_id.Trim() == oCompSunat.IdCP)
            {
                writer.WriteElementString("SUNATPerceptionDate", cadSac, Convert.ToDateTime(listaDetalle.retper_fecha).ToString("yyyy-MM-dd"));
                writer.WriteStartElement(prefixSac, "SUNATNetTotalCashed", cadSac);
            }
            writer.WriteAttributeString("currencyID", listaDetalle.retper_moneda_pagcob.ToString());
            writer.WriteValue(listaDetalle.retper_importe_pagcob);
            writer.WriteEndElement();

            // Tipo de cambio
            if (listaDetalle.tipocambio_monedaref != null && listaDetalle.tipocambio_monedaobj!= null != default(Boolean) && listaDetalle.tipocambio_factor!= null && listaDetalle.tipocambio_fecha != null)
            {
                writer.WriteStartElement(prefixCac, "ExchangeRate", cadCac);
                writer.WriteElementString("SourceCurrencyCode", cadCbc, listaDetalle.tipocambio_monedaref.Trim());
                writer.WriteElementString("TargetCurrencyCode", cadCbc, listaDetalle.tipocambio_monedaobj.Trim());
                writer.WriteElementString("CalculationRate", cadCbc, Strings.Format(listaDetalle.tipocambio_factor, "##,##0.0000"));
                writer.WriteElementString("Date", cadCbc, Convert.ToDateTime(listaDetalle.tipocambio_fecha).ToString("yyyy-MM-dd"));
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
            writer.WriteEndElement();
            return writer;
        }


        // Inicio Funciones para Firma y Validar y Enviar XML
        private static void AgregarFirma(EN_Certificado oCertificado, string rutadoc, string rutaemp, string Nombre, ref string valorResumen, ref string firma, string TipoDoc, EN_ComprobanteSunat oCompSunat, ref string cadenaXML)
        {
            try
            {
                string local_xmlArchivo = rutadoc + Nombre + ".XML";

                X509Certificate2 MiCertificado = new X509Certificate2(rutaemp + oCertificado.Nombre, oCertificado.ClaveCertificado);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(local_xmlArchivo);

                SignedXml signedXml = new SignedXml(xmlDoc);
                signedXml.SigningKey = MiCertificado.PrivateKey;

                KeyInfo KeyInfo = new KeyInfo();

                Reference Reference = new Reference();
                Reference.Uri = "";

                Reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());

                signedXml.AddReference(Reference);

                X509Chain X509Chain = new X509Chain();
                X509Chain.Build(MiCertificado);

                X509ChainElement local_element = X509Chain.ChainElements[0];
                KeyInfoX509Data x509Data = new KeyInfoX509Data(local_element.Certificate);
                string subjectName = local_element.Certificate.Subject;

                x509Data.AddSubjectName(subjectName);
                KeyInfo.AddClause(x509Data);

                signedXml.KeyInfo = KeyInfo;
                signedXml.ComputeSignature();

                XmlElement signature = signedXml.GetXml();
                signature.Prefix = "ds";
                signedXml.ComputeSignature();

                foreach (XmlNode node in signature.SelectNodes("descendant-or-self::*[namespace-uri()='http://www.w3.org/2000/09/xmldsig#']"))
                {
                    if (node.LocalName == "Signature")
                    {
                        XmlAttribute newAttribute = xmlDoc.CreateAttribute("Id");
                        newAttribute.Value = oCertificado.IdSignature;
                        node.Attributes.Append(newAttribute);
                        break;
                    }
                }

                string local_xpath = "";
                XmlNamespaceManager nsMgr;
                nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);

                if (TipoDoc == oCompSunat.IdCR)
                {
                    nsMgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:Retention-1");
                    local_xpath = "/tns:Retention/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent";
                }
                else if (TipoDoc == oCompSunat.IdCP)
                {
                    nsMgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:Perception-1");
                    local_xpath = "/tns:Perception/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent";
                }
                else if (TipoDoc == oCompSunat.IdRR)
                {
                    nsMgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1");
                    local_xpath = "/tns:VoidedDocuments/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent";
                }

                nsMgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
                nsMgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
                nsMgr.AddNamespace("ccts", "urn:un:unece:uncefact:documentation:2");
                nsMgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
                nsMgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
                nsMgr.AddNamespace("qdt", "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2");
                nsMgr.AddNamespace("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1");
                nsMgr.AddNamespace("udt", "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2");
                nsMgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");

                xmlDoc.SelectSingleNode(local_xpath, nsMgr).AppendChild(xmlDoc.ImportNode(signature, true));
                valorResumen = signature.ChildNodes.Item(0).ChildNodes.Item(2).ChildNodes.Item(2).FirstChild.Value;

                firma = signature.ChildNodes.Item(1).FirstChild.Value;

                System.IO.StreamWriter tw = new System.IO.StreamWriter(local_xmlArchivo, false, new System.Text.UTF8Encoding(false));
                xmlDoc.Save(tw);

                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("ds:Signature");

                // el nodo <ds:Signature> debe existir unicamente 1 vez
                if (nodeList.Count != 1)
                    throw new Exception("Se produjo un error en la firma del documento");
                signedXml.LoadXml((XmlElement)nodeList[0]);


                cadenaXML = xmlDoc.OuterXml.ToString().Trim();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private static void VerificarSunatCDR(XmlElement nodos, string codEtiqueta, ref string msjnodo, ref string codigo)
        {
            if (nodos.HasChildNodes)
            {
                if (nodos.ChildNodes[0].NodeType != XmlNodeType.Text & nodos.ChildNodes[0].NodeType != XmlNodeType.CDATA)
                {
                    foreach (XmlElement nodo in nodos.ChildNodes)
                    {
                        if (nodo.Name == "cac:DocumentResponse")
                            VerificarSunatCDR(nodo, codEtiqueta, ref msjnodo, ref codigo);
                        else if (nodo.Name == "cac:Response")
                            VerificarSunatCDR(nodo, codEtiqueta, ref msjnodo, ref codigo);
                        else if (nodo.Name == "cbc:ResponseCode")
                        {
                            if (nodo.FirstChild.Value.ToString().Replace(codEtiqueta, "").Trim() != "0")
                            {
                                // Rechazada con motivo
                                codigo = nodo.FirstChild.Value.ToString().Replace(codEtiqueta, "").Trim();
                                msjnodo = nodo.NextSibling.FirstChild.Value;
                            }
                            else
                            {
                                // Aceptada correctamente
                                codigo = "";
                                msjnodo = nodo.NextSibling.FirstChild.Value;
                            }
                            break;
                        }
                    }
                }
            }
        }


        private static void VerificarSunatTicket(XmlElement nodos, ref string ticket)
        {
            if (nodos.HasChildNodes)
            {
                if (nodos.ChildNodes[0].NodeType != XmlNodeType.Text & nodos.ChildNodes[0].NodeType != XmlNodeType.CDATA)
                {
                    foreach (XmlElement nodo in nodos.ChildNodes)
                    {
                        if (nodo.Name == "cbc:ID")
                        {
                            ticket = nodo.FirstChild.Value;
                            break;
                        }
                    }
                }
            }
        }



        private static void VerificarNBOSECDR(XmlElement nodos, string codEtiqueta, ref string msjnodo, ref string codigo)
        {
            if (nodos.HasChildNodes)
            {
                if (nodos.ChildNodes[0].NodeType != XmlNodeType.Text & nodos.ChildNodes[0].NodeType != XmlNodeType.CDATA)
                {
                    foreach (XmlElement nodo in nodos.ChildNodes)
                    {
                        if (nodo.Name == "cac:DocumentResponse")
                            VerificarNBOSECDR(nodo, codEtiqueta, ref msjnodo, ref codigo);
                        else if (nodo.Name == "cac:Response")
                            VerificarNBOSECDR(nodo, codEtiqueta, ref msjnodo, ref codigo);
                        else if (nodo.Name == "cbc:ResponseCode")
                        {
                            if (nodo.FirstChild.Value.ToString().Replace(codEtiqueta, "").Trim() != "0")
                            {
                                // Rechazada con motivo
                                codigo = nodo.FirstChild.Value.ToString().Replace(codEtiqueta, "").Trim();
                                msjnodo = nodo.NextSibling.FirstChild.Value;
                            }
                            else
                            {
                                // Aceptada correctamente
                                codigo = "";
                                msjnodo = nodo.NextSibling.FirstChild.Value;
                            }
                            break;
                        }
                    }
                }
            }
        }


        private static void VerificarNBOSETicket(XmlElement nodos, ref string ticket)
        {
            if (nodos.HasChildNodes)
            {
                if (nodos.ChildNodes[0].NodeType != XmlNodeType.Text & nodos.ChildNodes[0].NodeType != XmlNodeType.CDATA)
                {
                    foreach (XmlElement nodo in nodos.ChildNodes)
                    {
                        if (nodo.Name == "cbc:ID")
                        {
                            ticket = nodo.FirstChild.Value;
                            break;
                        }
                    }
                }
            }
        }

        private static void VerificarEFOSECDR(XmlElement nodos, string codEtiqueta, ref string msjnodo, ref string codigo)
        {
            if (nodos.HasChildNodes)
            {
                if (nodos.ChildNodes[0].NodeType != XmlNodeType.Text & nodos.ChildNodes[0].NodeType != XmlNodeType.CDATA)
                {
                    foreach (XmlElement nodo in nodos.ChildNodes)
                    {
                        if (nodo.Name == "ns3:DocumentResponse")
                            VerificarEFOSECDR(nodo, codEtiqueta, ref msjnodo, ref codigo);
                        else if (nodo.Name == "ns3:Response")
                            VerificarEFOSECDR(nodo, codEtiqueta, ref msjnodo, ref codigo);
                        else if (nodo.Name == "ResponseCode")
                        {
                            if (nodo.FirstChild.Value.ToString().Replace(codEtiqueta, "").Trim() != "0")
                            {
                                // Rechazada con motivo
                                codigo = nodo.FirstChild.Value.ToString().Replace(codEtiqueta, "").Trim();
                                msjnodo = nodo.NextSibling.FirstChild.Value;
                            }
                            else
                            {
                                // Aceptada correctamente
                                codigo = "";
                                msjnodo = nodo.NextSibling.FirstChild.Value;
                            }
                            break;
                        }
                    }
                }
            }
        }


        private static void VerificarEFOSETicket(XmlElement nodos, ref string ticket)
        {
            if (nodos.HasChildNodes)
            {
                if (nodos.ChildNodes[0].NodeType != XmlNodeType.Text & nodos.ChildNodes[0].NodeType != XmlNodeType.CDATA)
                {
                    foreach (XmlElement nodo in nodos.ChildNodes)
                    {
                        if (nodo.Name == "ID")
                        {
                            ticket = nodo.FirstChild.Value;
                            break;
                        }
                    }
                }
            }
        }


        // public static string EncodeArrayBase64(ref byte[] arrData)
        // {
        //     MSXML2.DOMDocument objXML;
        //     MSXML2.IXMLDOMElement objNode;

        //     objXML = new MSXML2.DOMDocument();
        //     // Matriz de bytes a base64
        //     objNode = objXML.createElement("b64");

        //     objNode.dataType = "bin.base64";
        //     objNode.nodeTypedValue = arrData;
        //     EncodeArrayBase64 = objNode.text;
        //     arrData = objNode.nodeTypedValue;
        //     objNode = null/* TODO Change to default(_) if this is not a reference type */;
        //     objXML = null/* TODO Change to default(_) if this is not a reference type */;
        // }


        // Fin Funciones para Firma y Validar y Enviar XML

        // Inicio Guardar el documento electronico y actualizar estado del documento 
        public static bool GuardarOtrosCpeNombreValorResumenFirma(EN_OtrosCpe oOtrosCpe, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            NE_OtrosCpe objOtrosCpe = new NE_OtrosCpe();
            return objOtrosCpe.GuardarOtrosCpeNombreValorResumenFirma(oOtrosCpe, nombreXML, valorResumen, valorFirma, xmlgenerado);
        }






        // Validaciones
        public static bool ValidarOtrosCpe(OtroscpeSunat oCpe, EN_ComprobanteSunat oCompuSunat, ref string MsjeRspta)
        {
            AppConfiguracion.Configuracion configurac = new AppConfiguracion.Configuracion();
            bool respuesta = true;
            string[] grupoRegimenPercep = new[] { "01", "02", "03" };
            string[] grupoRegimenRetenc = new[] { "01" };
            try
            {
                if (oCpe.tipodocumento_id == oCompuSunat.IdCP)
                {
                    // Validamos si el tipo de documento es Comprobante de Percepción
                    if (Strings.Left(oCpe.serie, 1) != "P")
                    {
                        // Validamos la serie alfanumerica que comienza con P
                        respuesta = false;
                        MsjeRspta = "Un comprobante de percepción debe comenzar con serie alfanumérica P";
                    }
                    else if (oCpe.serie.Length != 4)
                    {
                        // Validamos tamaño de la serie debe ser 4 caracteres
                        respuesta = false;
                        MsjeRspta = "El tamaño de la serie debe ser 4 caracteres";
                    }
                    else if (oCpe.numero.Length > 8)
                    {
                        // Validamos tamaño del correlativo hasta 8 caracteres
                        respuesta = false;
                        MsjeRspta = "El tamaño del correlativo debe ser hasta 8 caracteres";
                    }
                    else if (oCpe.persona_tipodoc.Trim() == "DNI" & oCpe.persona_nrodoc.Trim().Length != 8)
                    {
                        // Validamos que  si el cliente tiene DNI el número debe tener 8 dígitos
                        respuesta = false;
                        MsjeRspta = "Un DNI debe tener 8 dígitos";
                    }
                    else if (oCpe.persona_tipodoc == "RUC" & configurac.ValidationRUC(oCpe.persona_nrodoc) == false)
                    {
                        // Validamos que el RUC del cliente sea válido
                        respuesta = false;
                        MsjeRspta = "El número de RUC no es válido";
                    }
                    else if (oCpe.persona_descripcion.Trim() == "")
                    {
                        // Validamos que tenga nombre 
                        respuesta = false;
                        MsjeRspta = "Debe ingresar una descripcion del cliente";
                    }
                    else if (!grupoRegimenPercep.Contains(oCpe.regimen_retper.Trim()))
                    {
                        // Validamos que pertenezca a un regimen de percepcion valido
                        respuesta = false;
                        MsjeRspta = "Debe ingresar un regimen de percepcion válido";
                    }
                    else if (oCpe.ListaDetalleOtrosCpe.Count == 0)
                    {
                        // Validamos si existe detalle de documento
                        respuesta = false;
                        MsjeRspta = "Debe ingresar un detalle de comprobante";
                    }
                    else if (oCpe.ListaDetalleOtrosCpe.Count > 0)
                    {
                        // Validamos los campos del detalle de documento
                        string[] grupoTipoDoc = new[] { "01", "03", "07", "08", "12" };
                        // Dim grupoSerie As String() = {"E", "B", "F"}
                        foreach (EN_DetalleOtrosCpe docDetalle in oCpe.ListaDetalleOtrosCpe)
                        {
                            if (!grupoTipoDoc.Contains(docDetalle.docrelac_tipodoc_id.Trim()))
                            {
                                respuesta = false;
                                MsjeRspta = "Detalle Comprobante: Debe especificar tipo de documento valido.";
                                break;
                            }
                        }
                    }
                }
                else if (oCpe.tipodocumento_id == oCompuSunat.IdCR)
                {
                    // Validamos si el tipo de documento es Comprobante de Retención
                    if (Strings.Left(oCpe.serie, 1) != "R")
                    {
                        // Validamos la serie alfanumerica que comienza con R
                        respuesta = false;
                        MsjeRspta = "Un comprobante de retención debe comenzar con serie alfanumérica R";
                    }
                    else if (Strings.Len(oCpe.serie) != 4)
                    {
                        // Validamos tamaño de la serie debe ser 4 caracteres
                        respuesta = false;
                        MsjeRspta = "El tamaño de la serie debe ser 4 caracteres";
                    }
                    else if (Strings.Len(oCpe.numero) > 8)
                    {
                        // Validamos tamaño del correlativo hasta 8 caracteres
                        respuesta = false;
                        MsjeRspta = "El tamaño del correlativo debe ser hasta 8 caracteres";
                    }
                    else if (oCpe.persona_tipodoc.Trim() != "RUC")
                    {
                        // Validamos que el proveedor sea solo RUC
                        respuesta = false;
                        MsjeRspta = "Debe ingresar solo RUC";
                    }
                    else if (oCpe.persona_tipodoc == "RUC" & configurac.ValidationRUC(oCpe.persona_nrodoc) == false)
                    {
                        // Validamos que el RUC del cliente sea válido
                        respuesta = false;
                        MsjeRspta = "El número de RUC no es válido";
                    }
                    else if (oCpe.persona_descripcion.Trim() == "")
                    {
                        // Validamos que tenga nombre 
                        respuesta = false;
                        MsjeRspta = "Debe ingresar una descripcion del cliente";
                    }
                    else if (!grupoRegimenRetenc.Contains(oCpe.regimen_retper.Trim()))
                    {
                        // Validamos que pertenezca a un regimen de percepcion valido
                        respuesta = false;
                        MsjeRspta = "Debe ingresar un regimen de retención válido";
                    }
                    else if (oCpe.ListaDetalleOtrosCpe.Count == 0)
                    {
                        // Validamos si existe detalle de documento
                        respuesta = false;
                        MsjeRspta = "Debe ingresar un detalle de comprobante";
                    }
                    else if (oCpe.ListaDetalleOtrosCpe.Count > 0)
                    {
                        // Validamos los campos del detalle de documento
                        string[] grupoTipoDoc = new[] { "01", "07", "08", "12" };
                        // Dim grupoSerie As String() = {"E", "B", "F"}
                        foreach (EN_DetalleOtrosCpe docDetalle in oCpe.ListaDetalleOtrosCpe)
                        {
                            if (!grupoTipoDoc.Contains(docDetalle.docrelac_tipodoc_id.Trim()))
                            {
                                respuesta = false;
                                MsjeRspta = "Detalle Comprobante: Debe especificar tipo de documento valido.";
                                break;
                            }
                        }
                    }
                }
                else
                {
                    respuesta = false;
                    MsjeRspta = "El comprobante no es válido.";
                }
                return respuesta;
            }
            catch (Exception ex)
            {
                MsjeRspta = "Error durante la validacion de Otros Cpe: " + MsjeRspta;
                return false;
            }
        }



        // Inicio Reversiones Electronicas
        // public static bool GenerarXmlReversionesElectronico(EN_Reversion oReversion, EN_Empresa oEmpresa, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string ruta, string rutabase, ref string nombre, ref string vResumen, ref string vFirma, ref string MsjRspta)
        // {
        //     List<EN_DetalleReversion> detalleReversion = new List<EN_DetalleReversion>();
        //     GenerarPDF creaPDF = new GenerarPDF();
        //     NE_Reversion objReversion = new NE_Reversion();
        //     string mensaje = "";
        //     string cadenaXML = "";

        //     try
        //     {
        //         detalleReversion = (List<EN_DetalleReversion>)objReversion.listarDetalleReversion(oReversion);
        //         // Generamos el XML
        //         if (GenerarXmlReversion(oReversion, oCompSunat, oEmpresa, oCertificado, ruta, rutabase, ref nombre, ref vResumen, ref vFirma, ref mensaje, ref cadenaXML))
        //         {
        //             // Actualizo la tabla documento los campos nombrexml valorresumen y valorfirma
        //             if (GuardarReversionNombreValorResumenFirma(oReversion, nombre, vResumen, vFirma, cadenaXML))
        //             {
        //                 // Generamos el PDF
        //                 creaPDF.GenerarPDFReversion(oEmpresa, oReversion, detalleReversion, ruta, oCompSunat, vResumen, vFirma, rutabase);
        //                 return true;
        //             }
        //             else
        //             {
        //                 nombre = "";
        //                 vResumen = "";
        //                 vFirma = "";
        //                 MsjRspta = "Error al guardar el valor resumen, firma digital y nombre del archivo XML.";
        //                 return false;
        //             }
        //         }
        //         else
        //         {
        //             nombre = "";
        //             vResumen = "";
        //             vFirma = "";
        //             MsjRspta = mensaje;
        //             return false;
        //         }
        //     }
        //     catch (Exception ex)
        //     {
        //         return false;
        //     }
        // }


        public static bool GenerarXmlReversion(EN_Reversion oReversion, EN_ComprobanteSunat oCompSunat, EN_Empresa oEmpresa, EN_Certificado oCertificado, string ruta, string rutabase, ref string nombre, ref string vResumen, ref string vFirma, ref string msjRspta, ref string cadenaXML , ref string comprobante)
        {
            // Create XmlWriterSettings.
            XmlWriterSettings settings = new XmlWriterSettings();
            string prefixCac;
            string prefixCbc;
            string prefixDs;
            string prefixExt;
            string prefixSac;
            string cadCac = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2";
            string cadCbc = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2";
            string cadDs = "http://www.w3.org/2000/09/xmldsig#";
            string cadExt = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2";
            string cadSac = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1";
            string cadQdt = "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2";
            // Dim cadXsi As String = "http://www.w3.org/2001/XMLSchema-instance"
          
            NE_Reversion objProceso = new NE_Reversion();
            List<EN_DetalleReversion> listaDetalle;
            string rutaemp;

            rutaemp = string.Format("{0}/{1}/", rutabase, oReversion.Nrodocumento);

            // Validamos si existe directorio del documento, de lo contrario lo creamos
            if ((!System.IO.Directory.Exists(ruta)))
                System.IO.Directory.CreateDirectory(ruta);
            
            // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
            if ((!System.IO.Directory.Exists(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_xml)))
                System.IO.Directory.CreateDirectory(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_xml);

            if ((!System.IO.Directory.Exists(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_pdf)))
                System.IO.Directory.CreateDirectory(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_pdf);

            if ((!System.IO.Directory.Exists(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_cdr)))
                System.IO.Directory.CreateDirectory(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_cdr);

            // Validamos si existe comprobante en XML, ZIP y PDF generado y eliminamos
            if (File.Exists(ruta.Trim()  + EN_Constante.g_const_rutaSufijo_xml  + nombre.Trim() + EN_Constante.g_const_extension_xml))
                File.Delete(ruta.Trim()  + EN_Constante.g_const_rutaSufijo_xml  + nombre.Trim() + EN_Constante.g_const_extension_xml);
            if (File.Exists(ruta.Trim()  + EN_Constante.g_const_rutaSufijo_xml  + nombre.Trim() + EN_Constante.g_const_extension_zip))
                File.Delete(ruta.Trim()  + EN_Constante.g_const_rutaSufijo_xml  + nombre.Trim() + EN_Constante.g_const_extension_zip);
            if (File.Exists(ruta.Trim()  + EN_Constante.g_const_rutaSufijo_pdf  + nombre.Trim() + EN_Constante.g_const_extension_pdf))
                File.Delete(ruta.Trim()  + EN_Constante.g_const_rutaSufijo_pdf  + nombre.Trim() + EN_Constante.g_const_extension_pdf);
            // Validamos si existe CDR en XML y ZIP y eliminamos
            if (File.Exists(ruta.Trim()  + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta  + nombre.Trim() + EN_Constante.g_const_extension_xml))
                File.Delete(ruta.Trim()  + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta  + nombre.Trim() + EN_Constante.g_const_extension_xml);
            if (File.Exists(ruta.Trim()  + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta  + nombre.Trim() + EN_Constante.g_const_extension_zip))
                File.Delete(ruta.Trim()  + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta  + nombre.Trim() + EN_Constante.g_const_extension_zip);

            try
            {
                listaDetalle = (List<EN_DetalleReversion>)objProceso.listarDetalleReversion(oReversion);
                nombre = oReversion.Nrodocumento.Trim() + "-" + oReversion.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oReversion.FechaComunicacion), "yyyyMMdd") + "-" + oReversion.Correlativo.ToString();

                comprobante= nombre;
                settings.Indent = true;
                // Create XmlWriter.
                settings.Encoding = System.Text.UTF8Encoding.UTF8;

                ruta=ruta + EN_Constante.g_const_rutaSufijo_xml;

                using (XmlWriter writer = XmlWriter.Create(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml, settings))
                {
                    prefixCac = writer.LookupPrefix(cadCac);
                    prefixExt = writer.LookupPrefix(cadExt);
                    prefixSac = writer.LookupPrefix(cadSac);
                    prefixCbc = writer.LookupPrefix(cadCbc);
                    prefixDs = writer.LookupPrefix(cadDs);
                    // Begin writing.
                    writer.WriteStartDocument();
                    writer.WriteStartElement("VoidedDocuments", "urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1");

                    writer.WriteAttributeString("xmlns", "cac", null/* TODO Change to default(_) if this is not a reference type */, cadCac);
                    writer.WriteAttributeString("xmlns", "cbc", null/* TODO Change to default(_) if this is not a reference type */, cadCbc);
                    writer.WriteAttributeString("xmlns", "ds", null/* TODO Change to default(_) if this is not a reference type */, cadDs);
                    writer.WriteAttributeString("xmlns", "ext", null/* TODO Change to default(_) if this is not a reference type */, cadExt);
                    writer.WriteAttributeString("xmlns", "qdt", null/* TODO Change to default(_) if this is not a reference type */, cadQdt);
                    writer.WriteAttributeString("xmlns", "sac", null/* TODO Change to default(_) if this is not a reference type */, cadSac);
                    // writer.WriteAttributeString("xmlns", "xsi", Nothing, cadXsi)

                    ObtenerExtensionesReversion(writer, prefixExt, prefixSac, prefixCbc, prefixDs, cadExt, cadSac, cadCbc, cadDs);
                    ObtenerDatosCabeceraReversion(writer, prefixCbc, prefixCac, cadCbc, cadCac, oReversion, oEmpresa);
                    if (listaDetalle.Count > 0)
                    {
                        foreach (var item in listaDetalle.ToList())
                            ObtenerDatosDetalleReversion(writer, prefixCbc, prefixCac, prefixSac, cadCbc, cadCac, cadSac, item);
                    }
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
                AgregarFirma(oCertificado, ruta, rutaemp, nombre, ref vResumen, ref vFirma, oReversion.Tiporesumen, oCompSunat, ref cadenaXML);
                // Me.ValidarXMLCpe(ruta, nombre, oReversion.Tiporesumen.Trim, oCompSunat, rutabase)
              
                      //COPIAMOS XML A S3 Y ELIMINAMOS DE TEMPORAL
                AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oReversion.Nrodocumento,rutabase,EN_Constante.g_const_rutaSufijo_xml,nombre.Trim(),EN_Constante.g_const_extension_xml);
           
                msjRspta = "Comunicado de Reversiones Generado Correctamente";
                return true;
            }
            catch (Exception ex)
            {
                if (File.Exists(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml))
                    File.Delete(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml);
              
                msjRspta = "Error al generar Comunicado de Reversiones: " + ex.Message.ToString();
                return false;
            }
        }



        private static object ObtenerExtensionesReversion(XmlWriter writer, string prefixExt, string prefixSac, string prefixCbc, string prefixDs, string cadExt, string cadSac, string cadCbc, string cadDs)
        {
            writer.WriteStartElement(prefixExt, "UBLExtensions", cadExt);
            writer.WriteStartElement(prefixExt, "UBLExtension", cadExt);
            writer.WriteStartElement(prefixExt, "ExtensionContent", cadExt);
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            return writer;
        }


        private static XmlWriter ObtenerDatosCabeceraReversion(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_Reversion oReversion, EN_Empresa oEmpresa)
        {
            NE_Facturacion objFacturacion = new NE_Facturacion();
            NE_DocPersona objDocPersona = new NE_DocPersona();
            EN_DocPersona oDocPersona = new EN_DocPersona();
            List<EN_DocPersona> listaDocPersona = new List<EN_DocPersona>();

            // Listamos el codigo del tipo de documento RUC
            oDocPersona.Codigo = "RUC";
            oDocPersona.Estado = "";
            oDocPersona.Indpersona = "";
            listaDocPersona =(List<EN_DocPersona>) objDocPersona.listarDocPersona(oDocPersona, EN_Constante.g_const_vacio,EN_Constante.g_const_0);

            writer.WriteElementString("UBLVersionID", cadCbc, "2.0");
            writer.WriteElementString("CustomizationID", cadCbc, "1.0");

            // Datos del resumen
            // writer.WriteElementString("ID", cadCbc, oReversion.Tiporesumen.Trim() & "-" & Format(CDate(oReversion.FechaComunicacion), "yyyyMMdd") & "-" & oReversion.Correlativo.ToString().PadLeft(5, "0").Trim())
            writer.WriteElementString("ID", cadCbc, oReversion.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oReversion.FechaComunicacion),EN_Constante.g_const_formfecha_yyyyMMdd)   + "-" + oReversion.Correlativo.ToString());
            writer.WriteElementString("ReferenceDate", cadCbc, System.Convert.ToDateTime(oReversion.FechaDocumento).Year.ToString().PadLeft(2, '0').Trim() + "-" + System.Convert.ToDateTime(oReversion.FechaDocumento).Month.ToString().ToString().PadLeft(2, '0').Trim() + "-" + System.Convert.ToDateTime(oReversion.FechaDocumento).Day.ToString().PadLeft(2, '0').Trim());
            writer.WriteElementString("IssueDate", cadCbc, System.Convert.ToDateTime(oReversion.FechaComunicacion).Year.ToString().PadLeft(2, '0').Trim() + "-" + System.Convert.ToDateTime(oReversion.FechaComunicacion).Month.ToString().ToString().PadLeft(2, '0').Trim() + "-" + System.Convert.ToDateTime(oReversion.FechaComunicacion).Day.ToString().PadLeft(2, '0').Trim());

            // Datos de la empresa
            writer.WriteStartElement(prefixCac, "Signature", cadCac);
            writer.WriteElementString("ID", cadCbc, oEmpresa.SignatureID.Trim());
            writer.WriteStartElement(prefixCac, "SignatoryParty", cadCac);
            writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
            writer.WriteElementString("ID", cadCbc, oEmpresa.Nrodocumento.Trim());
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "PartyName", cadCac);
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(oEmpresa.RazonSocial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "DigitalSignatureAttachment", cadCac);
            writer.WriteStartElement(prefixCac, "ExternalReference", cadCac);
            writer.WriteElementString("URI", cadCbc, oEmpresa.SignatureURI.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "AccountingSupplierParty", cadCac);
            writer.WriteElementString("CustomerAssignedAccountID", cadCbc, oEmpresa.Nrodocumento.Trim());
            writer.WriteElementString("AdditionalAccountID", cadCbc, listaDocPersona[0].CodigoSunat.Trim());
            writer.WriteStartElement(prefixCac, "Party", cadCac);
            writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
            writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
            writer.WriteCData(oEmpresa.RazonSocial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();

            writer.WriteEndElement();
            writer.WriteEndElement();

            return writer;
        }



        private static XmlWriter ObtenerDatosDetalleReversion(XmlWriter writer, string prefixCbc, string prefixCac, string prefixSac, string cadCbc, string cadCac, string cadSac, EN_DetalleReversion listaDetalle)
        {

            // Datos del item de baja
            writer.WriteStartElement(prefixCbc, "VoidedDocumentsLine", cadSac);
            writer.WriteElementString("LineID", cadCbc, listaDetalle.Item.ToString());
            writer.WriteElementString("DocumentTypeCode", cadCbc, listaDetalle.Tipodocumentoid);
            writer.WriteElementString("DocumentSerialID", cadSac, listaDetalle.Serie);
            writer.WriteElementString("DocumentNumberID", cadSac, listaDetalle.Numero);
            writer.WriteElementString("VoidReasonDescription", cadSac, listaDetalle.MotivoReversion);
            writer.WriteEndElement();

            return writer;
        }


        public static bool GuardarReversionNombreValorResumenFirma(EN_Reversion oReversion, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            NE_Reversion objReversion = new NE_Reversion();
            return objReversion.GuardarReversionNombreValorResumenFirma(oReversion, nombreXML, valorResumen, valorFirma, xmlgenerado);
        }



        // Validaciones
        public static string ValidarSituacion(EN_OtrosCpe pDocumento)
        {
            NE_OtrosCpe objDoc = new NE_OtrosCpe();
            return objDoc.consultarSituacion(pDocumento);
        }



// private void ValidarXMLCpe(string rutadoc, string nombre, string tipo, EN_ComprobanteSunat oCompSunat, string rutabase)
// {
//     bool success = true;
//     string ruta = rutabase + @"XSD\maindoc\";
//     XmlReader validator = null/* TODO Change to default(_) if this is not a reference type */;
//     try
//     {
//         switch (tipo)
//         {
//             case object _ when oCompSunat.IdRR  // Reversiones de CPE
//            :
//                 {
//                     ruta = ruta + "UBLPE-VoidedDocuments-1.0.xsd";
//                     break;
//                 }
//         }
//         if (File.Exists(ruta))
//         {
//             // Si existe el archivo valido que cumpla el esquema xsd
//             XmlReaderSettings settings = new XmlReaderSettings();

//             settings.ValidationType = ValidationType.Schema;
//             XmlSchemaSet schemas = new XmlSchemaSet();
//             settings.Schemas = schemas;
//             schemas.Add(null/* TODO Change to default(_) if this is not a reference type */, ruta);

//             settings.ValidationFlags = XmlSchemaValidationFlags.ReportValidationWarnings;
//             settings.ValidationEventHandler += ValidationEventHandler;
//             validator = XmlReader.Create(rutadoc.Trim() + nombre.Trim() + ".XML", settings);

//             // Leemos el documento xml, si se produce un error no cumplirá el esquema
//             while (validator.Read())
//             {
//             }
//         }
//         else
//             throw new Exception("No se puede validar contra el esquema xds.");
//     }
//     catch (Exception ex)
//     {
//         Logs.SaveErrorLog<NE_Proceso>(ex, "{0} - {1}", pClase, "ValidarXML");
//         throw ex;
//     }
//     finally
//     {
//         if (!validator == null)
//             validator.Close();
//     }
//     return success;
// }


   
        public static bool ValidarUsuarioEmpresa(int p_idempresa, string p_usuario, string p_clave)
        {
            bool rspta = false;
            NE_Certificado objCert = new NE_Certificado();
            if ((bool)objCert.validarEmpresaCertificado(p_idempresa, p_usuario, p_clave, EN_Constante.g_const_vacio, EN_Constante.g_const_0) == true)
                return true;
            else
                return false;
        }

        public static EN_Empresa GetEmpresaById(int idEmpresa, int Idpuntoventa)
        {
            //DESCRIPCION: FUNCION OBTIENE DATOS DE EMPRESA POR ID

            EN_Empresa listaEmpresa;                    // CLASE ENTIDAD EMPRESA
            EN_Empresa oEmpresa = new EN_Empresa();     // CLASE ENTIDAD EMPRESA
            NE_Empresa objEmpresa = new NE_Empresa();   // CLASENEGOCIO DE EMPRESA
            oEmpresa.Id = idEmpresa;
            oEmpresa.Nrodocumento = EN_Constante.g_const_vacio;
            oEmpresa.RazonSocial = EN_Constante.g_const_vacio;
            oEmpresa.Estado = EN_Constante.g_const_vacio;
            listaEmpresa = (EN_Empresa)objEmpresa.listaEmpresa(oEmpresa, Idpuntoventa, EN_Constante.g_const_vacio);
            return listaEmpresa;
        }
    
    }
    
}
