/****************************************************************************************************************************************************************************************
 PROGRAMA: GenerarCpe.cs
 VERSION : 1.0
 OBJETIVO: Clase generar comprobante
 FECHA   : 04/11/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using AppConfiguracion;
using CEN;
using CLN;

namespace AppServicioCpe
{
    public partial class GenerarCpe
    {
        
        public GenerarCpe()
        {
            //DESCRIPCION: CONSTRUCTOR DE CLASE GENERARCPE
        }
    private static EN_ComprobanteSunat buildComprobante()
    {
        //DESCRIPCION: FUNCION DE SETEO DE VARIABLES DE COMPROBANTE
        var oCompSunat = new EN_ComprobanteSunat();

        oCompSunat.IdFC = EN_ConfigConstantes.Instance.const_IdFC;
        oCompSunat.IdBV = EN_ConfigConstantes.Instance.const_IdBV;
        oCompSunat.IdNC = EN_ConfigConstantes.Instance.const_IdNC;
        oCompSunat.IdND = EN_ConfigConstantes.Instance.const_IdND;
        oCompSunat.IdGR = EN_ConfigConstantes.Instance.const_IdGR;

        oCompSunat.IdCB =  EN_ConfigConstantes.Instance.const_IdCB;
        oCompSunat.IdRD = EN_ConfigConstantes.Instance.const_IdRD;
        oCompSunat.IdRR =  EN_ConfigConstantes.Instance.const_IdRR;

        oCompSunat.IdCP = EN_ConfigConstantes.Instance.const_IdCP;
        oCompSunat.IdCR =  EN_ConfigConstantes.Instance.const_IdCR;

        oCompSunat.codigoEtiquetaErrorDoc = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc;
        return oCompSunat;
    }
    public OtroscpeSunat CrearCpeElectronica(OtroscpeSunat oCpe, ref int codRspta, ref string msjeRspta,  ref bool success, ref string comprobante, ref string msjRptaError,  ref string ruc_emisor, ref string estado_comprobante)
    {
        // DESCRIPCION: FUNCION PARA CREAR OTROS COMPROBANTES ELECTRONICO

        string  rutabase, rutadoc;                                              // RUTA BASE Y RUTA DE DOCUMENTO
        string nombreArchivoXml = "";                                           // NOMBRE DE ARCHIVO XML
        string valorResumen = "";                                               // VALOR DE RESUMEN
        string firma = "";                                                      // FIRMA
        string mensaje = "";                                                    // MENSAJE
        string cadenaXML = "";                                                  // CADENA XML
        EN_Empresa empresaDocumento = new EN_Empresa();                         // CLASE ENTIDAD EMPRESA
        EN_Certificado certificadoDocumento = new EN_Certificado();             // CLASE ENTIDAD CERTIFICADO

        GenerarPDF creaPdf = new GenerarPDF();                                  // CLASE GENERAR PDF
        List<EN_DetalleOtrosCpe> detalleCpe = new List<EN_DetalleOtrosCpe>();   // LISTA DE CLASE ENTIDAD DETALLE OTROS COMPROBANTES
        string montoenletras;                                                   // MONTO EN LETRAS
        string monedaenletras;                                                  // MONEDA EN LETRAS
        NumeroALetras numeroALetras = new NumeroALetras();                      // CLASE ENTIDAD NUMERO A LETRAS
        NE_Facturacion objFacturacion = new NE_Facturacion();                   // CLASE ENTIDAD FACTURACION

        {
            oCpe.enviado_email="";
            oCpe.enviado_externo="";
            var withBlock = oCpe;
            
            try
            {
                if (GenerarCpeXML.ValidarUsuarioEmpresa(oCpe.empresa_id, oCpe.Usuario, oCpe.Clave))
                {
                    empresaDocumento = GetEmpresaById(withBlock.empresa_id, withBlock.puntoventa_id);
                    certificadoDocumento = GetCertificadoByEmpresaId(empresaDocumento.Id);

                    ruc_emisor=  empresaDocumento.Nrodocumento;

                    rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                    rutadoc = string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                    EN_ComprobanteSunat oCompSunat = buildComprobante();

                    if (withBlock.tipodocumento_id.Trim() == oCompSunat.IdCR || withBlock.tipodocumento_id.Trim() == oCompSunat.IdCP)
                    {
                        if (GenerarCpeXML.ValidarOtrosCpe(oCpe, oCompSunat, ref mensaje))
                        {
                            if (GuardarCpeSunat(oCpe))
                            {
                                NE_OtrosCpe obj = new NE_OtrosCpe();
                                var objCpe = new EN_OtrosCpe();
                                List<EN_OtrosCpe> listaCpes = new List<EN_OtrosCpe>();
                                objCpe.id = oCpe.id;
                                objCpe.empresa_id = oCpe.empresa_id;
                                objCpe.tipodocumento_id = oCpe.tipodocumento_id;
                                objCpe.estado = EN_Constante.g_const_estado_nuevo;
                                objCpe.situacion = EN_Constante.g_const_situacion_pendiente;
                                objCpe.serie = "";
                                objCpe.numero = "";
                                objCpe.CadenaAleatoria = (withBlock.UsuarioSession=="" || withBlock.UsuarioSession==null)?EN_Constante.g_const_UsuarioSession:withBlock.UsuarioSession;
                                objCpe.UsuarioSession = "";
                                objCpe.fechaIni = "";
                                objCpe.fechaFin ="";
                                listaCpes = (List<EN_OtrosCpe>)obj.listarOtrosCpe(objCpe);
                                if (GenerarCpeXML.GenerarXmlOtrosCpe(listaCpes[0], empresaDocumento, certificadoDocumento, rutadoc, ref nombreArchivoXml,ref valorResumen,ref firma, oCompSunat, rutabase, ref mensaje, ref cadenaXML))
                                {
                                    if (GenerarCpeXML.GuardarOtrosCpeNombreValorResumenFirma(listaCpes[0], nombreArchivoXml, valorResumen, firma, cadenaXML))
                                    {
                                        withBlock.NombreFichero = nombreArchivoXml;
                                        withBlock.ValorResumen = valorResumen;
                                        withBlock.ValorFirma = firma;
                                        codRspta = 0;
                                        msjeRspta = mensaje;
                                        // Generamos el documento en PDF
                                        montoenletras = numeroALetras.Convertir((double)oCpe.importe_retper);
                                        monedaenletras = objFacturacion.ObtenerDescripcionPorCodElemento("002", oCpe.moneda_impretper);

                                        detalleCpe = (List<EN_DetalleOtrosCpe>)obj.listaDetalleOtrosCpe(listaCpes[0]);
                                        creaPdf.GenerarPDFRetencionPercepcion(empresaDocumento, listaCpes[0], montoenletras, monedaenletras, detalleCpe, oCompSunat, valorResumen, firma, rutabase, rutadoc);
                                    }
                                        if(empresaDocumento.enviarxml== EN_Constante.g_const_si) 
                                        {
                                          
                                                string mensajeWA=EN_Constante.g_const_vacio;
                                                GenerarCpe.EnviarXmlOtrosCpeElectronico(listaCpes[EN_Constante.g_const_0], certificadoDocumento, oCompSunat, empresaDocumento.Produccion,  rutadoc, nombreArchivoXml, ref mensajeWA, ref codRspta,  ref estado_comprobante);
                                                if(codRspta==1){
                                                    string[] msjWA = mensajeWA.Split('|');
                                                    msjeRspta = mensaje + " - "+msjWA[0];  
                                                    msjRptaError = msjWA[1];
                                                }else{
                                                    msjeRspta = mensaje + " - "+mensajeWA;   
                                                     msjRptaError = "";          
                                                }

                                                Console.WriteLine(codRspta);
                                                  
                                                
                                            
                                        }

                                    success=true;
                                    comprobante= listaCpes[EN_Constante.g_const_0].serie   +   "-" +   listaCpes[EN_Constante.g_const_0].numero;

                                }
                                else
                                {
                                    withBlock.NombreFichero = "";
                                    withBlock.ValorResumen = "";
                                    withBlock.ValorFirma = "";
                                    codRspta = 1;
                                    msjeRspta = mensaje;
                                    obj.eliminarOtrosCpe(objCpe);
                                }
                            }
                        }
                        else
                        {
                            withBlock.NombreFichero = "";
                            withBlock.ValorResumen = "";
                            withBlock.ValorFirma = "";
                            codRspta = 1;
                            msjeRspta = mensaje;
                        }
                    }
                    else
                    {
                        withBlock.NombreFichero = "";
                        withBlock.ValorResumen = "";
                        withBlock.ValorFirma = "";
                        codRspta = 1;
                        msjeRspta = "El tipo de comprobante a generar no es válido.";
                    }
                }
                else
                {
                    withBlock.NombreFichero = "";
                    withBlock.ValorResumen = "";
                    withBlock.ValorFirma = "";
                    codRspta = 1;
                    msjeRspta = "No se ha encontrado el usuario del certificado de la empresa ingresada";
                }
            }
            catch (Exception ex)
            {
                withBlock.NombreFichero = "";
                withBlock.ValorResumen = "";
                withBlock.ValorFirma = "";
                codRspta = 1;
                msjeRspta = "Hubo una Excepción al generar el comprobante: " + ex.Message;
            }
        }
        return oCpe;
    }
    public EN_Certificado GetCertificadoByEmpresaId(int empresaId)
    {
        EN_Certificado oCertif = new EN_Certificado();
        NE_Certificado objCertif = new NE_Certificado();
        EN_Certificado listaCertificado;
        oCertif.Id = 0;
        oCertif.IdEmpresa = empresaId;
        oCertif.Nombre = "";
        oCertif.UsuarioSession = "";
        oCertif.CadenaAleatoria = "";
        listaCertificado = (EN_Certificado)objCertif.listarCertificado(oCertif, EN_Constante.g_const_vacio, EN_Constante.g_const_0);
        return listaCertificado;
    }
    public EN_Empresa GetEmpresaById(int idEmpresa, int Idpuntoventa)
    {
        EN_Empresa listaEmpresa;
        EN_Empresa oEmpresa = new EN_Empresa();
        NE_Empresa objEmpresa = new NE_Empresa();
        oEmpresa.Id = idEmpresa;
        oEmpresa.Nrodocumento = "";
        oEmpresa.RazonSocial = "";
        oEmpresa.Estado = "";
        listaEmpresa = (EN_Empresa)objEmpresa.listaEmpresa(oEmpresa,  Idpuntoventa, EN_Constante.g_const_vacio);
        return listaEmpresa;
    }
    
   
    public bool GuardarCpeSunat(OtroscpeSunat CpeSunat)
    {
        NE_GeneracionService negocioGeneracionService = new NE_GeneracionService();
        return negocioGeneracionService.GuardarCpeSunat(CpeSunat);
    }

    // Pendiente proceso de anular y consultar

    // Este proceso no debe utilizarse ya que debe estar en automàtico con la anulacion 
    public ReversionSunat CrearReversionElectronica(ReversionSunat oReversion, ref int codRspta, ref string msjeRspta,  ref string comprobante, ref bool success)
    {
        
        string  rutabase, rutadocs;
        string nombreArchivoXml = "";
        string valorResumen = "";
        string firma = "";
        EN_Empresa empresaDocumento = new EN_Empresa();
        EN_Certificado certificadoDocumento = new EN_Certificado();
        GenerarPDF creaPdf = new GenerarPDF();
        List<EN_DetalleReversion> detalleReversion = new List<EN_DetalleReversion>();
        int idreversion = 0;
        string mensaje = "";
        string cadenaXML = "";
        {
            var withBlock = oReversion;
            try
            {
                if (GenerarCpeXML.ValidarUsuarioEmpresa(oReversion.Empresa_id, oReversion.Usuario, oReversion.Clave))
                {
                    empresaDocumento = GenerarCpeXML.GetEmpresaById(withBlock.Empresa_id, withBlock.puntoventa_id);
                    certificadoDocumento = GetCertificadoByEmpresaId(empresaDocumento.Id);

                    rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                    rutadocs = string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                    //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
                    AppConfiguracion.FuncionesS3.InitialS3(empresaDocumento.Nrodocumento,certificadoDocumento.Nombre,empresaDocumento.Imagen);


                    EN_ComprobanteSunat oCompSunat = buildComprobante();

                    idreversion = GuardarReversionSunat(oReversion);
                    NE_Reversion obj = new NE_Reversion();
                    var objReversion = new EN_Reversion();
                    List<EN_Reversion> listaReversiones = new List<EN_Reversion>();
                    objReversion.Id = idreversion;
                    objReversion.Empresa_id = oReversion.Empresa_id;
                    objReversion.Estado = EN_Constante.g_const_estado_nuevo;
                    objReversion.Situacion = EN_Constante.g_const_situacion_pendiente;
                    objReversion.Correlativo = 0;
                    objReversion.CadenaAleatoria = "";
                    objReversion.UsuarioSession = "";
                    objReversion.FechaIni="";
                    objReversion.FechaFin="";
                    listaReversiones = (List<EN_Reversion>)obj.listarReversiones(objReversion);
                    if (listaReversiones.Count > 0)
                    {
                        if (GenerarCpeXML.GenerarXmlReversion(listaReversiones[0], oCompSunat, empresaDocumento, certificadoDocumento, rutadocs, rutabase, ref nombreArchivoXml,ref valorResumen, ref firma, ref mensaje, ref cadenaXML , ref comprobante))
                        {
                            if (GuardarReversionNombreValorResumenFirma(listaReversiones[0], nombreArchivoXml, valorResumen, firma, cadenaXML))
                            {
                                withBlock.NombreFichero = nombreArchivoXml;
                                withBlock.ValorResumen = valorResumen;
                                withBlock.ValorFirma = firma;
                                codRspta = 0;
                                msjeRspta = mensaje;
                                // Generamos el documento en PDF
                                detalleReversion = (List<EN_DetalleReversion>)obj.listarDetalleReversion(listaReversiones[0]);
                                creaPdf.GenerarPDFReversion(empresaDocumento, listaReversiones[0], detalleReversion, rutadocs, oCompSunat, valorResumen, firma, rutabase, "P", "");
                            }

                              success=true;
                            


                        }

                        else
                        {
                            withBlock.NombreFichero = "";
                            withBlock.ValorResumen = "";
                            withBlock.ValorFirma = "";
                            codRspta = 1;
                            msjeRspta = mensaje;
                        }
                    }
                    else
                    {
                        withBlock.NombreFichero = "";
                        withBlock.ValorResumen = "";
                        withBlock.ValorFirma = "";
                        codRspta = 1;
                        msjeRspta = "No se encuentra el comprobante electrónico generado.";
                    }
                }
                else
                {
                    withBlock.NombreFichero = "";
                    withBlock.ValorResumen = "";
                    withBlock.ValorFirma = "";
                    codRspta = 1;
                    msjeRspta = "No se ha encontrado el usuario del certificado de la empresa ingresada";
                }
            }
            catch (Exception ex)
            {
                withBlock.NombreFichero = "";
                withBlock.ValorResumen = "";
                withBlock.ValorFirma = "";
                codRspta = 1;
                msjeRspta = "Hubo una Excepción al generar la reversion:" + ex.Message;
            }
        }
        return oReversion;
    }
        public int GuardarReversionSunat(ReversionSunat ReversionSunat)
        {
            NE_GeneracionService negocioGeneracionService = new NE_GeneracionService();
            return negocioGeneracionService.GuardarReversionSunat(ReversionSunat);
        }



        public bool GuardarReversionNombreValorResumenFirma(EN_Reversion oReversion, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            NE_Reversion objReversion = new NE_Reversion();
            return objReversion.GuardarReversionNombreValorResumenFirma(oReversion, nombreXML, valorResumen, valorFirma, xmlgenerado);
        }


        public static void EnviarXmlOtrosCpeElectronico(EN_OtrosCpe oDocumento, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string produccion,string ruta, string nombre, ref string msjWA, ref int codRspta, ref string estado_comprobante)
    {
        // DESCRIPCION: Este metodo se encarga de enviar a SUNAT el comprobante generado
        string rutaXml=ruta+ EN_Constante.g_const_rutaSufijo_xml;     // RUTA XML
        string rutaCdr=ruta+ EN_Constante.g_const_rutaSufijo_cdr;     // RUTA CDR
        EN_RequestEnviodoc data=new EN_RequestEnviodoc() ;      // CLASE ENTIDAD SOLICITUD ENVIO DOCUMENTO

        // ParallelEnumerable validar xml (AQUI DEBE IR VALIDACION XSD Y XSL)
        try
        {
        
                data.flagOse= oCertificado.flagOSE;
                data.ruc= oDocumento.nrodocempresa;
                data.nombreArchivo= nombre.Trim();
                data.certUserName = oCertificado.UserName;
                data.certClave = oCertificado.Clave;
                data.Produccion= produccion;
                data.documentoId= oDocumento.id;
                data.empresaId = oDocumento.empresa_id.ToString(); 
                data.Idpuntoventa = oDocumento.puntoventa_id.ToString();
                data.tipodocumentoId = oDocumento.tipodocumento_id.ToString();
                data.fecha = oDocumento.fechaemision;
                data.Serie= oDocumento.serie;
                data.Numero= oDocumento.numero;
                
                var urlWA =  EN_ConfigConstantes.Instance.const_urlServiceOtrosCpe +  EN_ConfigConstantes.Instance.const_apiSendOtrosCpe;
                string dataSerializado = System.Text.Json.JsonSerializer.Serialize(data);

                dynamic responseWA= Configuracion.sendWebApi(urlWA, dataSerializado);
                if(responseWA.RptaRegistroCpeDoc.FlagVerificacion){
                    msjWA= responseWA.RptaRegistroCpeDoc.MensajeResp;
                    codRspta = EN_Constante.g_const_0;

                }else{
                    msjWA= "Error al enviar XML- " +responseWA.RptaRegistroCpeDoc.MensajeResp + " | Error al enviar XML-"+responseWA.ErrorWebService.DescripcionErr;
                    codRspta = EN_Constante.g_const_1;
                    
                }

                estado_comprobante= responseWA.RptaRegistroCpeDoc.estadoComprobante;
            
                
        }
        catch (Exception ex)
        {
            msjWA = "Se generó el XML - Error al enviar XML: " + ex.Message;
            codRspta = EN_Constante.g_const_2;
        
        }

    }

        
    
        
    }

}