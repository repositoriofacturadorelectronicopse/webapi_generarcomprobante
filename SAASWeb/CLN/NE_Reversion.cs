﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

using CEN;
using CAD;

namespace CLN
{
public class NE_Reversion
{
    private string pClase = "NE_Reversion";
    public object listarReversiones(EN_Reversion pReversion)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            return obj.listarReversiones(pReversion);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object listarDetalleReversion(EN_Reversion pReversion)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            return obj.listarDetalleReversion(pReversion);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public bool GuardarReversionNombreValorResumenFirma(EN_Reversion oReversion, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            return obj.GuardarReversionNombreValorResumenFirma(oReversion, nombreXML, valorResumen, valorFirma, xmlgenerado);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void GuardarReversionAceptada(EN_Reversion oReversion, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            obj.GuardarReversionAceptada(oReversion, nombrexml, xmlbase64, cdrxml, valorResumen, firma, mensaje, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void GuardarReversionRechazadaError(EN_Reversion oReversion, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            obj.GuardarReversionRechazadaError(oReversion, nombrexml, xmlbase64, cdrxml, valorResumen, firma, errortext, estado, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void ActualizarEnvioMail(EN_Reversion oReversion, string estado)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            obj.ActualizarEnvioMail(oReversion, estado);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object consultarDocumentoElectronico(EN_DocElectronico pDocumento)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            return obj.consultarDocumentoElectronico(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public List<EN_Reversion> listarAceptadoReversion(EN_Reversion pDocumento)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            return obj.listarAceptadoReversion(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public List<EN_Reversion> listarErrorRechazoReversion(EN_Reversion pDocumento)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            return obj.listarErrorRechazoReversion(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object reporteConsolidado(EN_Reversion pDocumento)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            return obj.reporteConsolidado(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object listarReversionAsociada(EN_Documento pDocumento)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            return obj.listarReversionAsociada(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public bool eliminarReversion(EN_Reversion pReversion)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            return obj.eliminarReversion(pReversion);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public List<EN_Reversion> listarErrorRechazoReversionConTicket(EN_Reversion pDocumento)
    {
        try
        {
            AD_Reversion obj = new AD_Reversion();
            return obj.listarErrorRechazoReversionConTicket(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}

}