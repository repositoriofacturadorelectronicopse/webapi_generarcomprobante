/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Ubicacion.cs
 VERSION : 1.0
 OBJETIVO: Clase de logica de negocio de ubicación
 FECHA   : 24/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/
using CAD;
using System;
namespace CLN
{
    public class NE_Ubicacion
    {
        public string ObtenerDescripcionUbicacion(int tipo, string idcodigo)
        {
        //DESCRIPCION: Listar ubicacion
        AD_Ubicacion obj = new AD_Ubicacion();
         try
            {            
             return obj.listarUbicacion(tipo, idcodigo);
            }
          catch (Exception ex)
          {
            throw ex;
         }
        }
        
    }
}