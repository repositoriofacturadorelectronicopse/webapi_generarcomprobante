﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Facturacion.cs
 VERSION : 1.0
 OBJETIVO: Clase de negocio facturación
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using CAD;
using CEN;

namespace CLN
{
  

    public partial class NE_Facturacion
    {

        public bool ValidarSession(string p_frase, string p_usuario,string p_password, ref int empresa_id, ref int puntoventa_id)
        {
            // DESCRIPCION: VALIDAR SESSION

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION

            try
            {
                return obj.validarSession( p_frase,  p_usuario, p_password, ref empresa_id, ref puntoventa_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string buscarParametroAppSettings(int codconcepto,int codcorrelativo)
        {
            // DESCRIPCION: BUSCAR PARAMETRO DE CONFIGURACION

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION

            try
            {
                return obj.buscarParametroAppSettings(codconcepto,codcorrelativo);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public object buscarParametroGeneral(int flag,int idBusqueda1 , int idBusqueda2, string stringBusqueda , int intBusqueda, bool isString=true)
        {
             // DESCRIPCION: BUSCAR DATA PARAMETRO GENERAL - NO FLAG 5

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION

            try
            {
                return obj.buscarParametroGeneral(flag, idBusqueda1 ,  idBusqueda2,  stringBusqueda ,  intBusqueda,  isString);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public object buscarCorrelativo(int empresa_id,int puntoventa_id , string tipodocumento_id, string serie, int intBusqueda, string tipodoc_afecta)            
        {
            // DESCRIPCION: BUSCAR CORRELATIVO

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION

            try
            {
                return obj.buscarCorrelativo(empresa_id,puntoventa_id ,  tipodocumento_id, serie,  intBusqueda, tipodoc_afecta);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
            

        public string buscarConstantesTablaSunat(string codtabla,string codalterno)
        {
            // DESCRIPCION: BUSCAR CONSTANTES DE LA TABLA SUNAT

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.buscarConstantesTablaSunat(codtabla,codalterno);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public string ObtenerCodigoSunat(string codtabla, string codigo)
        {
            // DESCRIPCION: OBTENER CODIGO SUNAT

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.ObtenerCodigoSunat(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string ObtenerDescripcionPorCodElemento(string codtabla, string codigo)
        {
            // DESCRIPCION: OBTENER DESCRIPCION POR CODIGO ELEMENTO

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.ObtenerDescripcionPorCodElemento(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

     

        public string ObtenerCodigoAlternoPorCodigoSistema(string codtabla, string codigo)
        {
            // DESCRIPCION: OBTENER CODIGO ALTERNO POR CODIGO DE SISTEMA

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.ObtenerCodigoAlternoPorCodigoSistema(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         public string ObtenerCodigoElementoPorCodigoSistema(string codtabla, string codigo)
        {
            // DESCRIPCION: OBTENER CODIGO ELEMENTO POR CODIGO DE SISTEMA

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.ObtenerCodigoElementoPorCodigoSistema(codtabla, codigo);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool GuardarNombreValorResumenFirma(EN_Documento oDocumento, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            // DESCRIPCION: GUARDAR NOMBRE VALOR RESUMEN DE FIRMA

             var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.GuardarNombreValorResumenFirma(oDocumento, nombreXML, valorResumen, valorFirma, xmlgenerado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string consultarSituacionCpeCB(string id, int empresa_id, int puntoventa_id)
        {
            // DESCRIPCION: CONSULTA SITUACION DE COMPROBANTE PARA PROCEDER A COMUNICADO DE BAJA

             var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.consultarSituacionCpeCB( id,  empresa_id,  puntoventa_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public object comprobarDatosDocBaja(string id, int empresa_id, int puntoventa_id, string tipodocumento_id)
        {
            // DESCRIPCION: CONSULTA SITUACION DE COMPROBANTE PARA PROCEDER A COMUNICADO DE BAJA

             var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.comprobarDatosDocBaja( id,  empresa_id,  puntoventa_id, tipodocumento_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
          public object comprobarDatosDocReversion(string id, int empresa_id, int puntoventa_id, string tipodocumento_id)
        {
            // DESCRIPCION: CONSULTA SITUACION DE COMPROBANTE PARA PROCEDER A COMUNICADO DE BAJA

             var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION
            try
            {
                return obj.comprobarDatosDocReversion( id,  empresa_id,  puntoventa_id, tipodocumento_id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
             public EN_RespuestaTablaSunat listarConstantesTablaSunat(int flag,string codtabla,string codalterno)
        {
            //DESCRIPCION: Listar constante de tabla Sunat
            EN_RespuestaTablaSunat repuestaTablaSunat = new EN_RespuestaTablaSunat();
          try
            {
                var obj = new AD_Facturacion();
                repuestaTablaSunat = obj.listarConstantesTablaSunat(flag,codtabla,codalterno);
                return repuestaTablaSunat;
            }
            catch (Exception ex)
            {
                
               throw ex;
            }

        }

        public object buscarCorrelativoOtrosCpe(int empresa_id,int puntoventa_id , string tipodocumento_id, string serie)            
        {
            // DESCRIPCION: BUSCAR CORRELATIVO

            var obj = new AD_Facturacion();        // CLASE DE CONEXION FACTURACION

            try
            {
                return obj.buscarCorrelativoOtrosCpe(empresa_id,puntoventa_id ,  tipodocumento_id, serie);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
            
        
 
  

    }
}
