/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Consulta.cs
 VERSION : 1.0
 OBJETIVO: Clase de capa de negocio consulta
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using CAD;
using CEN;
using Newtonsoft.Json;
namespace CLN
{
    public class NE_Consulta
    {
    public string getDateHourNowLima()
    {
            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);
            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString(EN_Constante.g_const_formhora);
    }
     public string   getDateNowLima()
    {

            TimeZoneInfo cstZone = TimeZoneInfo.FindSystemTimeZoneById(EN_Constante.g_cost_nameTimeDefect);

            DateTime cstTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, cstZone);
            return cstTime.ToString(EN_Constante.g_const_formfech);

    }
        
    public bool ValidarNumero(string numero)
    {
        //DESCRIPCIÓN: VALIDAR NUMERO
        try
        {
            if (string.IsNullOrWhiteSpace(numero))
            {
                return false;
            }
            else
            {
                numero = numero.Trim();
            }
            Regex reg = new Regex(EN_Constante.g_const_rango_num);//Regex
            for (int i = EN_Constante.g_const_0; i < numero.Length; i++)
            {
                if (!(reg.IsMatch(numero.Substring(i, EN_Constante.g_const_1))))
                {
                    return false;
                }
            }
            return true;
        }
        catch(Exception ex)
        {
            throw ex;
        }
        
    }

    public bool ValidaMinMaxDecimales(string numero, string minimoDecimales, string maximoDecimales)
    {
        //DESCRIPCIÓN: VALIDAR NUMERO TENGA N DECIMALES MINIMO Y MAXIMO 

       decimal convNumber; // numero convertido a decimal
       int conteoDecimal;   // condeo de decimales

		try
		{
            convNumber = Convert.ToDecimal(numero);
            if(convNumber == EN_Constante.g_const_0)
            {
                return true;
            }
            else 
            {
                conteoDecimal = convNumber.ToString().Split('.').Count() > EN_Constante.g_const_1 ? 
                                convNumber.ToString().Split('.').ToList().ElementAt(1).Length : EN_Constante.g_const_0;

                if(conteoDecimal >= Convert.ToInt32(minimoDecimales)  && conteoDecimal <= Convert.ToInt32(maximoDecimales))
                    return true;
                else return false;

            }
            
		}
		catch(System.Exception){
			
            return false;
		}

    }

    public bool ValidarFecha(string fecha)
    {
        // DESCRIPCIÓN: VALIDAR FECHA
         CultureInfo culture; // cultureinfo
        try
        {
            culture = new CultureInfo(EN_Constante.g_const_cultureEsp); 
            Convert.ToDateTime(fecha, culture);
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }
    public bool ValidarHora(string hora)
	{
        // DESCRIPCIÓN: VALIDAR HORA

		string vali="^([01]?[0-9]|2[0-3]):[0-5][0-9](:[0-5][0-9])?$";   // VALIDACION
		 try
        {
		    if (string.IsNullOrWhiteSpace(hora))
            {
                return false;
            }
            else
            {
                hora = hora.Trim();
            }
		  Regex reg = new Regex(vali);//Regex
		
		 		if (!(reg.IsMatch(hora)))
                {
                    return false;
                }
		return true;
		}
        catch(Exception ex)
        {
            throw ex;
        }
		
	}
    public bool validarEmail(string email)
    {
    string expresion = EN_Constante.g_const_exp_formcorr;
    try
    {
          if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }
            else
            {
                email = email.Trim();
            }

            if (Regex.Replace(email, expresion, String.Empty).Length == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
         
    }
    catch (Exception ex)
    {
        
        return false;
    }


    }


    public EN_Concepto ObtenerDescConcepto(int prefijo_cpe_doc, int const_error)
    {
         //DESCRIPCIÓN: Obtener descripcion de conceptos.
        AD_Concepto cad_consentimiento = new AD_Concepto();//Concepto
        EN_Concepto gbcon = new EN_Concepto();//entidad concepto
        
        gbcon = cad_consentimiento.Obtener_desc_gbcon(prefijo_cpe_doc,const_error);
        return gbcon;
    }
     public EN_ErrorWebService llenarErrorWebService(short tipoErr, int codErr, string descErr) 
        {
            //DESCRIPCION: Llenar los parametros de la clase de error de servicio
            EN_ErrorWebService errService = new EN_ErrorWebService();        //Clase error weservice
            try
            {
               errService.TipoError = tipoErr;
               errService.CodigoError = codErr;
               errService.DescripcionErr = descErr; 
               return errService;            
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
    public void SeteoParamComBaja(BajaSunat_Trama oDocumento)
    {
        //DESCRIPCION: SETEO DE PARAMETROS DE ENTRADA
        try
        {

           
            if(oDocumento.ListaDetalleBaja.Count()>0)
            {
                foreach (var itemDD in oDocumento.ListaDetalleBaja)
                {
                    if(itemDD.Tipodocumentoid==EN_Constante.g_const_tipodoc_factura)
                    {
                        oDocumento.Tiporesumen =  EN_ConfigConstantes.Instance.const_IdCB;     
                    }
                    else{
                        oDocumento.Tiporesumen = EN_ConfigConstantes.Instance.const_IdRD;
                    }
                }
            }
            
            oDocumento.Estado = EN_Constante.g_const_estado_nuevo;
            oDocumento.Situacion = EN_Constante.g_const_situacion_pendiente;
            
        }
        catch (System.Exception ex)
        {
            
            throw ex; 
        }
    }

    public void ComprobarDatosComBaja(BajaSunat_Trama oDocumento,ref bool success,  ref string msj)
    {
        // DESCRIPCION: CONSULTA SITUACION DE COMPROBANTE PARA PROCEDER A COMUNICADO DE BAJA

        NE_Facturacion facturacion = new NE_Facturacion();  // CLASE NEGOCIO FACTURACION DE
        string IdDocumento = EN_Constante.g_const_vacio;                             // ID DE DOCUMENTOS
        int empresa_id = EN_Constante.g_const_0;                                 // ID DE EMPRESA
        int puntoventa_id = EN_Constante.g_const_0 ;                             // ID PUNTO DE VENTA
        List<EN_CompDatosBaja> listaData = new List<EN_CompDatosBaja>();        // LISTA DE CLASE ENTIDAD COMPROBACION DATOS DE BAJA
        EN_CompDatosBaja data = new EN_CompDatosBaja();                       //    CLASE ENTIDAD COMPROBACION DATOS DE BAJA
        int conteobajas = EN_Constante.g_const_0;

        int conteodistTA=EN_Constante.g_const_0;                                // CONTEO DE DISTINTOS TIPOS DE COMPROBANTE AFECTO A LA NOTA
        int conteodistTDID=EN_Constante.g_const_0;                               // CONTEO DE DISTINTOS TIPOS DE DOCUMENTO ID
        int indicador= EN_Constante.g_const_0;                                  // INDICADOR 
        string tipodoc_afecta=EN_Constante.g_const_vacio;     

        try
        {

            empresa_id = Convert.ToInt32(oDocumento.Empresa_id);
            puntoventa_id = Convert.ToInt32(oDocumento.Idpuntoventa);

            conteobajas = oDocumento.ListaDetalleBaja.Count();

            if(oDocumento.ListaDetalleBaja.Count()>0)
            {

                //recorrido para revisar tipo de documento  
                dynamic linq_tdid = oDocumento.ListaDetalleBaja.Select(x=>x.Tipodocumentoid).Distinct();
                foreach(var dtTAGN in linq_tdid)
                {
                    conteodistTDID++;
                }
                if(conteodistTDID > EN_Constante.g_const_1)
                {
                    msj= EN_Constante.g_const_err_cpediferentestipodocsid_cb;
                    success=false;
                    return;
                }


                foreach (var itemDD in oDocumento.ListaDetalleBaja)
                {

                    IdDocumento = itemDD.Tipodocumentoid.ToString().Trim()+itemDD.Serie.ToString().Trim()+"-"+itemDD.Numero.ToString().Trim();   
                    data = (EN_CompDatosBaja)facturacion.comprobarDatosDocBaja(IdDocumento,empresa_id, puntoventa_id, itemDD.Tipodocumentoid.ToString().Trim());
                    listaData.Add(data);
                }
                
                foreach (var itemDD in oDocumento.ListaDetalleBaja)
                {
                    IdDocumento = itemDD.Tipodocumentoid.ToString().Trim()+itemDD.Serie.ToString().Trim()+"-"+itemDD.Numero.ToString().Trim();   
                     dynamic dataRec=listaData.Where(dt => IdDocumento == dt.id  );
                     foreach (var dr in dataRec)
                     {
                         indicador++;
                      
                            if(oDocumento.FechaDocumento!=dr.fecha)
                            {   
                                msj= EN_Constante.g_const_err_cpefechadiferente_cb+IdDocumento;
                                success=false;
                                return;
                            }
                     }
                     
                }
                    //rteVISA
                   dynamic distTA = listaData.Select(x=>x.tipodoc_afecta).Distinct();
                   
                                           //
                   foreach(var dtTA in distTA)
                   {
                       conteodistTA++;
                       tipodoc_afecta=dtTA;

                   }
                   if(conteodistTA > EN_Constante.g_const_1)
                   {
                        msj= EN_Constante.g_const_err_cpediferentestiposafecto_cb;
                        success=false;
                        return;
                   }
              
                
                   if(conteobajas !=indicador)
                    {
                        msj= EN_Constante.g_const_err_cpenoexiste_cb;
                        success=false;
                        return;
                    }
                    else{

                        success=true;
                    }

            }

            
        }
        catch (System.Exception ex)
        {
            
            throw ex; 
        }
    }

    public void ComprobarFechaComDocBaja(BajaSunat_Trama oDocumento,ref bool success,  ref string msj)
    {
        NE_Empresa nE_Empresa = new NE_Empresa();               // clase negocio de empresa
        EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
        EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
        int diasPermitidos=0;                               //Dias permitidos de envío de comunicado de baja
        DateTime fechaComunicacion;                             //  fecha de comunicación de baja
        DateTime fechaDocumento;                                // fecha del documento a dar de baja
        TimeSpan difFechas;
        int diasCalculado;
         //buscamos en tabla parametro si enviamos en linea las boletas
        bus_par.par_conceptopfij=EN_Constante.g_const_1;
        bus_par.par_conceptocorr=EN_Constante.g_const_114;
       
        res_par =nE_Empresa.buscar_tablaParametro(bus_par);
        diasPermitidos = (res_par.par_descripcion=="" || res_par.par_descripcion==null)?0:Convert.ToInt32(res_par.par_descripcion);;  

        fechaComunicacion= DateTime.ParseExact(oDocumento.FechaComunicacion.ToString(),"dd/MM/yyyy",new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru) );
        fechaDocumento= DateTime.ParseExact(oDocumento.FechaDocumento.ToString(),"dd/MM/yyyy",new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru) );


        

        difFechas= fechaComunicacion -fechaDocumento;
        diasCalculado = difFechas.Days;
        if(diasCalculado > (diasPermitidos*-1))
        {
           success=false;
            msj   ="Fecha del documento de baja pasa los "+(diasPermitidos*-1).ToString()+" días permitidos con respecto a la fecha de comunicación.";

        }
        else{
            success=true;
            msj="";
        }

         

    }

    public void ComprobarDatosComReversion(ReversionSunatTrama oDocumento, ref bool success,  ref string msj)
    {
        // DESCRIPCION: CONSULTA SITUACION DE COMPROBANTE PARA PROCEDER A COMUNICADO DE BAJA

        NE_Facturacion facturacion = new NE_Facturacion();  // CLASE NEGOCIO FACTURACION DE
        string IdDocumento = EN_Constante.g_const_vacio;                             // ID DE DOCUMENTOS
        int empresa_id = EN_Constante.g_const_0;                                 // ID DE EMPRESA
        int puntoventa_id = EN_Constante.g_const_0 ;                             // ID PUNTO DE VENTA
        List<EN_CompDatosBaja> listaData = new List<EN_CompDatosBaja>();        // LISTA DE CLASE ENTIDAD COMPROBACION DATOS DE BAJA
        EN_CompDatosBaja data = new EN_CompDatosBaja();                       //    CLASE ENTIDAD COMPROBACION DATOS DE BAJA
        int conteobajas = EN_Constante.g_const_0;
        int conteodistTDID=EN_Constante.g_const_0;                               // CONTEO DE DISTINTOS TIPOS DE DOCUMENTO ID
        int indicador= EN_Constante.g_const_0;                                  // INDICADOR 
          string[] grupoTipoCpe;                                                // strings de grupo de tipo de comprobantes permitidos
        try 
        {
            empresa_id = Convert.ToInt32(oDocumento.Empresa_id);
            puntoventa_id = Convert.ToInt32(oDocumento.puntoventa_id);
            conteobajas = oDocumento.ListaDetalleReversion.Count();

            //comprobamos q sean del mismo tipo de documento 20 o 40
            grupoTipoCpe = new[] {  EN_ConfigConstantes.Instance.const_IdCP,  EN_ConfigConstantes.Instance.const_IdCR };

            if(oDocumento.ListaDetalleReversion.Count()>0)
            {


                foreach (var item in oDocumento.ListaDetalleReversion)
                {


                    if(!grupoTipoCpe.Contains(item.Tipodocumentoid.Trim()))
                    {
                        msj= "Tipo de Comprobante deben ser Percepción o Retención.";
                        success=false;
                        return;

                    }
                }


                //recorrido para revisar tipo de documento  
                dynamic linq_tdid = oDocumento.ListaDetalleReversion.Select(x=>x.Tipodocumentoid).Distinct();
                foreach(var dtTAGN in linq_tdid)
                {
                    conteodistTDID++;
                }
                if(conteodistTDID > EN_Constante.g_const_1)
                {
                    msj= EN_Constante.g_const_err_cpediferentestipodocsid_cb;
                    success=false;
                    return;
                }

                 foreach (var itemDD in oDocumento.ListaDetalleReversion)
                {

                    IdDocumento = itemDD.Tipodocumentoid.ToString().Trim()+itemDD.Serie.ToString().Trim()+"-"+itemDD.Numero.ToString().Trim();   
                    data = (EN_CompDatosBaja)facturacion.comprobarDatosDocReversion(IdDocumento,empresa_id, puntoventa_id, itemDD.Tipodocumentoid.ToString().Trim());
                    listaData.Add(data);
                }
                
                foreach (var itemDD in oDocumento.ListaDetalleReversion)
                {
                    IdDocumento = itemDD.Tipodocumentoid.ToString().Trim()+itemDD.Serie.ToString().Trim()+"-"+itemDD.Numero.ToString().Trim();   
                     dynamic dataRec=listaData.Where(dt => IdDocumento == dt.id  );
                     foreach (var dr in dataRec)
                     {
                         indicador++;
                      
                            if(oDocumento.FechaDocumento!=dr.fecha)
                            {   
                                msj= EN_Constante.g_const_err_cpefechadiferente_cb+IdDocumento;
                                success=false;
                                return;
                            }
                     }
                     
                }
                if(conteobajas !=indicador)
                {
                    msj= EN_Constante.g_const_err_cpenoexiste_cb;
                    success=false;
                    return;
                }
                else{

                    success=true;
                }

                
             
              
                
                

            }

            
        }
        catch (System.Exception ex)
        {
            
            throw ex; 
        }
    }

     public void ComprobarFechaComDocReversion(ReversionSunatTrama oDocumento,ref bool success,  ref string msj)
    {
        NE_Empresa nE_Empresa = new NE_Empresa();               // clase negocio de empresa
        EN_Parametro res_par = new EN_Parametro();              // clase entidad parametro para resultado
        EN_Parametro bus_par = new EN_Parametro();              // clase entidad parametro para búsqueda
        int diasPermitidos=0;                               //Dias permitidos de envío de comunicado de baja
        DateTime fechaComunicacion;                             //  fecha de comunicación de baja
        DateTime fechaDocumento;                                // fecha del documento a dar de baja
        TimeSpan difFechas;
        int diasCalculado;
         //buscamos en tabla parametro si enviamos en linea las boletas
        bus_par.par_conceptopfij=EN_Constante.g_const_1;
        bus_par.par_conceptocorr=EN_Constante.g_const_115;
       
        res_par =nE_Empresa.buscar_tablaParametro(bus_par);
        diasPermitidos = (res_par.par_descripcion=="" || res_par.par_descripcion==null)?0:Convert.ToInt32(res_par.par_descripcion);;  

        fechaComunicacion= DateTime.ParseExact(oDocumento.FechaComunicacion.ToString(),"dd/MM/yyyy",new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru) );
        fechaDocumento= DateTime.ParseExact(oDocumento.FechaDocumento.ToString(),"dd/MM/yyyy",new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru) );


        

        difFechas= fechaComunicacion -fechaDocumento;
        diasCalculado = difFechas.Days;
        if(diasCalculado > (diasPermitidos*-1))
        {
           success=false;
           msj   ="Fecha del documento de baja pasa los "+(diasPermitidos*-1).ToString()+" dias permitidos con respecto a la fecha de comunicación.";

        }
        else{
            success=true;
            msj="";
        }

         

    }

    public EN_ResponseGuia seteoParametrosGuia(GuiaSunatTrama oguiaTrama) 
    {
    //DESCRIPCION: SETEO DE PARAMETROS DE ENTRADA
       bool flagSerie = EN_Constante.g_const_false;            // FLAG PARA VERIFICACION DE SERIES EN TABLA PARAMETRO 
       string serieSet=EN_Constante.g_const_vacio;             //  SERIE SETEADO
       string correlativoSet=EN_Constante.g_const_vacio;       //  CORRELATIVO SETEADO
       NE_Facturacion objFacturacion = new NE_Facturacion();   // CLASE DE NEGOCIO FACTURACION
       NE_Guia guia = new NE_Guia();
       EN_ResponseGuia respuestaGuia = new EN_ResponseGuia();
       EN_Concepto concepto = new EN_Concepto();
       EN_ErrorWebService errWebService = new EN_ErrorWebService();
       EN_RespuestaData respuesta = new EN_RespuestaData();
       try
       {
        if (oguiaTrama.empresa_id == null || oguiaTrama.empresa_id.Trim().Length == EN_Constante.g_const_0)
        {
                respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_idEmpresa;
                concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                respuestaGuia.ErrorWebServ = errWebService;
                return respuestaGuia;

        } else if (!ValidarNumero(oguiaTrama.empresa_id)) 
        {
            respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
            respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_idEmpresa;
            concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
            errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
            respuestaGuia.ErrorWebServ = errWebService;
            return respuestaGuia;

        } else if (oguiaTrama.tipodocumento_id == null || oguiaTrama.tipodocumento_id.Trim() == "") 
        {
            respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
            respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocumento;
            concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
            errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
            respuestaGuia.ErrorWebServ = errWebService;
            return respuestaGuia;

        } else if(oguiaTrama.tipodocumento_id.Trim().Length != EN_Constante.g_const_2) 
        {
            respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
            respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocumento;
            concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
            errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
            respuestaGuia.ErrorWebServ = errWebService;
            return respuestaGuia;

        } else if (!ValidarNumero(oguiaTrama.tipodocumento_id)){
            respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
            respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocumento;
            concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
            errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
            respuestaGuia.ErrorWebServ = errWebService;
            return respuestaGuia;

        } else if(oguiaTrama.tipodocumento_id != "09") 
            {
            respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
            respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocumento;
            concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
            errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
            respuestaGuia.ErrorWebServ = errWebService;
            return respuestaGuia;

        } else if (oguiaTrama.puntoemision == null || oguiaTrama.puntoemision.Trim().Length == EN_Constante.g_const_0) 
            {
            respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
            respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_punto_emision;
            concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
            errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
            respuestaGuia.ErrorWebServ = errWebService;
            return respuestaGuia;

        }
         else if (!ValidarNumero(oguiaTrama.puntoemision)) 
        {
            respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
            respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_punto_emision;
            concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
            errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
            respuestaGuia.ErrorWebServ = errWebService;
            return respuestaGuia;

        } else {

           flagSerie= Convert.ToBoolean(objFacturacion.buscarParametroGeneral(EN_Constante.g_const_8, Convert.ToInt32(oguiaTrama.empresa_id), Convert.ToInt32(oguiaTrama.puntoemision),  EN_Constante.g_const_vacio ,  EN_Constante.g_const_0,  EN_Constante.g_const_false));
           if(flagSerie) {
               oguiaTrama.serie = EN_Constante.g_const_vacio;
               serieSet=(string) objFacturacion.buscarParametroGeneral(EN_Constante.g_const_9, Convert.ToInt32(oguiaTrama.empresa_id), Convert.ToInt32(oguiaTrama.puntoemision),  oguiaTrama.tipodocumento_id ,   EN_Constante.g_const_0,  EN_Constante.g_const_true);

           } else
           {
                serieSet=oguiaTrama.serie;    
           }

            respuesta = guia.buscarCorrelativo(Convert.ToInt32(oguiaTrama.empresa_id),Convert.ToInt32(oguiaTrama.puntoemision),oguiaTrama.tipodocumento_id,serieSet);
            
            if (respuesta.data.flag) 
            {
                    correlativoSet= respuesta.data.dato.ToString("D8");

                    oguiaTrama.serie = serieSet;    
                    oguiaTrama.numero= correlativoSet;
                    oguiaTrama.id =oguiaTrama.tipodocumento_id+serieSet+"-"+correlativoSet;

                if(oguiaTrama.ListaDetalleGuia.Count()>0)
                {
                    foreach (var itemDD in oguiaTrama.ListaDetalleGuia)
                    {
                        itemDD.guia_id= oguiaTrama.tipodocumento_id+serieSet+"-"+correlativoSet;
                        itemDD.empresa_id= oguiaTrama.empresa_id;
                        itemDD.tipodocumento_id= oguiaTrama.tipodocumento_id;
                    }
                }

            } else {
                respuestaGuia.RespGuiaElectronica.FlagVerificacion = respuesta.data.flag;
                respuestaGuia.RespGuiaElectronica.DescRespuesta = respuesta.data.DescRespuesta;
                respuestaGuia.ErrorWebServ = respuesta.ErrorWebServ;
                return respuestaGuia;
            }
         
        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_2000);
        respuestaGuia.RespGuiaElectronica.FlagVerificacion = true;
        respuestaGuia.RespGuiaElectronica.DescRespuesta = concepto.conceptodesc;
                        
        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
        respuestaGuia.ErrorWebServ = errWebService;

        }

          
        return respuestaGuia;
       }
       catch (Exception ex)
       {
           
           throw;
       }

    }
    public void SeteoParam(DocumentoSunatTrama oDocumento)
    {
        //DESCRIPCION: SETEO DE PARAMETROS DE ENTRADA

        NE_Facturacion objFacturacion = new NE_Facturacion();   // CLASE DE NEGOCIO FACTURACION
        bool flagSerie = EN_Constante.g_const_false;            // FLAG PARA VERIFICACION DE SERIES EN TABLA PARAMETRO 
        string serieSet=EN_Constante.g_const_vacio;             //  SERIE SETEADO
        int correlativoTmp=EN_Constante.g_const_0 ;             //  CORRELATIVO temporal
        string correlativoSet=EN_Constante.g_const_vacio;       //  CORRELATIVO SETEADO
        string tipodoc_afecta = EN_Constante.g_const_vacio;     //  TIPO DE DOCUMENTO AFECTO A NOTA DE CREDITO O DEBITO
        bool has_listaDocRefeguia= false;                       // BOOLEAN PARA VERIFICAR LISTADO DOCUMENTO REFERENCIA GUIA
        bool has_ListaDocumentoAnticipo=false;                  // BOOLEAN PARA VERIFICAR LISTADO DOCUMENTO DE ANTICIPOS
        bool has_listadDocReferencia = false;                   // BOOLEAN PARA VERIFICAR LISTADO DOCUMENTO REFERENCIA 
        try
        {
            
            flagSerie= Convert.ToBoolean(objFacturacion.buscarParametroGeneral(EN_Constante.g_const_8, Convert.ToInt32(oDocumento.Idempresa), Convert.ToInt32(oDocumento.Idpuntoventa),  EN_Constante.g_const_vacio ,  EN_Constante.g_const_0,  EN_Constante.g_const_false));
            if(flagSerie)
            {
                oDocumento.Serie="";
                serieSet=(string) objFacturacion.buscarParametroGeneral(EN_Constante.g_const_9, Convert.ToInt32(oDocumento.Idempresa), Convert.ToInt32(oDocumento.Idpuntoventa),  oDocumento.Idtipodocumento ,   Convert.ToInt32(oDocumento.Idtipodocumentonota),  EN_Constante.g_const_true);

                correlativoTmp =(int)objFacturacion.buscarCorrelativo(Convert.ToInt32(oDocumento.Idempresa), Convert.ToInt32(oDocumento.Idpuntoventa), oDocumento.Idtipodocumento, serieSet, Convert.ToInt32(oDocumento.Idtipodocumentonota), tipodoc_afecta);

                correlativoSet= correlativoTmp.ToString("D8");
            }
            else
            {
                serieSet=oDocumento.Serie;    
                correlativoSet=oDocumento.Numero;
            }

            if(oDocumento.Idtipodocumento==EN_Constante.g_const_tipodoc_nc || oDocumento.Idtipodocumento==EN_Constante.g_const_tipodoc_nd)
            {
                if(oDocumento.Idtipodocumentonota==EN_Constante.g_const_1.ToString()) tipodoc_afecta=EN_Constante.g_const_tipodoc_factura; else tipodoc_afecta=EN_Constante.g_const_tipodoc_boleta;
            }
            
           

                oDocumento.Serie = serieSet;    
                oDocumento.Numero= correlativoSet;
                oDocumento.Id =oDocumento.Idtipodocumento+serieSet+"-"+correlativoSet;

            if(oDocumento.ListaDetalleDocumento.Count()>0)
            {
                foreach (var itemDD in oDocumento.ListaDetalleDocumento)
                {
                    itemDD.Iddocumento= oDocumento.Idtipodocumento+serieSet+"-"+correlativoSet;
                    itemDD.Idempresa= Convert.ToInt32(oDocumento.Idempresa);
                    itemDD.Idtipodocumento= oDocumento.Idtipodocumento;
                }
            }
            
         
            if(oDocumento.ListaDocRefeGuia.Count()>0)
            {
                 
                foreach (var itemDRG in oDocumento.ListaDocRefeGuia)
                {
                    if(itemDRG.tipo_guia.ToString().Length>0 && itemDRG.numero_guia.ToString().Length>0)
                    {
                        itemDRG.documento_id= oDocumento.Idtipodocumento+serieSet+"-"+correlativoSet;
                        itemDRG.empresa_id= Convert.ToInt32(oDocumento.Idempresa);
                        itemDRG.tipodocumento_id= oDocumento.Idtipodocumento;
                        has_listaDocRefeguia= true;
                    }
                    else{
                        has_listaDocRefeguia= false;
                    }

             
                }
            }

            if(!has_listaDocRefeguia) oDocumento.ListaDocRefeGuia= null;

            
            if(oDocumento.ListaDocumentoAnticipo.Count()>0)
            {
                foreach (var itemDA in oDocumento.ListaDocumentoAnticipo)
                {
                    if(Convert.ToInt32(itemDA.line_id)>0 &&  Convert.ToDecimal(itemDA.montoanticipo)>0 && itemDA.tipodocanticipo.ToString().Length >0 )
                    {
                        itemDA.documento_id= oDocumento.Idtipodocumento+serieSet+"-"+correlativoSet;
                        itemDA.empresa_id= Convert.ToInt32(oDocumento.Idempresa);
                        itemDA.tipodocumento_id= oDocumento.Idtipodocumento;
                        
                        has_ListaDocumentoAnticipo=true;

                    }
                    else
                    {
                        has_ListaDocumentoAnticipo=false;

                    }
                    
                }
            }

            if(!has_ListaDocumentoAnticipo) oDocumento.ListaDocumentoAnticipo= null;

            if(oDocumento.ListaDocumentoReferencia.Count()>0)
            {
                foreach (var itemDR in oDocumento.ListaDocumentoReferencia)
                {
                 
                    if(itemDR.Iddocumentoref.Trim().Count()>0 && itemDR.Idtipodocumentoref.ToString().Length>0 && itemDR.Numerodocref.ToString().Length>0)
                    {
                        itemDR.Iddocumento= oDocumento.Idtipodocumento+serieSet+"-"+correlativoSet;
                        itemDR.Idempresa= Convert.ToInt32(oDocumento.Idempresa);
                        itemDR.Idtipodocumento= oDocumento.Idtipodocumento;

                        has_listadDocReferencia=true;
                    }
                    else{
                        has_listadDocReferencia=false;
                    }
                }
            }

             if(!has_listadDocReferencia) oDocumento.ListaDocumentoReferencia= null;
             
        }
        catch (System.Exception ex)
        {
            
            throw ex;
        }
    }
      public void SeteoParamOtrosCpe(OtroscpeSunatTrama oDocumento)
    {
        //DESCRIPCION: SETEO DE PARAMETROS DE ENTRADA

        NE_Facturacion objFacturacion = new NE_Facturacion();   // CLASE DE NEGOCIO FACTURACION
        bool flagSerie = EN_Constante.g_const_false;            // FLAG PARA VERIFICACION DE SERIES EN TABLA PARAMETRO 
        string serieSet=EN_Constante.g_const_vacio;             //  SERIE SETEADO
        int correlativoTmp=EN_Constante.g_const_0 ;             //  CORRELATIVO temporal
        string correlativoSet=EN_Constante.g_const_vacio;       //  CORRELATIVO SETEADO
       
        try
        {
            
            flagSerie= Convert.ToBoolean(objFacturacion.buscarParametroGeneral(EN_Constante.g_const_8, Convert.ToInt32(oDocumento.empresa_id), Convert.ToInt32(oDocumento.puntoventa_id),  EN_Constante.g_const_vacio ,  EN_Constante.g_const_0,  EN_Constante.g_const_false));
            if(flagSerie)
            {
                oDocumento.serie="";
                serieSet=(string) objFacturacion.buscarParametroGeneral(
                    EN_Constante.g_const_9, 
                    Convert.ToInt32(oDocumento.empresa_id), 
                    Convert.ToInt32(oDocumento.puntoventa_id),  
                    oDocumento.tipodocumento_id ,   
                    Convert.ToInt32(EN_Constante.g_const_0), 
                     EN_Constante.g_const_true);

                correlativoTmp =(int)objFacturacion.buscarCorrelativoOtrosCpe(Convert.ToInt32(oDocumento.empresa_id), Convert.ToInt32(oDocumento.puntoventa_id), oDocumento.tipodocumento_id, serieSet);
                correlativoSet= correlativoTmp.ToString("D8");
            }
            else
            {
                serieSet=oDocumento.serie;  
                correlativoSet=oDocumento.numero;  
            }

            
          

                oDocumento.serie = serieSet;    
                oDocumento.numero= correlativoSet;
                oDocumento.id =oDocumento.tipodocumento_id+serieSet+"-"+correlativoSet;

            if(oDocumento.ListaDetalleOtrosCpe.Count()>0)
            {
                foreach (var itemDD in oDocumento.ListaDetalleOtrosCpe)
                {
                    itemDD.otroscpe_id= oDocumento.tipodocumento_id+serieSet+"-"+correlativoSet;
                    itemDD.empresa_id= oDocumento.empresa_id.ToString();
                    itemDD.tipodocumento_id= oDocumento.tipodocumento_id;
                }
            }
            
           
             
        }
        catch (System.Exception ex)
        {
            
            throw ex;
        }
    }

    public DocumentoSunat ParsearCamposDoc(DocumentoSunatTrama documento)
    {
         //DESCRIPCIÓN: CAST TIPO DE VARIABLES DE PARAMETROS DE ENTRADA A DOCUMENTOSUNAT
        

        try
        {
            var serializerSettings = new JsonSerializerSettings{NullValueHandling = NullValueHandling.Ignore};
            DocumentoSunat doc =  JsonConvert.DeserializeObject<DocumentoSunat>(JsonConvert.SerializeObject(documento, Formatting.Indented, serializerSettings));
            return doc;
        }
        catch(Exception ex)
        {
            throw ex;
        }

        
    }


    public OtroscpeSunat ParsearCamposOtrosCpe(OtroscpeSunatTrama documento)
    {
         //DESCRIPCIÓN: CAST TIPO DE VARIABLES DE PARAMETROS DE ENTRADA A OtroscpeSunat
        

        try
        {
            var serializerSettings = new JsonSerializerSettings{NullValueHandling = NullValueHandling.Ignore};
            OtroscpeSunat doc =  JsonConvert.DeserializeObject<OtroscpeSunat>(JsonConvert.SerializeObject(documento, Formatting.Indented, serializerSettings));
            return doc;
        }
        catch(Exception ex)
        {
            throw ex;
        }

        
    }

     public ReversionSunat ParsearCamposReversion(ReversionSunatTrama documento)
    {
         //DESCRIPCIÓN: CAST TIPO DE VARIABLES DE PARAMETROS DE ENTRADA A OtroscpeSunat
        

        try
        {
            var serializerSettings = new JsonSerializerSettings{NullValueHandling = NullValueHandling.Ignore};
            ReversionSunat doc =  JsonConvert.DeserializeObject<ReversionSunat>(JsonConvert.SerializeObject(documento, Formatting.Indented, serializerSettings));
            return doc;
        }
        catch(Exception ex)
        {
            throw ex;
        }

        
    }

    public BajaSunat ParsearCamposComunicadosBaja(BajaSunat_Trama documento)
    {
         //DESCRIPCIÓN: CAST TIPO DE VARIABLES DE PARAMETROS DE ENTRADA A OtroscpeSunat
        

        try
        {
            var serializerSettings = new JsonSerializerSettings{NullValueHandling = NullValueHandling.Ignore};
            BajaSunat doc =  JsonConvert.DeserializeObject<BajaSunat>(JsonConvert.SerializeObject(documento, Formatting.Indented, serializerSettings));
            return doc;
        }
        catch(Exception ex)
        {
            throw ex;
        }

        
    }

    public GuiaSunat ParsearCamposGuia (GuiaSunatTrama guia)
    {
        //DESCRIPCIÓN: CAST TIPO DE VARIABLES DE PARAMETROS DE ENTRADA A DOCUMENTOSUNAT
         try
        {
            var serializerSettings = new JsonSerializerSettings{NullValueHandling = NullValueHandling.Ignore};
            GuiaSunat doc =  JsonConvert.DeserializeObject<GuiaSunat>(JsonConvert.SerializeObject(guia, Formatting.Indented, serializerSettings));
            return doc;
        }
        catch(Exception ex)
        {
            throw ex;
        }


    }
    


    
     public ClassRptaGenerarCpeDoc ValidarCamposGenerarOtrosCpeDoc(OtroscpeSunatTrama documento)
    {
         //DESCRIPCIÓN: Validar campos generar comprobante electronico documentos.

        ClassRptaGenerarCpeDoc RespuestaCpeDoc = new ClassRptaGenerarCpeDoc();//Respuesta
        EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();//Error
        EN_Concepto gbcon = new EN_Concepto();//entidad concepto

        try
        {
            if (documento == null) {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_error_tramaGeneral;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

              if (documento.empresa_id.ToString().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE EMPRESA_ID EN TRAMA DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else if (!ValidarNumero(documento.empresa_id.ToString()) || Convert.ToInt32(documento.empresa_id) == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE EMPRESA_ID EN TRAMA DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            if ( documento.puntoventa_id.ToString().Length == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PUNTOVENTA_ID EN TRAMA DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else if (!ValidarNumero(documento.puntoventa_id.ToString())  || Convert.ToInt32(documento.puntoventa_id) == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PUNTOVENTA_ID EN TRAMA DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.tipodocumento_id == null || documento.tipodocumento_id.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE TIPODOCUMENTO_ID EN TRAMA DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

              SeteoParamOtrosCpe(documento);


             if (documento.serie == null || documento.serie.Trim().Length== EN_Constante.g_const_0 || documento.serie.Trim().Length>EN_Constante.g_const_4 || documento.serie.Trim().Length<EN_Constante.g_const_4 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_serie_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            if (documento.serie.Trim().Length== EN_Constante.g_const_4 )
            {
                char pL= char.Parse(documento.serie.Substring(0,1));
                if(!Char.IsLetter(pL))
                {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_serie_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
                }
            }
            if (documento.numero == null || documento.numero.Trim().Length != EN_Constante.g_const_length_numero_cpe_doc )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_numero_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            
            if (documento.id == null || documento.id.Trim().Length == EN_Constante.g_const_0 || documento.id.Trim().Length != EN_Constante.g_const_length_id_cpe_doc)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idcpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Usuario == null || documento.Usuario.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE USUARIO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Clave == null || documento.Clave.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp ="POR FAVOR VERIFIQUE CLAVE EN TRAMA DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

             else if (documento.fechaemision == null || documento.fechaemision.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE FECHAEMISION EN TRAMA DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.persona_tipodoc == null || documento.persona_tipodoc.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_TIPODOC DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.persona_nrodoc == null || documento.persona_nrodoc.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_NRODOC DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.persona_nombrecomercial == null || documento.persona_nombrecomercial.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_NOMBRECOMERCIAL DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

              else if (documento.persona_ubigeo == null || documento.persona_ubigeo.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_UBIGEO DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.persona_direccion == null || documento.persona_direccion.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_DIRECCION DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.persona_urbanizacion == null || documento.persona_urbanizacion.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_URBANIZACION DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.persona_departamento == null || documento.persona_departamento.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_DEPARTAMENTO DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.persona_provincia == null || documento.persona_provincia.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_PROVINCIA DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
              else if (documento.persona_distrito == null || documento.persona_distrito.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_DISTRITO DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.persona_distrito == null || documento.persona_distrito.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_DISTRITO DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.persona_codpais == null || documento.persona_codpais.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_CODPAIS DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.persona_descripcion == null || documento.persona_descripcion.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PERSONA_DESCRIPCION DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.regimen_retper == null || documento.regimen_retper.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE REGIMEN_RETPER DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else if (documento.tasa_retper == null || documento.tasa_retper.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE TASA_RETPER DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.importe_retper == null || documento.importe_retper.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE IMPORTE_RETPER DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.moneda_impretper == null || documento.moneda_impretper.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE MONEDA_IMPRETPER DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.importe_pagcob == null || documento.importe_pagcob.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE IMPORTE_PAGCOB DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.importe_pagcob == null || documento.importe_pagcob.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE IMPORTE_PAGCOB DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

              else if (documento.moneda_imppagcob == null || documento.moneda_imppagcob.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE MONEDA_IMPPAGCOB DE DOCUMENTO ELECTRONICO.";
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
               else if (documento.estado==null || documento.estado.Trim().Length== EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_estado_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.situacion == null || documento.situacion.Trim().Length== EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_situacion_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }


              foreach(var item in documento.ListaDetalleOtrosCpe)
            {
                 if (item.otroscpe_id == null || item.otroscpe_id.Trim().Length == EN_Constante.g_const_0 || item.otroscpe_id.Trim().Length != EN_Constante.g_const_length_id_cpe_doc)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE OTROSCPE_ID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.empresa_id == null ||item.empresa_id.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE EMPRESA_ID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (!ValidarNumero(item.empresa_id.ToString()))
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE EMPRESA_ID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.tipodocumento_id == null || item.tipodocumento_id.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp ="POR FAVOR VERIFIQUE TIPODOCUMENTO_ID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                else if (item.line_id.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE LINE_ID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (!ValidarNumero(item.line_id.ToString()))
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE LINE_ID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.docrelac_tipodoc_id == null || item.docrelac_tipodoc_id.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE DOCRELAC_TIPODOC_ID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.docrelac_numerodoc == null || item.docrelac_numerodoc.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp ="POR FAVOR VERIFIQUE DOCRELAC_NUMERODOC DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.docrelac_fechaemision == null || item.docrelac_fechaemision.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE DOCRELAC_FECHAEMISION DEL DOCUMENTO RELACIONADO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                else if (item.docrelac_importetotal == null || item.docrelac_importetotal.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE DOCRELAC_IMPORTETOTAL DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.docrelac_moneda == null || item.docrelac_moneda.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE DOCRELAC_MONEDA DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.pagcob_fecha.ToString() == null || item.pagcob_fecha.ToString().Length == EN_Constante.g_const_0 )
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PAGCOB_FECHA DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                  else if (item.pagcob_numero.ToString()==null || item.pagcob_numero.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PAGCOB_NUMERO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.pagcob_importe.ToString()==null || item.pagcob_importe.Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PAGCOB_IMPORTE DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                
                else if (item.pagcob_moneda.ToString()==null || item.pagcob_moneda.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE PAGCOB_MONEDA DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }

                 else if (item.retper_importe.ToString()==null || item.retper_importe.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE RETPER_IMPORTE DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.retper_moneda.ToString()==null || item.retper_moneda.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE RETPER_MONEDA DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                  else if (item.retper_fecha.ToString()==null || item.retper_fecha.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE RETPER_FECHA DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                else if (item.retper_importe_pagcob.ToString()==null || item.retper_importe_pagcob.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE RETPER_IMPORTE_PAGCOB DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.retper_moneda_pagcob.ToString()==null || item.retper_moneda_pagcob.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE RETPER_MONEDA_PAGCOB DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }

                 else if (item.tipocambio_monedaref.ToString()==null || item.tipocambio_monedaref.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE TIPOCAMBIO_MONEDAREF DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.tipocambio_monedaobj.ToString()==null || item.tipocambio_monedaobj.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE TIPOCAMBIO_MONEDAOBJ DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.tipocambio_factor.ToString()==null || item.tipocambio_factor.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE TIPOCAMBIO_FACTOR DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.tipocambio_fecha.ToString()==null || item.tipocambio_fecha.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = "POR FAVOR VERIFIQUE TIPOCAMBIO_FECHA DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 
                
            }
           
           
                
            gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
            RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = gbcon.conceptodesc;
            RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = true;
            RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
            ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
            RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
            return RespuestaCpeDoc;
            



        }
        catch(Exception e)
        {
            return RespuestaCpeDoc;

        }


    }


    public ClassRptaGenerarCpeDoc ValidarCamposGenerarDoc(DocumentoSunatTrama documento)
    {
        //DESCRIPCIÓN: Validar campos generar comprobante electronico documentos.

        ClassRptaGenerarCpeDoc RespuestaCpeDoc = new ClassRptaGenerarCpeDoc();//Respuesta
        EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();//Error
        EN_Concepto gbcon = new EN_Concepto();//entidad concepto
        char letraTipoDoc; //primera letra de Serie


        try
        {
                      
            if (documento == null) {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_error_tramaGeneral;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

             if (documento.Idempresa.ToString().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idempresa_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else if (!ValidarNumero(documento.Idempresa.ToString()) || Convert.ToInt32(documento.Idempresa) == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idempresa_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            if ( documento.Idpuntoventa.ToString().Length == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idpntvnt_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

             else if (!ValidarNumero(documento.Idpuntoventa.ToString())  || Convert.ToInt32(documento.Idpuntoventa) == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idpntvnt_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Idtipodocumento == null || documento.Idtipodocumento.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idtipodoc_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            if( documento.Idtipodocumento == "07" || documento.Idtipodocumento == "08" ) 
            {
                if(documento.Idtipodocumentonota!=EN_Constante.g_const_1.ToString() && documento.Idtipodocumentonota!=EN_Constante.g_const_3.ToString())
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idtipodocnota_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
            }

            SeteoParam(documento);

             if (documento.Serie == null || documento.Serie.Trim().Length== EN_Constante.g_const_0 || documento.Serie.Trim().Length>EN_Constante.g_const_4 || documento.Serie.Trim().Length<EN_Constante.g_const_4 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_serie_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            if (documento.Serie.Trim().Length== EN_Constante.g_const_4 )
            {
                char pL= char.Parse(documento.Serie.Substring(0,1));
                if(!Char.IsLetter(pL))
                {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_serie_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
                }
            }
            if (documento.Numero == null || documento.Numero.Trim().Length != EN_Constante.g_const_length_numero_cpe_doc )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_numero_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            
            if (documento.Id == null || documento.Id.Trim().Length == EN_Constante.g_const_0 || documento.Id.Trim().Length != EN_Constante.g_const_length_id_cpe_doc)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idcpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Usuario == null || documento.Usuario.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                // gbcon = cad_consentimiento.Obtener_desc_gbcon(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_usuario_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Clave == null || documento.Clave.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_pass_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
           
              else if (documento.TipoOperacion == null || documento.TipoOperacion.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_tipoop_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
              else if (  (documento.Descuento != null && 
              (documento.Descuento.Length > EN_Constante.g_const_0 && documento.Descuento != "0")) 
              &&  (documento.Descuentoafectabase==null || documento.Descuentoafectabase.Length==EN_Constante.g_const_0))
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_descuento_afecta_base;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.Tipodoccliente == null || documento.Tipodoccliente.Trim().Length== EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_tipodoccl_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Tipodoccliente.Trim().Length > EN_Constante.g_const_0 )
            {
                if(!ValidarNumero(documento.Tipodoccliente.ToString()))
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_tipodoccl_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
              
            }
            if (documento.Nrodoccliente ==null || documento.Nrodoccliente.Trim().Length== EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_nrodoccl_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Nrodoccliente.Trim().Length > EN_Constante.g_const_0 )
            {
                if(documento.Idtipodocumento == "01" && documento.Tipodoccliente.ToString()=="6" && documento.Nrodoccliente.Trim().Length != EN_Constante.g_const_11 ) 
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1003);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_nrodoccl_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;

                }
                
                if(documento.Idtipodocumento == "03" &&  documento.Tipodoccliente=="1" && documento.Nrodoccliente.Trim().Length != EN_Constante.g_const_8 ) 
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1001);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_nrodoccl_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;

                }
                 if(documento.Idtipodocumento == "03" &&  documento.Tipodoccliente=="6" && documento.Nrodoccliente.Trim().Length != EN_Constante.g_const_11 ) 
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1003);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_nrodoccl_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;

                }
                
            }

             if (documento.Nombrecliente==null || documento.Nombrecliente.Trim().Length== EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_nombrecl_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Fecha == null || !ValidarFecha(documento.Fecha.Trim()))
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_fecha_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.Hora == null || !ValidarHora(documento.Hora.Trim()))
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_hora_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.Moneda == null || documento.Moneda.Trim().Length== EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_moneda_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
              else if (documento.Igvventa.ToString() == null)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_igvventa_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.Importeventa.ToString() == null)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_importeventa_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Importefinal.ToString() == null)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_importefinal_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Estado==null || documento.Estado.Trim().Length== EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_estado_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.Situacion == null || documento.Situacion.Trim().Length== EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_situacion_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Estransgratuita ==null || documento.Estransgratuita.Trim().Length== EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_estransgratuita_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            
            letraTipoDoc=documento.Serie[0];
            
            if ((letraTipoDoc=='F' && documento.Formapago == null) || (letraTipoDoc=='F' && documento.Formapago.Trim().Length== EN_Constante.g_const_0) )
            {
                
                                            
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_formapago_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }




            foreach(var item in documento.ListaDetalleDocumento)
            {
                 if (item.Iddocumento == null || item.Iddocumento.Trim().Length == EN_Constante.g_const_0 || item.Iddocumento.Trim().Length != EN_Constante.g_const_length_id_cpe_doc)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idddcpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (!ValidarNumero(item.Idempresa.ToString()) || item.Idempresa.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idempresadd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.Idtipodocumento == null || item.Idtipodocumento.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idtipodocdd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                else if (item.Lineid.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_lineiddd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (!ValidarNumero(item.Lineid.ToString()))
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_lineiddd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.Producto == null || item.Producto.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_productodd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.AfectoIgv == null || item.AfectoIgv.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_afectoigvdd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.AfectoIsc == null || item.AfectoIsc.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_afectoiscdd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                else if (item.Afectacionigv == null || item.Afectacionigv.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_afectacionigvdd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.Unidad == null || item.Unidad.Trim().Length== EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_unidaddd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.Cantidad.ToString() == null || item.Cantidad.ToString().Length == EN_Constante.g_const_0 )
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_cantidaddd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                  else if (item.Preciounitario.ToString()==null || item.Preciounitario.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_preciounitariodd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.Precioreferencial.ToString()==null || item.Precioreferencial.Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_preciorefdd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (!ValidaMinMaxDecimales(item.Valorunitario.ToString(),EN_Constante.g_const_2.ToString(), EN_Constante.g_const_10.ToString()))
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = string.Format(EN_Constante.g_const_err_valorunitariodd_cpedoc, EN_Constante.g_const_2.ToString());
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (!ValidaMinMaxDecimales(item.Valorbruto.ToString(),EN_Constante.g_const_2.ToString(), EN_Constante.g_const_10.ToString()))
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = string.Format(EN_Constante.g_const_err_valorbrutodd_cpedoc, EN_Constante.g_const_2.ToString());
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                else if (!ValidaMinMaxDecimales(item.Valorventa.ToString(),EN_Constante.g_const_2.ToString(), EN_Constante.g_const_10.ToString()))
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp =string.Format(EN_Constante.g_const_err_valorventadd_cpedoc, EN_Constante.g_const_2.ToString()) ;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                else if (item.Igv.ToString()==null || item.Igv.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_igvdd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }

                 else if (item.Total.ToString()==null || item.Total.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_totaldd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.factorIgv.ToString()==null || item.factorIgv.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_factorigvdd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                else if (item.Valordscto.ToString()==null || item.Valordscto.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_valordscdd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                else if (item.Valorcargo.ToString()==null || item.Valorcargo.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_valorcargodd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                else if (item.Isc.ToString()==null || item.Isc.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_iscdd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                 else if (item.factorIsc.ToString()==null || item.factorIsc.ToString().Length == EN_Constante.g_const_0)
                {
                    gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_factoriscdd_cpedoc;
                    RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                    RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                    return RespuestaCpeDoc;
                }
                
            }
           
           
                
            gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
            RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = gbcon.conceptodesc;
            RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = true;
            RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
            ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
            RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
            return RespuestaCpeDoc;
        }
        catch(Exception ex)
        {
            throw ex;
        }
        

    }
 public ClassRptaGenerarCpeDoc ValidarCamposUserAuthWA(EN_AuthUserInfo documento)
    {
        //DESCRIPCIÓN: Validar campos generar comprobante electronico documentos.

        ClassRptaGenerarCpeDoc RespuestaCpeDoc = new ClassRptaGenerarCpeDoc();//Respuesta
        // AD_Concepto cad_consentimiento = new AD_Concepto();//Concepto
        EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();//Error
        EN_Concepto gbcon = new EN_Concepto();//entidad concepto
      
        
        try
        {

            if(documento== null)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_error_trama;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;

            }

            else if (documento.usuario == null || documento.usuario.Trim().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_usuario_token;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.password == null || documento.password.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_pass_token;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            
            gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
            RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = gbcon.conceptodesc;
            RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = true;
            RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
            ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
            RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
            return RespuestaCpeDoc;
            
        }
        catch (System.Exception ex)
        {
            
            throw ex;
        }
        
    }

     public ClassRptaGenerarCpeDoc ValidarCamposGenComunicadoBaja(BajaSunat_Trama documento)
    {

         //DESCRIPCIÓN: Validar campos generar comunicado de baja

        ClassRptaGenerarCpeDoc RespuestaCpeDoc = new ClassRptaGenerarCpeDoc();  //Respuesta
        EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //Error
        EN_Concepto gbcon = new EN_Concepto();                                  //entidad concepto

     


        try
        {
           if (documento == null) {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_error_tramaGeneral;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
          
            if (documento.Empresa_id.ToString().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idempresa_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            else if (!ValidarNumero(documento.Empresa_id.ToString()) || Convert.ToInt32(documento.Empresa_id) == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idempresa_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }   
            if ( documento.Idpuntoventa.ToString().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idpntvnt_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            } 

            else if (!ValidarNumero(documento.Idpuntoventa.ToString())  || Convert.ToInt32(documento.Idpuntoventa) == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idpntvnt_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            } 
            else if (documento.Usuario == null || documento.Usuario.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                // gbcon = cad_consentimiento.Obtener_desc_gbcon(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_usuario_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Clave == null || documento.Clave.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_pass_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
       
             else if (documento.FechaDocumento == null || !ValidarFecha(documento.FechaDocumento.Trim()))
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_fechadoc_cb;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.FechaComunicacion == null ||  !ValidarFecha(documento.FechaComunicacion.Trim()))
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_fechacom_cb;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.UsuarioSession == null || documento.UsuarioSession.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_usuariosession_cb;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            
            if(documento.ListaDetalleBaja.Count()==0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_listadetalle_cb;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
               
            }
            else
            {
                 foreach (var itemDD in documento.ListaDetalleBaja)
                {
                    if (itemDD.Tipodocumentoid == null || itemDD.Tipodocumentoid.Trim().Length== EN_Constante.g_const_0)
                    {
                        gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                        RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_tipodocid_db_cb;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                        ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                        RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                        return RespuestaCpeDoc;
                    }
                    else if (itemDD.Serie == null || itemDD.Serie.Trim().Length== EN_Constante.g_const_0)
                    {
                        gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                        RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_serie_db_cb;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                        ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                        RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                        return RespuestaCpeDoc;
                    }
                     else if (itemDD.Numero == null || itemDD.Numero.Trim().Length== EN_Constante.g_const_0)
                    {
                        gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                        RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_numero_db_cb;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                        ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                        RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                        return RespuestaCpeDoc;
                    }
                     else if (itemDD.MotivoBaja == null || itemDD.MotivoBaja.Trim().Length== EN_Constante.g_const_0)
                    {
                        gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                        RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_motivo_db_cb;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                        ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                        RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                        return RespuestaCpeDoc;
                    }
                   
                }

            }


            SeteoParamComBaja(documento);

            string msj="";
            bool success = false;
            
            ComprobarDatosComBaja(documento, ref success, ref msj);
            if(!success)
            {
                 gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = msj;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            //Comprobamos el tiempo de dias permitidos para la fechacomunicacion con fechadocumento
            string msjFechaComDocBaja="";
            bool successFechaComDocBaja = false;
            ComprobarFechaComDocBaja(documento, ref successFechaComDocBaja, ref msjFechaComDocBaja);
            if(!successFechaComDocBaja)
            {
                 gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = msjFechaComDocBaja;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }



            gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
            RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = gbcon.conceptodesc;
            RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = true;
            RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
            ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
            RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
            return RespuestaCpeDoc;




        }
        catch (System.Exception ex)
        {
            
            throw ex;
        }
      
    }

        public void SeteoParamReversion(ReversionSunatTrama oDocumento)
        {
            //DESCRIPCION: SETEO DE PARAMETROS DE ENTRADA
            try
            {
                oDocumento.Tiporesumen = EN_ConfigConstantes.Instance.const_IdRR;
                oDocumento.Estado = EN_Constante.g_const_estado_nuevo;
                oDocumento.Situacion = EN_Constante.g_const_situacion_pendiente;
            }
            catch (System.Exception ex)
            {
                throw ex; 
            }
        }

    public ClassRptaGenerarCpeDoc ValidarCamposGenReversion(ReversionSunatTrama documento)
    {

         //DESCRIPCIÓN: Validar campos generar reversión

        ClassRptaGenerarCpeDoc RespuestaCpeDoc = new ClassRptaGenerarCpeDoc();  //Respuesta
        EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //Error
        EN_Concepto gbcon = new EN_Concepto();                                  //entidad concepto

     


        try
        {
           if (documento == null) {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_error_tramaGeneral;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
          


            if (documento.Empresa_id.ToString().Length == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idempresa_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            } 
            else if (!ValidarNumero(documento.Empresa_id.ToString()) ||  Convert.ToInt32(documento.Empresa_id) == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idempresa_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }   

            if (documento.puntoventa_id.ToString().Length == EN_Constante.g_const_0 )
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idpntvnt_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            } 

            else if (!ValidarNumero(documento.puntoventa_id.ToString()) || documento.puntoventa_id.ToString().Length == EN_Constante.g_const_0 || Convert.ToInt32(documento.puntoventa_id) == EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_idpntvnt_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            } 
            else if (documento.Usuario == null || documento.Usuario.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_usuario_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.Clave == null || documento.Clave.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_pass_cpedoc;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
       
             else if (documento.FechaDocumento == null || !ValidarFecha(documento.FechaDocumento.Trim()))
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_fechadoc_cb;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
             else if (documento.FechaComunicacion == null ||  !ValidarFecha(documento.FechaComunicacion.Trim()))
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_fechacom_cb;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            else if (documento.UsuarioSession == null || documento.UsuarioSession.Trim().Length== EN_Constante.g_const_0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_usuariosession_cb;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }
            
            if(documento.ListaDetalleReversion.Count()==0)
            {
                gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_listadetalle_cb;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
               
            }
            else
            {
                 foreach (var itemDD in documento.ListaDetalleReversion)
                {
                    if (itemDD.Tipodocumentoid == null || itemDD.Tipodocumentoid.Trim().Length== EN_Constante.g_const_0)
                    {
                        gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                        RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_tipodocid_db_cb;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                        ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                        RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                        return RespuestaCpeDoc;
                    }
                    else if (itemDD.Serie == null || itemDD.Serie.Trim().Length== EN_Constante.g_const_0)
                    {
                        gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                        RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_serie_db_cb;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                        ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                        RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                        return RespuestaCpeDoc;
                    }
                     else if (itemDD.Numero == null || itemDD.Numero.Trim().Length== EN_Constante.g_const_0)
                    {
                        gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                        RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_numero_db_cb;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                        ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                        RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                        return RespuestaCpeDoc;
                    }
                     else if (itemDD.MotivoReversion == null || itemDD.MotivoReversion.Trim().Length== EN_Constante.g_const_0)
                    {
                        gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                        RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = EN_Constante.g_const_err_motivo_db_cb;
                        RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                        ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                        RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                        return RespuestaCpeDoc;
                    }
                   
                }

            }


            SeteoParamReversion(documento);

               string msj="";
            bool success = false;
            
            ComprobarDatosComReversion(documento, ref success, ref msj);
            if(!success)
            {
                 gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = msj;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            //Comprobamos el tiempo de dias permitidos para la fechacomunicacion con fechadocumento
            string msjFechaComDocBaja="";
            bool successFechaComDocBaja = false;
            ComprobarFechaComDocReversion(documento, ref successFechaComDocBaja, ref msjFechaComDocBaja);
            if(!successFechaComDocBaja)
            {
                 gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = false;
                RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = msjFechaComDocBaja;
                RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
                RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
                return RespuestaCpeDoc;
            }

            gbcon= ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
            RespuestaCpeDoc.RptaRegistroCpeDoc.DescripcionResp = gbcon.conceptodesc;
            RespuestaCpeDoc.RptaRegistroCpeDoc.FlagVerificacion = true;
            RespuestaCpeDoc.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
            ErrorWebSer = llenarErrorWebService(EN_Constante.g_const_0, gbcon.conceptocorr, gbcon.conceptodesc);
            RespuestaCpeDoc.ErrorWebService = ErrorWebSer;
            return RespuestaCpeDoc;




        }
        catch (System.Exception ex)
        {
            
            throw ex;
        }
      
    }

       public EN_ResponseGuia validarCamposGuiaElectronica (GuiaSunatTrama request) 
        {
            //DESCRIPCION: Validar trama de entrada de guia
            EN_ResponseGuia respuestaGuia = new EN_ResponseGuia();
            EN_Concepto concepto = new EN_Concepto();
            NE_Guia guia = new NE_Guia();
            EN_ErrorWebService errWebService = new EN_ErrorWebService();
            try
            {
                if (!(request == null)) 
                {
                 respuestaGuia =   seteoParametrosGuia(request);
                 if (respuestaGuia.RespGuiaElectronica.FlagVerificacion) 
                 {
                    if(request.id == null || request.id.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_id;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.empresa_id == null || request.empresa_id.Trim().Length == EN_Constante.g_const_0)
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_idEmpresa;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (!ValidarNumero(request.empresa_id)) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_idEmpresa;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if(request.serie == null || request.serie.Trim() == "") 
                     {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_serie;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;
                     } else if (request.serie.Trim().Length != EN_Constante.g_const_4) 
                     {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_err_serie;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                     } else if (request.serie.Trim().Length == EN_Constante.g_const_4) 
                     {
                         char pL= char.Parse(request.serie.Substring(0,1));
                         if(!Char.IsLetter(pL)) 
                         {
                            respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                            respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_err_serie;
                            concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                            errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                            respuestaGuia.ErrorWebServ = errWebService;
                            return respuestaGuia;
                         }
                        string letraTipoDoc=request.serie[0].ToString().ToUpper();
                        if (letraTipoDoc != "T") 
                        {
                            respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                            respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_err_serie;
                            concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                            errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                            respuestaGuia.ErrorWebServ = errWebService;
                            return respuestaGuia;

                        }
                     } 
                     if(request.numero == null || request.numero.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_numero;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                     } else if (request.numero.Trim().Length != EN_Constante.g_const_8) 
                     {
                         respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_err_numero;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;
                     }
                     else if (request.fechaemision == null || request.fechaemision.Trim() == "") 
                     {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_fechaemision;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                     }
                    else if (!ValidarFecha(request.fechaemision.Trim()))
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_fechaemision;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;
                    } 
                    else if(request.horaemision.Trim() == "")
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_horaemision;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } 
                    else if (!ValidarHora(request.horaemision)) {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_horaemision;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;
                        
                    }
                    else if (request.puntoemision == null || request.puntoemision.Trim().Length == EN_Constante.g_const_0) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_punto_emision;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    }
                    else if (!ValidarNumero(request.puntoemision)) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_punto_emision;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    }
                    else if (request.tipodocumento_id == null || request.tipodocumento_id.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocumento;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if(request.tipodocumento_id.Trim().Length != EN_Constante.g_const_2) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocumento;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (!ValidarNumero(request.tipodocumento_id)){
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocumento;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if(request.tipodocumento_id != "09") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocumento;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.id.Trim().Length > EN_Constante.g_const_15) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_IdGuia;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.codmotivo == null || request.codmotivo.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_codMorivo;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } 
                    
                    
                    else if (request.destinatario_tipodoc == null || request.destinatario_tipodoc.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocdestinatario;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if(request.destinatario_tipodoc.Trim().Length != EN_Constante.g_const_1) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocdestinatario;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if(request.destinatario_nrodoc == null || request.destinatario_nrodoc.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_nrodocdestinatario;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.destinatario_nrodoc.Trim() == "")
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_nrodocdestinatario;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.destinatario_nrodoc.Trim().Length > EN_Constante.g_const_15) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_nrodocdestinatario;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.destinatario_nombre == null || request.destinatario_nombre.Trim() == "" ){
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_nombredestinatario;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;
                    }

                    else if (request.pesobrutototal == null || request.pesobrutototal.Trim() == "") {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_pesobrutototal;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;
                    } else if (!ValidaMinMaxDecimales(request.pesobrutototal.ToString(),EN_Constante.g_const_0.ToString(), EN_Constante.g_const_3.ToString())) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_numeDecimalesPesoBruTotal;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.unidadpeso == null || request.unidadpeso.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_unidadPeso;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.modalidadtraslado == null || request.modalidadtraslado.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_modalidadTraslado;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.modalidadtraslado.Trim().Length != 2) {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_errmodalidadTraslado;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    }
                    
                    else if (request.fechatraslado == null || request.fechatraslado.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_fechaTraslado;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (!ValidarFecha(request.fechatraslado.Trim()))
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_errFechaTraslado;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;
                    } else if(request.ubigeoptollegada == null || request.ubigeoptollegada.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_ubigeollegada;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;
                    } else if (!ValidarNumero(request.ubigeoptollegada)) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_errubigeollegada;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if(request.direccionptollegada == null || request.direccionptollegada.Trim() == "")
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_direccionllegada;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.ubigeoptopartida == null || request.ubigeoptopartida.Trim() == "")
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_ubigeopartida;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    }
                    else if (request.direccionptopartida == null || request.direccionptopartida.Trim() == "")
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_direccionpartida;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    }
                    
                    
                    
                    
                    
                    else if(request.desctipodocumento == null || request.desctipodocumento.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocremitente;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;
                        
                    }else if(!ValidarNumero(request.desctipodocumento.Trim()) || (request.desctipodocumento.Trim().Length != EN_Constante.g_const_1)) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocremitente;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } 
                    
                    
                    
                    
                    
                    else if (request.numerodocempresa == null || request.numerodocempresa.Trim() == "") 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_nrodocremitente;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (!ValidarNumero(request.numerodocempresa.Trim())) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_nrodocremitente;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if (request.situacion == null || request.situacion.Trim() == "" ) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_situacion;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if(request.estado == null || request.estado.Trim() == "") {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_estado;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    }
                    
                    else if(request.Usuario == null || request.Usuario.Trim().Length == EN_Constante.g_const_0) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_usuario;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if(request.clave == null || request.clave.Trim().Length == EN_Constante.g_const_0) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_clave;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;

                    } else if(request.destinatario_email != null && request.destinatario_email.Trim() != "")
                     {
                         if (!validarEmail(request.destinatario_email)) 
                         {
                            respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                            respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_correo_destinatario;
                            concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                            errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                            respuestaGuia.ErrorWebServ = errWebService;
                            return respuestaGuia;

                         }

                    }
                    
                     if (request.ListaDetalleGuia.Count == 0) 
                    {
                        respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                        respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_listavacia;
                        concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                        errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                        respuestaGuia.ErrorWebServ = errWebService;
                        return respuestaGuia;
                    }
                    else {
                         foreach (var itemDD in request.ListaDetalleGuia)
                            {
                                if (itemDD.guia_id == null || itemDD.guia_id.Trim().Length== EN_Constante.g_const_0)
                                {
                                    respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                                    respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_id;
                                    concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                                    errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                                    respuestaGuia.ErrorWebServ = errWebService;
                                    return respuestaGuia;
                                }
                                else if (itemDD.empresa_id == null || itemDD.empresa_id.Trim().Length == EN_Constante.g_const_0 )
                                {
                                    respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                                    respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_idEmpresa;
                                    concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                                    errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                                    respuestaGuia.ErrorWebServ = errWebService;
                                    return respuestaGuia;
                                }
                                else if (!ValidarNumero(itemDD.empresa_id.ToString())) 
                                {
                                    respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                                    respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_idEmpresa;
                                    concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                                    errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                                    respuestaGuia.ErrorWebServ = errWebService;
                                    return respuestaGuia;

                                }
                                else if (itemDD.tipodocumento_id == null || itemDD.tipodocumento_id.Trim().Length== EN_Constante.g_const_0)
                                {
                                    respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                                    respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_tipodocumento;
                                    concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                                    errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                                    respuestaGuia.ErrorWebServ = errWebService;
                                    return respuestaGuia;
                                }
                                else if (itemDD.line_id == null || itemDD.line_id.Trim().Length== EN_Constante.g_const_0 || (!ValidarNumero(itemDD.line_id)))
                                {
                                    respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                                    respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_numero_linea_detalle;
                                    concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                                    errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                                    respuestaGuia.ErrorWebServ = errWebService;
                                    return respuestaGuia;
                                }
                                 else  if (itemDD.descripcion == null || itemDD.descripcion.Trim().Length == EN_Constante.g_const_0) 
                                {
                                    respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                                    respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_descripcion_producto;
                                    concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                                    errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                                    respuestaGuia.ErrorWebServ = errWebService;
                                    return respuestaGuia; 
                                } else if (itemDD.unidad == null || itemDD.unidad.Trim().Length == EN_Constante.g_const_0)
                                {
                                    respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                                    respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_unidad_bien;
                                    concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                                    errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                                    respuestaGuia.ErrorWebServ = errWebService;
                                    return respuestaGuia; 

                                

                                } else if(itemDD.cantidad == null || itemDD.cantidad.Trim().Length == EN_Constante.g_const_0) 
                                {
                                     respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                                    respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_cantidad_bien;
                                    concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1005);
                                    errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                                    respuestaGuia.ErrorWebServ = errWebService;
                                    return respuestaGuia; 

                                }
                                
                               
                            
                            }
                    }

                    concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_2000);
                    respuestaGuia.RespGuiaElectronica.FlagVerificacion = true;
                    respuestaGuia.RespGuiaElectronica.DescRespuesta = concepto.conceptodesc;
                        
                    errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                    respuestaGuia.ErrorWebServ = errWebService;
                    return respuestaGuia;
                 }
                }
                else {
                    respuestaGuia.RespGuiaElectronica.FlagVerificacion = false;
                    respuestaGuia.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_error_interno;
                    errWebService = llenarErrorWebService(EN_Constante.g_const_0,concepto.conceptocorr,concepto.conceptodesc);
                    respuestaGuia.ErrorWebServ = errWebService;
                    return respuestaGuia;
                }

                return respuestaGuia;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
    


    }
}