﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Resumen.cs
 VERSION : 1.0
 OBJETIVO: Clase de capa de negocio resumen
 FECHA   : 12/10/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using CEN;
using CAD;

namespace CLN
{


    public class NE_Resumen
    {
    
       
      
  
      
        public object listarDocumentosBajaResumen(string idDoc, int idEmpresa, int idPuntoVenta)
        {
             // DESCRIPCION: LISTAR DOCREFEGUIA
            
            AD_Resumen obj = new AD_Resumen(); // CLASE DE CONEXION DOCUMENTO
            try
            {
                return obj.listarDocumentosBajaResumen( idDoc,  idEmpresa,  idPuntoVenta);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        
        public int GuardarResumenSunat(ResumenSunat ResumenoSunat)
        {
            AD_Resumen obj = new AD_Resumen();
            try
            {
                return obj.GuardarResumenSunat(ResumenoSunat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object listarResumen(EN_Resumen pResumen)
        {
            try
            {
                AD_Resumen obj = new AD_Resumen();
                return obj.listarResumen(pResumen);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public object listarDetalleResumen(EN_Resumen pResumen)
        {
            try
            {
                AD_Resumen obj = new AD_Resumen();
                return obj.listarDetalleResumen(pResumen);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         public bool GuardarResumenNombreValorResumenFirma(EN_Resumen oResumen, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            try
            {
                AD_Resumen obj = new AD_Resumen();
                return obj.GuardarResumenNombreValorResumenFirma(oResumen, nombreXML, valorResumen, valorFirma, xmlgenerado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    
   



  
       
     
    }

}
