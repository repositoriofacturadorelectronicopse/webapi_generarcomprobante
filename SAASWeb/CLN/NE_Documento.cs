﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Documento.cs
 VERSION : 1.0
 OBJETIVO: Clase de capa de negocio documento
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using CEN;
using CAD;

namespace CLN
{


    public class NE_Documento
    {
    
       
        public object listarDocumento(EN_Documento pDocumento)
        {
             // DESCRIPCION: LISTAR DOCUMENTO

             AD_Documento obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO

            try
            {
                return obj.listarDocumento(pDocumento);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public object listarDetalleDocumento(EN_Documento pDocumento )
        {
             // DESCRIPCION: LISTAR DETALLE DOCUMENTO

             AD_Documento obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO
            try
            {
                return obj.listarDetalleDocumento(pDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            
            }
        }
        public object listarDocumentoReferencia(EN_DocReferencia pDocumento , string documentoId, int Idempresa)
        {
             // DESCRIPCION: LISTAR DOCUMENTO DE REFERENCIA

            AD_Documento obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO
            try
            {
                return obj.listarDocumentoReferencia(pDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public object listarDocumentoAnticipo(EN_DocAnticipo pDocumento, string documentoId, int Idempresa)
        {
             // DESCRIPCION: LISTAR DOCUMENTO ANTICIPO

            AD_Documento obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO
            try
            {
                return obj.listarDocumentoAnticipo(pDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public object listarDocumentoGuia(EN_FacturaGuia pDocGuia, string documentoId, int Idempresa)
        {
             // DESCRIPCION: LISTAR DOCUMENTO GUIA
             AD_Documento obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO

            try
            {
               
                return obj.listarDocumentoGuia(pDocGuia);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
      
        public bool eliminarDocumento(EN_Documento pDocumento)
        {
             // DESCRIPCION: ELIMINAR DOCUMENTO
            
            AD_Documento obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO
            try
            {
                return obj.eliminarDocumento(pDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
  
      
        public object listarDocRefeGuia(EN_DocRefeGuia pDocumento, string documentoId, int Idempresa)
        {
             // DESCRIPCION: LISTAR DOCREFEGUIA
            
            AD_Documento obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO
            try
            {
                return obj.listarDocRefeGuia(pDocumento);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    

        public object ConsultarSituacionesCpes(int empresa_id, int puntoventa_id, string id_cpes)
        {
             // DESCRIPCION: LISTAR DOCREFEGUIA
            
            AD_Documento obj = new AD_Documento(); // CLASE DE CONEXION DOCUMENTO
            try
            {
                return obj.ConsultarSituacionesCpes(empresa_id, puntoventa_id, id_cpes);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
   



  
       
     
    }

}
