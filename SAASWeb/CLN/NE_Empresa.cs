﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Empresa.cs
 VERSION : 1.0
 OBJETIVO: Clase de capa de negocio empresa
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using CAD;
using CEN;


namespace CLN
{
    public class NE_Empresa
    {


        public object listaEmpresa(EN_Empresa pEmpresa, int Idpuntoventa, string documentoId)
        {
            //DESCRIPCION: LISTA EMPRESA

             AD_Empresa obj = new AD_Empresa(); // CLASE DE CONEXION EMPRESA
            try
            {
               
                return obj.listarEmpresa(pEmpresa, Idpuntoventa);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

           public EN_Parametro buscar_tablaParametro(EN_Parametro par_busqueda)
        {
            try
            {
                AD_Empresa obj = new AD_Empresa();
                return obj.buscar_tablaParametro(par_busqueda);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
     
   

    }
}
