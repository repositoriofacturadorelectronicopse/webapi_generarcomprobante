﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_GeneracionService.cs
 VERSION : 1.0
 OBJETIVO: Clase de negocio generación de servicio
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using CAD;
using CEN;

namespace CLN
{


    public class NE_GeneracionService
    {
        public bool GuardarDocumentoSunat(DocumentoSunat documentoSunat)
        {
            // DESCRIPCION: GUARDAR DOCUMENTO SUNAT

            AD_GeneracionService dataHelper = new AD_GeneracionService();   // CLASE DE CONEXION GENERACION DE SERVICIO
            try
            {
                return dataHelper.GuardarDocumentoSunat(documentoSunat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GuardarBajaSunat(BajaSunat BajaSunat)
        {
             // DESCRIPCION: GUARDAR BAJA SUNAT

            var dataHelper = new AD_GeneracionService();        //// CLASE DE CONEXION GENERACION DE SERVICIO
            try
            {
                return dataHelper.GuardarBajaSunat(BajaSunat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GuardarResumenBajaPendiente(BajaSunat BajaSunat)
        {
             // DESCRIPCION: GUARDAR BAJA SUNAT

            var dataHelper = new AD_GeneracionService();        //// CLASE DE CONEXION GENERACION DE SERVICIO
            try
            {
                return dataHelper.GuardarResumenBajaPendiente(BajaSunat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         public bool GuardarCpeSunat(OtroscpeSunat CpeSunat)
        {
            AD_GeneracionService dataHelper = new AD_GeneracionService();
            try
            {
                return dataHelper.GuardarCpeSunat(CpeSunat);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int GuardarReversionSunat(ReversionSunat reversion)
        {
            AD_GeneracionService dataHelper = new AD_GeneracionService();
            try
            {
                return dataHelper.GuardarReversionSunat(reversion);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public EN_RegistrarGuiaElectronica GuardarGuiaSunat(GuiaSunat GuiaSunat)
        {
            AD_GeneracionService dataHelper = new AD_GeneracionService();
            AD_Guia guia= new AD_Guia();
            EN_RegistrarGuiaElectronica RsptaRegistrarGuia = new EN_RegistrarGuiaElectronica();
            try
            {
                
            RsptaRegistrarGuia = guia.RegistrarGuiaRemision(GuiaSunat);
                return RsptaRegistrarGuia;
                }
            catch (Exception ex)
            {
                throw ex;
            }
    }


     
    

    }

}
