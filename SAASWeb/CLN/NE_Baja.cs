/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Baja.cs
 VERSION : 1.0
 OBJETIVO: Clase de capa de negocio baja
 FECHA   : 20/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using CAD;
using CEN;
namespace CLN
{
    public class NE_Baja
    {
        public object listarBajas(EN_Baja pBaja)
        {
            try
            {
                var obj = new AD_Baja();
                return obj.listarBajas(pBaja);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object listarDetalleBaja(EN_Baja pBaja)
        {
            try
            {
                var obj = new AD_Baja();
                return obj.listarDetalleBaja(pBaja);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool GuardarBajaNombreValorResumenFirma(EN_Baja oBaja, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            try
            {
                var obj = new AD_Baja();
                return obj.GuardarBajaNombreValorResumenFirma(oBaja, nombreXML, valorResumen, valorFirma, xmlgenerado);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // public void GuardarBajaAceptada(EN_Baja oBaja, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
        // {
        //     try
        //     {
        //         var obj = new AD_Baja();
        //         obj.GuardarBajaAceptada(oBaja, nombrexml, xmlbase64, cdrxml, valorResumen, firma, mensaje, ticket);
        //     }
        //     catch (Exception ex)
        //     {
        //         Logs.SaveErrorLog<NE_Baja>(ex, "SAASNube - {0} - {1}", pClase, "GuardarBajaAceptada");
        //         throw ex;
        //     }
        // }

        // public void GuardarBajaRechazadaError(EN_Baja oBaja, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
        // {
        //     try
        //     {
        //         var obj = new AD_Baja();
        //         obj.GuardarBajaRechazadaError(oBaja, nombrexml, xmlbase64, cdrxml, valorResumen, firma, errortext, estado, ticket);
        //     }
        //     catch (Exception ex)
        //     {
        //         Logs.SaveErrorLog<NE_Baja>(ex, "SAASNube - {0} - {1}", pClase, "GuardarBajaRechazadaError");
        //         throw ex;
        //     }
        // }

        // public void ActualizarEnvioMail(EN_Baja oBaja, string estado)
        // {
        //     try
        //     {
        //         var obj = new AD_Baja();
        //         obj.ActualizarEnvioMail(oBaja, estado);
        //     }
        //     catch (Exception ex)
        //     {
        //         Logs.SaveErrorLog<NE_Baja>(ex, "SAASNube - {0} - {1}", pClase, "ActualizarEnvioMail");
        //         throw ex;
        //     }
        // }

        // public object consultarDocumentoElectronico(EN_DocElectronico pDocumento)
        // {
        //     try
        //     {
        //         var obj = new AD_Baja();
        //         return obj.consultarDocumentoElectronico(pDocumento);
        //     }
        //     catch (Exception ex)
        //     {
        //         Logs.SaveErrorLog<NE_Baja>(ex, "SAASNube - {0} - {1}", pClase, "consultarDocumentoElectronico");
        //         throw ex;
        //     }
        // }

        // public List<EN_Baja> listarAceptadoBaja(EN_Baja pDocumento)
        // {
        //     try
        //     {
        //         var obj = new AD_Baja();
        //         return obj.listarAceptadoBaja(pDocumento);
        //     }
        //     catch (Exception ex)
        //     {
        //         Logs.SaveErrorLog<NE_Baja>(ex, "SAASNube - {0} - {1}", pClase, "listarAceptadoBaja");
        //         throw ex;
        //     }
        // }

        // public List<EN_Baja> listarErrorRechazoBaja(EN_Baja pDocumento)
        // {
        //     try
        //     {
        //         var obj = new AD_Baja();
        //         return obj.listarErrorRechazoBaja(pDocumento);
        //     }
        //     catch (Exception ex)
        //     {
        //         Logs.SaveErrorLog<NE_Baja>(ex, "SAASNube - {0} - {1}", pClase, "listarErrorRechazoBaja");
        //         throw ex;
        //     }
        // }

        // public List<EN_Baja> listarErrorRechazoBajaConTicket(EN_Baja pDocumento)
        // {
        //     try
        //     {
        //         var obj = new AD_Baja();
        //         return obj.listarErrorRechazoBajaConTicket(pDocumento);
        //     }
        //     catch (Exception ex)
        //     {
        //         Logs.SaveErrorLog<NE_Baja>(ex, "SAASNube - {0} - {1}", pClase, "listarErrorRechazoBajaConTicket");
        //         throw ex;
        //     }
        // }

        // public object reporteConsolidado(EN_Baja pDocumento)
        // {
        //     try
        //     {
        //         var obj = new AD_Baja();
        //         return obj.reporteConsolidado(pDocumento);
        //     }
        //     catch (Exception ex)
        //     {
        //         Logs.SaveErrorLog<NE_Baja>(ex, "SAASNube - {0} - {1}", pClase, "reporteConsolidado");
        //         throw ex;
        //     }
        // }

        // public object listarBajaAsociada(EN_Documento pDocumento)
        // {
        //     try
        //     {
        //         var obj = new AD_Baja();
        //         return obj.listarBajaAsociada(pDocumento);
        //     }
        //     catch (Exception ex)
        //     {
        //         Logs.SaveErrorLog<NE_Baja>(ex, "SAASNube - {0} - {1}", pClase, "listarBajaAsociada");
        //         throw ex;
        //     }
        // }

        // public bool eliminarBaja(EN_Baja pBaja)
        // {
        //     try
        //     {
        //         var obj = new AD_Baja();
        //         return obj.eliminarBaja(pBaja);
        //     }
        //     catch (Exception ex)
        //     {
        //         Logs.SaveErrorLog<NE_Baja>(ex, "SAASNube - {0} - {1}", pClase, "eliminarBaja");
        //         throw ex;
        //     }
        // }
    }
}