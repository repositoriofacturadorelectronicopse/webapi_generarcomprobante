﻿using System;
using System.Collections.Generic;

using CEN;
using CAD;

public class NE_OtrosCpe
{
    private string pClase = "NE_OtrosCpe";
    public object listarOtrosCpe(EN_OtrosCpe pOtrosCpe)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.listarOtrosCpe(pOtrosCpe);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object listaDetalleOtrosCpe(EN_OtrosCpe pOtrosCpe)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.listarDetalleOtrosCpe(pOtrosCpe);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public bool GuardarOtrosCpeNombreValorResumenFirma(EN_OtrosCpe oOtrosCpe, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.GuardarOtrosCpeNombreValorResumenFirma(oOtrosCpe, nombreXML, valorResumen, valorFirma, xmlgenerado);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void GuardarOtrosCpeAceptada(EN_OtrosCpe oOtrosCpe, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            obj.GuardarOtrosCpeAceptada(oOtrosCpe, nombrexml, xmlbase64, cdrxml, valorResumen, firma, mensaje, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void GuardarOtrosCpeRechazadaError(EN_OtrosCpe oOtrosCpe, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            obj.GuardarOtrosCpeRechazadaError(oOtrosCpe, nombrexml, xmlbase64, cdrxml, valorResumen, firma, errortext, estado, ticket);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object consultarDocumentoElectronico(EN_DocElectronico pDocumento)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.consultarDocumentoElectronico(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void ActualizarEnvioMail(EN_OtrosCpe oOtrosCpe, string estado)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            obj.ActualizarEnvioMail(oOtrosCpe, estado);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public List<EN_OtrosCpe> listarAceptadoOtrosCpe(EN_OtrosCpe pDocumento)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.listarAceptadoOtrosCpe(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public List<EN_OtrosCpe> listarErrorRechazoOtrosCpe(EN_OtrosCpe pDocumento)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.listarErrorRechazoOtrosCpe(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object reporteConsolidado(EN_OtrosCpe pDocumento)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.reporteConsolidado(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public bool ActualizarSituacion(EN_OtrosCpe oDocumento, string situacion)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.ActualizarSituacion(oDocumento, situacion);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public bool eliminarOtrosCpe(EN_OtrosCpe pDocumento)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.eliminarOtrosCpe(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public string consultarSituacion(EN_OtrosCpe pDocumento)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.consultarSituacion(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object listarDocumentoReenviarMail(EN_OtrosCpe pDocumento)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.listarDocumentoReenviarMail(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public object consultaLibreDocumentoElectronico(EN_DocElectronico pDocumento)
    {
        try
        {
            AD_OtrosCpe obj = new AD_OtrosCpe();
            return obj.consultaLibreDocumentoElectronico(pDocumento);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
