/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Guia.cs
 VERSION : 1.0
 OBJETIVO: Clase de logica de Guia
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/
using System;
using CEN;
using CAD;
using System.Collections.Generic;
namespace CLN
{
    public class NE_Guia
    {
        public  EN_RespuestaListaGuia listarGuia(EN_Guia pGuia)
        {
            //DESCRIPCION: Listar guia
            AD_Guia cadGuia = new AD_Guia(); 
            EN_RespuestaListaGuia RespListarGuia = new EN_RespuestaListaGuia();          
            try
            {
                RespListarGuia = cadGuia.listarGuia(pGuia);
                return RespListarGuia;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
         public EN_RespuestaListaDetalleGuia listarDetalleGuia(EN_Guia pGuia)
        {
            //DESCRIPCION: Listar guia detalle
            AD_Guia cadGuia = new AD_Guia();   
            EN_RespuestaListaDetalleGuia ResptaListaDetalleGuia = new EN_RespuestaListaDetalleGuia();
            List<EN_DetalleGuia> listDetalleGuia = new List<EN_DetalleGuia>();        
            try
            {
                ResptaListaDetalleGuia = cadGuia.listarDetalleGuia(pGuia);
                return ResptaListaDetalleGuia;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
        public EN_ActualizarGuia GuardarGuiaNombreValorResumenFirma(EN_Guia pGuia, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            //DESCRIPCION: Guardar Guia Valor Resumen Firma
            AD_Guia cadGuia = new AD_Guia(); 
            EN_ActualizarGuia ResActualizarGuia = new EN_ActualizarGuia();
            try
            {
                ResActualizarGuia = cadGuia.GuardarGuiaNombreValorResumenFirma(pGuia,nombreXML,valorResumen,valorFirma,xmlgenerado, EN_Constante.g_const_2);
                return ResActualizarGuia;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
        public EN_RegistrarGuiaElectronica GuardarGuiaAceptada(EN_Guia pGuia,string nombreXML, string xmlenviado, string cdrxml, string valorresumen, string firma, string mensaje, string ticket)
        {
            //DESCRIPCION:Guardar guia aceptada
            AD_Guia cadGuia = new AD_Guia(); 
            EN_RegistrarGuiaElectronica RespGuiaAceptada = new EN_RegistrarGuiaElectronica();
            try
            {
               RespGuiaAceptada = cadGuia.GuardarGuiaAceptada(pGuia,nombreXML,xmlenviado,cdrxml,valorresumen,firma,mensaje,ticket, EN_Constante.g_const_2);
               return RespGuiaAceptada;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
        public void GuardarGuiaRechazadaError(EN_Guia pGuia, string nombreXML, string xmlenviado, string cdrxml, string valorresumen, string firma, string errortext, string estado, string ticket) 
        {
            //DESCRIPCION: Guardar guia Rechazada
            AD_Guia cadGuia = new AD_Guia(); 
            try
            {
                cadGuia.GuardarGuiaRechazadaError(pGuia,nombreXML,xmlenviado,cdrxml,valorresumen,firma,errortext,estado,ticket, EN_Constante.g_const_2);
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
        public object consultarDocumentoElectronico(EN_DocElectronico pDocumento)
        {
            //DESCRIPCION: Consultar documento electronico
            AD_Guia cadGuia = new AD_Guia(); 
            try
            {
                return cadGuia.consultarDocumentoElectronico(pDocumento);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public EN_EliminarGuia eliminarGuia(EN_Guia pDocumento)
        {
            //DESCRIPCION: Eliminar Guia
            EN_EliminarGuia RespEliminarGuia = new EN_EliminarGuia(); 
            AD_Guia obj = new AD_Guia();
            try
            {
                
                RespEliminarGuia = obj.eliminarGuia(pDocumento,EN_Constante.g_const_2);
                return RespEliminarGuia;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public EN_Concepto listarConcepto (int prefijo, short correlativo) 
        {
            //DESCRIPCION: Listar conceptos
            EN_Concepto concepto = new EN_Concepto();
            AD_Guia obj = new AD_Guia();
            try
            {
                concepto = obj.listarConcepto(prefijo, correlativo);
                return concepto;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public EN_RespuestaRegistro buscarParametroAppSettings(int codconcepto, int codcorrelativo) 
        {
            EN_RespuestaRegistro respuesta = new EN_RespuestaRegistro();
            AD_Guia guia = new AD_Guia();
            try
            {
                respuesta = guia.buscarParametroAppSettings(codconcepto,codcorrelativo);
                return respuesta;
                 
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

        }
        public EN_RespuestaData buscarCorrelativo(int empresa_id,int puntoemision_id , string tipodocumento_id,string serie)
        {
            EN_RespuestaData data = new EN_RespuestaData();
            AD_Guia guia = new AD_Guia();
            try
            {

                 data = guia.buscarCorrelativo(empresa_id,puntoemision_id,tipodocumento_id,serie);
                 return data;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }


        }
        
    }
}