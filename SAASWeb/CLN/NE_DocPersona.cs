﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_DocPersona.cs
 VERSION : 1.0
 OBJETIVO: Clase de capa de negocio docpersona
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using CAD;
using CEN;

namespace CLN
{
   

    public class NE_DocPersona
    {
        public object listarDocPersona(EN_DocPersona pDocPersona , string documentoId, int empresaId )
        {
            // DESCRIPCION: LISTAR DOCPERSONA
             AD_DocPersona obj = new AD_DocPersona();   // CLASE DE CONEXION DOCPERSONA
            try
            {
                return obj.listarDocPersona(pDocPersona);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
