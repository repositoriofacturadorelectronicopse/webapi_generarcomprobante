﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: NE_Certificado.cs
 VERSION : 1.0
 OBJETIVO: Clase de capa de negocio certificado
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using CAD;
using CEN;

namespace CLN
{

    public partial class NE_Certificado
    {

        public object listarCertificado(EN_Certificado pCertif, string documentoId, int IdPuntoVenta)
        {
            // DESCRIPCION: LISTAR CERTIFICADO
            var obj = new AD_Certificado(); // CLASE DE CONEXION CERTIFICADO
            try
            {
                return obj.listarCertificado(pCertif);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

            
        public bool validarEmpresaCertificado(int p_idempresa, string p_usuario, string p_clave, string documentoId, int Idempresa)
        {
            // DESCRIPCION: LISTAR CERTIFICADO
            var obj = new AD_Certificado(); // CLASE DE CONEXION CERTIFICADO

            try
            {
                return obj.validarEmpresaCertificado(p_idempresa, p_usuario, p_clave);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }
}
