/****************************************************************************************************************************************************************************************
 PROGRAMA: UserService.cs
 VERSION : 1.0
 OBJETIVO: Clase de servicio de usuario para autenticación
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using CEN;
using System;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using CLN;
using AppServicioDoc;

namespace SAASWeb.Services
{
    public interface IUserService
    {
        //DESCRIPCION: FUNCION DE INTERFAZ DE IUSERSERVICE
        ClassRptaGenerarCpeDoc Authenticate (EN_AuthUserInfo model);    //función de autenticación
        ClassRptaGenerarCpeDoc BuildToken(EN_AuthUserInfo userInfo,  int empresa_id, int puntoventa_id);    // función constructor de token
    }

     public class UserService : IUserService
    {

        private readonly IConfiguration _configuration;     // configuración

    
        public UserService(IConfiguration configuration)
        {
            //DESCRIPCION: CONSTRUCTOR DE USERSERVICE
           
            _configuration = configuration;
        }

        public ClassRptaGenerarCpeDoc Authenticate(EN_AuthUserInfo model)
        {
            //DESCRIPCION: FUNCION PARA VERIFICAR AUTENTICACIÓN

            EN_Concepto concepto;//entidad concepto
            NE_Consulta consulta = new NE_Consulta(); // consulta
            NE_Facturacion fact = new NE_Facturacion(); //clase de negocio facturación
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();  //error
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ; // respuesta Generar comprobante documento
            var user = new EN_Usuario();   //clase entidad usuario
            bool verified=false; // verifidor

            int empresa_id=EN_Constante.g_const_0;      // id de empresa 
            int puntoventa_id=EN_Constante.g_const_0;   //id de punto de venta

            try
            {
            
                verified= fact.ValidarSession(EN_ConfigConstantes.Instance.const_frase_cont,model.usuario.ToString().Trim(),model.password.ToString().Trim(),ref empresa_id, ref puntoventa_id);

                if(!verified)
                {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_1005);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= "usuario o password inválido";
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_1005;
                    ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_1, concepto.conceptocorr, concepto.conceptodesc);
                    respuesta.ErrorWebService = ErrorWebSer;

                    return respuesta;
                }else{
                   
                     return BuildToken(model , empresa_id, puntoventa_id);
                }

                
            }
            catch(Exception ex)
            {
                concepto = new EN_Concepto();
                concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                respuesta.RptaRegistroCpeDoc.MensajeResp= ex.Message.ToString();
                respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_2, concepto.conceptocorr, concepto.conceptodesc);
                respuesta.ErrorWebService = ErrorWebSer;
                return respuesta;
            }
            

        }

      
         public ClassRptaGenerarCpeDoc BuildToken(EN_AuthUserInfo userInfo, int empresa_id, int puntoventa_id)
        {
            //DESCRIPCION: FUNCION PARA CONSTRUIR TOKEN
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ; // respuesta Generar comprobante documento
            NE_Consulta consulta = new NE_Consulta(); // consulta
            EN_Concepto concepto;//entidad concepto
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();  //error
            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes(); // clase configuracion de constantes

            JwtSecurityToken token;         // token 

            string secretKey;               // secret key
            string issuer ;                 //issuer
            string audience ;               //audience
            int daysExpTime;                //tiempo de expiración 
            dynamic key = null;             //key
            dynamic creds = null;           //credenciales
            dynamic expiration = null ;     //expiración
            dynamic claims =null;           //claims
            
            try
            {
                
                claims = new[]
                {
                    new Claim("login", userInfo.usuario),
                    new Claim("password", userInfo.password),
                    new Claim("empresa_id", empresa_id.ToString()),
                    new Claim("puntoventa_id", puntoventa_id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                secretKey = EN_ConfigConstantes.Instance.const_SecretKey;
                issuer = EN_ConfigConstantes.Instance.const_Issuer;
                audience = EN_ConfigConstantes.Instance.const_Audience;
                daysExpTime= Convert.ToInt32(EN_ConfigConstantes.Instance.const_ExpirationTime);
                key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));
                creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                expiration = DateTime.Now.AddDays(daysExpTime);

                token = new JwtSecurityToken(
                issuer: issuer,
                audience: audience,
                claims: claims,
                expires: expiration,
                signingCredentials: creds);

                concepto = new EN_Concepto();
                concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2002);
                respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc + " - TOKEN GENERADO CORRECTAMENTE";
                respuesta.RptaRegistroCpeDoc.MensajeResp= new JwtSecurityTokenHandler().WriteToken(token);;
                respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2002;
                ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_0, concepto.conceptocorr, concepto.conceptodesc);
                respuesta.ErrorWebService = ErrorWebSer;

                return respuesta;
            }
            catch (Exception ex)
            {
                concepto = new EN_Concepto();
                concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                respuesta.RptaRegistroCpeDoc.MensajeResp= ex.Message.ToString();
                respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_2, concepto.conceptocorr, concepto.conceptodesc);
                respuesta.ErrorWebService = ErrorWebSer;
                return respuesta;
            }

           

        }

       
    }
}