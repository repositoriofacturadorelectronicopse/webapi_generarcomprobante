/****************************************************************************************************************************************************************************************
 PROGRAMA: ChallengeMiddleware.cs
 VERSION : 1.0
 OBJETIVO: Clase de respuesta de creación de token
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using CEN;
using CLN;
using Microsoft.AspNetCore.Http;

namespace SAASWeb.Helpers
{
    public class ChallengeMiddleware
    {
        private static void writeErrorResponse(HttpContext Context, params string[] Errors)
        {
            // DESCRIPCION: ESCRIBIR RESPUESTA CON ERROR

            NE_Consulta consulta = new NE_Consulta();           // consulta
            EN_Concepto concepto;                               // entidad concepto
            string errores="";                                  // variable de error
            StringBuilder bld = new StringBuilder();            // acumulador de cadena

            Context.Response.ContentType = "application/json";
            using (var writer = new Utf8JsonWriter(Context.Response.BodyWriter))
            {
             
                foreach (var error in Errors)
                {
                    bld.Append(error +" ");
                }
                errores=  bld.ToString();
                 
                concepto = new EN_Concepto();
                concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_1, Convert.ToInt32(Context.Items["ErrorCod"].ToString()));

                writer.WriteStartObject();
                writer.WriteStartObject("rptaRegistroCpeDoc");
                writer.WriteBoolean("flagVerificacion", false);
                writer.WriteString("descripcionResp", concepto.conceptodesc);
                writer.WriteString("mensajeResp", EN_Constante.g_const_error_tokensession);
                writer.WriteNumber("codigo", Convert.ToInt32(Context.Items["ErrorCod"].ToString()));
                writer.WriteEndObject();
                writer.WriteStartObject("errorWebService");
                writer.WriteNumber("tipoError", EN_Constante.g_const_menos1);
                writer.WriteNumber("codigoError", Convert.ToInt32(Context.Items["ErrorCod"].ToString()));
                writer.WriteString("tipoError", errores);

                writer.WriteEndObject();
                writer.WriteEndObject();
                writer.Flush();
            }
        }
        private readonly RequestDelegate _request;      // PETICION

        public ChallengeMiddleware(RequestDelegate RequestDelegate)
        {
            // DESCRIPCION: FUNCION DE PETICION REQUERIDA

            if (RequestDelegate == null)
            {
                throw new ArgumentNullException(nameof(RequestDelegate)
                    , nameof(RequestDelegate) + " is required");
            }

            _request = RequestDelegate;
        }

        public async Task InvokeAsync(HttpContext Context)
        {
            // DESCRIPCION: FUNCION DE INVOCACION ASINCRONA

            if (Context == null)
            {
                throw new ArgumentNullException(nameof(Context)
                    , nameof(Context) + " is required");
            }

            await _request(Context);

            if(Context.Response.StatusCode == 401)
            {
                writeErrorResponse(Context,  Context.Items["ErrorMsjJwt"].ToString());
            }
           
        }
    }
}