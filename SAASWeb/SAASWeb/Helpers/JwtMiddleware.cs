/****************************************************************************************************************************************************************************************
 PROGRAMA: JwtMiddleware.cs
 VERSION : 1.0
 OBJETIVO: Clase middleware JWT 
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppServicioDoc;
using CEN;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using SAASWeb.Services;

namespace SAASWeb.Helpers
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;         // PETICION
        private readonly  string _appSettings;          // APPSETTING
        private readonly  string _codError401;          // CODIGO DE ERROR 401
         private readonly IConfiguration _configuration;    // INYECCION DE CONFIGURACION
         private  bool verifyPassLifeTime = false;          // VERIFICACION DE TIEMPO DE CADUCIDAD

        public JwtMiddleware(RequestDelegate next, IConfiguration configuration)
        {
            // DESCRIPCION: MIDDLEWARE JWT

            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes(); // clase configuracion de constantes
            string secretKey ;                                              // LLAVE SECRETA
            string  codError401 ;                                           // CODIGO DE ERROR 401

            this._configuration = configuration;
            //Obtenemos la clave secreta guardada en JwtSettings:SecretKey
            secretKey =  EN_ConfigConstantes.Instance.const_SecretKey;
            codError401 = EN_ConfigConstantes.Instance.const_codErrorJwt ;

            _next = next;
            _appSettings = secretKey;
            _codError401=codError401;
        }

        public async Task Invoke(HttpContext context, IUserService userService)
        {
            // DESCRIPCION: INVOCACION

            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();   // TOKEN

            if (token != null)
                attachUserToContext(context, userService, token);
               
            await _next(context);
   
        }

        private void attachUserToContext(HttpContext context, IUserService userService,string token)
        {
            // DESCRIPCION: ATTAUCH USER TO CONTEXT

            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();               // MANIPULADOR DE TOKEN
                var key = Encoding.UTF8.GetBytes(_appSettings);                 // CLAVE
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                verifyPassLifeTime=true;

                var jwtToken = (JwtSecurityToken)validatedToken;            // JWT TOKEN
            }
            catch(Exception ex)
            {
                context.Items["ErrorCod"] = _codError401;
                context.Items["ErrorMsjJwt"] = ex.Message;
            }
        }
    }
}