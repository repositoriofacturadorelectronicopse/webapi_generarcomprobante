using Microsoft.AspNetCore.Mvc.Filters;
using FluentValidation.WebApi;
using System.Net;
using CEN;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace SAASWeb
{
    class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();  //error
            var errors = new Dictionary<string, List<string>>();
            if(!context.ModelState.IsValid)
            {

                ErrorWebSer.CodigoError=-1;
                ErrorWebSer.TipoError=-1;
                foreach(var modelState in context.ModelState)
                {
                    errors.Add(modelState.Key,               
                    modelState.Value.Errors.Select(a=>a.ErrorMessage).ToList());
                }
                ErrorWebSer.DescripcionErr= errors.ToString();
                context.Result = new BadRequestObjectResult(ErrorWebSer);
                // context.Response=   
                //  context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, context.ModelState);
            }
        }

        
    }
}