using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using DAO.WebApi;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using SAASWeb.Helpers;
using SAASWeb.Services;
using CEN;
using AppServicioDoc;
using Amazon.S3;

namespace SAASWeb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
             
        }

        public IConfiguration Configuration { get; }
        

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            

            services.AddControllers( opt=>
            {
                var policy =  new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                opt.Filters.Add(new AuthorizeFilter(policy));


            });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "SAASWeb", Version = "v1" });
            });

             //servicio de conexion a base de datos
            services.AddDbContext<conexion>(options =>
                    options.UseSqlServer(Configuration.GetConnectionString("ConexionBdFE")));

            // agregando authenticacion JWT

            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes(); // clase configuracion constantes
            daoConfConst.guardar_ConfigConstantes_JWT();

            string secretKey = EN_ConfigConstantes.Instance.const_SecretKey;
            string issuer = EN_ConfigConstantes.Instance.const_Issuer;
            string audience = EN_ConfigConstantes.Instance.const_Audience;
            bool requireHttps= Convert.ToBoolean(EN_ConfigConstantes.Instance.const_requireHttps);
            
             var keyJwt= Encoding.ASCII.GetBytes(secretKey);
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme =   JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme =   JwtBearerDefaults.AuthenticationScheme;;
                })
                    .AddJwtBearer(options =>
                    {
                        options.RequireHttpsMetadata= requireHttps;
                        options.SaveToken=true;

                        options.TokenValidationParameters= new TokenValidationParameters 
                        {
                            ValidateIssuer= true,
                            ValidateAudience= true,
                            ValidateLifetime= true,
                            ValidateIssuerSigningKey=true,
                            ValidIssuer= issuer,
                            ValidAudience= audience,
                            IssuerSigningKey= new SymmetricSecurityKey(keyJwt),
                            ClockSkew = TimeSpan.Zero
                            
                        };
                    });
                

            services.AddScoped<IUserService, UserService>();

            
            // services.AddSingleton<clsIConfigManager, clsConfigManager>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

                // services.AddSingleton<IS3Service, S3Service>();
            services.AddAWSService<IAmazonS3>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "SAASWeb v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            // app.UseCors("CorsPolicy");
            app.UseAuthentication();
          
            app.UseMiddleware<ChallengeMiddleware>();

            app.UseAuthorization();

            app.UseMiddleware<JwtMiddleware>();

          

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        
    }
}
