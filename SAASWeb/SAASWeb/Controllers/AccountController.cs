/****************************************************************************************************************************************************************************************
 PROGRAMA: AccountController.cs
 VERSION : 1.0
 OBJETIVO: Clase controlador de cuentas
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CEN;
using Microsoft.AspNetCore.Authorization;
using SAASWeb.Services;
using AppServicioDoc;
using CAD;
using System;
using CLN;
using Newtonsoft.Json;
using System.Globalization;

namespace SAASWeb.Controllers
{
    [AllowAnonymous]
    [Produces("application/json")]
    [Route("api/")]
    public class AccountController:Controller
    {
        private readonly IUserService _userService; //servicio de usuario

        public AccountController(IUserService userService)
        {
            // DESCRIPCION: CLASE CONSTRUCTOR
            _userService = userService;
        }


        [Route("IniciarSession")]
        [HttpPost]
        public async Task<IActionResult> CrearToken([FromBody] EN_AuthUserInfo model)
        {
            // DESCRIPCION: WEB MÉTODO PARA CREAR TOKEN DE INICIO DE SESSIÓN

            ClassRptaGenerarCpeDoc respuestaVal = new ClassRptaGenerarCpeDoc() ;    // respuesta Generar comprobante documento
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;       // respuesta Generar comprobante documento
            AD_clsLog adlog =  new AD_clsLog();                                     // clase Log
            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes();         // clase configuracion de constantes
            NE_Consulta consulta = new NE_Consulta();                               // consulta
             EN_Concepto concepto;                                                  //entidad concepto
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();              //error
            Int32 ntraLog = EN_Constante.g_const_0;                                 // Número de transacción
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

            // var response = new EN_AuthenticateResponse();
            try
            {

                ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa, EN_Constante.g_const_null , EN_Constante.g_const_null, 
                                                EN_Constante.g_const_0, EN_Constante.g_const_0,  EN_Constante.g_const_null, EN_Constante.g_const_1,
                                                EN_Constante.g_const_funcion_crearToken, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                

                respuestaVal = consulta.ValidarCamposUserAuthWA(model);

                if(respuestaVal.RptaRegistroCpeDoc.FlagVerificacion)
                {
                    respuesta = _userService.Authenticate(model);

                    if (ModelState.IsValid)
                    {
                        if(respuesta.RptaRegistroCpeDoc.FlagVerificacion)
                        {
                            //registramos log fin
                            adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_1, respuesta.RptaRegistroCpeDoc.DescripcionResp, EN_Constante.g_const_valExito, 
                                                    confg.getDateHourNowLima(), JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(model));
                                                        
                          
                            return Ok(respuesta);

                        }else
                        {
                             //registramos log fin
                            adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_1, respuesta.RptaRegistroCpeDoc.DescripcionResp, EN_Constante.g_const_valExito,
                                                confg.getDateHourNowLima(),  JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(model));
                                                        
                            return BadRequest(respuesta);
                        }
                    }
                    else
                    {
                         //registramos log fin
                            adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_1, respuesta.RptaRegistroCpeDoc.DescripcionResp, EN_Constante.g_const_valExito, 
                                                confg.getDateHourNowLima(), JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(model));
                                                        
                        return BadRequest(ModelState);
                    }
                      
                }
                else
                {

                     //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, 
                                    EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_crearToken, respuesta.ErrorWebService.DescripcionErr,
                                     confg.getDateHourNowLima(),  JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(model));

                   
                    return Ok(respuestaVal);

                }
                
            }
            catch (System.Exception ex)
            {
                concepto = new EN_Concepto();
                concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno;
                respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_2, concepto.conceptocorr, ex.Message.ToString());
                respuesta.ErrorWebService = ErrorWebSer;

                 //registramos log fin
                adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, 
                                EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_crearToken, respuesta.ErrorWebService.DescripcionErr,
                                    confg.getDateHourNowLima(),  JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(model));

                
                return Ok(respuesta);
            }
            



           

        }


    
      

        
    }
}