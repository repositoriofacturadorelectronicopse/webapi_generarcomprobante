/****************************************************************************************************************************************************************************************
 PROGRAMA: ConsultasController.cs
 VERSION : 1.0
 OBJETIVO: Clase controlador de consulta
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using Microsoft.AspNetCore.Mvc;
using CEN;
using System;
using AppServicioDoc;
using SAASWeb.Services;
using CLN;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualBasic;
using System.Linq;
using CAD;
using Newtonsoft.Json;

namespace SAASWeb.Controllers
{

    [Produces("application/json")]
    [Route("api/")]
    
    public class ConsultasController : Controller
    {
        private IUserService _userService; // inyector de user service
        public ConsultasController( IUserService userService)
        {
             // DESCRIPCION: CLASE CONSTRUCTOR
            _userService=userService;
        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public void setIdEmpresaPtoVenta(ref string empresa_id, ref string puntoventa_id)
        {
            try
            {
                var id_empresa = User.FindFirst("empresa_id").Value;
                var id_puntoventa = User.FindFirst("puntoventa_id").Value;

                empresa_id =id_empresa.ToString();
                puntoventa_id =id_puntoventa.ToString();
            }
            catch (System.Exception ex )
            { 
                
                throw ex;
            }
            
        }

    
        [Route("ExisteRecurso")]
        [HttpGet]
        [AllowAnonymous]
        public ActionResult <Boolean>ExisteRecurso()
        {
           return Ok(true);
        }   

        [Route("ConsultarSituacionCpe")]
        [HttpPost]
        public List<EN_ResponseSyncEstados> GenerarComprobanteDoc([FromBody] List<EN_RequestSyncEstados> documento)
        {
            
            string empresa_id= EN_Constante.g_const_vacio;
            string puntoventa_id= EN_Constante.g_const_vacio;
            string id_cpe= EN_Constante.g_const_vacio;
            string ruc_emisor= EN_Constante.g_const_vacio;
            EN_ResponseSyncEstados response = new EN_ResponseSyncEstados();
            List<EN_ResponseSyncEstados> listaResponse = new  List<EN_ResponseSyncEstados>();
            NE_Documento ne_doc = new NE_Documento();
            EN_Empresa empresaDocumento = new EN_Empresa();             // Entidad empresa
            ClassRptaGenerarCpeDoc rutasCpe = new ClassRptaGenerarCpeDoc();     //respuesta para setear rutas de comprobantes
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes();     // clase configuracion constantes
            Int32 ntraLog = EN_Constante.g_const_0;                             // Número de transacción    
            AD_clsLog adlog =  new AD_clsLog();                                 // clase Log
            
            try
            {

                daoConfConst.guardar_ConfigConstantes_S3();
                setIdEmpresaPtoVenta(ref empresa_id, ref puntoventa_id) ;

                StringBuilder bld = new StringBuilder();
                foreach(EN_RequestSyncEstados item in documento)
                {   
                     bld.Append(item.tipodocumento_id+item.serie+"-"+item.numero + ",");
                }
                id_cpe= bld.ToString();
                if (id_cpe.Trim() != EN_Constante.g_const_vacio)
                {
                    id_cpe = id_cpe.Substring(0, Strings.Len(id_cpe) - EN_Constante.g_const_1);
                }
                else
                {
                    id_cpe = EN_Constante.g_const_vacio;
                }

                if(documento != null)
                {
                    listaResponse = (List<EN_ResponseSyncEstados>)ne_doc.ConsultarSituacionesCpes( Convert.ToInt32(empresa_id),  Convert.ToInt32(puntoventa_id), id_cpe);
                }

                empresaDocumento = GenerarComprobante.GetEmpresaById( Convert.ToInt32(empresa_id),  Convert.ToInt32(puntoventa_id),"");
                ruc_emisor=  empresaDocumento.Nrodocumento;
                  dynamic dataRec=documento.Where(dt => true == dt.send_s3 );
                foreach (var dr in dataRec)
                {
                    string id= dr.tipodocumento_id+dr.serie+"-"+dr.numero;

                    foreach (var rsp in listaResponse.Where(r => r.id == id))
                    {
                        
                        //obtenemos las rutas de los comprobantes pdf, xml y cdr del S3
                        string nombreArchivo=ruc_emisor+"-"+rsp.tipodocumento_id+"-"+rsp.serie+"-"+rsp.numero;
                        rutasCpe= confg.GetRutasDocsS3(ruc_emisor, nombreArchivo);

                        rsp.url_pdf= rutasCpe.RptaRegistroCpeDoc.ruta_pdf;
                        rsp.url_xml= rutasCpe.RptaRegistroCpeDoc.ruta_xml;
                        rsp.url_cdr= rutasCpe.RptaRegistroCpeDoc.ruta_cdr;
                    }
                }


                return listaResponse;

            }
            catch(Exception ex)
            {
                ntraLog = adlog.registrar_log_inicio(
                                        EN_Constante.g_const_programa, 
                                        EN_Constante.g_const_vacio, 
                                        EN_Constante.g_const_vacio, 
                                        Convert.ToInt32(empresa_id), 
                                        Convert.ToInt32(puntoventa_id),
                                        EN_Constante.g_const_vacio, 
                                        EN_Constante.g_const_1,
                                        EN_Constante.g_const_funcion_generarCpe, 
                                        EN_Constante.g_const_url, 
                                        EN_Constante.g_const_1,
                                        EN_Constante.g_const_1,
                                        EN_Constante.g_const_0,
                                        EN_Constante.g_const_0, 
                                        EN_Constante.g_const_vacio,
                                        EN_Constante.g_const_vacio,
                                        confg.getDateNowLima(), confg.getDateHourNowLima(),
                                        EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, 
                                        EN_Constante.g_const_1,
                                         EN_Constante.g_const_1, 
                                         EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, 
                                         ex.Message, 
                                        confg.getDateHourNowLima(), 
                                        JsonConvert.SerializeObject(listaResponse), 
                                        JsonConvert.SerializeObject(documento));
                
                    return listaResponse;


            }


           

           

        
         

      
            
             
        }   


   





      
   
    }

  
}