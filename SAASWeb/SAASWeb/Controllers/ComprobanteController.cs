/****************************************************************************************************************************************************************************************
 PROGRAMA: ComprobanteController.cs
 VERSION : 1.0
 OBJETIVO: Clase controlador de comprobante
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using Microsoft.AspNetCore.Mvc;
using CEN;
using System;
using AppServicioDoc;
using AppServicioCpe;
using AppServicioGuia;
using CAD;
using SAASWeb.Services;
using CLN;
using Newtonsoft.Json;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace SAASWeb.Controllers
{

    [Produces("application/json")]
    [Route("api/")]
    
    public class ComprobanteController : Controller
    {
        private IUserService _userService; // inyector de user service
        public ComprobanteController( IUserService userService)
        {
             // DESCRIPCION: CLASE CONSTRUCTOR
            _userService=userService;
        }


        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public void setIdEmpresaPtoVenta(ref string empresa_id, ref string puntoventa_id)
        {
            try
            {
                var id_empresa = User.FindFirst("empresa_id").Value;
                var id_puntoventa = User.FindFirst("puntoventa_id").Value;

                empresa_id =id_empresa.ToString();
                puntoventa_id =id_puntoventa.ToString();
            }
            catch (System.Exception ex )
            { 
                
                throw ex;
            }
            
        }
     

    
    
        [Route("GenerarComprobanteDoc")]
        [HttpPost]
        public ClassRptaGenerarCpeDoc GenerarComprobanteDoc([FromBody] DocumentoSunatTrama documento)
        {
            
            // setIdEmpresaPtoVenta();

            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;   // respuesta Generar comprobante documento
            AD_clsLog adlog =  new AD_clsLog();                                 // clase Log
            GenerarComprobante _crearComprobante=  new GenerarComprobante();    // crear comprobante
            int codRspta =1;                                                    // código respuesta          
            string msjRspta= EN_Constante.g_const_vacio;                        // mensaje respuesta
            string msjRptaError= EN_Constante.g_const_vacio;                    // mensaje respuesta de Error
            string nombreEmpresa  = EN_Constante.g_const_vacio ;                // nombre de empresa
            string comprobante =EN_Constante.g_const_vacio;                     // nombre de comprobante
            bool success= false;                                                // éxito
            Int32 ntraLog = EN_Constante.g_const_0;                             // Número de transacción    
            var documentoSunatTrama = documento;                                // documento sunat
            dynamic documentoSunat ;                                            // documento sunat
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();          //error
            NE_Consulta consulta = new NE_Consulta();                           // consulta
            EN_Concepto concepto;                                               //entidad concepto
            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes();     // clase configuracion constantes
            DocumentoSunat documentoS = new DocumentoSunat();
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            string ruc_emisor= EN_Constante.g_const_vacio;                      // ruc del emisor del comprobante
            string estado_comprobante= EN_Constante.g_const_situacion_pendiente;                      // ruc del emisor del comprobante
            ClassRptaGenerarCpeDoc rutasCpe = new ClassRptaGenerarCpeDoc();     //respuesta para setear rutas de comprobantes
           
            try
            {
                      
                daoConfConst.guardar_ConfigConstantes();

                //seteamos id de empresa y punto de venta obtenidos del JWT
                 string tmp_id_empresa= EN_Constante.g_const_vacio;
                string tmp_id_puntoventa= EN_Constante.g_const_vacio;
                setIdEmpresaPtoVenta(ref tmp_id_empresa, ref tmp_id_puntoventa) ;
                documentoSunatTrama.Idempresa =tmp_id_empresa;
                documentoSunatTrama.Idpuntoventa =tmp_id_puntoventa;

                //validar parametros obligatorios de entrada
                respuesta = consulta.ValidarCamposGenerarDoc(documentoSunatTrama);
                

                if(respuesta.RptaRegistroCpeDoc.FlagVerificacion)
                {
                    documentoSunat= consulta.ParsearCamposDoc(documentoSunatTrama);
                    
                    ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa, documentoSunat.Id, documentoSunat.Idtipodocumento, 
                                                documentoSunat.Idempresa, documentoSunat.Idpuntoventa,  nombreEmpresa, EN_Constante.g_const_1,
                                                EN_Constante.g_const_funcion_generarCpe, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                
                    //Generamos el comprobante
                    _crearComprobante.CrearDocumentoElectronico(documentoSunat,ref codRspta, ref msjRspta , ref success , ref comprobante, ref msjRptaError, ref ruc_emisor, ref estado_comprobante);
                    //validamos si todo fue correcto
                    if(success)
                    {   
                        //obtenemos las rutas de los comprobantes pdf, xml y cdr del S3
                        string nombreArchivo=ruc_emisor+"-"+ documentoSunat.Idtipodocumento+"-"+comprobante;
                       rutasCpe= confg.GetRutasDocsS3(ruc_emisor, nombreArchivo);

                       respuesta.RptaRegistroCpeDoc.ruta_pdf= rutasCpe.RptaRegistroCpeDoc.ruta_pdf;
                       respuesta.RptaRegistroCpeDoc.ruta_xml= rutasCpe.RptaRegistroCpeDoc.ruta_xml;
                       respuesta.RptaRegistroCpeDoc.ruta_cdr= rutasCpe.RptaRegistroCpeDoc.ruta_cdr;


                        //si se generó xml y envío (si es que está activo o no)
                        if(codRspta== EN_Constante.g_const_0 || codRspta==EN_Constante.g_const_1)
                        {
                            concepto = new EN_Concepto();
                            concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
                            respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                            respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.MensajeResp= msjRspta;
                            respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                            respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                            respuesta.RptaRegistroCpeDoc.estadoComprobante = estado_comprobante;
                            ErrorWebSer = consulta.llenarErrorWebService(Convert.ToInt16(codRspta), concepto.conceptocorr, concepto.conceptodesc + ((msjRptaError=="")?"":" - "+msjRptaError));
                            respuesta.ErrorWebService = ErrorWebSer;
                        }
                        else
                        {
                             //si se generó xml y envío (si es que está activo) no está prendido servidor
                            concepto = new EN_Concepto();
                            concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2001);
                            respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                            respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.MensajeResp= concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2001;
                            respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                            respuesta.RptaRegistroCpeDoc.estadoComprobante = estado_comprobante;
                            ErrorWebSer = consulta.llenarErrorWebService(Convert.ToInt16(codRspta), concepto.conceptocorr, msjRspta);
                            respuesta.ErrorWebService = ErrorWebSer;
                        }
                      

                    }else{
                       
                        //error al generar comprobante
                        concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_4000);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno ;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_4000;
                        respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                        respuesta.RptaRegistroCpeDoc.estadoComprobante = EN_Constante.g_const_vacio;
                        ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_menos1, concepto.conceptocorr,msjRspta);
                        respuesta.ErrorWebService = ErrorWebSer;

                    }

                     //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_1, respuesta.RptaRegistroCpeDoc.DescripcionResp, EN_Constante.g_const_valExito, 
                                                confg.getDateHourNowLima(), JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoSunatTrama));
              
                }
                else{
                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, 
                                    EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, respuesta.ErrorWebService.DescripcionErr,
                                     confg.getDateHourNowLima(), JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoSunatTrama));

                }

                return respuesta;
            }
            catch (Exception ex)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno ;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_menos1, concepto.conceptocorr, ex.Message);
                    respuesta.ErrorWebService = ErrorWebSer;

                     //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, ex.Message, 
                                        confg.getDateHourNowLima(), JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoSunatTrama));

                    return respuesta;
            
            }
             
        }   


   


        [Route("GenerarComunicadoBaja")]
        [HttpPost]
        public ClassRptaGenerarCpeDoc GenComunicadoBaja([FromBody] BajaSunat_Trama documento)
        {
            
        
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;   // respuesta Generar comprobante documento
            AD_clsLog adlog =  new AD_clsLog();                                 // clase Log
            GenerarComprobante _crearComprobante=  new GenerarComprobante();    // crear comprobante
            int codRspta =1;                                                    // código respuesta          
            string msjRspta= EN_Constante.g_const_vacio;                        // mensaje respuesta
            string nombreEmpresa  = EN_Constante.g_const_vacio ;                // nombre de empresa
            string comprobante =EN_Constante.g_const_vacio;                     // nombre de comprobante
            bool success= false;                                                // éxito
            Int32 ntraLog = EN_Constante.g_const_0;                             // Número de transacción  
            var documentoBajaTrama = documento;                                      // documento sunat trama
            dynamic documentoBaja;                                             // documento sunat
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();          //error
            NE_Consulta consulta = new NE_Consulta();                           // consulta
            EN_Concepto concepto;                                               //entidad concepto
            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes(); // clase configuracion constantes
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            
            try
            {
                      
                daoConfConst.guardar_ConfigConstantes();

                 //seteamos id de empresa y punto de venta obtenidos del JWT
                 string tmp_id_empresa= EN_Constante.g_const_vacio;
                string tmp_id_puntoventa= EN_Constante.g_const_vacio;
                setIdEmpresaPtoVenta(ref tmp_id_empresa, ref tmp_id_puntoventa) ;
                documentoBajaTrama.Empresa_id =tmp_id_empresa;
                documentoBajaTrama.Idpuntoventa =tmp_id_puntoventa;

                if(documentoBajaTrama!=null)  documentoBajaTrama.FechaComunicacion= confg.getDateNowLima();
                //validar parametros obligatorios de entrada
                respuesta = consulta.ValidarCamposGenComunicadoBaja(documentoBajaTrama);

                if(respuesta.RptaRegistroCpeDoc.FlagVerificacion)
                {
                    documentoBaja= consulta.ParsearCamposComunicadosBaja(documentoBajaTrama);

                    ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa, documentoBaja.Id.ToString(), documentoBaja.Tiporesumen, 
                                                documentoBaja.Empresa_id, documentoBaja.Idpuntoventa,  nombreEmpresa, EN_Constante.g_const_1,
                                                EN_Constante.g_const_funcion_generarComunicadoBaja, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);

                    _crearComprobante.CrearBajaElectronica(documentoBaja,ref codRspta, ref msjRspta, ref comprobante, ref success);

                    if(success)
                    {
                        //si se generó xml y envío (si es que está activo o no)
                        if(codRspta== EN_Constante.g_const_0 || codRspta==EN_Constante.g_const_1)
                        {
                            concepto = new EN_Concepto();
                            concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
                            respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                            respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.MensajeResp= msjRspta;
                            respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                            respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                            ErrorWebSer = consulta.llenarErrorWebService(Convert.ToInt16(codRspta), concepto.conceptocorr, concepto.conceptodesc);
                            respuesta.ErrorWebService = ErrorWebSer;
                        }
                        
                        else if(codRspta== EN_Constante.g_const_2){

                            concepto = new EN_Concepto();
                            concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2001);
                            respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                            respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.MensajeResp= msjRspta;
                            respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2001;
                            respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                            ErrorWebSer = consulta.llenarErrorWebService(Convert.ToInt16(codRspta), concepto.conceptocorr, concepto.conceptodesc);
                            respuesta.ErrorWebService = ErrorWebSer;
                        }
                        else
                        {
                             //si se generó xml y envío (si es que está activo) no está prendido servidor
                            concepto = new EN_Concepto();
                            concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2001);
                            respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                            respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.MensajeResp= concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2001;
                            respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                            ErrorWebSer = consulta.llenarErrorWebService(Convert.ToInt16(codRspta), concepto.conceptocorr, msjRspta);
                            respuesta.ErrorWebService = ErrorWebSer;
                        }
                      

                    }else{
                       
                          //error al generar comprobante
                        concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_4000);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno ;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_4000;
                        respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                        ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_menos1, concepto.conceptocorr,msjRspta);
                        respuesta.ErrorWebService = ErrorWebSer;

                    }

                     //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_1, respuesta.RptaRegistroCpeDoc.DescripcionResp, EN_Constante.g_const_valExito, 
                                                confg.getDateHourNowLima(),JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoBajaTrama));
              
                }
                else{
                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, 
                                    EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, respuesta.ErrorWebService.DescripcionErr,
                                     confg.getDateHourNowLima(), JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoBajaTrama));

                }

                return respuesta;
            }
            catch (Exception ex)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno ;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_menos1, concepto.conceptocorr, ex.Message);
                    respuesta.ErrorWebService = ErrorWebSer;

                     //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, ex.Message, 
                                            confg.getDateHourNowLima(),JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoBajaTrama));

                    return respuesta;
            
            }
             
        }


        [Route("GenerarOtrosCpe")]
        [HttpPost]
        public ClassRptaGenerarCpeDoc GenerarOtrosCpe([FromBody] OtroscpeSunatTrama documento)
        {
            
        
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;   // respuesta Generar comprobante documento
            AD_clsLog adlog =  new AD_clsLog();                                 // clase Log
            GenerarCpe _crearComprobante=  new GenerarCpe();    // crear comprobante
            int codRspta =1;                                                    // código respuesta          
            string msjRspta= EN_Constante.g_const_vacio;                        // mensaje respuesta
            string msjRptaError= EN_Constante.g_const_vacio;                    // mensaje respuesta de Error
            string nombreEmpresa  = EN_Constante.g_const_vacio ;                // nombre de empresa
            string comprobante =EN_Constante.g_const_vacio;                     // nombre de comprobante
            bool success= false;                                                // éxito
            Int32 ntraLog = EN_Constante.g_const_0;                             // Número de transacción    
            var documentoSunatTrama = documento;                                // documento sunat trama
            dynamic documentoSunat;                                             // documento sunat
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();          //error
            NE_Consulta consulta = new NE_Consulta();                           // consulta
            EN_Concepto concepto;                                               //entidad concepto
  
            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes(); // clase configuracion constantes
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración

             string ruc_emisor= EN_Constante.g_const_vacio;                      // ruc del emisor del comprobante
            string estado_comprobante= EN_Constante.g_const_situacion_pendiente;                      // ruc del emisor del comprobante
            ClassRptaGenerarCpeDoc rutasCpe = new ClassRptaGenerarCpeDoc();     //respuesta para setear rutas de comprobantes
            try
            {
                      
                daoConfConst.guardar_ConfigConstantes();

                     //seteamos id de empresa y punto de venta obtenidos del JWT
                 string tmp_id_empresa= EN_Constante.g_const_vacio;
                string tmp_id_puntoventa= EN_Constante.g_const_vacio;
                setIdEmpresaPtoVenta(ref tmp_id_empresa, ref tmp_id_puntoventa) ;
                documentoSunatTrama.empresa_id =tmp_id_empresa;
                documentoSunatTrama.puntoventa_id =tmp_id_puntoventa;
          
                //validar parametros obligatorios de entrada
                respuesta = consulta.ValidarCamposGenerarOtrosCpeDoc(documentoSunatTrama);

                if(respuesta.RptaRegistroCpeDoc.FlagVerificacion)
                {

                    documentoSunat= consulta.ParsearCamposOtrosCpe(documentoSunatTrama);

                    ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa, documentoSunat.id, documentoSunat.tipodocumento_id, 
                                                                    documentoSunat.empresa_id, documentoSunat.puntoventa_id,  nombreEmpresa, EN_Constante.g_const_1,
                                                                    EN_Constante.g_const_funcion_generarOtrosCpe, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                                    EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                                    confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                                    EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                                    
                    //Generamos el comprobante
                    _crearComprobante.CrearCpeElectronica(documentoSunat,ref codRspta, ref msjRspta , ref success , ref comprobante, ref msjRptaError, ref ruc_emisor, ref estado_comprobante);
                    //validamos si todo fue correcto
                    if(success)
                    {
                         //obtenemos las rutas de los comprobantes pdf, xml y cdr del S3
                        string nombreArchivo=ruc_emisor+"-"+ documentoSunat.tipodocumento_id+"-"+comprobante;
                       rutasCpe= confg.GetRutasDocsS3(ruc_emisor, nombreArchivo);

                        respuesta.RptaRegistroCpeDoc.ruta_pdf= rutasCpe.RptaRegistroCpeDoc.ruta_pdf;
                       respuesta.RptaRegistroCpeDoc.ruta_xml= rutasCpe.RptaRegistroCpeDoc.ruta_xml;
                       respuesta.RptaRegistroCpeDoc.ruta_cdr= rutasCpe.RptaRegistroCpeDoc.ruta_cdr;

                        //si se generó xml y envío (si es que está activo o no)
                        if(codRspta== EN_Constante.g_const_0 || codRspta==EN_Constante.g_const_1)
                        {
                            concepto = new EN_Concepto();
                            concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
                            respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                            respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.MensajeResp= msjRspta;
                            respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                            respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                            respuesta.RptaRegistroCpeDoc.estadoComprobante = estado_comprobante;
                            ErrorWebSer = consulta.llenarErrorWebService(Convert.ToInt16(codRspta), concepto.conceptocorr, concepto.conceptodesc + " - "+msjRptaError);
                            respuesta.ErrorWebService = ErrorWebSer;
                        }
                        else
                        {
                             //si se generó xml y envío (si es que está activo) no está prendido servidor
                            concepto = new EN_Concepto();
                            concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2001);
                            respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                            respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.MensajeResp= concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2001;
                            respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                            respuesta.RptaRegistroCpeDoc.estadoComprobante = estado_comprobante;
                            ErrorWebSer = consulta.llenarErrorWebService(Convert.ToInt16(codRspta), concepto.conceptocorr, msjRspta);
                            respuesta.ErrorWebService = ErrorWebSer;
                        }
                      

                    }else{
                       
                          //error al generar comprobante
                        concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_4000);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno ;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_4000;
                        respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                        ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_menos1, concepto.conceptocorr,msjRspta);
                        respuesta.ErrorWebService = ErrorWebSer;

                    }

                     //registramos log fin
                        adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_1, respuesta.RptaRegistroCpeDoc.DescripcionResp, EN_Constante.g_const_valExito, 
                                         confg.getDateHourNowLima(),
                                                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoSunat));
              
                }
                else{
                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, 
                                    EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, respuesta.ErrorWebService.DescripcionErr,
                                     confg.getDateHourNowLima(),
                                     JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoSunatTrama));

                }

                return respuesta;
            }
            catch (Exception ex)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno ;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_menos1, concepto.conceptocorr, ex.Message);
                    respuesta.ErrorWebService = ErrorWebSer;

                     //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, ex.Message, confg.getDateHourNowLima(),
                                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoSunatTrama));

                    return respuesta;
            
            }
             
        } 



        [Route("GenerarReversionOtroscpe")]
        [HttpPost]
        public ClassRptaGenerarCpeDoc GenReversionOtrosCpe([FromBody] ReversionSunatTrama documento)
        {
            
        
            ClassRptaGenerarCpeDoc respuesta = new ClassRptaGenerarCpeDoc() ;   // respuesta Generar comprobante documento
            AD_clsLog adlog =  new AD_clsLog();                                 // clase Log
            GenerarCpe _crearComprobante=  new GenerarCpe();                    // crear comprobante otros cpe
            int codRspta =1;                                                    // código respuesta          
            string msjRspta= EN_Constante.g_const_vacio;                        // mensaje respuesta
            string nombreEmpresa  = EN_Constante.g_const_vacio ;                // nombre de empresa
            string comprobante =EN_Constante.g_const_vacio;                     // nombre de comprobante
            bool success= false;                                                // éxito
            Int32 ntraLog = EN_Constante.g_const_0;                             // Número de transacción    
            
            var documentoBajaTrama = documento;                                      // documento sunat trama
            dynamic documentoBaja;                                             // documento sunat
            EN_ErrorWebService ErrorWebSer = new EN_ErrorWebService();          //error
            NE_Consulta consulta = new NE_Consulta();                           // consulta
            EN_Concepto concepto;                                               //entidad concepto
            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes(); // clase configuracion constantes
            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            try
            {
                      
                daoConfConst.guardar_ConfigConstantes();

                     //seteamos id de empresa y punto de venta obtenidos del JWT
                 string tmp_id_empresa= EN_Constante.g_const_vacio;
                string tmp_id_puntoventa= EN_Constante.g_const_vacio;
                setIdEmpresaPtoVenta(ref tmp_id_empresa, ref tmp_id_puntoventa) ;
                documentoBajaTrama.Empresa_id =tmp_id_empresa;
                documentoBajaTrama.puntoventa_id =tmp_id_puntoventa;
                
                if(documentoBajaTrama!=null)  documentoBajaTrama.FechaComunicacion= confg.getDateNowLima();

                //validar parametros obligatorios de entrada
                respuesta = consulta.ValidarCamposGenReversion(documentoBajaTrama);
               

                if(respuesta.RptaRegistroCpeDoc.FlagVerificacion)
                {
                   

                    documentoBaja= consulta.ParsearCamposReversion(documentoBajaTrama);

                    ntraLog = adlog.registrar_log_inicio(EN_Constante.g_const_programa, documentoBaja.Id.ToString(), documentoBaja.Tiporesumen, 
                                                documentoBaja.Empresa_id, documentoBaja.puntoventa_id,  nombreEmpresa, EN_Constante.g_const_1,
                                                EN_Constante.g_const_funcion_generarReversionOtroscpe, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                                                EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                                                confg.getDateNowLima(), confg.getDateHourNowLima(),
                                                EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                    //Generamos el comprobante
                     
                    _crearComprobante.CrearReversionElectronica(documentoBaja,ref codRspta, ref msjRspta, ref comprobante, ref success);
                    //validamos si todo fue correcto
                    if(success)
                    {
                        //si se generó xml y envío (si es que está activo o no)
                        if(codRspta== EN_Constante.g_const_0 || codRspta==EN_Constante.g_const_1)
                        {
                            concepto = new EN_Concepto();
                            concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2000);
                            respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                            respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.MensajeResp= msjRspta;
                            respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2000;
                            respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                            ErrorWebSer = consulta.llenarErrorWebService(Convert.ToInt16(codRspta), concepto.conceptocorr, concepto.conceptodesc);
                            respuesta.ErrorWebService = ErrorWebSer;
                        }
                        else
                        {
                             //si se generó xml y envío (si es que está activo) no está prendido servidor
                            concepto = new EN_Concepto();
                            concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_2001);
                            respuesta.RptaRegistroCpeDoc.FlagVerificacion = true;
                            respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.MensajeResp= concepto.conceptodesc;
                            respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_2001;
                            respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                            ErrorWebSer = consulta.llenarErrorWebService(Convert.ToInt16(codRspta), concepto.conceptocorr, msjRspta);
                            respuesta.ErrorWebService = ErrorWebSer;
                        }
                      

                    }else{
                       
                          //error al generar comprobante
                        concepto = new EN_Concepto();
                        concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_4000);
                        respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                        respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                        respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno ;
                        respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_4000;
                        respuesta.RptaRegistroCpeDoc.comprobante = comprobante;
                        ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_menos1, concepto.conceptocorr,msjRspta);
                        respuesta.ErrorWebService = ErrorWebSer;

                    }

                     //registramos log fin
                        adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_1, respuesta.RptaRegistroCpeDoc.DescripcionResp, EN_Constante.g_const_valExito, 
                                                confg.getDateHourNowLima(),JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoBaja));
              
                }
                else{
                    //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, 
                                    EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, respuesta.ErrorWebService.DescripcionErr,
                                     confg.getDateHourNowLima(), JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoBajaTrama));

                }

                return respuesta;
            }
            catch (Exception ex)
            {
                    concepto = new EN_Concepto();
                    concepto= consulta.ObtenerDescConcepto(EN_Constante.g_const_prefijo_cpe_doc, EN_Constante.g_const_3000);
                    respuesta.RptaRegistroCpeDoc.FlagVerificacion = false;
                    respuesta.RptaRegistroCpeDoc.DescripcionResp = concepto.conceptodesc;
                    respuesta.RptaRegistroCpeDoc.MensajeResp= EN_Constante.g_const_error_interno ;
                    respuesta.RptaRegistroCpeDoc.codigo = EN_Constante.g_const_3000;
                    ErrorWebSer = consulta.llenarErrorWebService(EN_Constante.g_const_menos1, concepto.conceptocorr, ex.Message);
                    respuesta.ErrorWebService = ErrorWebSer;

                     //registramos log fin
                    adlog.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_1, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarCpe, ex.Message, 
                                            confg.getDateHourNowLima(),JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(documentoBajaTrama));

                    return respuesta;
            
            }
             
        }


        
        [Route("CrearGuiaXml")]
         [HttpPost]
        public EN_ResponseGuia CrearGuiaXml([FromBody] GuiaSunatTrama oGuiaTrama)
        {
            //DESCRIPCION: WEB METODO PARA CREAR GUIA ELECTRONICA
        
            EN_ResponseGuia response = new EN_ResponseGuia();
            EnvioGuia crearGuia = new EnvioGuia();
            EN_ResponseGuia respuesta = new EN_ResponseGuia();
            NE_Consulta verificacion = new NE_Consulta();
            EN_RespuestaTablaSunat respParametros = new EN_RespuestaTablaSunat();
            EN_RespuestaCrearGuia  repuestCrearGuia = new EN_RespuestaCrearGuia();
            EN_Concepto concepto = new EN_Concepto();
            NE_Guia objGuia = new NE_Guia();
            NE_Guia obj = new NE_Guia();
            EN_ErrorWebService errWebServ = new EN_ErrorWebService();
            DAO_ConfigConstantes daoConfConst = new DAO_ConfigConstantes();
            AD_clsLog adlog =  new AD_clsLog();   
            AD_Guia cadGuia = new AD_Guia();
            Int32 ntraLog = EN_Constante.g_const_0; // Número de transacción
            int codRspta =1;
            string msjRspta= "";
            string nombreEmpresa ="";
            GuiaSunat oGuia = new GuiaSunat();

            AppConfiguracion.Configuracion confg= new AppConfiguracion.Configuracion(); // Clase configuración
            string ruc_emisor= EN_Constante.g_const_vacio;                      // ruc del emisor del comprobante
            string estado_comprobante= EN_Constante.g_const_situacion_pendiente;                      // ruc del emisor del comprobante
            ClassRptaGenerarCpeDoc rutasCpe = new ClassRptaGenerarCpeDoc();     //respuesta para setear rutas de comprobantes


            try
            {
                respParametros = daoConfConst.listarConfiguracionConstante();

                //seteamos id de empresa y punto de venta obtenidos del JWT
                 string tmp_id_empresa= EN_Constante.g_const_vacio;
                string tmp_id_puntoventa= EN_Constante.g_const_vacio;
                setIdEmpresaPtoVenta(ref tmp_id_empresa, ref tmp_id_puntoventa) ;
                oGuiaTrama.empresa_id =tmp_id_empresa;
                oGuiaTrama.puntoemision =tmp_id_puntoventa; 

                if (respParametros.ResplistaTablaSunat.FlagVerificacion)
                {

                    respuesta = verificacion.validarCamposGuiaElectronica(oGuiaTrama);
                                     
                    if(respuesta.RespGuiaElectronica.FlagVerificacion) 
                    {
                        if (oGuiaTrama.nrobultos.Trim() == "") 
                        {
                            oGuiaTrama.nrobultos = "0";
                        }
                        oGuia = verificacion.ParsearCamposGuia(oGuiaTrama);

                        ntraLog = cadGuia.registrar_log_inicio(EN_Constante.g_const_programa, oGuia.id, oGuia.tipodocumento_id, 
                        oGuia.empresa_id, oGuia.puntoemision,  nombreEmpresa, EN_Constante.g_const_1,
                        EN_Constante.g_const_funcion_generarGuia, EN_Constante.g_const_url, EN_Constante.g_const_1,EN_Constante.g_const_1,
                        EN_Constante.g_const_0,EN_Constante.g_const_0, EN_Constante.g_const_vacio,EN_Constante.g_const_vacio,
                        verificacion.getDateNowLima(),verificacion.getDateHourNowLima(),
                        EN_Constante.g_const_null, EN_Constante.g_const_null, EN_Constante.g_const_null,EN_Constante.g_const_user_log);
                        
                                   
                        repuestCrearGuia = crearGuia.CrearGuiaElectronica(oGuia,ref codRspta, ref msjRspta , ref ruc_emisor, ref estado_comprobante);
                        respuesta.RespGuiaElectronica = repuestCrearGuia.RespGenerarGuia;
                        respuesta.ErrorWebServ = repuestCrearGuia.ErrorWebServ;

                        //obtenemos las rutas de los comprobantes pdf, xml y cdr del S3
                        string nombreArchivo=ruc_emisor+"-"+ oGuia.tipodocumento_id+"-"+repuestCrearGuia.RespGenerarGuia.comprobante;
                        rutasCpe= confg.GetRutasDocsS3(ruc_emisor, nombreArchivo);

                        respuesta.RespGuiaElectronica.ruta_pdf= rutasCpe.RptaRegistroCpeDoc.ruta_pdf;
                        respuesta.RespGuiaElectronica.ruta_xml= rutasCpe.RptaRegistroCpeDoc.ruta_xml;
                        respuesta.RespGuiaElectronica.ruta_cdr= rutasCpe.RptaRegistroCpeDoc.ruta_cdr;

                        respuesta.RespGuiaElectronica.estadoComprobante = estado_comprobante;

                      if (respuesta.RespGuiaElectronica.FlagVerificacion) 
                      {



                            
                            concepto = obj.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1021);
                            respuesta.RespGuiaElectronica.FlagVerificacion = true;
                            respuesta.RespGuiaElectronica.DescRespuesta = concepto.conceptodesc;
                            concepto = obj.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_2002);
                            errWebServ.DescripcionErr = concepto.conceptodesc;
                            errWebServ.CodigoError = EN_Constante.g_const_2000;
                            errWebServ.TipoError = EN_Constante.g_const_0;
                            respuesta.ErrorWebServ = errWebServ;
                            cadGuia.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_3, 
                            respuesta.ErrorWebServ.DescripcionErr,  EN_Constante.g_const_valExito,
                            verificacion.getDateHourNowLima(),
                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(oGuia));
                      }
                      else {
                        if (codRspta == 1) {
                              cadGuia.registrar_log_fin(ntraLog, EN_Constante.g_const_0, EN_Constante.g_const_3, 
                            respuesta.ErrorWebServ.DescripcionErr,  EN_Constante.g_const_valExito,
                            verificacion.getDateHourNowLima(),
                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(oGuia));

                          } else  {
                              cadGuia.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_3, 
                            respuesta.ErrorWebServ.DescripcionErr,EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarXML, 
                            verificacion.getDateHourNowLima(),
                            JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(oGuia));

                          }
                      }

                    } else {
                        cadGuia.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_3, 
                        respuesta.ErrorWebServ.DescripcionErr, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarXML, 
                        verificacion.getDateHourNowLima(),
                        JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(oGuia));
                    }
                } else {
                    respuesta.RespGuiaElectronica.FlagVerificacion = respParametros.ResplistaTablaSunat.FlagVerificacion;
                    respuesta.RespGuiaElectronica.DescRespuesta = respParametros.ResplistaTablaSunat.DescRespuesta;
                    respuesta.ErrorWebServ = respParametros.ErrorWebServ;
                    cadGuia.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_3, 
                    respuesta.ErrorWebServ.DescripcionErr, EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarXML, 
                    verificacion.getDateHourNowLima(),
                    JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(oGuia));

                }

                return respuesta; 
            }
            catch (Exception ex)
            {
                
                respuesta.RespGuiaElectronica.FlagVerificacion = false;
                respuesta.RespGuiaElectronica.DescRespuesta = EN_Constante.g_const_error_interno;
                respuesta.ErrorWebServ.TipoError = EN_Constante.g_const_1;
                respuesta.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
                respuesta.ErrorWebServ.DescripcionErr = ex.Message;
                 cadGuia.registrar_log_fin(ntraLog, EN_Constante.g_const_1, EN_Constante.g_const_3, 
                  respuesta.ErrorWebServ.DescripcionErr,EN_Constante.g_const_errlogCons + EN_Constante.g_const_funcion_generarXML, 
                 verificacion.getDateHourNowLima(),
                 JsonConvert.SerializeObject(respuesta), JsonConvert.SerializeObject(oGuia));
                return respuesta;
            }
        } 






      
   
    }

  
}