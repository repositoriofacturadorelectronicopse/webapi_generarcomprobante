﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: conexion.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a cadena de conexion a base de datos SQL
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using Microsoft.EntityFrameworkCore;
using CEN;

namespace DAO.WebApi
{
    public class conexion: DbContext
    {
        public DbSet<AU_Empresa> Empresa { get; set; }      // EMPRESA
        public DbSet<EN_Usuario> Tbl_usuario { get; set; }  //TABLA USUARIO
  
       
        public conexion()
        {
            // DESCRIPCION: CONSTRUCTOR DE CLASE
        }
  
        public conexion(DbContextOptions options): base(options)  
        { 
            // DESCRIPCION: CONSTRUCTOR DE CLASE
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
             // DESCRIPCION: CONFIGURACION DE CONEXION

            string cadena= EN_ConfigConstantes.Instance.const_cadenaCnxBdFE;        //CADENA DE CONEXION
            if (!optionBuilder.IsConfigured)
            {
                optionBuilder.UseSqlServer(cadena);
            }
        }
    }
}
