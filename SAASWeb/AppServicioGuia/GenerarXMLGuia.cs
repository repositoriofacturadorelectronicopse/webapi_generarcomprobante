using System;
using CEN;
using CLN;
using AppConfiguracion;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;
using CAD;
using System.Xml.Xsl;
using System.Xml;
using System.IO;
using System.IO.Compression;
using Microsoft.VisualBasic;
using System.Linq;
using System.Xml.Schema;
using System.Xml.XPath;
namespace AppServicioGuia
{
    public class GenerarXMLGuia
    {
          public bool GenerarXmlGuiasElectronicas(EN_Guia oGuia, EN_Empresa oEmpresa, EN_Certificado oCertificado, List<EN_DetalleGuia> detalleGuia, EN_ComprobanteSunat oCompSunat, string rutabase, string rutadoc, string motivotraslado, string modalidadtraslado, string origen, string destino, ref string nombre, ref string vResumen, ref string vFirma, ref string msjeRspta)
        {
            //DESCRIPCION: Generar guia electrónica
             GenerarPDF creaPDF = new GenerarPDF();
             EN_ActualizarGuia ResptaActualizarGuia = new EN_ActualizarGuia();
             EN_RespuestaRegistro RsptaCrearXML = new EN_RespuestaRegistro();
             NE_Guia obj = new NE_Guia();
            string mensaje = ""; //Variable de mensaje
            string cadenaXML = ""; //Variable de cadena XML
            
            try {
                if(oGuia.tipodocumento_id.Trim() == oCompSunat.IdGR){
                    ResptaActualizarGuia = GuardarGuiaNombreValorResumenFirma(oGuia, nombre, vResumen, vFirma, cadenaXML);
                    RsptaCrearXML = GenerarXmlGuia(oGuia, oCompSunat, oEmpresa, oCertificado, rutabase, rutadoc, ref nombre, ref vResumen, ref vFirma, ref mensaje,ref cadenaXML);
                    if(RsptaCrearXML.FlagVerificacion) {
                         if(ResptaActualizarGuia.RespActualizarGuia.FlagVerificacion) {
                                //Genero el documento en PDF
                                creaPDF.GenerarPDFGuiaRemision(oEmpresa, oGuia, detalleGuia, oCompSunat, motivotraslado, modalidadtraslado, origen, destino, vResumen, vFirma, rutabase, rutadoc);
                                return true;
                            } else {
                                nombre = "";
                                vResumen = "";
                                vFirma = "";
                                msjeRspta = "Error al guardar el valor resumen, firma digital y nombre del archivo XML.";
                                return false;
                            }
                    } else {
                         // Si hubo error al generar el XML se elimina el XML
                        nombre = "";
                        vResumen = "";
                        vFirma = "";
                        msjeRspta = mensaje;
                        // Se elimina el documento de la base de datos                       
                        obj.eliminarGuia(oGuia);
                        return false;

                    }

                } else {
                    nombre = "";
                    vResumen = "";
                    vFirma = "";
                    msjeRspta = "El comprobante a generar no es válido.";
                    return false;

                }

            
            }catch(Exception ex) {
                throw ex;

            }
            
        }

        public EN_RespuestaRegistro GenerarXmlGuia(EN_Guia oGuia, EN_ComprobanteSunat oCompSunat, EN_Empresa oEmpresa, EN_Certificado oCertificado, string rutabase, string rutadoc, ref string nombre, ref string vResumen, ref string vFirma, ref string msjRspta, ref string cadenaXML)
        {
            //DESCRIPCION: Generar XML GUIA
            XmlWriterSettings settings = new XmlWriterSettings();
            EN_RespuestaRegistro RsptaCrearXML = new EN_RespuestaRegistro();
            EN_RespuestaListaDetalleGuia RsptaListarDetalleGuia = new EN_RespuestaListaDetalleGuia();
            EN_Concepto concepto = new EN_Concepto();
            NE_Guia objProceso = new NE_Guia();
            List<EN_DetalleGuia> listaDetalle = new List<EN_DetalleGuia>();
            NE_Guia guia = new NE_Guia();
            string prefixCac = string.Empty; //VARIABLE prefijo cac
            string prefixCbc = string.Empty; //Variable prefijo CBC
            string prefixExt = string.Empty; //variable de prefijo Ext
            string prefixDs = string.Empty; //variable de prefijo DS
            string cadCac = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"; // encabezado de XML
            string cadCbc = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"; // encabezado de XML
            string cadExt = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"; // encabezado de XML
            string cadXsd = "http://www.w3.org/2001/XMLSchema"; // encabezado de XML
            string cadCcts = "urn:un:unece:uncefact:documentation:2"; // encabezado de XML

            string cadDs = "http://www.w3.org/2000/09/xmldsig#"; // encabezado de XML

            string cdrXML = string.Empty; // CDR XML

            string rutaemp = string.Empty; //Ruta
            try
            {
                rutaemp = String.Format("{0}/{1}/", rutabase, oGuia.numerodocempresa);

                // Validamos si existe directorio del documento, de lo contrario lo creamos
                // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
                if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml)))
                    System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml);

                if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf)))
                    System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf);

                if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr)))
                    System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr);

                rutadoc=rutadoc + EN_Constante.g_const_rutaSufijo_xml;
               
                 RsptaListarDetalleGuia = objProceso.listarDetalleGuia(oGuia);
                 if (!RsptaListarDetalleGuia.ResplistaDetalleGuia.FlagVerificacion){
                     RsptaCrearXML.FlagVerificacion = false;
                     RsptaCrearXML.DescRespuesta = RsptaListarDetalleGuia.ResplistaDetalleGuia.DescRespuesta;
                     return RsptaCrearXML;
                 }
                 nombre = oEmpresa.Nrodocumento.Trim() + "-" + oGuia.tipodocumento_id.Trim() + "-" + oGuia.serie.Trim() + "-" + oGuia.numero.Trim();
                //Validamos si existe comprobante en XML, ZIP y PDF generado y eliminamos
                if(File.Exists(rutadoc.Trim() + nombre.Trim() +  EN_Constante.g_const_extension_xml)) {
                    File.Delete(rutadoc.Trim() + nombre.Trim() +  EN_Constante.g_const_extension_xml);
                }
                if(File.Exists(rutadoc.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip)) {
                    File.Delete(rutadoc.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);
                }
                if(File.Exists(rutadoc.Trim() + nombre.Trim() + EN_Constante.g_const_extension_pdf)) {
                    File.Delete(rutadoc.Trim() + nombre.Trim() + EN_Constante.g_const_extension_pdf);
                }
                if(File.Exists(rutadoc.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() +  EN_Constante.g_const_extension_xml)) {
                    File.Delete(rutadoc.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() +  EN_Constante.g_const_extension_xml);
                }                                   
                if(File.Exists(rutadoc.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip)) {
                    File.Delete(rutadoc.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);
                }
                settings.Encoding = new System.Text.UTF8Encoding(false);
                settings.Indent = true;
                using (XmlWriter writer = XmlWriter.Create(rutadoc.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml, settings))
                {
                    prefixCac = writer.LookupPrefix(cadCac);
                    prefixCbc = writer.LookupPrefix(cadCbc);
                    prefixExt = writer.LookupPrefix(cadExt);
                    prefixDs = writer.LookupPrefix(cadDs);
                    writer.WriteStartDocument();
                    writer.WriteStartElement("DespatchAdvice", "urn:oasis:names:specification:ubl:schema:xsd:DespatchAdvice-2");

                    writer.WriteAttributeString("xmlns", "cac", null, cadCac);
                    writer.WriteAttributeString("xmlns", "cbc", null, cadCbc);
                    writer.WriteAttributeString("xmlns", "ext", null, cadExt);
                    writer.WriteAttributeString("xmlns", "xsd", null, cadXsd);
                    writer.WriteAttributeString("xmlns", "ccts", null, cadCcts);

                    writer.WriteAttributeString("xmlns", "ds", null, cadDs);

                    ObtenerExtensionesGuia(writer, prefixExt, cadExt);
                    ObtenerDatosCabeceraGuia(writer, prefixCbc, prefixCac, cadCbc, cadCac, oGuia, oEmpresa, oCompSunat);
                    if(RsptaListarDetalleGuia.listDetGuia.Count > 0 ) {
                    foreach(var item in RsptaListarDetalleGuia.listDetGuia) {
                    
                        ObtenerDatosDetalleGuia(writer, prefixCbc, prefixCac, cadCbc, cadCac, item, oCompSunat);
                    }
                    }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();

                }
                RsptaCrearXML = AgregarFirma(oCertificado, rutadoc, rutaemp, nombre, ref vResumen, ref vFirma, ref cadenaXML);
                
                //COPIAMOS XML A S3 Y ELIMINAMOS DE TEMPORAL
                AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oGuia.numerodocempresa,rutabase,EN_Constante.g_const_rutaSufijo_xml,nombre.Trim(),EN_Constante.g_const_extension_xml);

                if (!RsptaCrearXML.FlagVerificacion){
                    return RsptaCrearXML;
                }

                msjRspta = "Guía de Remisión Generada Correctamente";  
                concepto = guia.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1021);    
                RsptaCrearXML.FlagVerificacion = true;
                RsptaCrearXML.DescRespuesta = concepto.conceptodesc;
                return RsptaCrearXML;                     
               
            }
            catch (Exception ex)
            {
                if(File.Exists(rutadoc.Trim() + nombre.Trim() + ".XML")) {
                File.Delete(rutadoc.Trim() + nombre.Trim() + ".XML");
                }
                
                RsptaCrearXML.FlagVerificacion = false;
                RsptaCrearXML.DescRespuesta = ex.Message;
                return RsptaCrearXML;
            }
        }
        private static XmlWriter ObtenerExtensionesGuia(XmlWriter writer, string prefixExt, string cadExt)
        {
            // DESCRIPCION: Obtener extension de guia
            try
            {
                writer.WriteStartElement(prefixExt, "UBLExtensions", cadExt);
                writer.WriteStartElement(prefixExt, "UBLExtension", cadExt);
                writer.WriteStartElement(prefixExt, "ExtensionContent", cadExt);
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                return writer;
                
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
        }
        private static  XmlWriter ObtenerDatosCabeceraGuia(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_Guia oGuia, EN_Empresa oEmpresa, EN_ComprobanteSunat oCompSunat)
        {
             //DESCRIPCION: Obtener datos de cabecera de Guia
            List<EN_DocPersona> listaDocEmpresa = new List<EN_DocPersona>();
            NE_DocPersona objDocEmpresa = new NE_DocPersona();
            EN_DocPersona oDocEmpresa = new EN_DocPersona();

            try
            {
           // Listamos el codigo del tipo de documento del Remitente
           if (oEmpresa.Tipodocumento != "") {
               oDocEmpresa.Codigo = oEmpresa.Tipodocumento.Trim();
           }
            
            oDocEmpresa.Estado = string.Empty;
            oDocEmpresa.Indpersona = string.Empty;
            listaDocEmpresa = (List<EN_DocPersona>)objDocEmpresa.listarDocPersona(oDocEmpresa, oGuia.id,oGuia.empresa_id);

            writer.WriteElementString("UBLVersionID", cadCbc, "2.1");
            writer.WriteElementString("CustomizationID", cadCbc, "1.0");

            // Datos de la guia
            writer.WriteElementString("ID", cadCbc, oGuia.serie.Trim() + "-" + oGuia.numero.Trim());
            writer.WriteElementString("IssueDate", cadCbc, Convert.ToDateTime(oGuia.fechaemision).ToString("yyyy-MM-dd"));
            writer.WriteElementString("IssueTime", cadCbc, Convert.ToDateTime(oGuia.horaemision).ToString("HH:mm:ss"));
            writer.WriteElementString("DespatchAdviceTypeCode", cadCbc, oGuia.tipodocumento_id.Trim());
            if (oGuia.observaciones != null && oGuia.observaciones.Trim() != "")
            {
                writer.WriteStartElement(prefixCbc, "Note", cadCbc);
                writer.WriteCData(oGuia.observaciones.Trim());
                writer.WriteEndElement();
            }

            // Guía de Remisión Dada de Baja
            if ((oGuia.tipodocumentogrbaja != null && oGuia.tipodocumentogrbaja.Trim()!= "") && 
                (oGuia.numerodocumentogrbaja != null && oGuia.numerodocumentogrbaja.Trim() !=  "")) 
            {
                writer.WriteStartElement(prefixCac, "OrderReference", cadCac);
                writer.WriteElementString("ID", cadCbc, oGuia.numerodocumentogrbaja.Trim());
                writer.WriteStartElement(prefixCbc, "OrderTypeCode", cadCbc);
                if (oGuia.nombredocumentogrbaja.Trim() != "") {
                  writer.WriteAttributeString("name", oGuia.nombredocumentogrbaja);
                }
                 
                writer.WriteValue(oGuia.tipodocumentogrbaja.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();

            }
          

            // Documento Relacionado DAM
            if (oGuia.numeracion_dam != null && oGuia.numeracion_dam.Trim() != "")
            {
                writer.WriteStartElement(prefixCac, "AdditionalDocumentReference", cadCac);
                writer.WriteElementString("ID", cadCbc, oGuia.numeracion_dam.Trim());
                writer.WriteElementString("DocumentTypeCode", cadCbc, "01");
                writer.WriteEndElement();
            }

            // Documento Relacionado Nro de Manifiesto
            if (oGuia.numero_manifiesto != null && oGuia.numero_manifiesto.Trim() != "")
            {
                writer.WriteStartElement(prefixCac, "AdditionalDocumentReference", cadCac);
                writer.WriteElementString("ID", cadCbc, oGuia.numero_manifiesto.Trim());
                writer.WriteElementString("DocumentTypeCode", cadCbc, "04");
                writer.WriteEndElement();
            }

            // Documento Relacionado Otros
            if ((oGuia.tipodocumentorelac != null && oGuia.tipodocumentorelac.Trim() != "") && (oGuia.numerodocumentorelc != null && oGuia.numerodocumentorelc.Trim() != ""))
            {
                writer.WriteStartElement(prefixCac, "AdditionalDocumentReference", cadCac);
                writer.WriteElementString("ID", cadCbc, oGuia.tipodocumentorelac.Trim());
                writer.WriteElementString("DocumentTypeCode", cadCbc, oGuia.numerodocumentorelc.Trim());
                writer.WriteEndElement();
            }

            // Datos de la Firma Digital
            writer.WriteStartElement(prefixCac, "Signature", cadCac);
            writer.WriteElementString("ID", cadCbc, oEmpresa.SignatureID.Trim());
            writer.WriteStartElement(prefixCac, "SignatoryParty", cadCac);
            writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
            if (oEmpresa.Nrodocumento != "") 
            writer.WriteElementString("ID", cadCbc, oEmpresa.Nrodocumento.Trim());

            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "PartyName", cadCac);
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            if (oEmpresa.RazonSocial != "")
            writer.WriteCData(oEmpresa.RazonSocial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "DigitalSignatureAttachment", cadCac);
            writer.WriteStartElement(prefixCac, "ExternalReference", cadCac);
            if (oEmpresa.SignatureURI != "")
            writer.WriteElementString("URI", cadCbc, oEmpresa.SignatureURI.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();

            // Datos del Remitente
            writer.WriteStartElement(prefixCac, "DespatchSupplierParty", cadCac);
            writer.WriteStartElement(prefixCbc, "CustomerAssignedAccountID", cadCbc);
            if (listaDocEmpresa[0].CodigoSunat != "")
            writer.WriteAttributeString("schemeID", listaDocEmpresa[0].CodigoSunat.Trim());
            if (oEmpresa.Nrodocumento != "")
            writer.WriteValue(oEmpresa.Nrodocumento.Trim());
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "Party", cadCac);
            writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
            writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
            if(oEmpresa.RazonSocial != "")
            writer.WriteCData(oEmpresa.RazonSocial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();

            // Datos del Destinatario
            writer.WriteStartElement(prefixCac, "DeliveryCustomerParty", cadCac);
            writer.WriteStartElement(prefixCbc, "CustomerAssignedAccountID", cadCbc);
            if(oGuia.destinatario_tipodoc != "")
            writer.WriteAttributeString("schemeID", oGuia.destinatario_tipodoc.Trim());
            if(oGuia.destinatario_nrodoc != null && oGuia.destinatario_nrodoc.Trim() != "")
            {
                writer.WriteValue(oGuia.destinatario_nrodoc.Trim());
            }
            
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "Party", cadCac);
            writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
            writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
            if(oGuia.destinatario_nombre != "")
            writer.WriteCData(oGuia.destinatario_nombre.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();

            // Datos del establecimiento del tercero
            if (oGuia.tercero_tipodoc != null && oGuia.tercero_tipodoc.Trim() != "")
            {
                writer.WriteStartElement(prefixCac, "SellerSupplierParty", cadCac);
                writer.WriteStartElement(prefixCbc, "CustomerAssignedAccountID", cadCbc);
                writer.WriteAttributeString("schemeID", oGuia.tercero_tipodoc.Trim());
                writer.WriteValue(oGuia.tercero_nrodoc.Trim());
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "Party", cadCac);
                writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
                writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
                if(oGuia.tercero_nombre != null && oGuia.tercero_nombre.Trim() != "")
                writer.WriteCData(oGuia.tercero_nombre);
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            writer.WriteStartElement(prefixCac, "Shipment", cadCac);
            // Valor numerico -- Verificar si es variable
            writer.WriteElementString("ID", cadCbc, "1");
            // Motivo de Traslado
            writer.WriteElementString("HandlingCode", cadCbc, oGuia.codmotivo.Trim());
            if (oGuia.descripcionmotivo != null && oGuia.descripcionmotivo.Trim() != "")
            {
                writer.WriteStartElement(prefixCbc, "Information", cadCbc);
                writer.WriteCData(oGuia.descripcionmotivo);
                writer.WriteEndElement();
            }

            // Unidad de medida de peso
            writer.WriteStartElement(prefixCbc, "GrossWeightMeasure", cadCbc);
            if(oGuia.unidadpeso != "")
            writer.WriteAttributeString("unitCode", oGuia.unidadpeso.Trim());
            writer.WriteValue(oGuia.pesobrutototal);
            writer.WriteEndElement();
            // Numero de Bultos
            if (oGuia.nrobultos > 0)
                writer.WriteElementString("TotalTransportHandlingUnitQuantity", cadCbc, oGuia.nrobultos.ToString());
            // Indicador
            if (oGuia.indicadortransbordo.Trim() != ""){
                if (oGuia.indicadortransbordo.Trim() == "0")
                writer.WriteElementString("SplitConsignmentIndicator", cadCbc, "true");
            else
                writer.WriteElementString("SplitConsignmentIndicator", cadCbc, "false");

            }
  

            // Modalidad y Fecha de Traslado
            writer.WriteStartElement(prefixCac, "ShipmentStage", cadCac);
            // Valor numerico -- Verificar si es variable
            writer.WriteElementString("ID", cadCbc, "1");

            writer.WriteElementString("TransportModeCode", cadCbc, oGuia.modalidadtraslado.Trim());

            writer.WriteStartElement(prefixCac, "TransitPeriod", cadCac);
            if(oGuia.fechatraslado != "")
            writer.WriteElementString("StartDate", cadCbc, Convert.ToDateTime(oGuia.fechatraslado).ToString("yyyy-MM-dd"));
            writer.WriteEndElement();

            // Transporte Publico
            if (oGuia.modalidadtraslado.Trim() == oCompSunat.IdTrPub.Trim())
            {
                writer.WriteStartElement(prefixCac, "CarrierParty", cadCac);
                writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                if(oGuia.trpublic_tipodoc != "")
                writer.WriteAttributeString("schemeID", oGuia.trpublic_tipodoc.Trim());
                if(oGuia.trpublic_nrodoc != "")
                writer.WriteValue(oGuia.trpublic_nrodoc.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "PartyName", cadCac);
                writer.WriteStartElement(prefixCbc, "Name", cadCbc);
                if(oGuia.trpublic_nombre != "")
                writer.WriteCData(oGuia.trpublic_nombre.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

            // Transporte Privado
            if (oGuia.modalidadtraslado.Trim() == oCompSunat.IdTrPriv.Trim())
            {
                writer.WriteStartElement(prefixCac, "TransportMeans", cadCac);
                writer.WriteStartElement(prefixCac, "RoadTransport", cadCac);
                if(oGuia.trprivad_placa != "")
                writer.WriteElementString("LicensePlateID", cadCbc, oGuia.trprivad_placa.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "DriverPerson", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                if(oGuia.trprivad_tipodocconductor != "")
                writer.WriteAttributeString("schemeID", oGuia.trprivad_tipodocconductor.Trim());
                if(oGuia.trprivad_nrodocconductor != "")
                writer.WriteValue(oGuia.trprivad_nrodocconductor.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCac, "Delivery", cadCac);

            // Valor numerico -- Verificar si es variable
            writer.WriteElementString("ID", cadCbc, "1");
           if(oGuia.ubigeoptollegada != null && oGuia.ubigeoptollegada.Trim() != "") {
            writer.WriteStartElement(prefixCac, "DeliveryAddress", cadCac);           
            writer.WriteElementString("ID", cadCbc, oGuia.ubigeoptollegada.Trim());
           }
           if(oGuia.direccionptollegada != null && oGuia.direccionptollegada.Trim() != "") {
            writer.WriteStartElement(prefixCbc, "StreetName", cadCbc);            
            writer.WriteCData(oGuia.direccionptollegada.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
           }
            // End If

            // Contenedor -- Solo para motivo: Importacion
            if (oGuia.codmotivo.Trim() == "08")
            {
                if (oGuia.nrocontenedorimport != null && oGuia.nrocontenedorimport.Trim() != "")
                {
                    writer.WriteStartElement(prefixCac, "TransportHandlingUnit", cadCac);
                    // Valor numerico -- Verificar si es variable
                    writer.WriteElementString("ID", cadCbc, "1");
                    writer.WriteStartElement(prefixCac, "TransportEquipment", cadCac);
                    writer.WriteElementString("ID", cadCbc, oGuia.nrocontenedorimport.Trim());
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
            }

            // Partida
            if (oGuia.ubigeoptopartida != null && oGuia.ubigeoptopartida.Trim() != "") {
            writer.WriteStartElement(prefixCac, "OriginAddress", cadCac);
            writer.WriteElementString("ID", cadCbc, oGuia.ubigeoptopartida.Trim());
            writer.WriteStartElement(prefixCbc, "StreetName", cadCbc);
            writer.WriteCData(oGuia.direccionptopartida.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            }
            // End If

            // Puerto/Aeropuerto Desembarque
            if (oGuia.codpuertodesembarq != null && oGuia.codpuertodesembarq.Trim() != "")
            {
                writer.WriteStartElement(prefixCac, "FirstArrivalPortLocation", cadCac);
                writer.WriteElementString("ID", cadCbc, oGuia.codpuertodesembarq.Trim());
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
            return writer;
                
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
           
        }
        private static XmlWriter ObtenerDatosDetalleGuia(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_DetalleGuia listaDetalle, EN_ComprobanteSunat oCompSunat)
        {
            //DESCRIPCION: Obtener datos de detalle de guia
            try
            {
                writer.WriteStartElement(prefixCac, "DespatchLine", cadCac);
                writer.WriteElementString("ID", cadCbc, listaDetalle.line_id.ToString());
                writer.WriteStartElement(prefixCbc, "DeliveredQuantity", cadCbc);
                writer.WriteAttributeString("unitCode", listaDetalle.unidad.Trim());
                writer.WriteValue(listaDetalle.cantidad);
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "OrderLineReference", cadCac);
                writer.WriteElementString("LineID", cadCbc, listaDetalle.line_id.ToString());
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "Item", cadCac);
                writer.WriteStartElement(prefixCbc, "Name", cadCbc);
                writer.WriteCData(listaDetalle.descripcion.Replace(Constants.vbCrLf, " ").Trim());
                writer.WriteEndElement();
                if (listaDetalle.codigo.Trim() != "")
                {
                    writer.WriteStartElement(prefixCac, "SellersItemIdentification", cadCac);
                    writer.WriteElementString("ID", cadCbc, listaDetalle.codigo.Trim());
                    writer.WriteEndElement();
                }
                // Cambiar por el codigo de producto SUNAT
                if (listaDetalle.codigo.Trim()!= "")
                {
                    writer.WriteStartElement(prefixCac, "CommodityClassification", cadCac);
                    writer.WriteElementString("ItemClassificationCode", cadCbc, listaDetalle.codigo.Trim());
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
                writer.WriteEndElement();
                return writer;
                
            }
            catch (Exception ex)
            {               
                throw ex;
            }
            
        }
        private EN_RespuestaRegistro AgregarFirma(EN_Certificado oCertificado, string rutadoc, string rutaemp, string Nombre, ref string valorResumen, ref string firma, ref string cadenaXML)
        {
            //DESCRIPCION: Agregar firma
            string local_xmlArchivo = rutadoc + Nombre + ".XML"; //Archivo xml local
            string local_nombreXML = System.IO.Path.GetFileName(local_xmlArchivo); //Nombre de archivo
            string local_xpath = string.Empty;  //Path
            string subjectName = string.Empty;  //Variable certificate
            EN_RespuestaRegistro RsptaAgregarFirma = new EN_RespuestaRegistro();
            try
            {
                if (!File.Exists(rutaemp + oCertificado.Nombre)) {

                    
                    RsptaAgregarFirma.FlagVerificacion = false;
                    RsptaAgregarFirma.DescRespuesta = EN_Constante.g_const_firma;
                    return RsptaAgregarFirma;
                }
                
                X509Certificate2 MiCertificado = new X509Certificate2(rutaemp + oCertificado.Nombre, oCertificado.ClaveCertificado);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.Load(local_xmlArchivo);

                SignedXml signedXml = new SignedXml(xmlDoc);
                signedXml.SigningKey = MiCertificado.PrivateKey;

                KeyInfo KeyInfo = new KeyInfo();

                Reference Reference = new Reference();
                Reference.Uri = "";

                Reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());

                signedXml.AddReference(Reference);

                X509Chain X509Chain = new X509Chain();
                X509Chain.Build(MiCertificado);

                X509ChainElement local_element = X509Chain.ChainElements[0];
                KeyInfoX509Data x509Data = new KeyInfoX509Data(local_element.Certificate);
                 subjectName = local_element.Certificate.Subject;

                x509Data.AddSubjectName(subjectName);
                KeyInfo.AddClause(x509Data);

                signedXml.KeyInfo = KeyInfo;
                signedXml.ComputeSignature();

                XmlElement signature = signedXml.GetXml();
                signature.Prefix = "ds";
                signedXml.ComputeSignature();

                foreach (XmlNode node in signature.SelectNodes("descendant-or-self::*[namespace-uri()='http://www.w3.org/2000/09/xmldsig#']"))
                {
                    if (node.LocalName == "Signature")
                    {
                        XmlAttribute newAttribute = xmlDoc.CreateAttribute("Id");
                        newAttribute.Value = oCertificado.IdSignature;
                        node.Attributes.Append(newAttribute);
                        break;
                    }
                }

                
                XmlNamespaceManager nsMgr;
                nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);

                nsMgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:DespatchAdvice-2");
                
                local_xpath = "/tns:DespatchAdvice/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent";

                nsMgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
                nsMgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
                nsMgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
                nsMgr.AddNamespace("xsd", "http://www.w3.org/2001/XMLSchema");
                nsMgr.AddNamespace("ccts", "urn:un:unece:uncefact:documentation:2");

                nsMgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");

                xmlDoc.SelectSingleNode(local_xpath, nsMgr).AppendChild(xmlDoc.ImportNode(signature, true));
                valorResumen = signature.ChildNodes.Item(0).ChildNodes.Item(2).ChildNodes.Item(2).FirstChild.Value;
                firma = signature.ChildNodes.Item(1).FirstChild.Value;
                xmlDoc.Save(local_xmlArchivo);

                XmlNodeList nodeList = xmlDoc.GetElementsByTagName("ds:Signature");

                // el nodo <ds:Signature> debe existir unicamente 1 vez
                if (nodeList.Count != 1)
                    throw new Exception("Se produjo un error en la firma del documento");
                signedXml.LoadXml((XmlElement)nodeList[0]);

                // verificacion de la firma generada
                if (signedXml.CheckSignature() == false)
                    throw new Exception("Se produjo un error en la firma del documento");
                cadenaXML = xmlDoc.OuterXml.ToString().Trim();
                RsptaAgregarFirma.FlagVerificacion = true;
                RsptaAgregarFirma.DescRespuesta = EN_Constante.g_const_psfirma;
                return RsptaAgregarFirma;
            }
            catch (Exception ex)
            {
                RsptaAgregarFirma.FlagVerificacion = false;
                RsptaAgregarFirma.DescRespuesta = ex.Message;
                return RsptaAgregarFirma;
            }
        }
        public EN_ActualizarGuia GuardarGuiaNombreValorResumenFirma(EN_Guia oGuia, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            //DESCRIPCION: Guarda guia nombre valor firma
            NE_Guia objGuia = new NE_Guia();
            EN_ActualizarGuia ResActualizarGuia = new EN_ActualizarGuia();
            try
            {            
               ResActualizarGuia = objGuia.GuardarGuiaNombreValorResumenFirma(oGuia, nombreXML, valorResumen, valorFirma, xmlgenerado);       
               return ResActualizarGuia; 
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            
        }
        public static bool ValidarUsuarioEmpresa(int p_idempresa, string p_usuario, string p_clave, string documentoId, int Idpuntoventa)
        {
        //DESCRIPCION: Validar usuario empresa
        NE_Certificado objCert = new NE_Certificado();       
            try
            {
                if (objCert.validarEmpresaCertificado(p_idempresa, p_usuario, p_clave,  documentoId,  Idpuntoventa) == true)
                 return true;
                 else
                 return false;
                
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
         }
         public EN_RegistrarGuiaElectronica validarEnvioGuiaSunat(EN_Guia oGuia, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string produccion,string ruta, string nombre, ref string msjWA, ref int codRspta, ref string estado_comprobante) 
         {
             //DESCRIPCION: ENVIAR GUIA A SUNAT
             EN_RegistrarGuiaElectronica respuesta = new EN_RegistrarGuiaElectronica();
             EN_RequestEnvioGuia envioGuia = new EN_RequestEnvioGuia();
             EN_RegistrarGuiaElectronica senEnvioGuia = new EN_RegistrarGuiaElectronica();
             Configuracion objConf =new Configuracion();
             NE_Guia clnGuia = new NE_Guia();
              string rutaXml = string.Empty; // RUTA XML
              string rutaCdr = string.Empty; // RUTA CDR
              string urlServicio = string.Empty;
              string urllocalhost = string.Empty;
              string urlWA = string.Empty;
              string dataSerializado = string.Empty; //data serializado
             try
             {
                 if (oGuia.id == null || oGuia.id.Trim() == "") 
                 {
                     respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                     respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "ID de Guia no encontrado";
                     return respuesta;
                 }
                 if (oGuia.empresa_id <= EN_Constante.g_const_0) 
                 {
                    respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                     respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "ID de Empresa no encontrado";
                     return respuesta;
                 }
                 if (oGuia.tipodocumento_id == null || oGuia.tipodocumento_id.Trim() == "" ) 
                 {
                     respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                     respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "Tipo de Documento de Guia no encontrado";
                     return respuesta;

                 }
                 if (oCertificado.flagOSE == null || oCertificado.flagOSE.Trim() == "") 
                 {
                     respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                     respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "Flag OSE no encontrado";
                     return respuesta;
                 }
                 if (oCertificado.UserName == null || oCertificado.UserName.Trim() == "") 
                 {
                     respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                     respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "Usuario de Ceritificado no encontrado";
                     return respuesta;

                 }
                 if (oCertificado.Clave == null || oCertificado.Clave.Trim() == "") 
                 {
                    respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                    respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "Usuario de Ceritificado no encontrado";
                    return respuesta;
                 }
                 if (ruta == null || ruta.Trim() == "") 
                 {
                    respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                    respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "Ruta de Archivo XML no encontrado";
                    return respuesta;
                 }
                 if (nombre == null || nombre.Trim() == "") 
                 {
                    respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                    respuesta.RespRegistrarGuiaElectronica.DescRespuesta = "NOmbre de Archivo XML no encontrado";
                    return respuesta;
                 }
                 envioGuia.flagOse = oCertificado.flagOSE;
                 envioGuia.certUserName = oCertificado.UserName;
                 envioGuia.certClave = oCertificado.Clave;
                 envioGuia.Id = oGuia.id;
                 envioGuia.empresaId = oGuia.empresa_id;
                 envioGuia.tipodocumentoId = oGuia.tipodocumento_id;
                 envioGuia.ruc = oGuia.numerodocempresa;
                 envioGuia.nombreArchivo = nombre;
                 envioGuia.Produccion = produccion;
                 rutaXml=ruta+ EN_Constante.g_const_rutaSufijo_xml;     
                 rutaCdr=ruta+ EN_Constante.g_const_rutaSufijo_cdr;   
                 respuesta.RespRegistrarGuiaElectronica = clnGuia.buscarParametroAppSettings(EN_Constante.g_const_1,EN_Constante.g_const_152);
                 if (respuesta.RespRegistrarGuiaElectronica.FlagVerificacion) 
                 {
                     urllocalhost = respuesta.RespRegistrarGuiaElectronica.DescRespuesta;
   
                     respuesta.RespRegistrarGuiaElectronica = clnGuia.buscarParametroAppSettings(EN_Constante.g_const_1,EN_Constante.g_const_153);
                     if (respuesta.RespRegistrarGuiaElectronica.FlagVerificacion) 
                     {
                         urlServicio = respuesta.RespRegistrarGuiaElectronica.DescRespuesta;
                         urlWA = urllocalhost + urlServicio;        
                         dataSerializado = System.Text.Json.JsonSerializer.Serialize(envioGuia);
                         senEnvioGuia = objConf.sendWebApiGuia(urlWA, dataSerializado);
                         respuesta.RespRegistrarGuiaElectronica = senEnvioGuia.RespRegistrarGuiaElectronica;
                         respuesta.ErrorWebServ = senEnvioGuia.ErrorWebServ;
                           if(senEnvioGuia.RespRegistrarGuiaElectronica.FlagVerificacion){
                                msjWA= senEnvioGuia.RespRegistrarGuiaElectronica.DescRespuesta;
                                codRspta = EN_Constante.g_const_0;
                                respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = true;
                                respuesta.RespRegistrarGuiaElectronica.DescRespuesta = msjWA;

                            }else{
                                msjWA= senEnvioGuia.RespRegistrarGuiaElectronica.DescRespuesta;
                                codRspta = EN_Constante.g_const_1;
                                respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                                respuesta.RespRegistrarGuiaElectronica.DescRespuesta = msjWA;
                            }  


                            estado_comprobante= senEnvioGuia.RespRegistrarGuiaElectronica.estadoComprobante;                     
                     }                   
                 }
                  return   respuesta;                 
             }
             catch (Exception ex)
             {                 
                respuesta.RespRegistrarGuiaElectronica.FlagVerificacion = false;
                respuesta.RespRegistrarGuiaElectronica.DescRespuesta = EN_Constante.g_const_error_interno;
                respuesta.ErrorWebServ.TipoError = EN_Constante.g_const_1;
                respuesta.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
                respuesta.ErrorWebServ.DescripcionErr = ex.Message;
                return respuesta;
             }

         }
        
    }
}