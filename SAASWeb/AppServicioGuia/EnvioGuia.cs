/****************************************************************************************************************************************************************************************
 PROGRAMA: EnvioGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase de generar guia electronica
 FECHA   : 24/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/
using CEN;
using CLN;
using System;
using System.Collections.Generic;
using System.Linq;
using AppConfiguracion;
using Microsoft.VisualBasic;
namespace AppServicioGuia
{
    public class EnvioGuia
    {
        public EN_RespuestaCrearGuia CrearGuiaElectronica(GuiaSunat oGuia, ref int codRspta, ref string msjeRspta, ref string ruc_emisor, ref string estado_comprobante)
        {
        //DESCRIPCION: Crear Guia Electrónica
        
            EN_Empresa empresaDocumento = new EN_Empresa();
            EN_EliminarGuia RespEliminarGuia = new EN_EliminarGuia(); 
            EN_Certificado certificadoDocumento = new EN_Certificado();
            EN_ActualizarGuia ResActualizarGuia = new EN_ActualizarGuia();
            NE_Guia obj = new NE_Guia();
            EN_Concepto concepto = new EN_Concepto();
            EN_Guia objGuia = new EN_Guia();
            EN_RespuestaCrearGuia  repuestCrearGuia = new EN_RespuestaCrearGuia();
            EN_RegistrarGuiaElectronica RsptaRegistrarGuia = new EN_RegistrarGuiaElectronica();
            List<EN_Guia> listaGuias = new List<EN_Guia>();
            EN_RespuestaListaGuia  RespListarGuia = new EN_RespuestaListaGuia();
            GenerarXMLGuia GenerarXMLGuia = new GenerarXMLGuia(); 
            EN_ErrorWebService errWebServ = new EN_ErrorWebService();
            string rutaTemp = string.Empty; //RUTA TEMPORAL
            string rutaroot = string.Empty; //ruta 
            string  rutabase = string.Empty; // ruta base
            string rutadoc = string.Empty; // ruta de documento
            string nombreArchivoXml = string.Empty; //nombre de archivo xml
            string valorResumen = string.Empty; // valor resumen
            string firma = string.Empty; // firma
            string cadenaXML = string.Empty; // cadena de XML
            string mensaje = string.Empty; // mensaje
            EN_RegistrarGuiaElectronica respuEnvioGuia = new EN_RegistrarGuiaElectronica();

    
            try
            {
                if (GenerarXMLGuia.ValidarUsuarioEmpresa(oGuia.empresa_id, oGuia.Usuario, oGuia.clave, oGuia.id, oGuia.empresa_id))
                {
                    empresaDocumento = GetEmpresaById(oGuia.empresa_id, oGuia.puntoemision, oGuia.id);
                    certificadoDocumento = GetCertificadoByEmpresaId(empresaDocumento.Id, oGuia.id,oGuia.puntoemision);

                     rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;

                    if (rutabase == null || rutabase.Trim() == "") {
                        repuestCrearGuia.RespGenerarGuia.FlagVerificacion = false;
                        repuestCrearGuia.RespGenerarGuia.DescRespuesta = "";
                        return repuestCrearGuia;
                    } 

                     ruc_emisor=  empresaDocumento.Nrodocumento;
                
                    rutadoc = string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                    //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
                    AppConfiguracion.FuncionesS3.InitialS3(empresaDocumento.Nrodocumento,certificadoDocumento.Nombre,empresaDocumento.Imagen);


                    EN_ComprobanteSunat oCompSunat = buildComprobante();

                    if (oGuia.tipodocumento_id.Trim() == oCompSunat.IdGR)
                    {
                        RsptaRegistrarGuia = GuardarGuiaSunat(oGuia);
                        repuestCrearGuia.RespGenerarGuia =  RsptaRegistrarGuia.RespRegistrarGuiaElectronica;
                        repuestCrearGuia.ErrorWebServ = RsptaRegistrarGuia.ErrorWebServ;
                        if (repuestCrearGuia.RespGenerarGuia.FlagVerificacion)
                        {
                            
                            objGuia.id = oGuia.id;
                            objGuia.empresa_id = oGuia.empresa_id;
                            objGuia.tipodocumento_id = oGuia.tipodocumento_id;
                            objGuia.estado = EN_Constante.g_const_estado_n;
                            objGuia.situacion = EN_Constante.g_const_situacion_p;
                            objGuia.serie = oGuia.serie;
                            objGuia.numero = oGuia.numero;
                            objGuia.CadenaAleatoria = EN_Constante.g_const_vacio;
                            objGuia.Usuariosession = EN_Constante.g_const_vacio;
                            RespListarGuia = obj.listarGuia(objGuia);
                            repuestCrearGuia.RespGenerarGuia =  RespListarGuia.ResplistaGuia;
                        if (RespListarGuia.ResplistaGuia.FlagVerificacion)
                        {
                            repuestCrearGuia.RespGenerarGuia = GenerarXMLGuia.GenerarXmlGuia(RespListarGuia.listDetGuia[0], oCompSunat, empresaDocumento, certificadoDocumento, rutabase, rutadoc, ref nombreArchivoXml, ref valorResumen, ref firma, ref mensaje, ref cadenaXML);     
                            if (repuestCrearGuia.RespGenerarGuia.FlagVerificacion)
                            {
                                ResActualizarGuia = GenerarXMLGuia.GuardarGuiaNombreValorResumenFirma(RespListarGuia.listDetGuia[0], nombreArchivoXml, valorResumen, firma, cadenaXML);
                                if (ResActualizarGuia.RespActualizarGuia.FlagVerificacion)
                                {
                                    oGuia.nombreFichero = nombreArchivoXml;
                                    oGuia.valorResumen = valorResumen;
                                    oGuia.valorFirma = firma;
                                    codRspta = 0;
                                    msjeRspta = mensaje;
                                    // Generamos el documento en PDF
                                    repuestCrearGuia.RespGenerarGuia = GeneraPdfGuiaRemision(RespListarGuia.listDetGuia[0], oCompSunat, empresaDocumento, valorResumen, firma, rutabase, rutadoc);
                                    if (repuestCrearGuia.RespGenerarGuia.FlagVerificacion) {
                        
                                    
                                        if (empresaDocumento.enviarxml == EN_Constante.g_const_si) 
                                        {
                                            string mensajeWA=EN_Constante.g_const_vacio;
                                            respuEnvioGuia = GenerarXMLGuia.validarEnvioGuiaSunat(RespListarGuia.listDetGuia[0], certificadoDocumento, oCompSunat, empresaDocumento.Produccion,  rutadoc, nombreArchivoXml, ref mensajeWA, ref codRspta, ref estado_comprobante);
                                            repuestCrearGuia.RespGenerarGuia = respuEnvioGuia.RespRegistrarGuiaElectronica;
                                            repuestCrearGuia.ErrorWebServ = respuEnvioGuia.ErrorWebServ;
                                            if (repuestCrearGuia.RespGenerarGuia.FlagVerificacion) 
                                            {
                                                concepto = obj.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_1021);
                                                repuestCrearGuia.RespGenerarGuia.FlagVerificacion = true;
                                                repuestCrearGuia.RespGenerarGuia.DescRespuesta = concepto.conceptodesc;
                                                concepto = obj.listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_2002);
                                                errWebServ.DescripcionErr = concepto.conceptodesc;
                                                errWebServ.CodigoError = EN_Constante.g_const_2000;
                                                errWebServ.TipoError = EN_Constante.g_const_0;
                                                repuestCrearGuia.ErrorWebServ = errWebServ;

                                            }
                                        } else {
                                            codRspta = 1;
                                            repuestCrearGuia.RespGenerarGuia.FlagVerificacion = false;
                                            repuestCrearGuia.RespGenerarGuia.DescRespuesta = "DOCUMENTO GUIA CREADO CORRECTAMENTE";
                                            repuestCrearGuia.RespGenerarGuia.mensajeRespuesta = "Comprobante pendiente de envío a la SUNAT.";
                                        repuestCrearGuia.RespGenerarGuia.comprobante = RespListarGuia.listDetGuia[0].serie + "-"+RespListarGuia.listDetGuia[0].numero;

                                        }                                                       

                                    }
                                    else {
                                        repuestCrearGuia.ErrorWebServ.DescripcionErr = repuestCrearGuia.RespGenerarGuia.DescRespuesta;
                                        repuestCrearGuia.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
                                        repuestCrearGuia.ErrorWebServ.TipoError = EN_Constante.g_const_1;
                                        repuestCrearGuia.RespGenerarGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                                    }
                                } 
                                else 
                                {
                                repuestCrearGuia.RespGenerarGuia = ResActualizarGuia.RespActualizarGuia;  
                                }
                            }
                            else
                            {

                                errWebServ.DescripcionErr = repuestCrearGuia.RespGenerarGuia.DescRespuesta;
                                errWebServ.CodigoError = EN_Constante.g_const_3000;
                                errWebServ.TipoError = EN_Constante.g_const_1;
                                repuestCrearGuia.ErrorWebServ = errWebServ;
                                RespListarGuia.ResplistaGuia.FlagVerificacion = false;
                                repuestCrearGuia.RespGenerarGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                                oGuia.nombreFichero = "";
                                oGuia.valorResumen = "";
                                oGuia.valorFirma = "";
                                codRspta = 1; // Se transfirio el documento pero no generó XML
                                msjeRspta = mensaje;
                                // Se elimina el documento de la base de datos
                                RespEliminarGuia = obj.eliminarGuia(objGuia);

                            }
                        } 
                        else 
                        {
                            repuestCrearGuia.ErrorWebServ.DescripcionErr = RespListarGuia.ResplistaGuia.DescRespuesta;
                            repuestCrearGuia.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
                            repuestCrearGuia.ErrorWebServ.TipoError = EN_Constante.g_const_1;
                            RespListarGuia.ResplistaGuia.FlagVerificacion = false;
                            RespListarGuia.ResplistaGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                            repuestCrearGuia.RespGenerarGuia = RespListarGuia.ResplistaGuia;
                            

                        }
                    }
                }
                else
                    {
                        oGuia.nombreFichero = "";
                        oGuia.valorResumen = "";
                        oGuia.valorFirma = "";
                        codRspta = 1;
                        msjeRspta = "El tipo de comprobante a generar no es válido.";
                    }
                }
                else
                {
                    oGuia.nombreFichero = "";
                    oGuia.valorResumen = "";
                    oGuia.valorFirma = "";
                    codRspta = 1;
                    msjeRspta = "No se ha encontrado el usuario del certificado de la empresa ingresada";
                }
                return repuestCrearGuia;
            }
            catch (Exception ex)
            {

                oGuia.nombreFichero = "";
                oGuia.valorResumen = "";
                oGuia.valorFirma = "";
                codRspta = 1;
                msjeRspta = "Hubo una Excepción al generar la guía: " + ex.Message;
                repuestCrearGuia.RespGenerarGuia.FlagVerificacion = false;
                repuestCrearGuia.RespGenerarGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                repuestCrearGuia.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
                repuestCrearGuia.ErrorWebServ.DescripcionErr = ex.Message;
                return repuestCrearGuia;
            }
        
        
     }
      public EN_Empresa GetEmpresaById(int idEmpresa, int Idpuntoventa, string documentoId)
        {
            //DESCRIPCION: Consultar empresa
            EN_Empresa listaEmpresa = new EN_Empresa();;
            EN_Empresa oEmpresa = new EN_Empresa();
            NE_Empresa objEmpresa = new NE_Empresa();
            oEmpresa.Id = idEmpresa;
            oEmpresa.Nrodocumento = "";
            oEmpresa.RazonSocial = "";
            oEmpresa.Estado = "";
            listaEmpresa = (EN_Empresa)objEmpresa.listaEmpresa(oEmpresa, Idpuntoventa,  documentoId);
            return listaEmpresa;
        }
         public EN_Certificado GetCertificadoByEmpresaId(int empresaId,string documentoId,  int Idpuntoventa)
        {
            EN_Certificado oCertif = new EN_Certificado();
            NE_Certificado objCertif = new NE_Certificado();
            EN_Certificado listaCertificado = new EN_Certificado();

            oCertif.Id = 0;
            oCertif.IdEmpresa = empresaId;
            oCertif.Nombre = "";
            oCertif.UsuarioSession = "";
            oCertif.CadenaAleatoria = "";
            listaCertificado = (EN_Certificado)objCertif.listarCertificado(oCertif, documentoId, Idpuntoventa);
            return listaCertificado;
        }
        private EN_ComprobanteSunat buildComprobante()
        {
            var oCompSunat = new EN_ComprobanteSunat();

            oCompSunat.IdFC = EN_ConfigConstantes.Instance.const_IdFC;
            oCompSunat.IdBV = EN_ConfigConstantes.Instance.const_IdBV;
            oCompSunat.IdNC = EN_ConfigConstantes.Instance.const_IdNC;
            oCompSunat.IdND = EN_ConfigConstantes.Instance.const_IdND;
            oCompSunat.IdGR = EN_ConfigConstantes.Instance.const_IdGR;

            oCompSunat.IdCB =  EN_ConfigConstantes.Instance.const_IdCB;
            oCompSunat.IdRD = EN_ConfigConstantes.Instance.const_IdRD;
            oCompSunat.IdRR =  EN_ConfigConstantes.Instance.const_IdRR;

            oCompSunat.IdCP = EN_ConfigConstantes.Instance.const_IdCP;
            oCompSunat.IdCR =  EN_ConfigConstantes.Instance.const_IdCR;
            oCompSunat.IdTrPriv = EN_ConfigConstantes.Instance.const_IdTrPriv;
            oCompSunat.IdTrPub = EN_ConfigConstantes.Instance.const_IdTrPub;
            

            oCompSunat.codigoEtiquetaErrorDoc = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc;
            oCompSunat.codigoEtiquetaErrorGui = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorGui;
            return oCompSunat;
        }
        public EN_RegistrarGuiaElectronica GuardarGuiaSunat(GuiaSunat GuiaSunat)
         {
          NE_GeneracionService negocioGeneracionService = new NE_GeneracionService();
          EN_RegistrarGuiaElectronica RsptaRegistrarGuia = new EN_RegistrarGuiaElectronica();
          RsptaRegistrarGuia = negocioGeneracionService.GuardarGuiaSunat(GuiaSunat);
          return RsptaRegistrarGuia;
         }
        public EN_RespuestaRegistro GeneraPdfGuiaRemision(EN_Guia oGuia, EN_ComprobanteSunat oCompSunat, EN_Empresa empresaDocumento, string valorResumen, string firma, string rutabase, string rutadoc)
        {
            //DESCRIPCION: Generar PDF de Guia de Remision
         GenerarPDF creaPdf = new GenerarPDF();

         EN_RespuestaListaDetalleGuia RsptaListaDetalleguia = new EN_RespuestaListaDetalleGuia();
         EN_RespuestaRegistro RsptaGenerarPDFGuia  =new EN_RespuestaRegistro();
         EN_RespuestaRegistro RsptaCrearPDFGuia  =new EN_RespuestaRegistro();
         List<EN_DetalleGuia> detalleGuia = new List<EN_DetalleGuia>();
         NE_Guia objProceso = new NE_Guia();   
         EN_Concepto concepto = new EN_Concepto();
         NE_Guia guia = new NE_Guia();    
         string motivotraslado = string.Empty; // motivo de traslado
         EN_RespuestaTablaSunat respParametros = new EN_RespuestaTablaSunat();
         
          try
          {
              RsptaListaDetalleguia = objProceso.listarDetalleGuia(oGuia);
            if(RsptaListaDetalleguia.ResplistaDetalleGuia.FlagVerificacion){
                
          
                NE_Facturacion objMot = new NE_Facturacion();
                List<EN_Tabla> listMot = new List<EN_Tabla>();
                
                EN_Tabla pMot = new EN_Tabla();
                pMot.Codtabla = "020";
                pMot.Codsunat = oGuia.codmotivo;
                pMot.Codelemento = "";
                pMot.Codalterno = "";
                respParametros = objMot.listarConstantesTablaSunat(2,pMot.Codtabla,pMot.Codsunat);
               
               if (respParametros.ResplistaTablaSunat.FlagVerificacion){
                   motivotraslado = respParametros.ResplistaTablaSunat.DescRespuesta;
               }
               

        
                string modalidadtraslado = "";
                NE_Facturacion objMod = new NE_Facturacion();
                List<EN_Tabla> listMod = new List<EN_Tabla>();
                EN_Tabla pMod = new EN_Tabla();
                pMod.Codtabla = "018";
                pMod.Codsunat = oGuia.modalidadtraslado;
                pMod.Codelemento = "";
                pMod.Codalterno = "";
              
                respParametros = objMot.listarConstantesTablaSunat(2,pMod.Codtabla,pMod.Codsunat);
                if (respParametros.ResplistaTablaSunat.FlagVerificacion){
                   modalidadtraslado = respParametros.ResplistaTablaSunat.DescRespuesta;
               }
    
                NE_Ubicacion objUbi = new NE_Ubicacion();
                string destino = "";
                string DLLDepa = "";
                string DLLProv = "";
                string DLLDist = "";
        
                DLLDepa = objUbi.ObtenerDescripcionUbicacion(1,Strings.Left(oGuia.ubigeoptollegada, 2) + "0000");
                DLLProv = objUbi.ObtenerDescripcionUbicacion(2,Strings.Left(oGuia.ubigeoptollegada, 4) + "00");
                DLLDist = objUbi.ObtenerDescripcionUbicacion(3,oGuia.ubigeoptollegada);
                destino = DLLDist + "-" + DLLProv + "-" + DLLDepa; // Departamento - Provincia - Distrito

                string DPADepa = "";
                string DPAProv = "";
                string DPADist = "";
                string origen = "";
                DPADepa = objUbi.ObtenerDescripcionUbicacion(1,Strings.Left(oGuia.ubigeoptopartida, 2) + "0000");
                DPAProv = objUbi.ObtenerDescripcionUbicacion(2,Strings.Left(oGuia.ubigeoptopartida, 4) + "00");
                DPADist = objUbi.ObtenerDescripcionUbicacion(3,oGuia.ubigeoptopartida);
                origen = DPADist + "-" + DPAProv + "-" + DPADepa;  // Departamento - Provincia - Distrito
                 
                 RsptaGenerarPDFGuia = creaPdf.GenerarPDFGuiaRemision(empresaDocumento, oGuia, RsptaListaDetalleguia.listDetGuia, oCompSunat, motivotraslado, modalidadtraslado, origen, destino, valorResumen, firma, rutabase, rutadoc);
                 RsptaCrearPDFGuia = RsptaGenerarPDFGuia;
            }
            else 
            {
                RsptaCrearPDFGuia = RsptaListaDetalleguia.ResplistaDetalleGuia;
            }

             return RsptaCrearPDFGuia; 
          }
          catch (Exception ex)
          {
              RsptaCrearPDFGuia.FlagVerificacion = false;
              RsptaCrearPDFGuia.DescRespuesta = ex.Message;
              return RsptaCrearPDFGuia;
          }
        }
        
    }
}