
/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RequestSyncEstados.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de request syncronizar estados
 FECHA   : 03/03/2022
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System.Collections.Generic;

namespace CEN
{
    public class EN_RequestSyncEstados
    {
        public string tipodocumento_id {get; set;}              // ID TIPO DE DOCUMENTO
        public string serie {get; set;}               // SERIE DEL DOCUMENTO
        public string numero {get; set;}           // NUMERO CORRELATIVO DEL DOCUMENTO
        public bool send_s3 {get; set;}             // PETICIO DE URL DE S3
    
    }




}