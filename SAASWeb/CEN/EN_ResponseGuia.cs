/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_ResponseGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase Respuesta Guia
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_ResponseGuia
    {
        //DESCRIPCION: Clase de Respuesta de guia
        public EN_RespuestaRegistro RespGuiaElectronica { get; set; }  //respuesta de guia
        public EN_ErrorWebService ErrorWebServ { get; set; }   // respuesta de error de wweb service

        public EN_ResponseGuia() {
            RespGuiaElectronica = new EN_RespuestaRegistro();
            ErrorWebServ = new EN_ErrorWebService();
        }
    }
}