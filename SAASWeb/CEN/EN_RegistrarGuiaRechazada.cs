/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RegistrarGuiaRechazada.cs
 VERSION : 1.0
 OBJETIVO: Clase de registrar guia rechazada
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_RegistrarGuiaRechazada
    {
        //DESCRIPCION: Clase de registrar guia rechazada
        public EN_RespuestaRegistro RespRegistrarGuiaRechazada { get; set; }  
        public EN_ErrorWebService ErrorWebServ { get; set; }  

        public EN_RegistrarGuiaRechazada() {
            RespRegistrarGuiaRechazada = new EN_RespuestaRegistro();
            ErrorWebServ = new EN_ErrorWebService();
        }
        
    }
}