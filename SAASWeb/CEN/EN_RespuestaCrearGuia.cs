/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RespuestaCrearGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase respuesta de generar guia
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_RespuestaCrearGuia
    {
         //DESCRIPCION:  CLASE DE RESPUESTA DE CREAR GUIA
        public EN_RespuestaRegistro RespGenerarGuia { get; set; }  
        public EN_ErrorWebService ErrorWebServ { get; set; }  

        public EN_RespuestaCrearGuia() {
            RespGenerarGuia = new EN_RespuestaRegistro();
            ErrorWebServ = new EN_ErrorWebService();
        }
        
    }
}