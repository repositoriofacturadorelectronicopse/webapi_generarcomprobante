/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RespuestaTablaSunat.cs
 VERSION : 1.0
 OBJETIVO: Clase Respuesta Registro
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_RespuestaTablaSunat
    {
         //DESCRIPCION: CLase de respuesta de lista de parametros de tabla sunat
        public EN_RespuestaRegistro ResplistaTablaSunat { get; set; }  
        public EN_ErrorWebService ErrorWebServ { get; set; }  

        public EN_RespuestaTablaSunat() {
            ResplistaTablaSunat = new EN_RespuestaRegistro();
            ErrorWebServ = new EN_ErrorWebService();
        }
    }
}