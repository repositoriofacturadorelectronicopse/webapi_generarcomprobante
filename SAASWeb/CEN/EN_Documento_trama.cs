using System;
using System.Collections.Generic;

namespace CEN
{
    public class EN_Documento_trama
    {

  
    public string Idempresa { get; set; }
    public string Id { get; set; }
    public string Idtipodocumento { get; set; }
    //Idtipodocumentonota diferencia entre factura y boleta para nota de credito yo debito; 
    // 1:NC o ND factura, 3:NC o ND boleta 
    public string Idtipodocumentonota { get; set; } 
    public string NroDocEmpresa { get; set; }
    public string DescripcionEmpresa { get; set; }
    public string Tipodocumento { get; set; }
    public string Serie { get; set; }
    public string Numero { get; set; }
    public string Idpuntoventa { get; set; }
    public string DescripcionPuntoVenta { get; set; }
    public string CodigoEstablecimiento { get; set; }
    public string TipoOperacion { get; set; }
    public string TipoNotaCredNotaDeb { get; set; }
    public string Tipodoccliente { get; set; }
    public string Nrodoccliente { get; set; }
    public string Nombrecliente { get; set; }
    public string Direccioncliente { get; set; }
    public string Emailcliente  { get; set; }
    public string Fecha { get; set; }
    public string Hora { get; set; }
    public string Fechavencimiento { get; set; }
    public string Glosa { get; set; }
    public string Formapago { get; set; }
    public string Moneda { get; set; }
    public string Tipocambio { get; set; }
    public string Descuento { get; set; }
    public string Descuentoafectabase { get; set; }
    public string Igvventa { get; set; }
    public string Iscventa { get; set; }
    public string OtrosTributos { get; set; }
    public string OtrosCargos { get; set; }
    public string Importeventa { get; set; }
    public string Regimenpercep { get; set; }
    public string DescRegimenpercep { get; set; }
    public string Porcentpercep { get; set; }
    public string Importepercep { get; set; }
    public string Porcentdetrac { get; set; }
    public string Importedetrac { get; set; }
    public string Importerefdetrac { get; set; }
    public string Importefinal { get; set; }
    public string Importeanticipo { get; set; }
    public string IndicaAnticipo { get; set; }
    public string Estado { get; set; }
    public string Situacion { get; set; }
    public string Numeroelectronico { get; set; }
    public string Estransgratuita  { get; set; }
    public string EnviadoMail { get; set; }
    public string Envioexterno { get; set; }
    public string Valorpergravadas { get; set; }
    public string Valorperinafectas { get; set; }
    public string Valorperexoneradas { get; set; }
    public string Valorpergratuitas { get; set; }
    public string Valorperiscreferenc { get; set; }
    public string Valorperexportacion { get; set; }
    public string SumaIgvGratuito { get; set; }
    private string _SumaIscGratuito;
    public string SumaIscGratuito  { get; set; }
    public string TotalDsctoItem { get; set; }
    public string TotalCargoItem { get; set; }
    public string TipoGuiaRemision { get; set; }
    public string GuiaRemision { get; set; }
    public string TipoDocOtroDocRef { get; set; }
    public string OtroDocumentoRef { get; set; }
    public string OrdenCompra { get; set; }
    public string PlacaVehiculo { get; set; }
    public string MontoFise { get; set; }
    public string TipoRegimen { get; set; }
    public string CodigoBBSSSujetoDetrac { get; set; }
    public string NumCtaBcoNacion { get; set; }
    public string BienTransfAmazonia { get; set; }
    public string ServiTransfAmazonia { get; set; }
    public string ContratoConstAmazonia { get; set; }
   
    public string desctiponotcreddeb  { get; set; }
    public string descripciondetrac  { get; set; }
  
    // Para Validar Usuario Certificado Empresa
    public string Usuario  { get; set; }
    public string Clave  { get; set; }
    // Para docreferencias
    public string dr_tipodocumento_id  { get; set; }
    public string dr_tipodoc_desc  { get; set; }
    public string dr_serie { get; set; }
    public string dr_numero { get; set; }
    public string dr_fecha { get; set; }
    public string dr_motivo { get; set; }
    public string jsonpagocredito { get; set; }
    public string hasRetencionIgv { get; set; }
    public string porcRetencionIgv {get;set;}
    public string impRetencionIgv {get;set;}
    public string impOperacionRetencionIgv {get;set;}
    
    public string UsuarioSession  { get; set; }
}

public class DocumentoSunatTrama : EN_Documento_trama
{
    public DocumentoSunatTrama()
    {
        ListaDetalleDocumento = new List<EN_DetalleDocumento_trama>();
        ListaDocumentoReferencia = new List<EN_DocReferencia>();
        ListaDocumentoAnticipo = new List<EN_DocAnticipo>();
        ListaDocRefeGuia = new List<EN_DocRefeGuia>();
        FacturaGuia = new EN_FacturaGuia();
        FacturaRemitente = new EN_FacturaRemitente();

        // agregado para formas de pago en credito
        FormaPagoCredito = new List<EN_FormaPagoCredito>();
        // agregado para retencion del igv
        
    }

    public List<EN_DetalleDocumento_trama> ListaDetalleDocumento { get; set; }
    public List<EN_DocReferencia> ListaDocumentoReferencia { get; set; }
    public List<EN_DocAnticipo> ListaDocumentoAnticipo { get; set; }
    public EN_FacturaGuia FacturaGuia { get; set; }
    public EN_FacturaRemitente FacturaRemitente { get; set; }
    public List<EN_DocRefeGuia> ListaDocRefeGuia { get; set; }


    // agregado para forma de pago credito
     public List<EN_FormaPagoCredito> FormaPagoCredito { get; set; }
        

   
   


    }
}