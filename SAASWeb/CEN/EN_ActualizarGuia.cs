/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_ActualizarGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase actualizar guia
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_ActualizarGuia
    {
         //DESCRIPCION:  CLASE DE ACTUALIZAR RESPUESTA
        public EN_RespuestaRegistro RespActualizarGuia { get; set; }  
        public EN_ErrorWebService ErrorWebServ { get; set; }  

        public EN_ActualizarGuia() {
            RespActualizarGuia = new EN_RespuestaRegistro();
            ErrorWebServ = new EN_ErrorWebService();
        }
    }
}