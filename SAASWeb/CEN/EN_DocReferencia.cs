/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DocReferencia.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de documento referencia 
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
public class EN_DocReferencia
{
    
    public int Idempresa { get; set; }                      // ID DE EMPRESA
    public string Iddocumento { get; set; }                 // ID DE DOCUMENTO
    public string Idtipodocumento { get; set; }             // ID TIPO DE DOCUMENTO
    public string Iddocumentoref { get; set;}               // ID DOCUMENTO DE REFERENCIA
    public string Idtipodocumentoref { get; set; }          // ID TIPO DE DOCUMENTO DE REFERENCIA
    public string Numerodocref { get; set; }                // NUMERO DE DOCUMENTO DE REFERENCIA
    public string DescripcionTipoDoc { get; set; }          // DESCRIPCION TIPO DE DOCUMENTO
    // Informacion Auditoria
    public string UsuarioSession { get; set; }              // USUARIO SESION
    public string CadenaAleatoria { get; set; }             // CADENA ALEATORIA
  
}


}