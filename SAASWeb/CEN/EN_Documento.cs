using System;
using System.Collections.Generic;

namespace CEN
{
    public class EN_Documento
    {

  
    public int Idempresa { get; set; }
    public string Id { get; set; }
    public string Idtipodocumento { get; set; }
    //Idtipodocumentonota diferencia entre factura y boleta para nota de credito yo debito; 
    // 1:NC o ND factura, 3:NC o ND boleta 
    public int Idtipodocumentonota { get; set; } 
    public string NroDocEmpresa { get; set; }
    public string DescripcionEmpresa { get; set; }
    public string Tipodocumento { get; set; }
    public string Serie { get; set; }
    public string Numero { get; set; }
    public int Idpuntoventa { get; set; }
    public string DescripcionPuntoVenta { get; set; }
    public string CodigoEstablecimiento { get; set; }
    public string TipoOperacion { get; set; }
    public string TipoNotaCredNotaDeb { get; set; }
    public string Tipodoccliente { get; set; }
    public string Nrodoccliente { get; set; }
    public string Nombrecliente { get; set; }
    public string Direccioncliente { get; set; }
    public string Emailcliente  { get; set; }
    public string Fecha { get; set; }
    public string Hora { get; set; }
    public string Fechavencimiento { get; set; }
    public string Glosa { get; set; }
    public string Formapago { get; set; }
    public string Moneda { get; set; }
    public double Tipocambio { get; set; }
    public double Descuento { get; set; }
    public string Descuentoafectabase { get; set; }
    public double Igvventa { get; set; }
    public double Iscventa { get; set; }
    public double OtrosTributos { get; set; }
    public double OtrosCargos { get; set; }
    public double Importeventa { get; set; }
    public string Regimenpercep { get; set; }
    public string DescRegimenpercep { get; set; }
    public double Porcentpercep { get; set; }
    public double Importepercep { get; set; }
    public double Porcentdetrac { get; set; }
    public double Importedetrac { get; set; }
    public double Importerefdetrac { get; set; }
    public double Importefinal { get; set; }
    public double Importeanticipo { get; set; }
    public string IndicaAnticipo { get; set; }
    public string Estado { get; set; }
    public string Situacion { get; set; }
    public string Numeroelectronico { get; set; }
    public string Estransgratuita  { get; set; }
    public string EnviadoMail { get; set; }
    public string Envioexterno { get; set; }
    public double Valorpergravadas { get; set; }
    public double Valorperinafectas { get; set; }
    public double Valorperexoneradas { get; set; }
    public double Valorpergratuitas { get; set; }
    public double Valorperiscreferenc { get; set; }
    public double Valorperexportacion { get; set; }
    public double SumaIgvGratuito { get; set; }
    private double _SumaIscGratuito;
    public double SumaIscGratuito  { get; set; }
    public double TotalDsctoItem { get; set; }
    public double TotalCargoItem { get; set; }
    public string TipoGuiaRemision { get; set; }
    public string GuiaRemision { get; set; }
    public string TipoDocOtroDocRef { get; set; }
    public string OtroDocumentoRef { get; set; }
    public string OrdenCompra { get; set; }
    public string PlacaVehiculo { get; set; }
    public double MontoFise { get; set; }
    public string TipoRegimen { get; set; }
    public string CodigoBBSSSujetoDetrac { get; set; }
    public string NumCtaBcoNacion { get; set; }
    public string BienTransfAmazonia { get; set; }
    public string ServiTransfAmazonia { get; set; }
    public string ContratoConstAmazonia { get; set; }
    // Parametros para el XML
    public string nombreXML { get; set; }
    public string vResumen { get; set; }
    public string vFirma { get; set; }
    public string xmlEnviado { get; set; }
    public string xmlCDR   { get; set; }

    // Para Adicionales
    public string cod_aux_01  { get; set; }
    public string text_aux_01  { get; set; }
    public string cod_aux_02 { get; set; }
    public string text_aux_02 { get; set; }
    public string cod_aux_03 { get; set; }
    public string text_aux_03 { get; set; }
    public string cod_aux_04 { get; set; }
    public string text_aux_04 { get; set; }
    public string cod_aux_05 { get; set; }
    public string text_aux_05 { get; set; }
    public string cod_aux_06 { get; set; }
    public string text_aux_06 { get; set; }
    public string cod_aux_07 { get; set; }
    public string text_aux_07 { get; set; }
    public string cod_aux_08  { get; set; }
    public string text_aux_08  { get; set; }
    public string cod_aux_09  { get; set; }
    public string text_aux_09  { get; set; }
    public string cod_aux_10  { get; set; }
    public string text_aux_10  { get; set; }
    public string cod_aux_11  { get; set; }
    public string text_aux_11  { get; set; }
    public string desctiponotcreddeb  { get; set; }
    public string descripciondetrac  { get; set; }
    // Para Busquedas
    public string FechaIni  { get; set; }
    public string FechaFin  { get; set; }
    public DateTime FechaCDR  { get; set; }
    // Informacion Auditoria
    public string UsuarioSession  { get; set; }
    public string CadenaAleatoria  { get; set; }
    // Para Aceptados
    public string Mensaje  { get; set; }
    public string FechaEnvio  { get; set; }
    public string Ticket  { get; set; }
    // Para Errores Rechazos
    public string MensajeError  { get; set; }
    public string FechaError  { get; set; }
    // Para reportes
    public string Cantidad  { get; set; }
    // Para Validar Usuario Certificado Empresa
    public string Usuario  { get; set; }
    public string Clave  { get; set; }
    // Para docreferencias
    public string dr_tipodocumento_id  { get; set; }
    public string dr_tipodoc_desc  { get; set; }
    public string dr_serie { get; set; }
    public string dr_numero { get; set; }
    public string dr_fecha { get; set; }
    public string dr_motivo { get; set; }
    public string jsonpagocredito { get; set; }
    public string hasRetencionIgv { get; set; }
    public double porcRetencionIgv {get;set;}
    public decimal impRetencionIgv {get;set;}
    public decimal impOperacionRetencionIgv {get;set;}


    public string es_ivap {get;set;} 
    
}

public class DocumentoSunat : EN_Documento
{
    public DocumentoSunat()
    {
        ListaDetalleDocumento = new List<EN_DetalleDocumento>();
        ListaDocumentoReferencia = new List<EN_DocReferencia>();
        ListaDocumentoAnticipo = new List<EN_DocAnticipo>();
        ListaDocRefeGuia = new List<EN_DocRefeGuia>();
        FacturaGuia = new EN_FacturaGuia();
        FacturaRemitente = new EN_FacturaRemitente();

        // agregado para formas de pago en credito
        FormaPagoCredito = new List<EN_FormaPagoCredito>();
        // agregado para retencion del igv
        
    }

    public string ValorResumen { get; set; }
    public string ValorFirma { get; set; }
    public string NombreFichero { get; set; }
    public List<EN_DetalleDocumento> ListaDetalleDocumento { get; set; }
    public List<EN_DocReferencia> ListaDocumentoReferencia { get; set; }
    public List<EN_DocAnticipo> ListaDocumentoAnticipo { get; set; }
    public EN_FacturaGuia FacturaGuia { get; set; }
    public EN_FacturaRemitente FacturaRemitente { get; set; }
    public List<EN_DocRefeGuia> ListaDocRefeGuia { get; set; }


    // agregado para forma de pago credito
     public List<EN_FormaPagoCredito> FormaPagoCredito { get; set; }
        

   
   


    }
}