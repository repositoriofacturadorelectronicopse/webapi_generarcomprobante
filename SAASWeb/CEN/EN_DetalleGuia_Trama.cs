/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DetalleGuia_Trama.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de detalle documento trama
 FECHA   : 19/07/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

namespace CEN
{
    public class EN_DetalleGuia_Trama
    {
        public string guia_id { get; set; } // guia ID
        public string empresa_id { get; set; } // empresa
        public string tipodocumento_id  { get; set; } // tipo de documento
        public string line_id { get; set; } // line ID
        public string codigo { get; set; } // codigo
        public string descripcion { get; set; } // descripcion
        public string unidad { get; set; } // unidad
        public string cantidad { get; set; } // cantidad
        
    }
}