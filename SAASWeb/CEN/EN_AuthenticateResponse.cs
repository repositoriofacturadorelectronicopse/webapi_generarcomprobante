/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_AuthenticateResponse.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de autenticación
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;

namespace CEN
{
    public class EN_AuthenticateResponse:EN_RespuestaComun
    {
         public string  token {get;set;}
        public DateTime expiration {get;set;}
        
    }
}
