/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Certificado.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de certificado
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
 ﻿public class EN_Certificado
{
    private int _Id;
    public int Id
    {
        get
        {
            return _Id;
        }
        set
        {
            _Id = value;
        }
    }

    private int _IdEmpresa;
    public int IdEmpresa
    {
        get
        {
            return _IdEmpresa;
        }
        set
        {
            _IdEmpresa = value;
        }
    }
    public int IdPuntoVenta {get;set;}

    private string _Nombre;
    public string Nombre
    {
        get
        {
            return _Nombre;
        }
        set
        {
            _Nombre = value;
        }
    }

    private string _Ubicacion;
    public string Ubicacion
    {
        get
        {
            return _Ubicacion;
        }
        set
        {
            _Ubicacion = value;
        }
    }

    private string _UserName;
    public string UserName
    {
        get
        {
            return _UserName;
        }
        set
        {
            _UserName = value;
        }
    }

    private string _Clave;
    public string Clave
    {
        get
        {
            return _Clave;
        }
        set
        {
            _Clave = value;
        }
    }

    private string _ClaveCertificado;
    public string ClaveCertificado
    {
        get
        {
            return _ClaveCertificado;
        }
        set
        {
            _ClaveCertificado = value;
        }
    }

    private string _IdSignature;
    public string IdSignature
    {
        get
        {
            return _IdSignature;
        }
        set
        {
            _IdSignature = value;
        }
    }

    private string _flagOSE;
    public string flagOSE
    {
        get
        {
            return _flagOSE;
        }
        set
        {
            _flagOSE = value;
        }
    }

    // Informacion Auditoria
    private string _UsuarioSession;
    public string UsuarioSession
    {
        get
        {
            return _UsuarioSession;
        }
        set
        {
            _UsuarioSession = value;
        }
    }

    private string _CadenaAleatoria;
    public string CadenaAleatoria
    {
        get
        {
            return _CadenaAleatoria;
        }
        set
        {
            _CadenaAleatoria = value;
        }
    }
}
}