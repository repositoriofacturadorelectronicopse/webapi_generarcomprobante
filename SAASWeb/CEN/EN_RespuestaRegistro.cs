/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RespuestaRegistro.cs
 VERSION : 1.0
 OBJETIVO: Clase Respuesta Registro
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_RespuestaRegistro
    {
       //DESCRIPCION: Respuesta de registro Guia
        public bool FlagVerificacion { get; set; }  //Flag de verificacion de registro
        public string DescRespuesta { get; set; }  //descripcion de respuesta
        public string mensajeRespuesta { get; set; } // mensaje respuesta
        public string comprobante { get; set; } // comprobante

        public string estadoComprobante { get; set; } //  estado del comprobante
        public string ruta_pdf { get; set; } // ruta pdf
        public string ruta_xml { get; set; } // ruta xml
        public string ruta_cdr { get; set; } // ruta cdr
        public EN_RespuestaRegistro() {
            mensajeRespuesta = "";
            comprobante = "";
        }
    }
}