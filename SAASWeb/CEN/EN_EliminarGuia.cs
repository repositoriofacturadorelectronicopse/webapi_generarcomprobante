/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_EliminarGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase de eliminar guia 
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_EliminarGuia
    {
        //DESCRIPCION: Clase de eliminar Guia
        public EN_RespuestaRegistro RespEliminarGuia { get; set; }  
        public EN_ErrorWebService ErrorWebServ { get; set; }  

        public EN_EliminarGuia() {
            RespEliminarGuia = new EN_RespuestaRegistro();
            ErrorWebServ = new EN_ErrorWebService();
        }
        
    }
}