/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RespuestaListaDetalleGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase de respuesta de listar detalle guia
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/
using System.Collections.Generic;
namespace CEN
{
    public class EN_RespuestaListaDetalleGuia
    {
        //DESCRIPCION: Listar detalle guia
        public EN_RespuestaRegistro ResplistaDetalleGuia{ get; set; } 
        public List<EN_DetalleGuia> listDetGuia = new List<EN_DetalleGuia>();

        public EN_RespuestaListaDetalleGuia() {
            ResplistaDetalleGuia = new EN_RespuestaRegistro();
        }
        
    }
}