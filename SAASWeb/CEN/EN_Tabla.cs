namespace CEN
{
    public class EN_Tabla
    {
        private string _Codtabla;
    public string Codtabla
    {
        get
        {
            return _Codtabla;
        }
        set
        {
            _Codtabla = value;
        }
    }

    private string _Codelemento;
    public string Codelemento
    {
        get
        {
            return _Codelemento;
        }
        set
        {
            _Codelemento = value;
        }
    }

    private string _Descripcion;
    public string Descripcion
    {
        get
        {
            return _Descripcion;
        }
        set
        {
            _Descripcion = value;
        }
    }

    private string _Codsunat;
    public string Codsunat
    {
        get
        {
            return _Codsunat;
        }
        set
        {
            _Codsunat = value;
        }
    }

    private string _Codalterno;
    public string Codalterno
    {
        get
        {
            return _Codalterno;
        }
        set
        {
            _Codalterno = value;
        }
    }
    }
}