﻿

namespace CEN
{
    public class EN_Ubicacion
    {
        private string _IdDepartamento;
        public string IdDepartamento
        {
            get
            {
                return _IdDepartamento;
            }
            set
            {
                _IdDepartamento = value;
            }
        }

        private string _NomDepartamento;
        public string NomDepartamento
        {
            get
            {
                return _NomDepartamento;
            }
            set
            {
                _NomDepartamento = value;
            }
        }

        private string _IdProvincia;
        public string IdProvincia
        {
            get
            {
                return _IdProvincia;
            }
            set
            {
                _IdProvincia = value;
            }
        }

        private string _NomProvincia;
        public string NomProvincia
        {
            get
            {
                return _NomProvincia;
            }
            set
            {
                _NomProvincia = value;
            }
        }

        private string _IdDistrito;
        public string IdDistrito
        {
            get
            {
                return _IdDistrito;
            }
            set
            {
                _IdDistrito = value;
            }
        }

        private string _NomDistrito;
        public string NomDistrito
        {
            get
            {
                return _NomDistrito;
            }
            set
            {
                _NomDistrito = value;
            }
        }
    }

}
