/****************************************************************************************************************************************************************************************
 PROGRAMA: CEN_Guia.cs
 VERSION : 1.0
 OBJETIVO: Clase entidad de Guia
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

 using System;
 using System.Collections.Generic;
namespace CEN
{
    public class EN_Guia
    {
        public int empresa_id { get; set; } // id de empresa
         public string id { get; set; }  // id
         public string tipodocumento_id { get; set; }  //tipo de documento
         public string serie { get; set; } // serie
         public string numero { get; set; }  // numero
         public string fechaemision { get; set; }  // fecha de emision
         public string horaemision { get; set; }  //hora de emision
         public int puntoemision { get; set; }  //punto de emision
         public string observaciones { get; set; }  // observacion
         public string tipodocumentogrbaja { get; set; } // tipo documento baja
        public string numerodocumentogrbaja { get; set; }  //serie y numero de documento baja
         public string seriebaja { get; set; } // serie baja
         public string numerobaja { get; set; } // numero baja
         public string nombredocumentogrbaja { get; set; }  // nombre de documento baja
         public string tipodocumentorelac { get; set; } // tipo de documento relacionado
         public string numerodocumentorelc { get; set; } //numero de documento relacionado
         public string numeracion_dam { get; set; }  //numeracion dam
         public string numero_manifiesto{ get; set; }  //Numero de manifiesto
         // destinatario
         public string destinatario_tipodoc { get; set; } //Tipo de documento de destinatario
         public string destinatario_nrodoc { get; set; }  //Numero de documento de destinatario
         public string destinatario_nombre { get; set; }  //Nombre de destinatario
         public string destinatario_email { get; set; }  // Email de destinatario
         // Proveedor
         public string tercero_tipodoc { get; set; } //Tipo de documento de tercero
         public string tercero_nrodoc { get; set; }  //Numero de documento de tercero
         public string tercero_nombre { get; set; } //Nombre de tercero

         //datos de envio
         public string codmotivo { get; set; }  //Codigo de motivo
         public string descripcionmotivo { get; set; }  //Descripcion de motivo
         public string indicadortransbordo { get; set; }  //Indicador de transbordo
         public Double pesobrutototal { get; set; }  // Peso bruto
         public string unidadpeso { get; set; }  //Unidad de peso
         public int nrobultos { get; set; } // numero de bultos
         public string modalidadtraslado { get; set; }  //Modalidad de traslado
         public string fechatraslado { get; set; } //Fecha de traslado
         public string fechaentrega { get; set; } //Fecha de entrega de bienes al transportista

         //transporte publico
         public string trpublic_tipodoc { get; set; } //Tipo de documento de  transporte publico
         public string trpublic_nrodoc { get; set; } //Numero de documento de transporte publico
         public string trpublic_nombre { get; set; } //Nombre de transporte publico

         //transporte privado
         public string trprivad_placa { get; set; }  //Placa de transporte privado
         public string trprivad_tipodocconductor { get; set; }  // tipo de documento del conductor
         public string trprivad_nrodocconductor { get; set; }  //Numero de documento del conductor

         //Llegada
         public string ubigeoptollegada  { get; set; } //punto de llegada
         public string direccionptollegada { get; set; } //Direccion de punto de llegada
         //datos de contenedor
         public string nrocontenedorimport { get; set; } //Numero de Contenedor
         // Partida
         public string ubigeoptopartida { get; set; } //Punto de partida
         public string direccionptopartida { get; set; } //Direccion punto partida
         //Puerto de desembarque
         public string codpuertodesembarq { get; set; } // codigo de puerto de desembarque
        //datos de embarque
         public string codpuertoembarq { get; set; } // codigo de puerto embarque
         public string nombrepuertoembarq { get; set; } // numero puerto embarque

         public string estado { get; set; } // estado
         public string situacion { get; set; } // situacion
         public string enviado_email { get; set; } // enviado email
         public string enviado_externo { get; set; } // enviado externo
         public string usureg { get; set; } // usuario
         public string fechaIni { get; set; } // fecha inicial
         public string fechaFin { get; set; } // fecha final
         //datos del remitente
         public string numerodocempresa { get; set; } // numero de documento empresa        
         public string descripcionempresa { get; set; } // descripcion empresa
         public string desctipodocumento { get; set; } //descripcion tipo de documento
         public string Usuariosession  { get; set; } // usuario sesión
         public string CadenaAleatoria { get; set; } // cadena aleatoria
         public string Mensaje { get; set; } // mensaje
         public string FechaEnvio { get; set; } // fecha envio
         public string Ticket { get; set; } // ticket
         public string MensajeError { get; set; } // Mensaje de error
         public string FechaError { get; set; } // fecha de error
         public string cantidad { get; set; } // cantidad
         public string nombreXML { get; set; } // Nombre XML
         public string vResumen { get; set; } // Resumen
         public string vFirma { get; set; } // firma
         public string xmlenviado { get; set; } // xml enviado
         public string xmlCDR { get; set; } // CDR
         public string Usuario { get; set; } // usuario
         public string clave { get; set; } // clave
    }
    public class GuiaSunat: EN_Guia 
    {
       public List<EN_DetalleGuia> ListaDetalleGuia { get; set; } // Lista Detalle Guia
       public string valorResumen { get; set; } // valor resumen
       public string valorFirma { get; set; } // valor firma
       public string nombreFichero { get; set; } //nombre fichero


       public  GuiaSunat() {
           ListaDetalleGuia = new List<EN_DetalleGuia>();

       }
    }
}