/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_ConsultaSunat.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad consulta sunat
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_ConsultaSunat
    {
        public string  NroDocEmpresa { get; set; }
        public string  Idtipodocumento { get; set; }
        public string  Serie { get; set; }
        public string  Numero { get; set; }
        public string  NombreXml { get; set; }
        public string  Ticket { get; set; }

        public string  UsuarioCert { get; set; }
        public string  PassUserCert { get; set; }
        
    }
}