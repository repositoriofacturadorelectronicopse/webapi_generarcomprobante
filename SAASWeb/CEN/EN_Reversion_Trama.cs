﻿
using System.Collections.Generic;

namespace CEN
{
    public class EN_Reversion_Trama
    {
    
        public string Id   { get; set;}
        public string Empresa_id   { get; set;}
        public string puntoventa_id   { get; set;}
        public string Nrodocumento   { get; set;}
        public string DescripcionEmpresa   { get; set;}
        public string Tiporesumen   { get; set;}
        public string Correlativo   { get; set;}
        public string FechaDocumento   { get; set;}
        public string FechaComunicacion   { get; set;}
        public string Identificacomunicacion   { get; set;}
        public string Estado   { get; set;}
        public string Situacion   { get; set;}
        public string EnviadoMail   { get; set;}

        // Para Busquedas
        public string FechaIni  { get; set;}
        public string FechaFin   { get; set;}

        // Para Errores Rechazos
        public string MensajeError   { get; set;}
        public string FechaError   { get; set;}

        // Para Baja Aceptadas 
        public string MensajeCDR   { get; set;}
        public string FechaEnvio   { get; set;}

        // Informacion Auditoria
        public string UsuarioSession   { get; set;}
        public string CadenaAleatoria   { get; set;}

        // Para reportes
        public string Cantidad   { get; set;}

        // Ultimos Parametros Agregados
        public string nombreXML   { get; set;}
        public string XMLEnviado   { get; set;}
        public string CDRXML   { get; set;}
        public string vResumen   { get; set;}
        public string vFirma   { get; set;}
        public string ticket   { get; set;}

        // Para Validar Usuario Certificado Empresa
        public string Usuario   { get; set;}
        public string Clave   { get; set;}
    
    }

    public class ReversionSunatTrama : EN_Reversion_Trama
    {
        public ReversionSunatTrama()
        {
            ListaDetalleReversion = new List<EN_DetalleReversion_trama>();
        }
        public string ValorResumen { get; set; }
        public string ValorFirma { get; set; }
        public string NombreFichero { get; set; }
        public List<EN_DetalleReversion_trama> ListaDetalleReversion { get; set; }
    }

}