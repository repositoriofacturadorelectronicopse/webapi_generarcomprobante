/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DocRefeGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de documento referencia de guía 
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
  public class EN_DocRefeGuia
{
  
    public string documento_id {get; set; }         // ID DE DOCUMENTO
    public int empresa_id {get; set; }              // ID DE EMPRESA
    public string tipodocumento_id {get; set; }     // ID TIPO DE DOCUMENTO
    public string tipo_guia {get; set; }            // TIPO GUIA
    public string numero_guia {get; set; }          // NUMERO DE GUIA
    public string desctipoguia {get; set; }         // DESCRIPCION TIPO DE GUIA
    // Informacion Auditoria    
    public string UsuarioSession {get; set; }       // USUARIO SESION
    public string CadenaAleatoria {get; set; }      // CADENA ALEATORIA
    
}

}