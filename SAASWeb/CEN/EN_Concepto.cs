/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Concepto.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad concepto
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_Concepto
    {
        //DESCRIPCION: CLASE DE CONCEPTOS
        public int conceptopfij {get;set;} //Prefijo
        public int conceptocorr {get;set;}  //Correlativo
        public string conceptodesc {get;set;} //Descripción
    }
}