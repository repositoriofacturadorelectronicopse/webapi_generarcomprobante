/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_CompDatosBaja.cs
 VERSION : 1.0
 OBJETIVO: Clase de comprobación de datos de dodumento de baja
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System.Collections.Generic;

namespace CEN
{
    public class EN_CompDatosBaja
    {
      
        public string id { get; set; }                          // ID
        public string fecha { get; set; }                       // FECHA
        public string tipodoc_afecta { get; set; }              // TIPO DE DOCUMENTO DE AFECTACION

       
    }


}