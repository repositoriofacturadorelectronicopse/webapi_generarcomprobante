/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_RequestEnvioGuia.cs
 VERSION : 1.0
 OBJETIVO: Clase Respuesta de envio de Guia a SUNAT
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

namespace CEN
{
    public class EN_RequestEnvioGuia
    {
        //DESCRIPCION:Clase request de envio Guia
        public string flagOse { get; set;}
        public string certUserName { get; set;}
        public string certClave { get; set;}
        public string ruc { get; set;}
        public string nombreArchivo { get; set;}
        public string Id { get; set;}
        public int empresaId { get; set;}
        public string tipodocumentoId { get; set;}
        public string Produccion { get; set;}
        public string fechaemision { get; set;}
        public string Serie { get; set;}
        public string Numero { get; set;}
    }
}