/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DetalleResumen.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de detalle resumen
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_DetalleResumen
    {
  
        public int IdResumen { get; set; }                  // ID RESUMEN
        public int Item { get; set; }                       // ITEM
        public string Tipodocumentoid { get; set; }         // ID TIPO DE DOCUMENTO
        public string DescTipodocumento { get; set; }       // DESCFRIPCION TIPO DE DOCUMENTO
        public string Serie { get; set; }                   // SERIE
        public string Correlativo { get; set; }             // CORRELATIVO
        public string DocCliente { get; set; }              // DOCUMENTO DE CLIENTE
        public string NroDocCliente { get; set; }           // NUMERO DE DOCUMENTO DE CLIENTE
        public int Condicion { get; set; }                  // CONDICION
        public string Moneda { get; set; }                  // MONEDA
        public decimal Opegravadas { get; set; }            // OPERACION GRAVADA
        public decimal Opeexoneradas { get; set; }          // OPERACION EXONERADA
        public decimal Opeinafectas { get; set; }           // OPERACION INAFECTA
        public decimal Opeexportacion { get; set; }         // OPERACION EXPORTACION
        public decimal Opegratuitas { get; set; }           // OPERACION GRATUITA
        public decimal Otroscargos { get; set; }            // OTROS CARGOS
        public decimal TotalISC { get; set; }               // TOTAL ISC
        public decimal TotalIGV { get; set; }               // TOTAL IGV
        public decimal Otrostributos { get; set; }          // OTROS TRIBUTOS
        public decimal Importeventa { get; set; }           // IMPORTE VENTA
        public string Regimenpercep { get; set; }           // REGIMEN PERCEPCION
        public decimal Porcentpercep { get; set; }          // PORCENTAJE PERCEPCION
        public decimal Importepercep { get; set; }          // IMPORTE PERCEPCION
        public decimal Importefinal { get; set; }           // IMPORTE FINAL
        public string Tipodocref { get; set; }              // TIPO DOCUMENTO REFERENCIAL
        public string Nrodocref { get; set; }               // NUMERO DOCUMENTO REFERENCIAL
    
    }
}