﻿
/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DetalleReversion.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de detalle reversión
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

namespace CEN
{
public class EN_DetalleReversion
{
  
    public int IdReversion { get; set;}                     // ID REVERSION
    public int Item  { get; set;}                           //ITEM
    public string Tipodocumentoid { get; set;}              // ID TIPO DE DOCUMENTO    
    public string DescTipodocumento { get; set;}            // DESCRIPCION TIPO DE DOCUMENTO
    public string Serie { get; set;}                        // SERIE    
    public string Numero { get; set;}                       // NUMERO
    public string MotivoReversion { get; set;}              // MOTIVO REVERSION
  
}


}