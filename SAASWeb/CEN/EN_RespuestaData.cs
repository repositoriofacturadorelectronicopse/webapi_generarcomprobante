namespace CEN
{
    public class EN_RespuestaData
    {
         public RespuestaData data { get; set; } 
         public EN_ErrorWebService ErrorWebServ { get; set; }  

        public EN_RespuestaData() {
            data = new RespuestaData();
            ErrorWebServ = new EN_ErrorWebService();
        }
    }
    public class RespuestaData 
    {
        public bool flag { get; set; }  //flag
        public int dato { get; set; }  //dato
        public string DescRespuesta { get; set; }  //descripcion de respuesta

    }

}