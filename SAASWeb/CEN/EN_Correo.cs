/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Correo.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de correo
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

namespace CEN
{
 public class EN_Correo
{
   
    public int id { get; set; }
    public int empresa_id {get; set; }
    public string cuenta_email { get; set; }
    public string nombre_email { get; set; }
    public string clave_email { get; set; }
    public string cuenta_smtp { get; set; }
    public int puerto_smtp { get; set; }
    public string link_descarga { get; set; }

    // Informacion Auditoria
    public string UsuarioSession { get; set; }
    public string CadenaAleatoria { get; set; }
   
}
}