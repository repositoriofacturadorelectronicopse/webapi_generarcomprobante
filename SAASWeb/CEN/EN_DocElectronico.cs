
/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DocElectronico.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de documento electrónico
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

namespace CEN
{
    public class EN_DocElectronico
    {
        
        public string IdDocumento { get; set; }             // ID DE DOCUMENTO
        public int Idempresa { get; set; }                  // ID DE EMPRESA
        public string RucEmisor { get; set; }               // RUC DEL EMISOR
        public string Idtipodocumento { get; set; }         // ID TIPO DE DOCUMENTO
        public string Serie { get; set; }                   // SERIE
        public string Numero { get; set; }                  // NUMERO
        public string FechaEmision { get; set; }            // FECHA DE EMISION
        public decimal Importe { get; set; }                // IMPORTE
        public string Numcompelectronico { get; set; }      // NUMERO COMPROBANTE ELECTRONICO    
        public string NombreXML { get; set; }               // NOMBRE XML
        public string ValorResumen { get; set; }            // VALOR RESUMEN
        public string Firma { get; set; }                   // FIRMA
        public string xmlGenerado { get; set; }             // XML GENERADO
    
        // Informacion Auditoria
        
        public string UsuarioSession { get; set; }          // USUARIO SESION
        public string CadenaAleatoria { get; set; }         // CADENA ALEATORIA    
  
    }
}