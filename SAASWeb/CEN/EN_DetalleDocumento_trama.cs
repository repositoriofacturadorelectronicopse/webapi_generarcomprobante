/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DetalleDocumento_trama.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de detalle documento trama
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
  public class EN_DetalleDocumento_trama
{
    
    public int Idempresa {get; set; }
    public string Iddocumento {get; set; }
    public string Idtipodocumento {get; set; }
    public string Lineid {get; set; }
    public string Codigo {get; set; }
    public string CodigoSunat {get; set; }
    public string Producto {get; set; }
    public string AfectoIgv {get; set; }
    public string AfectoIsc {get; set; }
    public string Afectacionigv {get; set; }
    public string SistemaISC {get; set; }
    public string Unidad {get; set; }
    public string Cantidad {get; set; }
    public string Preciounitario {get; set; }
    public string Precioreferencial {get; set; }
    public string Valorunitario {get; set; }
    public string Valorbruto {get; set; }
    public string Valordscto {get; set; }
    public string Valorcargo {get; set; }
    public string Valorventa {get; set; }
    public string Isc {get; set; }
    public string Igv {get; set; }
    public string Total {get; set; }
    public string factorIsc {get; set; }
    public string factorIgv {get; set; }
    public string RHEmbarcacionPesquera {get; set; }
    public string RHNombreEmbarcacionPesquera {get; set; }
    public string RHDescripcionEspecieVendida {get; set; }
    public string RHLugarDescarga {get; set; }
    public string RHUnidadEspecieVendida {get; set; }
    public string RHCantidadEspecieVendida {get; set; }
    public string RHFechaDescarga {get; set; }
    public string TBVTPuntoOrigen {get; set; }
    public string TBVTDescripcionOrigen {get; set; }
    public string TBVTPuntoDestino {get; set; }
    public string TBVTDescripcionDestino {get; set; }
    public string TBVTDetalleViaje {get; set; }
    public string TBVTValorRefPreliminar {get; set; }
    public string TBVTValorCargaEfectiva {get; set; }
    public string TBVTValorCargaUtil {get; set; }
    public string TBVTNumRegistroMtc {get; set; }
    public string TBVTConfiguracionVehicular {get; set; }
    public string COCodUnicConcesMinera {get; set; }
    public string CONroDeclaracComprom {get; set; }
    public string CONroRegEspecial {get; set; }
    public string CONroResolucPlanta{get; set; }
    public string COLeyMineral {get; set; }
    public string BHCodPaisEmisionPasap {get; set; }
    public string BHCodPaisResidSujetoND {get; set; }
    public string BHFechaIngresoPais {get; set; }
    public string BHFechaIngresoEstab {get; set; }
    public string BHFechaSalidaEstab {get; set; }
    public string BHNroDiasPermanencia {get; set; }
    public string BHFechaConsumo {get; set; }
    public string BHNombreApellidoHuesped {get; set; }
    public string BHTipoDocumentoHuesped {get; set; }
    public string BHNroDocumentoHuesped {get; set; }
    public string PENroExpediente {get; set; }
    public string PECodUnidadEjecutora {get; set; }
    public string PENroProcesoSeleccion {get; set; }
    public string PENroContrato {get; set; }
   
}

}