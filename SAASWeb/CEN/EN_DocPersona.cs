﻿
/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DocPersona.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de documento persona
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

namespace CEN
{
    public class EN_DocPersona
    {
        
        public string Codigo  { get; set; }             // CODIGO
        public string CodigoSunat  { get; set; }        // CODIGO SUNAT    
        public string Nombre { get; set; }              // NOMBRE
        public string Longitud { get; set; }            // LONGITUD - Numero de caracteres
        public string Tipo { get; set; }                // TIPO  - 0-Alfanumerico 1-Numerico
        public string Indcontrib { get; set; }          // INDICADOR CONTRIBUYENTE 0-Docum Solo Nacional 1-Docum Solo Extranjero 2-Doc Ambos
        public string Indlongexacta { get; set; }       // INDICADOR LONGITUD EXACTA 0-LongMax 1-LongExact
        public string Indpersona { get; set; }          // INDICADOR PERSONA N--Natural || J--Juridica || A--Ambos
        public string Orden { get; set; }               // ORDEN
        public string Estado { get; set; }              // ESTADO
       
    }

}
