/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DetalleDocumento.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de detalle documento
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
  public class EN_DetalleDocumento
{
    
    public int Idempresa {get; set; }
    public string Iddocumento {get; set; }
    public string Idtipodocumento {get; set; }
    public int Lineid {get; set; }
    public string Codigo {get; set; }
    public string CodigoSunat {get; set; }
    public string Producto {get; set; }
    public string AfectoIgv {get; set; }
    public string AfectoIsc {get; set; }
    public string Afectacionigv {get; set; }
    public string SistemaISC {get; set; }
    public string Unidad {get; set; }
    public double Cantidad {get; set; }
    public double Preciounitario {get; set; }
    public double Precioreferencial {get; set; }
    public decimal Valorunitario {get; set; }
    public decimal Valorbruto {get; set; }
    public double Valordscto {get; set; }
    public double Valorcargo {get; set; }
    public decimal Valorventa {get; set; }
    public double Isc {get; set; }
    public double Igv {get; set; }
    public double Total {get; set; }
    public double factorIsc {get; set; }
    public double factorIgv {get; set; }
    public string RHEmbarcacionPesquera {get; set; }
    public string RHNombreEmbarcacionPesquera {get; set; }
    public string RHDescripcionEspecieVendida {get; set; }
    public string RHLugarDescarga {get; set; }
    public string RHUnidadEspecieVendida {get; set; }
    public string RHCantidadEspecieVendida {get; set; }
    public string RHFechaDescarga {get; set; }
    public string TBVTPuntoOrigen {get; set; }
    public string TBVTDescripcionOrigen {get; set; }
    public string TBVTPuntoDestino {get; set; }
    public string TBVTDescripcionDestino {get; set; }
    public string TBVTDetalleViaje {get; set; }
    public string TBVTValorRefPreliminar {get; set; }
    public string TBVTValorCargaEfectiva {get; set; }
    public string TBVTValorCargaUtil {get; set; }
    public string TBVTNumRegistroMtc {get; set; }
    public string TBVTConfiguracionVehicular {get; set; }
    public string COCodUnicConcesMinera {get; set; }
    public string CONroDeclaracComprom {get; set; }
    public string CONroRegEspecial {get; set; }
    public string CONroResolucPlanta{get; set; }
    public string COLeyMineral {get; set; }
    public string BHCodPaisEmisionPasap {get; set; }
    public string BHCodPaisResidSujetoND {get; set; }
    public string BHFechaIngresoPais {get; set; }
    public string BHFechaIngresoEstab {get; set; }
    public string BHFechaSalidaEstab {get; set; }
    public string BHNroDiasPermanencia {get; set; }
    public string BHFechaConsumo {get; set; }
    public string BHNombreApellidoHuesped {get; set; }
    public string BHTipoDocumentoHuesped {get; set; }
    public string BHNroDocumentoHuesped {get; set; }
    public string PENroExpediente {get; set; }
    public string PECodUnidadEjecutora {get; set; }
    public string PENroProcesoSeleccion {get; set; }
    public string PENroContrato {get; set; }
   
}

}