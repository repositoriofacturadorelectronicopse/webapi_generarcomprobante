﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DetalleOtrosCpe_trama.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de detalle otros cpe trama
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;

namespace CEN
{
public class EN_DetalleOtrosCpe_trama
{
    
    public string empresa_id  {get; set;}
    public string otroscpe_id {get;set;}
    public string tipodocumento_id {get;set;}
    public string line_id { get; set; }
    public string docrelac_tipodoc_id {get;set;}
    public string docrelac_tipodoc_desc {get; set;}
    public string docrelac_numerodoc { get; set; }
    public string docrelac_fechaemision {get;set;}
    public string docrelac_importetotal {get; set;}
    public string docrelac_moneda {get; set;}
    public string pagcob_fecha {get; set;}
    public string pagcob_fechaft {get; set;}
    public string pagcob_numero {get; set;}
    public string pagcob_importe {get; set;}
    public string pagcob_moneda {get; set;}
    public string retper_importe {get; set;}
    public string retper_moneda {get; set;}
    public string retper_fecha   {get; set;}
    public  string retper_fechaft { get; set; }
    public string retper_importe_pagcob { get;set; }
    public string retper_moneda_pagcob   { get;set; }
    public string tipocambio_monedaref { get;set;} 
    public string tipocambio_monedaobj { get; set; }
    public string tipocambio_factor { get; set; }
    public string tipocambio_fecha {get; set;}
}

}