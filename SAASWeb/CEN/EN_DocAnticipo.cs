
/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_DocAnticipo.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad de documento de anticipo
 FECHA   : 19/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
   public class EN_DocAnticipo
{
    public string documento_id {get; set;}                  // ID DOCUMENTO
    public int empresa_id {get; set;}                       // ID DE EMPRESA
    public string tipodocumento_id {get; set;}              // ID TIPO DE DOCUMENTO
    public int line_id {get; set;}                          // ID LINEA
    public string tipodocanticipo {get; set;}               // TIPO DE DOCUMENTO DE ANTICIPO
    public string desctipodocanticipo {get; set;}           // DESCRIPCION TIPO DE DOCUMENTO ANTICIPO
    public string desctipodocemisor {get; set;}             // DESCRIP TIPO DE DOCUMENTO DEL EMISOR
    public string nrodocanticipo {get; set;}                // NUMERO DOCUMENTO DE ANTICIPO
    public decimal montoanticipo {get; set;}                // MONTO DE ANTICIPO
    public string tipodocemisor {get; set;}                 // TIPO DE DOCUMENTO DEL EMISOR
    public string nrodocemisor {get; set;}                  // NUMERO DOCUMENTO DE EMISOR
    // Informacion Auditoria
    public string UsuarioSession {get; set;}                // USUARIO SESION
    public string CadenaAleatoria {get; set;}               // CADENA ALEATORIA
   
}

}