/****************************************************************************************************************************************************************************************
 PROGRAMA: EN_Constante.cs
 VERSION : 1.0
 OBJETIVO: Clase de entidad  constantes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
namespace CEN
{
    public class EN_Constante
    {
        public const bool g_const_true = true;   // Constante true
        public const bool g_const_false = false;   // Constante false
        public const int g_const_menos1 = -1;   // Constante -1
        public const int g_const_0 = 0; // Constante 0
        public const int g_const_1 = 1; // Constante 1
         public const int g_const_2 = 2; // Constante 2
         public const int g_const_3 = 3; // Constante 3
         public const int g_const_4 = 4; // Constante 4
         public const int g_const_5 = 5; // Constante 5 
         public const int g_const_6 = 6; // Constante 6
         public const int g_const_7 = 7; // Constante 7 
         public const int g_const_8 = 8; // Constante 8
         public const int g_const_9 = 9; // Constante 9 
         public const int g_const_10 = 10; // Constante 10
         public const int g_const_11 = 11; // Constante 11 
         public const int g_const_22 = 22; // Constante 22
         public const int g_const_23 = 23; // Constante 23 
         public const int g_const_24 = 24; // Constante 24 
         public const int g_const_25 = 25; // Constante 25 
        public const string g_const_null = null; // Constante null
        public const string g_const_vacio = ""; // Constante vacio
        public const string g_const_formfech = "dd/MM/yyyy"; // formato de fecha
        public const string g_const_formfechaGuion = "yyyy-MM-dd"; // formato de fecha con guión
        public const string g_const_formfecha_yyyyMM = "yyyyMM"; // formato de fecha
        public const string g_const_formfecha_yyyyMMdd = "yyyyMMdd"; // formato de fecha
         
        public const string g_const_formhora = "HH:mm:ss"; // formato de hora
        public const string g_cost_nameTimeDefect= "America/Bogota"; // Constante timeStandar
        public const string g_const_rango_num = "[0-9]"; // Rango número

        public const string g_const_formaPago_contado= "Contado"; // forma de pago al cotnado
        public const string g_const_factura = "FACTURA"; // FACTURA
        public const string g_const_electronica = "ELECTRÓNICA"; // ELECTRÓNICA
        public const string g_const_boletaVenta = "BOLETA DE VENTA"; // BOLETA DE VENTA

          public const string g_const_tipoResumen= "RA"; // TIPO RESUMEN ANULADOS FACTURA
          public const string g_const_tipoResumenRC= "RC"; // TIPO RESUMEN
          public const string g_const_estadoResumen_add= "1"; // ESTADO DE RESUMEN PARA ADICIONAR
          public const string g_const_estadoResumen_mod= "2"; // ESTADO DE RESUMEN PARA MODIFICAR
          public const string g_const_estadoResumen_baja= "3"; // ESTADO DE RESUMEN PARA DAR DE BAJA


         public const string g_const_funcion_generarXML= "CrearGuiaXml" ; // nombre método

        public const string g_const_id = "Ingrese el ID del documento";
        public const string g_const_serie = "Ingrese serie de Guia";
        public const string g_const_err_serie = "Ingrese correctamente la serie de Guia";
        public const string g_const_numero = "Ingrese numero de Guía";
        public const string g_const_err_numero = "Ingrese numero de Guía correctamente";
        public const string g_const_fechaemision = "Ingrese fecha de emision correctamente";
        public const string g_const_horaemision = "Ingrese hora de emision correctamente";
        public const string g_const_punto_emision = "Ingrese correctamente punto de emision";
        public const string g_const_tipodocumento = "Ingrese tipo de documento correctamente";
        public const string g_const_idEmpresa = "Ingrese ID de empresa correctamente";
        public const string g_const_IdGuia = "Ingrese correctamente el ID de Guia";
        public const string g_const_codMorivo = "Ingrese correctamente código de motivo";
        public const string g_const_descMotEnvio = "Ingrese la descripcion del motivo de traslado";
        public const string g_const_puntoemision ="Ingrese correctamente el punto de Emision";
        public const string g_const_nrodocdestinatario = "Ingrese correctamente el numero de documento de destinatario";
        public const string g_const_nombredestinatario = "Apellidos y nombres, denominación o razón social del destinatario";
        public const string g_const_pesobrutototal = "Ingrese el Peso bruto total de los guía correctamente";
        public const string g_const_numeDecimalesPesoBruTotal = "Ingrese hasta 3 números de decimales del peso bruto";
        public const string g_const_unidadPeso = "Ingrese la unidad de peso del peso bruto";
        public const string g_const_modalidadTraslado = "Ingrese la modalidad de traslado";
        public const string g_const_errmodalidadTraslado = "Ingrese correctamente la modalidad de traslado";
        public const string g_const_fechaTraslado = "Ingrese fecha de traslado";
        public const string g_const_errFechaTraslado = "Ingrese correctamente la fecha de traslado";
        public const string g_const_ubigeollegada = "Ingrese ubigeo de punto de llegada";
        public const string g_const_errubigeollegada = "Ingrese correctamente el ubigeo de punto de llegada";
        public const string g_const_direccionllegada = "Ingrese Dirección completa y detallada de punto llegada";
        public const string g_const_ubigeopartida = "Ingrese ubigeo de punto de partida";
        public const string g_const_direccionpartida = "Ingrese direccion completa y detallada de punto de partida";
        public const string g_const_tipodocdestinatario = "Ingrese correctamente el tipo de documento de destinatario";
        public const string g_const_tipodocremitente = "Ingrese correctamente el tipo de documento del remitente";
        public const string g_const_nrodocremitente = "Ingrese correctamente el numero de documento del remitente";
        public const string g_const_situacion = "Ingrese situación de Guia";
        public const string g_const_estado = "Ingrese estado de Guía";
        public const string g_const_usuario = "Ingrese correctamente el usuario";
        public const string g_const_clave = "Ingrese correctamente la clave";
        public const string g_const_correo_destinatario = "Ingrese correctamente Email de destinatario";
        public const string g_const_numero_linea_detalle = "Ingrese correctamente el numero de linea de detalle de guia";
        public const string g_const_cantidad_bien = "Ingrese correctamente la cantidad de bienes a transportar";
        public const string g_const_codigo_producto = "Ingrese correctamente el código del bien a transporat";
        public const string g_const_descripcion_producto = "Ingrese correctamente la descripcion del bien a transportar";
        public const string g_const_unidad_bien = "Ingrese correctamente la descripcion de la unidad del bien a tranportar";
        public const string g_const_formtnumero = "[0-9]";
        public const string g_const_consultaexitosa = "CONSULTA EXITOSA";
        public const int g_const_1022 = 1022;  //CONSTANTE DE VALOR 2022
        public const string g_const_ruc = "RUC";  // RUC
        public const string g_const_a = "A"; // PENDIENTE DE ENVIO
        public const string g_const_b = "B"; // dado de baja
        public const string g_const_r = "R"; //Constante R
        public const string g_const_e = "E"; //Constante E
        public const string g_const_p = "P"; //Constante P
        public const int g_const_prefijo_cpe_doc = 2; // Prefijo de comprobante electronico doc
        public const int  g_const_100 = 100; // Constante 100
        public const int  g_const_101 = 101; // Constante 101
        public const int  g_const_102 = 102; // Constante 102
        public const int  g_const_103 = 103; // Constante 103
        public const int  g_const_104= 104; // Constante 104
        public const int  g_const_105 = 105; // Constante 105
        public const int  g_const_106 = 106; // Constante 106
        public const int  g_const_107 = 107; // Constante 107
        public const int  g_const_108 = 108; // Constante 108
        public const int  g_const_109 = 109; // Constante 109
        public const int  g_const_110 = 110; // Constante 110
        public const int  g_const_111 = 111; // Constante 111
        public const int  g_const_112 = 112; // Constante 112
        public const int  g_const_113 = 113; // Constante 113
        public const int  g_const_114 = 114; // Constante 114
        public const int  g_const_115 = 115; // Constante 115
        public const int  g_const_116 = 116; // Constante 116
        public const int  g_const_117 = 117; // Constante 117
        public const int  g_const_118 = 118; // Constante 118
        public const int  g_const_119 = 119; // Constante 119
        public const int  g_const_150 = 150; // Constante 150
        public const int  g_const_151 = 151; // Constante 151
        public const int  g_const_152 = 152; // Constante 152
        public const int  g_const_153 = 153; // Constante 113
        public const int  g_const_154 = 154; // Constante 154
        public const int  g_const_155 = 155; // Constante 155


        public const int g_const_1000 = 1000; // Constante 1000
        public const int g_const_1001 = 1001; // Constante 1001
        public const int g_const_1003 = 1003; // Constante 1003
        public const int g_const_1005 = 1005; // Constante 1005
        public const int g_const_2000 = 2000; // Constante 2000
        public const int g_const_2001 = 2001; // Constante 2001
        public const int g_const_2002 = 2002; // Constante 2002
        public const int g_const_2003 = 2003; // Constante 2003
        public const int g_const_3000 = 3000; // Constante 3000
        public const int g_const_4000 = 4000; // Constante 4000
        public const int g_const_1020 = 1020; // Constante 1020
        
        public const int g_const_1021 = 1021; // Constante 1021

        public const string g_const_tipodoc_factura = "01"; // TIPO DE DOCUMENTO PARA FACTURA
        public const string g_const_tipodoc_boleta = "03"; // TIPO DE DOCUMENTO PARA BOLETA

        public const string g_const_tipodoc_nc = "07"; // TIPO DE DOCUMENTO PARA NOTA DE CREDITO
        public const string g_const_tipodoc_nd = "08"; // TIPO DE DOCUMENTO PARA NOTA DE DEBITO
         public const string g_const_tipodoc_guia = "09"; // TIPO DE DOCUMENTO PARA GUIA DE REMISION


        public const string g_const_cultureEsp="es-PE"; //constante culture español
        public const int g_const_length_numero_cpe_doc = 8; // Conteo de caracteres campo numero de serie de documento electrónico
        public const int g_const_length_id_cpe_doc = 15; // Conteo de caracteres campo ID de documento electrónico


        public const string g_const_valExito = "Operación Exitosa"; // Constante de éxito para registro log fin
        public const string g_const_errlogCons = "Error al ejecutar método "; // Constante de error para registro log fin

        public const string g_const_estado_nuevo = "N"; // Constante de estado nuevo
        public const string g_const_situacion_pendiente= "P"; // Constante de situación pendiente
        public const string g_const_situacion_aceptado= "A"; // Constante de situación aceptado
        public const string g_const_situacion_rechazado= "R"; // Constante de situación rechazado
        public const string g_const_situacion_error= "E"; // Constante de situación error
        public const string g_const_rutaSufijo_xml = "XML/"; // ruta sufijo para xml
        public const string g_const_rutaSufijo_pdf = "PDF/"; // ruta sufijo para pdf
        public const string g_const_rutaSufijo_cdr = "CDR/"; // ruta sufijo para cdr
        public const string g_const_extension_xml = ".XML"; //  extensión para xml
        public const string g_const_extension_zip = ".ZIP"; //  extensión para zip
        public const string g_const_extension_pdf = ".PDF"; //  extensión para pdf
        public const string g_const_prefijoXmlCdr_respuesta = "R-"; // prefijo para xml y cdr respuesta

        public const string g_const_si = "SI"; // SI
        public const char g_const_prefijo_Factura = 'F'; // prefijo de comprobante factura F001=F
        
        

        
        

        public const string g_const_UsuarioSession= "Sistema"; // Constante de UsuarioSesion

        public const string g_const_comprobante_noValido= "El comprobante a generar no es válido."; // mensaje respuesta: El comprobante a generar no es válido
        public const string g_const_certificado_NoEncontrado= "No se ha encontrado el usuario del certificado de la empresa ingresada"; // mensaje respuesta: No se ha encontrado el usuario del certificado de la empresa ingresada
        public const string g_const_exc_GenerarComp= "Hubo una Excepción al generar el comprobante:"; // mensaje respuesta excepción al generar comprobante

          public const string g_const_logoBlaco_jpg= "_logoblanco.jpg"; // mensaje respuesta excepción al generar comprobante

 
        

       
        
        
        

        //errores
        public const string g_const_error_tokensession = "POR FAVOR VERIFIQUE SU TOKEN DE SESION"; // Error inicio de sessión
        public const string g_const_error_trama = "POR FAVOR INGRESE DATOS"; // Error trama entrada
        public const string g_const_error_tramaGeneral = "HAY UN ERROR EN LA TRAMA DE ENVIO, FAVOR CORREGIR"; // Error interno general
          public const string g_const_error_interno = "Hubo un problema interno, por favor vuelva a intentar en unos momentos."; // Error interno general
          public const string g_const_err_usuario_token = "POR FAVOR VERIFIQUE EL USUARIO EN TRAMA";//Constante de error de USUARIO 
          public const string g_const_err_pass_token = "POR FAVOR VERIFIQUE EL PASSWORD EN TRAMA ";//Constante de error de PASSWORD
          public const string g_const_err_idcpedoc = "POR FAVOR VERIFIQUE EL ID EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ID de documento electrónico
          public const string g_const_err_usuario_cpedoc = "POR FAVOR VERIFIQUE EL USUARIO SOAP DE SUNAT EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de USUARIO de documento electrónico
          public const string g_const_err_pass_cpedoc = "POR FAVOR VERIFIQUE EL PASSWORD SOAP DE SUNAT EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de PASSWORD de documento electrónico
          public const string g_const_err_idempresa_cpedoc = "POR FAVOR VERIFIQUE EL ID DE EMPRESA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ID DE EMPRESA de documento electrónico
          public const string g_const_err_idtipodoc_cpedoc = "POR FAVOR VERIFIQUE EL ID DE TIPO DE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ID DE TIPO DE DOCUMENTO de documento electrónico
          public const string g_const_err_idtipodocnota_cpedoc = "POR FAVOR VERIFIQUE EL ID DE TIPO DE DOCUMENTO  DE NOTA (1 O 3) EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ID DE TIPO DE DOCUMENTO  DE NOTA de documento electrónico
          public const string g_const_err_serie_cpedoc = "POR FAVOR VERIFIQUE LA SERIE EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de SERIE de documento electrónico
          public const string g_const_err_numero_cpedoc = "POR FAVOR VERIFIQUE EL NUMERO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de NUMERO de documento electrónico
          public const string g_const_err_idpntvnt_cpedoc = "POR FAVOR VERIFIQUE EL ID DE PUNTO DE VENTA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IDE DE PUNTO DE VENTA de documento electrónico
          public const string g_const_err_tipoop_cpedoc = "POR FAVOR VERIFIQUE EL TIPO DE OPERACION EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TIPO DE OPERACION de documento electrónico
          public const string g_const_err_descuento_afecta_base = "POR FAVOR SI AFECTA A LA BASE IMPONIBLE EL DESCUENTO GLOBAL EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TIPO DE OPERACION de documento electrónico
          public const string g_const_err_tipodoccl_cpedoc = "POR FAVOR VERIFIQUE EL TIPO DE DOCUMENTO DEL CLIENTE EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TIPO DE DOCUMENTO DEL CLIENTE de documento electrónico
          public const string g_const_err_nrodoccl_cpedoc = "POR FAVOR VERIFIQUE EL NUMERO DE DOCUMENTO DEL CLIENTE EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de NUMERO DE DOCUMENTO DEL CLIENTE de documento electrónico
          public const string g_const_err_nombrecl_cpedoc = "POR FAVOR VERIFIQUE EL NOMBRE DEL CLIENTE EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de NOMBRE DEL CLIENTE de documento electrónico
          public const string g_const_err_fecha_cpedoc = "POR FAVOR VERIFIQUE LA FECHA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de FECHA de documento electrónico
          public const string g_const_err_hora_cpedoc = "POR FAVOR VERIFIQUE LA HORA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de FECHA de documento electrónico
          public const string g_const_err_moneda_cpedoc = "POR FAVOR VERIFIQUE LA MONEDA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de MONEDA de documento electrónico
          public const string g_const_err_igvventa_cpedoc = "POR FAVOR VERIFIQUE EL IGV DE VENTA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IGV DE VENTA de documento electrónico
          public const string g_const_err_importeventa_cpedoc = "POR FAVOR VERIFIQUE EL IMPORTE DE VENTA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IMPORTE DE VENTA de documento electrónico
          public const string g_const_err_importefinal_cpedoc = "POR FAVOR VERIFIQUE EL IMPORTE FINAL EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IMPORTE FINAL de documento electrónico
          public const string g_const_err_estado_cpedoc = "POR FAVOR VERIFIQUE EL ESTADO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ESTADO de documento electrónico
          public const string g_const_err_situacion_cpedoc = "POR FAVOR VERIFIQUE SITUACIÓN EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de SITUACIÓN de documento electrónico
          public const string g_const_err_estransgratuita_cpedoc = "POR FAVOR VERIFIQUE SI ES TRANSACCIÓN GRATUITA EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TRANSACCIÓN GRATUITA de documento electrónico
          public const string g_const_err_formapago_cpedoc = "POR FAVOR VERIFIQUE LA FORMA DE PAGO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de FORMA DE PAGO de documento electrónico
          public const string g_const_err_idddcpedoc = "POR FAVOR VERIFIQUE EL ID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ID DE DETALLE DOCUMENTO  de documento electrónico
          public const string g_const_err_idempresadd_cpedoc = "POR FAVOR VERIFIQUE EL ID DE EMPRESA DE DETALLE DOCUMENTO  EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IDE EMPRESA  DE DETALLE DOCUMENTO de documento electrónico
          public const string g_const_err_idtipodocdd_cpedoc = "POR FAVOR VERIFIQUE EL ID DE TIPO DE DOCUMENTO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TIPO DE DOCUMENTO DE DETALLE DOCUMENTO de documento electrónico
          public const string g_const_err_lineiddd_cpedoc = "POR FAVOR VERIFIQUE LINEID DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de LINEID DE DETALLE DOCUMENTO de documento electrónico
          public const string g_const_err_productodd_cpedoc = "POR FAVOR VERIFIQUE EL PRODUCTO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de PRODUCTO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_afectoigvdd_cpedoc = "POR FAVOR VERIFIQUE EL AFECTO IGV DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de AFECTO IGV DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_afectoiscdd_cpedoc = "POR FAVOR VERIFIQUE EL AFECTO ISC DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de AFECTO ISC DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_afectacionigvdd_cpedoc = "POR FAVOR VERIFIQUE LA AFECTACION IGV DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de AFECTACION IGV DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_unidaddd_cpedoc = "POR FAVOR VERIFIQUE LA UNIDAD DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de UNIDAD DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_cantidaddd_cpedoc = "POR FAVOR VERIFIQUE LA CANTIDAD DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de CANTIDAD DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_preciounitariodd_cpedoc = "POR FAVOR VERIFIQUE EL PRECIO UNITARIO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de PRECIO UNITARIO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_preciorefdd_cpedoc = "POR FAVOR VERIFIQUE EL PRECIO REFERENCIAL DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de PRECIO REFERENCIAL DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_valorunitariodd_cpedoc = "POR FAVOR VERIFIQUE EL VALOR UNITARIO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO. VALOR DEBE TENER MINIMO {0} DECIMALES";//Constante de error de VALOR UNITARIO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_valorbrutodd_cpedoc = "POR FAVOR VERIFIQUE EL VALOR BRUTO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO. VALOR DEBE TENER MINIMO {0} DECIMALES";//Constante de error de VALOR BRUTO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_valorventadd_cpedoc = "POR FAVOR VERIFIQUE EL VALOR VENTA DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO. VALOR DEBE TENER MINIMO {0} DECIMALES";//Constante de error de VALOR VENTA DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_igvdd_cpedoc = "POR FAVOR VERIFIQUE EL IGV DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de IGV DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_totaldd_cpedoc = "POR FAVOR VERIFIQUE EL TOTAL DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de TOTAL DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_factorigvdd_cpedoc = "POR FAVOR VERIFIQUE EL FACTOR IGV DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de FACTOR IGV DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_valordscdd_cpedoc = "POR FAVOR VERIFIQUE EL VALOR DESCUENTO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de VALOR DESCUENTO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_valorcargodd_cpedoc = "POR FAVOR VERIFIQUE EL VALOR CARGO DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de VALOR CARGO DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_iscdd_cpedoc = "POR FAVOR VERIFIQUE EL ISC DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de ISC DE DETALLE  DOCUMENTO de documento electrónico
          public const string g_const_err_factoriscdd_cpedoc = "POR FAVOR VERIFIQUE EL FACTOR ISC DE DETALLE DOCUMENTO EN TRAMA DE DOCUMENTO ELECTRONICO.";//Constante de error de FACTOR ISC DE DETALLE  DOCUMENTO de documento electrónico

           public const string g_const_err_usuariosession_cb = "POR FAVOR VERIFIQUE EL USUARIO SESION EN TRAMA.";//Constante de error de USUARIO SESION
           public const string g_const_err_fechadoc_cb = "POR FAVOR VERIFIQUE LA FECHA DEL DOCUMENTO EN TRAMA.";//Constante de error de FECHA DE DOCUMENTO 
           public const string g_const_err_fechacom_cb = "POR FAVOR VERIFIQUE LA FECHA DE COMUNICADO DE BAJA EN TRAMA.";//Constante de error de FECHA COMUNICADO DE BAJA
           public const string g_const_err_listadetalle_cb = "POR FAVOR VERIFIQUE LA LISTA DETALLE BAJA EN TRAMA.";//Constante de error de LISTA DETALLE BAJA
           public const string g_const_err_tipodocid_db_cb = "POR FAVOR VERIFIQUE EL ID DE TIPO DE DOCUMENTO EN LISTA DETALLE BAJA EN TRAMA.";//Constante de error de ID DE TIPO DE DOCUMENTO EN LISTA DETALLE BAJA
           public const string g_const_err_serie_db_cb = "POR FAVOR VERIFIQUE LA SERIE EN LISTA DETALLE BAJA EN TRAMA.";//Constante de error de SERIE EN LISTA DETALLE BAJA
           public const string g_const_err_numero_db_cb = "POR FAVOR VERIFIQUE EL NUMERO EN LISTA DETALLE BAJA EN TRAMA.";//Constante de error de NUMERO EN LISTA DETALLE BAJA
           public const string g_const_err_motivo_db_cb = "POR FAVOR VERIFIQUE EL MOTIVO DE BAJA EN LISTA DETALLE BAJA EN TRAMA.";//Constante de error de EL MOTIVO DE BAJA  EN LISTA DETALLE BAJA

          public const string g_const_err_cpependientesenvio_cb = "DOCUMENTO(S) PENDIENTE(S) DE ENVIO.";//documentos pendientes de envio

          public const string g_const_err_cpenoexiste_cb = "COMPROBANTE A DAR DE BAJA NO EXISTE";//COMPROBANTE A DAR DE BAJA NO EXISTE
          public const string g_const_err_cpediferentestiposafecto_cb = "HAY COMPROBANTES CON DIFERENTES TIPOS DE COMPROBANTE AFECTO A LA NOTA. DEBEN SER COMPROBANTES DEL MISMO TIPO ";//COMPROBANTE CON DIFERENTES TIPOS AFECTOS A LA NOTA
          public const string g_const_err_cpediferentestipodocsid_cb = "HAY COMPROBANTES CON DIFERENTES TIPOS DE DOCUMENTO ID. DEBEN SER COMPROBANTES DEL MISMO TIPO ";//COMPROBANTE CON DIFERENTES TIPOS DOCUMENTO ID
          public const string g_const_err_cpefechadiferente_cb = "COMPROBANTE A DAR DE BAJA NO CORRESPONDE CON FECHA DE DOCUMENTO: ";//DISTINTAS FECHAS EN COMPROBANTES A DAR DE BAJA

          

            


        public const string g_const_url_namespace_xsi = "http://www.w3.org/2001/XMLSchema-instance";  // namespace url xsi
        public const string g_const_url_namespace_xsd = "http://www.w3.org/2001/XMLSchema";  // namespace url xsd
            public const string g_const_url_namespace_ds = "http://www.w3.org/2000/09/xmldsig#";  // namespace url ds
              
        public const string g_const_programa = "SAASWeb" ; // Nombre del Web Api
        public const string g_const_url = "http://localhost/SAASWeb" ; // url de servicio
         public const string g_const_user_log= "IDE" ; // usuario de log
         public const string g_const_funcion_generarCpe= "GenerarComprobanteDoc" ; // nombre método
         public const string g_const_funcion_generarOtrosCpe= "GenerarOtrosCpe" ; // nombre método
         public const string g_const_funcion_generarComunicadoBaja= "GenComunicadoBaja" ; // nombre método
         public const string g_const_funcion_generarReversionOtroscpe= "GenReversionOtrosCpe" ; // nombre método

         
         public const string g_const_funcion_crearToken =  "IniciarSession" ; // nombre método



        public const string g_const_pendiente_envio = "El comprobante esta pendiente de envío a SUNAT";
        public const string g_const_dado_baja = "El comprobante ha sido dado de baja";
        public const string g_const_genPDF = "ARCHIVO PDF DE GUIA GENERADO CORRECTAMENTE";
        public const string g_const_genPDFerr = "ERROR AL GENERAR PDF DE GUIA";
        public const string g_const_genXML = "ARCHIVO XML DE GUIA GENERADO CORRECTAMENTE";
        public const string g_const_genXMLerr = "ERROR AL GENERAR XML DE GUIA";
        public const string g_const_textoTipoDoc = "GUIA DE REMISION"; //texto de guia
        public const string g_const_textoTipoDoc2 = "ELECTRÓNICA"; //texto de guia 
        public const string g_const_firma = "No existe documento firma";  // No existe firma parametrizado
        public const string g_const_psfirma = "Se ha agregado la firma correctamente"; //Firma a agregar
        public const string g_const_logoblanco = "Archivo logoblanco no encontrado"; //Archivo de logo

        public const string g_const_funcion_generarGuia= "CrearGuiaXml" ; // nombre método
        public const string g_const_estado_n = "N";// Estado de guia
        public const string g_const_situacion_p = "P"; //Situacion de guia
        public const string g_const_listavacia = "Ingrese contenido de lista detalle de guia";

        public const int g_const_15 = 15; // Constante 15 
        public const string g_const_exp_formcorr = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*"; //expresion regular formato correo

        public const string g_const_sufijoRutaTemporalPSE = "/PSEtmp"; //sufijo de ruta temporal
    }
}