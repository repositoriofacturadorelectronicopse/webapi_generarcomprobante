/****************************************************************************************************************************************************************************************
 PROGRAMA: DAO_ConfigConstantes.cs
 VERSION : 1.0
 OBJETIVO: Clase de configuración de constantes
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System.IO;
using Microsoft.Extensions.Configuration;
using CLN;
using CEN;
using System;
using AppConfiguracion;

namespace AppServicioDoc
{
    public class DAO_ConfigConstantes
    {

        private readonly IConfiguration _configuration;
     
        public DAO_ConfigConstantes()
        {
            // DESCRIPCION: CONSTRUCTOR DE CLASE CONFIGURACION CONSTANTES
              
               var builder = new ConfigurationBuilder() //CONSTRUCTOR
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json",optional:true, reloadOnChange:true);
                _configuration = builder.Build();
        }

        
       

           public void guardar_ConfigConstantes_JWT()
        {
            // DESCRIPCION: GUARDAR CONSTANTES DE CONFIGURACION
            
            NE_Facturacion ne_documento = new NE_Facturacion();     // CLASE DE NEGOCIO DE FACTURACION
            
            // EN_ConfigConstantes.Instance.const_cadenaCnxBdFE= _configuration.GetConnectionString("ConexionBdFE");
            if(EN_ConfigConstantes.Instance.const_cadenaCnxBdFE==  EN_Constante.g_const_vacio || EN_ConfigConstantes.Instance.const_cadenaCnxBdFE== null)
            {
                SetCadenaConexion();
            }
                    
            EN_ConfigConstantes.Instance.const_CultureInfoPeru = _configuration["AppSeettings:CultureInfoPeru"];

            EN_ConfigConstantes.Instance.const_TiempoEspera = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_110);  // TIEMPO DE ESPERA EJECUTAR QUERY- APPSETTINGS
                 
            EN_ConfigConstantes.Instance.const_frase_cont = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_100);  // FRASE DE ENCRIPTACION PARA CONTRASEÑA
            EN_ConfigConstantes.Instance.const_SecretKey = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_101);  // SECRET KEY - JWT SETTINGS
            EN_ConfigConstantes.Instance.const_Issuer = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_102);  // ISSUER - JWT SETTINGS
            EN_ConfigConstantes.Instance.const_Audience = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_103);  // AUDIENCE - JWT SETTINGS
            EN_ConfigConstantes.Instance.const_ExpirationTime = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_105);  // EXPIRATION TIME -POR DIA - JWT SETTINGS                   
            EN_ConfigConstantes.Instance.const_requireHttps = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_104);  // REQUIRE HTTP - JWT SETTINGS  
            EN_ConfigConstantes.Instance.const_codErrorJwt = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_106);  // CODIGO DE ERROR - JWT SETTINGS                   
           
           

    
        }

        public void SetCadenaConexion()
        {
              // DESCRIPCION: GUARDAR CONSTANTES DE CONFIGURACION CADENA DE CONEXION

              Configuracion config = new Configuracion();

            string const_ServiceManager_UrlWebApi;
            string const_ServiceManager_Puerto;
            string const_ServiceManager_ApiService;
            string const_ServiceManager_Region;
            string const_ServiceManager_VersionStage;
            string const_ServiceManager_SecretName;

            try
            {
                const_ServiceManager_UrlWebApi             = _configuration["AppSeettings:ServiceManager_UrlWebApi"];
                const_ServiceManager_Puerto                = _configuration["AppSeettings:ServiceManager_Puerto"];
                const_ServiceManager_ApiService            = _configuration["AppSeettings:ServiceManager_ApiService"];
                const_ServiceManager_Region                = _configuration["AppSeettings:ServiceManager_Region"];
                const_ServiceManager_VersionStage          = _configuration["AppSeettings:ServiceManager_VersionStage"];

                const_ServiceManager_SecretName            = (_configuration["AppSeettings:BaseDatosAmbiente"]=="desarrollo")? _configuration["AppSeettings:ServiceManager_SecretNameDesarrollo"]:_configuration["AppSeettings:ServiceManager_SecretNameProduccion"];

                EN_ConfigConstantes.Instance.const_cadenaCnxBdFE= config.GetCadenaConexionFromWA(const_ServiceManager_UrlWebApi, const_ServiceManager_Puerto, const_ServiceManager_ApiService, const_ServiceManager_Region, const_ServiceManager_VersionStage, const_ServiceManager_SecretName);

            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }

       
        }

        public void guardar_ConfigConstantes()
        {
            // DESCRIPCION: GUARDAR CONSTANTES DE CONFIGURACION
            NE_Facturacion ne_documento = new NE_Facturacion();     // CLASE DE NEGOCIO DE FACTURACION

            try
            {
                // EN_ConfigConstantes.Instance.const_cadenaCnxBdFE= _configuration.GetConnectionString("ConexionBdFE");

                if(EN_ConfigConstantes.Instance.const_cadenaCnxBdFE==  EN_Constante.g_const_vacio || EN_ConfigConstantes.Instance.const_cadenaCnxBdFE== null)
                {
                    SetCadenaConexion();
                }
                                
                EN_ConfigConstantes.Instance.const_CultureInfoPeru = _configuration["AppSeettings:CultureInfoPeru"];

                EN_ConfigConstantes.Instance.const_versionUBL = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_111);  // VERSION UBL- APPSETTINGS

                EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_116);  //  CODIGO DE ETIQUETA DE ERROR DOC - APPSETTINGS
                EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorRes = ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_117);  // CODIGO DE ETIQUETA DE ERROR RESUMEN - APPSETTINGS
                
                EN_ConfigConstantes.Instance.const_urlServiceDoc= ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_150);  // URL SERVICIO SERVICEDOC (URL ENVIO DE CPE)- APPSETTINGS
                // EN_ConfigConstantes.Instance.const_urlServiceDoc= "https://localhost:6001";  // DEV

                EN_ConfigConstantes.Instance.const_apiSendDoc= ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_151);  // API SERVICIO ENVIAR DOC (API WEBMETHOD DE ENVIO CPE)- APPSETTINGS

                EN_ConfigConstantes.Instance.const_urlServiceOtrosCpe= ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_154);  // URL SERVICIO SERVICEOTROSCPE (URL ENVIO DE OTROS COMPROBANTES)- APPSETTINGS
                // EN_ConfigConstantes.Instance.const_urlServiceOtrosCpe= "https://localhost:7001";  // dev
                EN_ConfigConstantes.Instance.const_apiSendOtrosCpe= ne_documento.buscarParametroAppSettings(EN_Constante.g_const_1, EN_Constante.g_const_155);  // API SERVICIO ENVIAR OTROS CPE (API WEBMETHOD DE ENVIO OTROS COMPROBANTES)- APPS

            
                EN_ConfigConstantes.Instance.const_TipoAfectOnerosas = ne_documento.buscarConstantesTablaSunat("007","10");
                EN_ConfigConstantes.Instance.const_TipoInafectOnerosas = ne_documento.buscarConstantesTablaSunat("007","30");
                EN_ConfigConstantes.Instance.const_TipoExonerado = ne_documento.buscarConstantesTablaSunat("007","20");
                EN_ConfigConstantes.Instance.const_TipoExportacion = ne_documento.buscarConstantesTablaSunat("007","40");
                EN_ConfigConstantes.Instance.const_TipoRetiroGratuito = ne_documento.buscarConstantesTablaSunat("007","RG");

                EN_ConfigConstantes.Instance.const_IdFC = ne_documento.buscarConstantesTablaSunat("001","01");
                EN_ConfigConstantes.Instance.const_IdBV = ne_documento.buscarConstantesTablaSunat("001","03");
                EN_ConfigConstantes.Instance.const_IdNC = ne_documento.buscarConstantesTablaSunat("001","07");
                EN_ConfigConstantes.Instance.const_IdND = ne_documento.buscarConstantesTablaSunat("001","08");
                EN_ConfigConstantes.Instance.const_IdGR = ne_documento.buscarConstantesTablaSunat("001","09");
                EN_ConfigConstantes.Instance.const_IdCB = ne_documento.buscarConstantesTablaSunat("001","RA");
                EN_ConfigConstantes.Instance.const_IdRD = ne_documento.buscarConstantesTablaSunat("001","RC");
                EN_ConfigConstantes.Instance.const_IdRR = ne_documento.buscarConstantesTablaSunat("001","RR");
                EN_ConfigConstantes.Instance.const_IdCP = ne_documento.buscarConstantesTablaSunat("001","40");
                EN_ConfigConstantes.Instance.const_IdCR = ne_documento.buscarConstantesTablaSunat("001","20");

    
                guardar_ConfigConstantes_S3();
                
            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
           
    
        }


        public void guardar_ConfigConstantes_S3()
        {
            // DESCRIPCION: GUARDAR CONSTANTES DE CONFIGURACION DE S3 AWS

            EN_Parametro res_par;              // clase entidad parametro para resultado
            EN_Parametro bus_par;              // clase entidad parametro para búsqueda

            NE_Empresa ne_empresa = new NE_Empresa();     // CLASE DE NEGOCIO DE EMPRESA

            try
            {

                res_par = new EN_Parametro();  
                bus_par = new EN_Parametro();
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_22;
                res_par =ne_empresa.buscar_tablaParametro(bus_par);
                EN_ConfigConstantes.Instance.const_s3_accessKey = res_par.par_descripcion;

                res_par = new EN_Parametro();  
                bus_par = new EN_Parametro();
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_23;
                res_par =ne_empresa.buscar_tablaParametro(bus_par);
                EN_ConfigConstantes.Instance.const_s3_secretKey = res_par.par_descripcion;

                res_par = new EN_Parametro();  
                bus_par = new EN_Parametro();
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_24;
                res_par =ne_empresa.buscar_tablaParametro(bus_par);
                EN_ConfigConstantes.Instance.const_s3_region = res_par.par_descripcion;

                res_par = new EN_Parametro();  
                bus_par = new EN_Parametro();
                bus_par.par_conceptopfij=EN_Constante.g_const_1;
                bus_par.par_conceptocorr=EN_Constante.g_const_25;
                res_par =ne_empresa.buscar_tablaParametro(bus_par);
                EN_ConfigConstantes.Instance.const_s3_bucketName = res_par.par_descripcion;

                
                if ((!System.IO.Directory.Exists(Directory.GetCurrentDirectory()+EN_Constante.g_const_sufijoRutaTemporalPSE)))
                {
                    System.IO.Directory.CreateDirectory(Directory.GetCurrentDirectory()+EN_Constante.g_const_sufijoRutaTemporalPSE);
                }

                EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos = Directory.GetCurrentDirectory()+EN_Constante.g_const_sufijoRutaTemporalPSE;
            }
            catch(Exception ex)
            {
                throw ex;

            }

    
        }

        public EN_RespuestaTablaSunat listarConfiguracionConstante() 
        {
            //DESCRIPCION: Listar parametrizacion de rutas

            EN_RespuestaTablaSunat listarParametros = new EN_RespuestaTablaSunat();
            try
            {
                int flag = 0;

                guardar_ConfigConstantes_S3();
            
                
                // guardar_constantes_tabla();
                NE_Facturacion ne_documento = new NE_Facturacion();
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"007","10");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_TipoAfectOnerosas  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }

                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"007","10");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_TipoAfectOnerosas  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }

                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"007","30");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_TipoInafectOnerosas  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }

                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"007","20");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_TipoExonerado  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"007","40");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_TipoExportacion  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"007","RG");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_TipoRetiroGratuito  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                // PARTE 2
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"001","01");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdFC  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"001","03");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdBV  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }

                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"001","07");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdBV  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"001","08");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdND  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"001","09");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdGR  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"001","RA");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdCB  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }

                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"001","RC");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdRD  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"001","RR");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdRR  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"001","40");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdCP  = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                listarParametros =  ne_documento.listarConstantesTablaSunat(flag,"001","20");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdCR = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                // PARTE 3

    

                listarParametros =  ne_documento.listarConstantesTablaSunat(1,"018","01");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdTrPub = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }
                listarParametros =  ne_documento.listarConstantesTablaSunat(1,"018","02");
                
                if(listarParametros.ResplistaTablaSunat.FlagVerificacion) 
                {
                    EN_ConfigConstantes.Instance.const_IdTrPriv = listarParametros.ResplistaTablaSunat.DescRespuesta; 
                }

            

                return listarParametros;
                
            }
            catch (Exception ex)
            {
                
                throw ex;
            }

        }
     

 
    }
}