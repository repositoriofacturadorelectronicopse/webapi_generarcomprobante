/****************************************************************************************************************************************************************************************
 PROGRAMA: GenerarComprobante.cs
 VERSION : 1.0
 OBJETIVO: Clase generar comprobante
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic;
using AppConfiguracion;
using CEN;
using CLN;
using System.Text;

namespace AppServicioDoc
{
    public partial class GenerarComprobante
    {
        
        public GenerarComprobante()
        {
            //DESCRIPCION: CONSTRUCTOR DE CLASE GENERARCOMPROBANTE
        }


        public DocumentoSunat CrearDocumentoElectronico(DocumentoSunat oDocumento, ref int codRspta, ref string msjeRspta , ref bool success, ref string comprobante, ref string msjRptaError, ref string ruc_emisor, ref string estado_comprobante)
        {
            //DESCRIPCION: FUNCION CREAR DOCUMENTO ELECTRONICO PARA FACTURA, BOLETA, NOTA DE CRÉDITO Y NOTÁ DE DÉBITO
            
            string rutabase, rutadoc;                                   // ruta base y ruta de documento
            string nombreArchivoXml     = EN_Constante.g_const_vacio;   // nombre del archivo xml
            string valorResumen         = EN_Constante.g_const_vacio;   // valor resumen
            string firma                = EN_Constante.g_const_vacio;   // firma
            string mensaje              = EN_Constante.g_const_vacio;   // mensaje
            string cadenaXML            = EN_Constante.g_const_vacio;   // cadenaXML
            EN_Empresa empresaDocumento = new EN_Empresa();             // Entidad empresa
            EN_Certificado certificadoDocumento = new EN_Certificado(); // entidad certificado
            NE_Empresa nE_Empresa = new NE_Empresa();                   // Clase negocio empresa
            EN_Parametro res_par = new EN_Parametro();                  // clase entidad parametro para resultado
            EN_Parametro bus_par = new EN_Parametro();                  // clase entidad parametro para búsqueda
            
                var withBlock = oDocumento;         //documento
                try
                {
                    if (GenerarXML.ValidarUsuarioEmpresa(oDocumento.Idempresa, oDocumento.Usuario, oDocumento.Clave, oDocumento.Id, oDocumento.Idpuntoventa))
                    {
                        empresaDocumento = GetEmpresaById(withBlock.Idempresa, withBlock.Idpuntoventa, withBlock.Id);
                        
                        //buscamos en tabla parametro si enviamos en linea las boletas
                        bus_par.par_conceptopfij=EN_Constante.g_const_1;
                        bus_par.par_conceptocorr=EN_Constante.g_const_9;
                        bus_par.par_int1        =oDocumento.Idempresa;
                        bus_par.par_int2        =oDocumento.Idpuntoventa;
                        res_par =nE_Empresa.buscar_tablaParametro(bus_par);
                        empresaDocumento.enviarxmlBoletas = (res_par.par_descripcion=="" || res_par.par_descripcion==null)?"NO":res_par.par_descripcion;  

                        //certificado
                        certificadoDocumento = GetCertificadoByEmpresaId(empresaDocumento.Id , oDocumento.Id, oDocumento.Idpuntoventa);
                        oDocumento.NroDocEmpresa = empresaDocumento.Nrodocumento;
                        ruc_emisor=  empresaDocumento.Nrodocumento;

                        rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;

                        rutadoc = string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                        //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
                        AppConfiguracion.FuncionesS3.InitialS3(empresaDocumento.Nrodocumento,certificadoDocumento.Nombre,empresaDocumento.Imagen);

                        EN_ComprobanteSunat oCompSunat = buildComprobante();

                        if (withBlock.Idtipodocumento.Trim() == oCompSunat.IdFC || withBlock.Idtipodocumento == oCompSunat.IdBV || withBlock.Idtipodocumento == oCompSunat.IdNC || withBlock.Idtipodocumento == oCompSunat.IdND)
                        {
                            if (GuardarDocumentoSunat(oDocumento))
                            {
                                NE_Documento obj = new NE_Documento();
                                var objDocumento = new EN_Documento();
                                List<EN_Documento> listDocumentos;
                                objDocumento.Id = withBlock.Id;
                                objDocumento.Idempresa = withBlock.Idempresa;
                                objDocumento.Idtipodocumento = withBlock.Idtipodocumento;
                                objDocumento.Estado = EN_Constante.g_const_estado_nuevo;
                                objDocumento.Situacion = EN_Constante.g_const_situacion_pendiente;
                                objDocumento.Serie = EN_Constante.g_const_vacio;
                                objDocumento.Numero = EN_Constante.g_const_vacio;
                                objDocumento.CadenaAleatoria = EN_Constante.g_const_vacio;
                                objDocumento.UsuarioSession = (withBlock.UsuarioSession=="" || withBlock.UsuarioSession==null)?EN_Constante.g_const_UsuarioSession:withBlock.UsuarioSession;
                                listDocumentos = (List<EN_Documento>)obj.listarDocumento(objDocumento);

                                listDocumentos[EN_Constante.g_const_0].Descuentoafectabase = oDocumento.Descuentoafectabase;
                               
                                if (GenerarXML.GenerarXmlComprobanteElectronico21(listDocumentos[EN_Constante.g_const_0], oCompSunat, empresaDocumento, certificadoDocumento, rutabase, rutadoc,ref nombreArchivoXml, ref valorResumen, ref firma,ref  mensaje, ref cadenaXML))
                                {
                                   
                                    if (GenerarXML.GuardarNombreValorResumenFirma(listDocumentos[EN_Constante.g_const_0], nombreArchivoXml, valorResumen, firma, cadenaXML))
                                    {
                                        withBlock.NombreFichero = nombreArchivoXml;
                                        withBlock.ValorResumen = valorResumen;
                                        withBlock.ValorFirma = firma;
                                        codRspta = 0;
                                        msjeRspta = mensaje;
                                        // Generamos el documento en PDF
                                        GeneraPdfDocumentoPago(listDocumentos[EN_Constante.g_const_0], oCompSunat, empresaDocumento, valorResumen, firma, rutabase, rutadoc);
                                    }

                                    //ACA VA VALIDACION XSD Y XSL


                                    //VALIDACION PARA ENVIO EN LINEA EL XML 
                                    if( (empresaDocumento.enviarxml== EN_Constante.g_const_si && oDocumento.Idtipodocumento== EN_ConfigConstantes.Instance.const_IdFC ) 
                                        || (empresaDocumento.enviarxml== EN_Constante.g_const_si && oDocumento.Idtipodocumento== EN_ConfigConstantes.Instance.const_IdNC && oDocumento.Idtipodocumentonota == Convert.ToInt32(EN_ConfigConstantes.Instance.const_IdFC) )
                                        || (empresaDocumento.enviarxml== EN_Constante.g_const_si && oDocumento.Idtipodocumento== EN_ConfigConstantes.Instance.const_IdND && oDocumento.Idtipodocumentonota == Convert.ToInt32(EN_ConfigConstantes.Instance.const_IdFC) )
                                        || (empresaDocumento.enviarxmlBoletas== EN_Constante.g_const_si && oDocumento.Idtipodocumento== EN_ConfigConstantes.Instance.const_IdBV )
                                        || (empresaDocumento.enviarxmlBoletas== EN_Constante.g_const_si && oDocumento.Idtipodocumento== EN_ConfigConstantes.Instance.const_IdNC && oDocumento.Idtipodocumentonota == Convert.ToInt32(EN_ConfigConstantes.Instance.const_IdBV) )
                                        || (empresaDocumento.enviarxmlBoletas== EN_Constante.g_const_si && oDocumento.Idtipodocumento== EN_ConfigConstantes.Instance.const_IdND && oDocumento.Idtipodocumentonota == Convert.ToInt32(EN_ConfigConstantes.Instance.const_IdBV) ))
                                        {
                                                string mensajeWA=EN_Constante.g_const_vacio;
                                                GenerarXML.EnviarXmlDocumentoElectronico(listDocumentos[EN_Constante.g_const_0], certificadoDocumento, oCompSunat, empresaDocumento.Produccion,  rutadoc, nombreArchivoXml, ref mensajeWA, ref codRspta, ref estado_comprobante);
                                                if(codRspta==1){
                                                    string[] msjWA = mensajeWA.Split('|');
                                                    msjeRspta = mensaje + " - "+msjWA[0]; 
                                                    
                                                    msjRptaError = msjWA[1];
                                                }else{
                                                    msjeRspta = mensaje + " - "+mensajeWA;   
                                                    msjRptaError = "";          
                                                    
                                                }
                                        
                                        }
                                       
                                     success=true;
                                    comprobante= listDocumentos[EN_Constante.g_const_0].Serie   +   "-" +   listDocumentos[EN_Constante.g_const_0].Numero;
                                }
                                else
                                {
                                    withBlock.NombreFichero = EN_Constante.g_const_vacio;
                                    withBlock.ValorResumen = EN_Constante.g_const_vacio;
                                    withBlock.ValorFirma = EN_Constante.g_const_vacio;
                                    codRspta = EN_Constante.g_const_1; // Se transfirio el documento pero no generó XML
                                    msjeRspta = mensaje; 
                                     success=false;
                                    // Se elimina el documento de la base de datos
                                    obj.eliminarDocumento(objDocumento);
                                }
                            }
                        }
                        else
                        {
                            withBlock.NombreFichero = EN_Constante.g_const_vacio;
                            withBlock.ValorResumen = EN_Constante.g_const_vacio;
                            withBlock.ValorFirma = EN_Constante.g_const_vacio;
                            codRspta = EN_Constante.g_const_1;
                            msjeRspta = EN_Constante.g_const_comprobante_noValido;
                        }
                    }
                    else
                    {
                        withBlock.NombreFichero = EN_Constante.g_const_vacio;
                        withBlock.ValorResumen = EN_Constante.g_const_vacio;
                        withBlock.ValorFirma = EN_Constante.g_const_vacio;
                        codRspta =  EN_Constante.g_const_1;
                        msjeRspta = EN_Constante.g_const_certificado_NoEncontrado;
                    }
                    
                }
                catch (Exception ex)
                {
                    withBlock.NombreFichero = EN_Constante.g_const_vacio;
                    withBlock.ValorResumen = EN_Constante.g_const_vacio;
                    withBlock.ValorFirma = EN_Constante.g_const_vacio;
                    codRspta = EN_Constante.g_const_1000;
                    msjeRspta = EN_Constante.g_const_exc_GenerarComp + ex.Message;
                    success=false;
                
                }
            
           
            return oDocumento;
        }
        public static EN_Certificado GetCertificadoByEmpresaId(int empresaId,string documentoId = "",  int Idpuntoventa = 0)
        {
            //DESCRIPCION: FUNCION PARA OBTENER CERTIFICADO POR ID DE EMPRESA
            EN_Certificado oCertif = new EN_Certificado();  // CLASE ENTIDAD CERTIFICADO
            NE_Certificado objCertif = new NE_Certificado();    // CLASE DE NEGOCIO DE CERTIFICADO
            EN_Certificado listaCertificado;        // CLASE DE ENTIDAD CERTIFICADO

            oCertif.Id = 0;
            oCertif.IdEmpresa = empresaId;
            oCertif.Nombre = EN_Constante.g_const_vacio;
            oCertif.UsuarioSession = EN_Constante.g_const_vacio;
            oCertif.CadenaAleatoria = EN_Constante.g_const_vacio;
            oCertif.IdPuntoVenta= Idpuntoventa;
            listaCertificado = (EN_Certificado)objCertif.listarCertificado(oCertif, documentoId, Idpuntoventa);
            return listaCertificado;
        }
        public static EN_Empresa GetEmpresaById(int idEmpresa, int Idpuntoventa, string documentoId)
        {
            //DESCRIPCION: FUNCION OBTIENE DATOS DE EMPRESA POR ID

            EN_Empresa listaEmpresa;                    // CLASE ENTIDAD EMPRESA
            EN_Empresa oEmpresa = new EN_Empresa();     // CLASE ENTIDAD EMPRESA
            NE_Empresa objEmpresa = new NE_Empresa();   // CLASENEGOCIO DE EMPRESA
            oEmpresa.Id = idEmpresa;
            oEmpresa.Nrodocumento = EN_Constante.g_const_vacio;
            oEmpresa.RazonSocial = EN_Constante.g_const_vacio;
            oEmpresa.Estado = EN_Constante.g_const_vacio;
            listaEmpresa = (EN_Empresa)objEmpresa.listaEmpresa(oEmpresa, Idpuntoventa,  documentoId);
            return listaEmpresa;
        }
        public void GeneraPdfDocumentoPago(EN_Documento oDocumento, EN_ComprobanteSunat oCompSunat, EN_Empresa empresaDocumento, string valorResumen, string firma, string rutabase, string rutadoc)
        {
            //DESCRIPCION: CREAR PDF DE DOCUMENTO DE PAGO

            rutadoc=rutadoc+ EN_Constante.g_const_rutaSufijo_pdf ;
            // Validamos si existe directorio del documento, de lo contrario lo creamos
            if ((!System.IO.Directory.Exists(rutadoc)))
                System.IO.Directory.CreateDirectory(rutadoc);
                
            EN_DocReferencia oDocumentoRef = new EN_DocReferencia();
            GenerarPDF creaPdf = new GenerarPDF();
            List<EN_DocReferencia> detalleNcndDocRefencia;
            NE_Documento objDocumentoRef = new NE_Documento();
            NumeroALetras numeroALetras = new NumeroALetras();
            string montoenletras;
            string monedaenletras;
            List<EN_DetalleDocumento> detalleDoc ;
            NE_Documento objProceso = new NE_Documento();
            NE_Facturacion objFacturacion = new NE_Facturacion();
            detalleDoc = (List<EN_DetalleDocumento>)objProceso.listarDetalleDocumento(oDocumento);

            // Listamos las guias de remision
            EN_DocRefeGuia oDocRefeGuia = new EN_DocRefeGuia();
            List<EN_DocRefeGuia> listaDocRefGuias;
            string strGuias = EN_Constante.g_const_vacio;
            oDocRefeGuia.empresa_id = oDocumento.Idempresa;
            oDocRefeGuia.documento_id = oDocumento.Id;
            oDocRefeGuia.tipodocumento_id = oDocumento.Idtipodocumento;
            listaDocRefGuias = (List<EN_DocRefeGuia>)objDocumentoRef.listarDocRefeGuia(oDocRefeGuia, oDocumento.Id, oDocumento.Idempresa);
            if (listaDocRefGuias.Count > EN_Constante.g_const_0)
            {
                StringBuilder bld = new StringBuilder();
                foreach (var item2 in listaDocRefGuias.ToList())
                {
                     bld.Append(item2.numero_guia + ",");
                }
                    strGuias = bld.ToString();
            }
            if (strGuias.Trim() != EN_Constante.g_const_vacio)
                strGuias = strGuias.Substring(0, Strings.Len(strGuias) - EN_Constante.g_const_1);
            else
                strGuias = EN_Constante.g_const_vacio;

            // Lista de Documentos anticipados
            string strAnticipos = EN_Constante.g_const_vacio;
            if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_1.ToString())
            {
                EN_DocAnticipo oDocumentoAnt = new EN_DocAnticipo();
                List<EN_DocAnticipo> listaAnticipos;
                oDocumentoAnt.empresa_id = oDocumento.Idempresa;
                oDocumentoAnt.documento_id = oDocumento.Id;
                oDocumentoAnt.tipodocumento_id = oDocumento.Idtipodocumento;
                listaAnticipos = (List<EN_DocAnticipo>)objDocumentoRef.listarDocumentoAnticipo(oDocumentoAnt, oDocumento.Id, oDocumento.Idpuntoventa);

                if (listaAnticipos.Count > EN_Constante.g_const_0)
                {
                    StringBuilder bld = new StringBuilder();
                    foreach (var item3 in listaAnticipos.ToList())
                    {
                         bld.Append(item3.nrodocanticipo + ",");
                    }
                        strAnticipos =  bld.ToString();;
                }
                if (strAnticipos.Trim() != EN_Constante.g_const_vacio)
                    strAnticipos = strAnticipos.Substring(0, Strings.Len(strAnticipos) - 1);
                else
                    strAnticipos = EN_Constante.g_const_vacio;
            }

            string symbmoneda;
            symbmoneda = objFacturacion.ObtenerCodigoAlternoPorCodigoSistema("002", oDocumento.Moneda);
            montoenletras = numeroALetras.Convertir(oDocumento.Importefinal);
            monedaenletras = objFacturacion.ObtenerDescripcionPorCodElemento("002", oDocumento.Moneda);

            //LEYENDA PARA IVAP
            if (oDocumento.text_aux_05.Trim() == EN_Constante.g_const_vacio && oDocumento.es_ivap =="SI")
            {
                  
                 oDocumento.text_aux_05= objFacturacion.ObtenerDescripcionPorCodElemento("052", "2007");
            }else{
                  if (oDocumento.text_aux_05.Trim() != EN_Constante.g_const_vacio && oDocumento.es_ivap =="SI")
                  {
                    oDocumento.text_aux_05=objFacturacion.ObtenerDescripcionPorCodElemento("052", "2007")+" - "+ oDocumento.text_aux_05.Trim();
                  }
                  else{
                    oDocumento.text_aux_05= oDocumento.text_aux_05.Trim();
                  }                  
            }


            if (oDocumento.Idtipodocumento.Trim() == oCompSunat.IdFC || oDocumento.Idtipodocumento == oCompSunat.IdBV)
            {
                string Tipodoccliente = this.getTipodoccliente(oDocumento.Tipodoccliente);
                creaPdf.GenerarPDFBoletaFactura(empresaDocumento, oDocumento, montoenletras, monedaenletras, detalleDoc, oCompSunat, valorResumen, firma, rutabase, rutadoc, symbmoneda, strGuias, strAnticipos, Tipodoccliente);
            }
               
            else if (oDocumento.Idtipodocumento == oCompSunat.IdNC || oDocumento.Idtipodocumento == oCompSunat.IdND)
            {
                oDocumentoRef.Idempresa = oDocumento.Idempresa;
                oDocumentoRef.Iddocumento = oDocumento.Id;
                oDocumentoRef.Idtipodocumento = oDocumento.Idtipodocumento;
                detalleNcndDocRefencia = (List<EN_DocReferencia>)objDocumentoRef.listarDocumentoReferencia(oDocumentoRef, oDocumento.Id, oDocumento.Idempresa);
                creaPdf.GenerarPDFNotaCreditoDebito(empresaDocumento, oDocumento, montoenletras, monedaenletras, detalleDoc, oCompSunat, valorResumen, firma, rutabase, rutadoc, symbmoneda, detalleNcndDocRefencia);
            }

       
                           
        }
        private static EN_ComprobanteSunat buildComprobante()
        {
            //DESCRIPCION: FUNCION DE SETEO DE VARIABLES DE COMPROBANTE
            var oCompSunat = new EN_ComprobanteSunat();

            oCompSunat.IdFC = EN_ConfigConstantes.Instance.const_IdFC;
            oCompSunat.IdBV = EN_ConfigConstantes.Instance.const_IdBV;
            oCompSunat.IdNC = EN_ConfigConstantes.Instance.const_IdNC;
            oCompSunat.IdND = EN_ConfigConstantes.Instance.const_IdND;
            oCompSunat.IdGR = EN_ConfigConstantes.Instance.const_IdGR;

            oCompSunat.IdCB =  EN_ConfigConstantes.Instance.const_IdCB;
            oCompSunat.IdRD = EN_ConfigConstantes.Instance.const_IdRD;
            oCompSunat.IdRR =  EN_ConfigConstantes.Instance.const_IdRR;

            oCompSunat.IdCP = EN_ConfigConstantes.Instance.const_IdCP;
            oCompSunat.IdCR =  EN_ConfigConstantes.Instance.const_IdCR;

            oCompSunat.codigoEtiquetaErrorDoc = EN_ConfigConstantes.Instance.const_codigoEtiquetaErrorDoc;
            return oCompSunat;
        }
        public static bool GuardarDocumentoSunat(DocumentoSunat documentoSunat)
        {
            //DESCRIPCION: FUNCION PARA LLAMAR GUARDAR DOCUMENTO SUNAT

            NE_GeneracionService negocioGeneracionService = new NE_GeneracionService();     // CLASE DE NEGOCIO GENERACIONSERVICE
            return negocioGeneracionService.GuardarDocumentoSunat(documentoSunat);
        }

        public string getTipodoccliente(string Tipodoccliente)
        { 
            //DESCRIPCION: FUNCION PARA OBTENER EL NOMBRE DOCUMENTO DE CLIENTE

            NE_Facturacion objFacturacion = new NE_Facturacion(); // CLASE DE NEGOCIO DE FACTURACION
            string result;
            
                try
                {
                    if (Tipodoccliente.Length == EN_Constante.g_const_1)
                    {
                        result = objFacturacion.ObtenerCodigoElementoPorCodigoSistema("006",Tipodoccliente);
                    }
                    else
                    {
                        result = Tipodoccliente;
                    }
                return result;
                }
                catch (System.Exception ex)
                {
                    
                    throw ex;
                }

        }

        public BajaSunat CrearBajaElectronica(BajaSunat oBaja, ref int codRspta, ref string msjeRspta, ref string comprobante, ref bool success)
        {
            //DESCRIPCION: RESUMEN DE BAJAS

            
            var obj = new NE_Baja();                                // CLASE DE NEGOCIO BAJA
            var objBaja = new EN_Baja();                            // CLASE ENTIDAD BAJA
            var listaBajas = new List<EN_Baja>();                   // LISTA DE CLASE ENTIDAD BAJA
            var empresaDocumento = new EN_Empresa();                // CLASE ENTIDAD EMPRESA
            var certificadoDocumento = new EN_Certificado();        // CLASE ENTIDAD CERTIFICADO
            var creaPdf = new GenerarPDF();                         // CLASE GENERAR PDF
            var detalleBaja = new List<EN_DetalleBaja>();           // LISTA DE CLASE ENTIDAD DETALLE BAJA
            ResumenSunat oResDiario = new ResumenSunat();           // CLASE DE RESUMEN SUNAT
            NE_Resumen objNegResumen = new NE_Resumen();            // CLASE DE NEGOCIO RESUMEN
            int idbaja = 0;                                         // ID DE BAJA
            int idresumen=0;                                        // ID DE RESUMEN
            string mensaje = "";                                    // MENSAJE
            string cadenaXML = "";                                  // CADENA XML
            string rutabase, rutadocs;                              // RUTA BASE Y RUTA DE DOCUMENTOS
            string nombreArchivoXml = "";                           // NOMBRE DE ARCHIVO XML
            string valorResumen = "";                               // VALOR DEL RESUMEN
            string firma = "";                                      // FIRMA

       
       
            try
            {
                if (GenerarXML.ValidarUsuarioEmpresa(oBaja.Empresa_id, oBaja.Usuario, oBaja.Clave, EN_Constante.g_const_vacio ,oBaja.Idpuntoventa))
                {
                    empresaDocumento = GetEmpresaById(oBaja.Empresa_id, EN_Constante.g_const_0, EN_Constante.g_const_vacio );
                    certificadoDocumento = GetCertificadoByEmpresaId(empresaDocumento.Id);

                    
                    rutabase= EN_ConfigConstantes.Instance.const_rutaTemporalPse_archivos;
                    rutadocs = string.Format("{0}/{1}/", rutabase, empresaDocumento.Nrodocumento);

                    //COMPROBAMOS CARPETAS Y ARCHIVOS INICIALES  DE S3 Y LOCAL TEMPORAL
                    AppConfiguracion.FuncionesS3.InitialS3(empresaDocumento.Nrodocumento,certificadoDocumento.Nombre,empresaDocumento.Imagen);

                    EN_ComprobanteSunat oCompSunat = buildComprobante();

                    if (oBaja.Tiporesumen.Trim() == oCompSunat.IdCB)
                    {
                        idbaja = GuardarBajaSunat(oBaja);
                       
                        objBaja.Id = idbaja;
                        objBaja.Empresa_id = oBaja.Empresa_id;
                        objBaja.Estado = EN_Constante.g_const_estado_nuevo;
                        objBaja.Situacion = EN_Constante.g_const_situacion_pendiente;
                        objBaja.Correlativo = EN_Constante.g_const_0;
                        objBaja.CadenaAleatoria = EN_Constante.g_const_vacio;
                        objBaja.UsuarioSession = EN_Constante.g_const_vacio;
                        listaBajas = (List<EN_Baja>)obj.listarBajas(objBaja);
                        if (listaBajas.Count > EN_Constante.g_const_0)
                        {
                            if (GenerarXML.GenerarXmlBaja(listaBajas[EN_Constante.g_const_0], oCompSunat, empresaDocumento, certificadoDocumento, rutadocs, rutabase, ref nombreArchivoXml, ref valorResumen, ref firma, ref mensaje, ref cadenaXML))
                            {
                                if (GenerarXML.GuardarBajaNombreValorResumenFirma(listaBajas[EN_Constante.g_const_0], nombreArchivoXml, valorResumen, firma, cadenaXML))
                                {
                                    oBaja.NombreFichero = nombreArchivoXml;
                                    oBaja.ValorResumen = valorResumen;
                                    oBaja.ValorFirma = firma;
                                    codRspta = EN_Constante.g_const_0;
                                    msjeRspta = mensaje;
                                    // Generamos el documento en PDF
                                    detalleBaja = (List<EN_DetalleBaja>)obj.listarDetalleBaja(listaBajas[EN_Constante.g_const_0]);
                                    creaPdf.GenerarPDFComunicadoBaja(empresaDocumento, listaBajas[EN_Constante.g_const_0], detalleBaja, rutadocs, oCompSunat, valorResumen, firma, rutabase);
                                }
                                comprobante = listaBajas[EN_Constante.g_const_0].Identificacomunicacion;
                                success=true;
                            }
                           
                            else
                            {
                                oBaja.NombreFichero = EN_Constante.g_const_vacio;
                                oBaja.ValorResumen = EN_Constante.g_const_vacio;
                                oBaja.ValorFirma = EN_Constante.g_const_vacio;
                                codRspta = EN_Constante.g_const_1;
                                msjeRspta = mensaje;
                            }
                        }
                        else
                        {
                            oBaja.NombreFichero = EN_Constante.g_const_vacio;
                            oBaja.ValorResumen = EN_Constante.g_const_vacio;
                            oBaja.ValorFirma = EN_Constante.g_const_vacio;
                            codRspta = EN_Constante.g_const_1;
                            msjeRspta = "No se encuentra el comunicado de bajas generado.";
                        }
                    }

                    else if(oBaja.Tiporesumen.Trim() == oCompSunat.IdRD)
                    {
                         
                                oResDiario.Empresa_id = oBaja.Empresa_id;
                                oResDiario.Tiporesumen = oBaja.Tiporesumen;
                                oResDiario.FechaGeneraResumen = oBaja.FechaComunicacion;
                                oResDiario.FechaEmisionDocumento = oBaja.FechaDocumento;
                                oResDiario.Estado = EN_Constante.g_const_estado_nuevo;
                                oResDiario.Situacion = EN_Constante.g_const_situacion_pendiente;
                                oResDiario.UsuarioSession = oBaja.UsuarioSession;
                                oResDiario.Usuario = oBaja.Usuario;
                                oResDiario.Clave = oBaja.Clave;
                                oResDiario.EstadoResumen= EN_Constante.g_const_estadoResumen_baja.ToString();
                                  List<EN_Documento> listaDocumentos = new List<EN_Documento>();

                                foreach (var itemDD in oBaja.ListaDetalleBaja)
                                {

                                    string IdDocumento = itemDD.Tipodocumentoid.ToString().Trim()+itemDD.Serie.ToString().Trim()+"-"+itemDD.Numero.ToString().Trim();   
                                    listaDocumentos= ( List<EN_Documento> )objNegResumen.listarDocumentosBajaResumen(IdDocumento,oBaja.Empresa_id, oBaja.Idpuntoventa);
                                        if (listaDocumentos.Count > 0)
                                        {

                                             for (var i = 0; i <= listaDocumentos.Count - 1; i++)
                                            {
                                                EN_DetalleResumen oDetalle = new EN_DetalleResumen();
                                                {
                                                    //verificamos la situación del comprobante
                                                    if(listaDocumentos[i].Situacion==EN_Constante.g_const_situacion_pendiente)
                                                    {
                                                        msjeRspta="El comprobante "+listaDocumentos[i].Serie+"-"+listaDocumentos[i].Numero+
                                                                " está pendiente de envío. Se registró su baja, se creará la baja posterior a enviarse.";
                                                        oBaja.NombreFichero = "";
                                                        oBaja.ValorResumen = "";
                                                        oBaja.ValorFirma = "";
                                                        codRspta = EN_Constante.g_const_1;
                                                        comprobante="";
                                                        

                                                        GuardarResumenBajaPendiente( oBaja, ref success);

                                                        return oBaja;

                                                    }
                                                    if(listaDocumentos[i].Situacion==EN_Constante.g_const_situacion_rechazado || listaDocumentos[i].Situacion==EN_Constante.g_const_situacion_error )
                                                    {
                                                        msjeRspta="El comprobante "+listaDocumentos[i].Serie+"-"+listaDocumentos[i].Numero+
                                                                " está con situación Rechazado o Error";
                                                        oBaja.NombreFichero = "";
                                                        oBaja.ValorResumen = "";
                                                        oBaja.ValorFirma = "";
                                                        codRspta = EN_Constante.g_const_menos1;
                                                        success=false;
                                                        return oBaja;

                                                    }


                                                    var withBlock = oDetalle;
                                                    string tipdoccli = "0";
                                                    if (listaDocumentos[i].Tipodoccliente.Length == 1)
                                                        tipdoccli = listaDocumentos[i].Tipodoccliente;
                                                    else if (listaDocumentos[i].Tipodoccliente == "RUC")
                                                        tipdoccli = "6";
                                                    else if (listaDocumentos[i].Tipodoccliente == "DNI")
                                                        tipdoccli = "1";
                                                    else if (listaDocumentos[i].Tipodoccliente == "CAE")
                                                        tipdoccli = "4";
                                                    else if (listaDocumentos[i].Tipodoccliente == "PAS")
                                                        tipdoccli = "7";
                                                    else if (listaDocumentos[i].Tipodoccliente == "CDI")
                                                        tipdoccli = "A";
                                                    else if (listaDocumentos[i].Tipodoccliente == "DPR")
                                                        tipdoccli = "B";
                                                    else if (listaDocumentos[i].Tipodoccliente == "TAX")
                                                        tipdoccli = "C";
                                                    else if (listaDocumentos[i].Tipodoccliente == "IND")
                                                        tipdoccli = "D";
                                                    else
                                                        tipdoccli = "0";
                                                    withBlock.Tipodocumentoid = listaDocumentos[i].Idtipodocumento;
                                                    withBlock.Serie = listaDocumentos[i].Serie;
                                                    withBlock.Correlativo = listaDocumentos[i].Numero;
                                                    withBlock.DocCliente = tipdoccli;
                                                    withBlock.NroDocCliente = listaDocumentos[i].Nrodoccliente;
                                                    withBlock.Condicion = 1;
                                                    withBlock.Moneda = listaDocumentos[i].Moneda;
                                                    withBlock.Opegravadas = (decimal)listaDocumentos[i].Valorpergravadas;
                                                    withBlock.Opeexoneradas = (decimal)listaDocumentos[i].Valorperexoneradas;
                                                    withBlock.Opeinafectas = (decimal)listaDocumentos[i].Valorperinafectas;
                                                    withBlock.Opeexportacion = (decimal)listaDocumentos[i].Valorperexportacion;
                                                    withBlock.Opegratuitas = (decimal)listaDocumentos[i].Valorpergratuitas;
                                                    withBlock.Otroscargos = (decimal)listaDocumentos[i].OtrosCargos;
                                                    withBlock.TotalIGV = (decimal)listaDocumentos[i].Igvventa;
                                                    withBlock.TotalISC = (decimal)listaDocumentos[i].Iscventa;
                                                    withBlock.Otrostributos = (decimal)listaDocumentos[i].OtrosTributos;
                                                    withBlock.Importeventa = (decimal)listaDocumentos[i].Importeventa;
                                                    withBlock.Regimenpercep = (string)listaDocumentos[i].Regimenpercep;
                                                    withBlock.Porcentpercep = (decimal)listaDocumentos[i].Porcentpercep;
                                                    withBlock.Importepercep = (decimal)listaDocumentos[i].Importepercep;
                                                    withBlock.Importefinal = (decimal)listaDocumentos[i].Importefinal;
                                                 
                                                }
                                                oResDiario.ListaDetalleResumen.Add(oDetalle);
                                            }
                                            
                                        }

                                }



                        idresumen= objNegResumen.GuardarResumenSunat(oResDiario);
                        
                        oResDiario.Id = idresumen;
                        oResDiario.Empresa_id = oBaja.Empresa_id;
                        oResDiario.Estado = EN_Constante.g_const_estado_nuevo;
                        oResDiario.Situacion = EN_Constante.g_const_situacion_pendiente;
                        oResDiario.Correlativo = 0;
                        oResDiario.CadenaAleatoria = EN_Constante.g_const_vacio;
                        oResDiario.UsuarioSession = EN_Constante.g_const_vacio;
                        oResDiario.FechaIni=EN_Constante.g_const_vacio;
                        oResDiario.FechaFin=EN_Constante.g_const_vacio;

                         List<EN_Resumen> listaResumens = new List<EN_Resumen>();    // lista de clase resumen
                         
                         listaResumens = (List<EN_Resumen>) objNegResumen.listarResumen(oResDiario);

                        if (listaResumens.Count > EN_Constante.g_const_0)
                        {
                            if (GenerarXML.GenerarXmlResumen(listaResumens[EN_Constante.g_const_0], oCompSunat, empresaDocumento, certificadoDocumento, rutadocs, rutabase, ref nombreArchivoXml, ref valorResumen, ref firma, ref mensaje,ref cadenaXML))
                            {
                                if (GenerarXML.GuardarResumenNombreValorResumenFirma(listaResumens[EN_Constante.g_const_0], nombreArchivoXml, valorResumen, firma, cadenaXML))
                                {
                                    oBaja.NombreFichero = nombreArchivoXml;
                                    oBaja.ValorResumen = valorResumen;
                                    oBaja.ValorFirma = firma;
                                    codRspta = 0;
                                    msjeRspta = mensaje;
                                }

                                comprobante = listaResumens[EN_Constante.g_const_0].IdentificaResumen;
                                success=true;
                            }
                            else
                            {
                                oBaja.NombreFichero = "";
                                oBaja.ValorResumen = "";
                                oBaja.ValorFirma = "";
                                codRspta = 1;
                                msjeRspta = mensaje;
                            }
                        }
                    }

                    else
                    {
                        oBaja.NombreFichero = "";
                        oBaja.ValorResumen = "";
                        oBaja.ValorFirma = "";
                        codRspta = EN_Constante.g_const_1;
                        msjeRspta = "El codigo de baja a generar no es válido ("+oCompSunat.IdCB +" , "+ oCompSunat.IdRD+").";
                    }
                }
                else
                {
                    oBaja.NombreFichero = "";
                    oBaja.ValorResumen = "";
                    oBaja.ValorFirma = "";
                    codRspta = EN_Constante.g_const_1;
                    msjeRspta = "No se ha encontrado el usuario del certificado de la empresa ingresada";
                }

            }
            catch (Exception ex)
            {
                 success=false;
                oBaja.NombreFichero = "";
                oBaja.ValorResumen = "";
                oBaja.ValorFirma = "";
                codRspta = EN_Constante.g_const_1;
                msjeRspta = "Hubo una Excepción al generar el comunicado de bajas:" + ex.Message;
            }

            return oBaja;
        }

        public int GuardarBajaSunat(BajaSunat BajaSunat)
        {
            var negocioGeneracionService = new NE_GeneracionService();
            return negocioGeneracionService.GuardarBajaSunat(BajaSunat);
        }


        public void GuardarResumenBajaPendiente(BajaSunat oBaja, ref bool success)
        {

            NE_GeneracionService negocioGeneracionService = new NE_GeneracionService();
            try
            {
                success= negocioGeneracionService.GuardarResumenBajaPendiente(oBaja);    
            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
            

        }
        
    
        
    }

}