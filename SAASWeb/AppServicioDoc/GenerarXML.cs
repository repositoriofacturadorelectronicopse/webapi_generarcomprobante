/****************************************************************************************************************************************************************************************
 PROGRAMA: GenerarXml.cs
 VERSION : 1.0
 OBJETIVO: Clase generar xml
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using CEN;
using CLN;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualBasic;
using AppConfiguracion;
using System.Xml;
using Newtonsoft.Json;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;
using Microsoft.VisualBasic.CompilerServices;
using System.Text;
using System.Globalization;

namespace AppServicioDoc
{
     public static class GenerarXML
    {
        
 

    // /*************************INI VERSION UBL 2.1*****************************/
    // Esta función genera el comprobante electrónico de los datos guardados en la BD
    // Devuelve como valores "nombre", "vResumen" y "vFirma" 
    // --- nombre: es el nombre del archivo XML
    // --- vResumen: es el valor Resumen (campo DigestValue del XML generado)
    // --- vFirma: es el valor de la firma digital adjunta en el XML (campo SignatureValue del XML generado)
    
   
    public static bool GenerarXmlComprobanteElectronico21(EN_Documento oDocumento, EN_ComprobanteSunat oCompSunat, EN_Empresa oEmpresa, EN_Certificado oCertificado, string rutabase, string rutadoc, ref string nombre, ref string vResumen, ref string vFirma, ref string msjRspta, ref string cadenaXML)
    {
        // DESCRIPCION:  // Funcion para Generar XML del comprobante en formato UBL 2.1

        // Create XmlWriterSettings.
        XmlWriterSettings settings = new XmlWriterSettings();

        string prefixSac;
        string prefixCac;
        string prefixCbc;
        string prefixExt;
        string prefixDs;
        string cadSac = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1";
        string cadCac = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2";
        string cadCbc = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2";
        string cadUdt = "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2";
        string cadCcts = "urn:un:unece:uncefact:documentation:2";
        string cadExt = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2";
        string cadQdt = "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2";
        string cadDs = "http://www.w3.org/2000/09/xmldsig#";
        string cadSLoc = @"urn:oasis:names:specification:ubl:schema:xsd:Invoice-2 ..\XSD\UBL21\maindoc\UBLPE-Invoice-2.1.xsd";
        string cadXsi = "http://www.w3.org/2001/XMLSchema-instance";

        NE_Documento objProceso = new NE_Documento();
        string moneda;
        string TipDoc;
        List<EN_DetalleDocumento> listaDetalle;
        string rutaemp;
        try
        {
            rutaemp = string.Format("{0}/{1}/", rutabase, oDocumento.NroDocEmpresa);
           
            // Validamos si existe directorio del documento, de lo contrario lo creamos
            // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
            if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml)))
                System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml);

            if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf)))
                System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf);

             if ((!System.IO.Directory.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr)))
                System.IO.Directory.CreateDirectory(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr);

            listaDetalle = (List<EN_DetalleDocumento>)objProceso.listarDetalleDocumento(oDocumento);
            TipDoc = oDocumento.Idtipodocumento.Trim();
            moneda = oDocumento.Moneda.Trim();
            nombre = oEmpresa.Nrodocumento.Trim() + "-" + TipDoc.Trim() + "-" + oDocumento.Serie.Trim() + "-" + oDocumento.Numero.Trim();

            // Validamos si existe comprobante en XML, ZIP y PDF generado y eliminamos
            if (File.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml + nombre.Trim() + EN_Constante.g_const_extension_xml))
                File.Delete(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml + nombre.Trim() + EN_Constante.g_const_extension_xml);

            if (File.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml + nombre.Trim() + EN_Constante.g_const_extension_zip))
                File.Delete(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_xml + nombre.Trim() + EN_Constante.g_const_extension_zip);

            if (File.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf + nombre.Trim() + EN_Constante.g_const_extension_pdf))
                File.Delete(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_pdf + nombre.Trim() + EN_Constante.g_const_extension_pdf);
            // Validamos si existe CDR en XML y ZIP y eliminamos
            if (File.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_xml))
                File.Delete(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_xml);
            if (File.Exists(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip))
                File.Delete(rutadoc.Trim() + EN_Constante.g_const_rutaSufijo_cdr + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);

            // Create XmlWriter.
            settings.Encoding = new System.Text.UTF8Encoding(false);
            settings.Indent = true;
            
            rutadoc=rutadoc + EN_Constante.g_const_rutaSufijo_xml;

            using (XmlWriter writer = System.Xml.XmlWriter.Create(rutadoc.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml, settings))
            {
                prefixSac = writer.LookupPrefix(cadSac);
                prefixCac = writer.LookupPrefix(cadCac);
                prefixCbc = writer.LookupPrefix(cadCbc);
                prefixExt = writer.LookupPrefix(cadExt);
                prefixDs = writer.LookupPrefix(cadDs);
                // Begin writing.
                writer.WriteStartDocument();
                if (oDocumento.Idtipodocumento == oCompSunat.IdFC || oDocumento.Idtipodocumento == oCompSunat.IdBV)
                    writer.WriteStartElement("Invoice", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2");
                else if (oDocumento.Idtipodocumento == oCompSunat.IdNC)
                    writer.WriteStartElement("CreditNote", "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2");
                else if (oDocumento.Idtipodocumento == oCompSunat.IdND)
                    writer.WriteStartElement("DebitNote", "urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2");
                writer.WriteAttributeString("xmlns", "sac", null, cadSac);
                writer.WriteAttributeString("xmlns", "cac", null, cadCac);
                writer.WriteAttributeString("xmlns", "cbc", null, cadCbc);
                writer.WriteAttributeString("xmlns", "udt", null, cadUdt);
                writer.WriteAttributeString("xmlns", "ccts", null, cadCcts);
                writer.WriteAttributeString("xmlns", "ext", null, cadExt);
                writer.WriteAttributeString("xmlns", "qdt", null, cadQdt);
                writer.WriteAttributeString("xmlns", "ds", null, cadDs);
                writer.WriteAttributeString("xmlns", "schemaLocation", null, cadSLoc);
                writer.WriteAttributeString("xmlns", "xsi", null, cadXsi);
                // writer.WriteAttributeString("xmlns", "xsd", Nothing, cadXsd)

                ObtenerExtensionesComprobante21(writer, prefixExt, prefixSac, prefixCac, prefixCbc, prefixDs, cadExt, cadSac, cadCac, cadCbc, cadDs, oDocumento, moneda.Trim(), oCompSunat);
                if (oDocumento.Idtipodocumento == oCompSunat.IdBV)
                {
                    ObtenerDatosCabeceraBoleta21(writer, prefixCbc, prefixCac, cadCbc, cadCac, oDocumento, listaDetalle.Count, oEmpresa, TipDoc, moneda.Trim(), oCompSunat);
                    if (listaDetalle.Count > EN_Constante.g_const_0)
                    {
                        foreach (var item in listaDetalle.ToList())
                            ObtenerDatosDetalleBoleta21(writer, prefixCbc, prefixCac, cadCbc, cadCac, item, moneda.Trim(), oDocumento);
                    }
                    else
                        throw new ArgumentNullException("No se encontró detalle en la boleta de venta");
                }
                else if (oDocumento.Idtipodocumento == oCompSunat.IdFC)
                {
                    ObtenerDatosCabeceraFactura21(writer, prefixCbc, prefixCac, cadCbc, cadCac, oDocumento,  oEmpresa, TipDoc, moneda.Trim());
                    if (listaDetalle.Count > EN_Constante.g_const_0)
                    {
                        foreach (var item in listaDetalle.ToList())
                            ObtenerDatosDetalleFactura21(writer, prefixCbc, prefixCac, cadCbc, cadCac, item, moneda.Trim(), oDocumento);
                    }
                    else
                        throw new ArgumentNullException("No se encontró detalle en la factura");
                }
                else if (oDocumento.Idtipodocumento == oCompSunat.IdNC)
                {
                    ObtenerDatosCabeceraNotaCredito21(writer, prefixCbc, prefixCac, cadCbc, cadCac, oDocumento, oEmpresa, moneda.Trim());
                    if (listaDetalle.Count > EN_Constante.g_const_0)
                    {
                        foreach (var item in listaDetalle.ToList())
                            ObtenerDatosDetalleNotaCredito21(writer, prefixCbc, prefixCac, cadCbc, cadCac, item, moneda.Trim(), oDocumento);
                    }
                    else
                        throw new ArgumentNullException("No se encontró detalle en la nota de crédito");
                }
                else if (oDocumento.Idtipodocumento == oCompSunat.IdND)
                {
                    ObtenerDatosCabeceraNotaDebito21(writer, prefixCbc, prefixCac, cadCbc, cadCac, oDocumento, oEmpresa, moneda.Trim());
                    if (listaDetalle.Count > EN_Constante.g_const_0)
                    {
                        foreach (var item in listaDetalle.ToList())
                            ObtenerDatosDetalleNotaDebito21(writer, prefixCbc, prefixCac, cadCbc, cadCac, item, moneda.Trim(), oDocumento);
                    }
                    else
                        throw new ArgumentNullException("No se encontró detalle en la nota de débito");
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
                writer.Close();
            }

            int numExtension;

            numExtension = 1;

            AgregarFirma21(oCertificado, rutadoc, rutaemp, nombre, ref vResumen, ref vFirma, TipDoc, oCompSunat, numExtension, ref cadenaXML, oDocumento.Id, oDocumento.Idempresa);
            msjRspta = "Comprobante Generado Correctamente";

            //COPIAMOS XML A S3 Y ELIMINAMOS DE TEMPORAL
            AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oDocumento.NroDocEmpresa,rutabase,EN_Constante.g_const_rutaSufijo_xml,nombre.Trim(),EN_Constante.g_const_extension_xml);

            return true;
        }
        catch (Exception ex)
        {
            // En caso de error eliminamos el XML generado
            if (File.Exists(rutadoc.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml))
                File.Delete(rutadoc.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml);
            

            msjRspta = "Error al generar Comprobante: " + ex.Message.ToString();
            return false;
        }
    }

    private static XmlWriter ObtenerExtensionesComprobante21(XmlWriter writer, string prefixExt, string prefixSac, string prefixCac, string prefixCbc, string prefixDs, string cadExt, string cadSac, string cadCac, string cadCbc, string cadDs, EN_Documento oDocumento, string moneda, EN_ComprobanteSunat oCompSunat)
    {
        // DESCRIPCION: Funcion para completar los contenedores del XML en formato UBL 2.1
        
        // 1. Firma Digital.
        writer.WriteStartElement(prefixExt, "UBLExtensions", cadExt);
        writer.WriteStartElement(prefixExt, "UBLExtension", cadExt);
        writer.WriteStartElement(prefixExt, "ExtensionContent", cadExt);
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        return writer;
    }

   
    private static XmlWriter ObtenerDatosCabeceraBoleta21(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_Documento oDocumento, int cantidadItems, EN_Empresa oEmpresa, string TipDoc, string CodMon, EN_ComprobanteSunat oCompSunat)
    {
         // DESCRIPCION: Funcion para generar los datos de la cabecera del XML en formato UBL 2.1 -- BOLETA DE VENTA

        NE_Facturacion objFacturacion = new NE_Facturacion();       // CLASE NEGOCIO FACTURACION
        NE_DocPersona objDocPersona = new NE_DocPersona();          // CLASE NEGOCIO DOCPERSONA
        EN_DocPersona oDocPersona = new EN_DocPersona();            // CLASE ENTIDAD DOCPERSONA
        List<EN_DocPersona> listaDocPersona ;                        // LISTA DE CLASE ENTIDAD DOCPERSONA
        string tipodoccliente_codsunat;     // TIPO DE DOCUMENTO DE CLIENTE
        string nrodocumentocliente;         // NUMERO DE DOCUMENTO DE CLIENTE
        NE_Documento objDocumentoRef = new NE_Documento();      // CLASE NEGOCIO DOCUMENTO
        EN_DocAnticipo oDocumentoAnt = new EN_DocAnticipo();    // CLASE ENTIDAD DOCANTICIPO
        EN_DocRefeGuia oDocRefeGuia = new EN_DocRefeGuia();     // CLASE ENTIDAD DOCREFEGUIA
        string MontoLetras;         // MONTO EN LETRAS
        NumeroALetras numeroALetras = new NumeroALetras();      // NUMERO A LETRAS

        // Listamos el codigo del tipo de documento de la empresa
        oDocPersona.Codigo = oEmpresa.Tipodocumento;
        oDocPersona.Estado = EN_Constante.g_const_vacio;
        oDocPersona.Indpersona = EN_Constante.g_const_vacio;
        listaDocPersona = (List<EN_DocPersona>)objDocPersona.listarDocPersona(oDocPersona, oDocumento.Id, oDocumento.Idpuntoventa);

        // Listamos el código del tipo de documento del cliente y su número de documento
        if (oDocumento.Tipodoccliente.Trim() == EN_Constante.g_const_vacio)
        {
            tipodoccliente_codsunat = EN_Constante.g_const_0.ToString();
            nrodocumentocliente = "-";
        }
        else if (Strings.Trim(oDocumento.Tipodoccliente).Length == 1)
        {
            tipodoccliente_codsunat = oDocumento.Tipodoccliente.Trim();
            nrodocumentocliente = oDocumento.Nrodoccliente.Trim();
        }
        else
        {
            tipodoccliente_codsunat = objFacturacion.ObtenerCodigoSunat("006", oDocumento.Tipodoccliente.Trim());
            nrodocumentocliente = oDocumento.Nrodoccliente.Trim();
        }

        // Versión del UBL.
        writer.WriteElementString("UBLVersionID", cadCbc, "2.1");

        // Versión de la estructura del documento.
        writer.WriteElementString("CustomizationID", cadCbc, "2.0");

        // Numeración, conformada por serie y número correlativo.
        writer.WriteElementString("ID", cadCbc, oDocumento.Serie.Trim() + "-" + oDocumento.Numero.Trim());

        // Fecha de emisión.
        writer.WriteElementString("IssueDate", cadCbc, Convert.ToDateTime(oDocumento.Fecha).ToString(EN_Constante.g_const_formfechaGuion));

        // Hora de emisión.
         writer.WriteElementString("IssueTime", cadCbc, Convert.ToDateTime(oDocumento.Hora).ToString(EN_Constante.g_const_formhora));

        // Fecha de Vencimiento
        if (oDocumento.Fechavencimiento.Trim() != EN_Constante.g_const_vacio)
            writer.WriteElementString("DueDate", cadCbc, Convert.ToDateTime(oDocumento.Fechavencimiento).ToString(EN_Constante.g_const_formfechaGuion));

        // Código de Tipo de documento.
        writer.WriteStartElement(prefixCbc, "InvoiceTypeCode", cadCbc);
        writer.WriteAttributeString("listID", oDocumento.TipoOperacion);
        writer.WriteValue(TipDoc.Trim());
        writer.WriteEndElement();

        // Leyendas.
        // Leyenda de Monto en Letras
        MontoLetras = numeroALetras.Convertir(oDocumento.Importefinal);
        writer.WriteStartElement(prefixCbc, "Note", cadCbc);
        writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "1000"));
        writer.WriteCData(MontoLetras);
        writer.WriteEndElement();

         //PARA IVAP
        if(oDocumento.es_ivap=="SI") 
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2007"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2007"));
            writer.WriteEndElement();
        }

        // Leyenda de Transferencia Gratuita
        if ((oDocumento.Estransgratuita == EN_Constante.g_const_si))
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "1002"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "1002"));
            writer.WriteEndElement();
        }

        // Leyenda de COMPROBANTE DE PERCEPCION
        if (oDocumento.Importepercep > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2000"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2000"));
            writer.WriteEndElement();
        }

        // Leyenda de Bienes transferidos de la amazonia
        if (oDocumento.BienTransfAmazonia == EN_Constante.g_const_1.ToString())
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2001"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2001"));
            writer.WriteEndElement();
        }

        // Leyenda de servicios transferidos de la amazonia
        if (oDocumento.ServiTransfAmazonia == EN_Constante.g_const_1.ToString())
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2002"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2002"));
            writer.WriteEndElement();
        }

        // Leyenda de contratos de construccion en la amazonia
        if (oDocumento.ContratoConstAmazonia == EN_Constante.g_const_1.ToString())
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2003"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2003"));
            writer.WriteEndElement();
        }

        // Leyenda Agencia de Viaje - Paquete Turístico

        // Leyenda de Venta Itinerante

        // Tipo de moneda.
        writer.WriteStartElement(prefixCbc, "DocumentCurrencyCode", cadCbc);
        writer.WriteValue(oDocumento.Moneda);
        writer.WriteEndElement();

        // Número de la orden de compra
        if (oDocumento.OrdenCompra.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "OrderReference", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteCData(oDocumento.OrdenCompra);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Documentos Relacionados al comprobante
        // Tipo y número de la guía de remisión relacionada con la operación
        // Listamos las guias de remision
        List<EN_DocRefeGuia> listaDocRefGuias;
        oDocRefeGuia.empresa_id = oDocumento.Idempresa;
        oDocRefeGuia.documento_id = oDocumento.Id;
        oDocRefeGuia.tipodocumento_id = oDocumento.Idtipodocumento;
        listaDocRefGuias = (List<EN_DocRefeGuia>)objDocumentoRef.listarDocRefeGuia(oDocRefeGuia, oDocumento.Id, oDocumento.Idempresa);
        if (listaDocRefGuias.Count > EN_Constante.g_const_0)
        {
            foreach (var item2 in listaDocRefGuias.ToList())
            {
                writer.WriteStartElement(prefixCac, "DespatchDocumentReference", cadCac);
                writer.WriteElementString("ID", cadCbc, item2.numero_guia.Trim());
                writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
                writer.WriteValue(item2.tipo_guia.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }
        else if (oDocumento.TipoGuiaRemision.Trim() != EN_Constante.g_const_vacio && oDocumento.GuiaRemision.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "DespatchDocumentReference", cadCac);
            writer.WriteElementString("ID", cadCbc, oDocumento.GuiaRemision.Trim());
            writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
            writer.WriteValue(oDocumento.TipoGuiaRemision.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }


        // Tipo y número de otro documento y/ código documento relacionado con la operación
        if (oDocumento.TipoDocOtroDocRef.Trim() != EN_Constante.g_const_vacio && oDocumento.OtroDocumentoRef.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "AdditionalDocumentReference", cadCac);
            writer.WriteElementString("ID", cadCbc, oDocumento.OtroDocumentoRef.Trim());
            writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
            writer.WriteValue(oDocumento.TipoDocOtroDocRef.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_1.ToString())
        // if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_si.ToString())
        {
            List<EN_DocAnticipo> listaAnticipos;
            oDocumentoAnt.empresa_id = oDocumento.Idempresa;
            oDocumentoAnt.documento_id = oDocumento.Id;
            oDocumentoAnt.tipodocumento_id = oDocumento.Idtipodocumento;
            listaAnticipos = (List<EN_DocAnticipo>)objDocumentoRef.listarDocumentoAnticipo(oDocumentoAnt, oDocumento.Id, oDocumento.Idempresa);
            if (listaAnticipos.Count > EN_Constante.g_const_0)
            {
                  
                foreach (var item2 in listaAnticipos.ToList())
                {
                     
                    writer.WriteStartElement(prefixCac, "AdditionalDocumentReference", cadCac);
                    writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                    writer.WriteValue(item2.nrodocanticipo.ToString());
                    writer.WriteEndElement();
                    writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
                    writer.WriteValue((item2.tipodocanticipo.ToString()=="01")?"02":"03");
                    writer.WriteEndElement();
                    writer.WriteStartElement(prefixCbc, "DocumentStatusCode", cadCbc);
                    writer.WriteAttributeString("listName", "Anticipo");
                    // writer.WriteValue(item2.nrodocanticipo.ToString());
                     writer.WriteValue(item2.line_id.ToString());

                    writer.WriteEndElement();
                    writer.WriteStartElement(prefixCac, "IssuerParty", cadCac);
                    writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
                    writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                    writer.WriteAttributeString("schemeID", item2.tipodocemisor.Trim());
                    writer.WriteValue(item2.nrodocemisor.ToString());
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
            }
        }

        // Datos de la empresa
        // RUC y Razon Social
        writer.WriteStartElement(prefixCac, "Signature", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.SignatureID.Trim());
        writer.WriteStartElement(prefixCac, "SignatoryParty", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.Nrodocumento.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "PartyName", cadCac);
        writer.WriteStartElement(prefixCbc, "Name", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "DigitalSignatureAttachment", cadCac);
        writer.WriteStartElement(prefixCac, "ExternalReference", cadCac);
        writer.WriteElementString("URI", cadCbc, oEmpresa.SignatureURI.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        // RUC
        writer.WriteStartElement(prefixCac, "AccountingSupplierParty", cadCac);
        writer.WriteStartElement(prefixCac, "Party", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteAttributeString("schemeID", listaDocPersona[0].CodigoSunat.Trim());
        writer.WriteValue(oEmpresa.Nrodocumento.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        // Nombre Comercial
        if ((oEmpresa.Nomcomercial.Trim() != EN_Constante.g_const_vacio))
        {
            writer.WriteStartElement(prefixCac, "PartyName", cadCac);
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(oEmpresa.Nomcomercial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
        // Razon Social
        writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "RegistrationAddress", cadCac);
        // Ubigeo
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteCData(oEmpresa.Coddis.Trim());
        writer.WriteEndElement();
        // Código de Establecimiento
        writer.WriteElementString("AddressTypeCode", cadCbc, oDocumento.CodigoEstablecimiento);
        // Provincia
        writer.WriteStartElement(prefixCbc, "CityName", cadCbc);
        writer.WriteCData(oEmpresa.Provincia.Trim());
        writer.WriteEndElement();
        // Departamento
        writer.WriteStartElement(prefixCbc, "CountrySubentity", cadCbc);
        writer.WriteCData(oEmpresa.Departamento.Trim());
        writer.WriteEndElement();
        // Distrito
        writer.WriteStartElement(prefixCbc, "District", cadCbc);
        writer.WriteCData(oEmpresa.Distrito.Trim());
        writer.WriteEndElement();
        // Dirección
        writer.WriteStartElement(prefixCac, "AddressLine", cadCac);
        writer.WriteStartElement(prefixCbc, "Line", cadCbc);
        writer.WriteCData(oEmpresa.Direccion.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        // Código de País
        writer.WriteStartElement(prefixCac, "Country", cadCac);
        writer.WriteElementString("IdentificationCode", cadCbc, oEmpresa.Codpais.Trim());
        writer.WriteEndElement();

        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Lugar en el que se entrega el bien
        // Listamos la guía asociada al documento
        EN_FacturaGuia oDocGuia = new EN_FacturaGuia();     // CLASE ENTIDAD FACTURAGUIA
        NE_Documento objDoc = new NE_Documento();           //CLASE NEGOCIO DOCUMENTO
        List<EN_FacturaGuia> listaGuia;                 // LISTA DE CLASE ENTIDAD FACTURAGUIA
        oDocGuia.documento_id = oDocumento.Id;
        oDocGuia.empresa_id = oDocumento.Idempresa;
        oDocGuia.tipodocumento_id = oDocumento.Idtipodocumento;
        listaGuia = (List<EN_FacturaGuia>)objDoc.listarDocumentoGuia(oDocGuia,oDocumento.Id, oDocumento.Idempresa);
        if (listaGuia.Count > EN_Constante.g_const_0)
        {
            if (oDocumento.TipoOperacion == "0201" || oDocumento.TipoOperacion == "0208")
            {
                // Pais del uso, explotación o aprovechamiento del servicio
                writer.WriteStartElement(prefixCac, "Delivery", cadCac);
                writer.WriteStartElement(prefixCac, "DeliveryLocation", cadCac);
                writer.WriteStartElement(prefixCac, "Address", cadCac);
                writer.WriteStartElement(prefixCac, "Country", cadCac);
                writer.WriteStartElement(prefixCbc, "IdentificationCode", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_pais.ToString());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
            else
            {
                writer.WriteStartElement(prefixCac, "Delivery", cadCac);
                writer.WriteStartElement(prefixCac, "DeliveryLocation", cadCac);
                writer.WriteStartElement(prefixCac, "Address", cadCac);

                // Ubigeo de entrega
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_distrito.ToString());
                writer.WriteEndElement();
                // Direccion de entrega
                writer.WriteStartElement(prefixCac, "AddressLine", cadCac);
                writer.WriteStartElement(prefixCbc, "Line", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_direccion.ToString());
                writer.WriteEndElement();
                writer.WriteEndElement();
                // Urbanizacion de entrega
                writer.WriteStartElement(prefixCbc, "CitySubdivisionName", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_urbanizacion.ToString());
                writer.WriteEndElement();
                // Departamento de entrega
                writer.WriteStartElement(prefixCbc, "CountrySubentity", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_departamentodesc.ToString());
                writer.WriteEndElement();
                // Provincia de entrega
                writer.WriteStartElement(prefixCbc, "CityName", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_provinciadesc.ToString());
                writer.WriteEndElement();
                // Distrito
                writer.WriteStartElement(prefixCbc, "District", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_distritodesc.ToString());
                writer.WriteEndElement();
                // País
                writer.WriteStartElement(prefixCac, "Country", cadCac);
                writer.WriteStartElement(prefixCbc, "IdentificationCode", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_pais.ToString());
                writer.WriteEndElement();
                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }

        // Cliente
        writer.WriteStartElement(prefixCac, "AccountingCustomerParty", cadCac);
        writer.WriteStartElement(prefixCac, "Party", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteAttributeString("schemeID", tipodoccliente_codsunat.Trim());
        writer.WriteValue(nrodocumentocliente);
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
        writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
        writer.WriteCData(oDocumento.Nombrecliente.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Anticipos
        // Datos de Anticipo -- Tabla docanticipo
        // Serie y número de comprobante del anticipo (para el caso de reorganización de empresas, incluye el RUC)
        // Código de tipo de documento
        // Monto prepagado o anticipado
        // Código de tipo de moneda del monto prepagado o anticipado
        // Número de RUC del emisor del comprobante de anticipo

        if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_1.ToString())
        // if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_si.ToString())
        {
            List<EN_DocAnticipo> listaAnticipos;
            oDocumentoAnt.empresa_id = oDocumento.Idempresa;
            oDocumentoAnt.documento_id = oDocumento.Id;
            oDocumentoAnt.tipodocumento_id = oDocumento.Idtipodocumento;
            listaAnticipos = (List<EN_DocAnticipo>)objDocumentoRef.listarDocumentoAnticipo(oDocumentoAnt, oDocumento.Id, oDocumento.Idpuntoventa);
            if (listaAnticipos.Count > EN_Constante.g_const_0)
            {
                foreach (var item2 in listaAnticipos.ToList())
                {
                    
                    writer.WriteStartElement(prefixCac, "PrepaidPayment", cadCac);
                    writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                    writer.WriteAttributeString("schemeName", "Anticipo");
                    // writer.WriteValue(item2.nrodocanticipo.ToString());
                    writer.WriteValue(item2.line_id.ToString());
                    writer.WriteEndElement();
                    writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
                    writer.WriteAttributeString("currencyID", CodMon);
                    writer.WriteValue(item2.montoanticipo);
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
            }
        }

        // Calculos de Descuentos Cargos Impuestos y Totales
        decimal totalOperaciones;
        decimal totalImpuestos;
        decimal totalImpGratuitos;
        decimal factorDscto;
        decimal factorCargo;
        decimal totalDescuentos;
        decimal totalCargos;

        if(oDocumento.Descuento > EN_Constante.g_const_0 && oDocumento.Descuentoafectabase=="SI")
        {
            oDocumento.Valorpergravadas=oDocumento.Importeventa;
        }

        totalOperaciones = (decimal)(oDocumento.Valorpergravadas + oDocumento.Valorperinafectas + oDocumento.Valorperexoneradas + oDocumento.Valorperexportacion);
        totalImpuestos = (decimal)(oDocumento.Igvventa + oDocumento.Iscventa + oDocumento.OtrosTributos);
        totalImpGratuitos = (decimal)(oDocumento.SumaIgvGratuito + oDocumento.SumaIscGratuito);

        factorCargo = (decimal)(Math.Round(oDocumento.OtrosCargos / (double)100, 5));
        factorDscto = (decimal)(Math.Round(oDocumento.Descuento / (double)100, 5));
        totalDescuentos = (totalOperaciones + totalImpuestos) * factorDscto;
        totalCargos = (totalOperaciones + totalImpuestos) * factorCargo;

        

        if (oDocumento.Descuento > EN_Constante.g_const_0)
        {

             string afectaBaseDesc = (oDocumento.Descuentoafectabase=="SI")? objFacturacion.ObtenerCodigoSunat("053", "02"): objFacturacion.ObtenerCodigoSunat("053", "03");

            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "false");
            // writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "03"));
            writer.WriteElementString("AllowanceChargeReasonCode",cadCbc, afectaBaseDesc );
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (factorDscto).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalDescuentos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Otros Cargos ' Se han considerado que no afectan a la base imponible
        if (oDocumento.OtrosCargos > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "50"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (factorCargo).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalCargos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Percepcion 'Cargo que no afecta la base imponible
        if (oDocumento.Importepercep > EN_Constante.g_const_0)
        {
            decimal importeBasePercep;
            // importeBasePercep = totalOperaciones + totalImpuestos - totalDescuentos + totalCargos;
            importeBasePercep = totalOperaciones - totalDescuentos + totalCargos;
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");

            if (oDocumento.Regimenpercep == "03")
                writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "53"));
            else if (oDocumento.Regimenpercep == "02")
                writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "52"));
            else
                writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "51"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (Math.Round(oDocumento.Porcentpercep / (double)100, 5)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue(oDocumento.Importepercep.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((importeBasePercep).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // TaxTotal - Monto Total de Impuestos
        writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue((totalImpuestos).ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // --- Operaciones Gravadas IGV -- 1000
        if ((oDocumento.Valorpergravadas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            // writer.WriteValue((oDocumento.Valorpergravadas).ToString("F2").Replace(",", "."));

            if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_1.ToString())
            {
                List<EN_DocAnticipo> listaAnticipos;
            oDocumentoAnt.empresa_id = oDocumento.Idempresa;
            oDocumentoAnt.documento_id = oDocumento.Id;
            oDocumentoAnt.tipodocumento_id = oDocumento.Idtipodocumento;
            listaAnticipos =( List<EN_DocAnticipo>) objDocumentoRef.listarDocumentoAnticipo(oDocumentoAnt, oDocumento.Id, oDocumento.Idpuntoventa);
                double sumaAnticipos=0;
                double valueOper =0;
                foreach (var item2 in listaAnticipos.ToList())
                {
                    sumaAnticipos= sumaAnticipos + Convert.ToDouble(item2.montoanticipo);
                }
                valueOper = oDocumento.Valorpergravadas - (sumaAnticipos/1.18);
                // valueOper = oDocumento.Valorpergravadas ;

                 writer.WriteValue(valueOper.ToString("F2").Replace(",", "."));
            }
            else{
                writer.WriteValue(oDocumento.Valorpergravadas.ToString("F2").Replace(",", "."));
            }


            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Igvventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);

            //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteValue("1016");
            else                            writer.WriteValue("1000");

            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);

            //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "1016"));
            else                            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "1000"));

            writer.WriteEndElement();

               //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "1016"));
            else                            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "1000"));

            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones Gravadas ISC -- 2000
        if ((oDocumento.Iscventa > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperiscreferenc.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Iscventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("2000");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "2000"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "2000"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones INAFECTAS FRE -- 9998
        if ((oDocumento.Valorperinafectas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((oDocumento.Valorperinafectas).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9998");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9998"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9998"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones EXONERADAS VAT -- 9997
        if ((oDocumento.Valorperexoneradas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperexoneradas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9997");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9997"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9997"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones EXPORTACIÓN FRE -- 9995 --- SI ES GRATUITO -- 9996
        if ((oDocumento.Valorperexportacion > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperexportacion.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteValue("9996");
            else
                writer.WriteValue("9995");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9996"));
            else
                writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9995"));
            writer.WriteEndElement();
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9996"));
            else
                writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9995"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        // --- Operaciones GRATUITO FRE -- 9996
        if ((oDocumento.Valorpergratuitas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorpergratuitas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(totalImpGratuitos);
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9996");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9996"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9996"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones Gravadas OTH -- 9999
        if ((oDocumento.OtrosTributos > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorpergravadas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.OtrosTributos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9999");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9999"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9999"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        writer.WriteEndElement();

        // LegalMonetaryTotal
        writer.WriteStartElement(prefixCac, "LegalMonetaryTotal", cadCac);

        // Total Valor de Venta (Sin Descuento Global, Sin IGV -- ISC -- Otros Tributos, Sin Otros Cargos)
        writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue(totalOperaciones.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // // Total Impuestos = IGV + ISC + Otros Tributos
        // writer.WriteStartElement(prefixCbc, "TaxExclusiveAmount", cadCbc);
        // writer.WriteAttributeString("currencyID", CodMon);
        // writer.WriteValue(totalImpuestos.ToString("F2").Replace(",", "."));
        // writer.WriteEndElement();

        // // Total Precio de Venta (ValorVenta + Impuestos) --- Impuestos = IGV + ISC + Otros Tributos
        // writer.WriteStartElement(prefixCbc, "TaxInclusiveAmount", cadCbc);
        // writer.WriteAttributeString("currencyID", CodMon);
        // writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
        // writer.WriteEndElement();

        // agregado
        if (oDocumento.IndicaAnticipo.Trim() != EN_Constante.g_const_1.ToString() || oDocumento.IndicaAnticipo.Trim()=="" || oDocumento.IndicaAnticipo.Trim()==null)
        {
             // Total Impuestos = IGV + ISC + Otros Tributos
            writer.WriteStartElement(prefixCbc, "TaxExclusiveAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(totalImpuestos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();

            // Total Precio de Venta (ValorVenta + Impuestos) --- Impuestos = IGV + ISC + Otros Tributos
            writer.WriteStartElement(prefixCbc, "TaxInclusiveAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }
        else{
             // Total Precio de Venta (ValorVenta + Impuestos) --- Impuestos = IGV + ISC + Otros Tributos
            writer.WriteStartElement(prefixCbc, "TaxInclusiveAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((Math.Round(totalOperaciones * Convert.ToDecimal(1.18),2)).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // Total de Descuentos (que no afectan la base imponible)
        if (totalDescuentos > EN_Constante.g_const_0 && oDocumento.Descuentoafectabase=="NO")
        {
            writer.WriteStartElement(prefixCbc, "AllowanceTotalAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((totalDescuentos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // Total de Cargos (que no afectan la base imponible)
        if (totalCargos > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "ChargeTotalAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(totalCargos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // **. Total de Anticipos
        if (oDocumento.Importeanticipo > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "PrepaidAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Importeanticipo.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // 34. Importe total de la venta, cesión en uso o del servicio prestado
        writer.WriteStartElement(prefixCbc, "PayableAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue(oDocumento.Importefinal.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        writer.WriteEndElement();

        return writer;
    }

    private static void ObtenerDatosDetalleBoleta21(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_DetalleDocumento listaDetalle, string moneda, EN_Documento oDocumento)
    {
        // DESCRIPCION: Funcion para generar los datos del detalle del XML en formato UBL 2.1 -- BOLETA DE VENTA
        
        NE_Facturacion objFacturacion = new NE_Facturacion();   // CLASE NEGOCIO FACTURACION

        // Datos del item unidad, cantidad, precio_unitario, valorventa
        writer.WriteStartElement(prefixCac, "InvoiceLine", cadCac);
        // 35. Número de orden del Ítem
        writer.WriteElementString("ID", cadCbc, listaDetalle.Lineid.ToString());

        // 36. Cantidad y Unidad de medida por ítem 
        writer.WriteStartElement(prefixCbc, "InvoicedQuantity", cadCbc);
        writer.WriteAttributeString("unitCode", listaDetalle.Unidad.ToString());

        // 36. Cantidad de unidades por ítem
        writer.WriteValue((Math.Round(listaDetalle.Cantidad, 10)).ToString().Replace(",", "."));
        writer.WriteEndElement();

        // 37. Valor de venta del ítem
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valorventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }
        else
        {
            // Operacion gratuita
            writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // 38. Precio de venta unitario por item y código
        // 39. Valor referencial unitario por ítem en operaciones no onerosas
        writer.WriteStartElement(prefixCac, "PricingReference", cadCac);
        // Validamos si el valor venta es mayor a 0 no es gratuito
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "AlternativeConditionPrice", cadCac);
            // Precio Unitario
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Preciounitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            // Código
            writer.WriteStartElement(prefixCbc, "PriceTypeCode", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerCodigoSunat("016", "001").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        else
        {
            // Es gratuito Precioreferencial
            writer.WriteStartElement(prefixCac, "AlternativeConditionPrice", cadCac);
            // Precio Unitario
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Valorunitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            // Código
            writer.WriteStartElement(prefixCbc, "PriceTypeCode", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerCodigoSunat("016", "002").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();

        // 40. Descuentos / Cargos por Item
        decimal valbruto;
        if (listaDetalle.Valorbruto > EN_Constante.g_const_0)
            valbruto = (decimal)listaDetalle.Valorbruto;
        else
            valbruto = (decimal)((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario);

        if (listaDetalle.Valordscto > EN_Constante.g_const_0)
        {
            // Descuentos que afectan la base imponible
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "false");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "00"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (Math.Round(listaDetalle.Valordscto / (double)valbruto, 5)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valordscto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(valbruto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        if (listaDetalle.Valorcargo > EN_Constante.g_const_0)
        {
            // Cargos que afectan la base imponible
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "47"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (Math.Round(listaDetalle.Valorcargo / (double)valbruto, 5)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valorcargo.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(valbruto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        decimal montoTributo;
        decimal valorventa;
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            valorventa = (decimal)((decimal)listaDetalle.Valorventa + (decimal)listaDetalle.Isc);
            montoTributo = (decimal)(listaDetalle.Igv + listaDetalle.Isc);
        }
        else
        {
            // Operacion Gratuita
            valorventa = (decimal)((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario);
            montoTributo = 0;
        }

        writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(montoTributo.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // 42. Afectación al IGV por ítem
        string tipoafect;
        // 10 -- Gravado Operación Onerosa
        // 11, 12, 13, 14, 15, 16 -- Gravado Retiro - Bonificaciones (TG)
        // 20 -- Exonerado Operación Onerosa
        // 21 -- Exonerado Transferencia gratuita
        // 30 -- Inafecto Operación Onerosa
        // 31, 32, 33, 34, 35, 36, 37 -- Inafecto Retiro - TG
        // 40 -- Exportaciones

        switch (listaDetalle.Afectacionigv.Trim())
        {
            case "10":
                {
                    tipoafect = "1000";
                    break;
                }

            case "11":
            case "12":
            case "13":
            case "14":
            case "15":
            case "16":
                {
                    tipoafect = "9996";
                    break;
                }

              //IVAP

            case "17":
                {
                    tipoafect = "1016";
                    break;
                }

            case "20":
                {
                    tipoafect = "9997";
                    break;
                }

            case "21":
                {
                    tipoafect = "9996";
                    break;
                }

            case "30":
                {
                    tipoafect = "9998";
                    break;
                }

            case "31":
            case "32":
            case "33":
            case "34":
            case "35":
            case "36":
            case "37":
                {
                    tipoafect = "9996";
                    break;
                }

            case "40":
                {
                    if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                        tipoafect = "9996";
                    else
                        tipoafect = "9995";
                    break;
                }

            default:
                {
                    tipoafect = "1000";
                    break;
                }
        }

        writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(valorventa.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(listaDetalle.Igv.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
        writer.WriteElementString("Percent", cadCbc, (Math.Round(listaDetalle.factorIgv, 5)).ToString().Replace(",", "."));
        writer.WriteStartElement(prefixCbc, "TaxExemptionReasonCode", cadCbc);
        writer.WriteValue(listaDetalle.Afectacionigv.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteValue(tipoafect);
        writer.WriteEndElement();
        writer.WriteElementString("Name", cadCbc, objFacturacion.ObtenerDescripcionPorCodElemento("005", tipoafect).Trim());
        writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", tipoafect).Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();


        // 43. Afectación ISC por ítem
        if ((listaDetalle.Isc > EN_Constante.g_const_0))
        {
            decimal montobaseisc;
            if (listaDetalle.SistemaISC == "01")
                // Para el tipocalculoisc 01 Sistema al valor
                montobaseisc = (decimal)listaDetalle.Valorventa;
            else if (listaDetalle.SistemaISC == "02")
                // Para el tipocalculoisc 02 Aplicacion al Monto Fijo - Por confirmar formula
                montobaseisc = (decimal)listaDetalle.Valorventa;
            else if (listaDetalle.SistemaISC == "03")
                // Para el tipocalculoisc 03 Sistema de Precio de Venta al Publico
                montobaseisc = (decimal)(listaDetalle.Precioreferencial * listaDetalle.Cantidad);
            else
                montobaseisc = (decimal)listaDetalle.Valorventa;

            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((montobaseisc).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Isc.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteElementString("Percent", cadCbc, (Math.Round(listaDetalle.factorIsc, 5)).ToString().Replace(",", "."));
            writer.WriteElementString("TierRange", cadCbc, listaDetalle.SistemaISC.Trim());
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("2000");
            writer.WriteEndElement();
            writer.WriteElementString("Name", cadCbc, objFacturacion.ObtenerDescripcionPorCodElemento("005", "2000").Trim());
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "2000").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        writer.WriteEndElement();

        writer.WriteStartElement(prefixCac, "Item", cadCac);
        // 44. Descripción detallada del servicio prestado, bien vendido o cedido en uso, indicando las características
        writer.WriteStartElement(prefixCbc, "Description", cadCbc);
        writer.WriteCData(listaDetalle.Producto.Replace(Constants.vbCrLf, " ").Trim());
        writer.WriteEndElement();
        // Código de producto
        if (listaDetalle.Codigo.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "SellersItemIdentification", cadCac);
            writer.WriteElementString("ID", cadCbc, listaDetalle.Codigo.Trim());
            writer.WriteEndElement();
        }
        // Código de producto SUNAT
        if (listaDetalle.CodigoSunat.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "CommodityClassification", cadCac);
            writer.WriteStartElement(prefixCbc, "ItemClassificationCode", cadCbc);
            writer.WriteValue(listaDetalle.CodigoSunat.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();

        // 48. Valor Unitario por Item
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "Price", cadCac);
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Valorunitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        else
        {
            // Gratuito
            writer.WriteStartElement(prefixCac, "Price", cadCac);
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(0);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();
        
    }

    
    private static void ObtenerDatosCabeceraFactura21(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_Documento oDocumento,  EN_Empresa oEmpresa, string TipDoc, string CodMon)
    {
        // DESCRIPCION: Funcion para generar los datos de la cabecera del XML en formato UBL 2.1 -- FACTURA

        NE_Facturacion objFacturacion = new NE_Facturacion();   // CLASE NEGOCIO FACTURACION
        NE_DocPersona objDocPersona = new NE_DocPersona();      // CLASE NEGOCIO DOPERSONA
        EN_DocPersona oDocPersona = new EN_DocPersona();        // CLASE ENTIDAD DOCPERSONA
        List<EN_DocPersona> listaDocPersona;                    // CLASE ENTIDAD DOCPERSONA
        string tipodoccliente_codsunat;                         // TIPO DE DOCUMENTO CLIENTE DE CODIGO SUNAT
        string nrodocumentocliente;                             // NUMERO DE DOCUMENTO DE CLIENTE
        NE_Documento objDocumentoRef = new NE_Documento();      // CLASE NEGOCIO DOCUMENTO
        EN_DocAnticipo oDocumentoAnt = new EN_DocAnticipo();    // CLASE ENTIDAD DOCANTICIPO
        EN_DocRefeGuia oDocRefeGuia = new EN_DocRefeGuia();     // CLASE ENTIDAD DOCREFEGUIA
        string MontoLetras;                                     // MONTO LETRAS
        NumeroALetras numeroALetras = new NumeroALetras();      // NUMERO A LETRAS

        // Listamos el codigo del tipo de documento de la empresa
        oDocPersona.Codigo = oEmpresa.Tipodocumento;
        oDocPersona.Estado = EN_Constante.g_const_vacio;
        oDocPersona.Indpersona = EN_Constante.g_const_vacio;
        listaDocPersona = (List<EN_DocPersona>)objDocPersona.listarDocPersona(oDocPersona,  oDocumento.Id, oDocumento.Idpuntoventa);

        // Listamos el código del tipo de documento del cliente y su número de documento
        if (oDocumento.Tipodoccliente.Trim() == EN_Constante.g_const_vacio)
        {
            tipodoccliente_codsunat = "0";
            nrodocumentocliente = "-";
        }
        else if (Strings.Trim(oDocumento.Tipodoccliente).Length == 1)
        {
            tipodoccliente_codsunat = oDocumento.Tipodoccliente.Trim();
            nrodocumentocliente = oDocumento.Nrodoccliente.Trim();
        }
        else
        {
            tipodoccliente_codsunat = objFacturacion.ObtenerCodigoSunat("006", oDocumento.Tipodoccliente.Trim());
            nrodocumentocliente = oDocumento.Nrodoccliente.Trim();
        }

        // Versión del UBL.
        writer.WriteElementString("UBLVersionID", cadCbc, "2.1");

        // Versión de la estructura del documento.
        writer.WriteElementString("CustomizationID", cadCbc, "2.0");

        // Numeración, conformada por serie y número correlativo.
        writer.WriteElementString("ID", cadCbc, oDocumento.Serie.Trim() + "-" + oDocumento.Numero.Trim());

        // Fecha de emisión.
        writer.WriteElementString("IssueDate", cadCbc, Convert.ToDateTime(oDocumento.Fecha).ToString(EN_Constante.g_const_formfechaGuion));

        // Hora de emisión.
        writer.WriteElementString("IssueTime", cadCbc, Convert.ToDateTime(oDocumento.Hora).ToString(EN_Constante.g_const_formhora));

        // Fecha de Vencimiento
        if (oDocumento.Fechavencimiento.Trim() != EN_Constante.g_const_vacio)
        {
           writer.WriteElementString("DueDate", cadCbc, Convert.ToDateTime(oDocumento.Fechavencimiento).ToString(EN_Constante.g_const_formfechaGuion));
            
        }
            

        // Código de Tipo de documento.
        writer.WriteStartElement(prefixCbc, "InvoiceTypeCode", cadCbc);
        writer.WriteAttributeString("listID", oDocumento.TipoOperacion);
        writer.WriteValue(TipDoc.Trim());
        writer.WriteEndElement();

        // Leyendas.

        // Leyenda de Monto en Letras
        MontoLetras = numeroALetras.Convertir(oDocumento.Importefinal);
        writer.WriteStartElement(prefixCbc, "Note", cadCbc);
        writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "1000"));
        writer.WriteCData(MontoLetras);
        writer.WriteEndElement();

        //PARA IVAP
        if(oDocumento.es_ivap=="SI") 
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2007"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2007"));
            writer.WriteEndElement();
        }


        // Leyenda de Transferencia Gratuita
        if ((oDocumento.Estransgratuita == EN_Constante.g_const_si))
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "1002"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "1002"));
            writer.WriteEndElement();
        }

        // Leyenda de COMPROBANTE DE PERCEPCION
        if (oDocumento.Importepercep > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2000"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2000"));
            writer.WriteEndElement();
        }

        // Leyenda de Bienes transferidos de la amazonia
        if (oDocumento.BienTransfAmazonia == EN_Constante.g_const_1.ToString())
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2001"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2001"));
            writer.WriteEndElement();
        }

        // Leyenda de servicios transferidos de la amazonia
        if (oDocumento.ServiTransfAmazonia == EN_Constante.g_const_1.ToString())
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2002"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2002"));
            writer.WriteEndElement();
        }

        // Leyenda de contratos de construccion en la amazonia
        if (oDocumento.ContratoConstAmazonia == EN_Constante.g_const_1.ToString())
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2003"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2003"));
            writer.WriteEndElement();
        }

        // Leyenda Agencia de Viaje - Paquete Turístico

        // Leyenda de Venta Itinerante

        // Leyenda de operacion de detraccion
            if ((oDocumento.TipoRegimen != EN_Constante.g_const_vacio) && (oDocumento.TipoRegimen == "DTR" || oDocumento.TipoRegimen == "DST"))
            {
                writer.WriteStartElement(prefixCbc, "Note", cadCbc);
                writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2006"));
                writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2006"));
                writer.WriteEndElement();
            }

        // Tipo de moneda.
        writer.WriteStartElement(prefixCbc, "DocumentCurrencyCode", cadCbc);
        writer.WriteValue(oDocumento.Moneda);
        writer.WriteEndElement();



        // Número de la orden de compra
        if (oDocumento.OrdenCompra.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "OrderReference", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteCData(oDocumento.OrdenCompra);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Documentos Relacionados al comprobante
        // Tipo y número de la guía de remisión relacionada con la operación
        // Listamos las guias de remision
        List<EN_DocRefeGuia> listaDocRefGuias;
        oDocRefeGuia.empresa_id = oDocumento.Idempresa;
        oDocRefeGuia.documento_id = oDocumento.Id;
        oDocRefeGuia.tipodocumento_id = oDocumento.Idtipodocumento;
        listaDocRefGuias = (List<EN_DocRefeGuia>)objDocumentoRef.listarDocRefeGuia(oDocRefeGuia, oDocumento.Id, oDocumento.Idempresa);
        if (listaDocRefGuias.Count > EN_Constante.g_const_0)
        {
            foreach (var item2 in listaDocRefGuias.ToList())
            {
                writer.WriteStartElement(prefixCac, "DespatchDocumentReference", cadCac);
                writer.WriteElementString("ID", cadCbc, item2.numero_guia.Trim());
                writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
                writer.WriteValue(item2.tipo_guia.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }
        else if (oDocumento.TipoGuiaRemision.Trim() != EN_Constante.g_const_vacio && oDocumento.GuiaRemision.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "DespatchDocumentReference", cadCac);
            writer.WriteElementString("ID", cadCbc, oDocumento.GuiaRemision.Trim());
            writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
            writer.WriteValue(oDocumento.TipoGuiaRemision.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }


        // Tipo y número de otro documento y/ código documento relacionado con la operación
        if (oDocumento.TipoDocOtroDocRef.Trim() != EN_Constante.g_const_vacio && oDocumento.OtroDocumentoRef.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "AdditionalDocumentReference", cadCac);
            writer.WriteElementString("ID", cadCbc, oDocumento.OtroDocumentoRef.Trim());
            writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
            writer.WriteValue(oDocumento.TipoDocOtroDocRef.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_1.ToString())
        // if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_si.ToString())
        
        {
            List<EN_DocAnticipo> listaAnticipos;
            oDocumentoAnt.empresa_id = oDocumento.Idempresa;
            oDocumentoAnt.documento_id = oDocumento.Id;
            oDocumentoAnt.tipodocumento_id = oDocumento.Idtipodocumento;
            listaAnticipos = (List<EN_DocAnticipo>)objDocumentoRef.listarDocumentoAnticipo(oDocumentoAnt, oDocumento.Id, oDocumento.Idpuntoventa);
            if (listaAnticipos.Count > EN_Constante.g_const_0)
            {
                
                foreach (var item2 in listaAnticipos.ToList())
                {
                
                    writer.WriteStartElement(prefixCac, "AdditionalDocumentReference", cadCac);
                    writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                    writer.WriteValue(item2.nrodocanticipo.ToString());
                    writer.WriteEndElement();
                    writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
                    writer.WriteValue((item2.tipodocanticipo.ToString()=="01")?"02":"03");
                    
                    writer.WriteEndElement();
                    writer.WriteStartElement(prefixCbc, "DocumentStatusCode", cadCbc);
                    writer.WriteAttributeString("listName", "Anticipo");
                    // writer.WriteValue(item2.nrodocanticipo.ToString());
                    writer.WriteValue(item2.line_id.ToString());

                    writer.WriteEndElement();
                    writer.WriteStartElement(prefixCac, "IssuerParty", cadCac);
                    writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
                    writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                    writer.WriteAttributeString("schemeID", item2.tipodocemisor.Trim());
                    writer.WriteValue(item2.nrodocemisor.ToString());
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
            }
        }

        // Datos de la empresa
        // RUC y Razon Social
        writer.WriteStartElement(prefixCac, "Signature", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.SignatureID.Trim());
        writer.WriteStartElement(prefixCac, "SignatoryParty", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.Nrodocumento.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "PartyName", cadCac);
        writer.WriteStartElement(prefixCbc, "Name", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "DigitalSignatureAttachment", cadCac);
        writer.WriteStartElement(prefixCac, "ExternalReference", cadCac);
        writer.WriteElementString("URI", cadCbc, oEmpresa.SignatureURI.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        // RUC
        writer.WriteStartElement(prefixCac, "AccountingSupplierParty", cadCac);
        writer.WriteStartElement(prefixCac, "Party", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteAttributeString("schemeID", listaDocPersona[0].CodigoSunat.Trim());
        writer.WriteValue(oEmpresa.Nrodocumento.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        // Nombre Comercial
        if ((oEmpresa.Nomcomercial.Trim() != EN_Constante.g_const_vacio))
        {
            writer.WriteStartElement(prefixCac, "PartyName", cadCac);
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(oEmpresa.Nomcomercial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
        // Razon Social
        writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "RegistrationAddress", cadCac);
        // Ubigeo
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteCData(oEmpresa.Coddis.Trim());
        writer.WriteEndElement();
        // Código de Establecimiento
        writer.WriteElementString("AddressTypeCode", cadCbc, oDocumento.CodigoEstablecimiento);
        // Provincia
        writer.WriteStartElement(prefixCbc, "CityName", cadCbc);
        writer.WriteCData(oEmpresa.Provincia.Trim());
        writer.WriteEndElement();
        // Departamento
        writer.WriteStartElement(prefixCbc, "CountrySubentity", cadCbc);
        writer.WriteCData(oEmpresa.Departamento.Trim());
        writer.WriteEndElement();
        // Distrito
        writer.WriteStartElement(prefixCbc, "District", cadCbc);
        writer.WriteCData(oEmpresa.Distrito.Trim());
        writer.WriteEndElement();
        // Dirección
        writer.WriteStartElement(prefixCac, "AddressLine", cadCac);
        writer.WriteStartElement(prefixCbc, "Line", cadCbc);
        writer.WriteCData(oEmpresa.Direccion.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        // Código de País
        writer.WriteStartElement(prefixCac, "Country", cadCac);
        writer.WriteElementString("IdentificationCode", cadCbc, oEmpresa.Codpais.Trim());
        writer.WriteEndElement();

        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Lugar en el que se entrega el bien
        // Listamos la guía asociada al documento
        EN_FacturaGuia oDocGuia = new EN_FacturaGuia();
        NE_Documento objDoc = new NE_Documento();
        List<EN_FacturaGuia> listaGuia;
        oDocGuia.documento_id = oDocumento.Id;
        oDocGuia.empresa_id = oDocumento.Idempresa;
        oDocGuia.tipodocumento_id = oDocumento.Idtipodocumento;
        listaGuia = (List<EN_FacturaGuia>)objDoc.listarDocumentoGuia(oDocGuia, oDocumento.Id, oDocumento.Idempresa);
        if (listaGuia.Count > EN_Constante.g_const_0)
        {
            if (oDocumento.TipoOperacion == "0201" || oDocumento.TipoOperacion == "0208")
            {
                // Pais del uso, explotación o aprovechamiento del servicio
                writer.WriteStartElement(prefixCac, "Delivery", cadCac);
                writer.WriteStartElement(prefixCac, "DeliveryLocation", cadCac);
                writer.WriteStartElement(prefixCac, "Address", cadCac);
                writer.WriteStartElement(prefixCac, "Country", cadCac);
                writer.WriteStartElement(prefixCbc, "IdentificationCode", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_pais.ToString());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
            else
            {
                writer.WriteStartElement(prefixCac, "Delivery", cadCac);
                writer.WriteStartElement(prefixCac, "DeliveryLocation", cadCac);
                writer.WriteStartElement(prefixCac, "Address", cadCac);

                // Ubigeo de entrega
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_distrito.ToString());
                writer.WriteEndElement();
                // Direccion de entrega
                writer.WriteStartElement(prefixCac, "AddressLine", cadCac);
                writer.WriteStartElement(prefixCbc, "Line", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_direccion.ToString());
                writer.WriteEndElement();
                writer.WriteEndElement();
                // Urbanizacion de entrega
                writer.WriteStartElement(prefixCbc, "CitySubdivisionName", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_urbanizacion.ToString());
                writer.WriteEndElement();
                // Departamento de entrega
                writer.WriteStartElement(prefixCbc, "CountrySubentity", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_departamentodesc.ToString());
                writer.WriteEndElement();
                // Provincia de entrega
                writer.WriteStartElement(prefixCbc, "CityName", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_provinciadesc.ToString());
                writer.WriteEndElement();
                // Distrito
                writer.WriteStartElement(prefixCbc, "District", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_distritodesc.ToString());
                writer.WriteEndElement();
                // País
                writer.WriteStartElement(prefixCac, "Country", cadCac);
                writer.WriteStartElement(prefixCbc, "IdentificationCode", cadCbc);
                writer.WriteValue(listaGuia[0].llegada_pais.ToString());
                writer.WriteEndElement();
                writer.WriteEndElement();

                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }

        // Cliente
        writer.WriteStartElement(prefixCac, "AccountingCustomerParty", cadCac);
        writer.WriteStartElement(prefixCac, "Party", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteAttributeString("schemeID", tipodoccliente_codsunat.Trim());
        writer.WriteValue(nrodocumentocliente);
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
        writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
        writer.WriteCData(oDocumento.Nombrecliente.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        //etiqueta adicional para percepcion PaymentTerms

         // Percepcion 'Cargo que no afecta la base imponible
        if (oDocumento.Importepercep > EN_Constante.g_const_0)
        {
             // Calculos de Descuentos Cargos Impuestos y Totales
                decimal totalOperacionesIP;
                decimal totalImpuestosIP;
                decimal totalImpGratuitosIP;
                decimal factorDsctoIP;
                decimal factorCargoIP;
                decimal totalDescuentosIP;
                decimal totalCargosIP;

                totalOperacionesIP =(decimal) (oDocumento.Valorpergravadas + oDocumento.Valorperinafectas + oDocumento.Valorperexoneradas + oDocumento.Valorperexportacion);
                totalImpuestosIP = (decimal)(oDocumento.Igvventa + oDocumento.Iscventa + oDocumento.OtrosTributos);
                totalImpGratuitosIP = (decimal)(oDocumento.SumaIgvGratuito + oDocumento.SumaIscGratuito);

                factorCargoIP = (decimal)(Math.Round(oDocumento.OtrosCargos / (double)100, 5));
                factorDsctoIP = (decimal)(Math.Round(oDocumento.Descuento / (double)100, 5));
                // factorDscto = (decimal)(Math.Round((decimal)oDocumento.Descuento / (decimal)(totalOperaciones + totalImpuestos), 5));  //add

                totalDescuentosIP = (totalOperacionesIP + totalImpuestosIP) * factorDsctoIP;
                // totalDescuentos = (decimal)(Math.Round((decimal)oDocumento.Descuento , 2)); //add

                totalCargosIP = (totalOperacionesIP + totalImpuestosIP) * factorCargoIP;
           
            decimal importeBasePercep;
            decimal impPaymentPercep;
            // importeBasePercep = totalOperaciones + totalImpuestos - totalDescuentos + totalCargos;
           
            importeBasePercep = totalOperacionesIP - totalDescuentosIP + totalCargosIP;

            impPaymentPercep = Convert.ToDecimal(importeBasePercep)+Convert.ToDecimal(oDocumento.Importepercep);

                writer.WriteStartElement(prefixCac, "PaymentTerms", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue("Percepcion");
                writer.WriteEndElement();

                writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
                writer.WriteAttributeString("currencyID", CodMon);
                writer.WriteValue(impPaymentPercep.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();

                writer.WriteEndElement();
        }

         // Detracciones
            if ((oDocumento.TipoRegimen != EN_Constante.g_const_vacio) && (oDocumento.TipoRegimen == "DTR" || oDocumento.TipoRegimen == "DST"))
            {
                // /Invoice/cac:PaymentMeans/cac:PayeeFinancialAccount
                writer.WriteStartElement(prefixCac, "PaymentMeans", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue("Detraccion");
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "PaymentMeansCode", cadCbc);
                writer.WriteValue("001");
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "PayeeFinancialAccount", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue(oDocumento.NumCtaBcoNacion.ToString());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();

                // /Invoice/cac:PaymentTerms
                writer.WriteStartElement(prefixCac, "PaymentTerms", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue("Detraccion");
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "PaymentMeansID", cadCbc);
                writer.WriteValue(oDocumento.CodigoBBSSSujetoDetrac.ToString());
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "PaymentPercent", cadCbc);
                writer.WriteValue((Math.Round(oDocumento.Porcentdetrac, 5)).ToString().Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
                writer.WriteAttributeString("currencyID", "PEN");
                writer.WriteValue(oDocumento.Importedetrac.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteEndElement();
            }

         //forma de pago

        if(oDocumento.Idtipodocumento== EN_ConfigConstantes.Instance.const_IdFC || (oDocumento.Idtipodocumento== EN_ConfigConstantes.Instance.const_IdNC && oDocumento.TipoNotaCredNotaDeb == "13")){

        
            if(oDocumento.Formapago == EN_Constante.g_const_formaPago_contado){
                writer.WriteStartElement(prefixCac, "PaymentTerms", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue("FormaPago");
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "PaymentMeansID", cadCbc);
                writer.WriteValue(oDocumento.Formapago.ToString());
                writer.WriteEndElement();
                writer.WriteEndElement();
            }else{
                // credito
                writer.WriteStartElement(prefixCac, "PaymentTerms", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue("FormaPago");
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "PaymentMeansID", cadCbc);
                writer.WriteValue(oDocumento.Formapago.ToString());
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
                writer.WriteAttributeString("currencyID", CodMon);
                writer.WriteValue(oDocumento.Importefinal.ToString("F2").Replace(",", "."));
                writer.WriteEndElement();
                writer.WriteEndElement();


                
            List<EN_FormaPagoCredito> jsonCredigo=  (List<EN_FormaPagoCredito>)JsonConvert.DeserializeObject(oDocumento.jsonpagocredito,typeof( List<EN_FormaPagoCredito>));
            foreach (var item in jsonCredigo)
            {
                writer.WriteStartElement(prefixCac, "PaymentTerms", cadCac);

                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue("FormaPago");
                writer.WriteEndElement();

                writer.WriteStartElement(prefixCbc, "PaymentMeansID", cadCbc);
                writer.WriteValue(item.NumeroCuota);
                writer.WriteEndElement();
                
                writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
                writer.WriteAttributeString("currencyID", CodMon);
                writer.WriteValue(item.MontoCuota.ToString());
                writer.WriteEndElement();

                writer.WriteStartElement(prefixCbc, "PaymentDueDate", cadCbc);
                writer.WriteValue(Convert.ToDateTime(item.FechaCuota.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion));
                writer.WriteEndElement();

                writer.WriteEndElement();


            }


            }
         }

       

        // Anticipos
        // Datos de Anticipo -- Tabla docanticipo
        // Serie y número de comprobante del anticipo (para el caso de reorganización de empresas, incluye el RUC)
        // Código de tipo de documento
        // Monto prepagado o anticipado
        // Código de tipo de moneda del monto prepagado o anticipado
        // Número de RUC del emisor del comprobante de anticipo

        if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_1.ToString())
        // if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_si.ToString())
        {
            List<EN_DocAnticipo> listaAnticipos;
            oDocumentoAnt.empresa_id = oDocumento.Idempresa;
            oDocumentoAnt.documento_id = oDocumento.Id;
            oDocumentoAnt.tipodocumento_id = oDocumento.Idtipodocumento;
            listaAnticipos =( List<EN_DocAnticipo>) objDocumentoRef.listarDocumentoAnticipo(oDocumentoAnt, oDocumento.Id, oDocumento.Idpuntoventa);

            decimal sumaAnticipos=0;
            if (listaAnticipos.Count > EN_Constante.g_const_0)
            {
                foreach (var item2 in listaAnticipos.ToList())
                {
                    
                    sumaAnticipos= sumaAnticipos + Convert.ToDecimal(item2.montoanticipo/Convert.ToDecimal(1.18));
                    writer.WriteStartElement(prefixCac, "PrepaidPayment", cadCac);
                    writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                    writer.WriteAttributeString("schemeName", "Anticipo");
                    // writer.WriteValue(item2.nrodocanticipo.ToString());
                    writer.WriteValue(item2.line_id.ToString());
                    writer.WriteEndElement();
                    writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
                    writer.WriteAttributeString("currencyID", CodMon);
                    writer.WriteValue(item2.montoanticipo.ToString("F2").Replace(",", "."));
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
            }
            decimal baseAnt=0;
            decimal factorAnt=0;
            baseAnt= sumaAnticipos+ (decimal) (oDocumento.Valorpergravadas + oDocumento.Valorperinafectas + oDocumento.Valorperexoneradas + oDocumento.Valorperexportacion);;
            factorAnt= Math.Round(sumaAnticipos/ baseAnt,5);
            //AGREGADO
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "false");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "04"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (factorAnt).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((sumaAnticipos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((baseAnt).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Calculos de Descuentos Cargos Impuestos y Totales
        decimal totalOperaciones;
        decimal totalImpuestos;
        decimal totalImpGratuitos;
        decimal factorDscto;
        decimal factorCargo;
        decimal totalDescuentos;
        decimal totalCargos;

         if(oDocumento.Descuento > EN_Constante.g_const_0 && oDocumento.Descuentoafectabase=="SI")
        {
            oDocumento.Valorpergravadas=oDocumento.Importeventa;
        }

        totalOperaciones =(decimal) (oDocumento.Valorpergravadas + oDocumento.Valorperinafectas + oDocumento.Valorperexoneradas + oDocumento.Valorperexportacion);
        totalImpuestos = (decimal)(oDocumento.Igvventa + oDocumento.Iscventa + oDocumento.OtrosTributos);
        totalImpGratuitos = (decimal)(oDocumento.SumaIgvGratuito + oDocumento.SumaIscGratuito);

        factorCargo = (decimal)(Math.Round(oDocumento.OtrosCargos / (double)100, 5));
        factorDscto = (decimal)(Math.Round(oDocumento.Descuento / (double)100, 5));
        // factorDscto = (decimal)(Math.Round((decimal)oDocumento.Descuento / (decimal)(totalOperaciones + totalImpuestos), 5));  //add

        totalDescuentos = (totalOperaciones + totalImpuestos) * factorDscto;
        // totalDescuentos = (decimal)(Math.Round((decimal)oDocumento.Descuento , 2)); //add
        totalCargos = (totalOperaciones + totalImpuestos) * factorCargo;


        if (oDocumento.Descuento > EN_Constante.g_const_0)
        {

            string afectaBaseDesc = (oDocumento.Descuentoafectabase=="SI")? objFacturacion.ObtenerCodigoSunat("053", "02"): objFacturacion.ObtenerCodigoSunat("053", "03");
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "false");
            // writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "03"));
            writer.WriteElementString("AllowanceChargeReasonCode",cadCbc, afectaBaseDesc );
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (factorDscto).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalDescuentos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Otros Cargos ' Se han considerado que no afectan a la base imponible
        if (oDocumento.OtrosCargos > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "50"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (factorCargo).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalCargos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Percepcion 'Cargo que no afecta la base imponible
        if (oDocumento.Importepercep > EN_Constante.g_const_0)
        {
            decimal importeBasePercep;
            // importeBasePercep = totalOperaciones + totalImpuestos - totalDescuentos + totalCargos;
            importeBasePercep = totalOperaciones - totalDescuentos + totalCargos;
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");

            if (oDocumento.Regimenpercep == "03")
                writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "53"));
            else if (oDocumento.Regimenpercep == "02")
                writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "52"));
            else
                writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "51"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (Math.Round(oDocumento.Porcentpercep / (double)100, 5)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue(oDocumento.Importepercep.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((importeBasePercep).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

       
                

                  // retencion del igv
        if(oDocumento.hasRetencionIgv==EN_Constante.g_const_si)
        {

        
                writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
                writer.WriteStartElement(prefixCbc, "ChargeIndicator", cadCbc);
                writer.WriteValue("false");
                writer.WriteEndElement();

                writer.WriteStartElement(prefixCbc, "AllowanceChargeReasonCode", cadCbc);
                writer.WriteAttributeString("listAgencyName", "PE:SUNAT");
                writer.WriteAttributeString("listName", "Cargo/descuento");
                writer.WriteAttributeString("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo53");
                writer.WriteValue("62");
                writer.WriteEndElement();

                writer.WriteStartElement(prefixCbc, "MultiplierFactorNumeric", cadCbc);
                writer.WriteValue(oDocumento.porcRetencionIgv);
                writer.WriteEndElement();

                writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
                writer.WriteAttributeString("currencyID", CodMon);
                 writer.WriteValue((oDocumento.impRetencionIgv).ToString("F2").Replace(",", "."));
                writer.WriteEndElement();


                writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
                writer.WriteAttributeString("currencyID", CodMon);
                 writer.WriteValue((oDocumento.impOperacionRetencionIgv).ToString("F2").Replace(",", "."));
                writer.WriteEndElement();

                writer.WriteEndElement();
            }
        // fin de retencion igv

        // TaxTotal - Monto Total de Impuestos
        writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue((totalImpuestos).ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // --- Operaciones Gravadas IGV -- 1000
        if ((oDocumento.Valorpergravadas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);



            if (oDocumento.IndicaAnticipo.Trim() == EN_Constante.g_const_1.ToString())
            {
                List<EN_DocAnticipo> listaAnticipos;
            oDocumentoAnt.empresa_id = oDocumento.Idempresa;
            oDocumentoAnt.documento_id = oDocumento.Id;
            oDocumentoAnt.tipodocumento_id = oDocumento.Idtipodocumento;
            listaAnticipos =( List<EN_DocAnticipo>) objDocumentoRef.listarDocumentoAnticipo(oDocumentoAnt, oDocumento.Id, oDocumento.Idpuntoventa);
                double sumaAnticipos=0;
                double valueOper =0;
                foreach (var item2 in listaAnticipos.ToList())
                {
                    sumaAnticipos= sumaAnticipos + Convert.ToDouble(item2.montoanticipo);
                }
                valueOper = oDocumento.Valorpergravadas - (sumaAnticipos/1.18);
                // valueOper = oDocumento.Valorpergravadas ;

                 writer.WriteValue(valueOper.ToString("F2").Replace(",", "."));
            }
            else{
                writer.WriteValue(oDocumento.Valorpergravadas.ToString("F2").Replace(",", "."));
            }

            // writer.WriteValue((oDocumento.Valorpergravadas).ToString("F2").Replace(",", "."));

            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Igvventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);

            //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteValue("1016");
            else                            writer.WriteValue("1000");

            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);

            //IVAP
            if(oDocumento.es_ivap=="SI")     writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "1016"));
            else                             writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "1000"));
       
            writer.WriteEndElement();

            //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "1016"));
            else                            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "1000"));

            
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones Gravadas ISC -- 2000
        if ((oDocumento.Iscventa > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperiscreferenc.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Iscventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("2000");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "2000"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "2000"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones INAFECTAS FRE -- 9998
        if ((oDocumento.Valorperinafectas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((oDocumento.Valorperinafectas).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9998");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9998"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9998"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones EXONERADAS VAT -- 9997
        if ((oDocumento.Valorperexoneradas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
  
            writer.WriteValue(oDocumento.Valorperexoneradas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9997");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9997"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9997"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones EXPORTACIÓN FRE -- 9995 --- SI ES GRATUITO -- 9996
        if ((oDocumento.Valorperexportacion > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperexportacion.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteValue("9996");
            else
                writer.WriteValue("9995");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9996"));
            else
                writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9995"));
            writer.WriteEndElement();
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9996"));
            else
                writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9995"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones GRATUITO FRE -- 9996
        if ((oDocumento.Valorpergratuitas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorpergratuitas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(totalImpGratuitos);
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9996");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9996"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9996"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones Gravadas OTH -- 9999
        if ((oDocumento.OtrosTributos > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorpergravadas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.OtrosTributos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9999");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9999"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9999"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        writer.WriteEndElement();

        // LegalMonetaryTotal
        writer.WriteStartElement(prefixCac, "LegalMonetaryTotal", cadCac);

        // Total Valor de Venta (Sin Descuento Global, Sin IGV -- ISC -- Otros Tributos, Sin Otros Cargos)
        writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue(totalOperaciones.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // agregado
        if (oDocumento.IndicaAnticipo.Trim() != EN_Constante.g_const_1.ToString() || oDocumento.IndicaAnticipo.Trim()=="" || oDocumento.IndicaAnticipo.Trim()==null)
        {
             // Total Impuestos = IGV + ISC + Otros Tributos
            writer.WriteStartElement(prefixCbc, "TaxExclusiveAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(totalImpuestos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();

            // Total Precio de Venta (ValorVenta + Impuestos) --- Impuestos = IGV + ISC + Otros Tributos
            writer.WriteStartElement(prefixCbc, "TaxInclusiveAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }
        else{
             // Total Precio de Venta (ValorVenta + Impuestos) --- Impuestos = IGV + ISC + Otros Tributos
            writer.WriteStartElement(prefixCbc, "TaxInclusiveAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((Math.Round(totalOperaciones * Convert.ToDecimal(1.18),2)).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }
       

        // Total de Descuentos (que no afectan la base imponible)
        if (totalDescuentos > EN_Constante.g_const_0 && oDocumento.Descuentoafectabase=="NO")
        {
            writer.WriteStartElement(prefixCbc, "AllowanceTotalAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((totalDescuentos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // Total de Cargos (que no afectan la base imponible)
        if (totalCargos > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "ChargeTotalAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(totalCargos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // **. Total de Anticipos
        if (oDocumento.Importeanticipo > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "PrepaidAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Importeanticipo.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // 34. Importe total de la venta, cesión en uso o del servicio prestado
        writer.WriteStartElement(prefixCbc, "PayableAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue(oDocumento.Importefinal.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        writer.WriteEndElement();

    }


    private static void ObtenerDatosDetalleFactura21(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_DetalleDocumento listaDetalle, string moneda, EN_Documento oDocumento)
    {
        //DESCRIPCION: Funcion para generar los datos del detalle del XML en formato UBL 2.1 -- FACTURA
        NE_Facturacion objFacturacion = new NE_Facturacion();       // CLASE NEGOCIO FACTURACION

        // Datos del item unidad, cantidad, precio_unitario, valorventa
        writer.WriteStartElement(prefixCac, "InvoiceLine", cadCac);
        // 35. Número de orden del Ítem
        writer.WriteElementString("ID", cadCbc, listaDetalle.Lineid.ToString());

        // 36. Cantidad y Unidad de medida por ítem 
        writer.WriteStartElement(prefixCbc, "InvoicedQuantity", cadCbc);
        writer.WriteAttributeString("unitCode", listaDetalle.Unidad.ToString());
        // 36. Cantidad de unidades por ítem
        writer.WriteValue((Math.Round(listaDetalle.Cantidad, 10)).ToString().Replace(",", "."));
        writer.WriteEndElement();

        // 37. Valor de venta del ítem
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            // Si es afecto a algun impuesto se toma el valor venta ya que este incluye el descuento que afecta la base imponible
            writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((listaDetalle.Valorventa).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }
        else
        {
            // Operacion gratuita
            writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // 38. Precio de venta unitario --> Con Impuesto y Menos el Descuento
        writer.WriteStartElement(prefixCac, "PricingReference", cadCac);
        // Validamos si el valor venta es mayor a 0 no es gratuito
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "AlternativeConditionPrice", cadCac);
            // Precio Unitario
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Preciounitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            // Código
            writer.WriteStartElement(prefixCbc, "PriceTypeCode", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerCodigoSunat("016", "001").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        else
        {
            // Es gratuito Precioreferencial
            writer.WriteStartElement(prefixCac, "AlternativeConditionPrice", cadCac);
            // Precio Unitario
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Valorunitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            // Código
            writer.WriteStartElement(prefixCbc, "PriceTypeCode", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerCodigoSunat("016", "002").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();

        // '92. Transporte de bienes - 027   
            // Para Detracciones Servicio de Transporte de Carga
            if ((oDocumento.CodigoBBSSSujetoDetrac == "027") && (listaDetalle.TBVTPuntoDestino.Trim() != EN_Constante.g_const_vacio && listaDetalle.TBVTDescripcionDestino.Trim() != EN_Constante.g_const_vacio && listaDetalle.TBVTDetalleViaje.Trim() != EN_Constante.g_const_vacio && listaDetalle.TBVTPuntoOrigen.Trim() != EN_Constante.g_const_vacio && listaDetalle.TBVTDescripcionOrigen.Trim() != EN_Constante.g_const_vacio))
            {
                writer.WriteStartElement(prefixCac, "Delivery", cadCac);

                // <cac:DeliveryLocation>
                writer.WriteStartElement(prefixCac, "DeliveryLocation", cadCac);
                writer.WriteStartElement(prefixCac, "Address", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteAttributeString("schemeAgencyName", "PE:INEI");
                writer.WriteAttributeString("schemeName", "Ubigeos");
                writer.WriteValue(listaDetalle.TBVTPuntoDestino.Trim());
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "AddressLine", cadCac);
                writer.WriteStartElement(prefixCbc, "Line", cadCbc);
                writer.WriteCData(listaDetalle.TBVTDescripcionDestino.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();

                // <cac:Despatch>
                writer.WriteStartElement(prefixCac, "Despatch", cadCac);
                writer.WriteStartElement(prefixCbc, "Instructions", cadCbc);
                writer.WriteCData(listaDetalle.TBVTDetalleViaje);
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "DespatchAddress", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteAttributeString("schemeAgencyName", "PE:INEI");
                writer.WriteAttributeString("schemeName", "Ubigeos");
                writer.WriteValue(listaDetalle.TBVTPuntoOrigen.Trim());
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCac, "AddressLine", cadCac);
                writer.WriteStartElement(prefixCbc, "Line", cadCbc);
                writer.WriteCData(listaDetalle.TBVTDescripcionOrigen.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();
                writer.WriteEndElement();

                // <cac:DeliveryTerms>
                writer.WriteStartElement(prefixCac, "DeliveryTerms", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue("01");
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
                writer.WriteAttributeString("currencyID", oDocumento.Moneda);
                writer.WriteValue(listaDetalle.TBVTValorRefPreliminar);
                writer.WriteEndElement();
                writer.WriteEndElement();

                // <cac:DeliveryTerms>
                writer.WriteStartElement(prefixCac, "DeliveryTerms", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue("02");
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
                writer.WriteAttributeString("currencyID", oDocumento.Moneda);
                writer.WriteValue(listaDetalle.TBVTValorCargaEfectiva);
                writer.WriteEndElement();
                writer.WriteEndElement();

                // <cac:DeliveryTerms>
                writer.WriteStartElement(prefixCac, "DeliveryTerms", cadCac);
                writer.WriteStartElement(prefixCbc, "ID", cadCbc);
                writer.WriteValue("03");
                writer.WriteEndElement();
                writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
                writer.WriteAttributeString("currencyID", oDocumento.Moneda);
                writer.WriteValue(listaDetalle.TBVTValorCargaUtil);
                writer.WriteEndElement();
                writer.WriteEndElement();

                writer.WriteEndElement();
            }
        

        // 40. Descuentos / Cargos por Item
        decimal valbruto;
        if (listaDetalle.Valorbruto > EN_Constante.g_const_0)
            valbruto = (decimal)listaDetalle.Valorbruto;
        else
            // Gratuito
            valbruto = (decimal)((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario);

        if (listaDetalle.Valordscto > EN_Constante.g_const_0)
        {
            // Descuentos que afectan la base imponible
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "false");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "00"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (Math.Round(listaDetalle.Valordscto / (double)valbruto, 5)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valordscto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(valbruto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        if (listaDetalle.Valorcargo > EN_Constante.g_const_0)
        {
            // Cargos que afectan la base imponible
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "47"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (Math.Round(listaDetalle.Valorcargo / (double)valbruto, 5)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valorcargo.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(valbruto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        decimal valorventa;
        decimal montoTributo;

        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            valorventa = (decimal)((decimal)listaDetalle.Valorventa + (decimal)listaDetalle.Isc);
            montoTributo = (decimal)(listaDetalle.Igv + listaDetalle.Isc);
        }
        else
        {
            // Operacion Gratuita
            valorventa = (decimal)((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario);
            montoTributo = 0;
        }

        writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(montoTributo.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // 42. Afectación al IGV por ítem
        string tipoafect;
        // 10 -- Gravado Operación Onerosa
        // 11, 12, 13, 14, 15, 16 -- Gravado Retiro - Bonificaciones (TG)
        // 20 -- Exonerado Operación Onerosa
        // 21 -- Exonerado Transferencia gratuita
        // 30 -- Inafecto Operación Onerosa
        // 31, 32, 33, 34, 35, 36, 37 -- Inafecto Retiro - TG
        // 40 -- Exportaciones

        switch (listaDetalle.Afectacionigv.Trim())
        {
            case "10":
                {
                    tipoafect = "1000";
                    break;
                }

            case "11":
            case "12":
            case "13":
            case "14":
            case "15":
            case "16":
                {
                    tipoafect = "9996";
                    break;
                }

            //IVAP

            case "17":
                {
                    tipoafect = "1016";
                    break;
                }

            case "20":
                {
                    tipoafect = "9997";
                    break;
                }

            case "21":
                {
                    tipoafect = "9996";
                    break;
                }

            case "30":
                {
                    tipoafect = "9998";
                    break;
                }

            case "31":
            case "32":
            case "33":
            case "34":
            case "35":
            case "36":
            case "37":
                {
                    tipoafect = "9996";
                    break;
                }

            case "40":
                {
                    if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                        tipoafect = "9996";
                    else
                        tipoafect = "9995";
                    break;
                }

            default:
                {
                    tipoafect = "1000";
                    break;
                }
        }

        // Afectacion IGV por ITEM
        writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(valorventa.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(listaDetalle.Igv.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
        writer.WriteElementString("Percent", cadCbc, (Math.Round(listaDetalle.factorIgv, 5)).ToString().Replace(",", "."));
        writer.WriteStartElement(prefixCbc, "TaxExemptionReasonCode", cadCbc);
        writer.WriteValue(listaDetalle.Afectacionigv.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteValue(tipoafect);
        writer.WriteEndElement();
        writer.WriteElementString("Name", cadCbc, objFacturacion.ObtenerDescripcionPorCodElemento("005", tipoafect).Trim());
        writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", tipoafect).Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Afectación ISC por ítem
        if (listaDetalle.AfectoIsc == EN_Constante.g_const_si)
        {
            decimal montobaseisc;
            if (listaDetalle.SistemaISC == "01")
                // Para el tipocalculoisc 01 Sistema al valor
                montobaseisc = (decimal)listaDetalle.Valorventa;
            else if (listaDetalle.SistemaISC == "02")
                // Para el tipocalculoisc 02 Aplicacion al Monto Fijo - Por confirmar formula
                montobaseisc = (decimal)listaDetalle.Valorventa;
            else if (listaDetalle.SistemaISC == "03")
                // Para el tipocalculoisc 03 Sistema de Precio de Venta al Publico
                montobaseisc = (decimal)(listaDetalle.Precioreferencial * listaDetalle.Cantidad);
            else
                montobaseisc = (decimal)listaDetalle.Valorventa;

            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((montobaseisc).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Isc.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteElementString("Percent", cadCbc, (Math.Round(listaDetalle.factorIsc, 5)).ToString().Replace(",", "."));
            writer.WriteElementString("TierRange", cadCbc, listaDetalle.SistemaISC.Trim());
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("2000");
            writer.WriteEndElement();
            writer.WriteElementString("Name", cadCbc, objFacturacion.ObtenerDescripcionPorCodElemento("005", "2000").Trim());
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "2000").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        writer.WriteEndElement();

        writer.WriteStartElement(prefixCac, "Item", cadCac);
        // 44. Descripción detallada del servicio prestado, bien vendido o cedido en uso, indicando las características
        writer.WriteStartElement(prefixCbc, "Description", cadCbc);
        writer.WriteCData(listaDetalle.Producto.Replace(Constants.vbCrLf, " ").Trim());
        writer.WriteEndElement();
        // Código de producto
        if (listaDetalle.Codigo.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "SellersItemIdentification", cadCac);
            writer.WriteElementString("ID", cadCbc, listaDetalle.Codigo.Trim());
            writer.WriteEndElement();
        }
        // Código de producto SUNAT
        if (listaDetalle.CodigoSunat.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "CommodityClassification", cadCac);
            writer.WriteStartElement(prefixCbc, "ItemClassificationCode", cadCbc);
            writer.WriteValue(listaDetalle.CodigoSunat.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();

        // 48. Valor Unitario por Item
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "Price", cadCac);
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Valorunitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        else
        {
            // Gratuito
            writer.WriteStartElement(prefixCac, "Price", cadCac);
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(0);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();
    }

    
    private static void ObtenerDatosCabeceraNotaCredito21(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_Documento oDocumento, EN_Empresa oEmpresa, string CodMon)
    {
        //DECRIPCION: Funcion para generar los datos de la cabecera del XML en formato UBL 2.1 -- NOTA DE CREDITO

        NE_Facturacion objFacturacion = new NE_Facturacion();       // CLASE NEGOCIO FACTURACION
        NE_DocPersona objDocPersona = new NE_DocPersona();          // CLASE NEGOCIO DOCPERSONA
        EN_DocPersona oDocPersona = new EN_DocPersona();            // CLASE ENTIDAD DOCPERSONA
        List<EN_DocPersona> listaDocPersona;                        // LISTA DE CLASE ENTIDAD DOCPERSONA
        string tipodoccliente_codsunat;                             // TIPO DOCUMENTO DE CLIENTE DE CODIGO SUNAT
        string nrodocumentocliente;                                 // NUMERO DE DOCUMENTO DE CLIENTE
        NE_Documento objDocumentoRef = new NE_Documento();          // CLASE NEGOCIO DOCUMENTO
        EN_DocReferencia oDocumentoRef = new EN_DocReferencia();    // CLASE ENTIDAD DOCREFERENCIA  
        List<EN_DocReferencia> listaNCNDDocRefencia;                // LISTA DE CLASE ENTIDAD DOCREFERENCIA  
        EN_DocRefeGuia oDocRefeGuia = new EN_DocRefeGuia();         // CLASE ENTIDAD DOCREFEGUIA
        string MontoLetras;                                         // MONTO LETRAS
        NumeroALetras numeroALetras = new NumeroALetras();          // NUMERO A LETRAS

        // Listamos el codigo del tipo de documento de la empresa
        oDocPersona.Codigo = oEmpresa.Tipodocumento;
        oDocPersona.Estado = EN_Constante.g_const_vacio;
        oDocPersona.Indpersona = EN_Constante.g_const_vacio;
        listaDocPersona = (List<EN_DocPersona>)objDocPersona.listarDocPersona(oDocPersona,  oDocumento.Id, oDocumento.Idpuntoventa);

        // Listamos el código del tipo de documento del cliente y su número de documento
        if (oDocumento.Tipodoccliente.Trim() == EN_Constante.g_const_vacio)
        {
            tipodoccliente_codsunat = "0";
            nrodocumentocliente = "-";
        }
        else if (Strings.Trim(oDocumento.Tipodoccliente).Length == 1)
        {
            tipodoccliente_codsunat = oDocumento.Tipodoccliente.Trim();
            nrodocumentocliente = oDocumento.Nrodoccliente.Trim();
        }
        else
        {
            tipodoccliente_codsunat = objFacturacion.ObtenerCodigoSunat("006", oDocumento.Tipodoccliente.Trim());
            nrodocumentocliente = oDocumento.Nrodoccliente.Trim();
        }

        // Versión del UBL.
        writer.WriteElementString("UBLVersionID", cadCbc, "2.1");

        // Versión de la estructura del documento.
        writer.WriteElementString("CustomizationID", cadCbc, "2.0");

        // Numeración, conformada por serie y número correlativo.
        writer.WriteElementString("ID", cadCbc, oDocumento.Serie.Trim() + "-" + oDocumento.Numero.Trim());

        // Fecha de emisión.
        
        writer.WriteElementString("IssueDate", cadCbc, Convert.ToDateTime(oDocumento.Fecha).ToString(EN_Constante.g_const_formfechaGuion));

        // Hora de emisión.
        writer.WriteElementString("IssueTime", cadCbc, Convert.ToDateTime(oDocumento.Hora).ToString(EN_Constante.g_const_formhora));

        // Leyenda de Monto del importe de la venta
        MontoLetras = numeroALetras.Convertir(oDocumento.Importefinal);

        writer.WriteStartElement(prefixCbc, "Note", cadCbc);
        writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "1000"));
        writer.WriteCData(MontoLetras);
        writer.WriteEndElement();

         //PARA IVAP
        if(oDocumento.es_ivap=="SI") 
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2007"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2007"));
            writer.WriteEndElement();
        }

        // Leyenda de Transferencia Gratuita
        if ((oDocumento.Estransgratuita == EN_Constante.g_const_si))
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "1002"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "1002"));
            writer.WriteEndElement();
        }

        // Tipo de moneda.
        writer.WriteStartElement(prefixCbc, "DocumentCurrencyCode", cadCbc);
        writer.WriteValue(oDocumento.Moneda);
        writer.WriteEndElement();

        // Buscamos el documento que hace referencia la nota de credito o nota de debito
        string tipodocref = EN_Constante.g_const_vacio;
        string numerodocref = EN_Constante.g_const_vacio;
        oDocumentoRef.Idempresa = oDocumento.Idempresa;
        oDocumentoRef.Iddocumento = oDocumento.Id;
        oDocumentoRef.Idtipodocumento = oDocumento.Idtipodocumento;
        listaNCNDDocRefencia = (List<EN_DocReferencia>)objDocumentoRef.listarDocumentoReferencia(oDocumentoRef,oDocumento.Id, oDocumento.Idempresa);
        if (listaNCNDDocRefencia.Count > EN_Constante.g_const_0)
        {
            tipodocref = listaNCNDDocRefencia[0].Idtipodocumentoref;
            numerodocref = listaNCNDDocRefencia[0].Numerodocref;
        }

        writer.WriteStartElement(prefixCac, "DiscrepancyResponse", cadCac);
        writer.WriteElementString("ReferenceID", cadCbc, numerodocref); // Cambiar por Serie y número del documento que modifica
        writer.WriteElementString("ResponseCode", cadCbc, oDocumento.TipoNotaCredNotaDeb);
        if (oDocumento.Glosa != EN_Constante.g_const_vacio)
        {
            // writer.WriteElementString("Description", cadCbc, oDocumento.Glosa) 'Validar si en la glosa se guarda el motivo de la nota de credito
            writer.WriteStartElement(prefixCbc, "Description", cadCbc);
            writer.WriteCData(oDocumento.Glosa.Trim());
            writer.WriteEndElement();
        }
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "BillingReference", cadCac);
        writer.WriteStartElement(prefixCac, "InvoiceDocumentReference", cadCac);
        writer.WriteElementString("ID", cadCbc, numerodocref);
        writer.WriteElementString("DocumentTypeCode", cadCbc, tipodocref);
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Documentos Relacionados al comprobante
        // 12. Tipo y número de la guía de remisión relacionada con la operación
        // Listamos las guias de remision
        List<EN_DocRefeGuia> listaDocRefGuias;
        oDocRefeGuia.empresa_id = oDocumento.Idempresa;
        oDocRefeGuia.documento_id = oDocumento.Id;
        oDocRefeGuia.tipodocumento_id = oDocumento.Idtipodocumento;
        listaDocRefGuias = (List<EN_DocRefeGuia>)objDocumentoRef.listarDocRefeGuia(oDocRefeGuia, oDocumento.Id, oDocumento.Idempresa);
        if (listaDocRefGuias.Count > EN_Constante.g_const_0)
        {
            foreach (var item2 in listaDocRefGuias.ToList())
            {
                writer.WriteStartElement(prefixCac, "DespatchDocumentReference", cadCac);
                writer.WriteElementString("ID", cadCbc, item2.numero_guia.Trim());
                writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
                writer.WriteAttributeString("listAgencyName", "PE:SUNAT");
                writer.WriteAttributeString("listName", "SUNAT:Identificador de guía relacionada");
                writer.WriteAttributeString("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01");
                writer.WriteValue(item2.tipo_guia.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }
        else if (oDocumento.TipoGuiaRemision.Trim() != EN_Constante.g_const_vacio && oDocumento.GuiaRemision.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "DespatchDocumentReference", cadCac);
            writer.WriteElementString("ID", cadCbc, oDocumento.GuiaRemision.Trim());
            writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
            writer.WriteAttributeString("listAgencyName", "PE:SUNAT");
            writer.WriteAttributeString("listName", "SUNAT:Identificador de guía relacionada");
            writer.WriteAttributeString("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01");
            writer.WriteValue(oDocumento.TipoGuiaRemision.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // X. Documento de referencia por servicio publico
        // /Invoice/cac:ContractDocumentReference

        // 13. Tipo y número de otro documento y/ código documento relacionado con la operación
        if (oDocumento.TipoDocOtroDocRef.Trim() != EN_Constante.g_const_vacio && oDocumento.OtroDocumentoRef.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "AdditionalDocumentReference", cadCac);
            writer.WriteElementString("ID", cadCbc, oDocumento.OtroDocumentoRef.Trim());
            writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
            writer.WriteAttributeString("listAgencyName", "PE:SUNAT");
            writer.WriteAttributeString("listName", "SUNAT: Identificador de documento relacionado");
            writer.WriteAttributeString("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo12");
            writer.WriteValue(oDocumento.TipoDocOtroDocRef.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Datos de la empresa
        // Información adicional de la firma
        writer.WriteStartElement(prefixCac, "Signature", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.SignatureID.Trim());
        writer.WriteStartElement(prefixCac, "SignatoryParty", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.Nrodocumento.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "PartyName", cadCac);
        writer.WriteStartElement(prefixCbc, "Name", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "DigitalSignatureAttachment", cadCac);
        writer.WriteStartElement(prefixCac, "ExternalReference", cadCac);
        writer.WriteElementString("URI", cadCbc, oEmpresa.SignatureURI.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // RUC
        writer.WriteStartElement(prefixCac, "AccountingSupplierParty", cadCac);
        writer.WriteStartElement(prefixCac, "Party", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteAttributeString("schemeID", listaDocPersona[0].CodigoSunat.Trim());
        writer.WriteValue(oEmpresa.Nrodocumento.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        // Nombre Comercial
        if ((oEmpresa.Nomcomercial.Trim() != EN_Constante.g_const_vacio))
        {
            writer.WriteStartElement(prefixCac, "PartyName", cadCac);
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(oEmpresa.Nomcomercial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
        // Razon Social
        writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "RegistrationAddress", cadCac);
        // Ubigeo
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteCData(oEmpresa.Coddis.Trim());
        writer.WriteEndElement();
        // Código de Establecimiento
        writer.WriteElementString("AddressTypeCode", cadCbc, oDocumento.CodigoEstablecimiento);
        // Provincia
        writer.WriteStartElement(prefixCbc, "CityName", cadCbc);
        writer.WriteCData(oEmpresa.Provincia.Trim());
        writer.WriteEndElement();
        // Departamento
        writer.WriteStartElement(prefixCbc, "CountrySubentity", cadCbc);
        writer.WriteCData(oEmpresa.Departamento.Trim());
        writer.WriteEndElement();
        // Distrito
        writer.WriteStartElement(prefixCbc, "District", cadCbc);
        writer.WriteCData(oEmpresa.Distrito.Trim());
        writer.WriteEndElement();
        // Dirección
        writer.WriteStartElement(prefixCac, "AddressLine", cadCac);
        writer.WriteStartElement(prefixCbc, "Line", cadCbc);
        writer.WriteCData(oEmpresa.Direccion.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        // Código de País
        writer.WriteStartElement(prefixCac, "Country", cadCac);
        writer.WriteElementString("IdentificationCode", cadCbc, oEmpresa.Codpais.Trim());
        writer.WriteEndElement();

        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // AccountingCustomerParty
        writer.WriteStartElement(prefixCac, "AccountingCustomerParty", cadCac);
        writer.WriteStartElement(prefixCac, "Party", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteAttributeString("schemeID", tipodoccliente_codsunat.Trim());
        writer.WriteValue(nrodocumentocliente);
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
        writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
        writer.WriteCData(oDocumento.Nombrecliente.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Calculos de Descuentos Cargos Impuestos y Totales
        decimal totalOperaciones;
        decimal totalImpuestos;
        decimal totalImpGratuitos;
        decimal factorDscto;
        decimal factorCargo;
        decimal totalDescuentos;
        decimal totalCargos;

        totalOperaciones = (decimal)(oDocumento.Valorpergravadas + oDocumento.Valorperinafectas + oDocumento.Valorperexoneradas + oDocumento.Valorperexportacion);
        totalImpuestos = (decimal)(oDocumento.Igvventa + oDocumento.Iscventa + oDocumento.OtrosTributos);
        totalImpGratuitos = (decimal)(oDocumento.SumaIgvGratuito + oDocumento.SumaIscGratuito);

        factorCargo = (decimal)(Math.Round(oDocumento.OtrosCargos / (double)100, 5));
        factorDscto = (decimal)(Math.Round(oDocumento.Descuento / (double)100, 5));
        totalDescuentos = (totalOperaciones + totalImpuestos) * factorDscto;
        totalCargos = (totalOperaciones + totalImpuestos) * factorCargo;

        if (oDocumento.Descuento > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "false");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "03"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (factorDscto).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalDescuentos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Otros Cargos ' Se han considerado que no afectan a la base imponible
        if (oDocumento.OtrosCargos > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "50"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (factorCargo).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalCargos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // TaxTotal - Monto Total de Impuestos
        writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue((totalImpuestos).ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // --- Operaciones Gravadas IGV -- 1000
        if ((oDocumento.Valorpergravadas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((oDocumento.Valorpergravadas).ToString("F2").Replace(",", "."));

            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Igvventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);

            //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteValue("1016");
            else                            writer.WriteValue("1000");


            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);

            //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "1016"));
            else                            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "1000"));

            writer.WriteEndElement();

            //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "1016"));
            else                            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "1000"));

            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones Gravadas ISC -- 2000
        if ((oDocumento.Iscventa > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperiscreferenc.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Iscventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("2000");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "2000"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "2000"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones INAFECTAS FRE -- 9998
        if ((oDocumento.Valorperinafectas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((oDocumento.Valorperinafectas).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9998");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9998"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9998"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones EXONERADAS VAT -- 9997
        if ((oDocumento.Valorperexoneradas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperexoneradas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9997");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9997"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9997"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones EXPORTACIÓN FRE -- 9995 --- SI ES GRATUITO -- 9996
        if ((oDocumento.Valorperexportacion > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperexportacion.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteValue("9996");
            else
                writer.WriteValue("9995");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9996"));
            else
                writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9995"));
            writer.WriteEndElement();
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9996"));
            else
                writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9995"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones GRATUITO FRE -- 9996
        if ((oDocumento.Valorpergratuitas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorpergratuitas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(totalImpGratuitos);
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9996");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9996"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9996"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones Gravadas OTH -- 9999
        if ((oDocumento.OtrosTributos > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorpergravadas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.OtrosTributos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9999");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9999"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9999"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        writer.WriteEndElement();

        // LegalMonetaryTotal
        writer.WriteStartElement(prefixCac, "LegalMonetaryTotal", cadCac);

        // Total Valor de Venta (Sin Descuento Global, Sin IGV -- ISC -- Otros Tributos, Sin Otros Cargos)
        writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue(totalOperaciones.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // Total Impuestos = IGV + ISC + Otros Tributos
        writer.WriteStartElement(prefixCbc, "TaxExclusiveAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue(totalImpuestos.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // Total Precio de Venta (ValorVenta + Impuestos) --- Impuestos = IGV + ISC + Otros Tributos
        writer.WriteStartElement(prefixCbc, "TaxInclusiveAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // Total de Descuentos (que no afectan la base imponible)
        if (totalDescuentos > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "AllowanceTotalAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((totalDescuentos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // Total de Cargos (que no afectan la base imponible)
        if (totalCargos > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "ChargeTotalAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(totalCargos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // 34. Importe total de la venta, cesión en uso o del servicio prestado
        writer.WriteStartElement(prefixCbc, "PayableAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue(oDocumento.Importefinal.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        writer.WriteEndElement();
      
    }

    
    private static void ObtenerDatosDetalleNotaCredito21(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_DetalleDocumento listaDetalle, string moneda, EN_Documento oDocumento)
    {
        // DESCRIPCION: Funcion para generar los datos del detalle del XML en formato UBL 2.1 -- NOTA DE CREDITO
        
        NE_Facturacion objFacturacion = new NE_Facturacion();           // CLASE NEGOCIO FACTURACION

        // Datos del item unidad, cantidad, precio_unitario, valorventa
        writer.WriteStartElement(prefixCac, "CreditNoteLine", cadCac);
        // Número de orden del Ítem
        writer.WriteElementString("ID", cadCbc, listaDetalle.Lineid.ToString());

        // Cantidad de unidades por ítem
        writer.WriteStartElement(prefixCbc, "CreditedQuantity", cadCbc);
        writer.WriteAttributeString("unitCode", listaDetalle.Unidad.ToString());
        writer.WriteValue((Math.Round(listaDetalle.Cantidad, 10)).ToString().Replace(",", "."));
        writer.WriteEndElement();

        // Valor de venta del ítem
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valorventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }
        else
        {
            writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // Precio de venta unitario por item y código
        // Valor referencial unitario por ítem en operaciones no onerosas
        writer.WriteStartElement(prefixCac, "PricingReference", cadCac);
        // Validamos si el valor venta es mayor a 0 no es gratuito
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "AlternativeConditionPrice", cadCac);
            // Precio Unitario
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Preciounitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            // Código
            writer.WriteStartElement(prefixCbc, "PriceTypeCode", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerCodigoSunat("016", "001").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        else
        {
            // Es gratuito Precioreferencial
            writer.WriteStartElement(prefixCac, "AlternativeConditionPrice", cadCac);
            // Precio Unitario
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Valorunitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            // Código
            writer.WriteStartElement(prefixCbc, "PriceTypeCode", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerCodigoSunat("016", "002").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();

        // 40. Descuentos / Cargos por Item
        decimal valbruto;
        if (listaDetalle.Valorbruto > EN_Constante.g_const_0)
            valbruto = (decimal)listaDetalle.Valorbruto;
        else
            // Gratuito
            valbruto = (decimal)((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario);

        if (listaDetalle.Valordscto > EN_Constante.g_const_0)
        {
            // Descuentos que afectan la base imponible
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "false");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "00"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (Math.Round(listaDetalle.Valordscto / (double)valbruto, 5)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valordscto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(valbruto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        if (listaDetalle.Valorcargo > EN_Constante.g_const_0)
        {
            // Cargos que afectan la base imponible
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "47"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (Math.Round(listaDetalle.Valorcargo / (double)valbruto, 5)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valorcargo.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(valbruto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        decimal valorventa;
        decimal montoTributo;

        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            valorventa = (decimal)((decimal)listaDetalle.Valorventa + (decimal)listaDetalle.Isc);
            montoTributo = (decimal)(listaDetalle.Igv + listaDetalle.Isc);
        }
        else
        {
            // Operacion Gratuita
            valorventa = (decimal)((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario);
            montoTributo = 0;
        }

        writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(montoTributo.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // 42. Afectación al IGV por ítem
        string tipoafect;
        // 10 -- Gravado Operación Onerosa
        // 11, 12, 13, 14, 15, 16 -- Gravado Retiro - Bonificaciones (TG)
        // 20 -- Exonerado Operación Onerosa
        // 21 -- Exonerado Transferencia gratuita
        // 30 -- Inafecto Operación Onerosa
        // 31, 32, 33, 34, 35, 36, 37 -- Inafecto Retiro - TG
        // 40 -- Exportaciones

        switch (listaDetalle.Afectacionigv.Trim())
        {
            case "10":
                {
                    tipoafect = "1000";
                    break;
                }

            case "11":
            case "12":
            case "13":
            case "14":
            case "15":
            case "16":
                {
                    tipoafect = "9996";
                    break;
                }

              //IVAP

            case "17":
                {
                    tipoafect = "1016";
                    break;
                }

            case "20":
                {
                    tipoafect = "9997";
                    break;
                }

            case "21":
                {
                    tipoafect = "9996";
                    break;
                }

            case "30":
                {
                    tipoafect = "9998";
                    break;
                }

            case "31":
            case "32":
            case "33":
            case "34":
            case "35":
            case "36":
            case "37":
                {
                    tipoafect = "9996";
                    break;
                }

            case "40":
                {
                    if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                        tipoafect = "9996";
                    else
                        tipoafect = "9995";
                    break;
                }

            default:
                {
                    tipoafect = "1000";
                    break;
                }
        }

        // Afectacion IGV por ITEM
        writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(valorventa.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(listaDetalle.Igv.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
        writer.WriteElementString("Percent", cadCbc, (Math.Round(listaDetalle.factorIgv, 5)).ToString().Replace(",", "."));
        writer.WriteStartElement(prefixCbc, "TaxExemptionReasonCode", cadCbc);
        writer.WriteValue(listaDetalle.Afectacionigv.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteValue(tipoafect);
        writer.WriteEndElement();
        writer.WriteElementString("Name", cadCbc, objFacturacion.ObtenerDescripcionPorCodElemento("005", tipoafect).Trim());
        writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", tipoafect).Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Afectación ISC por ítem
        if (listaDetalle.AfectoIsc == EN_Constante.g_const_si)
        {
            decimal montobaseisc;
            if (listaDetalle.SistemaISC == "01")
                // Para el tipocalculoisc 01 Sistema al valor
                montobaseisc = (decimal)listaDetalle.Valorventa;
            else if (listaDetalle.SistemaISC == "02")
                // Para el tipocalculoisc 02 Aplicacion al Monto Fijo - Por confirmar formula
                montobaseisc = (decimal)listaDetalle.Valorventa;
            else if (listaDetalle.SistemaISC == "03")
                // Para el tipocalculoisc 03 Sistema de Precio de Venta al Publico
                montobaseisc = (decimal)(listaDetalle.Precioreferencial * listaDetalle.Cantidad);
            else
                montobaseisc = (decimal)listaDetalle.Valorventa;

            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((montobaseisc).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Isc.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteElementString("Percent", cadCbc, (Math.Round(listaDetalle.factorIsc, 5)).ToString().Replace(",", "."));
            writer.WriteElementString("TierRange", cadCbc, listaDetalle.SistemaISC.Trim());
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("2000");
            writer.WriteEndElement();
            writer.WriteElementString("Name", cadCbc, objFacturacion.ObtenerDescripcionPorCodElemento("005", "2000").Trim());
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "2000").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        writer.WriteEndElement();

        writer.WriteStartElement(prefixCac, "Item", cadCac);
        // 44. Descripción detallada del servicio prestado, bien vendido o cedido en uso, indicando las características
        writer.WriteStartElement(prefixCbc, "Description", cadCbc);
        writer.WriteCData(listaDetalle.Producto.Replace(Constants.vbCrLf, " ").Trim());
        writer.WriteEndElement();
        // Código de producto
        if (listaDetalle.Codigo.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "SellersItemIdentification", cadCac);
            writer.WriteElementString("ID", cadCbc, listaDetalle.Codigo.Trim());
            writer.WriteEndElement();
        }
        // Código de producto SUNAT
        if (listaDetalle.CodigoSunat.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "CommodityClassification", cadCac);
            writer.WriteStartElement(prefixCbc, "ItemClassificationCode", cadCbc);
            writer.WriteValue(listaDetalle.CodigoSunat.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();

        // Valor Unitario por Item
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "Price", cadCac);
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Valorunitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        else
        {
            // Gratuito
            writer.WriteStartElement(prefixCac, "Price", cadCac);
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(0);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();
    }

    
    private static void ObtenerDatosCabeceraNotaDebito21(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_Documento oDocumento, EN_Empresa oEmpresa, string CodMon)
    {
         //DESCRIPCION: Funcion para generar los datos de la cabecera del XML en formato UBL 2.1 -- NOTA DE DEBITO
        
        NE_Facturacion objFacturacion = new NE_Facturacion();       // CLASE NEGOCIO FACTURACION
        NE_DocPersona objDocPersona = new NE_DocPersona();          // CLASE NEGOCIO DOCPERSONA
        EN_DocPersona oDocPersona = new EN_DocPersona();            // CLASE ENTIDAD DOCPERSONA
        List<EN_DocPersona> listaDocPersona;                        // LISTA DE CLASE ENTIDAD DOCPERSONA
        string tipodoccliente_codsunat;                             // TIPO DE DOCUMENTO DE CLIENTE DE CODIGO SUNAT
        string nrodocumentocliente;                                 // NUMERO DE DOCUMENTO DE CLIENTE   
        NE_Documento objDocumentoRef = new NE_Documento();          // CLASE NEGOCIO DOCUMENTO
        EN_DocReferencia oDocumentoRef = new EN_DocReferencia();    // CLASE ENTIDAD DOCREFERENCIA
        List<EN_DocReferencia> listaNCNDDocRefencia;                // LISTA DE CLASE ENTIDAD DOCREFERENCIA            
        EN_DocRefeGuia oDocRefeGuia = new EN_DocRefeGuia();         // CLASE ENTIDAD DOCREFEGUIA
        string MontoLetras;                                         // MONTO LETRAS
        NumeroALetras numeroALetras = new NumeroALetras();          // NUMERO A LETRAS

        // Listamos el codigo del tipo de documento de la empresa
        oDocPersona.Codigo = oEmpresa.Tipodocumento;
        oDocPersona.Estado = EN_Constante.g_const_vacio;
        oDocPersona.Indpersona = EN_Constante.g_const_vacio;
        listaDocPersona = (List<EN_DocPersona>)objDocPersona.listarDocPersona(oDocPersona,  oDocumento.Id, oDocumento.Idpuntoventa);

        // Listamos el código del tipo de documento del cliente y su número de documento
        if (oDocumento.Tipodoccliente.Trim() == EN_Constante.g_const_vacio)
        {
            tipodoccliente_codsunat = "0";
            nrodocumentocliente = "-";
        }
        else if (Strings.Trim(oDocumento.Tipodoccliente).Length == 1)
        {
            tipodoccliente_codsunat = oDocumento.Tipodoccliente.Trim();
            nrodocumentocliente = oDocumento.Nrodoccliente.Trim();
        }
        else
        {
            tipodoccliente_codsunat = objFacturacion.ObtenerCodigoSunat("006", oDocumento.Tipodoccliente.Trim());
            nrodocumentocliente = oDocumento.Nrodoccliente.Trim();
        }

        // Versión del UBL.
        writer.WriteElementString("UBLVersionID", cadCbc, "2.1");

        // Versión de la estructura del documento.
        writer.WriteElementString("CustomizationID", cadCbc, "2.0");

        // Numeración, conformada por serie y número correlativo.
        writer.WriteElementString("ID", cadCbc, oDocumento.Serie.Trim() + "-" + oDocumento.Numero.Trim());

        // Fecha de emisión.
        writer.WriteElementString("IssueDate", cadCbc, Convert.ToDateTime(oDocumento.Fecha).ToString(EN_Constante.g_const_formfechaGuion));

        // Hora de emisión.
        writer.WriteElementString("IssueTime", cadCbc, Convert.ToDateTime(oDocumento.Hora).ToString(EN_Constante.g_const_formhora));

        // Leyenda de Monto del importe de la venta
        MontoLetras = numeroALetras.Convertir(oDocumento.Importeventa);

        writer.WriteStartElement(prefixCbc, "Note", cadCbc);
        writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "1000"));
        writer.WriteValue(MontoLetras);
        writer.WriteEndElement();


        //PARA IVAP
        if(oDocumento.es_ivap=="SI") 
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "2007"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "2007"));
            writer.WriteEndElement();
        }


        // Leyenda de Transferencia Gratuita
        if ((oDocumento.Estransgratuita == EN_Constante.g_const_si))
        {
            writer.WriteStartElement(prefixCbc, "Note", cadCbc);
            writer.WriteAttributeString("languageLocaleID", objFacturacion.ObtenerCodigoSunat("052", "1002"));
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("052", "1002"));
            writer.WriteEndElement();
        }

        // Tipo de moneda.
        writer.WriteStartElement(prefixCbc, "DocumentCurrencyCode", cadCbc);
        writer.WriteValue(oDocumento.Moneda);
        writer.WriteEndElement();

        // Buscamos el documento que hace referencia la nota de credito o nota de debito
        string tipodocref = EN_Constante.g_const_vacio;
        string numerodocref = EN_Constante.g_const_vacio;
        oDocumentoRef.Idempresa = oDocumento.Idempresa;
        oDocumentoRef.Iddocumento = oDocumento.Id;
        oDocumentoRef.Idtipodocumento = oDocumento.Idtipodocumento;
        listaNCNDDocRefencia = (List<EN_DocReferencia>)objDocumentoRef.listarDocumentoReferencia(oDocumentoRef, oDocumento.Id, oDocumento.Idempresa);
        if (listaNCNDDocRefencia.Count > EN_Constante.g_const_0)
        {
            tipodocref = listaNCNDDocRefencia[0].Idtipodocumentoref;
            numerodocref = listaNCNDDocRefencia[0].Numerodocref;
        }

        writer.WriteStartElement(prefixCac, "DiscrepancyResponse", cadCac);
        writer.WriteElementString("ReferenceID", cadCbc, numerodocref); // Cambiar por Serie y número del documento que modifica
        writer.WriteElementString("ResponseCode", cadCbc, oDocumento.TipoNotaCredNotaDeb);
        if (oDocumento.Glosa != EN_Constante.g_const_vacio)
        {
            // writer.WriteElementString("Description", cadCbc, oDocumento.Glosa) 'Validar si en la glosa se guarda el motivo de la nota de credito
            writer.WriteStartElement(prefixCbc, "Description", cadCbc);
            writer.WriteCData(oDocumento.Glosa.Trim());
            writer.WriteEndElement();
        }
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "BillingReference", cadCac);
        writer.WriteStartElement(prefixCac, "InvoiceDocumentReference", cadCac);
        writer.WriteElementString("ID", cadCbc, numerodocref);
        writer.WriteElementString("DocumentTypeCode", cadCbc, tipodocref);
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Documentos Relacionados al comprobante
        // 12. Tipo y número de la guía de remisión relacionada con la operación

        // Listamos las guias de remision
        List<EN_DocRefeGuia> listaDocRefGuias;
        oDocRefeGuia.empresa_id = oDocumento.Idempresa;
        oDocRefeGuia.documento_id = oDocumento.Id;
        oDocRefeGuia.tipodocumento_id = oDocumento.Idtipodocumento;
        listaDocRefGuias = (List<EN_DocRefeGuia>)objDocumentoRef.listarDocRefeGuia(oDocRefeGuia, oDocumento.Id, oDocumento.Idempresa);
        if (listaDocRefGuias.Count > EN_Constante.g_const_0)
        {
            foreach (var item2 in listaDocRefGuias.ToList())
            {
                writer.WriteStartElement(prefixCac, "DespatchDocumentReference", cadCac);
                writer.WriteElementString("ID", cadCbc, item2.numero_guia.Trim());
                writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
                writer.WriteAttributeString("listAgencyName", "PE:SUNAT");
                writer.WriteAttributeString("listName", "SUNAT:Identificador de guía relacionada");
                writer.WriteAttributeString("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01");
                writer.WriteValue(item2.tipo_guia.Trim());
                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }
        else if (oDocumento.TipoGuiaRemision.Trim() != EN_Constante.g_const_vacio && oDocumento.GuiaRemision.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "DespatchDocumentReference", cadCac);
            writer.WriteElementString("ID", cadCbc, oDocumento.GuiaRemision.Trim());
            writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
            writer.WriteAttributeString("listAgencyName", "PE:SUNAT");
            writer.WriteAttributeString("listName", "SUNAT:Identificador de guía relacionada");
            writer.WriteAttributeString("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo01");
            writer.WriteValue(oDocumento.TipoGuiaRemision.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // X. Documento de referencia por servicio publico
        // /Invoice/cac:ContractDocumentReference

        // 13. Tipo y número de otro documento y/ código documento relacionado con la operación
        if (oDocumento.TipoDocOtroDocRef.Trim() != EN_Constante.g_const_vacio && oDocumento.OtroDocumentoRef.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "AdditionalDocumentReference", cadCac);
            writer.WriteElementString("ID", cadCbc, oDocumento.OtroDocumentoRef.Trim());
            writer.WriteStartElement(prefixCbc, "DocumentTypeCode", cadCbc);
            writer.WriteAttributeString("listAgencyName", "PE:SUNAT");
            writer.WriteAttributeString("listName", "SUNAT: Identificador de documento relacionado");
            writer.WriteAttributeString("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo12");
            writer.WriteValue(oDocumento.TipoDocOtroDocRef.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Datos de la empresa
        // Información adicional de la firma
        writer.WriteStartElement(prefixCac, "Signature", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.SignatureID.Trim());
        writer.WriteStartElement(prefixCac, "SignatoryParty", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.Nrodocumento.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "PartyName", cadCac);
        writer.WriteStartElement(prefixCbc, "Name", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "DigitalSignatureAttachment", cadCac);
        writer.WriteStartElement(prefixCac, "ExternalReference", cadCac);
        writer.WriteElementString("URI", cadCbc, oEmpresa.SignatureURI.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // RUC
        writer.WriteStartElement(prefixCac, "AccountingSupplierParty", cadCac);
        writer.WriteStartElement(prefixCac, "Party", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteAttributeString("schemeID", listaDocPersona[0].CodigoSunat.Trim());
        writer.WriteValue(oEmpresa.Nrodocumento.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        // Nombre Comercial
        if ((oEmpresa.Nomcomercial.Trim() != EN_Constante.g_const_vacio))
        {
            writer.WriteStartElement(prefixCac, "PartyName", cadCac);
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(oEmpresa.Nomcomercial.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
        // Razon Social
        writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "RegistrationAddress", cadCac);
        // Ubigeo
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteCData(oEmpresa.Coddis.Trim());
        writer.WriteEndElement();
        // Código de Establecimiento
        writer.WriteElementString("AddressTypeCode", cadCbc, oDocumento.CodigoEstablecimiento);
        // Provincia
        writer.WriteStartElement(prefixCbc, "CityName", cadCbc);
        writer.WriteCData(oEmpresa.Provincia.Trim());
        writer.WriteEndElement();
        // Departamento
        writer.WriteStartElement(prefixCbc, "CountrySubentity", cadCbc);
        writer.WriteCData(oEmpresa.Departamento.Trim());
        writer.WriteEndElement();
        // Distrito
        writer.WriteStartElement(prefixCbc, "District", cadCbc);
        writer.WriteCData(oEmpresa.Distrito.Trim());
        writer.WriteEndElement();
        // Dirección
        writer.WriteStartElement(prefixCac, "AddressLine", cadCac);
        writer.WriteStartElement(prefixCbc, "Line", cadCbc);
        writer.WriteCData(oEmpresa.Direccion.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        // Código de País
        writer.WriteStartElement(prefixCac, "Country", cadCac);
        writer.WriteElementString("IdentificationCode", cadCbc, oEmpresa.Codpais.Trim());
        writer.WriteEndElement();

        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // AccountingCustomerParty
        writer.WriteStartElement(prefixCac, "AccountingCustomerParty", cadCac);
        writer.WriteStartElement(prefixCac, "Party", cadCac);

        // Tipo y número de documento de identidad del adquirente o usuario.
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteAttributeString("schemeID", tipodoccliente_codsunat.Trim());
        writer.WriteValue(nrodocumentocliente);
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
        writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
        writer.WriteCData(oDocumento.Nombrecliente.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Calculos de Descuentos Cargos Impuestos y Totales
        decimal totalOperaciones;
        decimal totalImpuestos;
        decimal totalImpGratuitos;
        decimal factorDscto;
        decimal factorCargo;
        decimal totalDescuentos;
        decimal totalCargos;

        totalOperaciones = (decimal)(oDocumento.Valorpergravadas + oDocumento.Valorperinafectas + oDocumento.Valorperexoneradas + oDocumento.Valorperexportacion);
        totalImpuestos = (decimal)(oDocumento.Igvventa + oDocumento.Iscventa + oDocumento.OtrosTributos);
        totalImpGratuitos = (decimal)(oDocumento.SumaIgvGratuito + oDocumento.SumaIscGratuito);

        factorCargo = (decimal)(Math.Round(oDocumento.OtrosCargos / (double)100, 5));
        factorDscto = (decimal)(Math.Round(oDocumento.Descuento / (double)100, 5));
        totalDescuentos = (totalOperaciones + totalImpuestos) * factorDscto;
        totalCargos = (totalOperaciones + totalImpuestos) * factorCargo;

        if (oDocumento.Descuento > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "false");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "03"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (factorDscto).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalDescuentos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Otros Cargos ' Se han considerado que no afectan a la base imponible
        if (oDocumento.OtrosCargos > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "50"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (factorCargo).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalCargos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", oDocumento.Moneda);
            writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // TaxTotal - Monto Total de Impuestos
        writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue((totalImpuestos).ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // --- Operaciones Gravadas IGV -- 1000
        if ((oDocumento.Valorpergravadas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((oDocumento.Valorpergravadas).ToString("F2").Replace(",", "."));

            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Igvventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);

            //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteValue("1016");
            else                            writer.WriteValue("1000");

            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);

            //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "1016"));
            else                            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "1000"));
       
            writer.WriteEndElement();


            //IVAP
            if(oDocumento.es_ivap=="SI")    writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "1016"));
            else                            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "1000"));
            
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones Gravadas ISC -- 2000
        if ((oDocumento.Iscventa > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperiscreferenc.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Iscventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("2000");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "2000"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "2000"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones INAFECTAS FRE -- 9998
        if ((oDocumento.Valorperinafectas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((oDocumento.Valorperinafectas).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9998");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9998"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9998"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones EXONERADAS VAT -- 9997
        if ((oDocumento.Valorperexoneradas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperexoneradas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9997");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9997"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9997"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones EXPORTACIÓN FRE -- 9995 --- SI ES GRATUITO -- 9996
        if ((oDocumento.Valorperexportacion > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorperexportacion.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue("0.00");
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteValue("9996");
            else
                writer.WriteValue("9995");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9996"));
            else
                writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9995"));
            writer.WriteEndElement();
            if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9996"));
            else
                writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9995"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones GRATUITO FRE -- 9996
        if ((oDocumento.Valorpergratuitas > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorpergratuitas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(totalImpGratuitos);
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9996");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9996"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9996"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // --- Operaciones Gravadas OTH -- 9999
        if ((oDocumento.OtrosTributos > EN_Constante.g_const_0))
        {
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.Valorpergravadas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(oDocumento.OtrosTributos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("9999");
            writer.WriteEndElement();

            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9999"));
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9999"));
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        writer.WriteEndElement();

        // LegalMonetaryTotal
        writer.WriteStartElement(prefixCac, "RequestedMonetaryTotal", cadCac);


        // Total Impuestos = IGV + ISC + Otros Tributos
        writer.WriteStartElement(prefixCbc, "TaxExclusiveAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue(totalImpuestos.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // Total Precio de Venta (ValorVenta + Impuestos) --- Impuestos = IGV + ISC + Otros Tributos
        writer.WriteStartElement(prefixCbc, "TaxInclusiveAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue((totalOperaciones + totalImpuestos).ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // Total de Descuentos (que no afectan la base imponible)
        if (totalDescuentos > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "AllowanceTotalAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue((totalDescuentos).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // Total de Cargos (que no afectan la base imponible)
        if (totalCargos > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "ChargeTotalAmount", cadCbc);
            writer.WriteAttributeString("currencyID", CodMon);
            writer.WriteValue(totalCargos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // 34. Importe total de la venta, cesión en uso o del servicio prestado
        writer.WriteStartElement(prefixCbc, "PayableAmount", cadCbc);
        writer.WriteAttributeString("currencyID", CodMon);
        writer.WriteValue(oDocumento.Importefinal.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        writer.WriteEndElement();

    }

    // Funcion para generar los datos del detalle del XML en formato UBL 2.1 -- NOTA DE DEBITO
    private static void ObtenerDatosDetalleNotaDebito21(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_DetalleDocumento listaDetalle, string moneda, EN_Documento oDocumento)
    {
        NE_Facturacion objFacturacion = new NE_Facturacion();           // CLASE NEGOCIO FACTURACION

        // Datos del item unidad, cantidad, precio_unitario, valorventa
        writer.WriteStartElement(prefixCac, "DebitNoteLine", cadCac);
        // 35. Número de orden del Ítem
        writer.WriteElementString("ID", cadCbc, listaDetalle.Lineid.ToString());

        // Cantidad de unidades por ítem
        writer.WriteStartElement(prefixCbc, "DebitedQuantity", cadCbc);
        writer.WriteAttributeString("unitCode", listaDetalle.Unidad.ToString());
        writer.WriteValue((Math.Round(listaDetalle.Cantidad, 10)).ToString().Replace(",", "."));
        writer.WriteEndElement();

        // 37. Valor de venta del ítem
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valorventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }
        else
        {
            writer.WriteStartElement(prefixCbc, "LineExtensionAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
        }

        // 38. Precio de venta unitario por item y código
        // 39. Valor referencial unitario por ítem en operaciones no onerosas
        writer.WriteStartElement(prefixCac, "PricingReference", cadCac);
        // Validamos si el valor venta es mayor a 0 no es gratuito
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "AlternativeConditionPrice", cadCac);
            // Precio Unitario
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Preciounitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            // Código
            writer.WriteStartElement(prefixCbc, "PriceTypeCode", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerCodigoSunat("016", "001").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        else
        {
            // Es gratuito Precioreferencial
            writer.WriteStartElement(prefixCac, "AlternativeConditionPrice", cadCac);
            // Precio Unitario
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Valorunitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            // Código
            writer.WriteStartElement(prefixCbc, "PriceTypeCode", cadCbc);
            writer.WriteValue(objFacturacion.ObtenerCodigoSunat("016", "002").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();

        // 40. Descuentos / Cargos por Item
        decimal valbruto;
        if (listaDetalle.Valorbruto > EN_Constante.g_const_0)
            valbruto = (decimal)listaDetalle.Valorbruto;
        else
            // Gratuito
            valbruto = (decimal)((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario);

        if (listaDetalle.Valordscto > EN_Constante.g_const_0)
        {
            // Descuentos que afectan la base imponible
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "false");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "00"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (Math.Round(listaDetalle.Valordscto / (double)valbruto, 5)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valordscto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(valbruto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        if (listaDetalle.Valorcargo > EN_Constante.g_const_0)
        {
            // Cargos que afectan la base imponible
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");
            writer.WriteElementString("AllowanceChargeReasonCode", cadCbc, objFacturacion.ObtenerCodigoSunat("053", "47"));
            writer.WriteElementString("MultiplierFactorNumeric", cadCbc, (Math.Round(listaDetalle.Valorcargo / (double)valbruto, 5)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Valorcargo.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "BaseAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(valbruto.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        decimal valorventa;
        decimal montoTributo;

        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            valorventa = (decimal)((decimal)listaDetalle.Valorventa + (decimal)listaDetalle.Isc);
            montoTributo = (decimal)(listaDetalle.Igv + listaDetalle.Isc);
        }
        else
        {
            // Operacion Gratuita
            valorventa = (decimal)((decimal)listaDetalle.Cantidad * (decimal)listaDetalle.Valorunitario);
            montoTributo = 0;
        }

        writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(montoTributo.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // 42. Afectación al IGV por ítem
        string tipoafect;
        // 10 -- Gravado Operación Onerosa
        // 11, 12, 13, 14, 15, 16 -- Gravado Retiro - Bonificaciones (TG)
        // 20 -- Exonerado Operación Onerosa
        // 21 -- Exonerado Transferencia gratuita
        // 30 -- Inafecto Operación Onerosa
        // 31, 32, 33, 34, 35, 36, 37 -- Inafecto Retiro - TG
        // 40 -- Exportaciones

        switch (listaDetalle.Afectacionigv.Trim())
        {
            case "10":
                {
                    tipoafect = "1000";
                    break;
                }

            case "11":
            case "12":
            case "13":
            case "14":
            case "15":
            case "16":
                {
                    tipoafect = "9996";
                    break;
                }

              //IVAP

            case "17":
                {
                    tipoafect = "1016";
                    break;
                }

            case "20":
                {
                    tipoafect = "9997";
                    break;
                }

            case "21":
                {
                    tipoafect = "9996";
                    break;
                }

            case "30":
                {
                    tipoafect = "9998";
                    break;
                }

            case "31":
            case "32":
            case "33":
            case "34":
            case "35":
            case "36":
            case "37":
                {
                    tipoafect = "9996";
                    break;
                }

            case "40":
                {
                    if (oDocumento.Estransgratuita == EN_Constante.g_const_si)
                        tipoafect = "9996";
                    else
                        tipoafect = "9995";
                    break;
                }

            default:
                {
                    tipoafect = "1000";
                    break;
                }
        }

        // Afectacion IGV por ITEM
        writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(valorventa.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(listaDetalle.Igv.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
        writer.WriteElementString("Percent", cadCbc, (Math.Round(listaDetalle.factorIgv, 5)).ToString().Replace(",", "."));
        writer.WriteStartElement(prefixCbc, "TaxExemptionReasonCode", cadCbc);
        writer.WriteValue(listaDetalle.Afectacionigv.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
        writer.WriteStartElement(prefixCbc, "ID", cadCbc);
        writer.WriteValue(tipoafect);
        writer.WriteEndElement();
        writer.WriteElementString("Name", cadCbc, objFacturacion.ObtenerDescripcionPorCodElemento("005", tipoafect).Trim());
        writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", tipoafect).Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Afectación ISC por ítem
        if (listaDetalle.AfectoIsc == EN_Constante.g_const_si)
        {
            decimal montobaseisc;
            if (listaDetalle.SistemaISC == "01")
                // Para el tipocalculoisc 01 Sistema al valor
                montobaseisc = (decimal)listaDetalle.Valorventa;
            else if (listaDetalle.SistemaISC == "02")
                // Para el tipocalculoisc 02 Aplicacion al Monto Fijo - Por confirmar formula
                montobaseisc = (decimal)(listaDetalle.Valorventa);
            else if (listaDetalle.SistemaISC == "03")
                // Para el tipocalculoisc 03 Sistema de Precio de Venta al Publico
                montobaseisc = (decimal)(listaDetalle.Precioreferencial * listaDetalle.Cantidad);
            else
                montobaseisc = (decimal)listaDetalle.Valorventa;

            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((montobaseisc).ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Isc.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteElementString("Percent", cadCbc, (Math.Round(listaDetalle.factorIsc, 5)).ToString().Replace(",", "."));
            writer.WriteElementString("TierRange", cadCbc, listaDetalle.SistemaISC.Trim());
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteStartElement(prefixCbc, "ID", cadCbc);
            writer.WriteValue("2000");
            writer.WriteEndElement();
            writer.WriteElementString("Name", cadCbc, objFacturacion.ObtenerDescripcionPorCodElemento("005", "2000").Trim());
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "2000").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        writer.WriteEndElement();

        writer.WriteStartElement(prefixCac, "Item", cadCac);
        // 44. Descripción detallada del servicio prestado, bien vendido o cedido en uso, indicando las características
        writer.WriteStartElement(prefixCbc, "Description", cadCbc);
        writer.WriteCData(listaDetalle.Producto.Replace(Constants.vbCrLf, " ").Trim());
        writer.WriteEndElement();
        // Código de producto
        if (listaDetalle.Codigo.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "SellersItemIdentification", cadCac);
            writer.WriteElementString("ID", cadCbc, listaDetalle.Codigo.Trim());
            writer.WriteEndElement();
        }
        // Código de producto SUNAT
        if (listaDetalle.CodigoSunat.Trim() != EN_Constante.g_const_vacio)
        {
            writer.WriteStartElement(prefixCac, "CommodityClassification", cadCac);
            writer.WriteStartElement(prefixCbc, "ItemClassificationCode", cadCbc);
            writer.WriteValue(listaDetalle.CodigoSunat.Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();

        // 48. Valor Unitario por Item
        if (listaDetalle.Valorventa > EN_Constante.g_const_0)
        {
            writer.WriteStartElement(prefixCac, "Price", cadCac);
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue((Math.Round(listaDetalle.Valorunitario, 10)).ToString().Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        else
        {
            // Gratuito
            writer.WriteStartElement(prefixCac, "Price", cadCac);
            writer.WriteStartElement(prefixCbc, "PriceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(0);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }
        writer.WriteEndElement();
    }

    

    
    private static void AgregarFirma21(EN_Certificado oCertificado, string rutadoc, string rutaemp, string Nombre, ref string valorResumen, ref string firma, string TipoDoc, EN_ComprobanteSunat oCompSunat, int numExtension, ref string cadenaXML, string idDoc, int empresaId)    
    {
        // DESCRIPCION: Función para agregar la firma al comprobante generado

        try
        {
            string local_xmlArchivo = rutadoc + Nombre + EN_Constante.g_const_extension_xml;    // RUTA ARCHIVO XML
            string local_typoDocumento;                             // TIPO DE DOCUMENTO
            local_typoDocumento = TipoDoc;

            
            X509Certificate2 MiCertificado = new X509Certificate2(rutaemp + oCertificado.Nombre, oCertificado.ClaveCertificado, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Load(local_xmlArchivo);

            SignedXml signedXml = new SignedXml(xmlDoc);
            signedXml.SigningKey = MiCertificado.PrivateKey;
            
            KeyInfo KeyInfo = new KeyInfo();

            Reference Reference = new Reference();
            Reference.Uri = EN_Constante.g_const_vacio;

            Reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());

            signedXml.AddReference(Reference);

            X509Chain X509Chain = new X509Chain();
            X509Chain.Build(MiCertificado);

            X509ChainElement local_element = X509Chain.ChainElements[0];
            KeyInfoX509Data x509Data = new KeyInfoX509Data(local_element.Certificate);
            string subjectName = local_element.Certificate.Subject;

            x509Data.AddSubjectName(subjectName);
            KeyInfo.AddClause(x509Data);

            signedXml.KeyInfo = KeyInfo;
            signedXml.ComputeSignature();

            XmlElement signature = signedXml.GetXml();
            signature.Prefix = "ds";
            signedXml.ComputeSignature();

            foreach (XmlNode node in signature.SelectNodes("descendant-or-self::*[namespace-uri()='http://www.w3.org/2000/09/xmldsig#']"))
            {
                if (node.LocalName == "Signature")
                {
                    XmlAttribute newAttribute = xmlDoc.CreateAttribute("Id");
                    newAttribute.Value = oCertificado.IdSignature;
                    node.Attributes.Append(newAttribute);
                    break;
                }
            }

            string local_xpath = EN_Constante.g_const_vacio;
            XmlNamespaceManager nsMgr;
            nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);

            nsMgr.AddNamespace("xsi", EN_Constante.g_const_url_namespace_xsi);
            nsMgr.AddNamespace("xsd", EN_Constante.g_const_url_namespace_xsd);
            nsMgr.AddNamespace("ds", EN_Constante.g_const_url_namespace_ds);
            nsMgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
            if (local_typoDocumento == oCompSunat.IdFC || local_typoDocumento == oCompSunat.IdBV || local_typoDocumento == oCompSunat.IdNC || local_typoDocumento == oCompSunat.IdND)
                nsMgr.AddNamespace("qdt", "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2");
            if (local_typoDocumento == oCompSunat.IdFC || local_typoDocumento == oCompSunat.IdBV || local_typoDocumento == oCompSunat.IdNC || local_typoDocumento == oCompSunat.IdND)
                nsMgr.AddNamespace("udt", "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2");
            nsMgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
            nsMgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
            if (local_typoDocumento == oCompSunat.IdFC || local_typoDocumento == oCompSunat.IdBV || local_typoDocumento == oCompSunat.IdNC || local_typoDocumento == oCompSunat.IdND)
                nsMgr.AddNamespace("ccts", "urn:un:unece:uncefact:documentation:2");
            nsMgr.AddNamespace("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1");

                switch (local_typoDocumento)
                {
                    case var @case when @case == oCompSunat.IdFC:
                    case var case1 when case1 == oCompSunat.IdBV: // Factura / boleta  En la cabecera tiene [3] nodos UBLExtension
                        {
                            nsMgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2");
                            if (numExtension == 2)
                            {
                                local_xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension[2]/ext:ExtensionContent";
                            }
                            else if (numExtension == 3)
                            {
                                local_xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension[3]/ext:ExtensionContent";
                            }
                            else
                            {
                                local_xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension[1]/ext:ExtensionContent";
                            }

                            break;
                        }

                    case var case2 when case2 == oCompSunat.IdNC: // Nota de credito En la cabecera tiene [3] nodos UBLExtension
                        {
                            nsMgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2");
                            if (numExtension == 2)
                            {
                                local_xpath = "/tns:CreditNote/ext:UBLExtensions/ext:UBLExtension[2]/ext:ExtensionContent";
                            }
                            else if (numExtension == 3)
                            {
                                local_xpath = "/tns:CreditNote/ext:UBLExtensions/ext:UBLExtension[3]/ext:ExtensionContent";
                            }
                            else
                            {
                                local_xpath = "/tns:CreditNote/ext:UBLExtensions/ext:UBLExtension[1]/ext:ExtensionContent";
                            }

                            break;
                        }

                    case var case3 when case3 == oCompSunat.IdND: // Nota de debito En la cabecera tiene [3] nodos UBLExtension
                        {
                            nsMgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2");
                            if (numExtension == 2)
                            {
                                local_xpath = "/tns:DebitNote/ext:UBLExtensions/ext:UBLExtension[2]/ext:ExtensionContent";
                            }
                            else if (numExtension == 3)
                            {
                                local_xpath = "/tns:DebitNote/ext:UBLExtensions/ext:UBLExtension[3]/ext:ExtensionContent";
                            }
                            else
                            {
                                local_xpath = "/tns:DebitNote/ext:UBLExtensions/ext:UBLExtension[1]/ext:ExtensionContent";
                            }

                            break;
                        }

                    case var case4 when case4 == oCompSunat.IdCB:  // Communicacion de baja
                        {
                            nsMgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1");
                            local_xpath = "/tns:VoidedDocuments/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent";
                            break;
                        }

                    case var case5 when case5 == oCompSunat.IdRD: // Resumen de diario
                        {
                            nsMgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1");
                            local_xpath = "/tns:SummaryDocuments/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent";
                            break;
                        }
                }

                xmlDoc.SelectSingleNode(local_xpath, nsMgr).AppendChild(xmlDoc.ImportNode(signature, true));

            valorResumen = signature.ChildNodes.Item(0).ChildNodes.Item(2).ChildNodes.Item(2).FirstChild.Value;
            firma = signature.ChildNodes.Item(1).FirstChild.Value;

            System.IO.StreamWriter tw = new System.IO.StreamWriter(local_xmlArchivo, false, new System.Text.UTF8Encoding(false));
            xmlDoc.Save(tw);

            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("ds:Signature");

            // el nodo <ds:Signature> debe existir unicamente 1 vez
             if (nodeList.Count != 1){
                throw new ArgumentNullException("Se produjo un error en la firma del documento");
               
            }

            signedXml.LoadXml((XmlElement)nodeList[0]);

            cadenaXML = xmlDoc.OuterXml.ToString().Trim();
            
        }

        catch (Exception ex)
        {
            throw ex;
        }
    }

    

      

        
        public static bool GuardarNombreValorResumenFirma(EN_Documento oDocumento, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
    {
        // DESCRIPCION: Inicio Guardar el documento electronico y actualizar estado del documento 
        NE_Facturacion objFacturacion = new NE_Facturacion();       // CLASE NEGOCIO FACTURACION
        return objFacturacion.GuardarNombreValorResumenFirma(oDocumento, nombreXML, valorResumen, valorFirma, xmlgenerado);
    }
 
  
    
    
    public static bool ValidarUsuarioEmpresa(int p_idempresa, string p_usuario, string p_clave, string documentoId, int Idpuntoventa)
    {
          // DESCRIPCION: Validaciones  
        NE_Certificado objCert = new NE_Certificado();          // CLASE NEGOCIO CERTIFICADO
        if ((bool)objCert.validarEmpresaCertificado(p_idempresa, p_usuario, p_clave,  documentoId,  Idpuntoventa) == true)
            return true;
        else
            return false;
    }


// /*************************FIN VERSION UBL 2.1*****************************/


    public static void EnviarXmlDocumentoElectronico(EN_Documento oDocumento, EN_Certificado oCertificado, EN_ComprobanteSunat oCompSunat, string produccion,string ruta, string nombre, ref string msjWA, ref int codRspta , ref string estado_comprobante)
    {
        // DESCRIPCION: Este metodo se encarga de enviar a SUNAT el comprobante generado
        EN_RequestEnviodoc data=new EN_RequestEnviodoc() ;      // CLASE ENTIDAD SOLICITUD ENVIO DOCUMENTO

        // ParallelEnumerable validar xml (AQUI DEBE IR VALIDACION XSD Y XSL)
        try
        {
        
                data.flagOse= oCertificado.flagOSE;
                data.ruc= oDocumento.NroDocEmpresa;
                data.nombreArchivo= nombre.Trim();
                data.certUserName = oCertificado.UserName;
                data.certClave = oCertificado.Clave;
                data.Produccion= produccion;
                data.documentoId= oDocumento.Id;
                data.empresaId = oDocumento.Idempresa.ToString(); 
                data.Idpuntoventa = oDocumento.Idpuntoventa.ToString();
                data.tipodocumentoId = oDocumento.Idtipodocumento.ToString();
                data.fecha = oDocumento.Fecha;
                data.Serie= oDocumento.Serie;
                data.Numero= oDocumento.Numero;
                
                var urlWA =  EN_ConfigConstantes.Instance.const_urlServiceDoc +  EN_ConfigConstantes.Instance.const_apiSendDoc;
                string dataSerializado = System.Text.Json.JsonSerializer.Serialize(data);

                dynamic responseWA= Configuracion.sendWebApi(urlWA, dataSerializado);
                
                if(responseWA.RptaRegistroCpeDoc.FlagVerificacion){
                    msjWA= responseWA.RptaRegistroCpeDoc.MensajeResp;
                    codRspta = EN_Constante.g_const_0;

                }else{
                    msjWA= "Error al enviar XML- " +responseWA.RptaRegistroCpeDoc.MensajeResp + " | Error al enviar XML-"+responseWA.ErrorWebService.DescripcionErr;
                    codRspta = EN_Constante.g_const_1;
                }

                estado_comprobante= responseWA.RptaRegistroCpeDoc.estadoComprobante;
            
                
        }
        catch (Exception ex)
        {
            msjWA = "Se generó el XML - Error al enviar XML: " + ex.Message;
            codRspta = EN_Constante.g_const_2;
        
        }

    }

     /************************* DOCUMENTOS DE BAJA *****************************/


   
    public static bool GenerarXmlBaja(EN_Baja oBaja, EN_ComprobanteSunat oCompSunat, EN_Empresa oEmpresa, EN_Certificado oCertificado, string ruta, string rutabase, ref string nombre, ref string vResumen, ref string vFirma, ref string msjRspta, ref string cadenaXML)
    {
        // Create XmlWriterSettings.
        var settings = new XmlWriterSettings();
        string prefixCac;
        string prefixCbc;
        string prefixDs;
        string prefixExt;
        string prefixSac;
        string cadCac = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2";
        string cadCbc = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2";
        string cadDs = "http://www.w3.org/2000/09/xmldsig#";
        string cadExt = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2";
        string cadSac = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1";
        string cadQdt = "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2";
        var objProceso = new NE_Baja();
        List<EN_DetalleBaja> listaDetalle;


        string rutaemp;
        rutaemp = string.Format("{0}/{1}/", rutabase, oBaja.Nrodocumento);
        ruta=ruta + EN_Constante.g_const_rutaSufijo_xml;


        // Validamos si existe directorio del documento, de lo contrario lo creamos
        if (!Directory.Exists(ruta))
        {
            Directory.CreateDirectory(ruta);
        }

                    // CREAMOS TODOS LOS DIRECTORIOS NECESARIOS. SI NO EXISTE
        if ((!System.IO.Directory.Exists(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_xml)))
            System.IO.Directory.CreateDirectory(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_xml);

        if ((!System.IO.Directory.Exists(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_pdf)))
            System.IO.Directory.CreateDirectory(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_pdf);

            if ((!System.IO.Directory.Exists(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_cdr)))
            System.IO.Directory.CreateDirectory(rutaemp.Trim() + EN_Constante.g_const_rutaSufijo_cdr);


        // Validamos si existe comprobante en XML, ZIP y PDF generado y eliminamos
        if (File.Exists(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml))
        {
            File.Delete(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_xml);
        }

        if (File.Exists(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip))
        {
            File.Delete(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_zip);
        }

        if (File.Exists(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_pdf))
        {
            File.Delete(ruta.Trim() + nombre.Trim() + EN_Constante.g_const_extension_pdf);
        }
        // Validamos si existe CDR en XML y ZIP y eliminamos
        if (File.Exists(ruta.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_xml))
        {
            File.Delete(ruta.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_xml);
        }

        if (File.Exists(ruta.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip))
        {
            File.Delete(ruta.Trim() + EN_Constante.g_const_prefijoXmlCdr_respuesta + nombre.Trim() + EN_Constante.g_const_extension_zip);
        }

        try
        {
            listaDetalle = (List<EN_DetalleBaja>)objProceso.listarDetalleBaja(oBaja);
            nombre = oBaja.Nrodocumento.Trim() + "-" + oBaja.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oBaja.FechaComunicacion), "yyyyMMdd") + "-" + oBaja.Correlativo.ToString();
            settings.Indent = true;
            // Create XmlWriter.
            settings.Encoding = Encoding.UTF8;
            using (XmlWriter writer = XmlWriter.Create(ruta.Trim() + nombre.Trim() + ".XML", settings))
            {
                prefixCac = writer.LookupPrefix(cadCac);
                prefixExt = writer.LookupPrefix(cadExt);
                prefixSac = writer.LookupPrefix(cadSac);
                prefixCbc = writer.LookupPrefix(cadCbc);
                prefixDs = writer.LookupPrefix(cadDs);
                // Begin writing.
                writer.WriteStartDocument();
                writer.WriteStartElement("VoidedDocuments", "urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1");
                writer.WriteAttributeString("xmlns", "cac", default, cadCac);
                writer.WriteAttributeString("xmlns", "cbc", default, cadCbc);
                writer.WriteAttributeString("xmlns", "ds", default, cadDs);
                writer.WriteAttributeString("xmlns", "ext", default, cadExt);
                writer.WriteAttributeString("xmlns", "qdt", default, cadQdt);
                writer.WriteAttributeString("xmlns", "sac", default, cadSac);
                // writer.WriteAttributeString("xmlns", "xsi", Nothing, cadXsi)

                ObtenerExtensionesBaja(writer, prefixExt, prefixSac, prefixCbc, prefixDs, cadExt, cadSac, cadCbc, cadDs);
                ObtenerDatosCabeceraBaja(writer, prefixCbc, prefixCac, cadCbc, cadCac, oBaja, oEmpresa);
                if (listaDetalle.Count > 0)
                {
                    foreach (var item in listaDetalle.ToList())
                        ObtenerDatosDetalleBaja(writer, prefixCbc, prefixCac, prefixSac, cadCbc, cadCac, cadSac, item);
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            AgregarFirma(oCertificado, ruta, rutaemp, nombre, ref vResumen, ref vFirma, oBaja.Tiporesumen, oCompSunat, 2, ref cadenaXML);
            // ValidarXML(ruta, nombre, oBaja.Tiporesumen.Trim, oCompSunat, rutabase);

             //COPIAMOS XML A S3 Y ELIMINAMOS DE TEMPORAL
            AppConfiguracion.FuncionesS3.CopyS3DeleteLocal(oBaja.Nrodocumento,rutabase,EN_Constante.g_const_rutaSufijo_xml,nombre.Trim(),EN_Constante.g_const_extension_xml);
           
            msjRspta = "Comunicado de Bajas Generado Correctamente";
            return true;
        }
        catch (Exception ex)
        {
            // En caso de error eliminamos el XML generado
            if (File.Exists(ruta.Trim() + nombre.Trim() + ".XML"))
            {
                File.Delete(ruta.Trim() + nombre.Trim() + ".XML");
            }
            // Guardamos en el LOG que hubo error al generar el XML
            msjRspta = "Error al generar Comunicado de Bajas: " + ex.Message.ToString();
            return false;
        }
    }

    private static object ObtenerExtensionesBaja(XmlWriter writer, string prefixExt, string prefixSac, string prefixCbc, string prefixDs, string cadExt, string cadSac, string cadCbc, string cadDs)
    {
        writer.WriteStartElement(prefixExt, "UBLExtensions", cadExt);
        writer.WriteStartElement(prefixExt, "UBLExtension", cadExt);
        writer.WriteStartElement(prefixExt, "ExtensionContent", cadExt);
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        return writer;
    }

    private static XmlWriter ObtenerDatosCabeceraBaja(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_Baja oBaja, EN_Empresa oEmpresa)
    {
        var objFacturacion = new NE_Facturacion();
        var objDocPersona = new NE_DocPersona();
        var oDocPersona = new EN_DocPersona();
        var listaDocPersona = new List<EN_DocPersona>();

        // Listamos el codigo del tipo de documento RUC
        oDocPersona.Codigo = "RUC";
        oDocPersona.Estado = "";
        oDocPersona.Indpersona = "";
        listaDocPersona =(List<EN_DocPersona>)objDocPersona.listarDocPersona(oDocPersona, EN_Constante.g_const_vacio,EN_Constante.g_const_0);
        writer.WriteElementString("UBLVersionID", cadCbc, "2.0");
        writer.WriteElementString("CustomizationID", cadCbc, "1.0");

        // Datos del resumen
        
        writer.WriteElementString("ID", cadCbc, oBaja.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oBaja.FechaComunicacion),EN_Constante.g_const_formfecha_yyyyMMdd) + "-" + oBaja.Correlativo.ToString());
        writer.WriteElementString("ReferenceDate", cadCbc, Convert.ToDateTime(oBaja.FechaDocumento).ToString(EN_Constante.g_const_formfechaGuion));
        writer.WriteElementString("IssueDate", cadCbc, Convert.ToDateTime(oBaja.FechaComunicacion).ToString(EN_Constante.g_const_formfechaGuion));

        // Datos de la empresa
        writer.WriteStartElement(prefixCac, "Signature", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.SignatureID.Trim());
        writer.WriteStartElement(prefixCac, "SignatoryParty", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.Nrodocumento.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "PartyName", cadCac);
        writer.WriteStartElement(prefixCbc, "Name", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "DigitalSignatureAttachment", cadCac);
        writer.WriteStartElement(prefixCac, "ExternalReference", cadCac);
        writer.WriteElementString("URI", cadCbc, oEmpresa.SignatureURI.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "AccountingSupplierParty", cadCac);
        writer.WriteElementString("CustomerAssignedAccountID", cadCbc, oEmpresa.Nrodocumento.Trim());
        writer.WriteElementString("AdditionalAccountID", cadCbc, listaDocPersona[EN_Constante.g_const_0].CodigoSunat.Trim());
        writer.WriteStartElement(prefixCac, "Party", cadCac);
        writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
        writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        return writer;
    }

    private static XmlWriter ObtenerDatosDetalleBaja(XmlWriter writer, string prefixCbc, string prefixCac, string prefixSac, string cadCbc, string cadCac, string cadSac, EN_DetalleBaja listaDetalle)
    {

        // Datos del item de baja
        writer.WriteStartElement(prefixCbc, "VoidedDocumentsLine", cadSac);
        writer.WriteElementString("LineID", cadCbc, listaDetalle.Item.ToString());
        writer.WriteElementString("DocumentTypeCode", cadCbc, listaDetalle.Tipodocumentoid);
        writer.WriteElementString("DocumentSerialID", cadSac, listaDetalle.Serie);
        writer.WriteElementString("DocumentNumberID", cadSac, listaDetalle.Numero);
        writer.WriteElementString("VoidReasonDescription", cadSac, listaDetalle.MotivoBaja);
        writer.WriteEndElement();
        return writer;
    }

    public static bool GuardarBajaNombreValorResumenFirma(EN_Baja oBaja, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
    {
        var objBaja = new NE_Baja();
        return objBaja.GuardarBajaNombreValorResumenFirma(oBaja, nombreXML, valorResumen, valorFirma, xmlgenerado);
    }


    
    private static void AgregarFirma(EN_Certificado oCertificado, string rutadoc, string rutaemp, string Nombre, ref string valorResumen, ref string firma, string TipoDoc, EN_ComprobanteSunat oCompSunat, int numExtension, ref string cadenaXML)
    {
        //DESCRIPCION: FUNCION PARA AGREGAR FIRMA
        try
        {
            string local_xmlArchivo = rutadoc + Nombre + EN_Constante.g_const_extension_xml;
            string local_typoDocumento;
            local_typoDocumento = TipoDoc;
            var MiCertificado = new X509Certificate2(rutaemp + oCertificado.Nombre, oCertificado.ClaveCertificado, X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
            var xmlDoc = new XmlDocument();
            xmlDoc.PreserveWhitespace = true;
            xmlDoc.Load(local_xmlArchivo);
            var signedXml = new SignedXml(xmlDoc);
            signedXml.SigningKey = MiCertificado.PrivateKey;
            var KeyInfo = new KeyInfo();
            var Reference = new Reference();
            Reference.Uri = "";
            Reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
            signedXml.AddReference(Reference);
            var X509Chain = new X509Chain();
            X509Chain.Build(MiCertificado);
            X509ChainElement local_element = X509Chain.ChainElements[EN_Constante.g_const_0];
            var x509Data = new KeyInfoX509Data(local_element.Certificate);
            string subjectName = local_element.Certificate.Subject;
            x509Data.AddSubjectName(subjectName);
            KeyInfo.AddClause(x509Data);
            signedXml.KeyInfo = KeyInfo;
            signedXml.ComputeSignature();
            XmlElement signature = signedXml.GetXml();
            signature.Prefix = "ds";
            signedXml.ComputeSignature();
            foreach (XmlNode node in signature.SelectNodes("descendant-or-self::*[namespace-uri()='http://www.w3.org/2000/09/xmldsig#']"))
            {
                if (node.LocalName == "Signature")
                {
                    XmlAttribute newAttribute = xmlDoc.CreateAttribute("Id");
                    newAttribute.Value = oCertificado.IdSignature;
                    node.Attributes.Append(newAttribute);
                    break;
                }
            }

            string local_xpath = "";
            XmlNamespaceManager nsMgr;
            nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);
            switch (local_typoDocumento ?? "")
            {
                case var @case when @case == oCompSunat.IdFC:
                case var case1 when case1 == oCompSunat.IdBV: // Factura / boleta  En la cabecera tiene [3] nodos UBLExtension
                    {
                        nsMgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2");
                        if (numExtension == 2)
                        {
                            local_xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension[2]/ext:ExtensionContent";
                        }
                        else if (numExtension == 3)
                        {
                            local_xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension[3]/ext:ExtensionContent";
                        }
                        else
                        {
                            local_xpath = "/tns:Invoice/ext:UBLExtensions/ext:UBLExtension[1]/ext:ExtensionContent";
                        }

                        break;
                    }

                case var case2 when case2 == oCompSunat.IdNC: // Nota de credito En la cabecera tiene [3] nodos UBLExtension
                    {
                        nsMgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2");
                        if (numExtension == 2)
                        {
                            local_xpath = "/tns:CreditNote/ext:UBLExtensions/ext:UBLExtension[2]/ext:ExtensionContent";
                        }
                        else if (numExtension == 3)
                        {
                            local_xpath = "/tns:CreditNote/ext:UBLExtensions/ext:UBLExtension[3]/ext:ExtensionContent";
                        }
                        else
                        {
                            local_xpath = "/tns:CreditNote/ext:UBLExtensions/ext:UBLExtension[1]/ext:ExtensionContent";
                        }

                        break;
                    }

                case var case3 when case3 == oCompSunat.IdND: // Nota de debito En la cabecera tiene [3] nodos UBLExtension
                    {
                        nsMgr.AddNamespace("tns", "urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2");
                        if (numExtension == 2)
                        {
                            local_xpath = "/tns:DebitNote/ext:UBLExtensions/ext:UBLExtension[2]/ext:ExtensionContent";
                        }
                        else if (numExtension == 3)
                        {
                            local_xpath = "/tns:DebitNote/ext:UBLExtensions/ext:UBLExtension[3]/ext:ExtensionContent";
                        }
                        else
                        {
                            local_xpath = "/tns:DebitNote/ext:UBLExtensions/ext:UBLExtension[1]/ext:ExtensionContent";
                        }

                        break;
                    }

                case var case4 when case4 == oCompSunat.IdCB:  // Communicacion de baja
                    {
                        nsMgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:VoidedDocuments-1");
                        local_xpath = "/tns:VoidedDocuments/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent";
                        break;
                    }

                case var case5 when case5 == oCompSunat.IdRD: // Resumen de diario
                    {
                        nsMgr.AddNamespace("tns", "urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1");
                        local_xpath = "/tns:SummaryDocuments/ext:UBLExtensions/ext:UBLExtension/ext:ExtensionContent";
                        break;
                    }
            }

            nsMgr.AddNamespace("cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
            nsMgr.AddNamespace("cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
            if (local_typoDocumento == oCompSunat.IdFC | local_typoDocumento == oCompSunat.IdBV | local_typoDocumento == oCompSunat.IdNC | local_typoDocumento == oCompSunat.IdND)
            {
                nsMgr.AddNamespace("ccts", "urn:un:unece:uncefact:documentation:2");
            }

            nsMgr.AddNamespace("ds", "http://www.w3.org/2000/09/xmldsig#");
            nsMgr.AddNamespace("ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
            if (local_typoDocumento == oCompSunat.IdFC | local_typoDocumento == oCompSunat.IdBV | local_typoDocumento == oCompSunat.IdNC | local_typoDocumento == oCompSunat.IdND)
            {
                nsMgr.AddNamespace("qdt", "urn:oasis:names:specification:ubl:schema:xsd:QualifiedDatatypes-2");
            }

            nsMgr.AddNamespace("sac", "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1");
            if (local_typoDocumento == oCompSunat.IdFC | local_typoDocumento == oCompSunat.IdBV | local_typoDocumento == oCompSunat.IdNC | local_typoDocumento == oCompSunat.IdND)
            {
                nsMgr.AddNamespace("udt", "urn:un:unece:uncefact:data:specification:UnqualifiedDataTypesSchemaModule:2");
            }

            nsMgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
            xmlDoc.SelectSingleNode(local_xpath, nsMgr).AppendChild(xmlDoc.ImportNode(signature, true));
            valorResumen = signature.ChildNodes.Item(0).ChildNodes.Item(2).ChildNodes.Item(2).FirstChild.Value;
            firma = signature.ChildNodes.Item(1).FirstChild.Value;
            
            var tw = new StreamWriter(local_xmlArchivo, false, new UTF8Encoding(false));
            xmlDoc.Save(tw);
            
            XmlNodeList nodeList = xmlDoc.GetElementsByTagName("ds:Signature");

            // el nodo <ds:Signature> debe existir unicamente 1 vez
            if (nodeList.Count != 1)
            {
                throw new Exception("Se produjo un error en la firma del documento");
            }

            signedXml.LoadXml((XmlElement)nodeList[0]);
           
            cadenaXML = xmlDoc.OuterXml.ToString().Trim();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    //RESUMEN DIARIO PARA BAJAS

    public static bool GenerarXmlResumen(EN_Resumen oResumen, EN_ComprobanteSunat oCompSunat, EN_Empresa oEmpresa, EN_Certificado oCertificado, string ruta, string rutabase, ref string nombre, ref string valorResumen, ref string firma, ref string msjRspta, ref string cadenaXML)
    {
        //DESCRIPCION: FUNCION PARA GENERAR XML RESUMEN

        XmlWriterSettings settings = new XmlWriterSettings();   // settings
        string prefixCac;   // prefijo Cac
        string prefixCbc;   // prefijo Cbc
        string prefixDs;    // prefijo Ds
        string prefixExt;   // prefijo Ext
        string prefixSac;   // prefijo Sac
        string cadCac = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"; //cadCac
        string cadCbc = "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"; //cadCbc
        string cadDs = "http://www.w3.org/2000/09/xmldsig#";        //cadDs
        string cadExt = "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2"; // cadExt
        string cadSac = "urn:sunat:names:specification:ubl:peru:schema:xsd:SunatAggregateComponents-1"; // cadSac
        string cadXsi = "http://www.w3.org/2001/XMLSchema-instance";    //cadXsi
        NE_Resumen objProceso = new NE_Resumen();       // clase de negocio resumen
        List<EN_DetalleResumen> listaDetalle;           // lista de clase entidad detalle resumen

        string rutaemp;     // ruta temporal

        rutaemp = string.Format("{0}/{1}/", rutabase, oResumen.Nrodocumento);
        ruta=ruta +"XML/";

        // Validamos si existe directorio del documento, de lo contrario lo creamos
        if ((!System.IO.Directory.Exists(ruta)))
            System.IO.Directory.CreateDirectory(ruta);

        // Validamos si existe comprobante en XML, ZIP y PDF generado y eliminamos
        if (File.Exists(ruta.Trim() + nombre.Trim() + ".XML"))
            File.Delete(ruta.Trim() + nombre.Trim() + ".XML");
        if (File.Exists(ruta.Trim() + nombre.Trim() + ".ZIP"))
            File.Delete(ruta.Trim() + nombre.Trim() + ".ZIP");
        if (File.Exists(ruta.Trim() + nombre.Trim() + ".PDF"))
            File.Delete(ruta.Trim() + nombre.Trim() + ".PDF");
        // Validamos si existe CDR en XML y ZIP y eliminamos
        if (File.Exists(ruta.Trim() + "R-" + nombre.Trim() + ".XML"))
            File.Delete(ruta.Trim() + "R-" + nombre.Trim() + ".XML");
        if (File.Exists(ruta.Trim() + "R-" + nombre.Trim() + ".ZIP"))
            File.Delete(ruta.Trim() + "R-" + nombre.Trim() + ".ZIP");

        try
        {
            listaDetalle = (List<EN_DetalleResumen>)objProceso.listarDetalleResumen(oResumen);
            nombre = oResumen.Nrodocumento.Trim() + "-" + oResumen.Tiporesumen.Trim() + "-" +
            Strings.Format(Conversions.ToDate(oResumen.FechaGeneraResumen), "yyyyMMdd")
             + "-" + oResumen.Correlativo.ToString();
            settings.Indent = true;
            // Create XmlWriter.
            settings.Encoding = System.Text.UTF8Encoding.UTF8;
            using (XmlWriter writer = XmlWriter.Create(ruta.Trim() + nombre.Trim() + ".XML", settings))
            {
                prefixCac = writer.LookupPrefix(cadCac);
                prefixExt = writer.LookupPrefix(cadExt);
                prefixSac = writer.LookupPrefix(cadSac);
                prefixCbc = writer.LookupPrefix(cadCbc);
                prefixDs = writer.LookupPrefix(cadDs);
                // Begin writing.
                writer.WriteStartDocument();
                writer.WriteStartElement("SummaryDocuments", "urn:sunat:names:specification:ubl:peru:schema:xsd:SummaryDocuments-1");

                writer.WriteAttributeString("xmlns", "cac", null/* TODO Change to default(_) if this is not a reference type */, cadCac);
                writer.WriteAttributeString("xmlns", "cbc", null/* TODO Change to default(_) if this is not a reference type */, cadCbc);
                writer.WriteAttributeString("xmlns", "ds", null/* TODO Change to default(_) if this is not a reference type */, cadDs);
                writer.WriteAttributeString("xmlns", "ext", null/* TODO Change to default(_) if this is not a reference type */, cadExt);
                writer.WriteAttributeString("xmlns", "sac", null/* TODO Change to default(_) if this is not a reference type */, cadSac);
                writer.WriteAttributeString("xmlns", "xsi", null/* TODO Change to default(_) if this is not a reference type */, cadXsi);

                ObtenerExtensionesResumen(writer, prefixExt, prefixSac, prefixCbc, prefixDs, cadExt, cadSac, cadCbc, cadDs);
                ObtenerDatosCabeceraResumen(writer, prefixCbc, prefixCac, cadCbc, cadCac, oResumen, oEmpresa);
                if (listaDetalle.Count > 0)
                {
                    foreach (var item in listaDetalle.ToList())
                        ObtenerDatosDetalleResumen(writer, prefixCbc, prefixCac, prefixSac, cadCbc, cadCac, cadSac, item);
                }
                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
            AgregarFirma(oCertificado, ruta, rutaemp, nombre, ref valorResumen,ref firma, oResumen.Tiporesumen, oCompSunat, 2, ref cadenaXML);
            // Me.ValidarXML(ruta, nombre, oResumen.Tiporesumen.Trim, oCompSunat, rutabase)
          
            msjRspta = "Resumen Diario Generado Correctamente";
            return true;
        }
        catch (Exception ex)
        {
            // En caso de error eliminamos el XML generado
            if (File.Exists(ruta.Trim() + nombre.Trim() + ".XML"))
                File.Delete(ruta.Trim() + nombre.Trim() + ".XML");
            // Guardamos en el LOG que hubo error al generar el XML
            msjRspta = "Error al generar Resumen Diario: " + ex.Message.ToString();
            return false;
        }
    }


     private static XmlWriter ObtenerExtensionesResumen(XmlWriter writer, string prefixExt, string prefixSac, string prefixCbc, string prefixDs, string cadExt, string cadSac, string cadCbc, string cadDs)
    {
        writer.WriteStartElement(prefixExt, "UBLExtensions", cadExt);
        writer.WriteStartElement(prefixExt, "UBLExtension", cadExt);
        writer.WriteStartElement(prefixExt, "ExtensionContent", cadExt);
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        return writer;
    }
    private static XmlWriter ObtenerDatosCabeceraResumen(XmlWriter writer, string prefixCbc, string prefixCac, string cadCbc, string cadCac, EN_Resumen oResumen, EN_Empresa oEmpresa)
    {
        //DESCRIPCION: FUNCION PARA OBTENER DATOS DE CABECERA RESUMEN

        NE_Facturacion objFacturacion = new NE_Facturacion();   // Clase de negocio facturación
        NE_DocPersona objDocPersona = new NE_DocPersona();      // clase de negocio docpersona
        EN_DocPersona oDocPersona = new EN_DocPersona();    // clase de entidad docpersona
        List<EN_DocPersona> listaDocPersona = new List<EN_DocPersona>();    // lista de clase entidad docpersona

        // Listamos el codigo del tipo de documento RUC
        oDocPersona.Codigo = "RUC";
        oDocPersona.Estado = "";
        oDocPersona.Indpersona = "";
        listaDocPersona =(List<EN_DocPersona>)objDocPersona.listarDocPersona(oDocPersona, oResumen.Id.ToString(), oEmpresa.Id);

        writer.WriteElementString("UBLVersionID", cadCbc, "2.0");
        writer.WriteElementString("CustomizationID", cadCbc, "1.1");

        // Datos del resumen
        writer.WriteElementString("ID", cadCbc, oResumen.Tiporesumen.Trim() + "-" +
        Strings.Format(Conversions.ToDate(oResumen.FechaGeneraResumen), "yyyyMMdd")
         + "-" + oResumen.Correlativo.ToString());
         
        writer.WriteElementString("ReferenceDate", cadCbc, Convert.ToDateTime(oResumen.FechaEmisionDocumento).ToString("yyyy-MM-dd"));
        writer.WriteElementString("IssueDate", cadCbc,  Convert.ToDateTime(oResumen.FechaGeneraResumen).ToString("yyyy-MM-dd"));

        // Datos de la empresa
        writer.WriteStartElement(prefixCac, "Signature", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.SignatureID.Trim());
        writer.WriteStartElement(prefixCac, "SignatoryParty", cadCac);
        writer.WriteStartElement(prefixCac, "PartyIdentification", cadCac);
        writer.WriteElementString("ID", cadCbc, oEmpresa.Nrodocumento.Trim());
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "PartyName", cadCac);
        writer.WriteStartElement(prefixCbc, "Name", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "DigitalSignatureAttachment", cadCac);
        writer.WriteStartElement(prefixCac, "ExternalReference", cadCac);
        writer.WriteElementString("URI", cadCbc, oEmpresa.SignatureURI.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "AccountingSupplierParty", cadCac);
        writer.WriteElementString("CustomerAssignedAccountID", cadCbc, oEmpresa.Nrodocumento.Trim());
        writer.WriteElementString("AdditionalAccountID", cadCbc, listaDocPersona[0].CodigoSunat.Trim());
        writer.WriteStartElement(prefixCac, "Party", cadCac);
        writer.WriteStartElement(prefixCac, "PartyLegalEntity", cadCac);
        writer.WriteStartElement(prefixCbc, "RegistrationName", cadCbc);
        writer.WriteCData(oEmpresa.RazonSocial.Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();

        writer.WriteEndElement();
        writer.WriteEndElement();

        return writer;
    }
    private static XmlWriter ObtenerDatosDetalleResumen(XmlWriter writer, string prefixCbc, string prefixCac, string prefixSac, string cadCbc, string cadCac, string cadSac, EN_DetalleResumen listaDetalle)
    {
        //DESCRIPCION: FUNCION PARA OBTENER DATOS  DE DETALLE RESUMEN

        NE_Facturacion objFacturacion = new NE_Facturacion();   // Clase de negocio facturación
        string moneda;      // moneda
        // Datos del item de Resumen
        moneda = listaDetalle.Moneda.Trim();
        writer.WriteStartElement(prefixSac, "SummaryDocumentsLine", cadSac);

        // Nro de Linea de Resumen
        writer.WriteElementString("LineID", cadCbc, listaDetalle.Item.ToString());
        // Serie y número de comprobante
        writer.WriteElementString("DocumentTypeCode", cadCbc, listaDetalle.Tipodocumentoid);
        writer.WriteElementString("ID", cadCbc, listaDetalle.Serie + "-" + listaDetalle.Correlativo);

        // Documento de identidad del cliente
        if (listaDetalle.NroDocCliente.Trim() != "")
        {
            writer.WriteStartElement(prefixCac, "AccountingCustomerParty", cadCac);
            writer.WriteElementString("CustomerAssignedAccountID", cadCbc, listaDetalle.NroDocCliente);
            writer.WriteElementString("AdditionalAccountID", cadCbc, listaDetalle.DocCliente);
            writer.WriteEndElement();
        }

        // Documento de Referencia
        if (listaDetalle.Tipodocumentoid == "07" | listaDetalle.Tipodocumentoid == "08")
        {
            writer.WriteStartElement(prefixCac, "BillingReference", cadCac);
            writer.WriteStartElement(prefixCac, "InvoiceDocumentReference", cadCac);
            writer.WriteElementString("ID", cadCbc, listaDetalle.Nrodocref);
            writer.WriteElementString("DocumentTypeCode", cadCbc, listaDetalle.Tipodocref);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Datos de la percepción
        if (listaDetalle.Regimenpercep != "")
        {
            writer.WriteStartElement(prefixSac, "SUNATPerceptionSummaryDocumentReference", cadSac);
            writer.WriteElementString("SUNATPerceptionSystemCode", cadSac, listaDetalle.Regimenpercep);
            writer.WriteElementString("SUNATPerceptionPercent", cadSac, (Math.Round(listaDetalle.Porcentpercep, 3)).ToString().Replace(",", "."));
            writer.WriteStartElement(prefixCbc, "TotalInvoiceAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Importepercep.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            // writer.WriteElementString("SUNATTotalCashed", cadSac, listaDetalle.Importefinal)
            writer.WriteStartElement(prefixSac, "SUNATTotalCashed", cadSac);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Importefinal.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "TaxableAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Importeventa.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Estado del ítem
        writer.WriteStartElement(prefixCac, "Status", cadCac);
        writer.WriteElementString("ConditionCode", cadCbc, listaDetalle.Condicion.ToString());
        writer.WriteEndElement();

        // Importe total de la venta
        writer.WriteStartElement(prefixSac, "TotalAmount", cadSac);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(listaDetalle.Importefinal.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();

        // Operaciones Gravadas
        if (listaDetalle.Opegravadas != 0)
        {
            writer.WriteStartElement(prefixSac, "BillingPayment", cadSac);
            writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Opegravadas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "InstructionID", cadCbc);
            writer.WriteValue("01");
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Operaciones Exoneradas
        if (listaDetalle.Opeexoneradas != 0)
        {
            writer.WriteStartElement(prefixSac, "BillingPayment", cadSac);
            writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Opeexoneradas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "InstructionID", cadCbc);
            writer.WriteValue("02");
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Operaciones Inafectas
        if (listaDetalle.Opeinafectas != 0)
        {
            writer.WriteStartElement(prefixSac, "BillingPayment", cadSac);
            writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Opeinafectas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "InstructionID", cadCbc);
            writer.WriteValue("03");
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Operaciones Exportación
        if (listaDetalle.Opeexportacion != 0)
        {
            writer.WriteStartElement(prefixSac, "BillingPayment", cadSac);
            writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Opeexportacion.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "InstructionID", cadCbc);
            writer.WriteValue("04");
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Operaciones Gratuitas
        if (listaDetalle.Opegratuitas != 0)
        {
            writer.WriteStartElement(prefixSac, "BillingPayment", cadSac);
            writer.WriteStartElement(prefixCbc, "PaidAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Opegratuitas.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCbc, "InstructionID", cadCbc);
            writer.WriteValue("05");
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // Otros cargos del item
        if (listaDetalle.Otroscargos != 0)
        {
            writer.WriteStartElement(prefixCac, "AllowanceCharge", cadCac);
            writer.WriteElementString("ChargeIndicator", cadCbc, "true");
            writer.WriteStartElement(prefixCbc, "Amount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Otroscargos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // ISC
        if (listaDetalle.TotalISC != 0)
        {
            writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.TotalISC.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.TotalISC.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteElementString("ID", cadCbc, "2000");
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("005", "2000").Trim());
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "2000").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        // IGV
        writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(listaDetalle.TotalIGV.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
        writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
        writer.WriteAttributeString("currencyID", moneda);
        writer.WriteValue(listaDetalle.TotalIGV.ToString("F2").Replace(",", "."));
        writer.WriteEndElement();
        writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
        writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
        writer.WriteElementString("ID", cadCbc, "1000");
        writer.WriteStartElement(prefixCbc, "Name", cadCbc);
        writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("005", "1000").Trim());
        writer.WriteEndElement();
        writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "1000").Trim());
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();
        writer.WriteEndElement();

        // Otros Tributos
        if (listaDetalle.Otrostributos != 0)
        {
            writer.WriteStartElement(prefixCac, "TaxTotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Otrostributos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxSubtotal", cadCac);
            writer.WriteStartElement(prefixCbc, "TaxAmount", cadCbc);
            writer.WriteAttributeString("currencyID", moneda);
            writer.WriteValue(listaDetalle.Otrostributos.ToString("F2").Replace(",", "."));
            writer.WriteEndElement();
            writer.WriteStartElement(prefixCac, "TaxCategory", cadCac);
            writer.WriteStartElement(prefixCac, "TaxScheme", cadCac);
            writer.WriteElementString("ID", cadCbc, "9999");
            writer.WriteStartElement(prefixCbc, "Name", cadCbc);
            writer.WriteCData(objFacturacion.ObtenerDescripcionPorCodElemento("005", "9999").Trim());
            writer.WriteEndElement();
            writer.WriteElementString("TaxTypeCode", cadCbc, objFacturacion.ObtenerCodigoSunat("005", "9999").Trim());
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        writer.WriteEndElement();

        return writer;
    }


    public static bool GuardarResumenNombreValorResumenFirma(EN_Resumen oResumen, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
    {
        NE_Resumen objBaja = new NE_Resumen();
        return objBaja.GuardarResumenNombreValorResumenFirma(oResumen, nombreXML, valorResumen, valorFirma, xmlgenerado);
    }

   

    
    }
    
}
