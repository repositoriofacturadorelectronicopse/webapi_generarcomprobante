/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Baja.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a baja
 FECHA   : 20/07/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Microsoft.VisualBasic; // Install-Package Microsoft.VisualBasic
using System.Globalization;
using CEN;
using System.Data;

namespace CAD
{
    public class AD_Baja
    {
        private readonly AD_Cado _datosConexion;         // CLASE DE CONEXION CADO
        private readonly SqlConnection _sqlConexion;     // CONEXION SQL

        public AD_Baja()
        {
             // DESCRIPCION: CONSTRUCTOR DE CLASE

            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
         
        }

        public List<EN_Baja> listarBajas(EN_Baja pBaja)
        {
             // DESCRIPCION: FUNCION PARA LISTAR BAJAS

            SqlCommand cmd; // comando para certificado
            SqlDataReader Rs;   //Data Reader para certificado
            var olstBaja = new List<EN_Baja>();
            EN_Baja oBaja_EN;
            
            try
            {
                 _sqlConexion.Open();
                cmd= new SqlCommand("Usp_listar_bajas",_sqlConexion);
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id_baja",  pBaja.Id);
                cmd.Parameters.AddWithValue("@empresa_id", pBaja.Empresa_id);
                cmd.Parameters.AddWithValue("@estado", pBaja.Estado.ToString());
                cmd.Parameters.AddWithValue("@situacion", pBaja.Situacion.ToString());
                cmd.Parameters.AddWithValue("@correlativo", pBaja.Correlativo);
                cmd.Parameters.AddWithValue("@fecha_inicio", (pBaja.FechaIni==null)?"":Convert.ToDateTime(pBaja.FechaIni.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion) );
                cmd.Parameters.AddWithValue("@fecha_fin", (pBaja.FechaFin==null)?"":Convert.ToDateTime(pBaja.FechaFin.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion)  );
              
                Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oBaja_EN = new EN_Baja();
                        oBaja_EN.Id = (int)Rs["id"];
                        oBaja_EN.Empresa_id = (int)Rs["empresa_id"];
                        oBaja_EN.Nrodocumento = (string)Rs["nrodocumento"];
                        oBaja_EN.DescripcionEmpresa =(string) Rs["razonsocial"];
                        oBaja_EN.Tiporesumen = (string)Rs["tiporesumen"];
                        oBaja_EN.Correlativo = (int)Rs["correlativo"];
                        oBaja_EN.FechaDocumento = Rs["fecgenera_documento"].ToString();;
                        oBaja_EN.FechaComunicacion = Rs["fecgenera_comunicacion"].ToString();;
                        oBaja_EN.Identificacomunicacion = (string)Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
                        oBaja_EN.Estado = (string)Rs["estado"];
                        oBaja_EN.Situacion = (string)Rs["situacion"];
                        oBaja_EN.EnviadoMail = (string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
                        oBaja_EN.nombreXML = (string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                        oBaja_EN.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                        oBaja_EN.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                        oBaja_EN.XMLEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
                        olstBaja.Add(oBaja_EN);
                    }
                }

                Rs.Close();
                return olstBaja;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        public List<EN_DetalleBaja> listarDetalleBaja(EN_Baja pBaja)
        {
            //DESCRIPCION: FUNCION LISTAR DETALLE BAJA

            SqlCommand cmd; // comando para certificado
            SqlDataReader Rs;   //Data Reader para certificado
            var olstDetalleBaja = new List<EN_DetalleBaja>();
            EN_DetalleBaja oDetalleBaja_EN;

            try
            {
                _sqlConexion.Open();
                cmd= new SqlCommand("Usp_listar_detalleBaja",_sqlConexion);
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id_baja",  pBaja.Id);
               
              
                Rs = cmd.ExecuteReader();

               
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDetalleBaja_EN = new EN_DetalleBaja();
                        oDetalleBaja_EN.IdBaja = (int)Rs["baja_id"];
                        oDetalleBaja_EN.Item = (int)Rs["item"];
                        oDetalleBaja_EN.Tipodocumentoid = (string)Rs["tipodocumento_id"];
                        oDetalleBaja_EN.DescTipodocumento = (string)Rs["descripcion"];
                        oDetalleBaja_EN.Serie = (string)Rs["serie"];
                        oDetalleBaja_EN.Numero = (string)Rs["numero"];
                        oDetalleBaja_EN.MotivoBaja =(string) Rs["motivobaja"];
                        olstDetalleBaja.Add(oDetalleBaja_EN);
                    }
                }

                Rs.Close();
                return olstDetalleBaja;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

        public bool GuardarBajaNombreValorResumenFirma(EN_Baja oBaja, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
        {
            //DESCRIPCION: FUNCION PARA GUARDAR NOMBRE DE VALOR RESUMEN FIRMA DE BAJA

            SqlCommand _sqlCommand; // comando 
            try
            {
                _sqlConexion.Open();
                _sqlCommand= new SqlCommand("Usp_actualizar_bajaxml",_sqlConexion);
                _sqlCommand.CommandType= CommandType.StoredProcedure;
                _sqlCommand.Parameters.AddWithValue("@empresa_id", oBaja.Empresa_id);
                _sqlCommand.Parameters.AddWithValue("@baja_id", oBaja.Id);
                _sqlCommand.Parameters.AddWithValue("@nombrexml", nombreXML);
                _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
                _sqlCommand.Parameters.AddWithValue("@valorfirma", valorFirma);
                _sqlCommand.Parameters.AddWithValue("@xmlgenerado", xmlgenerado);

              
                _sqlCommand.ExecuteNonQuery();
            
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }

       
    }
}