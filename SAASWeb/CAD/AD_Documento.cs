﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Documento.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos documento
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using Microsoft.VisualBasic;
using CEN;
using System.Data.SqlClient;
using System.Data;
using Microsoft.VisualBasic.CompilerServices;

namespace CAD
{

        public partial class AD_Documento
        {
            private readonly AD_Cado _datosConexion;        // CLASE DE CONEXION CADO
            private readonly SqlConnection _sqlConexion;    // CONEXION SQL
            private readonly SqlCommand _sqlCommand;        // COMANDO SQL

            public AD_Documento()
            {
                // DESCRIPCION: CONSTRUCTOR DE CLASE
                _datosConexion = new AD_Cado();
                _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
                _sqlCommand = new SqlCommand();
            }

          

            public List<EN_Documento> listarDocumento(EN_Documento pDocumento)
            {
                // DESCRIPCION: FUNCION DEVUELVE LISTA DE DOCUMENTO

                SqlCommand cmd; // COMANDO SQL

                string opeafec = EN_ConfigConstantes.Instance.const_TipoAfectOnerosas;      // TIPO DE AFECTACION ONEROSA
                string opeinafec = EN_ConfigConstantes.Instance.const_TipoInafectOnerosas;  // TIPO INAFECTO ONEROSA
                string operetir = EN_ConfigConstantes.Instance.const_TipoRetiroGratuito;    // TIPO RETIRO GRATUITO
                string opexoner = EN_ConfigConstantes.Instance.const_TipoExonerado ;        // TIPO EXONERADO
                string opexport = EN_ConfigConstantes.Instance.const_TipoExportacion;       // TIPO EXPORTACION

                var olstDocumento = new List<EN_Documento>();   // LISTA DE CLASE ENTIDAD DOCUMENTO
                EN_Documento oDocumento_EN;                     // CLASE ENTIDAD DOCUMENTO
                string fechaIni=null;                           // FECHA DE INICIO
                string fechaFin=null;                           // FECHA FIN
                try
                {
                    
                    // Para el tipocalculoisc 02 Aplicacion al Monto Fijo - Por confirmar formula
                    
                    if (pDocumento.FechaIni != null && pDocumento.FechaFin != null)
                    {
                       
                        fechaIni=Strings.Format(Conversions.ToDate(pDocumento.FechaIni), "yyyy-MM-dd");
                        fechaFin=Strings.Format(Conversions.ToDate(pDocumento.FechaFin), "yyyy-MM-dd");
                    }

                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_documento",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id",  pDocumento.Id.ToString());
                    cmd.Parameters.AddWithValue("@Idempresa", pDocumento.Idempresa.ToString());
                    cmd.Parameters.AddWithValue("@Idtipodocumento", pDocumento.Idtipodocumento.ToString());
                    cmd.Parameters.AddWithValue("@Estado", pDocumento.Estado.ToString());
                    cmd.Parameters.AddWithValue("@Situacion", pDocumento.Situacion.ToString());
                    cmd.Parameters.AddWithValue("@Serie",pDocumento.Serie.ToString());
                    cmd.Parameters.AddWithValue("@Numero",pDocumento.Numero.ToString());
                    cmd.Parameters.AddWithValue("@FechaIni", fechaIni);
                    cmd.Parameters.AddWithValue("@FechaFin", fechaFin);
                    cmd.Parameters.AddWithValue("@opeafec", opeafec);
                    cmd.Parameters.AddWithValue("@opeinafec", opeinafec);
                    cmd.Parameters.AddWithValue("@operetir",operetir);
                    cmd.Parameters.AddWithValue("@opexoner", opexoner);
                    cmd.Parameters.AddWithValue("@opexport", opexport);

                    SqlDataReader Rs = cmd.ExecuteReader();

                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDocumento_EN = new EN_Documento();
                            oDocumento_EN.Idempresa= (int)Rs["empresa_id"];
                            oDocumento_EN.Id= (string)Rs["id"];
                            oDocumento_EN.Idtipodocumento= (string)Rs["tipodocumento_id"];
                            oDocumento_EN.NroDocEmpresa= (string)Rs["empnrodoc"];
                            oDocumento_EN.DescripcionEmpresa= (string)Rs["empnombre"];
                            oDocumento_EN.Tipodocumento= (string)Rs["tipodocumento"];
                            oDocumento_EN.Serie= (string)Rs["serie_comprobante"];
                            oDocumento_EN.Numero= (string)Rs["nro_comprobante"];
                            oDocumento_EN.Idpuntoventa =(int)Interaction.IIf(Information.IsDBNull(Rs["puntoventa_id"]), (object)0, Rs["puntoventa_id"]);
                            oDocumento_EN.DescripcionPuntoVenta =(string)Interaction.IIf(Information.IsDBNull(Rs["puntoventadescripcion"]), "", Rs["puntoventadescripcion"]);
                            oDocumento_EN.CodigoEstablecimiento =(string)Interaction.IIf(Information.IsDBNull(Rs["codigoestablec"]), "0000", Rs["codigoestablec"]);
                            oDocumento_EN.TipoOperacion =(string)Interaction.IIf(Information.IsDBNull(Rs["tipooperacion"]), "", Rs["tipooperacion"]);
                            oDocumento_EN.TipoNotaCredNotaDeb =(string)Interaction.IIf(Information.IsDBNull(Rs["tiponotacreddeb"]), "", Rs["tiponotacreddeb"]);
                            oDocumento_EN.Tipodoccliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_tipodoc"]), "", Rs["cliente_tipodoc"]);
                            oDocumento_EN.Nrodoccliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_nrodoc"]), "", Rs["cliente_nrodoc"]);
                            oDocumento_EN.Nombrecliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_nombre"]), "", Rs["cliente_nombre"]);
                            oDocumento_EN.Direccioncliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_direccion"]), "", Rs["cliente_direccion"]);
                            oDocumento_EN.Emailcliente =(string)Interaction.IIf(Information.IsDBNull(Rs["cliente_email"]), "", Rs["cliente_email"]);
                            oDocumento_EN.Fecha= Rs["fecha"].ToString();
                            oDocumento_EN.Hora =(string) Rs["hora"];
                            oDocumento_EN.Fechavencimiento =(string)Interaction.IIf(Information.IsDBNull(Rs["fechavencimientopago"].ToString()), "", Rs["fechavencimientopago"].ToString());
                            oDocumento_EN.Glosa =(string)Interaction.IIf(Information.IsDBNull(Rs["glosa"]), "", Rs["glosa"]);
                            oDocumento_EN.Moneda= (string)Rs["moneda"];
                            oDocumento_EN.Tipocambio =(double)Interaction.IIf(Information.IsDBNull(Rs["tipocambio"]), (object)0, Rs["tipocambio"]);
                            oDocumento_EN.Igvventa =(double)Interaction.IIf(Information.IsDBNull(Rs["igvventa"]), (object)0, Rs["igvventa"]);
                            oDocumento_EN.Iscventa =(double)Interaction.IIf(Information.IsDBNull(Rs["iscventa"]), (object)0, Rs["iscventa"]);
                            oDocumento_EN.OtrosTributos =(double)Interaction.IIf(Information.IsDBNull(Rs["otrostributos"]), (object)0, Rs["otrostributos"]);
                            oDocumento_EN.Descuento =(double)Interaction.IIf(Information.IsDBNull(Rs["descuento"]), (object)0, Rs["descuento"]);
                            oDocumento_EN.OtrosCargos =(double)Interaction.IIf(Information.IsDBNull(Rs["otroscargos"]), (object)0, Rs["otroscargos"]);
                            oDocumento_EN.Importeventa= (double)Rs["importeventa"];
                            oDocumento_EN.Regimenpercep =(string)Interaction.IIf(Information.IsDBNull(Rs["regimenpercep"]), "", Rs["regimenpercep"]);
                            oDocumento_EN.Porcentpercep =(double)Interaction.IIf(Information.IsDBNull(Rs["porcentpercep"]), (object)0, Rs["porcentpercep"]);
                            oDocumento_EN.DescRegimenpercep =(string)Interaction.IIf(Information.IsDBNull(Rs["descregimenpercep"]), "", Rs["descregimenpercep"]);
                            oDocumento_EN.Importepercep =(double)Interaction.IIf(Information.IsDBNull(Rs["importepercep"]), (object)0, Rs["importepercep"]);
                            oDocumento_EN.Porcentdetrac =(double)Interaction.IIf(Information.IsDBNull(Rs["porcentdetrac"]), (object)0, Rs["porcentdetrac"]);
                            oDocumento_EN.Importedetrac =(double)Interaction.IIf(Information.IsDBNull(Rs["importedetrac"]), (object)0, Rs["importedetrac"]);
                            oDocumento_EN.Importerefdetrac =(double)Interaction.IIf(Information.IsDBNull(Rs["importerefdetrac"]), (object)0, Rs["importerefdetrac"]);
                            oDocumento_EN.Importefinal= (double)Rs["importefinal"];
                            oDocumento_EN.Importeanticipo =(double)Interaction.IIf(Information.IsDBNull(Rs["importeanticipo"]), (object)0, Rs["importeanticipo"]);
                            oDocumento_EN.IndicaAnticipo =(string)Interaction.IIf(Information.IsDBNull(Rs["indicaanticipo"]), "", Rs["indicaanticipo"]);
                            oDocumento_EN.Estado= (string)Rs["estado"];
                            oDocumento_EN.Situacion= (string)Rs["situacion"];
                            oDocumento_EN.Numeroelectronico =(string)Interaction.IIf(Information.IsDBNull(Rs["numeroelectronico"]), "0", Rs["numeroelectronico"]);
                            oDocumento_EN.Estransgratuita =(string)Interaction.IIf(Information.IsDBNull(Rs["estransgratuita"]), "NO", Rs["estransgratuita"]);
                            oDocumento_EN.EnviadoMail =(string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
                            oDocumento_EN.Envioexterno =(string)Interaction.IIf(Information.IsDBNull(Rs["enviado_externo"]), "NO", Rs["enviado_externo"]);
                            oDocumento_EN.Valorpergravadas =(double)Interaction.IIf(Information.IsDBNull(Rs["valopergravadas"]), (object)0, Rs["valopergravadas"]);
                            oDocumento_EN.Valorperinafectas =(double)Interaction.IIf(Information.IsDBNull(Rs["valoperinafectas"]), (object)0, Rs["valoperinafectas"]);
                            oDocumento_EN.Valorperexoneradas =(double)Interaction.IIf(Information.IsDBNull(Rs["valoperexoneradas"]), (object)0, Rs["valoperexoneradas"]);
                            oDocumento_EN.Valorperexportacion =(double)Interaction.IIf(Information.IsDBNull(Rs["valoperexportacion"]), (object)0, Rs["valoperexportacion"]);
                            oDocumento_EN.Valorpergratuitas =(double)Interaction.IIf(Information.IsDBNull(Rs["valopergratuitas"]), (object)0, Rs["valopergratuitas"]);
                            oDocumento_EN.Valorperiscreferenc =(double)Interaction.IIf(Information.IsDBNull(Rs["valoperiscreferenc"]), (object)0, Rs["valoperiscreferenc"]);
                            oDocumento_EN.SumaIgvGratuito =(double)Interaction.IIf(Information.IsDBNull(Rs["sumaigvgratuito"]), (double)0, Rs["sumaigvgratuito"]);
                            oDocumento_EN.SumaIscGratuito =(double)Interaction.IIf(Information.IsDBNull(Rs["sumaiscgratuito"]), (double)0, Rs["sumaiscgratuito"]);
                            oDocumento_EN.TotalDsctoItem =(double)Interaction.IIf(Information.IsDBNull(Rs["totaldsctoitem"]), (double)0, Rs["totaldsctoitem"]);
                            oDocumento_EN.TotalCargoItem =(double)Interaction.IIf(Information.IsDBNull(Rs["totalcargoitem"]), (double)0, Rs["totalcargoitem"]);
                            oDocumento_EN.TipoGuiaRemision =(string)Interaction.IIf(Information.IsDBNull(Rs["tipoguiaremision"]), "", Rs["tipoguiaremision"]);
                            oDocumento_EN.GuiaRemision =(string)Interaction.IIf(Information.IsDBNull(Rs["guiaremision"]), "", Rs["guiaremision"]);
                            oDocumento_EN.TipoDocOtroDocRef =(string)Interaction.IIf(Information.IsDBNull(Rs["tipodocotrodocref"]), "", Rs["tipodocotrodocref"]);
                            oDocumento_EN.OtroDocumentoRef =(string)Interaction.IIf(Information.IsDBNull(Rs["otrodocumentoref"]), "", Rs["otrodocumentoref"]);
                            oDocumento_EN.OrdenCompra =(string)Interaction.IIf(Information.IsDBNull(Rs["ordencompra"]), "", Rs["ordencompra"]);
                            oDocumento_EN.PlacaVehiculo =(string)Interaction.IIf(Information.IsDBNull(Rs["placavehiculo"]), "", Rs["placavehiculo"]);
                            oDocumento_EN.MontoFise =(double)Interaction.IIf(Information.IsDBNull(Rs["montofise"]), Convert.ToDouble(0), Convert.ToDouble(Rs["montofise"]));
                            oDocumento_EN.TipoRegimen =(string)Interaction.IIf(Information.IsDBNull(Rs["tiporegimen"]), "", Rs["tiporegimen"]);
                            oDocumento_EN.CodigoBBSSSujetoDetrac =(string)Interaction.IIf(Information.IsDBNull(Rs["codigobbsssujetodetrac"]), "", Rs["codigobbsssujetodetrac"]);
                            oDocumento_EN.NumCtaBcoNacion =(string)Interaction.IIf(Information.IsDBNull(Rs["numctabconacion"]), "", Rs["numctabconacion"]);
                            oDocumento_EN.BienTransfAmazonia =(string)Interaction.IIf(Information.IsDBNull(Rs["bientransfamazonia"]), "", Rs["bientransfamazonia"]);
                            oDocumento_EN.ServiTransfAmazonia =(string)Interaction.IIf(Information.IsDBNull(Rs["servitransfamazonia"]), "", Rs["servitransfamazonia"]);
                            oDocumento_EN.ContratoConstAmazonia =(string)Interaction.IIf(Information.IsDBNull(Rs["contratoconstamazonia"]), "", Rs["contratoconstamazonia"]);
                            oDocumento_EN.nombreXML =(string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                            oDocumento_EN.vResumen =(string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                            oDocumento_EN.vFirma =(string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                            oDocumento_EN.xmlEnviado =(string)Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
                            oDocumento_EN.cod_aux_01 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_01"]), "", Rs["cod_aux_01"]);
                            oDocumento_EN.text_aux_01 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_01"]), "", Rs["text_aux_01"]);
                            oDocumento_EN.cod_aux_02 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_02"]), "", Rs["cod_aux_02"]);
                            oDocumento_EN.text_aux_02 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_02"]), "", Rs["text_aux_02"]);
                            oDocumento_EN.cod_aux_03 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_03"]), "", Rs["cod_aux_03"]);
                            oDocumento_EN.text_aux_03 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_03"]), "", Rs["text_aux_03"]);
                            oDocumento_EN.cod_aux_04 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_04"]), "", Rs["cod_aux_04"]);
                            oDocumento_EN.text_aux_04 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_04"]), "", Rs["text_aux_04"]);
                            oDocumento_EN.cod_aux_05 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_05"]), "", Rs["cod_aux_05"]);
                            oDocumento_EN.text_aux_05 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_05"]), "", Rs["text_aux_05"]);
                            oDocumento_EN.cod_aux_06 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_06"]), "", Rs["cod_aux_06"]);
                            oDocumento_EN.text_aux_06 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_06"]), "", Rs["text_aux_06"]);
                            oDocumento_EN.cod_aux_07 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_07"]), "", Rs["cod_aux_07"]);
                            oDocumento_EN.text_aux_07 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_07"]), "", Rs["text_aux_07"]);
                            oDocumento_EN.cod_aux_08 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_08"]), "", Rs["cod_aux_08"]);
                            oDocumento_EN.text_aux_08 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_08"]), "", Rs["text_aux_08"]);
                            oDocumento_EN.cod_aux_09 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_09"]), "", Rs["cod_aux_09"]);
                            oDocumento_EN.text_aux_09 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_09"]), "", Rs["text_aux_09"]);
                            oDocumento_EN.cod_aux_10 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_10"]), "", Rs["cod_aux_10"]);
                            oDocumento_EN.text_aux_10 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_10"]), "", Rs["text_aux_10"]);
                            oDocumento_EN.cod_aux_11 =(string)Interaction.IIf(Information.IsDBNull(Rs["cod_aux_11"]), "", Rs["cod_aux_11"]);
                            oDocumento_EN.text_aux_11 =(string)Interaction.IIf(Information.IsDBNull(Rs["text_aux_11"]), "", Rs["text_aux_11"]);
                            oDocumento_EN.descripciondetrac =(string)Interaction.IIf(Information.IsDBNull(Rs["descripciondetrac"]), "", Rs["descripciondetrac"]);
                            oDocumento_EN.desctiponotcreddeb =(string)Interaction.IIf(Information.IsDBNull(Rs["desctiponotcreddeb"]), "", Rs["desctiponotcreddeb"]);
                            oDocumento_EN.Formapago= (string)Rs["Formapago"];
                            oDocumento_EN.jsonpagocredito =(string)Rs["jsonpagocredito"];
                            oDocumento_EN.hasRetencionIgv=(string)Rs["hasRetencionIgv"];
                            oDocumento_EN.porcRetencionIgv=(double)Interaction.IIf(Information.IsDBNull(Rs["porcRetIgv"]),(double)0, Rs["porcRetIgv"]);
                            oDocumento_EN.impRetencionIgv=(decimal)Interaction.IIf(Information.IsDBNull(Rs["impRetIgv"]), (decimal)0, Rs["impRetIgv"]);
                            oDocumento_EN.impOperacionRetencionIgv=(decimal)Interaction.IIf(Information.IsDBNull(Rs["impOpeRetIgv"]), (decimal)0, Rs["impOpeRetIgv"]);
                            oDocumento_EN.es_ivap = (string)Interaction.IIf(Information.IsDBNull(Rs["es_ivap"]), "NO", Rs["es_ivap"]);
                            olstDocumento.Add(oDocumento_EN);
                        }
                    }

                    Rs.Close();
                   
                    return olstDocumento;
                }
                catch (Exception ex)
                {
                   
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public List<EN_DetalleDocumento> listarDetalleDocumento(EN_Documento pDocumento)
            {
                // DESCRIPCION: FUNCION DEVUELVE LISTA DE DETALLE DOCUMENTO

                var olstDetalleDocumento = new List<EN_DetalleDocumento>();     // LISTA DE CLASE ENTIDAD DETALLE DOCUMENTO
                EN_DetalleDocumento oDetalleDocumento_EN;                       // CLASE ENTIDAD DETALLE DOCUMENTO
                 SqlCommand cmd;                                                // COMANDO SQL
                try
                {
                   
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_detalledocumento",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id",  pDocumento.Id.ToString());
                    cmd.Parameters.AddWithValue("@Idempresa", pDocumento.Idempresa.ToString());
                    cmd.Parameters.AddWithValue("@Idtipodocumento",  pDocumento.Idtipodocumento.ToString());

                    SqlDataReader Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDetalleDocumento_EN = new EN_DetalleDocumento();
                            oDetalleDocumento_EN.Idempresa= (int)Rs["empresa_id"];
                            oDetalleDocumento_EN.Iddocumento= (string)Rs["documento_id"];
                            oDetalleDocumento_EN.Idtipodocumento= (string)Rs["tipodocumento_id"];
                            oDetalleDocumento_EN.Lineid= (int)Rs["line_id"];
                            oDetalleDocumento_EN.Codigo =(string)Interaction.IIf(Information.IsDBNull(Rs["codigoproducto"]), "", Rs["codigoproducto"]);
                            oDetalleDocumento_EN.CodigoSunat =(string)Interaction.IIf(Information.IsDBNull(Rs["codigosunat"]), "", Rs["codigosunat"]);
                            oDetalleDocumento_EN.Producto= (string)Rs["nombreproducto"];
                            oDetalleDocumento_EN.AfectoIgv =(string)Interaction.IIf(Information.IsDBNull(Rs["afectoigv"]), "NO", Rs["afectoigv"]);
                            oDetalleDocumento_EN.AfectoIsc =(string)Interaction.IIf(Information.IsDBNull(Rs["afectoisc"]), "NO", Rs["afectoisc"]);
                            oDetalleDocumento_EN.Afectacionigv =(string)Interaction.IIf(Information.IsDBNull(Rs["tipoafectacionigv"]), "", Rs["tipoafectacionigv"]);
                            oDetalleDocumento_EN.SistemaISC =(string)Interaction.IIf(Information.IsDBNull(Rs["tipocalculoisc"]), "", Rs["tipocalculoisc"]);
                            oDetalleDocumento_EN.Unidad= (string)Rs["unidad"];
                            oDetalleDocumento_EN.Cantidad= (double)Rs["cantidad"];
                            oDetalleDocumento_EN.Preciounitario= (double)Rs["preciounitario"];
                            oDetalleDocumento_EN.Precioreferencial =(double)Interaction.IIf(Information.IsDBNull(Rs["precioreferencial"]), Rs["preciounitario"], Rs["precioreferencial"]);
                            oDetalleDocumento_EN.Valorunitario= Convert.ToDecimal(Rs["valorunitario"]);
                            oDetalleDocumento_EN.Valorbruto =Convert.ToDecimal(Interaction.IIf(Information.IsDBNull(Rs["valorbruto"]), (object)0, Rs["valorbruto"]));
                            oDetalleDocumento_EN.Valordscto =(double)Interaction.IIf(Information.IsDBNull(Rs["valordscto"]), (object)0, Rs["valordscto"]);
                            oDetalleDocumento_EN.Valorcargo =(double)Interaction.IIf(Information.IsDBNull(Rs["valorcargo"]), (object)0, Rs["valorcargo"]);
                            oDetalleDocumento_EN.Valorventa= Convert.ToDecimal(Rs["valorventa"]);
                            oDetalleDocumento_EN.Isc =(double)Interaction.IIf(Information.IsDBNull(Rs["isc"]), (object)0, Rs["isc"]);
                            oDetalleDocumento_EN.Igv =(double)Interaction.IIf(Information.IsDBNull(Rs["igv"]), (object)0, Rs["igv"]);
                            oDetalleDocumento_EN.factorIsc =(double)Interaction.IIf(Information.IsDBNull(Rs["factorisc"]), (object)0, Rs["factorisc"]);
                            oDetalleDocumento_EN.factorIgv =(double)Interaction.IIf(Information.IsDBNull(Rs["factorigv"]), (object)0, Rs["factorigv"]);
                            oDetalleDocumento_EN.Total= (double)Rs["total"];
                            oDetalleDocumento_EN.TBVTPuntoOrigen =(string)Interaction.IIf(Information.IsDBNull(Rs["tbvt_puntoorigen"]), "", Rs["tbvt_puntoorigen"]);
                            oDetalleDocumento_EN.TBVTDescripcionOrigen =(string)Interaction.IIf(Information.IsDBNull(Rs["tbvt_descripcionorigen"]), "", Rs["tbvt_descripcionorigen"]);
                            oDetalleDocumento_EN.TBVTPuntoDestino =(string)Interaction.IIf(Information.IsDBNull(Rs["tbvt_puntodestino"]), "", Rs["tbvt_puntodestino"]);
                            oDetalleDocumento_EN.TBVTDescripcionDestino =(string)Interaction.IIf(Information.IsDBNull(Rs["tbvt_descripciondestino"]), "", Rs["tbvt_descripciondestino"]);
                            oDetalleDocumento_EN.TBVTDetalleViaje =(string)Interaction.IIf(Information.IsDBNull(Rs["tbvt_detalleviaje"]), "", Rs["tbvt_detalleviaje"]);
                            oDetalleDocumento_EN.TBVTValorRefPreliminar =(string)Interaction.IIf(Information.IsDBNull(Rs["tbvt_valorrefpreliminar"]), "", Rs["tbvt_valorrefpreliminar"]);
                            oDetalleDocumento_EN.TBVTValorCargaEfectiva =(string)Interaction.IIf(Information.IsDBNull(Rs["tbvt_valorrefcargaefectiva"]), "", Rs["tbvt_valorrefcargaefectiva"]);
                            oDetalleDocumento_EN.TBVTValorCargaUtil =(string)Interaction.IIf(Information.IsDBNull(Rs["tbvt_valorrefcargautil"]), "", Rs["tbvt_valorrefcargautil"]);
                            olstDetalleDocumento.Add(oDetalleDocumento_EN);
                        }
                    }

                    Rs.Close();
                    return olstDetalleDocumento;
                }
                catch (Exception ex)
                {
                  
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public List<EN_DocReferencia> listarDocumentoReferencia(EN_DocReferencia pDocumento)
            {
                // DESCRIPCION: FUNCION DEVUELVE LISTA DE DOCUMENTO DE REFERENCIA

                var olstDocumento = new List<EN_DocReferencia>();       // LISTA DE CLASE ENTIDAD DOCREFERENCIA
                EN_DocReferencia oDocumento_EN;                         // CLASE ENTIDAD DOCREFERENCIA
                SqlCommand cmd;                                         // COMANDO SQL
                SqlDataReader Rs;                                       // DATA READER SQL

                try
                {
                   
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_documentoreferencia",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id",  pDocumento.Iddocumento.ToString());
                    cmd.Parameters.AddWithValue("@Idempresa", pDocumento.Idempresa.ToString());
                    cmd.Parameters.AddWithValue("@Idtipodocumento",pDocumento.Idtipodocumento.ToString());

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDocumento_EN = new EN_DocReferencia();
                            oDocumento_EN.Idempresa= (int)Rs["empresa_id"];
                            oDocumento_EN.Iddocumento= (string)Rs["documento_id"];
                            oDocumento_EN.Idtipodocumento= (string)Rs["tipodocumento_id"];
                            oDocumento_EN.Iddocumentoref= (string)Rs["documentoref_id"];
                            oDocumento_EN.Idtipodocumentoref= (string)Rs["tipodocumentoref_id"];
                            oDocumento_EN.Numerodocref= (string)Rs["numerodocreferencia"];
                            oDocumento_EN.DescripcionTipoDoc= (string)Rs["descripcion"];
                            olstDocumento.Add(oDocumento_EN);
                        }
                    }

                    Rs.Close();
                    return olstDocumento;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public List<EN_DocAnticipo> listarDocumentoAnticipo(EN_DocAnticipo pDocumento)
            {
                // DESCRIPCION: FUNCION DEVUELVE LISTA DE DOCUMENTO DE ANTICIPO

                var olstDocumento = new List<EN_DocAnticipo>();     // LISTA DE CLASE ENTIDAD DOCANTICIPO
                EN_DocAnticipo oDocumento_EN;                       // CLASE ENTIDAD DOCANTICIPO
                SqlCommand cmd;                                     // COMANDO SQL
                SqlDataReader Rs;                                   // DATA READER SQL

                try
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_documentoanticipo",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id",  pDocumento.documento_id.ToString());
                    cmd.Parameters.AddWithValue("@Idempresa", pDocumento.empresa_id.ToString());
                    cmd.Parameters.AddWithValue("@Idtipodocumento",pDocumento.tipodocumento_id.ToString());

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDocumento_EN = new EN_DocAnticipo();
                            oDocumento_EN.documento_id= (string)Rs["documento_id"];
                            oDocumento_EN.empresa_id= (int)Rs["empresa_id"];
                            oDocumento_EN.tipodocumento_id= (string)Rs["tipodocumento_id"];
                            oDocumento_EN.line_id= (int)Rs["line_id"];
                            oDocumento_EN.tipodocanticipo= (string)Rs["tipodocanticipo"];
                            oDocumento_EN.desctipodocanticipo =(string)Interaction.IIf(Information.IsDBNull(Rs["desctipodocant"]), "", Rs["desctipodocant"]);
                            oDocumento_EN.nrodocanticipo= (string)Rs["nrodocanticipo"];
                            oDocumento_EN.montoanticipo= Convert.ToDecimal(Rs["montoanticipo"]);
                            oDocumento_EN.tipodocemisor= (string)Rs["tipodocemisor"];
                            oDocumento_EN.nrodocemisor= (string)Rs["nrodocemisor"];
                            oDocumento_EN.desctipodocemisor =(string)Interaction.IIf(Information.IsDBNull(Rs["desctipodocemi"]), "", Rs["desctipodocemi"]);
                            olstDocumento.Add(oDocumento_EN);
                        }
                    }

                    Rs.Close();
                    return olstDocumento;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public List<EN_FacturaGuia> listarDocumentoGuia(EN_FacturaGuia pDocGuia)
            {
                //DESCRIPCION: FUNCION DEVUELVE LISTA DE DOCUMENTO GUIA

                var olstFguia = new List<EN_FacturaGuia>();         // LISTA DE CLASE ENTIDAD FACTURAGUIA
                EN_FacturaGuia oDocGuia_EN;                         // CLASE ENTIDAD FACTURAGUIA
                SqlCommand cmd;                                     // COMANDO SQL
                SqlDataReader Rs;                                   // DATA READER SQL

                try
                {
                                        

                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_documentoguia",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id",  pDocGuia.documento_id.ToString());
                    cmd.Parameters.AddWithValue("@Idempresa", pDocGuia.empresa_id.ToString());
                    cmd.Parameters.AddWithValue("@Idtipodocumento",pDocGuia.tipodocumento_id.ToString());

                    Rs = cmd.ExecuteReader();

                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDocGuia_EN = new EN_FacturaGuia();
                            oDocGuia_EN.documento_id= (string)Rs["documento_id"];
                            oDocGuia_EN.empresa_id= (int)Rs["empresa_id"];
                            oDocGuia_EN.tipodocumento_id= (string)Rs["tipodocumento_id"];
                            oDocGuia_EN.partida_direccion= (string)Rs["partida_direccion"];
                            oDocGuia_EN.partida_urbanizacion= (string)Rs["partida_urbanizacion"];
                            oDocGuia_EN.partida_departamento= (string)Rs["partida_departamento"];
                            oDocGuia_EN.partida_provincia= (string)Rs["partida_provincia"];
                            oDocGuia_EN.partida_distrito= (string)Rs["partida_distrito"];
                            oDocGuia_EN.partida_departamentodesc= (string)Rs["descpartida_departamento"];
                            oDocGuia_EN.partida_provinciadesc= (string)Rs["descpartida_provincia"];
                            oDocGuia_EN.partida_distritodesc= (string)Rs["descpartida_distrito"];
                            oDocGuia_EN.partida_pais= (string)Rs["partida_pais"];
                            oDocGuia_EN.llegada_direccion= (string)Rs["llegada_direccion"];
                            oDocGuia_EN.llegada_urbanizacion= (string)Rs["llegada_urbanizacion"];
                            oDocGuia_EN.llegada_departamento= (string)Rs["llegada_departamento"];
                            oDocGuia_EN.llegada_provincia= (string)Rs["llegada_provincia"];
                            oDocGuia_EN.llegada_distrito= (string)Rs["llegada_distrito"];
                            oDocGuia_EN.llegada_departamentodesc= (string)Rs["descllegada_departamento"];
                            oDocGuia_EN.llegada_provinciadesc= (string)Rs["descllegada_provincia"];
                            oDocGuia_EN.llegada_distritodesc= (string)Rs["descllegada_distrito"];
                            oDocGuia_EN.llegada_pais= (string)Rs["llegada_pais"];
                            oDocGuia_EN.vehiculo_placa= (string)Rs["vehiculo_placa"];
                            oDocGuia_EN.vehiculo_constancia= (string)Rs["vehiculo_constancia"];
                            oDocGuia_EN.vehiculo_marca= (string)Rs["vehiculo_marca"];
                            oDocGuia_EN.licencia_conducir= (string)Rs["licencia_conducir"];
                            oDocGuia_EN.transportista_ruc= (string)Rs["transportista_ruc"];
                            oDocGuia_EN.transportista_razonsocial= (string)Rs["transportista_razonsocial"];
                            oDocGuia_EN.modalidad_transporte= (string)Rs["modalidad_transporte"];
                            oDocGuia_EN.modalidad_transportedesc= (string)Rs["desc_modalidad_transporte"];
                            oDocGuia_EN.peso_unidad= (string)Rs["peso_unidad"];
                            oDocGuia_EN.peso_cantidad= (decimal)Rs["peso_cantidad"];
                            olstFguia.Add(oDocGuia_EN);
                        }
                    }

                    Rs.Close();
                    return olstFguia;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public bool eliminarDocumento(EN_Documento pDocumento)
            {
                // DESCRIPCON: ELIMINA DOCUMENTO

                try
                {
                    _sqlConexion.Open();
                    _sqlCommand.CommandType = CommandType.StoredProcedure;
                    _sqlCommand.Connection = _sqlConexion;
                    _sqlCommand.CommandText = "Usp_eliminar_documento";
                    _sqlCommand.Parameters.AddWithValue("@id", pDocumento.Id);
                    _sqlCommand.Parameters.AddWithValue("@empresa_id", pDocumento.Idempresa);
                    _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", pDocumento.Idtipodocumento);
                    
                    _sqlCommand.ExecuteNonQuery();
                    
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }


            public List<EN_DocRefeGuia> listarDocRefeGuia(EN_DocRefeGuia pDocumento)
            {
               //DESCRIPCION: FUNCION PARA LISTAR DOCUMENTOS DE REFERENCIA EN GUIA DEL
                var olstDocumento = new List<EN_DocRefeGuia>(); //lista de entidad docrefguia
                EN_DocRefeGuia oDocumento_EN;          // entidad DocRefGuia
                SqlCommand cmd ;        //comando
                SqlDataReader Rs;        //Data reader

                try
                {
               
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_docRefeGuia",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id",  pDocumento.documento_id.ToString());
                    cmd.Parameters.AddWithValue("@Idempresa", pDocumento.empresa_id.ToString());
                    cmd.Parameters.AddWithValue("@Idtipodocumento",pDocumento.tipodocumento_id.ToString());

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDocumento_EN = new EN_DocRefeGuia();
                            oDocumento_EN.documento_id = (string)Rs["documento_id"];
                            oDocumento_EN.empresa_id = (int)Rs["empresa_id"];
                            oDocumento_EN.tipodocumento_id = (string)Rs["tipodocumento_id"];
                            oDocumento_EN.tipo_guia = (string)Rs["tipo_guia"];
                            oDocumento_EN.numero_guia = (string)Rs["numero_guia"];
                            oDocumento_EN.desctipoguia = (string)Interaction.IIf(Information.IsDBNull(Rs["desctipoguia"]), Rs["tipo_guia"], Rs["desctipoguia"]);
                            olstDocumento.Add(oDocumento_EN);
                        }
                    }

                    Rs.Close();
                    return olstDocumento;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }


             public List<EN_ResponseSyncEstados> ConsultarSituacionesCpes(int empresa_id, int puntoventa_id, string id_cpes)
            {
               //DESCRIPCION: FUNCION PARA LISTAR DOCUMENTOS DE REFERENCIA EN GUIA DEL
                var olstDocumento = new List<EN_ResponseSyncEstados>(); //lista de entidad docrefguia
                EN_ResponseSyncEstados oDocumento_EN;          // entidad DocRefGuia
                SqlCommand cmd ;        //comando
                SqlDataReader Rs;        //Data reader

                try
                {
               
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_consultar_situacionCpes",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@empresa_id",  empresa_id);
                    cmd.Parameters.AddWithValue("@puntoventa_id", puntoventa_id);
                    cmd.Parameters.AddWithValue("@id_cpes",id_cpes);

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                            oDocumento_EN = new EN_ResponseSyncEstados();
                            oDocumento_EN.id = (string)Rs["tipodocumento_id"] + (string)Rs["serie_comprobante"]+"-"+ (string)Rs["nro_comprobante"];
                            oDocumento_EN.serie = (string)Rs["serie_comprobante"];
                            oDocumento_EN.tipodocumento_id = (string)Rs["tipodocumento_id"] ;
                            oDocumento_EN.numero = (string)Rs["nro_comprobante"];
                            oDocumento_EN.situacion = (string)Rs["situacion"];
                            oDocumento_EN.url_pdf   = EN_Constante.g_const_vacio;
                            oDocumento_EN.url_xml   = EN_Constante.g_const_vacio;
                            oDocumento_EN.url_cdr   = EN_Constante.g_const_vacio;
                            
                            
                            olstDocumento.Add(oDocumento_EN);
                        }
                    }

                    Rs.Close();
                    return olstDocumento;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }




         


        }
    

}
