/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Ubicacion.cs
 VERSION : 1.0
 OBJETIVO: Clase acceso a datos de ubicacion
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

using System.Data.SqlClient;
using System.Collections;
using System.Data;
using System;
namespace CAD
{
    public class AD_Ubicacion
    {
    
        private SqlConnection connection; // Variable de conexion
        public string listarUbicacion(int tipo,string idubicacion) 
        {
          //DESCRIPCION: Listar ubicacion
          AD_Cado conector = new AD_Cado();
          string descripcion = string.Empty;  //descripcion de ubicacion              
          SqlDataReader dr = null;
          try
          {
              using (connection = new SqlConnection(conector.CadenaConexion()))
            {
                connection.Open();
                using (SqlCommand Command = new SqlCommand("Usp_listar_ubicacion", connection)) 
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@id_tipo", SqlDbType.Int).Value = tipo;
                    Command.Parameters.Add("@idubicacion", SqlDbType.VarChar,6).Value = idubicacion;
                    dr = Command.ExecuteReader();
                    while(dr.Read())
                    {
                        descripcion = dr["descripcion"].ToString();
                    }
                }
                dr.Close();
            }
            return descripcion;  
          }
          catch (Exception ex)
          {
              
              throw ex;
          }
          finally 
          {
              connection.Close();
          }
        }
        
    }
}
