﻿using System;
using System.Collections.Generic;

using Microsoft.VisualBasic;
using log4net;
using CEN;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Collections;
using Microsoft.VisualBasic.CompilerServices;
using System.Data;

namespace CAD
{
public class AD_OtrosCpe
{
    private readonly AD_Cado _datosConexion;
    private readonly SqlConnection _sqlConexion;
    private readonly SqlCommand _sqlCommand;
    public AD_OtrosCpe()
    {
        _datosConexion = new AD_Cado();
        _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
        _sqlCommand = new SqlCommand();
    }
    public List<EN_OtrosCpe> listarOtrosCpe(EN_OtrosCpe pOtrosCpe)
    {
        ArrayList list = new ArrayList();
        list.Add("IdComprobante: " + System.Convert.ToString(pOtrosCpe.id));
        list.Add("IdEmpresa: " + System.Convert.ToString(pOtrosCpe.empresa_id));
        list.Add("IdTipoDocumento: " + System.Convert.ToString(pOtrosCpe.tipodocumento_id));
        list.Add("Estado: " + pOtrosCpe.estado);
        list.Add("Situacion: " + pOtrosCpe.situacion);
        // Fin llenado
        try
        {
            List<EN_OtrosCpe> olstOtrosCpe = new List<EN_OtrosCpe>();
            EN_OtrosCpe oOtrosCpe_EN;
            
            string sql;
            sql = "select d.*, td.descripcion, e.nrodocumento empresanrodoc, e.razonsocial empresadesc from otroscpe d inner join tipodocumento td on td.id=d.tipodocumento_id inner join empresa e on e.id=d.empresa_id where 1=1 ";
            if ((pOtrosCpe.id.ToString() != ""))
                sql += " and d.id = @param1";
            if ((pOtrosCpe.empresa_id.ToString() != "0"))
                sql += " and d.empresa_id = @param2";
            if ((pOtrosCpe.tipodocumento_id.ToString() != ""))
                sql += " and d.tipodocumento_id = @param3";
            if ((pOtrosCpe.estado.ToString() != ""))
                sql += " and d.estado = @param4";
            if ((pOtrosCpe.situacion.ToString() != ""))
                sql += " and d.situacion = @param5";
            if ((pOtrosCpe.serie != ""))
                sql += " and d.serie like '%' + @param6 + '%'";
            if ((pOtrosCpe.numero != ""))
                sql += " and d.numero like '%' + @param7 + '%'";
            if ((pOtrosCpe.fechaIni != "" & pOtrosCpe.fechaFin != ""))
                sql += " and d.fechaemision between '" + Strings.Format(Conversions.ToDate(pOtrosCpe.fechaIni), "yyyy-MM-dd")
                 + "' and '" + Strings.Format(Conversions.ToDate(pOtrosCpe.fechaIni), "yyyy-MM-dd") + "'";
            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pOtrosCpe.id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pOtrosCpe.empresa_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pOtrosCpe.tipodocumento_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param4", pOtrosCpe.estado.ToString()));
            cmd.Parameters.Add(new SqlParameter("param5", pOtrosCpe.situacion.ToString()));
            cmd.Parameters.Add(new SqlParameter("param6", pOtrosCpe.serie.ToString()));
            cmd.Parameters.Add(new SqlParameter("param7", pOtrosCpe.numero.ToString()));

            cmd.CommandTimeout =  Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oOtrosCpe_EN = new EN_OtrosCpe();
                    {
                        var withBlock = oOtrosCpe_EN;
                        withBlock.id = (string)Rs["id"];
                        withBlock.empresa_id = (int)Rs["empresa_id"];
                        withBlock.tipodocumento_id = (string)Rs["tipodocumento_id"];
                        withBlock.desctipodocumento = (string)Rs["descripcion"];
                        withBlock.nrodocempresa = (string)Rs["empresanrodoc"];
                        withBlock.descripcionempresa = (string)Rs["empresadesc"];
                        withBlock.serie = (string)Rs["serie"];
                        withBlock.numero = (string)Rs["numero"];
                        withBlock.fechaemision = Convert.ToString(Rs["fechaemision"]);
                        withBlock.persona_tipodoc = (string)Rs["persona_tipodoc"];
                        withBlock.persona_nrodoc = (string)Rs["persona_nrodoc"];
                        withBlock.persona_nombrecomercial = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_nombrecomercial"]), "", Rs["persona_nombrecomercial"]);
                        withBlock.persona_ubigeo = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_ubigeo"]), "", Rs["persona_ubigeo"]);
                        withBlock.persona_direccion = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_direccion"]), "", Rs["persona_direccion"]);
                        withBlock.persona_urbanizacion = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_urbanizacion"]), "", Rs["persona_urbanizacion"]);
                        withBlock.persona_departamento = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_departamento"]), "", Rs["persona_departamento"]);
                        withBlock.persona_provincia = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_provincia"]), "", Rs["persona_provincia"]);
                        withBlock.persona_distrito = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_distrito"]), "", Rs["persona_distrito"]);
                        withBlock.persona_codpais = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_codpais"]), "", Rs["persona_codpais"]);
                        withBlock.persona_descripcion = (string)Rs["persona_descripcion"];
                        withBlock.persona_email = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_email"]), "", Rs["persona_email"]);
                        withBlock.regimen_retper = (string)Rs["regimen_retper"];
                        withBlock.tasa_retper = (decimal)Rs["tasa_retper"];
                        withBlock.observaciones = (string)Interaction.IIf(Information.IsDBNull(Rs["observaciones"]), "", Rs["observaciones"]);
                        withBlock.importe_retper = (decimal)Rs["importe_retper"];
                        withBlock.moneda_impretper = (string)Rs["moneda_impretper"];
                        withBlock.importe_pagcob = (decimal)Rs["importe_pagcob"];
                        withBlock.moneda_imppagcob = (string)Rs["moneda_imppagcob"];
                        withBlock.estado = (string)Rs["estado"];
                        withBlock.situacion = (string)Rs["situacion"];
                        withBlock.enviado_email = (string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "", Rs["enviado_email"]);
                        withBlock.enviado_externo = (string)Interaction.IIf(Information.IsDBNull(Rs["enviado_externo"]), "", Rs["enviado_externo"]);
                        withBlock.nombreXML =(string) Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                        withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                        withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                        withBlock.xmlEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
                    }
                    olstOtrosCpe.Add(oOtrosCpe_EN);
                }
            }
            Rs.Close();
           
            return olstOtrosCpe;
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_DetalleOtrosCpe> listarDetalleOtrosCpe(EN_OtrosCpe pOtrosCpe)
    {
        
        try
        {
            List<EN_DetalleOtrosCpe> olstDetalleOtrosCpe = new List<EN_DetalleOtrosCpe>();
            EN_DetalleOtrosCpe oDetalleOtrosCpe_EN;
            // Guardamos Log
            var sql = "select dd.*, t.descripcion descripciontipodocumento from detalleotroscpe dd inner join tipodocumento t on dd.docrelac_tipodoc_id=t.id where 1=1 ";
            if ((pOtrosCpe.id.ToString() != "0"))
                sql += " and dd.otroscpe_id = @param1";
            if ((pOtrosCpe.empresa_id.ToString() != "0"))
                sql += " and dd.empresa_id = @param2";
            if ((pOtrosCpe.tipodocumento_id.ToString() != "0"))
                sql += " and dd.tipodocumento_id = @param3";
            sql += " order by cast(line_id as varchar)";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pOtrosCpe.id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pOtrosCpe.empresa_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pOtrosCpe.tipodocumento_id.ToString()));
            cmd.CommandTimeout =  Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDetalleOtrosCpe_EN = new EN_DetalleOtrosCpe();
                    {
                        var withBlock = oDetalleOtrosCpe_EN;
                        withBlock.empresa_id = (int)Rs["empresa_id"];
                        withBlock.otroscpe_id = (string)Rs["otroscpe_id"];
                        withBlock.tipodocumento_id = (string)Rs["tipodocumento_id"];
                        withBlock.line_id = (int)Rs["line_id"];
                        withBlock.docrelac_tipodoc_id = (string)Rs["docrelac_tipodoc_id"];
                        withBlock.docrelac_tipodoc_desc = (string)Rs["descripciontipodocumento"];
                        withBlock.docrelac_numerodoc = (string)Rs["docrelac_numerodoc"];
                        withBlock.docrelac_fechaemision = Convert.ToString(Rs["docrelac_fechaemision"]);
                        withBlock.docrelac_importetotal = (decimal)Rs["docrelac_importetotal"];
                        withBlock.docrelac_moneda = (string)Rs["docrelac_moneda"];
                        withBlock.pagcob_fecha = Convert.ToString(Rs["pagcob_fecha"]);
                        withBlock.pagcob_fechaft = Convert.ToString(Rs["pagcob_fecha"]);
                        withBlock.pagcob_numero = (string)Rs["pagcob_numero"];
                        withBlock.pagcob_importe = (decimal)Rs["pagcob_importe"];
                        withBlock.pagcob_moneda = (string)Rs["pagcob_moneda"];
                        withBlock.retper_fecha = Convert.ToString(Rs["retper_fecha"]);
                        withBlock.retper_fechaft = Convert.ToString(Rs["retper_fecha"]);
                        withBlock.retper_importe = (decimal)Rs["retper_importe"];
                        withBlock.retper_moneda = (string)Rs["retper_moneda"];
                        withBlock.retper_importe_pagcob = (decimal)Rs["retper_importe_pagcob"];
                        withBlock.retper_moneda_pagcob = (string)Rs["retper_moneda_pagcob"];
                        withBlock.tipocambio_monedaref = (string)Interaction.IIf(Information.IsDBNull(Rs["tipocambio_monedaref"]), null, Rs["tipocambio_monedaref"]);
                        withBlock.tipocambio_monedaobj = (string)Interaction.IIf(Information.IsDBNull(Rs["tipocambio_monedaobj"]), null, Rs["tipocambio_monedaobj"]);
                        withBlock.tipocambio_factor = (decimal)Interaction.IIf(Information.IsDBNull(Rs["tipocambio_factor"]), null, Rs["tipocambio_factor"]);
                        withBlock.tipocambio_fecha = Convert.ToString(Interaction.IIf(Information.IsDBNull(Rs["tipocambio_fecha"]), null, Rs["tipocambio_fecha"]));
                    }
                    olstDetalleOtrosCpe.Add(oDetalleOtrosCpe_EN);
                }
            }
            Rs.Close();
            
            return olstDetalleOtrosCpe;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public bool GuardarOtrosCpeNombreValorResumenFirma(EN_OtrosCpe oOtrosCpe, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
    {
       
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "actualizar_otroscpexml";
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oOtrosCpe.empresa_id);
            _sqlCommand.Parameters.AddWithValue("@otroscpe_id", oOtrosCpe.id);
            _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oOtrosCpe.tipodocumento_id);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombreXML);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@valorfirma", valorFirma);
            _sqlCommand.Parameters.AddWithValue("@xmlgenerado", xmlgenerado);
            
            _sqlCommand.ExecuteNonQuery();
           
            return true;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public void GuardarOtrosCpeAceptada(EN_OtrosCpe oOtrosCpe, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
        
        try
        {
            string nrocomprobElect = oOtrosCpe.serie + "-" + oOtrosCpe.numero;
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "guardar_otroscpeelectronica";
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oOtrosCpe.empresa_id);
            _sqlCommand.Parameters.AddWithValue("@otroscpe_id", oOtrosCpe.id);
            _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oOtrosCpe.tipodocumento_id);
            _sqlCommand.Parameters.AddWithValue("@numcompelectronico", nrocomprobElect);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
            _sqlCommand.Parameters.AddWithValue("@xmlbase64", xmlenviado);
            _sqlCommand.Parameters.AddWithValue("@xmlcdr", cdrxml);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@firma", firma);
            _sqlCommand.Parameters.AddWithValue("@mensaje", mensaje);
            _sqlCommand.Parameters.AddWithValue("@ticket", ticket);
           
            _sqlCommand.ExecuteNonQuery();
           
        }
        catch (Exception ex)
        {
          
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public void GuardarOtrosCpeRechazadaError(EN_OtrosCpe oOtrosCpe, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
       
        try
        {
            string nrocomprobElect = oOtrosCpe.serie + "-" + oOtrosCpe.numero;
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "guardar_otroscperechazadaerror";
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oOtrosCpe.empresa_id);
            _sqlCommand.Parameters.AddWithValue("@otroscpe_id", oOtrosCpe.id);
            _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oOtrosCpe.tipodocumento_id);
            _sqlCommand.Parameters.AddWithValue("@numcomprobante", nrocomprobElect);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
            _sqlCommand.Parameters.AddWithValue("@xmlbase64", xmlenviado);
            _sqlCommand.Parameters.AddWithValue("@cdrxml", cdrxml);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@firma", firma);
            _sqlCommand.Parameters.AddWithValue("@mensajeerror", Strings.Replace(errortext, "'", "''"));
            _sqlCommand.Parameters.AddWithValue("@estado", estado);
            _sqlCommand.Parameters.AddWithValue("@ticket", ticket);
            
            _sqlCommand.ExecuteNonQuery();
            
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_DocElectronico> consultarDocumentoElectronico(EN_DocElectronico pDocumento)
    {
        
        try
        {
            List<EN_DocElectronico> olstDocumento = new List<EN_DocElectronico>();
            EN_DocElectronico oDocumento_EN;
       
            string sql;
            sql = "SELECT d.empresa_id, e.nrodocumento ,d.id, d.tipodocumento_id, d.serie, d.numero, " + "d.fechaemision, d.nombrexml,d.valorresumen,d.valorfirma, d.xmlgenerado FROM otroscpe d " + "inner join empresa e on e.id=d.empresa_id " + "WHERE e.nrodocumento = @param1 AND d.tipodocumento_id = @param2 " + "AND d.serie = @param3 AND d.numero = @param4 AND d.fechaemision = @param5";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.RucEmisor.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Idtipodocumento.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pDocumento.Serie.ToString()));
            cmd.Parameters.Add(new SqlParameter("param4", pDocumento.Numero.ToString()));
            cmd.Parameters.Add(new SqlParameter("param5", pDocumento.FechaEmision.ToString()));

            cmd.CommandTimeout =  Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDocumento_EN = new EN_DocElectronico();
                    {
                        var withBlock = oDocumento_EN;
                        withBlock.Idempresa = (int)Rs["empresa_id"];
                        withBlock.RucEmisor = (string)Rs["nrodocumento"];
                        withBlock.IdDocumento = (string)Rs["id"];
                        withBlock.Idtipodocumento = (string)Rs["tipodocumento_id"];
                        withBlock.Serie = (string)Rs["serie"];
                        withBlock.Numero = (string)Rs["numero"];
                        withBlock.FechaEmision = (string)Rs["fechaemision"];
                        withBlock.Numcompelectronico = (string)Rs["serie"] + "-" + Rs["numero"];
                        withBlock.NombreXML = (string)Rs["nombrexml"];
                        withBlock.ValorResumen = (string)Rs["valorresumen"];
                        withBlock.Firma = (string)Rs["valorfirma"];
                        withBlock.xmlGenerado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
                    }
                    olstDocumento.Add(oDocumento_EN);
                }
            }
            Rs.Close();
           
            return olstDocumento;
        }
        catch (Exception ex)
        {
          
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public void ActualizarEnvioMail(EN_OtrosCpe oOtrosCpe, string estado)
    {
        
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "actualizar_otroscpeenviomail";
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oOtrosCpe.empresa_id);
            _sqlCommand.Parameters.AddWithValue("@otroscpe_id", oOtrosCpe.id);
            _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oOtrosCpe.tipodocumento_id);
            _sqlCommand.Parameters.AddWithValue("@estado", estado);
           
            _sqlCommand.ExecuteNonQuery();
           
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_OtrosCpe> listarAceptadoOtrosCpe(EN_OtrosCpe pDocumento)
    {
       
        try
        {
            List<EN_OtrosCpe> olstDocumento = new List<EN_OtrosCpe>();
            EN_OtrosCpe oDocumento_EN;
           
            string sql;
            sql = "select empresa_id, otroscpe_id, tipodocumento_id, numcomprobante, nombrexml, valorresumen, firma, fechaenvio, mensaje, xmlbase64, cdrxml from otroscpeelectronico d where 1=1  ";
            if ((pDocumento.id.ToString() != ""))
                sql += " and d.otroscpe_id = @param1";
            if ((pDocumento.empresa_id.ToString() != "0"))
                sql += " and d.empresa_id = @param2";
            if ((pDocumento.tipodocumento_id.ToString() != "0"))
                sql += " and d.tipodocumento_id = @param3";
            sql += " order by d.fechaenvio desc";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.empresa_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pDocumento.tipodocumento_id.ToString()));
            cmd.CommandTimeout =  Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDocumento_EN = new EN_OtrosCpe();
                    {
                        var withBlock = oDocumento_EN;
                        withBlock.empresa_id = (int)Rs["empresa_id"];
                        withBlock.id = (string)Rs["otroscpe_id"];
                        withBlock.tipodocumento_id = (string)Rs["tipodocumento_id"];
                        withBlock.nombreXML =(string) Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                        withBlock.xmlEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlbase64"]), "", Rs["xmlbase64"]);
                        withBlock.xmlCDR = (string)Interaction.IIf(Information.IsDBNull(Rs["cdrxml"]), "", Rs["cdrxml"]);
                        withBlock.vResumen = (string)Rs["valorresumen"];
                        withBlock.vFirma = (string)Rs["firma"];
                        withBlock.FechaEnvio = (string)Rs["fechaenvio"];
                        withBlock.Mensaje = Regex.Replace((string)Rs["mensaje"], "[^a-zA-Z0-9_.-]+ ", "", RegexOptions.Compiled);
                    }
                    olstDocumento.Add(oDocumento_EN);
                }
            }
            Rs.Close();
            
            return olstDocumento;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_OtrosCpe> listarErrorRechazoOtrosCpe(EN_OtrosCpe pDocumento)
    {
       
        try
        {
            List<EN_OtrosCpe> olstDocumento = new List<EN_OtrosCpe>();
            EN_OtrosCpe oDocumento_EN;
          
            string sql;
            sql = "select empresa_id, otroscpe_id, tipodocumento_id, numcomprobante, nombrexml, xmlbase64, cdrxml, valorresumen, firma, mensajeerror, fecharechazo from otroscperechazadaerror d where 1=1  ";
            if ((pDocumento.id.ToString() != ""))
                sql += " and d.otroscpe_id = @param1";
            if ((pDocumento.empresa_id.ToString() != "0"))
                sql += " and d.empresa_id = @param2";
            if ((pDocumento.tipodocumento_id.ToString() != "0"))
                sql += " and d.tipodocumento_id = @param3";
            sql += " order by d.fecharechazo desc";
            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.empresa_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pDocumento.tipodocumento_id.ToString()));

            cmd.CommandTimeout =  Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDocumento_EN = new EN_OtrosCpe();
                    {
                        var withBlock = oDocumento_EN;
                        withBlock.empresa_id = (int)Rs["empresa_id"];
                        withBlock.id = (string)Rs["otroscpe_id"];
                        withBlock.tipodocumento_id = (string)Rs["tipodocumento_id"];
                        withBlock.xmlEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlbase64"]), "", Rs["xmlbase64"]);
                        withBlock.xmlCDR = (string)Interaction.IIf(Information.IsDBNull(Rs["cdrxml"]), "", Rs["cdrxml"]);
                        withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                        withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["firma"]), "", Rs["firma"]);
                        withBlock.MensajeError = Regex.Replace((string)Rs["mensajeerror"], "[^a-zA-Z0-9_.-]+ ", "", RegexOptions.Compiled);
                        withBlock.FechaError = (string)Rs["fecharechazo"];
                    }
                    olstDocumento.Add(oDocumento_EN);
                }
            }
            Rs.Close();
           
            return olstDocumento;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_OtrosCpe> reporteConsolidado(EN_OtrosCpe pDocumento)
    {
        
        try
        {
            List<EN_OtrosCpe> olstDocumento = new List<EN_OtrosCpe>();
            EN_OtrosCpe oDocumento_EN;
            
            string sql;
            sql = "select t.descripcion, d.situacion, count(*) cantidad from otroscpe d " + "inner join tipodocumento t on d.tipodocumento_id=t.id  " + "where d.estado='N' ";
            if ((pDocumento.empresa_id.ToString() != "0"))
                sql += " and d.empresa_id = @param1";
            if ((pDocumento.tipodocumento_id.ToString() != ""))
                sql += " and d.tipodocumento_id = @param2";
            if ((pDocumento.situacion.ToString() != ""))
                sql += " and d.situacion = @param3";
            if ((pDocumento.fechaIni != "" & pDocumento.fechaFin != ""))
                sql += " and d.fechaemision between '" + Strings.Format(Conversions.ToDate(pDocumento.fechaIni), "yyyy-MM-dd")
                 + "' and '" + Strings.Format(Conversions.ToDate(pDocumento.fechaIni), "yyyy-MM-dd")  + "'";
            sql += " group by t.descripcion, d.situacion order by t.descripcion, d.situacion";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.empresa_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.tipodocumento_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pDocumento.situacion.ToString()));

            cmd.CommandTimeout =  Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDocumento_EN = new EN_OtrosCpe();
                    {
                        var withBlock = oDocumento_EN;
                        withBlock.desctipodocumento = (string)Rs["descripcion"];
                        withBlock.situacion = (string)Rs["situacion"];
                        withBlock.Cantidad = (string)Rs["cantidad"];
                    }
                    olstDocumento.Add(oDocumento_EN);
                }
            }
            Rs.Close();
           
            return olstDocumento;
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public bool ActualizarSituacion(EN_OtrosCpe oDocumento, string situacion)
    {
        
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "actualizar_otroscpesituacion";
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oDocumento.empresa_id);
            _sqlCommand.Parameters.AddWithValue("@otroscpe_id", oDocumento.id);
            _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oDocumento.tipodocumento_id);
            _sqlCommand.Parameters.AddWithValue("@situacion", situacion);
           
            _sqlCommand.ExecuteNonQuery();
          
            return true;
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public bool eliminarOtrosCpe(EN_OtrosCpe pDocumento)
    {
        
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_eliminar_otroscpe";
            _sqlCommand.Parameters.AddWithValue("@id", pDocumento.id);
            _sqlCommand.Parameters.AddWithValue("@empresa_id", pDocumento.empresa_id);
            _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", pDocumento.tipodocumento_id);
           
            _sqlCommand.ExecuteNonQuery();
           
            return true;
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public string consultarSituacion(EN_OtrosCpe pDocumento)
    {
        try
        {
            string Situacion = "";
            var sql = "select situacion from otroscpe where id = @param1 and empresa_id = @param2  and tipodocumento_id = @param3 ";
            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.id.Trim()));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.empresa_id));
            cmd.Parameters.Add(new SqlParameter("param3", pDocumento.tipodocumento_id.Trim()));
            cmd.CommandTimeout = Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                    Situacion = (string)Interaction.IIf(Information.IsDBNull(Rs["situacion"]), "", Rs["situacion"]);
            }
            else
                Situacion = "";
            Rs.Close();
            return Situacion.Trim();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_OtrosCpe> listarDocumentoReenviarMail(EN_OtrosCpe pOtrosCpe)
    {
        
        try
        {
            List<EN_OtrosCpe> olstOtrosCpe = new List<EN_OtrosCpe>();
            EN_OtrosCpe oOtrosCpe_EN;
           
            string sql;
            sql = "select d.*, td.descripcion, e.nrodocumento empresanrodoc, e.razonsocial empresadesc from otroscpe d inner join tipodocumento td on td.id=d.tipodocumento_id inner join empresa e on e.id=d.empresa_id where 1=1 ";
            if ((pOtrosCpe.id.ToString() != ""))
                sql += " and d.id = @param1";
            if ((pOtrosCpe.empresa_id.ToString() != "0"))
                sql += " and d.empresa_id = @param2";
            if ((pOtrosCpe.tipodocumento_id.ToString() != ""))
                sql += " and d.tipodocumento_id = @param3";
            if ((pOtrosCpe.estado.ToString() != ""))
                sql += " and d.estado = @param4";
            if ((pOtrosCpe.situacion.ToString() != ""))
                sql += " and d.situacion = @param5";
            sql += " and ISNULL(d.enviado_email,'NO') <> 'SI' order by d.fechaemision desc, d.serie, d.numero asc";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pOtrosCpe.id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pOtrosCpe.empresa_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pOtrosCpe.tipodocumento_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param4", pOtrosCpe.estado.ToString()));
            cmd.Parameters.Add(new SqlParameter("param5", pOtrosCpe.situacion.ToString()));

            cmd.CommandTimeout =  Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oOtrosCpe_EN = new EN_OtrosCpe();
                    {
                        var withBlock = oOtrosCpe_EN;
                        withBlock.id = (string)Rs["id"];
                        withBlock.empresa_id = (int)Rs["empresa_id"];
                        withBlock.tipodocumento_id = (string)Rs["tipodocumento_id"];
                        withBlock.desctipodocumento = (string)Rs["descripcion"];
                        withBlock.nrodocempresa = (string)Rs["empresanrodoc"];
                        withBlock.descripcionempresa = (string)Rs["empresadesc"];
                        withBlock.serie = (string)Rs["serie"];
                        withBlock.numero = (string)Rs["numero"];
                        withBlock.fechaemision = Convert.ToString(Rs["fechaemision"]);
                        withBlock.persona_tipodoc = (string)Rs["persona_tipodoc"];
                        withBlock.persona_nrodoc = (string)Rs["persona_nrodoc"];
                        withBlock.persona_nombrecomercial = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_nombrecomercial"]), "", Rs["persona_nombrecomercial"]);
                        withBlock.persona_ubigeo = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_ubigeo"]), "", Rs["persona_ubigeo"]);
                        withBlock.persona_direccion = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_direccion"]), "", Rs["persona_direccion"]);
                        withBlock.persona_urbanizacion = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_urbanizacion"]), "", Rs["persona_urbanizacion"]);
                        withBlock.persona_departamento = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_departamento"]), "", Rs["persona_departamento"]);
                        withBlock.persona_provincia = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_provincia"]), "", Rs["persona_provincia"]);
                        withBlock.persona_distrito = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_distrito"]), "", Rs["persona_distrito"]);
                        withBlock.persona_codpais = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_codpais"]), "", Rs["persona_codpais"]);
                        withBlock.persona_descripcion = (string)Rs["persona_descripcion"];
                        withBlock.persona_email = (string)Interaction.IIf(Information.IsDBNull(Rs["persona_email"]), "", Rs["persona_email"]);
                        withBlock.regimen_retper = (string)Rs["regimen_retper"];
                        withBlock.tasa_retper = (decimal)Rs["tasa_retper"];
                        withBlock.observaciones = (string)Interaction.IIf(Information.IsDBNull(Rs["observaciones"]), "", Rs["observaciones"]);
                        withBlock.importe_retper = (decimal)Rs["importe_retper"];
                        withBlock.moneda_impretper = (string)Rs["moneda_impretper"];
                        withBlock.importe_pagcob = (decimal)Rs["importe_pagcob"];
                        withBlock.moneda_imppagcob = (string)Rs["moneda_imppagcob"];
                        withBlock.estado = (string)Rs["estado"];
                        withBlock.situacion = (string)Rs["situacion"];
                        withBlock.enviado_email = (string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "", Rs["enviado_email"]);
                        withBlock.enviado_externo = (string)Interaction.IIf(Information.IsDBNull(Rs["enviado_externo"]), "", Rs["enviado_externo"]);
                        withBlock.nombreXML = (string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                        withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                        withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                        withBlock.xmlEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
                    }
                    olstOtrosCpe.Add(oOtrosCpe_EN);
                }
            }
            Rs.Close();
            
            return olstOtrosCpe;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_DocElectronico> consultaLibreDocumentoElectronico(EN_DocElectronico pDocumento)
    {
       
        try
        {
            List<EN_DocElectronico> olstDocumento = new List<EN_DocElectronico>();
            EN_DocElectronico oDocumento_EN;
           
            string sql;
            sql = "SELECT d.empresa_id, e.nrodocumento ,d.id, d.tipodocumento_id, d.serie, d.numero, " + "d.fechaemision, d.nombrexml,d.valorresumen,d.valorfirma, d.xmlgenerado FROM otroscpe d " + "inner join empresa e on e.id=d.empresa_id " + "WHERE e.nrodocumento = @param1 AND d.tipodocumento_id = @param2 " + "AND d.serie = @param3 AND d.numero = @param4 AND d.fechaemision = @param5 AND d.importe_retper = @param6";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.RucEmisor.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Idtipodocumento.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pDocumento.Serie.ToString()));
            cmd.Parameters.Add(new SqlParameter("param4", pDocumento.Numero.ToString()));
            cmd.Parameters.Add(new SqlParameter("param5", pDocumento.FechaEmision.ToString()));
            cmd.Parameters.Add(new SqlParameter("param6", pDocumento.Importe.ToString()));

            cmd.CommandTimeout = Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDocumento_EN = new EN_DocElectronico();
                    {
                        var withBlock = oDocumento_EN;
                        withBlock.Idempresa = (int)Rs["empresa_id"];
                        withBlock.RucEmisor = (string)Rs["nrodocumento"];
                        withBlock.IdDocumento = (string)Rs["id"];
                        withBlock.Idtipodocumento = (string)Rs["tipodocumento_id"];
                        withBlock.Serie = (string)Rs["serie"];
                        withBlock.Numero = (string)Rs["numero"];
                        withBlock.FechaEmision = (string)Rs["fechaemision"];
                        withBlock.Numcompelectronico = (string)Rs["serie"] + "-" + Rs["numero"];
                        withBlock.NombreXML = (string)Rs["nombrexml"];
                        withBlock.ValorResumen = (string)Rs["valorresumen"];
                        withBlock.Firma = (string)Rs["valorfirma"];
                        withBlock.xmlGenerado =(string) Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
                    }
                    olstDocumento.Add(oDocumento_EN);
                }
            }
            Rs.Close();
            
            return olstDocumento;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
}
}
