﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using log4net;
using CEN;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Collections;
using System.Data;
using Microsoft.VisualBasic.CompilerServices;

namespace CAD
{
public class AD_Reversion
{
    private readonly AD_Cado _datosConexion;
    private readonly SqlConnection _sqlConexion;
    private readonly SqlCommand _sqlCommand;
    public AD_Reversion()
    {
        _datosConexion = new AD_Cado();
        _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
        _sqlCommand = new SqlCommand();
    }

    public List<EN_Reversion> listarReversiones(EN_Reversion pReversion)
    {
        
        ArrayList list = new ArrayList();
        list.Add("IdReversion: " + System.Convert.ToString(pReversion.Id));
        list.Add("IdEmpresa: " + System.Convert.ToString(pReversion.Empresa_id));
        list.Add("Estado: " + pReversion.Estado);
        list.Add("Situacion: " + pReversion.Situacion);
        list.Add("Correlativo: " + System.Convert.ToString(pReversion.Correlativo));
        // Fin llenado
        try
        {
            List<EN_Reversion> olstReversion = new List<EN_Reversion>();
            EN_Reversion oReversion_EN;

            string sql;
            sql = "SELECT b.*, e.nrodocumento, e.razonsocial from reversion b inner join empresa e on b.empresa_id=e.id WHERE 1=1 ";
            if ((pReversion.Id.ToString() != "0"))
                sql += " and b.id = @param1";
            if ((pReversion.Empresa_id.ToString() != "0"))
                sql += " and b.empresa_id = @param2";
            if ((pReversion.Estado.ToString() != ""))
                sql += " and b.estado = @param3";
            if ((pReversion.Situacion.ToString() != ""))
                sql += " and b.situacion = @param4";
            if ((pReversion.Correlativo != 0))
                sql += " and b.correlativo = @param5";
            if ((pReversion.FechaIni != "" & pReversion.FechaFin != ""))
                sql += " and b.fecgenera_comunicacion between '" + Strings.Format(Conversions.ToDate(pReversion.FechaIni), "yyyy-MM-dd")
                + "' and '" + Strings.Format(Conversions.ToDate(pReversion.FechaIni), "yyyy-MM-dd") + "'";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pReversion.Id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pReversion.Empresa_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pReversion.Estado.ToString()));
            cmd.Parameters.Add(new SqlParameter("param4", pReversion.Situacion.ToString()));
            cmd.Parameters.Add(new SqlParameter("param5", pReversion.Correlativo.ToString()));

            cmd.CommandTimeout = Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oReversion_EN = new EN_Reversion();
                    {
                        var withBlock = oReversion_EN;
                        withBlock.Id = (int)Rs["id"];
                        withBlock.Empresa_id =(int) Rs["empresa_id"];
                        withBlock.Nrodocumento = (string)Rs["nrodocumento"];
                        withBlock.DescripcionEmpresa = (string)Rs["razonsocial"];
                        withBlock.Tiporesumen = (string)Rs["tiporesumen"];
                        withBlock.Correlativo = (int)Rs["correlativo"];
                        withBlock.FechaDocumento = Rs["fecgenera_documento"].ToString();
                        withBlock.FechaComunicacion = Rs["fecgenera_comunicacion"].ToString();
                        withBlock.Identificacomunicacion = (string)Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
                        withBlock.Estado = (string)Rs["estado"];
                        withBlock.Situacion = (string)Rs["situacion"];
                        withBlock.EnviadoMail = (string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
                        withBlock.nombreXML = (string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                        withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                        withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                        withBlock.XMLEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlgenerado"]), "", Rs["xmlgenerado"]);
                    }
                    olstReversion.Add(oReversion_EN);
                }
            }
            Rs.Close();
            
            return olstReversion;
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_DetalleReversion> listarDetalleReversion(EN_Reversion pReversion)
    {
        
        try
        {
            List<EN_DetalleReversion> olstDetalleReversion = new List<EN_DetalleReversion>();
            EN_DetalleReversion oDetalleReversion_EN;
            // Guardamos Log
            var sql = "select db.*, t.descripcion from detallereversion db inner join tipodocumento t on db.tipodocumento_id = t.id where 1=1 ";
            if ((pReversion.Id.ToString() != "0"))
                sql += " and db.reversion_id = @param1";
            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pReversion.Id.ToString()));

            cmd.CommandTimeout = Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDetalleReversion_EN = new EN_DetalleReversion();
                    {
                        var withBlock = oDetalleReversion_EN;
                        withBlock.IdReversion = (int)Rs["reversion_id"];
                        withBlock.Item = (int)Rs["item"];
                        withBlock.Tipodocumentoid = (string)Rs["tipodocumento_id"];
                        withBlock.DescTipodocumento = (string)Rs["descripcion"];
                        withBlock.Serie = (string)Rs["serie"];
                        withBlock.Numero = (string)Rs["numero"];
                        withBlock.MotivoReversion =(string) Rs["motivoreversion"];
                    }
                    olstDetalleReversion.Add(oDetalleReversion_EN);
                }
            }
            Rs.Close();
            
            return olstDetalleReversion;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public bool GuardarReversionNombreValorResumenFirma(EN_Reversion oReversion, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
    {
        
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_actualizar_reversionxml";
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oReversion.Empresa_id);
            _sqlCommand.Parameters.AddWithValue("@reversion_id", oReversion.Id);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombreXML);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@valorfirma", valorFirma);
            _sqlCommand.Parameters.AddWithValue("@xmlgenerado", xmlgenerado);
            
            _sqlCommand.ExecuteNonQuery();
         
            return true;
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public void GuardarReversionAceptada(EN_Reversion oReversion, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string mensaje, string ticket)
    {
       
        string identcomunicacion = oReversion.Tiporesumen.Trim() + "-" + Strings.Format(Conversions.ToDate(oReversion.FechaComunicacion),EN_Constante.g_const_formfecha_yyyyMMdd) + "-" + oReversion.Correlativo;
     
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_guardar_reversionelectronica";
            _sqlCommand.Parameters.AddWithValue("@reversion_id", oReversion.Id);
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oReversion.Empresa_id);
            _sqlCommand.Parameters.AddWithValue("@identifica_comunicacion", identcomunicacion);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
            _sqlCommand.Parameters.AddWithValue("@xmlbase64", xmlbase64);
            _sqlCommand.Parameters.AddWithValue("@xmlcdr", cdrxml);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@firma", firma);
            _sqlCommand.Parameters.AddWithValue("@mensaje", mensaje);
            _sqlCommand.Parameters.AddWithValue("@ticket", ticket);
            
            _sqlCommand.ExecuteNonQuery();
            
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public void GuardarReversionRechazadaError(EN_Reversion oReversion, string nombrexml, string xmlbase64, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket)
    {
        
        string identcomunicacion = oReversion.Tiporesumen.Trim() + "-" +  Strings.Format(Conversions.ToDate(oReversion.FechaComunicacion), EN_Constante.g_const_formfecha_yyyyMMdd) + "-" + oReversion.Correlativo;
       
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_guardar_reversionrechazadaerror";
            _sqlCommand.Parameters.AddWithValue("@reversion_id", oReversion.Id);
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oReversion.Empresa_id);
            _sqlCommand.Parameters.AddWithValue("@identifica_comunicacion", identcomunicacion);
            _sqlCommand.Parameters.AddWithValue("@nombrexml", nombrexml);
            _sqlCommand.Parameters.AddWithValue("@xmlbase64", xmlbase64);
            _sqlCommand.Parameters.AddWithValue("@xmlcdr", cdrxml);
            _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
            _sqlCommand.Parameters.AddWithValue("@firma", firma);
            _sqlCommand.Parameters.AddWithValue("@mensajeerror", Strings.Replace(errortext, "'", "''"));
            _sqlCommand.Parameters.AddWithValue("@estado", estado);
            _sqlCommand.Parameters.AddWithValue("@ticket", ticket);

           
            _sqlCommand.ExecuteNonQuery();
           
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public void ActualizarEnvioMail(EN_Reversion oReversion, string estado)
    {
       
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_actualizar_reversionenviomail";
            _sqlCommand.Parameters.AddWithValue("@reversion_id", oReversion.Id);
            _sqlCommand.Parameters.AddWithValue("@empresa_id", oReversion.Empresa_id);
            _sqlCommand.Parameters.AddWithValue("@estado", estado);
            
            _sqlCommand.ExecuteNonQuery();
            
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_DocElectronico> consultarDocumentoElectronico(EN_DocElectronico pDocumento)
    {
        try
        {
            
            List<EN_DocElectronico> olstDocumento = new List<EN_DocElectronico>();
            EN_DocElectronico oDocumento_EN;
            string sql;
            sql = "SELECT d.empresa_id,e.nrodocumento,d.id,d.tiporesumen,d.correlativo,d.fecgenera_comunicacion, " + "d.identifica_comunicacion,d.nombrexml,d.valorresumen,d.valorfirma FROM reversion d " + "inner join empresa e on e.id=d.empresa_id " + "WHERE e.nrodocumento = @param1 AND d.tiporesumen = @param2 " + "AND d.correlativo = @param3 AND d.fecgenera_comunicacion = @param4";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.RucEmisor));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Idtipodocumento));
            cmd.Parameters.Add(new SqlParameter("param3", pDocumento.Numero));
            cmd.Parameters.Add(new SqlParameter("param4", pDocumento.FechaEmision));

            cmd.CommandTimeout = Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDocumento_EN = new EN_DocElectronico();
                    {
                        var withBlock = oDocumento_EN;
                        withBlock.Idempresa = (int)Rs["empresa_id"];
                        withBlock.RucEmisor = (string)Rs["nrodocumento"];
                        withBlock.IdDocumento = (string)Rs["id"];
                        withBlock.Idtipodocumento = (string)Rs["tiporesumen"];
                        withBlock.Numero =(string) Rs["correlativo"];
                        withBlock.FechaEmision =(string) Rs["fecgenera_comunicacion"];
                        withBlock.Numcompelectronico = (string)Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
                        withBlock.NombreXML = (string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                        withBlock.ValorResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                        withBlock.Firma = (string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                    }
                    olstDocumento.Add(oDocumento_EN);
                }
            }
            Rs.Close();
            
            return olstDocumento;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_Reversion> listarAceptadoReversion(EN_Reversion pDocumento)
    {
        
        try
        {
            List<EN_Reversion> olstDocumento = new List<EN_Reversion>();
            EN_Reversion oDocumento_EN;

            

            string sql;
            sql = "select reversion_id,empresa_id,identifica_comunicacion,nombrexml,xmlbase64,cdrxml,valorresumen,firma,mensaje,fechaenvio,ticket from reversionelectronica d where 1=1  ";
            if ((pDocumento.Id.ToString() != ""))
                sql += " and d.reversion_id = @param1";
            if ((pDocumento.Empresa_id.ToString() != "0"))
                sql += " and d.empresa_id = @param2";
            sql += " order by d.fechaenvio desc";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.Id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Empresa_id.ToString()));

            cmd.CommandTimeout = Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDocumento_EN = new EN_Reversion();
                    {
                        var withBlock = oDocumento_EN;
                        withBlock.Id =(int) Rs["reversion_id"];
                        withBlock.Empresa_id = (int)Rs["empresa_id"];
                        withBlock.Identificacomunicacion =(string) Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
                        withBlock.nombreXML =(string) Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                        withBlock.XMLEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlbase64"]), "", Rs["xmlbase64"]);
                        withBlock.CDRXML =(string) Interaction.IIf(Information.IsDBNull(Rs["cdrxml"]), "", Rs["cdrxml"]);
                        withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                        withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["firma"]), "", Rs["firma"]);
                        withBlock.MensajeCDR = (string)Regex.Replace((string)Rs["mensaje"], "[^a-zA-Z0-9_.-]+ ", "", RegexOptions.Compiled); // Rs("mensajeerror")
                        withBlock.FechaEnvio = (string)Rs["fechaenvio"];
                        withBlock.ticket = (string)Interaction.IIf(Information.IsDBNull(Rs["ticket"]), "", Rs["ticket"]);
                    }
                    olstDocumento.Add(oDocumento_EN);
                }
            }
            Rs.Close();
           
            return olstDocumento;
        }
        catch (Exception ex)
        {
          
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_Reversion> listarErrorRechazoReversion(EN_Reversion pDocumento)
    {
        
        try
        {
            List<EN_Reversion> olstDocumento = new List<EN_Reversion>();
            EN_Reversion oDocumento_EN;

            

            string sql;
            sql = "select reversion_id,empresa_id,identifica_comunicacion,nombrexml,xmlbase64,cdrxml,valorresumen,firma,mensajeerror,fecharechazo,ticket from reversionrechazoerror d where 1=1  ";
            if ((pDocumento.Id.ToString() != ""))
                sql += " and d.reversion_id = @param1";
            if ((pDocumento.Empresa_id.ToString() != "0"))
                sql += " and d.empresa_id = @param2";
            sql += " order by d.fecharechazo desc";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.Id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Empresa_id.ToString()));

            cmd.CommandTimeout = Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDocumento_EN = new EN_Reversion();
                    {
                        var withBlock = oDocumento_EN;
                        withBlock.Id = (int)Rs["reversion_id"];
                        withBlock.Empresa_id = (int)Rs["empresa_id"];
                        withBlock.Identificacomunicacion = (string)Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
                        withBlock.nombreXML =(string) Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                        withBlock.XMLEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlbase64"]), "", Rs["xmlbase64"]);
                        withBlock.CDRXML =(string) Interaction.IIf(Information.IsDBNull(Rs["cdrxml"]), "", Rs["cdrxml"]);
                        withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                        withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["firma"]), "", Rs["firma"]);
                        withBlock.MensajeError = (string)Regex.Replace((string)Rs["mensajeerror"], "[^a-zA-Z0-9_.-]+ ", "", RegexOptions.Compiled); // Rs("mensajeerror")
                        withBlock.FechaError= (string) Rs["fecharechazo"].ToString();
                        withBlock.ticket = (string)Interaction.IIf(Information.IsDBNull(Rs["ticket"]), "", Rs["ticket"]);
                    }
                    olstDocumento.Add(oDocumento_EN);
                }
            }
            Rs.Close();
           
            return olstDocumento;
        }
        catch (Exception ex)
        {
         
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_Reversion> reporteConsolidado(EN_Reversion pResumen)
    {
        try
        {
            List<EN_Reversion> olstDocumento = new List<EN_Reversion>();
            EN_Reversion oDocumento_EN;
           
            string sql;
            sql = "select t.descripcion, d.situacion, count(*) cantidad from reversion d " + "inner join tipodocumento t on d.tiporesumen=t.id " + "where d.estado='N' ";
            if ((pResumen.Empresa_id.ToString() != "0"))
                sql += " and d.empresa_id = @param1";
            if ((pResumen.Tiporesumen.ToString() != ""))
                sql += " and d.tiporesumen = @param2";
            if ((pResumen.Situacion.ToString() != ""))
                sql += " and d.situacion = @param3";
            if ((pResumen.FechaIni != "" & pResumen.FechaFin != ""))
                sql += " and d.fecgenera_comunicacion between '" + Strings.Format(Conversions.ToDate(pResumen.FechaIni), "yyyy-MM-dd")
                 + "' and '" + Strings.Format(Conversions.ToDate(pResumen.FechaIni), "yyyy-MM-dd") + "'";
            sql += " group by t.descripcion, d.situacion order by t.descripcion, d.situacion";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pResumen.Empresa_id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pResumen.Tiporesumen.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pResumen.Situacion.ToString()));
            cmd.CommandTimeout = Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDocumento_EN = new EN_Reversion();
                    {
                        var withBlock = oDocumento_EN;
                        withBlock.Tiporesumen= (string) Rs["descripcion"];
                        withBlock.Situacion= (string) Rs["situacion"];
                        withBlock.Cantidad= (string) Rs["cantidad"];
                    }
                    olstDocumento.Add(oDocumento_EN);
                }
            }
            Rs.Close();
            
            return olstDocumento;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_Reversion> listarReversionAsociada(EN_Documento pDocumento)
    {
        
        try
        {
            List<EN_Reversion> olstReversion = new List<EN_Reversion>();
            EN_Reversion oReversion_EN;
            

            string sql;
            sql = "select id, empresa_id, tiporesumen, correlativo, fecgenera_documento, fecgenera_comunicacion, identifica_comunicacion, estado, situacion, enviado_email, nombrexml, valorresumen, valorfirma  from reversion b inner join detallereversion d on b.id=d.reversion_id WHERE 1=1 ";
            if ((pDocumento.Idempresa.ToString() != "0"))
                sql += " and b.empresa_id = @param1";
            if ((pDocumento.Idtipodocumento.ToString() != ""))
                sql += " and d.tipodocumento_id = @param2";
            if ((pDocumento.Serie.ToString() != ""))
                sql += " and d.serie = @param3";
            if ((pDocumento.Numero.ToString() != ""))
                sql += " and convert(int,d.numero) = @param4";
            if ((pDocumento.Estado.ToString() != ""))
                sql += " and b.estado = @param5";
            if ((pDocumento.Situacion.ToString() != ""))
                sql += " and b.situacion = @param6";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.Idempresa.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Idtipodocumento.ToString()));
            cmd.Parameters.Add(new SqlParameter("param3", pDocumento.Serie.ToString()));
            cmd.Parameters.Add(new SqlParameter("param4", System.Convert.ToInt32(pDocumento.Numero)));
            cmd.Parameters.Add(new SqlParameter("param5", pDocumento.Estado.ToString()));
            cmd.Parameters.Add(new SqlParameter("param6", pDocumento.Situacion.ToString()));
            cmd.CommandTimeout = Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oReversion_EN = new EN_Reversion();
                    {
                        var withBlock = oReversion_EN;
                        withBlock.Id= (int) Rs["id"];
                        withBlock.Empresa_id= (int) Rs["empresa_id"];
                        withBlock.Tiporesumen= (string) Rs["tiporesumen"];
                        withBlock.Correlativo= (int) Rs["correlativo"];
                        withBlock.FechaDocumento= (string) Rs["fecgenera_documento"];
                        withBlock.FechaComunicacion= (string) Rs["fecgenera_comunicacion"];
                        withBlock.Identificacomunicacion = (string)Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
                        withBlock.Estado= (string) Rs["estado"];
                        withBlock.Situacion= (string) Rs["situacion"];
                        withBlock.EnviadoMail = (string)Interaction.IIf(Information.IsDBNull(Rs["enviado_email"]), "NO", Rs["enviado_email"]);
                        withBlock.nombreXML = (string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                        withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                        withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["valorfirma"]), "", Rs["valorfirma"]);
                    }
                    olstReversion.Add(oReversion_EN);
                }
            }
            Rs.Close();
           
            return olstReversion;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public bool eliminarReversion(EN_Reversion pReversion)
    {
        
        try
        {
            _sqlConexion.Open();
            _sqlCommand.CommandType = CommandType.StoredProcedure;
            _sqlCommand.Connection = _sqlConexion;
            _sqlCommand.CommandText = "Usp_eliminar_reversion";
            _sqlCommand.Parameters.AddWithValue("@idreversion", pReversion.Id);
            
            _sqlCommand.ExecuteNonQuery();
           
            return true;
        }
        catch (Exception ex)
        {
           
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
    public List<EN_Reversion> listarErrorRechazoReversionConTicket(EN_Reversion pDocumento)
    {
        
        try
        {
            List<EN_Reversion> olstDocumento = new List<EN_Reversion>();
            EN_Reversion oDocumento_EN;

           

            string sql;
            sql = "select reversion_id,empresa_id,identifica_comunicacion,nombrexml,xmlbase64,cdrxml,valorresumen,firma,mensajeerror,fecharechazo,ticket from reversionrechazoerror d where 1=1  ";
            if ((pDocumento.Id.ToString() != ""))
                sql += " and d.reversion_id = @param1";
            if ((pDocumento.Empresa_id.ToString() != "0"))
                sql += " and d.empresa_id = @param2";
            sql += " and d.ticket <> '' order by d.fecharechazo desc";

            SqlCommand cmd = new SqlCommand(sql, _sqlConexion);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.Add(new SqlParameter("param1", pDocumento.Id.ToString()));
            cmd.Parameters.Add(new SqlParameter("param2", pDocumento.Empresa_id.ToString()));

            cmd.CommandTimeout = Convert.ToInt32(EN_ConfigConstantes.Instance.const_TiempoEspera);
            _sqlConexion.Open();
            SqlDataReader Rs = cmd.ExecuteReader(CommandBehavior.CloseConnection);
            if (Rs.HasRows)
            {
                while (Rs.Read())
                {
                    oDocumento_EN = new EN_Reversion();
                    {
                        var withBlock = oDocumento_EN;
                        withBlock.Id= (int) Rs["reversion_id"];
                        withBlock.Empresa_id= (int) Rs["empresa_id"];
                        withBlock.Identificacomunicacion = (string)Interaction.IIf(Information.IsDBNull(Rs["identifica_comunicacion"]), "", Rs["identifica_comunicacion"]);
                        withBlock.nombreXML = (string)Interaction.IIf(Information.IsDBNull(Rs["nombrexml"]), "", Rs["nombrexml"]);
                        withBlock.XMLEnviado = (string)Interaction.IIf(Information.IsDBNull(Rs["xmlbase64"]), "", Rs["xmlbase64"]);
                        withBlock.CDRXML = (string)Interaction.IIf(Information.IsDBNull(Rs["cdrxml"]), "", Rs["cdrxml"]);
                        withBlock.vResumen = (string)Interaction.IIf(Information.IsDBNull(Rs["valorresumen"]), "", Rs["valorresumen"]);
                        withBlock.vFirma = (string)Interaction.IIf(Information.IsDBNull(Rs["firma"]), "", Rs["firma"]);
                        withBlock.MensajeError = Regex.Replace((string)Rs["mensajeerror"], "[^a-zA-Z0-9_.-]+ ", "", RegexOptions.Compiled); // Rs("mensajeerror")
                        withBlock.FechaError= (string) Rs["fecharechazo"];
                        withBlock.ticket = (string)Interaction.IIf(Information.IsDBNull(Rs["ticket"]), "", Rs["ticket"]);
                    }
                    olstDocumento.Add(oDocumento_EN);
                }
            }
            Rs.Close();
            
            return olstDocumento;
        }
        catch (Exception ex)
        {
            
            throw ex;
        }
        finally
        {
            _sqlConexion.Close();
        }
    }
}
}