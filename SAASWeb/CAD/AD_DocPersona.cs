﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_DocPersona.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos Doc persona
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using System.Collections.Generic;
using CEN;
using System.Data.SqlClient;
using System.Data;

namespace CAD
{

    public class AD_DocPersona
    {
        private readonly AD_Cado _datosConexion;            // CLASE DE CONEXION CADO
        private readonly SqlConnection _sqlConexion;        // CONEXION SQL
        public AD_DocPersona()
        {
            // DESCRIPCION: CONSTRUCTOR DE CLASE 

            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
        }
        public List<EN_DocPersona> listarDocPersona(EN_DocPersona pDocPersona)
        {
            // DESCRIPCION: FUNCION PARA LISTAR DOCUMENTO DE PERSONA
            
            List<EN_DocPersona> olstDP = new List<EN_DocPersona>(); // LISTA DE CLASE ENTIDAD DOCPERSONA
            EN_DocPersona oDP_EN;                                   // CLASE ENTIDAD DOCPERSONA
            SqlCommand cmd;                                         // COMANDO SQL
            SqlDataReader Rs;                                       // DATA READER SQL
            try
            {
             
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_listar_docpersona",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@codigo",  pDocPersona.Codigo);
                    cmd.Parameters.AddWithValue("@estado", pDocPersona.Estado);
                    cmd.Parameters.AddWithValue("@indpersona", pDocPersona.Indpersona);

                    Rs = cmd.ExecuteReader();

                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                        oDP_EN = new EN_DocPersona();
                        
                            var withBlock = oDP_EN;
                            withBlock.Codigo =(string) Rs["codigo"];
                            withBlock.CodigoSunat =(string) Rs["codigosunat"];
                            withBlock.Nombre =(string) Rs["nombre"];
                            withBlock.Longitud =Convert.ToString(Rs["longitud"]);
                            withBlock.Tipo =(string) Rs["tipo"];
                            withBlock.Indcontrib =(string) Rs["indcontrib"];
                            withBlock.Indlongexacta =(string) Rs["indlongexacta"];
                            withBlock.Indpersona =(string) Rs["indpersona"];
                            withBlock.Orden =Convert.ToString(Rs["orden"]);
                            withBlock.Estado =(string) Rs["estado"];
                        
                        olstDP.Add(oDP_EN);
                    }
                }
                Rs.Close();
                return olstDP;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
    }

}
