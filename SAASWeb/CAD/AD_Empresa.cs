﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Empresa.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos empresa
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using Microsoft.VisualBasic;
using CEN;
using System.Data.SqlClient;
using System.Data;

namespace CAD
{
    public class AD_Empresa
    {
      
        private readonly AD_Cado _datosConexion;        //clase conexión
        private readonly SqlConnection _sqlConexion;    //conector
        private readonly SqlCommand _sqlCommand;        //comando
         
        public AD_Empresa()
        {
            //DESCRIPCION: CONSTRUCTOR DE CLASE
            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
        }
        public EN_Empresa listarEmpresa(EN_Empresa pEmpresa, int Idpuntoventa)
        {
            //DESCRIPCION: FUNCION PAARA LISTAR LOS DATOS DE LA EMPRESA

            var withBlock = new EN_Empresa(); // Entidad Empresa 
            SqlCommand cmd ;                    // Comando para empresa
            SqlCommand cmdPar ;                    // Comando para parámetro
            SqlDataReader Rs;                   // DataReader para empresa
            SqlDataReader RsPar;                   // DataReader para parámetro
            string uspName= "";                 // variable nombre de procedimiento
            
            try
            {
                    if(Idpuntoventa==0) uspName="Usp_listar_empresaresumen";
                    else uspName="Usp_listar_empresa";

                    _sqlConexion.Open();
                    cmd = new SqlCommand(uspName,_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdEmpresa",  pEmpresa.Id);
                    cmd.Parameters.AddWithValue("@Nrodocumento",  pEmpresa.Nrodocumento);
                    cmd.Parameters.AddWithValue("@RazonSocial", pEmpresa.RazonSocial);
                    cmd.Parameters.AddWithValue("@Estado",pEmpresa.Estado);
                    if(Idpuntoventa!=0)  cmd.Parameters.AddWithValue("@Idpuntoventa", Idpuntoventa);

                    Rs = cmd.ExecuteReader();
                if (Rs.HasRows)
                {
                    while (Rs.Read())
                    {
                            withBlock.Id = (int)Rs["id"];
                            withBlock.Tipoempresa = (string)Rs["tipoempresa"];
                            withBlock.Nrodocumento = (string)Rs["nrodocumento"];
                            withBlock.RazonSocial = (string)Rs["razonsocial"];
                            withBlock.Nomcomercial = (string)Interaction.IIf(Information.IsDBNull(Rs["nomcomercial"]), "", Rs["nomcomercial"]);
                            withBlock.Propietario = (string)Interaction.IIf(Information.IsDBNull(Rs["propietario"]), "", Rs["propietario"]);
                            withBlock.Tipodocumento = (string)Rs["tipodocumento"];
                            withBlock.Nrodoc = (string)Interaction.IIf(Information.IsDBNull(Rs["nrodoc"]), "", Rs["nrodoc"]);
                            withBlock.Apellido = (string)Interaction.IIf(Information.IsDBNull(Rs["apellido"]), "", Rs["apellido"]);
                            withBlock.Nombre = (string)Interaction.IIf(Information.IsDBNull(Rs["nombre"]), "", Rs["nombre"]);
                            withBlock.Direccion = (string)Interaction.IIf(Information.IsDBNull(Rs["direccion"]), "", Rs["direccion"]);
                            withBlock.Sector = (string)Interaction.IIf(Information.IsDBNull(Rs["sector"]), "", Rs["sector"]);
                            withBlock.Telefono1 = (string)Interaction.IIf(Information.IsDBNull(Rs["telefono1"]), "", Rs["telefono1"]);
                            withBlock.Telefono2 = (string)Interaction.IIf(Information.IsDBNull(Rs["telefono2"]), "", Rs["telefono2"]);
                            withBlock.Email = (string)Interaction.IIf(Information.IsDBNull(Rs["email"]), "", Rs["email"]);
                            withBlock.Web = (string)Interaction.IIf(Information.IsDBNull(Rs["web"]), "", Rs["web"]);
                            withBlock.Imagen = (string)Interaction.IIf(Information.IsDBNull(Rs["imagen"]), "", Rs["imagen"]);
                            if(Idpuntoventa!=0)  withBlock.Codpais = (string)Interaction.IIf(Information.IsDBNull(Rs["codpais"]), "", Rs["codpais"]);
                            if(Idpuntoventa!=0) withBlock.Coddis = (string)Interaction.IIf(Information.IsDBNull(Rs["coddis"]), "", Rs["coddis"]);
                            if(Idpuntoventa!=0)  withBlock.Departamento = (string)Interaction.IIf(Information.IsDBNull(Rs["departamento"]), "", Rs["departamento"]);
                            if(Idpuntoventa!=0)  withBlock.Provincia = (string)Interaction.IIf(Information.IsDBNull(Rs["provincia"]), "", Rs["provincia"]);
                            if(Idpuntoventa!=0)  withBlock.Distrito = (string)Interaction.IIf(Information.IsDBNull(Rs["distrito"]), "", Rs["distrito"]);
                            withBlock.SignatureID = (string)Interaction.IIf(Information.IsDBNull(Rs["signatureID"]), "", Rs["signatureID"]);
                            withBlock.SignatureURI = (string)Interaction.IIf(Information.IsDBNull(Rs["signatureURI"]), "", Rs["signatureURI"]);
                            withBlock.NroResolucion = (string)Interaction.IIf(Information.IsDBNull(Rs["numeroresolucion"]), "", Rs["numeroresolucion"]);
                            withBlock.CuentaBcoDet = (string)Interaction.IIf(Information.IsDBNull(Rs["cuentabcodet"]), "", Rs["cuentabcodet"]);
                            withBlock.AplicaISC = (string)Interaction.IIf(Information.IsDBNull(Rs["aplicaisc"]), "0", Rs["aplicaisc"]);
                            withBlock.AplicaDctoItem = (string)Interaction.IIf(Information.IsDBNull(Rs["aplicadctoitem"]), "0", Rs["aplicadctoitem"]);
                            withBlock.AplicaCargoItem = (string)Interaction.IIf(Information.IsDBNull(Rs["aplicacargoitem"]), "0", Rs["aplicacargoitem"]);
                            withBlock.MuestraIgv = (string)Interaction.IIf(Information.IsDBNull(Rs["muestraigv"]), "0", Rs["muestraigv"]);
                            withBlock.MuestraSubTotal = (string)Interaction.IIf(Information.IsDBNull(Rs["muestrasubtotal"]), "0", Rs["muestrasubtotal"]);
                            withBlock.AplicaDctoGlobal = (string)Interaction.IIf(Information.IsDBNull(Rs["aplicadctoglobal"]), "0", Rs["aplicadctoglobal"]);
                            withBlock.AplicaCargoGlobal = (string)Interaction.IIf(Information.IsDBNull(Rs["aplicacargoglobal"]), "0", Rs["aplicacargoglobal"]);
                            withBlock.Estado = (string)Rs["estado"];
                        
                    }
                }
                Rs.Close();

                cmdPar = new SqlCommand("Usp_buscar_parametro",_sqlConexion);
                //buscamos en tabla parámetro ; flag=5: busca ruta, enviarxml y produccion 
                cmdPar.CommandType= CommandType.StoredProcedure;
                cmdPar.Parameters.AddWithValue("@flag",  EN_Constante.g_const_5);
                cmdPar.Parameters.AddWithValue("@idBusqueda1", pEmpresa.Id );
                cmdPar.Parameters.AddWithValue("@idBusqueda2", Idpuntoventa );
                RsPar = cmdPar.ExecuteReader();
                if(RsPar.HasRows)
                {
                    while(RsPar.Read())
                    {
                        
                        withBlock.enviarxml = (string)Interaction.IIf(Information.IsDBNull(RsPar["envio"].ToString()), "NO", RsPar["envio"].ToString());  
                        withBlock.Produccion = (string)Interaction.IIf(Information.IsDBNull(RsPar["produccion"].ToString()), "NO", RsPar["produccion"].ToString());  
                    }
                }
                RsPar.Close();
                return withBlock;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
        public EN_Parametro buscar_tablaParametro(EN_Parametro par_busqueda)
        {
            //DESCRIPCION:BUSCAR EN TABLA PARAMETRO

            SqlCommand cmd; // comando para certificado
            SqlDataReader Rs;   //Data Reader para certificado
            EN_Parametro res_par = new EN_Parametro();
    

            try
            {
                _sqlConexion.Open();
                cmd = new SqlCommand("Usp_buscar_parametroGeneral",_sqlConexion);
                 
    
                cmd.CommandType= CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@par_concepto",  par_busqueda.par_conceptopfij);
                cmd.Parameters.AddWithValue("@par_correlativo", par_busqueda.par_conceptocorr);
                cmd.Parameters.AddWithValue("@p_par_descripcion", par_busqueda.par_descripcion);
                cmd.Parameters.AddWithValue("@p_par_descripcion2",  par_busqueda.par_descripcion2 );
                cmd.Parameters.AddWithValue("@p_par_int1",  par_busqueda.par_int1 );
                cmd.Parameters.AddWithValue("@p_par_int2", par_busqueda.par_int2 );
                cmd.Parameters.AddWithValue("@p_par_tipo", par_busqueda.par_tipo );
                Rs = cmd.ExecuteReader();

                if(Rs.HasRows)
                {
                    while(Rs.Read())
                    {
                       res_par.par_descripcion= (string)Interaction.IIf(Information.IsDBNull(Rs["par_descripcion"]), "", Rs["par_descripcion"]);           
                       res_par.par_descripcion2= (string)Interaction.IIf(Information.IsDBNull(Rs["par_descripcion2"]), "", Rs["par_descripcion2"]);           
                       res_par.par_int1= (int)Interaction.IIf(Information.IsDBNull(Rs["par_int1"]), 0, Rs["par_int1"]);           
                       res_par.par_int2= (int)Interaction.IIf(Information.IsDBNull(Rs["par_int2"]), 0, Rs["par_int2"]);   
                       res_par.par_float1= (decimal)Interaction.IIf(Information.IsDBNull(Rs["par_float1"]),(decimal)0, Rs["par_float1"]);   
                       res_par.par_float2= (decimal)Interaction.IIf(Information.IsDBNull(Rs["par_float2"]), (decimal)0, Rs["par_float2"]);   
                       res_par.par_date1= (string)Interaction.IIf(Information.IsDBNull(Rs["par_date1"]), "", Rs["par_date1"].ToString());   
                       res_par.par_date2= (string)Interaction.IIf(Information.IsDBNull(Rs["par_date1"]), "", Rs["par_date2"].ToString()); 
                       res_par.par_tipo= (int)Interaction.IIf(Information.IsDBNull(Rs["par_tipo"]), 0, Rs["par_tipo"]);   
                    }
                }
                Rs.Close();
                return res_par;

            }
            catch (System.Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                _sqlConexion.Close();
            }
        }
       

    }
}
