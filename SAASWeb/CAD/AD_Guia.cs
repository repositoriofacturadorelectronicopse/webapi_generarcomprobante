/****************************************************************************************************************************************************************************************
 PROGRAMA: CAD_Guia.cs
 VERSION : 1.0
 OBJETIVO: Clase acceso a datos de guia
 FECHA   : 18/06/2021
 AUTOR   : JOSE CHUMIOQUE -  IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 ***************************************************************************************************************************************************************************************/

using System;
using CEN;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using System.Globalization;
namespace CAD
{
    public class AD_Guia
    {
        private SqlConnection connection;   // Variable de conexion
        public EN_RespuestaListaGuia listarGuia (EN_Guia pGuia) 
        {
        //DESCRIPCION: Listar guia
        EN_RespuestaListaGuia ResListaGuia = new EN_RespuestaListaGuia();
        AD_Cado conector = new AD_Cado();
        List<EN_Guia> olsGuia  =  new List<EN_Guia>();        
        SqlDataReader dr = null;
        try
        {
            using (connection = new SqlConnection(conector.CadenaConexion()))
            {
                connection.Open();
                using (SqlCommand Command = new SqlCommand("Usp_listar_guia", connection)) 
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@idguia", SqlDbType.VarChar,15).Value = pGuia.id;
                    Command.Parameters.Add("@idempresa", SqlDbType.Int).Value = pGuia.empresa_id;
                    Command.Parameters.Add("@idtipodocumento", SqlDbType.VarChar,2).Value = pGuia.tipodocumento_id;
                    Command.Parameters.Add("@estado", SqlDbType.VarChar,1).Value = pGuia.estado;
                    Command.Parameters.Add("@situacion", SqlDbType.VarChar,1).Value = pGuia.situacion;
                    Command.Parameters.Add("@serie", SqlDbType.VarChar,5).Value = pGuia.serie;
                    Command.Parameters.Add("@numero", SqlDbType.VarChar,10).Value = pGuia.numero;
                    dr = Command.ExecuteReader();
                    while(dr.Read()) 
                    {
                        EN_Guia objGuia = new EN_Guia();
                        if (dr["id"] != null)
                        objGuia.id = dr["id"].ToString();

                        if(dr["empresa_id"] != null)
                        objGuia.empresa_id = Convert.ToInt32(dr["empresa_id"].ToString());

                        if (dr["tipodocumento_id"] != null)
                        objGuia.tipodocumento_id = dr["tipodocumento_id"].ToString();

                        if (dr["serie"] != null)
                        objGuia.serie = dr["serie"].ToString();

                        if (dr["numero"] != null) 
                        objGuia.numero = dr["numero"].ToString();


                        if (dr["ptoemision"] != null)
                        {
                             if (dr["ptoemision"].ToString() !="")
                             {
                                 objGuia.puntoemision = int.Parse(dr["ptoemision"].ToString());
                             }
                       
                        }
                        
                        if (dr["fechaemision"] != null)
                        objGuia.fechaemision = dr["fechaemision"].ToString();

                        if (dr["observaciones"] != null) 
                        objGuia.observaciones = dr["observaciones"].ToString();

                        if (dr["tipodocumentogrbaja"] != null)
                        objGuia.tipodocumentogrbaja = dr["tipodocumentogrbaja"].ToString();

                         if (dr["numerdocumentogrbaja"] != null)
                        objGuia.numerodocumentogrbaja = dr["numerdocumentogrbaja"].ToString();

                        if (dr["nombredocumentogrbaja"] != null) 
                        objGuia.nombredocumentogrbaja = dr["nombredocumentogrbaja"].ToString();

                        if (dr["tipodocumentorelac"] != null)
                        objGuia.tipodocumentorelac = dr["tipodocumentorelac"].ToString();

                        if (dr["numerodocumentorelac"] != null)
                        objGuia.numerodocumentorelc = dr["numerodocumentorelac"].ToString();
                       
                        if (dr["numeracion_dam"] != null)
                        objGuia.numeracion_dam = dr["numeracion_dam"].ToString();

                        if (dr["numero_manifiesto"] != null)
                        objGuia.numero_manifiesto = dr["numero_manifiesto"].ToString();

                        if (dr["destinatario_tipodoc"] != null)
                        objGuia.destinatario_tipodoc = dr["destinatario_tipodoc"].ToString();

                        if (dr["destinatario_nrodoc"] != null)
                        objGuia.destinatario_nrodoc = dr["destinatario_nrodoc"].ToString();

                        if (dr["destinatario_nombre"]!= null)
                        objGuia.destinatario_nombre = dr["destinatario_nombre"].ToString();

                        if(dr["destinatario_email"]!= null)
                        objGuia.destinatario_email= dr["destinatario_email"].ToString();

                        if (dr["tercero_tipodoc"] != null)
                        objGuia.tercero_tipodoc = dr["tercero_tipodoc"].ToString();

                        if (dr["tercero_nrodoc"] != null)
                        objGuia.tercero_nrodoc = dr["tercero_nrodoc"].ToString();

                        if(dr["tercero_nombre"] != null)
                        objGuia.tercero_nombre = dr["tercero_nombre"].ToString();

                        if(dr["codmotivo"] != null)
                        objGuia.codmotivo = dr["codmotivo"].ToString();

                        if(dr["descripcionmotivo"]!= null)
                        objGuia.descripcionmotivo = dr["descripcionmotivo"].ToString();

                        if (dr["indicadortransbordo"] != null)
                        objGuia.indicadortransbordo = dr["indicadortransbordo"].ToString();

                        if(dr["pesobrutototal"]!= null)
                        {
                            if(dr["pesobrutototal"].ToString() != "") 
                            {
                                 objGuia.pesobrutototal = Convert.ToDouble(dr["pesobrutototal"].ToString());
                            }
                        }
                       
                        if(dr["unidadpeso"]!= null)
                        objGuia.unidadpeso = dr["unidadpeso"].ToString();

                        if(dr["nrobultos"]!= null)
                        {
                            if (dr["nrobultos"].ToString() != "") 
                            {
                                objGuia.nrobultos = Convert.ToInt32(dr["nrobultos"].ToString());
                            }
                        }
                        
                        if(dr["modalidadtraslado"]!= null)
                        objGuia.modalidadtraslado = dr["modalidadtraslado"].ToString();

                        if(dr["fechatraslado"]!= null)
                        objGuia.fechatraslado = dr["fechatraslado"].ToString();

                        if(dr["trpublic_tipodoc"]!= null)
                        objGuia.trpublic_tipodoc = dr["trpublic_tipodoc"].ToString();

                        if(dr["trpublic_nrodoc"]!= null)
                        objGuia.trpublic_nrodoc = dr["trpublic_nrodoc"].ToString();

                        if(dr["trpublic_nombre"]!= null)
                        objGuia.trpublic_nombre = dr["trpublic_nombre"].ToString();

                        if(dr["trprivad_placa"]!=null)
                        objGuia.trprivad_placa =dr["trprivad_placa"].ToString();

                        if(dr["trprivad_tipodocconductor"]!= null)
                        objGuia.trprivad_tipodocconductor = dr["trprivad_tipodocconductor"].ToString();

                        if(dr["trprivad_nrodocconductor"]!= null)
                        objGuia.trprivad_nrodocconductor = dr["trprivad_nrodocconductor"].ToString();

                        if(dr["ubigeoptollegada"]!= null)
                        objGuia.ubigeoptollegada = dr["ubigeoptollegada"].ToString();

                        if(dr["direccionptollegada"]!=null)
                        objGuia.direccionptollegada = dr["direccionptollegada"].ToString();

                        if(dr["nrocontenedorimport"]!= null)
                        objGuia.nrocontenedorimport = dr["nrocontenedorimport"].ToString();

                        if(dr["ubigeoptopartida"]!= null)
                        objGuia.ubigeoptopartida = dr["ubigeoptopartida"].ToString();

                        if(dr["direccionptopartida"]!= null)
                        objGuia.direccionptopartida = dr["direccionptopartida"].ToString();
                        if(dr["codpuertoembarq"]!= null)
                        objGuia.codpuertoembarq = dr["codpuertoembarq"].ToString();

                        if(dr["nombrepuertoembarq"]!= null)
                        objGuia.nombrepuertoembarq = dr["nombrepuertoembarq"].ToString();

                        if(dr["codpuertodesembarq"]!= null)
                        objGuia.codpuertodesembarq = dr["codpuertodesembarq"].ToString();

                        if(dr["estado"]!= null)
                        objGuia.estado = dr["estado"].ToString();

                        if(dr["situacion"]!=null)
                        objGuia.situacion = dr["situacion"].ToString();

                        if(dr["enviado_email"]!= null)
                        objGuia.enviado_email = dr["enviado_email"].ToString();

                        if(dr["enviado_externo"]!= null)
                        objGuia.enviado_externo = dr["enviado_externo"].ToString();

                        if(dr["nombrexml"]!= null)
                        objGuia.nombreXML = dr["nombrexml"].ToString();

                        if(dr["valorresumen"]!= null)
                        objGuia.vResumen = dr["valorresumen"].ToString();

                        if(dr["valorfirma"]!= null)
                        objGuia.vFirma = dr["valorfirma"].ToString();

                        if(dr["nrodocempresa"]!=null)
                        objGuia.numerodocempresa = dr["nrodocempresa"].ToString();

                        if(dr["descripcionempresa"]!= null)
                        objGuia.descripcionempresa = dr["descripcionempresa"].ToString();
						
                        if(dr["desctipodocumento"]!= null)
                        objGuia.desctipodocumento = dr["desctipodocumento"].ToString();

                        ResListaGuia.listDetGuia.Add(objGuia);
                    }
                    if (ResListaGuia.listDetGuia.Count > EN_Constante.g_const_0)
                    {
                        ResListaGuia.ResplistaGuia.FlagVerificacion = true;
                        ResListaGuia.ResplistaGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                    } else {
                        ResListaGuia.ResplistaGuia.FlagVerificacion = false;
                        ResListaGuia.ResplistaGuia.DescRespuesta = "";

                    }
                }
                dr.Close();
            }
            return ResListaGuia;
        }
        catch(Exception ex)
        {
            ResListaGuia.ResplistaGuia.FlagVerificacion = false;
            ResListaGuia.ResplistaGuia.DescRespuesta = ex.Message;;
            return ResListaGuia;
        }
        finally 
        {
            connection.Close();
        }

        
        }
        public EN_RespuestaListaDetalleGuia listarDetalleGuia (EN_Guia pGuia) 
        {
        // DESCRIPCION: Listar DetalleGUia
        AD_Cado conector = new AD_Cado();
        EN_RespuestaListaDetalleGuia ResptaListaDetalleGuia = new EN_RespuestaListaDetalleGuia();
        List<EN_DetalleGuia> olsDetalleGuia  =  new List<EN_DetalleGuia>();        
        SqlDataReader dr = null;
        try
        {
            using (connection = new SqlConnection(conector.CadenaConexion()))
            {
                connection.Open();
                using (SqlCommand Command = new SqlCommand("Usp_listar_detalle_guia", connection)) 
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@idguia", SqlDbType.VarChar,15).Value = pGuia.id;
                    Command.Parameters.Add("@idempresa", SqlDbType.Int).Value = pGuia.empresa_id;
                    dr = Command.ExecuteReader();
                    while(dr.Read()) 
                    {
                        EN_DetalleGuia  objDetalleGuia = new  EN_DetalleGuia();    
                        if(dr["empresa_id"] != null)
                         {
                             if(dr["empresa_id"].ToString() != "") 
                             {
                                objDetalleGuia.empresa_id = Convert.ToInt32(dr["empresa_id"].ToString());
                             }
                         }
                        
                        if(dr["guia_id"] != null)
                        objDetalleGuia.guia_id = dr["guia_id"].ToString();
                        if(dr["line_id"]!=null)
                        objDetalleGuia.line_id = dr["line_id"].ToString();
                        if(dr["codigo"] != null)
                        objDetalleGuia.codigo = dr["codigo"].ToString();
                        if(dr["descripcion"] != null)
                        objDetalleGuia.descripcion =dr["descripcion"].ToString();
                        if(dr["unidad"] != null)
                        objDetalleGuia.unidad =dr["unidad"].ToString();
                        if(dr["cantidad"] != null)
                        {
                            if(dr["cantidad"].ToString() != "") {
                                 objDetalleGuia.cantidad = Convert.ToDouble(dr["cantidad"].ToString());  
                            }
                        }                    
                        ResptaListaDetalleGuia.listDetGuia.Add(objDetalleGuia);                              
                    }
                    if (ResptaListaDetalleGuia.listDetGuia.Count > EN_Constante.g_const_0) 
                    {
                        ResptaListaDetalleGuia.ResplistaDetalleGuia.FlagVerificacion = true;
                        ResptaListaDetalleGuia.ResplistaDetalleGuia.DescRespuesta = EN_Constante.g_const_consultaexitosa;
                    }
                    else 
                    {
                        ResptaListaDetalleGuia.ResplistaDetalleGuia.FlagVerificacion = false;
                        ResptaListaDetalleGuia.ResplistaDetalleGuia.DescRespuesta = "";
                    }
                    
                }
                dr.Close();
            }
            return ResptaListaDetalleGuia;
        }
        catch (Exception ex)
        {
            ResptaListaDetalleGuia.ResplistaDetalleGuia.FlagVerificacion = false;
            ResptaListaDetalleGuia.ResplistaDetalleGuia.DescRespuesta = ex.Message;
            return ResptaListaDetalleGuia;
        }
        finally 
        {
            connection.Close();
        }

        }
        public EN_ActualizarGuia GuardarGuiaNombreValorResumenFirma(EN_Guia pGuia, string nombrexml, string valorResumen , string valorFirma, string xmlgenerado, int prefijo)
        {
        //DESCRIPCION: Guardar Guia Nombre
        AD_Cado conector = new AD_Cado();  //Clase de acceso de datos
        EN_Concepto concepto = new EN_Concepto(); // clase de conceptos
        EN_ErrorWebService errorwebserv = new EN_ErrorWebService(); //Clase de error web service
        EN_ActualizarGuia ResActualizarGuia = new EN_ActualizarGuia(); //Clase de actualizar guia
         SqlDataReader dr = null; //SqlDataReader
        try
        {
            using (connection = new SqlConnection(conector.CadenaConexion()))
            {
                connection.Open();
                using (SqlCommand Command = new SqlCommand("actualizar_guiaxml", connection)) 
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@empresa_id", SqlDbType.Int).Value = pGuia.empresa_id;
                    Command.Parameters.Add("@guia_id", SqlDbType.VarChar,15).Value = pGuia.id;
                    Command.Parameters.Add("@tipodocumento_id", SqlDbType.VarChar,2).Value = pGuia.tipodocumento_id;
                    Command.Parameters.Add("@nombrexml", SqlDbType.VarChar,100).Value = nombrexml;
                    Command.Parameters.Add("@valorresumen", SqlDbType.VarChar,100).Value = valorResumen;
                    Command.Parameters.Add("@valorfirma", SqlDbType.VarChar).Value = valorFirma;
                    Command.Parameters.Add("@xmlgenerado", SqlDbType.VarChar).Value = xmlgenerado;
                    dr = Command.ExecuteReader();
                    while (dr.Read())
                    {
                        if(dr["estado"] != null && Convert.ToInt16(dr["estado"].ToString()) == EN_Constante.g_const_1){
                            ResActualizarGuia.RespActualizarGuia.FlagVerificacion = true;
                            ResActualizarGuia.RespActualizarGuia.DescRespuesta = dr["mensaje"].ToString();
                            concepto = listarConcepto(prefijo, EN_Constante.g_const_2000);
                            errorwebserv.DescripcionErr = concepto.conceptodesc;
                            errorwebserv.CodigoError = concepto.conceptocorr;
                            errorwebserv.TipoError = EN_Constante.g_const_0;
                            ResActualizarGuia.ErrorWebServ = errorwebserv;
                        }
                        else {
                            ResActualizarGuia.RespActualizarGuia.FlagVerificacion = false;
                            ResActualizarGuia.RespActualizarGuia.DescRespuesta = EN_Constante.g_const_error_interno;
                            errorwebserv.DescripcionErr = dr["mensaje"].ToString();
                            errorwebserv.CodigoError = EN_Constante.g_const_3000;
                            errorwebserv.TipoError = EN_Constante.g_const_1;
                            ResActualizarGuia.ErrorWebServ = errorwebserv;
                        }
                    }

                }
                dr.Close();
            }
            return ResActualizarGuia;
        }
        catch(Exception ex) 
        {
            ResActualizarGuia.RespActualizarGuia.FlagVerificacion = false;
            ResActualizarGuia.RespActualizarGuia.DescRespuesta = EN_Constante.g_const_error_interno;
            ResActualizarGuia.ErrorWebServ.TipoError = EN_Constante.g_const_1;
            ResActualizarGuia.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
            ResActualizarGuia.ErrorWebServ.DescripcionErr = ex.Message;
            return ResActualizarGuia;
        }
        finally 
        { 
            connection.Close();
        }

        }
        public EN_RegistrarGuiaElectronica RegistrarGuiaRemision(GuiaSunat guiaSunat)
        {
            //DESCRIPCION: Registrar Guia
            AD_Cado conector = new AD_Cado(); //Clase de acceso de datos
            EN_Concepto concepto = new EN_Concepto(); //clase de conceptos
            EN_ErrorWebService errorwebserv = new EN_ErrorWebService(); //clase de error de web service
            EN_RegistrarGuiaElectronica RespGuiaAceptada = new EN_RegistrarGuiaElectronica(); //clase de registro de guia
            SqlDataReader dr = null;
            try
            {
                // foreach(var item in guiaSunat.ListaDetalleGuia) {

                //     datastring = "[{"+ "guia_id:"+item.guia_id+","+"empresa_id:"+item.empresa_id+"," +
                //     "tipodocumento_id:"+item.tipodocumento_id+"," +"line_id:"+item.guia_id+","+"codigo:"+item.codigo+","+
                //     "descripcion:"+item.descripcion+","+"unidad:"+item.unidad+","+"cantidad:"+item.cantidad+"}" ;
                //     dataJson = datastring + dataJson;
                // }

               
                        // int i = 1;
                        // int conteo = guiaSunat.ListaDetalleGuia.Count;
                        // foreach (var item in guiaSunat.ListaDetalleGuia)
                        // {
                        //     if (i == 1)
                        //     {
                        //         datastring = datastring + "[";
                        //     }
                           
                        //         datastring = datastring + "{" + "\"guia_id\":" + "\""+item.guia_id+"\""+ "," + "\"empresa_id\":" + item.empresa_id + "," +
                        //        "\"tipodocumento_id\":" + "\""+item.tipodocumento_id +"\""+ "," + "\"line_id\":" + item.line_id + "," + "\"codigo\":" + "\""+item.codigo +"\""+ "," +
                        //        "\"descripcion\":" + "\""+item.descripcion +"\""+ "," + "\"unidad\":" + "\""+item.unidad +"\""+ "," + "\"cantidad\":" + item.cantidad + "}";
                                


                        //         if(i<conteo)
                        //         {
                        //             datastring = datastring + ",";
                        //         }
                                
                                
                            
                        //         i = i+ 1;
                           
                        // }

                        // datastring = datastring + "]";
                using (connection = new SqlConnection(conector.CadenaConexion()))
                {
                    connection.Open();
       
                    using (SqlCommand Command = new SqlCommand("Ups_registrar_guia_detalle", connection))
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@id", SqlDbType.VarChar,15).Value = guiaSunat.id;
                        Command.Parameters.Add("@empresa_id", SqlDbType.Int).Value = guiaSunat.empresa_id;
                        Command.Parameters.Add("@tipodocumento_id", SqlDbType.VarChar,2).Value = guiaSunat.tipodocumento_id;
                        Command.Parameters.Add("@serie", SqlDbType.VarChar,5).Value = guiaSunat.serie;
                        Command.Parameters.Add("@numero", SqlDbType.VarChar,10).Value = guiaSunat.numero;
                        Command.Parameters.Add("@fechaemision", SqlDbType.Date).Value = Convert.ToDateTime(guiaSunat.fechaemision, new CultureInfo(EN_Constante.g_const_cultureEsp));

                        Command.Parameters.Add("@horaemision", SqlDbType.VarChar,8).Value = guiaSunat.horaemision;
                        Command.Parameters.Add("@puntoemision", SqlDbType.Int).Value = guiaSunat.puntoemision;
                        Command.Parameters.Add("@observaciones", SqlDbType.VarChar,250).Value = guiaSunat.observaciones;
                        Command.Parameters.Add("@tipodocumentogrbaja", SqlDbType.VarChar,2).Value = guiaSunat.tipodocumentogrbaja;
                        Command.Parameters.Add("@numerdocumentogrbaja", SqlDbType.VarChar,15).Value = guiaSunat.numerodocumentogrbaja;
                        Command.Parameters.Add("@nombredocumentogrbaja", SqlDbType.VarChar,50).Value = guiaSunat.nombredocumentogrbaja;
                        Command.Parameters.Add("@tipodocumentorelac", SqlDbType.VarChar,2).Value = guiaSunat.tipodocumentorelac;
                        Command.Parameters.Add("@numerodocumentorelac", SqlDbType.VarChar,20).Value = guiaSunat.numerodocumentorelc;
                        Command.Parameters.Add("@numeracion_dam", SqlDbType.VarChar,20).Value = guiaSunat.numeracion_dam;
                        Command.Parameters.Add("@numero_manifiesto", SqlDbType.VarChar,20).Value = guiaSunat.numero_manifiesto;

                        Command.Parameters.Add("@destinatario_tipodoc", SqlDbType.VarChar,3).Value = guiaSunat.destinatario_tipodoc;
                        Command.Parameters.Add("@destinatario_nrodoc", SqlDbType.VarChar,20).Value = guiaSunat.destinatario_nrodoc;
                        Command.Parameters.Add("@destinatario_nombre", SqlDbType.VarChar,200).Value = guiaSunat.destinatario_nombre;
                        Command.Parameters.Add("@destinatario_email", SqlDbType.VarChar,200).Value = guiaSunat.destinatario_email;
                        Command.Parameters.Add("@tercero_tipodoc", SqlDbType.VarChar,3).Value = guiaSunat.tercero_tipodoc;
                        Command.Parameters.Add("@tercero_nrodoc", SqlDbType.VarChar,20).Value = guiaSunat.tercero_nrodoc;
                        Command.Parameters.Add("@tercero_nombre", SqlDbType.VarChar,200).Value = guiaSunat.tercero_nombre;
                        Command.Parameters.Add("@codmotivo", SqlDbType.VarChar,2).Value = guiaSunat.codmotivo;
                        Command.Parameters.Add("@descripcionmotivo", SqlDbType.VarChar,200).Value = guiaSunat.descripcionmotivo;
                        Command.Parameters.Add("@indicadortransbordo", SqlDbType.VarChar,2).Value = guiaSunat.indicadortransbordo;

                        Command.Parameters.Add("@pesobrutototal", SqlDbType.Decimal).Value = guiaSunat.pesobrutototal;
                        Command.Parameters.Add("@unidadpeso", SqlDbType.VarChar,4).Value = guiaSunat.unidadpeso;
                        Command.Parameters.Add("@nrobultos", SqlDbType.Int).Value = guiaSunat.nrobultos;
                        Command.Parameters.Add("@modalidadtraslado", SqlDbType.VarChar,2).Value = guiaSunat.modalidadtraslado;
                        Command.Parameters.Add("@fechatraslado", SqlDbType.Date).Value = Convert.ToDateTime(guiaSunat.fechatraslado, new CultureInfo(EN_Constante.g_const_cultureEsp)); 
                        Command.Parameters.Add("@trpublic_tipodoc", SqlDbType.VarChar,3).Value = guiaSunat.trpublic_tipodoc;
                        Command.Parameters.Add("@trpublic_nrodoc", SqlDbType.VarChar,20).Value = guiaSunat.trpublic_nrodoc;
                        Command.Parameters.Add("@trpublic_nombre", SqlDbType.VarChar,200).Value = guiaSunat.trpublic_nombre;
                        Command.Parameters.Add("@trprivad_placa", SqlDbType.VarChar,10).Value = guiaSunat.trprivad_placa;
                        Command.Parameters.Add("@trprivad_tipodocconductor", SqlDbType.VarChar,3).Value = guiaSunat.trprivad_tipodocconductor;

                        Command.Parameters.Add("@trprivad_nrodocconductor", SqlDbType.VarChar,20).Value = guiaSunat.trprivad_nrodocconductor;
                        Command.Parameters.Add("@ubigeoptollegada", SqlDbType.VarChar,8).Value = guiaSunat.ubigeoptollegada;
                        Command.Parameters.Add("@direccionptollegada", SqlDbType.VarChar,100).Value = guiaSunat.direccionptollegada;
                        Command.Parameters.Add("@nrocontenedorimport", SqlDbType.VarChar,20).Value = guiaSunat.nrocontenedorimport;
                        Command.Parameters.Add("@ubigeoptopartida", SqlDbType.VarChar,8).Value = guiaSunat.ubigeoptopartida;
                        Command.Parameters.Add("@direccionptopartida", SqlDbType.VarChar,100).Value = guiaSunat.direccionptopartida;
                        Command.Parameters.Add("@codpuertoembarq", SqlDbType.VarChar,10).Value = guiaSunat.codpuertoembarq;
                        Command.Parameters.Add("@nombrepuertoembarq", SqlDbType.VarChar,50).Value = guiaSunat.nombrepuertoembarq;
                        Command.Parameters.Add("@codpuertodesembarq", SqlDbType.VarChar,10).Value = guiaSunat.codpuertodesembarq;
                        Command.Parameters.Add("@estado", SqlDbType.Char,1).Value = guiaSunat.estado;

                        Command.Parameters.Add("@situacion", SqlDbType.Char,1).Value = guiaSunat.situacion;
                        Command.Parameters.Add("@enviado_email", SqlDbType.Char,2).Value = guiaSunat.enviado_email;
                        Command.Parameters.Add("@enviado_externo", SqlDbType.Char,2).Value = guiaSunat.enviado_externo;
                        Command.Parameters.Add("@usureg", SqlDbType.VarChar,20).Value = guiaSunat.usureg;
                        Command.Parameters.Add("@p_datajson", SqlDbType.NVarChar).Value = JsonConvert.SerializeObject(guiaSunat.ListaDetalleGuia);
                        Command.CommandTimeout = 15;   
                         dr = Command.ExecuteReader();
                         while(dr.Read()) 
                         {
                             if(dr["estadoRpta"] != null && Convert.ToInt16(dr["estadoRpta"].ToString()) == EN_Constante.g_const_1) 
                             {
                                RespGuiaAceptada.RespRegistrarGuiaElectronica.FlagVerificacion = true;                                    
                                RespGuiaAceptada.RespRegistrarGuiaElectronica.DescRespuesta = dr["descripcionRpta"].ToString();
                                concepto = listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_2000);
                                errorwebserv.DescripcionErr = concepto.conceptodesc;
                                errorwebserv.CodigoError = concepto.conceptocorr;
                                errorwebserv.TipoError = EN_Constante.g_const_0;
                                RespGuiaAceptada.ErrorWebServ = errorwebserv;

                             }
                             else {
                                RespGuiaAceptada.RespRegistrarGuiaElectronica.FlagVerificacion = false;                            
                                RespGuiaAceptada.RespRegistrarGuiaElectronica.DescRespuesta = EN_Constante.g_const_error_interno;
                                errorwebserv.DescripcionErr = dr["descripcionRpta"].ToString();
                                errorwebserv.CodigoError = Convert.ToInt32(dr["codigoRpta"].ToString());
                                errorwebserv.TipoError = EN_Constante.g_const_1;
                                RespGuiaAceptada.ErrorWebServ = errorwebserv;

                             }

                         }

                    }

                dr.Close();
                }
                return RespGuiaAceptada;
                 
            }
            catch (Exception ex)
            {
                
            RespGuiaAceptada.RespRegistrarGuiaElectronica.FlagVerificacion = false;           
            RespGuiaAceptada.RespRegistrarGuiaElectronica.DescRespuesta = EN_Constante.g_const_error_interno;
            RespGuiaAceptada.ErrorWebServ.TipoError = EN_Constante.g_const_1;
            RespGuiaAceptada.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
            RespGuiaAceptada.ErrorWebServ.DescripcionErr = ex.Message;
            return RespGuiaAceptada;
            }
            finally 
            {
                connection.Close();
            }

        }
        public EN_RegistrarGuiaElectronica GuardarGuiaAceptada(EN_Guia pGuia, string nombrexml, string xmlenviado,string cdrxml, string valorResumen, string firma, string mensaje, string ticket, int prefijo) 
        {
        //DESCRIPCION: Guardar Guia Aceptada
        AD_Cado conector = new AD_Cado(); //Clase de acceso a datos
        EN_Concepto concepto = new EN_Concepto(); //clase de conceptos
        EN_ErrorWebService errorwebserv = new EN_ErrorWebService(); // clase de error web service
        EN_RegistrarGuiaElectronica RespGuiaAceptada = new EN_RegistrarGuiaElectronica(); //Clase de registro de guia electrónica
        string numcompelectronico = pGuia.serie + pGuia.numero; //numero de electrónico
        SqlDataReader dr = null; //SqlDataReader
         try
         {
            using (connection = new SqlConnection(conector.CadenaConexion()))
            {
                connection.Open();
                using (SqlCommand Command = new SqlCommand("guardar_guiaelectronica", connection)) 
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@empresa_id", SqlDbType.Int).Value = pGuia.empresa_id;
                    Command.Parameters.Add("@guia_id", SqlDbType.VarChar,15).Value = pGuia.id;
                    Command.Parameters.Add("@tipodocumento_id", SqlDbType.VarChar,2).Value = pGuia.tipodocumento_id;
                    Command.Parameters.Add("@numcompelectronico", SqlDbType.VarChar,15).Value = numcompelectronico;       
                    Command.Parameters.Add("@nombrexml", SqlDbType.VarChar,30).Value = nombrexml;
                    Command.Parameters.Add("@xmlenviado", SqlDbType.VarChar).Value = xmlenviado;
                    Command.Parameters.Add("@xmlcdr", SqlDbType.VarChar).Value = cdrxml;
                    Command.Parameters.Add("@valorresumen", SqlDbType.VarChar,100).Value = valorResumen;
                    Command.Parameters.Add("@firma", SqlDbType.VarChar).Value = firma;
                    Command.Parameters.Add("@mensaje", SqlDbType.VarChar,200).Value = mensaje;
                    Command.Parameters.Add("@ticket", SqlDbType.VarChar,50).Value = ticket;
                    dr = Command.ExecuteReader();
                    while(dr.Read()) 
                    {
                        if(dr["estado"] != null && Convert.ToInt16(dr["estado"].ToString()) == EN_Constante.g_const_1) {
                            RespGuiaAceptada.RespRegistrarGuiaElectronica.FlagVerificacion = true;                                    
                            RespGuiaAceptada.RespRegistrarGuiaElectronica.DescRespuesta = dr["mensaje"].ToString();
                            concepto = listarConcepto(prefijo, EN_Constante.g_const_2000);
                            errorwebserv.DescripcionErr = concepto.conceptodesc;
                            errorwebserv.CodigoError = concepto.conceptocorr;
                            errorwebserv.TipoError = EN_Constante.g_const_0;
                            RespGuiaAceptada.ErrorWebServ = errorwebserv;
                        }
                        else {
                            RespGuiaAceptada.RespRegistrarGuiaElectronica.FlagVerificacion = false;                            
                            RespGuiaAceptada.RespRegistrarGuiaElectronica.DescRespuesta = EN_Constante.g_const_error_interno;
                            errorwebserv.DescripcionErr = dr["mensaje"].ToString();
                            errorwebserv.CodigoError = EN_Constante.g_const_3000;
                            errorwebserv.TipoError = EN_Constante.g_const_1;
                            RespGuiaAceptada.ErrorWebServ = errorwebserv;

                        }

                    }
                     
                }
                dr.Close();
            }
            return RespGuiaAceptada;
         } 
         catch(Exception ex) 
         {
            RespGuiaAceptada.RespRegistrarGuiaElectronica.FlagVerificacion = false;
            
            RespGuiaAceptada.RespRegistrarGuiaElectronica.DescRespuesta = EN_Constante.g_const_error_interno;
            RespGuiaAceptada.ErrorWebServ.TipoError = EN_Constante.g_const_1;
            RespGuiaAceptada.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
            RespGuiaAceptada.ErrorWebServ.DescripcionErr = ex.Message;
            return RespGuiaAceptada;
         }
         finally 
         {
             connection.Close();
         }
        }
        public EN_RegistrarGuiaRechazada GuardarGuiaRechazadaError(EN_Guia pGuia, string nombrexml, string xmlenviado, string cdrxml, string valorResumen, string firma, string errortext, string estado, string ticket, int prefijo)
        {
        //DESCRIPCION: Guardar Guia Rechazada
        AD_Cado conector = new AD_Cado(); //clase de acceso a datos
        EN_Concepto concepto = new EN_Concepto(); //clase de concepto
        EN_RegistrarGuiaRechazada RespRegistroGuiaRech = new EN_RegistrarGuiaRechazada(); //clase de registro de guia rechazada
        EN_ErrorWebService errorwebserv = new EN_ErrorWebService(); //clase de error web service
        SqlDataReader dr = null; //SqlDataReader
        string numcompelectronico = pGuia.serie + pGuia.numero; // numero comprobante electronico
        try
        {
            using (connection = new SqlConnection(conector.CadenaConexion()))
            {
                connection.Open();
                using (SqlCommand Command = new SqlCommand("guardar_guiarechazadaerror", connection)) 
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@empresa_id", SqlDbType.Int).Value = pGuia.empresa_id;
                    Command.Parameters.Add("@guia_id", SqlDbType.VarChar,15).Value = pGuia.id;
                    Command.Parameters.Add("@tipodocumento_id", SqlDbType.VarChar,2).Value = pGuia.tipodocumento_id;
                    Command.Parameters.Add("@numcomprobante", SqlDbType.VarChar,15).Value = numcompelectronico;       
                    Command.Parameters.Add("@nombrexml", SqlDbType.VarChar,30).Value = nombrexml;
                    Command.Parameters.Add("@xmlbase64", SqlDbType.VarChar).Value = xmlenviado;
                    Command.Parameters.Add("@cdrxml", SqlDbType.VarChar).Value = cdrxml;
                    Command.Parameters.Add("@valorresumen", SqlDbType.VarChar,100).Value = valorResumen;
                    Command.Parameters.Add("@firma", SqlDbType.VarChar).Value = firma;
                    Command.Parameters.Add("@mensajeerror", SqlDbType.VarChar,200).Value = errortext;
                    Command.Parameters.Add("@estado", SqlDbType.Char,1).Value = estado;
                    Command.Parameters.Add("@ticket", SqlDbType.VarChar,50).Value = ticket;                   
                    dr = Command.ExecuteReader();
                    while(dr.Read()) 
                    {
                         if(dr["estado"] != null && Convert.ToInt16(dr["estado"].ToString()) == EN_Constante.g_const_1) {
                            RespRegistroGuiaRech.RespRegistrarGuiaRechazada.FlagVerificacion = true;                  
                            RespRegistroGuiaRech.RespRegistrarGuiaRechazada.DescRespuesta = dr["mensaje"].ToString();
                            concepto = listarConcepto(prefijo, EN_Constante.g_const_2000);
                            errorwebserv.DescripcionErr = concepto.conceptodesc;
                            errorwebserv.CodigoError = concepto.conceptocorr;
                            errorwebserv.TipoError = EN_Constante.g_const_0;
                            RespRegistroGuiaRech.ErrorWebServ = errorwebserv;
                        }
                        else {
                            RespRegistroGuiaRech.RespRegistrarGuiaRechazada.FlagVerificacion = false;                           
                            RespRegistroGuiaRech.RespRegistrarGuiaRechazada.DescRespuesta = EN_Constante.g_const_error_interno;
                            errorwebserv.DescripcionErr = dr["mensaje"].ToString();
                            errorwebserv.CodigoError = EN_Constante.g_const_3000;
                            errorwebserv.TipoError = EN_Constante.g_const_1;
                            RespRegistroGuiaRech.ErrorWebServ = errorwebserv;
                        }

                    }
                }
                dr.Close();
            }
            return RespRegistroGuiaRech;
        } 
        catch(Exception ex) 
        {
            RespRegistroGuiaRech.RespRegistrarGuiaRechazada.FlagVerificacion = false;
            RespRegistroGuiaRech.RespRegistrarGuiaRechazada.DescRespuesta = EN_Constante.g_const_error_interno;
            RespRegistroGuiaRech.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
            RespRegistroGuiaRech.ErrorWebServ.TipoError = EN_Constante.g_const_1;
            RespRegistroGuiaRech.ErrorWebServ.DescripcionErr = ex.Message;
            return RespRegistroGuiaRech;
        }
        finally 
        {
            connection.Close();
        }

        }
        public List<EN_DocElectronico> consultarDocumentoElectronico (EN_DocElectronico pDocumento) 
        {
        //DESCRIPCION: Consultar documento electronico
        AD_Cado conector = new AD_Cado();
        List<EN_DocElectronico> listDocumentoElectronico = new List<EN_DocElectronico>();
        EN_Concepto concepto = new EN_Concepto();
        SqlDataReader dr = null;

        try
        {
            using (connection = new SqlConnection(conector.CadenaConexion()))
            {
                connection.Open();
                using (SqlCommand Command = new SqlCommand("Usp_consultar_docElectronico", connection)) 
                {
                    Command.CommandType = CommandType.StoredProcedure;
                    Command.Parameters.Add("@nrodocumento", SqlDbType.VarChar,11).Value = pDocumento.RucEmisor;
                    Command.Parameters.Add("@idtipodocumento", SqlDbType.VarChar,2).Value = pDocumento.Idtipodocumento;
                    Command.Parameters.Add("@serie", SqlDbType.VarChar,5).Value = pDocumento.Serie;
                    Command.Parameters.Add("@numero", SqlDbType.VarChar,10).Value = pDocumento.Numero;       
                    Command.Parameters.Add("@fechaemision", SqlDbType.Date).Value = Convert.ToDateTime(pDocumento.FechaEmision, new CultureInfo(EN_Constante.g_const_cultureEsp));;
                    dr = Command.ExecuteReader();
                    while(dr.Read()) 
                    { 
                       EN_DocElectronico objDocElectronico = new EN_DocElectronico();
                        if(dr["empresa_id"]!= null)
                        objDocElectronico.Idempresa = Convert.ToInt32(dr["empresa_id"].ToString());

                        if(dr["nrodocumento"]!= null)
                        objDocElectronico.RucEmisor = dr["nrodocumento"].ToString();

                        if(dr["id"]!= null)
                        objDocElectronico.IdDocumento = dr["id"].ToString();

                        if(dr["tipodocumento_id"]!= null)
                        objDocElectronico.Idtipodocumento = dr["tipodocumento_id"].ToString(); 

                         if(dr["serie"]!= null)
                        objDocElectronico.Serie = dr["serie"].ToString();

                        if(dr["numero"]!= null)
                        objDocElectronico.Numero = dr["numero"].ToString();

                         if(dr["fechaemision"]!= null)
                        objDocElectronico.FechaEmision = dr["fechaemision"].ToString();

                        if(dr["nombrexml"]!= null)
                        objDocElectronico.NombreXML = dr["nombrexml"].ToString();

                        if(dr["valorreseumen"]!= null)
                        objDocElectronico.ValorResumen = dr["valorreseumen"].ToString();

                        if(dr["valorfirma"]!= null)
                        objDocElectronico.Firma = dr["valorfirma"].ToString();

                        listDocumentoElectronico.Add(objDocElectronico);
                    }

                }
                dr.Close();
            }
            return listDocumentoElectronico;
        }
        catch (Exception)
            {
            return listDocumentoElectronico;

        }
        finally
        {
            connection.Close();
        }

        }
        public EN_EliminarGuia eliminarGuia (EN_Guia pGuia, int prefijo ) 
        {
        //DESCRIPCION: Eliminar Guia
        AD_Cado conector = new AD_Cado();  
        EN_Concepto concepto = new EN_Concepto();
        EN_ErrorWebService errorwebserv = new EN_ErrorWebService();
        EN_EliminarGuia RespEliminarGuia = new EN_EliminarGuia();      
        SqlDataReader dr = null;
            try
            {
                using (connection = new SqlConnection(conector.CadenaConexion()))
                {
                    connection.Open();
                    using (SqlCommand Command = new SqlCommand("Usp_eliminar_guia", connection)) 
                    {
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@id", SqlDbType.VarChar,15).Value = pGuia.id;
                        Command.Parameters.Add("@empresa_id", SqlDbType.Int).Value = pGuia.empresa_id;
                        Command.Parameters.Add("@tipodocumento_id", SqlDbType.VarChar,2).Value = pGuia.tipodocumento_id;
                        dr = Command.ExecuteReader();
                        while(dr.Read())  
                        {
                            if(dr["estado"] != null && Convert.ToInt16(dr["estado"].ToString()) == 1) {
                            RespEliminarGuia.RespEliminarGuia.FlagVerificacion = true;                               
                            RespEliminarGuia.RespEliminarGuia.DescRespuesta = dr["mensaje"].ToString();
                            concepto = listarConcepto(prefijo, EN_Constante.g_const_2002);
                            errorwebserv.DescripcionErr = concepto.conceptodesc;
                            errorwebserv.CodigoError = concepto.conceptocorr;
                            errorwebserv.TipoError = EN_Constante.g_const_0;
                            RespEliminarGuia.ErrorWebServ = errorwebserv;
                        }
                        else {
                            RespEliminarGuia.RespEliminarGuia.FlagVerificacion = false;                        
                            RespEliminarGuia.RespEliminarGuia.DescRespuesta = "Hubo un problema interno, por favor intentar nuevamente en unos momentos";
                            errorwebserv.DescripcionErr = dr["mensaje"].ToString();
                            errorwebserv.CodigoError = EN_Constante.g_const_3000;
                            errorwebserv.TipoError = EN_Constante.g_const_1;
                            RespEliminarGuia.ErrorWebServ = errorwebserv;
                        }

                        }
                    }
                }
                return RespEliminarGuia;
                
            }
            catch (Exception ex)
            {
                
               RespEliminarGuia.RespEliminarGuia.FlagVerificacion = false;
               RespEliminarGuia.RespEliminarGuia.DescRespuesta = "Hubo un problema interno, por favor intentar nuevamente en unos momentos";
               RespEliminarGuia.ErrorWebServ.TipoError = 1;
               RespEliminarGuia.ErrorWebServ.CodigoError = 3000;
               RespEliminarGuia.ErrorWebServ.DescripcionErr = ex.Message;
               return RespEliminarGuia;
            }
            finally {
                connection.Close();
            }

        }
        public EN_Concepto listarConcepto (int prefijo, short correlativo) 
        {
           //DESCRIPCION: Listar parametrizacion de conceptos
           AD_Cado conector = new AD_Cado();       
           SqlDataReader dr = null;
           EN_Concepto concepto = new EN_Concepto();
           try
           { 
               using (connection = new SqlConnection(conector.CadenaConexion()))
                {
                    connection.Open();
                    using (SqlCommand Command = new SqlCommand("Usp_listar_concepto", connection)) 
                    { 
                        Command.CommandType = CommandType.StoredProcedure;
                        Command.Parameters.Add("@prefijo", SqlDbType.Int).Value = prefijo;
                        Command.Parameters.Add("@correlativo", SqlDbType.VarChar,6).Value = correlativo;
                        Command.CommandTimeout = 0;
                        dr = Command.ExecuteReader();
                    while(dr.Read())
                    {
                        concepto.conceptopfij = Convert.ToInt32(dr[EN_Constante.g_const_0]);
                        concepto.conceptocorr = Convert.ToInt16(dr[EN_Constante.g_const_1]);
                        concepto.conceptodesc = dr[EN_Constante.g_const_2].ToString();
                    }

                    }
                    dr.Close();
                }
                return concepto;
               
           }
           catch (Exception ex)
           {
               
               throw ex;
           }
           finally
           {
               connection.Close();
           }
        }
        public Int32 registrar_log_inicio(string p_prog,string p_idoc,  string p_tdoc,int p_iemp,int p_ipvt,string p_nemp, int p_nive,
                                        string p_nwba, string p_durl, int p_tser,int p_tipo,int p_ferr,  int p_oerr, string p_erro,string p_derr,string p_fech,
                                        string p_hini, string p_date, string p_hfin, string p_dats,string p_user)
        {
            //DESCRIPCION: función para registrar log inicio
            AD_Cado conector = new AD_Cado(); 
            SqlDataReader dr; // Data Reader
            Int32 ntraLog = 0; // Número de transacción
            try
            {
                
                using (connection = new SqlConnection(conector.CadenaConexion()))
                {
                    connection.Open();
                    using(SqlCommand cmd = new SqlCommand("Usp_registrar_log_inicio_webapi", connection))
                    {
                        cmd.CommandType= CommandType.StoredProcedure;

                        cmd.Parameters.Add("@p_prog", SqlDbType.VarChar).Value= p_prog  ;
                        cmd.Parameters.Add("@p_idoc", SqlDbType.VarChar).Value= p_idoc  ;
                        cmd.Parameters.Add("@p_tdoc", SqlDbType.VarChar).Value= p_tdoc  ;
                        cmd.Parameters.Add("@p_iemp", SqlDbType.SmallInt).Value= p_iemp;
                        cmd.Parameters.Add("@p_ipvt", SqlDbType.VarChar).Value= p_ipvt  ;
                        cmd.Parameters.Add("@p_nemp", SqlDbType.VarChar).Value= p_nemp  ;
                        cmd.Parameters.Add("@p_nive", SqlDbType.SmallInt).Value= p_nive  ;
                        cmd.Parameters.Add("@p_nwba", SqlDbType.VarChar).Value=  p_nwba ;
                        cmd.Parameters.Add("@p_durl", SqlDbType.VarChar).Value= p_durl   ;
                        cmd.Parameters.Add("@p_tser", SqlDbType.SmallInt).Value = p_tser ;
                        cmd.Parameters.Add("@p_tipo", SqlDbType.SmallInt).Value= p_tipo  ;
                        cmd.Parameters.Add("@p_ferr", SqlDbType.SmallInt).Value= p_ferr  ;
                        cmd.Parameters.Add("@p_oerr", SqlDbType.SmallInt).Value= p_oerr  ;
                        cmd.Parameters.Add("@p_erro", SqlDbType.VarChar).Value= p_erro  ;
                        cmd.Parameters.Add("@p_derr", SqlDbType.VarChar).Value= p_derr  ;
                        cmd.Parameters.Add("@p_fech", SqlDbType.Date).Value = Convert.ToDateTime(p_fech, new CultureInfo(EN_Constante.g_const_cultureEsp));;
                        cmd.Parameters.Add("@p_hini", SqlDbType.VarChar).Value= p_hini  ;
                        cmd.Parameters.Add("@p_date", SqlDbType.VarChar).Value=  p_date ;
                        cmd.Parameters.Add("@p_hfin", SqlDbType.VarChar).Value= p_hfin  ;
                        cmd.Parameters.Add("@p_dats", SqlDbType.VarChar).Value= p_dats  ;
                        cmd.Parameters.Add("@p_user", SqlDbType.VarChar).Value= p_user  ;

                        cmd.CommandTimeout = EN_Constante.g_const_0;
                        dr = cmd.ExecuteReader();

                        while(dr.Read())
                        {
                            if(dr["estado"] != DBNull.Value && Convert.ToInt32(dr["estado"].ToString())== EN_Constante.g_const_1)
                            {
                                return Convert.ToInt32(dr["codigo"].ToString());
                            }
                            else
                            {
                                return EN_Constante.g_const_0;
                            }


                        }

                    }
                    dr.Close();
                }
                
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            finally
            {
                connection.Close();

            }
            return ntraLog;
            
        }
        public bool registrar_log_fin(int p_codi, int p_ferr, int p_oerr, string p_erro, string p_derr, string p_hfin,string p_dats, string p_date)
        {
            //DESCRIPCION: funcion para registrar log de fin
            AD_Cado CadCx = new AD_Cado(); // CONEXION
            SqlDataReader dr; //Data reader
            try
            {
                using (connection = new SqlConnection(CadCx.CadenaConexion()))
                {
                    connection.Open();
                    using (SqlCommand Comando = new SqlCommand("Usp_registrar_log_fin_webapi", connection))
                    {
                        Comando.CommandType = CommandType.StoredProcedure;

                        Comando.Parameters.Add("@p_codi", SqlDbType.Int).Value = p_codi;
                        Comando.Parameters.Add("@p_ferr", SqlDbType.SmallInt).Value = p_ferr;
                        Comando.Parameters.Add("@p_oerr", SqlDbType.SmallInt).Value = p_oerr;
                        Comando.Parameters.Add("@p_erro", SqlDbType.Char).Value = p_erro;
                        Comando.Parameters.Add("@p_derr", SqlDbType.Char).Value = p_derr;
                        Comando.Parameters.Add("@p_hfin", SqlDbType.Char).Value = p_hfin;
                        Comando.Parameters.Add("@p_dats", SqlDbType.Char).Value = p_dats;
                        Comando.Parameters.Add("@p_date", SqlDbType.Char).Value = p_date;
                                                

                        Comando.CommandTimeout = EN_Constante.g_const_0;
                        dr = Comando.ExecuteReader();

                        while (dr.Read())
                        {
                            if (dr["estado"] != DBNull.Value && Convert.ToInt32(dr["estado"].ToString()) == EN_Constante.g_const_1)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }

                        }

                    }
                    dr.Close();
                    
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }

        }
        public EN_RespuestaRegistro buscarParametroAppSettings(int codconcepto, int codcorrelativo)
            {
                // DESCRIPCION: BUSCAR CONSTANTES EN TABLA SUNAT
                EN_RespuestaRegistro respuesta = new EN_RespuestaRegistro();
                SqlDataReader reader ; //Data reader
                AD_Cado CadCx = new AD_Cado(); // CONEXION
                string response = string.Empty;
                try 
                {
                using (connection = new SqlConnection(CadCx.CadenaConexion()))
                {
                    connection.Open();
                    using (SqlCommand Comando = new SqlCommand("Usp_buscar_parametro", connection))
                    {
                        Comando.CommandType= CommandType.StoredProcedure;
                        Comando.Parameters.AddWithValue("@flag", 0);
                        Comando.Parameters.AddWithValue("@idBusqueda1", codconcepto);
                        Comando.Parameters.AddWithValue("@idBusqueda2", codcorrelativo);
                        Comando.CommandTimeout = EN_Constante.g_const_0;
                        reader = Comando.ExecuteReader();

      
                        while(reader.Read())
                        {
                            response = (reader["par_descripcion"]==null)?"":reader["par_descripcion"].ToString();
                        }

                        if (response != null && response.Trim() != "") 
                        {
                            respuesta.FlagVerificacion = true;
                            respuesta.DescRespuesta = response;

                        } else {
                            respuesta.FlagVerificacion = false;
                            respuesta.DescRespuesta = response;
                        }


                    }
                    reader.Close();
                }
                    
               
                 return respuesta;   
                }
                catch (Exception ex)
                {                  
                     respuesta.FlagVerificacion = false;
                     respuesta.DescRespuesta = ex.Message;
                     return respuesta;
                   
                }
                finally 
                {
                    connection.Close();
                  
                }
                
            }
        public EN_RespuestaData buscarCorrelativo(int empresa_id,int puntoemision_id , string tipodocumento_id,string serie)            
        {
                // DESCRIPCION: BUSCAR CORRELATIVO
                
                AD_Cado CadCx = new AD_Cado(); // CONEXION
                EN_RespuestaData respuestadata = new EN_RespuestaData();
                EN_Concepto concepto = new EN_Concepto(); //clase de conceptos
                EN_ErrorWebService errorwebserv = new EN_ErrorWebService(); //clase de error de web service
                SqlDataReader dr = null ; //Data reader

            try 
            {

                using (connection = new SqlConnection(CadCx.CadenaConexion()))
                {
                    connection.Open();
                    using (SqlCommand Comando = new SqlCommand("Usp_buscar_correlativo_guia", connection))
                    {
                        Comando.CommandType= CommandType.StoredProcedure;
                        Comando.Parameters.Add("@empresa_id", SqlDbType.Int).Value = empresa_id;
                        Comando.Parameters.Add("@puntoemision_id", SqlDbType.Int).Value = puntoemision_id;
                        Comando.Parameters.Add("@tipodocumento_id", SqlDbType.Char,2).Value = tipodocumento_id;
                        Comando.Parameters.Add("@serieguia", SqlDbType.Char,4).Value = serie;
                        Comando.CommandTimeout = EN_Constante.g_const_0;
                        dr = Comando.ExecuteReader();

                        while(dr.Read())
                        {
                            if (dr["estado"] != DBNull.Value && Convert.ToInt32(dr["estado"].ToString()) == EN_Constante.g_const_1)
                            {
                                respuestadata.data.flag = true;
                                respuestadata.data.dato = Convert.ToInt32(dr["correlativo"].ToString());
                                respuestadata.data.DescRespuesta = dr["mensaje"].ToString();
                                concepto = listarConcepto(EN_Constante.g_const_2, EN_Constante.g_const_2000);
                                errorwebserv.DescripcionErr = concepto.conceptodesc;
                                errorwebserv.CodigoError = concepto.conceptocorr;
                                errorwebserv.TipoError = EN_Constante.g_const_0;
                                respuestadata.ErrorWebServ = errorwebserv;


                            } else {
                                respuestadata.data.flag = false;
                                respuestadata.data.dato = Convert.ToInt32(dr["correlativo"].ToString());
                                respuestadata.data.DescRespuesta = EN_Constante.g_const_error_interno;
                                errorwebserv.DescripcionErr = dr["mensaje"].ToString();
                                errorwebserv.CodigoError = EN_Constante.g_const_3000;
                                errorwebserv.TipoError = EN_Constante.g_const_1;
                                respuestadata.ErrorWebServ = errorwebserv;

                            }
                        
                        }
                     }
                     dr.Close();

                    }
                    return respuestadata;
                }

                catch (Exception ex)
                {
                    respuestadata.data.flag = false;
                    respuestadata.data.DescRespuesta = EN_Constante.g_const_error_interno;
                    errorwebserv.DescripcionErr = ex.Message;
                    errorwebserv.CodigoError = EN_Constante.g_const_3000;
                    errorwebserv.TipoError = EN_Constante.g_const_1;
                    respuestadata.ErrorWebServ = errorwebserv;
                    return respuestadata;
                }
                finally {
                    connection.Close();
                }
                
            }
        
    }
}