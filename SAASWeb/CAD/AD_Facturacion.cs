﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_Facturacion.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos facturación
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using CEN;
using System.Text;
using System.Collections.Generic;

namespace CAD
{

 
        public partial class AD_Facturacion
        {
            private readonly AD_Cado _datosConexion;        // CLASE DE CONEXION CADO
            private readonly SqlConnection _sqlConexion;    // CONEXION SQL
            private readonly SqlCommand _sqlCommand;        // COMANDO SQL

            public AD_Facturacion()
            {
                // DESCRIPCION: CONSTRUCTOR DE CLASE 

                _datosConexion = new AD_Cado();
                _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
                _sqlCommand = new SqlCommand();
            }

            

            public bool validarSession(string p_frase, string p_usuario,string p_password ,ref int empresa_id, ref int puntoventa_id)
            {
                // DESCRIPCION: VALIDAR SESSION
                
                SqlCommand cmd ; //comando
                SqlDataReader reader ; //Data reader
                bool result = false; // resultado
                try 
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_validar_sesion_basica",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@p_frase",p_frase);
                    cmd.Parameters.AddWithValue("@p_usu", p_usuario);
                    cmd.Parameters.AddWithValue("@p_pass", p_password);

                    reader = cmd.ExecuteReader();

                   
                    while(reader.Read())
                    {
                        if(reader["estado"]!=null)
                        {
                            if(Convert.ToInt32(reader["estado"])==EN_Constante.g_const_1)
                            {
                                empresa_id= Convert.ToInt32(reader["idempresa"]);
                                puntoventa_id= Convert.ToInt32(reader["idpuntoventa"]);
                                result= true;
                            }
                            else
                            {
                                result=  false;
                            }
                        }
                        else
                        {
                           result=  false;
                        }
                    }
                    return result;

                   
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }
                
            }

            public string buscarParametroAppSettings(int codconcepto, int codcorrelativo)
            {
                // DESCRIPCION: BUSCAR CONSTANTES EN TABLA SUNAT
                
                SqlCommand cmd ; //comando
                SqlDataReader reader ; //Data reader
                try 
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_parametro",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@flag", 0);
                    cmd.Parameters.AddWithValue("@idBusqueda1", codconcepto);
                    cmd.Parameters.AddWithValue("@idBusqueda2", codcorrelativo);

                    reader = cmd.ExecuteReader();

                    string response="";
                    while(reader.Read())
                    {
                        response = (reader["par_descripcion"]==null)?"":reader["par_descripcion"].ToString();
                    }

                    return response;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }
                
            }
            public object buscarParametroGeneral(int flag,int idBusqueda1 , int idBusqueda2, string stringBusqueda , int intBusqueda, bool isString=true)
            {
                // DESCRIPCION: BUSCAR DATA PARAMETRO GENERAL - NO FLAG 5
                
                SqlCommand cmd ; //comando
                SqlDataReader reader ; //Data reader
                dynamic response = null;    // data de respuesta
                try 
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_parametro",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@flag", flag);
                    cmd.Parameters.AddWithValue("@idBusqueda1", idBusqueda1);
                    cmd.Parameters.AddWithValue("@idBusqueda2", idBusqueda2);
                    cmd.Parameters.AddWithValue("@stringBusqueda", stringBusqueda);
                    cmd.Parameters.AddWithValue("@intBusqueda", intBusqueda);

                    reader = cmd.ExecuteReader();

                
                   
                    while(reader.Read())
                    {
                        if(isString)
                            response = (reader[0]==null)?"":reader[0].ToString();
                        else
                            response= (int) (int)Interaction.IIf(Information.IsDBNull(reader[0]), (object)0, reader[0]);
                    }
                    

                    return response;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }
                
            }
            public object buscarCorrelativoOtrosCpe(int empresa_id,int puntoventa_id , string tipodocumento_id,string serie)            
            {
                // DESCRIPCION: BUSCAR CORRELATIVO
                
                SqlCommand cmd ; //comando
                SqlDataReader reader ; //Data reader
                int responseInt=0; // data de respuesta entero
                try 
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_correlativo_otrosCpe",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@empresa_id", empresa_id);
                    cmd.Parameters.AddWithValue("@puntoventa_id", puntoventa_id);
                    cmd.Parameters.AddWithValue("@tipodocumento_id", tipodocumento_id);
                    cmd.Parameters.AddWithValue("@serieDoc", serie);
                    
                    
                    

                    reader = cmd.ExecuteReader();

                    
                    
                    while(reader.Read())
                    {
                        if(Convert.ToInt32(reader["estado"])==EN_Constante.g_const_1)
                        {
                             responseInt =(int) (int)Interaction.IIf(Information.IsDBNull(reader["correlativo"]), (object)0, reader["correlativo"]);
                        }
                       
                    }

                    return responseInt;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }
                
            }
            
            public object buscarCorrelativo(int empresa_id,int puntoventa_id , string tipodocumento_id,string serie, int intBusqueda, string tipodoc_afecta)            
            {
                // DESCRIPCION: BUSCAR CORRELATIVO
                
                SqlCommand cmd ; //comando
                SqlDataReader reader ; //Data reader
                int responseInt=0; // data de respuesta entero
                try 
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_correlativo",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@empresa_id", empresa_id);
                    cmd.Parameters.AddWithValue("@puntoventa_id", puntoventa_id);
                    cmd.Parameters.AddWithValue("@tipodocumento_id", tipodocumento_id);
                    cmd.Parameters.AddWithValue("@serieDoc", serie);
                    cmd.Parameters.AddWithValue("@intBusqueda", intBusqueda);
                    cmd.Parameters.AddWithValue("@tipodoc_afecta", tipodoc_afecta);
                    

                    reader = cmd.ExecuteReader();

                    
                    
                    while(reader.Read())
                    {
                        if(Convert.ToInt32(reader["estado"])==EN_Constante.g_const_1)
                        {
                             responseInt =(int) (int)Interaction.IIf(Information.IsDBNull(reader["correlativo"]), (object)0, reader["correlativo"]);
                        }
                       
                    }

                    return responseInt;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }
                
            }
            public string buscarConstantesTablaSunat(string codtabla,string codalterno)
            {
                // DESCRIPCION: BUSCAR CONSTANTES EN TABLA SUNAT

                SqlCommand cmd ;            //comando
                SqlDataReader reader ;      //Data reader
                int contador=0;             // CONTADOR
                string response="";         // RESPUESTA
                StringBuilder bld ;         // ACUMULADOR DE STRING  
                try 
                {
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento

                    cmd.Parameters.AddWithValue("@flag", 0);
                    cmd.Parameters.AddWithValue("@codtabla", codtabla);
                    cmd.Parameters.AddWithValue("@codbusqueda", codalterno);

                    reader = cmd.ExecuteReader();
                    bld = new StringBuilder();

                    while(reader.Read())
                    {
                        if(codalterno=="RG"){
                            if(contador!=0)
                            {
                                bld.Append(",");
                            }
                               bld.Append(reader["descpcampo"].ToString());
                               contador++;
                        }else{
                              bld.Append(reader["descpcampo"].ToString());
                        }
                    }
                    
                    response = bld.ToString();

                    return response;
                    
                }
                catch (Exception ex)
                {
                     _sqlConexion.Close();
                    
                    throw ex;
                }
                finally {
                    _sqlConexion.Close();
                }

            }

            public string ObtenerCodigoSunat(string codtabla, string codigo)
            {
                // DESCRIPCION: BUSCAR CODIGO EN TABLA SUNAT
                string CodigoSunat = "";   // CODIGO SUNAT
                SqlCommand cmd;            // COMANDO
                SqlDataReader Rs;          // DATA READER
                try
                {
                   
                      _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 1);
                    cmd.Parameters.AddWithValue("@codtabla", codtabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda", codigo.Trim());

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            CodigoSunat = Conversions.ToString(Rs["descpcampo"]);
                    }

                    Rs.Close();
                    return CodigoSunat.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public string ObtenerDescripcionPorCodSunat(string strCodTabla, string strCodSunat)
            {
                // DESCRIPCION: BUSCAR DESCRIPCION POR CODIGO SUNAT

                string descripcion = "";    // DESCRIPCION
                SqlCommand cmd ;            // COMANDO
                SqlDataReader Rs;           // DATA READER

                try
                {
                  
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 2);
                    cmd.Parameters.AddWithValue("@codtabla",strCodTabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda",strCodSunat.Trim());

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            descripcion = Conversions.ToString(Rs["descpcampo"]);
                    }

                    Rs.Close();
                    return descripcion.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public string ObtenerDescripcionPorCodElemento(string strCodTabla, string strCodSistema)
            {
                // DESCRIPCION: BUSCAR DESCRIPCION POR CODIGO CODIGO ELEMENTO

                 string descripcion = "";       // DESCRIPCION
                 SqlCommand cmd;                // COMANDO
                 SqlDataReader Rs;              // DATA READER

                try
                {
                   
              
                 _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 3);
                    cmd.Parameters.AddWithValue("@codtabla",strCodTabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda",strCodSistema.Trim());

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            descripcion = Conversions.ToString(Rs["descpcampo"]);
                    }

                    Rs.Close();
                    return descripcion.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public string ObtenerCodigoAlternoPorCodigoSistema(string strCodTabla, string strCodSistema)
            {
                // DESCRIPCION: OBTENER CODIGO ALTERNO POR CODIGO DE SISTEMA
                string CodigoSunat = ""; // CODIGO SUNAT
                SqlCommand cmd ; // COMANDO
                SqlDataReader Rs; // DATA READER

                try
                {
                    
             
                    _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento
                    cmd.Parameters.AddWithValue("@flag", 4);
                    cmd.Parameters.AddWithValue("@codtabla",strCodTabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda",strCodSistema.Trim());

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            CodigoSunat = Conversions.ToString(Rs["descpcampo"]);
                    }

                    Rs.Close();
                    return CodigoSunat.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

             public string ObtenerCodigoElementoPorCodigoSistema(string strCodTabla, string strCodSistema)
            {
                // DESCRIPCION: OBTENER CODIGO ALTERNO POR CODIGO DE SISTEMA
                string CodigoSunat = ""; // CODIGO SUNAT
                SqlCommand cmd ; // COMANDO
                SqlDataReader Rs; // DATA READER

                try
                {
             
                 _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@flag", 5);
                    cmd.Parameters.AddWithValue("@codtabla",strCodTabla.Trim());
                    cmd.Parameters.AddWithValue("@codbusqueda",strCodSistema.Trim());

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            CodigoSunat = Conversions.ToString(Rs["descpcampo"]);
                    }

                    Rs.Close();
                    return CodigoSunat.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

          



            public bool GuardarNombreValorResumenFirma(EN_Documento oDocumento, string nombreXML, string valorResumen, string valorFirma, string xmlgenerado)
            {
                
                try
                {
                    _sqlConexion.Open();
                    _sqlCommand.CommandType = CommandType.StoredProcedure;
                    _sqlCommand.Connection = _sqlConexion;
                    _sqlCommand.CommandText = "actualizar_facturaxml";
                    _sqlCommand.Parameters.AddWithValue("@empresa_id", oDocumento.Idempresa);
                    _sqlCommand.Parameters.AddWithValue("@documento_id", oDocumento.Id);
                    _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", oDocumento.Idtipodocumento);
                    _sqlCommand.Parameters.AddWithValue("@nombrexml", nombreXML);
                    _sqlCommand.Parameters.AddWithValue("@valorresumen", valorResumen);
                    _sqlCommand.Parameters.AddWithValue("@valorfirma", valorFirma);
                    _sqlCommand.Parameters.AddWithValue("@xmlgenerado", xmlgenerado);

                    _sqlCommand.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }


             public string consultarSituacionCpeCB(string id, int empresa_id, int puntoventa_id)
            {
                // DESCRIPCION: CONSULTA SITUACION DE COMPROBANTE PARA PROCEDER A COMUNICADO DE BAJA
                string situacion = ""; // CODIGO SUNAT
                SqlCommand cmd ; // COMANDO
                SqlDataReader Rs; // DATA READER

                try
                {
             
                 _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_validarAceptado_cpe",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@id_doc", id);
                    cmd.Parameters.AddWithValue("@id_empresa",empresa_id);
                    cmd.Parameters.AddWithValue("@id_puntoventa",puntoventa_id);

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                            situacion = Conversions.ToString(Rs["situacion"]);
                    }

                    Rs.Close();
                    return situacion.Trim();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }


            
             public EN_CompDatosBaja  comprobarDatosDocBaja(string id, int empresa_id, int puntoventa_id, string tipodocumento_id)
            {
                // DESCRIPCION: COMPROBAR DATOS PARA PROCEDER A COMUNICADO DE BAJA
               
                SqlCommand cmd ; // COMANDO
                SqlDataReader Rs; // DATA READER

                EN_CompDatosBaja data = new EN_CompDatosBaja(); // DATA DATOS DE COMPROBACION
               

                try
                {
             
                 _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_comprobatDatos_docbaja",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@idDoc", id);
                    cmd.Parameters.AddWithValue("@empresa_id",empresa_id);
                    cmd.Parameters.AddWithValue("@puntoventa_id",puntoventa_id);
                    cmd.Parameters.AddWithValue("@tipodocumento_id",tipodocumento_id);
                    

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                          
                       
                            data.id = Conversions.ToString(Rs["id"]);
                            data.fecha = Convert.ToDateTime(Rs["fecha"]).ToString(EN_Constante.g_const_formfech);
                            data.tipodoc_afecta = Conversions.ToString(Rs["tipodoc_afecta"]);
                             
                        }
                            
                    }

                    Rs.Close();
                    return data;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }

            public EN_CompDatosBaja  comprobarDatosDocReversion(string id, int empresa_id, int puntoventa_id, string tipodocumento_id)
            {
                // DESCRIPCION: COMPROBAR DATOS PARA PROCEDER A COMUNICADO DE BAJA
               
                SqlCommand cmd ; // COMANDO
                SqlDataReader Rs; // DATA READER

                EN_CompDatosBaja data = new EN_CompDatosBaja(); // DATA DATOS DE COMPROBACION
               

                try
                {
             
                 _sqlConexion.Open();
                    cmd = new SqlCommand("Usp_comprobatDatos_docReversion",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@idDoc", id);
                    cmd.Parameters.AddWithValue("@empresa_id",empresa_id);
                    cmd.Parameters.AddWithValue("@puntoventa_id",puntoventa_id);
                    cmd.Parameters.AddWithValue("@tipodocumento_id",tipodocumento_id);
                    

                    Rs = cmd.ExecuteReader();
                    if (Rs.HasRows)
                    {
                        while (Rs.Read())
                        {
                          
                       
                            data.id = Conversions.ToString(Rs["id"]);
                            data.fecha = Convert.ToDateTime(Rs["fecha"]).ToString(EN_Constante.g_const_formfech);
                             
                        }
                            
                    }

                    Rs.Close();
                    return data;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    _sqlConexion.Close();
                }
            }
            public EN_RespuestaTablaSunat listarConstantesTablaSunat(int flag,string codtabla,string codalterno)
            {
                //DESCRIPCION: Llenamos los valores para el LOG
                var objLog = new EN_clsLog();
                EN_Concepto concepto = new EN_Concepto();
                AD_Guia objGuia = new AD_Guia();
                objLog.ClaseDatos = "AD_Documento";
                objLog.MetodoFuncionClase = "buscarConstantesTablaSunat";
                EN_RespuestaTablaSunat repuestaTablaSunat = new EN_RespuestaTablaSunat();
                int prefijo = EN_Constante.g_const_2; //Prefijo de parametros
                          
                try 
                {
                               
                    _sqlConexion.Open();
                    SqlCommand cmd = new SqlCommand("Usp_buscar_tabla_codigosunat",_sqlConexion);
                    cmd.CommandType= CommandType.StoredProcedure;
                    // flag=0 para buscar por cod alterno, y 1 para codelemento, 2 obtener descripcion y buscar por codsunat
                    // 3= obtienes descripcion y busqueda codelemento, 4 obtienes codalterno y busqueda codelemento

                    cmd.Parameters.AddWithValue("@flag", flag);
                    cmd.Parameters.AddWithValue("@codtabla", codtabla);
                    cmd.Parameters.AddWithValue("@codbusqueda", codalterno);

                    SqlDataReader reader = cmd.ExecuteReader();

                    string response="";
                    int contador=0;
                    while(reader.Read())
                    {
                        if(codalterno=="RG"){
                            if(contador!=0){response +=",";}
                          
                            response+="'"+reader["descpcampo"].ToString()+"'";
                            contador++;
                        }else{
                            response= reader["descpcampo"].ToString();
                        }
                    }
                    if (response!= null || response.Trim() != "") 
                    {
                        repuestaTablaSunat.ResplistaTablaSunat.FlagVerificacion = true;
                        repuestaTablaSunat.ResplistaTablaSunat.DescRespuesta = response;
                        concepto = objGuia.listarConcepto(prefijo, EN_Constante.g_const_2002);
                        repuestaTablaSunat.ErrorWebServ.TipoError = EN_Constante.g_const_0;
                        repuestaTablaSunat.ErrorWebServ.DescripcionErr = concepto.conceptodesc;
                        repuestaTablaSunat.ErrorWebServ.CodigoError = EN_Constante.g_const_2002;


                    } else {
                        repuestaTablaSunat.ResplistaTablaSunat.FlagVerificacion = false;
                        repuestaTablaSunat.ResplistaTablaSunat.DescRespuesta = EN_Constante.g_const_vacio;

                        repuestaTablaSunat.ErrorWebServ.TipoError = EN_Constante.g_const_1;
                        repuestaTablaSunat.ErrorWebServ.DescripcionErr = EN_Constante.g_const_vacio;
                        repuestaTablaSunat.ErrorWebServ.CodigoError = EN_Constante.g_const_2002;

                        

                    }

                    return repuestaTablaSunat;
                    
                }
                catch (Exception ex)
                {
                    repuestaTablaSunat.ResplistaTablaSunat.FlagVerificacion = false;
                    repuestaTablaSunat.ResplistaTablaSunat.DescRespuesta = EN_Constante.g_const_error_interno;
                    repuestaTablaSunat.ErrorWebServ.TipoError = EN_Constante.g_const_1;
                    repuestaTablaSunat.ErrorWebServ.DescripcionErr = ex.Message;
                    repuestaTablaSunat.ErrorWebServ.CodigoError = EN_Constante.g_const_3000;
                    return repuestaTablaSunat;
                   
                }
                finally {
                    _sqlConexion.Close();
                }
            }




        
        }
    
}
