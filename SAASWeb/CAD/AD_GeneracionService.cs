﻿/****************************************************************************************************************************************************************************************
 PROGRAMA: AD_GeneracionService.cs
 VERSION : 1.0
 OBJETIVO: Clase de acceso a datos generación de servicio
 FECHA   : 21/06/2021
 AUTOR   : Juan Alarcón - IDE SOLUTION
 ----------------+
 BITACORA        |
 ----------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 CODIGO      |  REQUERIMIENTO  | USUARIO   |  NOMBRE                        |    FECHA     |      MOTIVO
 ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 *************************************************************/
using System;
using Microsoft.VisualBasic;
using System.Data.SqlClient;
using CEN;
using System.Data;
using Newtonsoft.Json;
using System.Globalization;
namespace CAD
{


    public class AD_GeneracionService
    {
        private readonly AD_Cado _datosConexion;        // CLASE CONEXION CADO
        private readonly SqlConnection _sqlConexion;    // CONEXION SQL
        private readonly SqlCommand _sqlCommand;        // COMANDO SQL

        public AD_GeneracionService()
        {
            // DESCRIPCION: CONSTRUCTOR DE CLASE 

            _datosConexion = new AD_Cado();
            _sqlConexion = new SqlConnection(_datosConexion.CadenaConexion());
            _sqlCommand = new SqlCommand();
        }

        public bool GuardarDocumentoSunat(DocumentoSunat documentoSunat)
        {
             // DESCRIPCION: GUARDAR DOCUMENTO SUNAT

            SqlTransaction transaction=null;                    // TRANSACCION
            SqlCommand detCommand = new SqlCommand();           // COMANDO SQL DETALLE 
            SqlCommand docrefCommand = new SqlCommand();        // COMANDO SQL DOCUMENTO DE REFERENCIA
            SqlCommand docantCommand = new SqlCommand();        // COMANDO SQL DOCUMENTO ANTICIPO
            SqlCommand docfactguiaCommand = new SqlCommand();   // COMANDO SQL DOCUMENTO FACTURA GUIA
            SqlCommand docfactremitCommand = new SqlCommand();  // COMANDO SQL DOCUMENTO FACTURA REMITENTE
            SqlCommand docfacttransCommand = new SqlCommand();  // COMANDO SQL DOCUMENTO FACTURA TRANSACCION


            try
            {
                _sqlConexion.Open();
                transaction = _sqlConexion.BeginTransaction();

                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Transaction = transaction;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_registrar_documento";
                _sqlCommand.Transaction = transaction;

                detCommand.CommandType = CommandType.StoredProcedure;
                detCommand.Transaction = transaction;
                detCommand.Connection = _sqlConexion;
                detCommand.CommandText = "Usp_registrar_deta_documento";
                detCommand.Transaction = transaction;

                _sqlCommand.Parameters.AddWithValue("@id", documentoSunat.Id);
                _sqlCommand.Parameters.AddWithValue("@empresa_id", documentoSunat.Idempresa);
                _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", documentoSunat.Idtipodocumento);
                _sqlCommand.Parameters.AddWithValue("@serie_comprobante", documentoSunat.Serie);
                _sqlCommand.Parameters.AddWithValue("@nro_comprobante", documentoSunat.Numero);
                _sqlCommand.Parameters.AddWithValue("@puntoventa_id", documentoSunat.Idpuntoventa);
                _sqlCommand.Parameters.AddWithValue("@tipooperacion", documentoSunat.TipoOperacion);
                _sqlCommand.Parameters.AddWithValue("@tiponotacreddeb", documentoSunat.TipoNotaCredNotaDeb);
                _sqlCommand.Parameters.AddWithValue("@cliente_tipodoc", documentoSunat.Tipodoccliente);
                _sqlCommand.Parameters.AddWithValue("@cliente_nrodoc", documentoSunat.Nrodoccliente);
                _sqlCommand.Parameters.AddWithValue("@cliente_nombre", documentoSunat.Nombrecliente);
                _sqlCommand.Parameters.AddWithValue("@cliente_direccion", documentoSunat.Direccioncliente);
                _sqlCommand.Parameters.AddWithValue("@cliente_email", documentoSunat.Emailcliente);
                _sqlCommand.Parameters.AddWithValue("@fecha", Convert.ToDateTime(documentoSunat.Fecha.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion));
                _sqlCommand.Parameters.AddWithValue("@hora", documentoSunat.Hora);
                if ((Information.IsNothing(documentoSunat.Fechavencimiento) || documentoSunat.Fechavencimiento == ""))
                    _sqlCommand.Parameters.AddWithValue("@fechavencimientopago", DBNull.Value);
                else
                    _sqlCommand.Parameters.AddWithValue("@fechavencimientopago", Convert.ToDateTime(documentoSunat.Fechavencimiento.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion));
                _sqlCommand.Parameters.AddWithValue("@glosa", documentoSunat.Glosa);
                _sqlCommand.Parameters.AddWithValue("@moneda", documentoSunat.Moneda);
                _sqlCommand.Parameters.AddWithValue("@tipocambio", documentoSunat.Tipocambio);
                _sqlCommand.Parameters.AddWithValue("@descuento", documentoSunat.Descuento);
                _sqlCommand.Parameters.AddWithValue("@igvventa", documentoSunat.Igvventa);
                _sqlCommand.Parameters.AddWithValue("@iscventa", documentoSunat.Iscventa);
                _sqlCommand.Parameters.AddWithValue("@otrostributos", documentoSunat.OtrosTributos);
                _sqlCommand.Parameters.AddWithValue("@otroscargos", documentoSunat.OtrosCargos);
                _sqlCommand.Parameters.AddWithValue("@importeventa", documentoSunat.Importeventa);
                _sqlCommand.Parameters.AddWithValue("@regimenpercep", documentoSunat.Regimenpercep);
                _sqlCommand.Parameters.AddWithValue("@porcentpercep", documentoSunat.Porcentpercep);
                _sqlCommand.Parameters.AddWithValue("@importepercep", documentoSunat.Importepercep);
                _sqlCommand.Parameters.AddWithValue("@porcentdetrac", documentoSunat.Porcentdetrac);
                _sqlCommand.Parameters.AddWithValue("@importedetrac", documentoSunat.Importedetrac);
                _sqlCommand.Parameters.AddWithValue("@importerefdetrac", documentoSunat.Importedetrac);
                _sqlCommand.Parameters.AddWithValue("@importefinal", documentoSunat.Importefinal);
                _sqlCommand.Parameters.AddWithValue("@importeanticipo", documentoSunat.Importeanticipo);
                _sqlCommand.Parameters.AddWithValue("@indicaanticipo", documentoSunat.IndicaAnticipo);
                _sqlCommand.Parameters.AddWithValue("@estado", documentoSunat.Estado);
                _sqlCommand.Parameters.AddWithValue("@situacion", documentoSunat.Situacion);
                _sqlCommand.Parameters.AddWithValue("@numeroelectronico", documentoSunat.Numeroelectronico);
                _sqlCommand.Parameters.AddWithValue("@estransgratuita", documentoSunat.Estransgratuita);
                _sqlCommand.Parameters.AddWithValue("@enviado_email", documentoSunat.EnviadoMail);
                _sqlCommand.Parameters.AddWithValue("@enviado_externo", documentoSunat.Envioexterno);
                _sqlCommand.Parameters.AddWithValue("@tipoguiaremision", documentoSunat.TipoGuiaRemision);
                _sqlCommand.Parameters.AddWithValue("@guiaremision", documentoSunat.GuiaRemision);
                _sqlCommand.Parameters.AddWithValue("@tipodocotrodocref", documentoSunat.TipoDocOtroDocRef);
                _sqlCommand.Parameters.AddWithValue("@otrodocumentoref", documentoSunat.OtroDocumentoRef);
                _sqlCommand.Parameters.AddWithValue("@ordencompra", documentoSunat.OrdenCompra);
                _sqlCommand.Parameters.AddWithValue("@placavehiculo", documentoSunat.PlacaVehiculo);
                _sqlCommand.Parameters.AddWithValue("@montofise", documentoSunat.MontoFise);
                _sqlCommand.Parameters.AddWithValue("@tiporegimen", Interaction.IIf(Information.IsNothing(documentoSunat.TipoRegimen), DBNull.Value, documentoSunat.TipoRegimen));
                _sqlCommand.Parameters.AddWithValue("@codigobbsssujetodetrac", Interaction.IIf(Information.IsNothing(documentoSunat.CodigoBBSSSujetoDetrac), DBNull.Value, documentoSunat.CodigoBBSSSujetoDetrac));
                _sqlCommand.Parameters.AddWithValue("@numctabconacion", Interaction.IIf(Information.IsNothing(documentoSunat.NumCtaBcoNacion), DBNull.Value, documentoSunat.NumCtaBcoNacion));
                _sqlCommand.Parameters.AddWithValue("@bientransfamazonia", Interaction.IIf(Information.IsNothing(documentoSunat.BienTransfAmazonia), DBNull.Value, documentoSunat.BienTransfAmazonia));
                _sqlCommand.Parameters.AddWithValue("@servitransfamazonia", Interaction.IIf(Information.IsNothing(documentoSunat.ServiTransfAmazonia), DBNull.Value, documentoSunat.ServiTransfAmazonia));
                _sqlCommand.Parameters.AddWithValue("@contratoconstamazonia", Interaction.IIf(Information.IsNothing(documentoSunat.ContratoConstAmazonia), DBNull.Value, documentoSunat.ContratoConstAmazonia));

                                // agregado forma de pago
                _sqlCommand.Parameters.AddWithValue("@formapago", (documentoSunat.Formapago!=null)?documentoSunat.Formapago:"");
                _sqlCommand.Parameters.AddWithValue("@jsonpagocredito",JsonConvert.SerializeObject(documentoSunat.FormaPagoCredito));

                _sqlCommand.Parameters.AddWithValue("@hasRetencionIgv",(documentoSunat.hasRetencionIgv!=null)?documentoSunat.hasRetencionIgv:"NO");
                 _sqlCommand.Parameters.AddWithValue("@porcRetIgv",(documentoSunat.hasRetencionIgv!="NO")?documentoSunat.porcRetencionIgv: null);
                 _sqlCommand.Parameters.AddWithValue("@impRetIgv",(documentoSunat.hasRetencionIgv!="NO")?documentoSunat.impRetencionIgv: null);
                 _sqlCommand.Parameters.AddWithValue("@impOpeRetIgv",(documentoSunat.hasRetencionIgv!="NO")?documentoSunat.impOperacionRetencionIgv: null);
                 _sqlCommand.Parameters.AddWithValue("@usureg",(documentoSunat.UsuarioSession=="" || documentoSunat.UsuarioSession==null)?null: documentoSunat.UsuarioSession);



                _sqlCommand.ExecuteNonQuery();

                foreach (EN_DetalleDocumento detalle in documentoSunat.ListaDetalleDocumento)
                {
                    detCommand.Parameters.AddWithValue("@documento_id", detalle.Iddocumento);
                    detCommand.Parameters.AddWithValue("@empresa_id", detalle.Idempresa);
                    detCommand.Parameters.AddWithValue("@tipodocumento_id", detalle.Idtipodocumento);
                    detCommand.Parameters.AddWithValue("@line_id", detalle.Lineid);
                    detCommand.Parameters.AddWithValue("@codigoproducto", detalle.Codigo);
                    detCommand.Parameters.AddWithValue("@codigosunat", detalle.CodigoSunat);
                    detCommand.Parameters.AddWithValue("@nombreproducto", detalle.Producto);
                    detCommand.Parameters.AddWithValue("@afectoigv", detalle.AfectoIgv);
                    detCommand.Parameters.AddWithValue("@afectoisc", detalle.AfectoIsc);
                    detCommand.Parameters.AddWithValue("@tipoafectacionigv", detalle.Afectacionigv);
                    detCommand.Parameters.AddWithValue("@tipocalculoisc", detalle.SistemaISC);
                    detCommand.Parameters.AddWithValue("@unidad", detalle.Unidad);
                    detCommand.Parameters.AddWithValue("@cantidad", detalle.Cantidad);
                    detCommand.Parameters.AddWithValue("@preciounitario", detalle.Preciounitario);
                    detCommand.Parameters.AddWithValue("@precioreferencial", detalle.Precioreferencial);
                    detCommand.Parameters.AddWithValue("@valorunitario", detalle.Valorunitario);
                    detCommand.Parameters.AddWithValue("@valorbruto", detalle.Valorbruto);
                    detCommand.Parameters.AddWithValue("@valordscto", detalle.Valordscto);
                    detCommand.Parameters.AddWithValue("@valorcargo", detalle.Valorcargo);
                    detCommand.Parameters.AddWithValue("@valorventa", detalle.Valorventa);
                    detCommand.Parameters.AddWithValue("@isc", detalle.Isc);
                    detCommand.Parameters.AddWithValue("@igv", detalle.Igv);
                    detCommand.Parameters.AddWithValue("@total", detalle.Total);
                    detCommand.Parameters.AddWithValue("@factorigv", detalle.factorIgv);
                    detCommand.Parameters.AddWithValue("@factorisc", detalle.factorIsc);
                    detCommand.Parameters.AddWithValue("@tbvt_puntoorigen", Interaction.IIf(Information.IsNothing(detalle.TBVTPuntoOrigen), DBNull.Value, detalle.TBVTPuntoOrigen));
                    detCommand.Parameters.AddWithValue("@tbvt_descripcionorigen", Interaction.IIf(Information.IsNothing(detalle.TBVTDescripcionOrigen), DBNull.Value, detalle.TBVTDescripcionOrigen));
                    detCommand.Parameters.AddWithValue("@tbvt_puntodestino", Interaction.IIf(Information.IsNothing(detalle.TBVTPuntoDestino), DBNull.Value, detalle.TBVTPuntoDestino));
                    detCommand.Parameters.AddWithValue("@tbvt_descripciondestino", Interaction.IIf(Information.IsNothing(detalle.TBVTDescripcionDestino), DBNull.Value, detalle.TBVTDescripcionDestino));
                    detCommand.Parameters.AddWithValue("@tbvt_detalleviaje", Interaction.IIf(Information.IsNothing(detalle.TBVTDetalleViaje), DBNull.Value, detalle.TBVTDetalleViaje));
                    detCommand.Parameters.AddWithValue("@tbvt_valorrefpreliminar", Interaction.IIf(Information.IsNothing(detalle.TBVTValorRefPreliminar), DBNull.Value, detalle.TBVTValorRefPreliminar));
                    detCommand.Parameters.AddWithValue("@tbvt_valorrefcargaefectiva", Interaction.IIf(Information.IsNothing(detalle.TBVTValorCargaEfectiva), DBNull.Value, detalle.TBVTValorCargaEfectiva));
                    detCommand.Parameters.AddWithValue("@tbvt_valorrefcargautil", Interaction.IIf(Information.IsNothing(detalle.TBVTValorCargaUtil), DBNull.Value, detalle.TBVTValorCargaUtil));
                    detCommand.ExecuteNonQuery();
                    detCommand.Parameters.Clear();
                }

                if (!Information.IsNothing(documentoSunat.ListaDocumentoReferencia))
                {
                    docrefCommand.CommandType = CommandType.StoredProcedure;
                    docrefCommand.Transaction = transaction;
                    docrefCommand.Connection = _sqlConexion;
                    docrefCommand.CommandText = "usp_insert_docreferencia";
                    docrefCommand.Transaction = transaction;

                    foreach (EN_DocReferencia docReferencia in documentoSunat.ListaDocumentoReferencia)
                    {
                        docrefCommand.Parameters.AddWithValue("@empresa_id", docReferencia.Idempresa);
                        docrefCommand.Parameters.AddWithValue("@documento_id", docReferencia.Iddocumento);
                        docrefCommand.Parameters.AddWithValue("@tipodocumento_id", docReferencia.Idtipodocumento);
                        docrefCommand.Parameters.AddWithValue("@documentoref_id", docReferencia.Iddocumentoref);
                        docrefCommand.Parameters.AddWithValue("@tipodocumentoref_id", docReferencia.Idtipodocumentoref);
                        docrefCommand.Parameters.AddWithValue("@numerodocreferencia", docReferencia.Numerodocref);
                        docrefCommand.ExecuteNonQuery();
                        docrefCommand.Parameters.Clear();
                    }
                }

                if (!Information.IsNothing(documentoSunat.ListaDocumentoAnticipo))
                {
                    docantCommand.CommandType = CommandType.StoredProcedure;
                    docantCommand.Transaction = transaction;
                    docantCommand.Connection = _sqlConexion;
                    docantCommand.CommandText = "usp_insert_docanticipo";
                    docantCommand.Transaction = transaction;

                    foreach (EN_DocAnticipo docAnticipo in documentoSunat.ListaDocumentoAnticipo)
                    {
                        docantCommand.Parameters.AddWithValue("@documento_id", docAnticipo.documento_id);
                        docantCommand.Parameters.AddWithValue("@empresa_id", docAnticipo.empresa_id);
                        docantCommand.Parameters.AddWithValue("@tipodocumento_id", docAnticipo.tipodocumento_id);
                        docantCommand.Parameters.AddWithValue("@line_id", docAnticipo.line_id);
                        docantCommand.Parameters.AddWithValue("@tipodocanticipo", docAnticipo.tipodocanticipo);
                        docantCommand.Parameters.AddWithValue("@nrodocanticipo", docAnticipo.nrodocanticipo);
                        docantCommand.Parameters.AddWithValue("@montoanticipo", docAnticipo.montoanticipo);
                        docantCommand.Parameters.AddWithValue("@tipodocemisor", docAnticipo.tipodocemisor);
                        docantCommand.Parameters.AddWithValue("@nrodocemisor", docAnticipo.nrodocemisor);
                        docantCommand.ExecuteNonQuery();
                        docantCommand.Parameters.Clear();
                    }
                }

                if (!Information.IsNothing(documentoSunat.ListaDocRefeGuia))
                {
                    docantCommand.CommandType = CommandType.StoredProcedure;
                    docantCommand.Transaction = transaction;
                    docantCommand.Connection = _sqlConexion;
                    docantCommand.CommandText = "usp_insert_docrefeguia";
                    docantCommand.Transaction = transaction;

                    foreach (EN_DocRefeGuia docRefeGuia in documentoSunat.ListaDocRefeGuia)
                    {
                        docantCommand.Parameters.AddWithValue("@empresa_id", docRefeGuia.empresa_id);
                        docantCommand.Parameters.AddWithValue("@documento_id", docRefeGuia.documento_id);
                        docantCommand.Parameters.AddWithValue("@tipodocumento_id", docRefeGuia.tipodocumento_id);
                        docantCommand.Parameters.AddWithValue("@tipoguia", docRefeGuia.tipo_guia);
                        docantCommand.Parameters.AddWithValue("@numeroguia", docRefeGuia.numero_guia);
                        docantCommand.ExecuteNonQuery();
                        docantCommand.Parameters.Clear();
                    }
                }
                transaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }

        public int GuardarBajaSunat(BajaSunat bajaSunat)
        {
             // DESCRIPCION: GUARDAR DOCUMENTO SUNAT

            SqlTransaction transaction=default;                 // TRANSACCION
            SqlCommand detCommand = new SqlCommand();           // COMANDO SQL DETALLE 
            SqlCommand docrefCommand = new SqlCommand();        // COMANDO SQL DOCUMENTO DE REFERENCIA
            int idbaja = 0;                                     // ID BAJA
            
            try
            {
                _sqlConexion.Open();
                transaction = _sqlConexion.BeginTransaction();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Transaction = transaction;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_registrar_baja";
                _sqlCommand.Transaction = transaction;
                detCommand.CommandType = CommandType.StoredProcedure;
                detCommand.Transaction = transaction;
                detCommand.Connection = _sqlConexion;
                detCommand.CommandText = "Usp_registrar_deta_baja";
                detCommand.Transaction = transaction;
                _sqlCommand.Parameters.AddWithValue("@empresa_id", bajaSunat.Empresa_id);
                _sqlCommand.Parameters.AddWithValue("@tiporesumen", bajaSunat.Tiporesumen);
                _sqlCommand.Parameters.AddWithValue("@fecgenera_documento",  DateTime.ParseExact(bajaSunat.FechaDocumento,"dd/MM/yyyy", new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru) ));
                _sqlCommand.Parameters.AddWithValue("@fecgenera_comunicacion", DateTime.ParseExact(bajaSunat.FechaComunicacion,"dd/MM/yyyy",new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru) ));
                _sqlCommand.Parameters.AddWithValue("@estado", bajaSunat.Estado);
                _sqlCommand.Parameters.AddWithValue("@situacion", bajaSunat.Situacion);
                _sqlCommand.Parameters.AddWithValue("@usureg", bajaSunat.UsuarioSession);
                _sqlCommand.Parameters.Add("@idbaja", SqlDbType.Int).Direction = ParameterDirection.Output;
                _sqlCommand.ExecuteNonQuery();
                idbaja = (int)_sqlCommand.Parameters["@idbaja"].Value;
                int i = 1;      // contador
                foreach (EN_DetalleBaja detalle in bajaSunat.ListaDetalleBaja)
                {
                    detCommand.Parameters.AddWithValue("@baja_id", idbaja);
                    detCommand.Parameters.AddWithValue("@item", i);
                    detCommand.Parameters.AddWithValue("@tipodocumento_id", detalle.Tipodocumentoid);
                    detCommand.Parameters.AddWithValue("@serie", detalle.Serie);
                    detCommand.Parameters.AddWithValue("@numero", detalle.Numero);
                    detCommand.Parameters.AddWithValue("@motivobaja", detalle.MotivoBaja);
                    detCommand.ExecuteNonQuery();
                    detCommand.Parameters.Clear();
                    i = i + 1;
                }

                transaction.Commit();
                return idbaja;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }

        public bool GuardarResumenBajaPendiente(BajaSunat bajaSunat)
        {
             // DESCRIPCION: GUARDAR DOCUMENTO SUNAT

            SqlTransaction transaction=default;                 // TRANSACCION
            SqlCommand detCommand = new SqlCommand();           // COMANDO SQL DETALLE 
            SqlCommand docrefCommand = new SqlCommand();        // COMANDO SQL DOCUMENTO DE REFERENCIA
            int idbaja = 0;                                     // ID BAJA
            
            try
            {
                _sqlConexion.Open();
                transaction = _sqlConexion.BeginTransaction();
                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Transaction = transaction;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_registrar_bajapendiente";
                _sqlCommand.Transaction = transaction;
                detCommand.CommandType = CommandType.StoredProcedure;
                detCommand.Transaction = transaction;
                detCommand.Connection = _sqlConexion;
                detCommand.CommandText = "Usp_registrar_deta_bajapendiente";
                detCommand.Transaction = transaction;


                _sqlCommand.Parameters.AddWithValue("@empresa_id", bajaSunat.Empresa_id);
                _sqlCommand.Parameters.AddWithValue("@puntoventa_id", bajaSunat.Idpuntoventa);
                _sqlCommand.Parameters.AddWithValue("@fecgenera_documento",  DateTime.ParseExact(bajaSunat.FechaDocumento,"dd/MM/yyyy", new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru) ));
                _sqlCommand.Parameters.AddWithValue("@fecgenera_comunicacion", DateTime.ParseExact(bajaSunat.FechaComunicacion,"dd/MM/yyyy",new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru) ));
                _sqlCommand.Parameters.AddWithValue("@usureg", bajaSunat.UsuarioSession);
                _sqlCommand.Parameters.Add("@idbaja", SqlDbType.Int).Direction = ParameterDirection.Output;
                _sqlCommand.ExecuteNonQuery();
                idbaja = (int)_sqlCommand.Parameters["@idbaja"].Value;
                foreach (EN_DetalleBaja detalle in bajaSunat.ListaDetalleBaja)
                {


                    detCommand.Parameters.AddWithValue("@resumenbaja_id", idbaja);
                    detCommand.Parameters.AddWithValue("@documento_id",  detalle.Tipodocumentoid.ToString().Trim()+detalle.Serie.ToString().Trim()+"-"+detalle.Numero.ToString().Trim()   );
                    detCommand.Parameters.AddWithValue("@documento_idtipodocumento", detalle.Tipodocumentoid);
                    detCommand.Parameters.AddWithValue("@motivobaja", detalle.MotivoBaja);
                    detCommand.ExecuteNonQuery();
                    detCommand.Parameters.Clear();
                }

                transaction.Commit();
                if(idbaja>0) return true;
                else return false;
         
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                
                throw ex;
                
            }
        }



        public bool GuardarCpeSunat(OtroscpeSunat cpeSunat)
        {
            SqlTransaction transaction = null/* TODO Change to default(_) if this is not a reference type */;
            SqlCommand detCommand = new SqlCommand();
            SqlCommand docrefCommand = new SqlCommand();
            try
            {
                _sqlConexion.Open();
                transaction = _sqlConexion.BeginTransaction();

                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Transaction = transaction;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_registrar_otroscpe";
                _sqlCommand.Transaction = transaction;

                detCommand.CommandType = CommandType.StoredProcedure;
                detCommand.Transaction = transaction;
                detCommand.Connection = _sqlConexion;
                detCommand.CommandText = "Usp_registrar_deta_otroscpe";
                detCommand.Transaction = transaction;

                _sqlCommand.Parameters.AddWithValue("@id", cpeSunat.id);
                _sqlCommand.Parameters.AddWithValue("@empresa_id", cpeSunat.empresa_id);
                _sqlCommand.Parameters.AddWithValue("@puntoventa_id", cpeSunat.puntoventa_id);
                _sqlCommand.Parameters.AddWithValue("@tipodocumento_id", cpeSunat.tipodocumento_id);
                _sqlCommand.Parameters.AddWithValue("@serie", cpeSunat.serie);
                _sqlCommand.Parameters.AddWithValue("@numero", cpeSunat.numero);
                _sqlCommand.Parameters.AddWithValue("@fechaemision", Convert.ToDateTime(cpeSunat.fechaemision.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion));
                _sqlCommand.Parameters.AddWithValue("@persona_tipodoc", cpeSunat.persona_tipodoc);
                _sqlCommand.Parameters.AddWithValue("@persona_nrodoc", cpeSunat.persona_nrodoc);
                _sqlCommand.Parameters.AddWithValue("@persona_nombrecomercial", cpeSunat.persona_nombrecomercial);
                _sqlCommand.Parameters.AddWithValue("@persona_ubigeo", cpeSunat.persona_ubigeo);
                _sqlCommand.Parameters.AddWithValue("@persona_direccion", cpeSunat.persona_direccion);
                _sqlCommand.Parameters.AddWithValue("@persona_urbanizacion", cpeSunat.persona_urbanizacion);
                _sqlCommand.Parameters.AddWithValue("@persona_departamento", cpeSunat.persona_departamento);
                _sqlCommand.Parameters.AddWithValue("@persona_provincia", cpeSunat.persona_provincia);
                _sqlCommand.Parameters.AddWithValue("@persona_distrito", cpeSunat.persona_distrito);
                _sqlCommand.Parameters.AddWithValue("@persona_codpais", cpeSunat.persona_codpais);
                _sqlCommand.Parameters.AddWithValue("@persona_descripcion", cpeSunat.persona_descripcion);
                _sqlCommand.Parameters.AddWithValue("@persona_email", cpeSunat.persona_email);
                _sqlCommand.Parameters.AddWithValue("@regimen_retper", cpeSunat.regimen_retper);
                _sqlCommand.Parameters.AddWithValue("@tasa_retper", cpeSunat.tasa_retper);
                _sqlCommand.Parameters.AddWithValue("@observaciones", cpeSunat.observaciones);
                _sqlCommand.Parameters.AddWithValue("@importe_retper", cpeSunat.importe_retper);
                _sqlCommand.Parameters.AddWithValue("@moneda_impretper", cpeSunat.moneda_impretper);
                _sqlCommand.Parameters.AddWithValue("@importe_pagcob", cpeSunat.importe_pagcob);
                _sqlCommand.Parameters.AddWithValue("@moneda_imppagcob", cpeSunat.moneda_imppagcob);
                _sqlCommand.Parameters.AddWithValue("@estado", cpeSunat.estado);
                _sqlCommand.Parameters.AddWithValue("@situacion", cpeSunat.situacion);
                _sqlCommand.Parameters.AddWithValue("@enviado_email", cpeSunat.enviado_email);
                _sqlCommand.Parameters.AddWithValue("@enviado_externo", cpeSunat.enviado_externo);
                _sqlCommand.Parameters.AddWithValue("@usureg",(cpeSunat.UsuarioSession=="" || cpeSunat.UsuarioSession==null)?null: cpeSunat.UsuarioSession);

                _sqlCommand.ExecuteNonQuery();

                foreach (EN_DetalleOtrosCpe detalle in cpeSunat.ListaDetalleOtrosCpe)
                {
                    detCommand.Parameters.AddWithValue("@otroscpe_id", detalle.otroscpe_id);
                    detCommand.Parameters.AddWithValue("@empresa_id", detalle.empresa_id);
                    detCommand.Parameters.AddWithValue("@tipodocumento_id", detalle.tipodocumento_id);
                    detCommand.Parameters.AddWithValue("@line_id", detalle.line_id);
                    detCommand.Parameters.AddWithValue("@docrelac_tipodoc_id", detalle.docrelac_tipodoc_id);
                    detCommand.Parameters.AddWithValue("@docrelac_numerodoc", detalle.docrelac_numerodoc);
                    detCommand.Parameters.AddWithValue("@docrelac_fechaemision", Convert.ToDateTime(detalle.docrelac_fechaemision.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion) );
                    detCommand.Parameters.AddWithValue("@docrelac_importetotal", detalle.docrelac_importetotal);
                    detCommand.Parameters.AddWithValue("@docrelac_moneda", detalle.docrelac_moneda);
                    detCommand.Parameters.AddWithValue("@pagcob_fecha", Convert.ToDateTime(detalle.pagcob_fecha.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion) );
                    detCommand.Parameters.AddWithValue("@pagcob_numero", detalle.pagcob_numero);
                    detCommand.Parameters.AddWithValue("@pagcob_importe", detalle.pagcob_importe);
                    detCommand.Parameters.AddWithValue("@pagcob_moneda", detalle.pagcob_moneda);
                    detCommand.Parameters.AddWithValue("@retper_importe", detalle.retper_importe);
                    detCommand.Parameters.AddWithValue("@retper_moneda", detalle.retper_moneda);
                    detCommand.Parameters.AddWithValue("@retper_fecha", Convert.ToDateTime(detalle.retper_fecha.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion) );
                    detCommand.Parameters.AddWithValue("@retper_importe_pagcob", detalle.retper_importe_pagcob);
                    detCommand.Parameters.AddWithValue("@retper_moneda_pagcob", detalle.retper_moneda_pagcob);
                    detCommand.Parameters.AddWithValue("@tipocambio_monedaref", detalle.tipocambio_monedaref);
                    detCommand.Parameters.AddWithValue("@tipocambio_monedaobj", detalle.tipocambio_monedaobj);
                    detCommand.Parameters.AddWithValue("@tipocambio_factor", detalle.tipocambio_factor);

                    if (Information.IsNothing(detalle.tipocambio_fecha))
                        detCommand.Parameters.AddWithValue("@tipocambio_fecha", DBNull.Value);
                    else
                        detCommand.Parameters.AddWithValue("@tipocambio_fecha", Convert.ToDateTime(detalle.tipocambio_fecha.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion) );

                    detCommand.ExecuteNonQuery();
                    detCommand.Parameters.Clear();
                }
                transaction.Commit();
                return true;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }
        public int GuardarReversionSunat(ReversionSunat reversionSunat)
        {
            int idreversion = 0;
            SqlTransaction transaction = null/* TODO Change to default(_) if this is not a reference type */;
            SqlCommand detCommand = new SqlCommand();
            SqlCommand docrefCommand = new SqlCommand();
            try
            {
                _sqlConexion.Open();
                transaction = _sqlConexion.BeginTransaction();

                _sqlCommand.CommandType = CommandType.StoredProcedure;
                _sqlCommand.Transaction = transaction;
                _sqlCommand.Connection = _sqlConexion;
                _sqlCommand.CommandText = "Usp_registrar_reversion";
                _sqlCommand.Transaction = transaction;

                detCommand.CommandType = CommandType.StoredProcedure;
                detCommand.Transaction = transaction;
                detCommand.Connection = _sqlConexion;
                detCommand.CommandText = "Usp_registrar_deta_reversion";
                detCommand.Transaction = transaction;

                _sqlCommand.Parameters.AddWithValue("@empresa_id", reversionSunat.Empresa_id);
                _sqlCommand.Parameters.AddWithValue("@tiporesumen", reversionSunat.Tiporesumen);
                _sqlCommand.Parameters.AddWithValue("@fecgenera_documento", Convert.ToDateTime(reversionSunat.FechaDocumento.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion) );
                _sqlCommand.Parameters.AddWithValue("@fecgenera_comunicacion", Convert.ToDateTime(reversionSunat.FechaComunicacion.ToString(), new CultureInfo(EN_ConfigConstantes.Instance.const_CultureInfoPeru)).ToString(EN_Constante.g_const_formfechaGuion) );
                _sqlCommand.Parameters.AddWithValue("@estado", reversionSunat.Estado);
                _sqlCommand.Parameters.AddWithValue("@situacion", reversionSunat.Situacion);
                _sqlCommand.Parameters.AddWithValue("@usureg", reversionSunat.UsuarioSession);
                _sqlCommand.Parameters.Add("@idreversion", SqlDbType.Int).Direction = ParameterDirection.Output;
                _sqlCommand.ExecuteNonQuery();

                idreversion = (int)_sqlCommand.Parameters["@idreversion"].Value;

                int i = 1;
                foreach (EN_DetalleReversion detalle in reversionSunat.ListaDetalleReversion)
                {
                    detCommand.Parameters.AddWithValue("@reversion_id", idreversion);
                    detCommand.Parameters.AddWithValue("@item", i);
                    detCommand.Parameters.AddWithValue("@tipodocumento_id", detalle.Tipodocumentoid);
                    detCommand.Parameters.AddWithValue("@serie", detalle.Serie);
                    detCommand.Parameters.AddWithValue("@numero", detalle.Numero);
                    detCommand.Parameters.AddWithValue("@motivoreversion", detalle.MotivoReversion);
                    detCommand.ExecuteNonQuery();
                    detCommand.Parameters.Clear();
                    i = i + 1;
                }
                transaction.Commit();
                return idreversion;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }




       
               
        
    }

}
